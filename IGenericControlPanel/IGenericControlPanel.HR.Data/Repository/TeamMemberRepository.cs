﻿using AutoMapper;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.HR.Dto;
using IGenericControlPanel.HR.Dto.TeamMember;
using IGenericControlPanel.HR.IData;
using IGenericControlPanel.Model;
using IGenericControlPanel.Model.Content;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using IGenericControlPanel.SharedKernal.Utils;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.HR.Data
{
    public class TeamMemberRepository : BasicRepository<CPDbContext, TeamMember>, ITeamMemberRepository
    {
        #region Properties & Constructor 
        private IGamificationMovementsRepository _gamificationMovementRepo { get; }
        private IUserLanguageRepository UserLanguageRepository { get; }
        public IUserRepository UserRepository { get; }
        public TeamMemberRepository(CPDbContext dbContext, IMapper mapper,
            IUserLanguageRepository userLanguageRepository,
            IUserRepository userRepository,
            IGamificationMovementsRepository gamificationMovementRepo
            ) : base(dbContext, mapper)
        {
            UserLanguageRepository = userLanguageRepository;
            UserRepository = userRepository;
            _gamificationMovementRepo = gamificationMovementRepo;
        }

        #endregion

        #region Get Section
        public async Task<OperationResult<GenericOperationResult, IEnumerable<TeamMemberDto>>> GetTeamMembersAsync(
            int pageNumber = 0, int pageSize = 5)
        {
            var result = new OperationResult<GenericOperationResult,
                IEnumerable<TeamMemberDto>>(GenericOperationResult.Success);

            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var data = ValidEntities
                    .Skip(pageSize * pageNumber)
                    .Take(pageSize)
                    .Where(entity => userInfo.CurrentUserId == 0 || userInfo.IsUserAdmin
                     || (userInfo.IsHealthCareProvider ? userInfo.CurrentUserId == entity.CreatedBy : true))
                    .Select(entity => new TeamMemberDto()
                    {
                        About = entity.About,
                        Biography = entity.Biography,
                        CultureCode = entity.CultureCode,
                        Description = entity.Description,
                        Email = entity.Email,
                        Id = entity.Id,
                        LastName = entity.LastName,
                        Name = entity.Name,
                        PhoneNumber = entity.PhoneNumber,
                        Position = entity.Position,
                        ImageUrl = entity.Image == null ? null : entity.Image.Url
                    }).ToList();

                return result.UpdateResultData(data);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public async Task<OperationResult<GenericOperationResult, TeamMemberDto>> GetTeamMemberByIdAsync(
            int TeamMemberId)
        {
            var result = new OperationResult<GenericOperationResult,
                TeamMemberDto>(GenericOperationResult.Success);

            try
            {
                var data = ValidEntities
                    .Where(entity => entity.Id == TeamMemberId)
                    .Select(entity => new TeamMemberDto()
                    {
                        About = entity.About,
                        Biography = entity.Biography,
                        CultureCode = entity.CultureCode,
                        Description = entity.Description,
                        Email = entity.Email,
                        Id = entity.Id,
                        LastName = entity.LastName,
                        Name = entity.Name,
                        PhoneNumber = entity.PhoneNumber,
                        Position = entity.Position,
                        ImageUrl = entity.Image == null ? null : entity.Image.Url
                    }).SingleOrDefault();

                if (data is null)
                {
                    return result.AddError(ErrorMessages.TeamMemberNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                return await Task.FromResult(result.UpdateResultData(data));
            }
            catch (Exception)
            {
                return await Task.FromResult(result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed));
            }
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<InstructorDto>>> GetAllTeamMembersAsync()
        {
            var result = new OperationResult<GenericOperationResult,
                IEnumerable<InstructorDto>>(GenericOperationResult.Success);

            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var data = ValidEntities
                    .Where(entity => userInfo.CurrentUserId == 0 || userInfo.IsUserAdmin
                     || (userInfo.IsHealthCareProvider ? userInfo.CurrentUserId == entity.CreatedBy : true))
                    .Select(entity => new InstructorDto()
                    {
                        Id = entity.Id,
                        Name = entity.FullName
                    }).ToList();

                return result.UpdateResultData(data);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion

        #region Action Section
        public async Task<OperationResult<GenericOperationResult, TeamMemberFormDto>> ActionTeamMemberAsync(
            TeamMemberFormDto formDto)
        {
            var result = new OperationResult<GenericOperationResult,
                TeamMemberFormDto>(GenericOperationResult.Success);

            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();
                var Entity = ValidEntities
                    .Where(entity => entity.Id == formDto.Id)
                    .Include(entity => entity.Image)
                    .SingleOrDefault();

                if (Entity == null)
                {
                    Entity = new TeamMember()
                    {
                        About = formDto.About,
                        CultureCode = formDto.CultureCode,
                        Description = formDto.Description,
                        Email = formDto.Email,
                        LastName = formDto.LastName,
                        Name = formDto.Name,
                        PhoneNumber = formDto.PhoneNumber,
                        Position = formDto.Position,
                        UserId = userInfo.CurrentUserId,
                    };
                    if (formDto.ImageDetails != null)
                    {
                        var imageFile = new TeamMemberFile()
                        {

                            Checksum = formDto.ImageDetails.Checksum,
                            FileContentType = formDto.ImageDetails.FileContentType,
                            Description = formDto.ImageDetails.Description,
                            Extension = formDto.ImageDetails.Extension,
                            FileName = formDto.ImageDetails.FileName,
                            Name = formDto.ImageDetails.Name,
                            Url = formDto.ImageDetails.Url
                        };
                        Entity.Image = imageFile;
                    }
                    await Context.AddAsync(Entity);
                }
                else
                {
                    Entity.About = formDto.About;
                    Entity.CultureCode = formDto.CultureCode;
                    Entity.Description = formDto.Description;
                    Entity.Email = formDto.Email;
                    Entity.Id = formDto.Id;
                    Entity.LastName = formDto.LastName;
                    Entity.Name = formDto.Name;
                    Entity.CreatedBy = Entity.CreatedBy;
                    Entity.PhoneNumber = formDto.PhoneNumber;
                    Entity.Position = formDto.Position;
                    if (formDto.ImageDetails != null)
                    {
                        if (Entity.Image != null)
                        {
                            Entity.Image.Checksum = formDto.ImageDetails.Checksum;
                            Entity.Image.FileContentType = formDto.ImageDetails.FileContentType;
                            Entity.Image.Description = formDto.ImageDetails.Description;
                            Entity.Image.Extension = formDto.ImageDetails.Extension;
                            Entity.Image.FileName = formDto.ImageDetails.FileName;
                            Entity.Image.Name = formDto.ImageDetails.Name;
                            Entity.Image.Url = formDto.ImageDetails.Url;
                        }
                        else
                        {
                            var imageFile = new TeamMemberFile()
                            {

                                Checksum = formDto.ImageDetails.Checksum,
                                FileContentType = formDto.ImageDetails.FileContentType,
                                Description = formDto.ImageDetails.Description,
                                Extension = formDto.ImageDetails.Extension,
                                FileName = formDto.ImageDetails.FileName,
                                Name = formDto.ImageDetails.Name,
                                Url = formDto.ImageDetails.Url
                            };
                            Entity.Image = imageFile;
                        }
                    }
                    Context.Update(Entity);
                }

                await Context.SaveChangesAsync();

                if (formDto.Id == 0)
                {
                    var savePointsResult = await _gamificationMovementRepo
                        .SavePoints(new List<string>
                    {
                        GamificationNames.AddTeamMember
                    });

                    if (!savePointsResult.IsSuccess)
                    {
                        return result.AddError(savePointsResult.ErrorMessages)
                            .UpdateResultStatus(GenericOperationResult.Failed);
                    }
                }


                formDto.Id = Entity.Id;
                formDto.ImageUrl = Entity.Image == null ? null : Entity.Image.Url;

                return result.UpdateResultData(formDto);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion

        #region Remove Section
        public async Task<OperationResult<GenericOperationResult, string>> RemoveTeamMemberAsync(
            int TeamMemberId)
        {
            var result = new OperationResult<GenericOperationResult,
                string>(GenericOperationResult.Success);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var Entity = ValidEntities
                    .Where(entity => entity.Id == TeamMemberId
                    && (userInfo.IsUserAdmin || userInfo.CurrentUserId == entity.CreatedBy))
                    .Include(entity => entity.Image)
                    .SingleOrDefault();

                if (Entity is null)
                {
                    return result.AddError(ErrorMessages.TeamMemberNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                var removeImageUrl = Entity.Image == null ? null : Entity.Image.Url;
                if (Entity.Image != null)
                {
                    Context.Remove(Entity.Image);
                }
                Context.Remove(Entity);
                Context.SaveChanges();

                return result.UpdateResultData(removeImageUrl);
            }

            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.HR.Dto.Partners;
using IGenericControlPanel.HR.IData.Interfaces;
using IGenericControlPanel.Model.HR;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;

namespace IGenericControlPanel.HR.Data.Repository
{
    public class PartnersRepository : BasicRepository<CPDbContext, PartnerSet>, IPartnersRepository
    {
        public PartnersRepository(CPDbContext context) : base(context)
        {

        }
        public OperationResult<GenericOperationResult, PartnersFormDto> ActionPartner(PartnersFormDto formDto)
        {
            OperationResult<GenericOperationResult, PartnersFormDto> result = new(GenericOperationResult.ValidationError);
            try
            {
                PartnerSet Entity = ValidEntities.Where(entity => entity.Id == formDto.Id).SingleOrDefault();
                if (Entity == null)
                {
                    Entity = new PartnerSet()
                    {
                        Name = formDto.Name,
                        Link = formDto.Link,
                        ImageUrl = formDto.ImageUrl
                    };
                    _ = Context.Add(Entity);
                }
                else
                {
                    Entity.Name = formDto.Name;
                    Entity.Link = formDto.Link;
                    if (formDto.ImageUrl != null)
                    {
                        Entity.ImageUrl = formDto.ImageUrl;
                    }
                }
                _ = Context.SaveChanges();
                formDto.Id = Entity.Id;
                return result.UpdateResultData(formDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }

        public OperationResult<GenericOperationResult, PartnersDto> GetPartner(int PartnerId)
        {
            OperationResult<GenericOperationResult, PartnersDto> result = new(GenericOperationResult.ValidationError);
            try
            {
                PartnersDto data = ValidEntities.Where(entity => entity.Id == PartnerId).Select(entity => new PartnersDto()
                {
                    Id = entity.Id,
                    ImageUrl = entity.ImageUrl,
                    Link = entity.Link,
                    Name = entity.Name
                }).SingleOrDefault();
                return data == null
                    ? result.UpdateResultStatus(GenericOperationResult.NotFound)
                    : result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(data);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }

        public OperationResult<GenericOperationResult, IEnumerable<PartnersDto>> GetPartners(int pageSize = 6, int pageNumber = 0)
        {
            OperationResult<GenericOperationResult, IEnumerable<PartnersDto>> result = new(GenericOperationResult.ValidationError);
            try
            {
                List<PartnersDto> data = ValidEntities.Skip(pageSize * pageNumber).Take(pageSize).Select(entity => new PartnersDto()
                {
                    Id = entity.Id,
                    ImageUrl = entity.ImageUrl,
                    Link = entity.Link,
                    Name = entity.Name
                }).ToList();
                return data == null
                    ? result.UpdateResultStatus(GenericOperationResult.NotFound)
                    : result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(data);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }

        public OperationResult<GenericOperationResult> RemovePartner(int PartnersId)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.ValidationError);
            try
            {
                PartnerSet Entity = ValidEntities.Where(entity => entity.Id == PartnersId).SingleOrDefault();
                if (Entity == null)
                {
                    return result.UpdateResultStatus(GenericOperationResult.NotFound);
                }
                _ = Context.Remove(Entity);
                _ = Context.SaveChanges();
                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }
    }
}

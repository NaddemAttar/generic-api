﻿
namespace IGenericControlPanel.UserExperiences.Dto;
public class FileDataDto
{
    public int Id { get; set; }
    public string Url { get; set; }
    public string Extension { get; set; }
}

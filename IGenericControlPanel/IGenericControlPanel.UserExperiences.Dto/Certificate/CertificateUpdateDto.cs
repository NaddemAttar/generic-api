﻿
using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.UserExperiences.Dto.Certificate;
public class CertificateUpdateDto: CertificateBasicDto
{
    public List<IFormFile> CertificateFiles { get; set; }
    public List<int> RemoveFileIds { get; set; }
}

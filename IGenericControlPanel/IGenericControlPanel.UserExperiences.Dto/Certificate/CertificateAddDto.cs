﻿
using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.UserExperiences.Dto.Certificate;
public class CertificateAddDto: CertificateBasicDto
{
    public List<IFormFile> CertificateFiles { get; set; }
}

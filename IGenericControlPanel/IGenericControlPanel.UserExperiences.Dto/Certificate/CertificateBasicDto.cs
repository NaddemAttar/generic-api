﻿
namespace IGenericControlPanel.UserExperiences.Dto.Certificate;
public class CertificateBasicDto
{
    public int Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Institution { get; set; }
}

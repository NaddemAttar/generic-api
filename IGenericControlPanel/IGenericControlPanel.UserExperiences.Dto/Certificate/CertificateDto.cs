﻿
namespace IGenericControlPanel.UserExperiences.Dto.Certificate;
public class CertificateDto: CertificateBasicDto
{
    public string CertificateImageUrl { get; set; }
}

﻿
namespace IGenericControlPanel.UserExperiences.Dto.Certificate;
public class CertificateDetailsDto: CertificateBasicDto
{
    public IEnumerable<FileDataDto> CertificateFileDtos { get; set; }
}

﻿
namespace IGenericControlPanel.UserExperiences.Dto.Experience;
public class ExperienceDto: ExperienceBasicDto
{
    public string ExperienceImageUrl { get; set; }
}

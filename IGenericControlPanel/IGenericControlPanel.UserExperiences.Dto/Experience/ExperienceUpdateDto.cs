﻿
using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.UserExperiences.Dto.Experience;
public class ExperienceUpdateDto: ExperienceBasicDto
{
    public List<IFormFile> ExperienceFiles { get; set; }
    public List<int> RemoveFileIds { get; set; }
}

﻿
using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.UserExperiences.Dto.Experience;
public class ExperienceAddDto: ExperienceBasicDto
{
    public List<IFormFile> ExperienceFiles { get; set; }
}

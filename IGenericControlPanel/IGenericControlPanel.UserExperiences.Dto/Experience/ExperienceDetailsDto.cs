﻿
namespace IGenericControlPanel.UserExperiences.Dto.Experience;
public class ExperienceDetailsDto: ExperienceBasicDto
{
    public List<FileDataDto> ExperienceFiles { get; set; }
}

﻿
namespace IGenericControlPanel.UserExperiences.Dto.Experience;
public class ExperienceBasicDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Institution { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime? EndDate { get; set; }
    public string Link { get; set; }
    public string Description { get; set; }
}

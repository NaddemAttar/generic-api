﻿
using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.UserExperiences.Dto.License;
public class LicenseUpdateDto: LicenseBasicDto
{
    public List<IFormFile> LicenseFiles { get; set; }
    public List<int> RemoveFileIds { get; set; }
}

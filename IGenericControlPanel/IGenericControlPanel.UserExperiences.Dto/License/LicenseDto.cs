﻿
namespace IGenericControlPanel.UserExperiences.Dto.License;
public class LicenseDto: LicenseBasicDto
{
    public string LicenseUrl { get; set; }
}

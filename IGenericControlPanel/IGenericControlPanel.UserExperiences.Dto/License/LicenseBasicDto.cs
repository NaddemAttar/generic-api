﻿
namespace IGenericControlPanel.UserExperiences.Dto.License;
public class LicenseBasicDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
}

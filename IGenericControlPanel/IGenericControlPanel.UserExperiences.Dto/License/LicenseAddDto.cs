﻿
using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.UserExperiences.Dto.License;
public class LicenseAddDto: LicenseBasicDto
{
    public List<IFormFile> LicenseFiles { get; set; }
}

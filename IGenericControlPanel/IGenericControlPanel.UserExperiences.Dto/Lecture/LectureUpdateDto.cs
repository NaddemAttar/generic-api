﻿
using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.UserExperiences.Dto.Lecture;
public class LectureUpdateDto: LectureBasicDto
{
    public List<IFormFile> LectureFiles { get; set; }
    public List<int> RemoveFileIds { get; set; }
}

﻿
namespace IGenericControlPanel.UserExperiences.Dto.Lecture;
public class LectureDetailsDto: LectureBasicDto
{
    public IEnumerable<FileDataDto> LectureFileDtos { get; set; }
}

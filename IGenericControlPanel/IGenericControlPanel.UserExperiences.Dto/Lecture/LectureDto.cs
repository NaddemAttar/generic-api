﻿
namespace IGenericControlPanel.UserExperiences.Dto.Lecture;
public class LectureDto: LectureBasicDto
{
    public string LectureImageUrl { get; set; }
}

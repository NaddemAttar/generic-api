﻿
using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.UserExperiences.Dto.Lecture;
public class LectureAddDto: LectureBasicDto
{
    public List<IFormFile> LectureFiles { get; set; }
}

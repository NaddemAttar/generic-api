﻿
using IGenericControlPanel.Security.Dto.User;
using IGenericControlPanel.UserExperiences.Dto.Certificate;
using IGenericControlPanel.UserExperiences.Dto.Education;
using IGenericControlPanel.UserExperiences.Dto.Experience;
using IGenericControlPanel.UserExperiences.Dto.Lecture;
using IGenericControlPanel.UserExperiences.Dto.License;

namespace IGenericControlPanel.UserExperiences.Dto.Resume;
public class ResumeDto
{
    public NormalUserDto ProfileInfo { get; set; }
    public IEnumerable<ExperienceDetailsDto> Experiences { get; set; }
    public IEnumerable<EducationDetailsDto> Educations { get; set; }
    public IEnumerable<CertificateDetailsDto> Certificates { get; set; }
    public IEnumerable<LectureDetailsDto> Lectures { get; set; }
    public IEnumerable<LicenseDetailsDto> Licenses { get; set; }
}

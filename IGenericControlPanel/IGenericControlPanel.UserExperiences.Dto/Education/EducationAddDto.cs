﻿
using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.UserExperiences.Dto.Education;
public class EducationAddDto: EducationBasicDto
{
    public List<IFormFile> EducationFiles { get; set; }
}

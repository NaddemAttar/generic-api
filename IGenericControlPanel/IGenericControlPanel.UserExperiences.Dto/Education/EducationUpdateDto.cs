﻿using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.UserExperiences.Dto.Education;
public class EducationUpdateDto: EducationBasicDto
{
    public List<IFormFile> EducationFiles { get; set; }
    public List<int> RemoveFileIds { get; set; }
}

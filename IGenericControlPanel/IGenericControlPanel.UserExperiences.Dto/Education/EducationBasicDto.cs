﻿
namespace IGenericControlPanel.UserExperiences.Dto.Education;
public class EducationBasicDto
{
    public int Id { get; set; }
    public string StudyingField { get; set; }
    public string SchoolName { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime? EndDate { get; set; }
    public string ScientificDegree { get; set; }
    public string Grade { get; set; }
    public string Link { get; set; }
    public string Description { get; set; }
}

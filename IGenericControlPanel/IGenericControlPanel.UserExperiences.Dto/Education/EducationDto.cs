﻿
namespace IGenericControlPanel.UserExperiences.Dto.Education;
public class EducationDto: EducationBasicDto
{
    public string EducationImageUrl { get; set; }
}

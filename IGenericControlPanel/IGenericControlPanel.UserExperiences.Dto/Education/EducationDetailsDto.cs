﻿
using IGenericControlPanel.UserExperiences.Dto.Experience;

namespace IGenericControlPanel.UserExperiences.Dto.Education;
public class EducationDetailsDto: EducationBasicDto
{
    public IEnumerable<FileDataDto> EducationFiles { get; set; }
}

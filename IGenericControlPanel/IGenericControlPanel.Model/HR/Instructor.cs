﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Core;
using IGenericControlPanel.Model.Util;

namespace IGenericControlPanel.Model.HR
{

#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.Instructors)]
#elif DEBUG
    [Table(TableName.Courses, Schema = Schema.Core)]

#endif
    public class Instructor : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public int TeamMemberId { get; set; }
        public TeamMember TeamMember { get; set; }
        public int CourseId { get; set; }
        public Course Course { get; set; }
    }
}

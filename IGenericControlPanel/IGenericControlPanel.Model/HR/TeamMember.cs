﻿using IGenericControlPanel.Model.Util;
using IGenericControlPanel.Model.Content;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.HR;
using IGenericControlPanel.Model.Security;

namespace IGenericControlPanel.Model
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.TeamMembers)]
#elif DEBUG
    [Table(TableName.TeamMembers, Schema = Schema.HR)]

#endif
    public class TeamMember : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string About { get; set; }
        public string Biography { get; set; }
        [NotMapped]
        public string FullName => $"{Name} {LastName}";

        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public string Position { get; set; }
        public string CultureCode { get; set; }

        #region Navigation Properties
        public TeamMemberFile Image { get; set; }
        public ICollection<Instructor> Instructors { get; set; }
        public CPUser User { get; set; }
        public int? UserId { get; set; }

        #endregion
    }
}

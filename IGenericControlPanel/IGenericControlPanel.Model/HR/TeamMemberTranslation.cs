﻿using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Util;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.TeamMemberTranslations)]
#elif DEBUG
    [Table(TableName.TeamMemberTranslations, Schema = Schema.HR)]

#endif
    public class TeamMemberTranslation : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }

        public string Position { get; set; }

        [NotMapped]
        public string FullName => $"{Name} {LastName}";

        #region Navigation Properties

        [ForeignKey(nameof(CultureCode))]
        public string CultureCode { get; set; }
        public UserLanguage? UserLanguage { get; set; }

        public int TeamMemberId { get; set; }
        public TeamMember TeamMember { get; set; }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Communication
{
    public class SessionServices : BaseIntEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        //public ICollection<SessionType> SessionTypes { get; set; }
    }
}

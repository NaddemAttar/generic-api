﻿using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model.Communication
{
    public class SessionType : BaseIntEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        //public double Price { get; set; }
        //public int Duration { get; set; }
        #region Navigation Properties
        //public int? ServiceId { get; set; }
        //public Service Service { get; set; }
        //public int? UserId { get; set; }
        //public CPUser User { get; set; }
        //public int HealthCareProviderServiceId { get; set; }
        //public HealthCareProviderService HealthCareProviderService { get; set; }
        public ICollection<UserServiceSession> UserServiceSession { get; set; }
        #endregion
    }
}
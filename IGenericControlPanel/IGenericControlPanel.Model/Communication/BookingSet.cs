﻿using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Model.Communication
{
    public class BookingSet : BaseIntEntity
    {
        public int Id { get; set; }
        public int UserServiceSessionId { get; set; }
        public UserServiceSession UserServiceSession { get; set; } // from this i can know which enterprise he book on it.
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime Birthdate { get; set; }
        public Gender Gender { get; set; }
        public DateTime Appointment { get; set; }
        public int DurationInMinutes { get; set; }
        public bool PatientInRoom { get; set; }
        public bool HealthCareProviderInRoom { get; set; }
        public bool SessionStarted { get; set; }
        public bool SessionEnded { get; set; }
        public bool IsConfirmed { get; set; }
        public string ConfirmToken { get; set; }
        public int CPUserId { get; set; } // this for who booking on it.
        public CPUser CpUser { get; set; }
        public int UserId { get; set; } // this for who made the booking.
        public CPUser User { get; set; }
    }
}
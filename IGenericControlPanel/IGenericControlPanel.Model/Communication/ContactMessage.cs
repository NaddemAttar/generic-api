﻿using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Util;
using IGenericControlPanel.SharedKernal.Enums;


using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.Communication
{

#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.ContactMessages)]
#elif DEBUG
    [Table(TableName.ContactMessages, Schema = Schema.Communication)]
#endif
    public class ContactMessage : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        [StringLength(maximumLength: 5000)]
        [Required]
        public string Message { get; set; }
        [Required]
        [StringLength(maximumLength: 300)]
        public string FullName { get; set; }
        [Required]
        [StringLength(maximumLength: 300)]
        public string Email { get; set; }

        [StringLength(maximumLength: 2000)]
        public string Address { get; set; }
        [StringLength(maximumLength: 300)]
        public string PhoneNumber { get; set; }
        [StringLength(maximumLength: 300)]
        public string Subject { get; set; }

        public ContactMessageType ContactMessageType { get; set; }

        public bool Seen { get; set; }

    }
}

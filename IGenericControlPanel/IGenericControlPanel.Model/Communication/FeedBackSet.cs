﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Core;
using IGenericControlPanel.Model.Security;

namespace IGenericControlPanel.Model.Communication
{
    public class FeedBackSet:BaseIntEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string YouTubeLink { get; set; }
        public string ImageUrl { get; set; }
        public string ClientName { get; set; }

        #region Navigation Properties
        public int? ServiceId { get; set; }
        public Service Service { get; set; }
        public CPUser User { get; set; }
        public int? UserId { get; set; }

        #endregion
    }
}

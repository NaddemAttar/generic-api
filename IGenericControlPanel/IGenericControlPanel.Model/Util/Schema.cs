﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.Model.Util
{
    public static class Schema
    {
        /// <summary>
        /// This schema contains the User Identity of the project
        /// </summary>
        public const string Security = nameof(Security);
        /// <summary>
        /// This schema contains Products, Services, Categories
        /// </summary>
        public const string Core = nameof(Core);
        /// <summary>
        /// This schema contains Projects
        /// </summary>
        public const string Portfolio = nameof(Portfolio);
        /// <summary>
        /// This schema contains Files, Labels, Pages, Slides
        /// </summary>
        public const string Content = nameof(Content);
        /// <summary>
        /// This schema contains SupportedLangauges, SupportedCurrencies, Settings
        /// </summary>
        public const string Configuration = nameof(Configuration);
        /// <summary>
        /// This schema contains Contact, AutoReply
        /// </summary>
        public const string Communication = nameof(Communication);
        /// <summary>
        /// This schema contains TeamMembers
        /// </summary>
        public const string HR = nameof(HR);

    }
}

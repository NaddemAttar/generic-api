﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.Model.Util
{
    public static class TableName
    {
        #region Core Schema

        public const string Departments = nameof(Departments);
        public const string DepartmentTranslations = nameof(DepartmentTranslations);

        public const string Categories = nameof(Categories);
        public const string CategoryTranslations = nameof(CategoryTranslations);

        public const string DepartmentCategories = nameof(DepartmentCategories);

        public const string Products = nameof(Products);
        public const string Courses = nameof(Courses);
        public const string Locations = nameof(Locations);
        public const string CourseTypes = nameof(CourseTypes);
        public const string CourseTypeCourses = nameof(CourseTypeCourses);
        public const string ProductTranslations = nameof(ProductTranslations);
        public const string ProductCategories = nameof(ProductCategories);

        public const string Services = nameof(Services);
        public const string Specification = nameof(Specification);
        public const string ServiceTranslations = nameof(ServiceTranslations);
        public const string ServiceCategories = nameof(ServiceCategories);

        public const string Projects = nameof(Projects);
        public const string ProjectTranslations = nameof(ProjectTranslations);

        public const string Apartments = nameof(Apartments);
        public const string ApartmentTranslations = nameof(ApartmentTranslations);
        public const string Floors = nameof(Floors);
        public const string FloorTranslations = nameof(FloorTranslations);


        public const string ApartmentFeatures = nameof(ApartmentFeatures);
        public const string ApartmentFeatureTranslations = nameof(ApartmentFeatureTranslations);

        public const string Features = nameof(Features);
        public const string FeatureTranslations = nameof(FeatureTranslations);

        public const string ApartmentDirections = nameof(ApartmentDirections);
        public const string Constructions = nameof(Constructions);
        public const string ConstructionTranslations = nameof(ConstructionTranslations);

        #endregion

        #region HR

        public const string TeamMembers = nameof(TeamMembers);
        public const string Instructors = nameof(Instructors);
        public const string TeamMemberTranslations = nameof(TeamMemberTranslations);

        #endregion

        #region Content

        public const string Files = nameof(Files);

        public const string Carousels = nameof(Carousels);
        public const string CarouselTranslations = nameof(CarouselTranslations);

        public const string CarouselItems = nameof(CarouselItems);
        public const string CarouselItemTranslations = nameof(CarouselItemTranslations);

        public const string Labels = nameof(Labels);
        public const string LabelTranslations = nameof(LabelTranslations);

        public const string Directions = nameof(Directions);
        public const string DirectionTranslations = nameof(DirectionTranslations);

        public const string WebsitePages = nameof(WebsitePages);

        #endregion

        #region Configuration

        public const string Currencies = nameof(Currencies);

        public const string Settings = nameof(Settings);

        public const string UserLanguages = nameof(UserLanguages);
        public const string UserLanguageTranslations = nameof(UserLanguageTranslations);

        #endregion

        #region Communication

        public const string ContactMessages = nameof(ContactMessages);

        #endregion


    }
}

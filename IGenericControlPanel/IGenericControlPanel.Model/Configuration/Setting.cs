﻿using CraftLab.Core.Model.Interface;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.Model.Util;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.Configuration
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.Settings)]
#elif DEBUG
    [Table(TableName.Settings, Schema = Schema.Configuration)]

#endif
    public class Setting : IBaseEntity<int>
    {
        [Key]
        public int Id { get; set; }
        public static string FacebookLink { get; set; }
        public static string YoutubeLink { get; set; }
        public static string TwitterLink { get; set; }
        public static string LinkedInLink { get; set; }
        public static string GooglePlusLink { get; set; }
        public static string Email { get; set; }
        public static string PhoneNumber { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public bool IsValid { get; set; }
        public int CreatedBy { get; set; }
        public DateTimeOffset CreationDate { get; set; }
        public int LastUpdatedBy { get; set; }
        public DateTimeOffset LastUpdateDate { get; set; }
        [NotMapped]
        public bool ForceDelete { get; set; }
        public Currency Currency { get; set; }
        public int? CurrencyId { get; set; }
    }
}
﻿using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Util;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.UserLanguageTranslations)]
#elif DEBUG
    [Table(TableName.UserLanguageTranslations, Schema = Schema.Configuration)]

#endif
    public class UserLanguageTranslation : BaseIntEntity
    {
        public int Id { get; set; }
        public string LanguageName { get; set; }


        #region Navigation Properties

        [ForeignKey(nameof(CultureCode))]
        public string CultureCode { get; set; }
        public UserLanguage? UserLanguage { get; set; }

        #endregion
    }
}

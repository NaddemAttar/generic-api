﻿using CraftLab.Core.Model.Interface;

using IGenericControlPanel.Model.Util;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.UserLanguages)]
#elif DEBUG
    [Table(TableName.UserLanguages, Schema = Schema.Configuration)]

#endif
    public class UserLanguage : IBaseEntity<int>
    {
        public UserLanguage()
        {
            Translations = new List<UserLanguageTranslation>();
        }

        [Key]
        public int Id { get; set; }
        public string CultureCode { get; set; }
        public string LanguageName { get; set; }
        public string LanguageCultureCode { get; set; }
        public int Order { get; set; }
        public bool IsPrimaryLanguage { get; set; }


        public bool IsValid { get; set; }
        public int CreatedBy { get; set; }
        public DateTimeOffset CreationDate { get; set; }
        public int LastUpdatedBy { get; set; }
        public DateTimeOffset LastUpdateDate { get; set; }

        [NotMapped]
        public bool ForceDelete { get; set; }

        #region Navigation Properties

        public ICollection<UserLanguageTranslation> Translations { get; set; }
        #endregion
    }
}

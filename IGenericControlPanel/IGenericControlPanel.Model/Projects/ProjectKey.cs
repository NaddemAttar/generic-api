﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Projects
{
    public class ProjectKey : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string KeyName { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
    }
}

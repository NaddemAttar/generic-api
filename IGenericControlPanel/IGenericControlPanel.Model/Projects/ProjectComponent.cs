﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Content.File;

namespace IGenericControlPanel.Model.Projects
{
    public class ProjectComponent : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public string ProjectComponentImageUrl { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Security;

namespace IGenericControlPanel.Model.Projects
{
    public class Project : BaseIntEntity
    {
        public Project()
        {
            ProjectFiles = new List<ProjectFile>();
            ProjectKeys = new List<ProjectKey>();
            ProjectComponents = new List<ProjectComponent>();
            ProjectEditions = new List<ProjectEdition>();
        }
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string YoutubeLink { get; set; }

        #region Navigation Properties
        public ICollection<ProjectFile> ProjectFiles { get; set; }
        public ICollection<ProjectKey> ProjectKeys { get; set; }
        public ICollection<ProjectComponent> ProjectComponents { get; set; }
        public ICollection<ProjectEdition> ProjectEditions { get; set; }
        public CPUser User { get; set; }
        public int? UserId { get; set; }

        #endregion
    }
}

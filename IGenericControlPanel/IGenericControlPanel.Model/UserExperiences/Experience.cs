﻿
using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Security;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.UserExperiences;
public class Experience : BaseIntEntity
{
    public Experience()
    {
        ExperienceFiles = new List<ExperienceFile>();
    }
    [Key]
    public int Id { get; set; }
    public string Name { get; set; }
    public string Institution { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime? EndDate { get; set; }
    public string Link { get; set; }
    public string Description { get; set; }

    #region Navigation Properties
    public ICollection<ExperienceFile> ExperienceFiles { get; set; }
    [ForeignKey(nameof(Hcp))]
    public int HcpId { get; set; }
    public CPUser Hcp { get; set; }
    #endregion

}

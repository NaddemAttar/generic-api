﻿
using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Security;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.UserExperiences;
public class Certificate: BaseIntEntity
{
    public Certificate()
    {
        CertificateFiles = new List<CertificateFile>();
    }
    [Key]
    public int Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Institution { get; set; }

    #region Navigation Properties
    [ForeignKey(nameof(Hcp))]
    public int HcpId { get; set; }
    public CPUser Hcp { get; set; }
    public ICollection<CertificateFile> CertificateFiles { get; set; }
    #endregion
}

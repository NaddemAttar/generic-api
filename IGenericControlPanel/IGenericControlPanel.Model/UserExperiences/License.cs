﻿
using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Security;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.UserExperiences;
public class License : BaseIntEntity
{
    public License()
    {
        LicenseFiles = new List<LicenseFile>();
    }

    [Key]
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }

    #region Navigation Properties
    [ForeignKey(nameof(Hcp))]
    public int HcpId { get; set; }
    public CPUser Hcp { get; set; }
    public ICollection<LicenseFile> LicenseFiles { get; set; }
    #endregion
}

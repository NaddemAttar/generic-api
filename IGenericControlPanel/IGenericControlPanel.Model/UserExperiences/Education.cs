﻿
using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Security;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.UserExperiences;
public class Education: BaseIntEntity
{
    public Education()
    {
        EducationFiles = new List<EducationFile>();
    }
    [Key]
    public int Id { get; set; }
    public string StudyingField { get; set; }
    public string SchoolName { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime? EndDate { get; set; }
    public string ScientificDegree { get; set; }
    public string Grade { get; set; }
    public string Link { get; set; }
    public string Description { get; set; }

    #region Navigation Properties
    public ICollection<EducationFile> EducationFiles { get; set; }
    [ForeignKey(nameof(Hcp))]
    public int HcpId { get; set; }
    public CPUser Hcp { get; set; }
    #endregion
}

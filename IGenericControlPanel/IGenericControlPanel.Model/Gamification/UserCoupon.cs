﻿
using CraftLab.Core.Model.Entity;
using IGenericControlPanel.SharedKernal.Extension;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.Gamification;
public class UserCoupon : BaseIntEntity
{
    public UserCoupon()
    {
        Activate = false;
        Code = ExtensionMethods.GenerateUniqueCode(6);
        IsBeneficiary = false;
    }
    [Key]
    public int Id { get; set; }
    public bool Activate { get; set; }
    public bool IsBeneficiary { get; set; }
    public DateTime ExpiryDate { get; set; }
    public string Code { get; set; }
    public int Percentage { get; set; }
    public int Points { get; set; }

    #region Navigation Properties
    public Coupon Coupon { get; set; }
    [ForeignKey(nameof(Coupon))]
    public int CouponId { get; set; }
    #endregion
}

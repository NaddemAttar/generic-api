﻿
using CraftLab.Core.Model.Entity;
using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.Model.Gamification;
public class Coupon : BaseIntEntity
{
    [Key]
    public int Id { get; set; }
    public int Points { get; set; }
    public int Percentage { get; set; }
    public int CouponPeriod { get; set; }

    #region Navigation Properties
    public ICollection<UserCoupon> UserCoupons { get; set; }
    #endregion
}

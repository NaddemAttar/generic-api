﻿
using CraftLab.Core.Model.Entity;
using System.ComponentModel.DataAnnotations;
using IGenericControlPanel.SharedKernal.Enums.GamificationSystem;

namespace IGenericControlPanel.Model.Gamification;
public class Gamification : BaseIntEntity
{
    [Key]
    public int Id { get; set; }
    public string TextKey { get; set; }
    public int PointsValue { get; set; }
    public bool Enable { get; set; }
    public GamificationType GamificationType { get; set; }
    public ICollection<UsersPoints> UsersPoints { get; set; }
}

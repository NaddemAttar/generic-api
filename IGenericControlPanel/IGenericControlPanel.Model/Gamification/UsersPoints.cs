﻿
using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.Security;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.Gamification;
public class UsersPoints : BaseIntEntity
{
    [Key]
    public int Id { get; set; }
    public int Value { get; set; }

    #region Navigation Properties
    public Gamification Gamification { get; set; }
    [ForeignKey(nameof(Gamification))]
    public int? GamificationId { get; set; }
    #endregion
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Security.Permissions
{
    public class WebContentSet: BaseIntEntity
    {
        #region Properties

        public int Id { get; set; }
        public string Name { get; set; }
        #endregion
        #region Navigation Properties

        public ICollection<WebContentRoleSet> WebContentRoles { get; set; }
        #endregion
    }
}

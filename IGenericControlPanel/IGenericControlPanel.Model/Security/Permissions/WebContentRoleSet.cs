﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Security.Permissions
{
    public class WebContentRoleSet: BaseIntEntity
    {
        public int Id { get; set; }

        public bool CanDelete { get; set; }
        public bool CanAdd { get; set; }
        public bool CanEdit { get; set; }
        public bool CanView { get; set; }
        #region Navigation Properties

        public CPRole Role { get; set; }
        public int RoleId { get; set; }

        public WebContentSet Content { get; set; }
        public int ContentId { get; set; }
        #endregion
    }
}
﻿using IGenericControlPanel.Model.Util;

using Microsoft.AspNetCore.Identity;

using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.Security
{
#if (NUGET || RELEASE || DEBUG)
    [Table(nameof(CPRoleClaim))]
#elif DEBUG
    [Table(nameof(CPRoleClaim), Schema = Schema.Security)]

#endif
    public class CPRoleClaim : IdentityRoleClaim<int>
    {
    }
}

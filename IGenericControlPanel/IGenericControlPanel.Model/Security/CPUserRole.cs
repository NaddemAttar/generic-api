﻿using IGenericControlPanel.Model.Util;

using Microsoft.AspNetCore.Identity;

using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.Security
{
#if (NUGET || RELEASE || DEBUG)
    [Table(nameof(CPUserRole))]
#elif DEBUG
    [Table(nameof(CPUserRole), Schema = Schema.Security)]

#endif
    public class CPUserRole : IdentityUserRole<int>
    {
        public virtual CPUser User { get; set; }
        public virtual CPRole Role { get; set; }
    }
}

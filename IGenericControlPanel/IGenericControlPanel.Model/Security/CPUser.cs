﻿using CraftLab.Core.Model.Interface;
using IGenericControlPanel.Model.Communication;
using IGenericControlPanel.Model.Consultations;
using IGenericControlPanel.Model.Content;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.Model.Gamification;
using IGenericControlPanel.Model.Projects;
using IGenericControlPanel.Model.UserExperiences;
using IGenericControlPanel.SharedKernal.Enums.Core;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.Security;
#if (NUGET || RELEASE || DEBUG)
[Table(nameof(CPUser))]
#elif DEBUG
    [Table(nameof(CPUser), Schema = Schema.Security)]
#endif
public class CPUser : IdentityUser<int>, IBaseEntity<int>
{
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public Gender Gender { get; set; }
    public HcpType HcpType { get; set; }
    public DateTime? Birthdate { get; set; }
    public string? PersonalPhoto { get; set; }
    public bool IsValid { get; set; }
    public int CreatedBy { get; set; }
    public DateTimeOffset CreationDate { get; set; }
    public int LastUpdatedBy { get; set; }
    public DateTimeOffset LastUpdateDate { get; set; }
    [NotMapped]
    public bool ForceDelete { get; set; }
    public string RefreshToken { get; set; }
    public string ProfilePhotoUrl { get; set; }
    public DateTime RefreshTokenExpiryTime { get; set; }
    public int TotalOfPoints { get; set; }

    #region Navigation Properties
    public ICollection<HealthCareProviderService> EnterpriseServices { get; set; }
    public ICollection<HealthCareProviderService> UserServices { get; set; }
    public ICollection<HealthCareProviderSpecialty>  HealthCareProviderSpecialties { get; set; }
    public ICollection<Blog> Blogs { get; set; }
    public ICollection<BlogComment> BlogComments { get; set; }
    public ICollection<Question> Questions { get; set; }
    public ICollection<GroupMember> GroupMembers { get; set; }
    public ICollection<Course> Courses { get; set; }
    public ICollection<Product> Products { get; set; }
    public ICollection<Project> Projects { get; set; }
    public ICollection<TeamMember> TeamMembers { get; set; }
    public ICollection<Group> Groups { get; set; }
    public ICollection<FeedBackSet> FeedBacks { get; set; }
    public ICollection<TipsSet> Tips { get; set; }
    public ICollection<OpenningHouresSet> OpeningHours { get; set; }
    public ICollection<BookingSet> UserBookings { get; set; }
    public ICollection<BookingSet> HcpBookings { get; set; }
    public ICollection<LocationSchedule> LocationSchedules { get; set; }
    public ICollection<SessionType> SessionTypes { get; set; }
    public ICollection<PatientMedicine> PatientMedicines { get; set; }
    public ICollection<PatientSurgery> PatientsSurgers { get; set; }
    public ICollection<PatientsDiseases> PatientsDiseases { get; set; }
    public ICollection<MedicalTests> MedicalTests { get; set; }
    public ICollection<PatientAllergy> PatientAllegries { get; set; }
    public ICollection<UserNotes> UserNotes { get; set; }
    public ICollection<ProductReview> ProductReviews { get; set; }
    public ICollection<UserCourse> UserCourses { get; set; }
    public ICollection<JobApplication> JobApplications { get; set; }
    public ICollection<Order> Orders { get; set; }
    public ICollection<ConsultationEntity> Consultations { get; set; }
    public virtual ICollection<CPUserRole> UserRoles { get; set; }
    public ICollection<UsersPoints> UsersPoints { get; set; }
    public ICollection<Experience> Experiences { get; set; }
    public ICollection<Education> Educations { get; set; }
    public ICollection<HealthCareProviderEnterprise> Enterprises { get; set; }
    public ICollection<HealthCareProviderEnterprise> UsersEnterprise { get; set; }
    #endregion
}
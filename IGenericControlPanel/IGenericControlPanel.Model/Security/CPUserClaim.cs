﻿using IGenericControlPanel.Model.Util;

using Microsoft.AspNetCore.Identity;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IGenericControlPanel.Model.Security
{
#if (NUGET || RELEASE || DEBUG)
    [Table(nameof(CPUserClaim))]
#elif DEBUG
    [Table(nameof(CPUserClaim), Schema = Schema.Security)]

#endif
    public class CPUserClaim : IdentityUserClaim<int>
    {

    }
}

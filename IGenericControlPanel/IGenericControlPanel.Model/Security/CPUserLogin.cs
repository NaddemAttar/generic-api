﻿using IGenericControlPanel.Model.Util;

using Microsoft.AspNetCore.Identity;

using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.Security
{
#if (NUGET || RELEASE || DEBUG)
    [Table(nameof(CPUserLogin))]
#elif DEBUG
    [Table(nameof(CPUserLogin), Schema = Schema.Security)]

#endif
    public class CPUserLogin : IdentityUserLogin<int>
    {

    }
}

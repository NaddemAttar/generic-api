﻿using CraftLab.Core.Model.Interface;

using IGenericControlPanel.Model.Security.Permissions;
using IGenericControlPanel.Model.Util;

using Microsoft.AspNetCore.Identity;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.Security
{
#if (NUGET || RELEASE || DEBUG)
    [Table(nameof(CPRole))]
#elif DEBUG
    [Table(nameof(CPRole), Schema = Schema.Security)]

#endif
    public class CPRole : IdentityRole<int>, IBaseEntity<int>
    {
        public ICollection<WebContentRoleSet> WebContentRoles { get; set; }
        public virtual ICollection<CPUserRole> UserRoles { get; set; }
        public bool IsValid { get; set; }
        public bool ForceDelete { get; set; }
        public int CreatedBy { get; set; }
        public DateTimeOffset CreationDate { get; set; }
        public int LastUpdatedBy { get; set; }
        public DateTimeOffset LastUpdateDate { get; set; }
    }
}
﻿using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Util;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.Content
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.CarouselItemTranslations)]
#elif DEBUG
    [Table(TableName.CarouselItemTranslations, Schema = Schema.Content)]

#endif
    public class CarouselItemTranslation : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string TopSubTitle { get; set; }
        public string BottomSubTitle { get; set; }
        public string ButtonText { get; set; }


        #region Navigation Properties

        public int CarouselItemId { get; set; }
        [ForeignKey(nameof(CarouselItemId))]
        public CarouselItem CarouselItem { get; set; }

        [ForeignKey(nameof(CultureCode))]
        public string CultureCode { get; set; }

        public UserLanguage? UserLanguage { get; set; }

        #endregion
    }
}

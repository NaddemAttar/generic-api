﻿
using IGenericControlPanel.Model.UserExperiences;

namespace IGenericControlPanel.Model.Content.File;
public class CertificateFile: FileBase
{
    public override string SourceName => "";
    public int CertificateId { get; set; }
    public Certificate Certificate { get; set; }
}

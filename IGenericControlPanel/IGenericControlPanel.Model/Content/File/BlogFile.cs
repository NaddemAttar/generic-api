﻿using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model.Content.File
{
   public class BlogFile: FileBase
    {
        public override string SourceName => Blog.Title;
        public int BlogId { get; set; }
        public Blog Blog { get; set; }
    }
}

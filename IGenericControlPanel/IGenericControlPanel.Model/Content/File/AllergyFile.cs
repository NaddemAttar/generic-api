﻿using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model.Content.File
{
    public class AllergyFile : FileBase
    {
        public override string SourceName => Allergy.Name;
        public int AllergyId { get; set; }
        public Allergy Allergy { get; set; }
    }
}

﻿using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model.Content.File
{
    public class PatientsDiseasesFile : FileBase
    {
        public override string SourceName => PatientsDiseases.Diseases.Name + " " + PatientsDiseases.User.FirstName + " " + PatientsDiseases.User.LastName;
        public int PatientsDiseasesId { get; set; }
        public PatientsDiseases PatientsDiseases { get; set; }
    }
}

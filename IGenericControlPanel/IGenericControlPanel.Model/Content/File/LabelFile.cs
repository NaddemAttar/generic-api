﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Model.Content.File
{
    public class LabelFile : FileBase
    {
        public override string SourceName => "Images";
        public string LabelKey { get; set; }
        public Label Label { get; set; }
    }
}

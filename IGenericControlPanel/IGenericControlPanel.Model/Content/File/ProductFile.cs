﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IGenericControlPanel.Model.Content
{
    public class ProductFile : FileBase
    {
        public override string SourceName => Product.Name;

        public int ProductId { get; set; }
        [ForeignKey(nameof(ProductId))]
        public Product Product { get; set; }
    }
}

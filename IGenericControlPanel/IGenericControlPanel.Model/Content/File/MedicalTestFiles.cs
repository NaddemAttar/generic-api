﻿using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model.Content.File
{
    public class MedicalTestFiles : FileBase
    {
        public override string SourceName => MedicalTest.Name;
        public int MedicalTestId { get; set; }
        public MedicalTests MedicalTest { get; set; }
    }
}

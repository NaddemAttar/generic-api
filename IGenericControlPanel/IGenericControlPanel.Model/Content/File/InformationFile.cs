﻿using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model.Content.File
{
    public class InformationFile : FileBase
    {
        public override string SourceName => Information.Address;
        public int InformationId { get; set; }
        public Information Information { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model.Content.File
{
    public class PatientAllergyFile : FileBase
    {
        public override string SourceName => PatientAllergy.Allegry.Name+" "+PatientAllergy.User.FirstName + " "+PatientAllergy.User.LastName;
        public PatientAllergy PatientAllergy { get; set; }
        public int PatientAllergyId { get; set; }
    }
}

﻿
using IGenericControlPanel.Model.UserExperiences;

namespace IGenericControlPanel.Model.Content.File;
public class ExperienceFile: FileBase
{
    public override string SourceName => "";
    public int ExperienceId { get; set; }
    public Experience Experience { get; set; }
}

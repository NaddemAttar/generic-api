﻿using CraftLab.Core.Model.Entity;
using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Model.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IGenericControlPanel.Model
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.Files)]
#elif DEBUG
    [Table(TableName.Files, Schema = Schema.Content)]

#endif
    public abstract class FileBase : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }

        public string FileName { get; set; }
        public string Extension { get; set; }
        public string Url { get; set; }
        public long Length { get; set; }
        public string Checksum { get; set; }
        public FileContentType FileContentType { get; set; }
        public abstract string SourceName { get; }
    }
}

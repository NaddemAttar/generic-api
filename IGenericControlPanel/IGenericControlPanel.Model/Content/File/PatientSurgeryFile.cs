﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model.Content.File
{
    public class PatientSurgeryFile : FileBase
    {
        public override string SourceName => "";
        public PatientSurgery PatientSurgery { get; set; }
        public int PatientSurgeryId { get; set; }
    }
}

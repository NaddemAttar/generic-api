﻿
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.SharedKernal.Enums.File;

namespace IGenericControlPanel.Model.Content.File
{
    public class GroupFile : FileBase
    {
        public override string SourceName => Group.Name;
        public int GroupId { get; set; }
        public Group Group { get; set; }
        public ImageType? ImageType { get; set; }
    }
}

﻿using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model.Content.File
{
    public class MedicineFile : FileBase
    {
        public override string SourceName => Medicine.Name;
        public int MedicineId { get; set; }
        public Medicine Medicine { get; set; }
    }
}

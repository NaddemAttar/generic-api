﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.Model.Content
{
    public class CarouselItemFile : FileBase
    {
        public override string SourceName => CarouselItem.Title;

        public int CarouselItemId { get; set; }
        public CarouselItem CarouselItem { get; set; }
    }
}

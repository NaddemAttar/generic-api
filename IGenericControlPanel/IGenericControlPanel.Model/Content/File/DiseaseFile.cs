﻿using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model.Content.File
{
    public class DiseaseFile : FileBase
    {
        public override string SourceName => Diseases.Name;
        public int DiseasesId { get; set; }
        public Diseases Diseases { get; set; }
    }
}

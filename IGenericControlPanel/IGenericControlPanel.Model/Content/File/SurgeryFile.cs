﻿namespace IGenericControlPanel.Model.Content.File
{
    public class SurgeryFile : FileBase
    {
        public override string SourceName => Surgery.Name;
        public int SurgeryId { get; set; }
        public Surgery Surgery { get; set; }
    }
}

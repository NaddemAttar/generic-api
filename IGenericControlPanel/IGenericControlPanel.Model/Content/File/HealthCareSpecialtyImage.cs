﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model.Content.File
{
    public class HealthCareSpecialtyImage : FileBase
    {
        public override string SourceName => HealthCareSpecialty.Name;
        public int HealthCareSpecialtyId { get; set; }
        [ForeignKey(nameof(HealthCareSpecialtyId))]
        public HealthCareSpecialty HealthCareSpecialty { get; set; }
    }
}

﻿namespace IGenericControlPanel.Model.Content
{
    public class TeamMemberFile : FileBase
    {
        public override string SourceName => TeamMember.FullName;

        public int TeamMemberId { get; set; }
        public TeamMember TeamMember { get; set; }
    }
}

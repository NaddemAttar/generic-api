﻿using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model.Content
{
    public class ServiceFile : FileBase
    {
        public override string SourceName => Service.Name;

        public int ServiceId { get; set; }
        public Service Service { get; set; }
    }
}

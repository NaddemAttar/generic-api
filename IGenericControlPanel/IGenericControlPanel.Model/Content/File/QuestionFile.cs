﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model.Content.File
{
    public class QuestionFile: FileBase
    {
        public override string SourceName => Question.Title;
        public int QuestionId { get; set; }
        public Question Question { get; set; }
    }
}

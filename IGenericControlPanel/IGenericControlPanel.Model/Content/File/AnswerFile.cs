﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model.Content.File
{
    public class AnswerFile: FileBase
    {
        public override string SourceName => Answer.Id.ToString();
        public int AnswerId { get; set; }
        public Answer Answer { get; set; }
    }
}

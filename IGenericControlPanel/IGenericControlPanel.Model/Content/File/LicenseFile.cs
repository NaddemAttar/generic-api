﻿
using IGenericControlPanel.Model.UserExperiences;

namespace IGenericControlPanel.Model.Content.File;
public class LicenseFile: FileBase
{
    public override string SourceName => "";
    public int LicenseId { get; set; }
    public License License { get; set; }
}

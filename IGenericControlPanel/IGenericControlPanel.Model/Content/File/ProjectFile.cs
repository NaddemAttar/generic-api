﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Model.Projects;

namespace IGenericControlPanel.Model.Content.File
{
    public class ProjectFile : FileBase
    {
        public override string SourceName => Project.Title;
        public int ProjectId { get; set; }
        [ForeignKey(nameof(ProjectId))]
        public Project Project { get; set; }

    }
}

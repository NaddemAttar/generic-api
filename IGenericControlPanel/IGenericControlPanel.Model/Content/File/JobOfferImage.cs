﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model.Content.File
{
    public class JobOfferImage : FileBase
    {
        public override string SourceName => JobOffer.Title;
        public int JobOfferId { get; set; }
        [ForeignKey(nameof(JobOfferId))]
        public JobOffer JobOffer { get; set; }

    }
}

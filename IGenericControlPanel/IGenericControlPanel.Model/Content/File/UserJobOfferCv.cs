﻿using System.ComponentModel.DataAnnotations.Schema;

using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model.Content.File
{
    public class UserJobOfferCv : FileBase
    {
        public override string SourceName => JobApplication.FirstName;
        public int JobApplicationId { get; set; }
        [ForeignKey(nameof(JobApplicationId))]
        public JobApplication JobApplication { get; set; }
    }
}

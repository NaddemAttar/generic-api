﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.Model.Content
{
    public class CarouselFile : FileBase
    {
        public override string SourceName => Carousel.Id.ToString();

        public int CarouselId { get; set; }
        public Carousel Carousel { get; set; }
    }
}

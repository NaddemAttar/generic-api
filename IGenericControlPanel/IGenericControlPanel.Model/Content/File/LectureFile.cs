﻿
using IGenericControlPanel.Model.UserExperiences;

namespace IGenericControlPanel.Model.Content.File;
public class LectureFile: FileBase
{
    public override string SourceName => "";
    public int LectureId { get; set; }
    public Lecture Lecture { get; set; }
}

﻿
using IGenericControlPanel.Model.UserExperiences;

namespace IGenericControlPanel.Model.Content.File;
public class EducationFile: FileBase
{
    public override string SourceName => "";
    public int EducationId { get; set; }
    public Education Education { get; set; }
}

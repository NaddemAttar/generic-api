﻿using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Util;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.Content
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.CarouselItems)]
#elif DEBUG
    [Table(TableName.CarouselItems, Schema = Schema.Content)]

#endif
    public class CarouselItem : BaseIntEntity
    {
        public CarouselItem()
        {
            Translations = new List<CarouselItemTranslation>();
        }
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string TopSubTitle { get; set; }
        public string BottomSubTitle { get; set; }
        public int Order { get; set; }
        public string ButtonActionUrl { get; set; }
        public string ButtonText { get; set; }
        public string TextColor { get; set; }

        #region Navigation properties
        public CarouselItemFile Image { get; set; }
        [ForeignKey(nameof(CultureCode))]
        public string CultureCode { get; set; }
        public UserLanguage? UserLanguage { get; set; }

        public int CarouselId { get; set; }
        [ForeignKey(nameof(CarouselId))]
        public Carousel Carousel { get; set; }

        public ICollection<CarouselItemTranslation> Translations { get; set; }

        #endregion
    }
}

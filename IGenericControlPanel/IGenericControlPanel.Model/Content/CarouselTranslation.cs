﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Util;

namespace IGenericControlPanel.Model.Content
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.CarouselTranslations)]
#elif DEBUG
    [Table(TableName.CarouselTranslations, Schema = Schema.Content)]

#endif
    public class CarouselTranslation : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [ForeignKey(nameof(CultureCode))]
        public string CultureCode { get; set; }
        public UserLanguage? UserLanguage { get; set; }

        public int CarouselId { get; set; }
        [ForeignKey(nameof(CarouselId))]
        public Carousel Carousel { get; set; }
    }
}

﻿using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Util;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.Content
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.Carousels)]
#elif DEBUG
    [Table(TableName.Carousels, Schema = Schema.Content)]

#endif
    public class Carousel : BaseIntEntity
    {
        public Carousel()
        {
            CarouselItems = new List<CarouselItem>();
            Translations = new List<CarouselTranslation>();
        }
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        #region Navigation Properties

        //public int ContainedInPageId { get; set; }
        //[ForeignKey(nameof(ContainedInPageId))]
        //public WebsitePage ContainedInPage { get; set; }

        public ICollection<CarouselItem> CarouselItems { get; set; }
        public ICollection<CarouselTranslation> Translations { get; set; }
        [ForeignKey(nameof(CultureCode))]
        public string CultureCode { get; set; }
        public UserLanguage? UserLanguage { get; set; }

        #endregion



    }
}

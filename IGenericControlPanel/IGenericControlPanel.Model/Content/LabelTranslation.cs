﻿using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Util;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IGenericControlPanel.Model.Content
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.LabelTranslations)]
#elif DEBUG
    [Table(TableName.LabelTranslations, Schema = Schema.Content)]

#endif
    public class LabelTranslation : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string Text { get; set; }

        #region Navigation Properties

        public string LabelKey { get; set; }
        [ForeignKey(nameof(LabelKey))]
        public Label Label { get; set; }

        [ForeignKey(nameof(CultureCode))]
        public string CultureCode { get; set; }
        public UserLanguage? UserLanguage { get; set; }

        #endregion
    }
}

﻿using CraftLab.Core.Model.Interface;

using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Util;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IGenericControlPanel.Model.Content
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.Labels)]
#elif DEBUG
    [Table(TableName.Labels, Schema = Schema.Content)]

#endif
    public class Label : IDeletable
    {
        public Label()
        {
            Translations = new List<LabelTranslation>();
            Files = new List<LabelFile>();
        }

        [Key]
        public string Key { get; set; }
        public string Text { get; set; }

        [ForeignKey(nameof(CultureCode))]
        public string CultureCode { get; set; }
        public UserLanguage? UserLanguage { get; set; }
        public bool IsValid { get; set; }

        #region Navigation Properties

        //public int WebsitePageId { get; set; }
        //public WebsitePage WebsitePage { get; set; }

        public ICollection<LabelTranslation> Translations { get; set; }
        public ICollection<LabelFile> Files { get; set; }

        #endregion

    }
}

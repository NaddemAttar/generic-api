﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Security;

namespace IGenericControlPanel.Model.Consultations;
public class ConsultationEntity : BaseIntEntity
{
    [Key]
    public int Id { get; set; }
    public string HealthProviderAnswer { get; set; }
    public string CaseDescription { get; set; }
    #region Navigation Properties
    public ICollection<ConsultationAnswers> ConsultaionAnswers { get; set; }
    public CPUser HealthCareProvider { get; set; }
    [ForeignKey(nameof(HealthCareProvider))]
    public int HealthCareProviderId { get; set; }
    #endregion
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Consultations;
public class ConsultationAnswers : BaseIntEntity
{
    [Key]
    public int Id { get; set; }
    public string Comment { get; set; }
    public bool? Status { get; set; }
    public string MultiChoices { get; set; }

    #region Navigation Properties
    public ConsultationEntity Consultation { get; set; }
    [ForeignKey(nameof(Consultation))]
    public int ConsultationId { get; set; }
    public HealthProviderQuestions HealthProviderQuestions { get; set; }
    [ForeignKey(nameof(HealthProviderQuestions))]
    public int HealthProviderQuestionsId { get; set; }
    #endregion

}

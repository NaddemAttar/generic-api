﻿using System.ComponentModel.DataAnnotations;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.SharedKernal.Enums.Consultations;

namespace IGenericControlPanel.Model.Consultations;
public class HealthProviderQuestions : BaseIntEntity
{
    [Key]
    public int Id { get; set; }
    public ConsultationQuestionType ConsultationQuestionType { get; set; }
    public string QuestionText { get; set; }

    #region Navigation Properties
    public ICollection<ConsultationQuestionChoices> ConsultationQuestionChoices { get; set; }
    public ICollection<ConsultationAnswers> ConsultaionAnswers { get; set; }
    #endregion
}

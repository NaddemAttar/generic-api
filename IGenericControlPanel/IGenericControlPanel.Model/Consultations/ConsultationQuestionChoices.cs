﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Consultations;
public class ConsultationQuestionChoices : BaseIntEntity
{
    [Key]
    public int Id { get; set; }
    [ForeignKey(nameof(ConsultationChoices))]
    public int ConsultationChoicesId { get; set; }
    public ConsultationChoices ConsultationChoices { get; set; }
    [ForeignKey(nameof(HealthProviderQuestions))]
    public int HealthProviderQuestionsId { get; set; }
    public HealthProviderQuestions HealthProviderQuestions { get; set; }
}


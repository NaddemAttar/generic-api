﻿using System.ComponentModel.DataAnnotations;

using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Consultations;
public class ConsultationChoices : BaseIntEntity
{
    [Key]
    public int Id { get; set; }
    public string Name { get; set; }

    #region Navigation Properties
    public ICollection<ConsultationQuestionChoices> ConsultationQuestionChoices { get; set; }
    #endregion
}

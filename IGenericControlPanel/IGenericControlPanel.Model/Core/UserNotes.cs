﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Security;

namespace IGenericControlPanel.Model.Core
{
    public class UserNotes:BaseIntEntity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public CPUser User { get; set; }
        public int NoteId { get; set; }
        public Note Note { get; set; }
    }
}

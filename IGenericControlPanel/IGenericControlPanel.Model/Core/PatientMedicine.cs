﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Security;

namespace IGenericControlPanel.Model.Core
{
    public class PatientMedicine : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public Medicine Medicine { get; set; }
        public int MedicineId { get; set; }
        //[ForeignKey(nameof(User))]
        public int? UserId { get; set; }
        public CPUser User { get; set; }
        public string Description { get; set; } = string.Empty;
        public int? PatientAllergyId { get; set; }
        public PatientAllergy PatientAllergy { get; set; }
        public int? PatientsDiseasesId { get; set; }
        public PatientsDiseases PatientsDiseases { get; set; }
    }
}

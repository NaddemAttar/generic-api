﻿using CraftLab.Core.Model.Entity;
using IGenericControlPanel.SharedKernal.Enums.Core;
using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.Model.Core
{
    public class MultiLevelCategory : BaseIntEntity
    {
        public MultiLevelCategory()
        {
            Products = new List<Product>();
            MultiLevelCategoryRelatedEntities = new List<MultiLevelCategoryRelatedEntity>();
        }
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
        public OfferFor CategoryType { get; set; } // To Know This Category for what? Products, Courses or Services.

        #region Navigation proprties
        public int? ParentCategoryId { get; set; }
        public virtual MultiLevelCategory Parent { get; set; }
        public virtual HashSet<MultiLevelCategory> Children { get; set; }
        public ICollection<Product> Products { get; set; }
        public ICollection<MultiLevelCategoryRelatedEntity> MultiLevelCategoryRelatedEntities { get; set; }
        public ICollection<Group> Groups { get; set; }

        public Offer Offer { get; set; }
        public int? OfferId { get; set; }
        #endregion
    }
}

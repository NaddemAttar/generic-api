﻿using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Core;
using IGenericControlPanel.Model.Util;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.ServiceTranslations)]
#elif DEBUG
    [Table(TableName.ServiceTranslations, Schema = Schema.Core)]

#endif
    public class ServiceTranslation : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }

        #region Navigation Properties

        public int ServiceId { get; set; }
        public Service Service { get; set; }

        public string CultureCode { get; set; }
        //[ForeignKey(nameof(CultureCode))]
        public UserLanguage? UserLanguage { get; set; }

        #endregion

    }
}

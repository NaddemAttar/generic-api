﻿using IGenericControlPanel.Model.Util;
using IGenericControlPanel.Model.Content;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CraftLab.Core.Model.Entity;
using System.ComponentModel.DataAnnotations;
using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.Specification)]
#elif DEBUG
    [Table(TableName.Specification, Schema = Schema.Core)]
#endif
    public class Specification : BaseIntEntity
    {

        public Specification()
        {
            ProductSpecifications = new List<ProductSpecification>();
        }

        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<ProductSpecification> ProductSpecifications { get; set; }
    }
}

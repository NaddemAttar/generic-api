﻿using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.Security;
using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.Model.Core
{
    public class HealthCareProviderEnterprise : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int EnterpriseId { get; set; }

        #region Navigations
        public CPUser User { get; set; }
        public CPUser Enterprise { get; set; }
        public ICollection<OpenningHouresSet> OpeningHours { get; set; }
        public ICollection<HealthCareProdviderEnterpriseServices> HealthCareProdviderEnterpriseService { get; set; }
        public ICollection<HealthCareProviderService> Services { get; set; }
        #endregion
    }
}
﻿using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.Security;

namespace IGenericControlPanel.Model.Core
{
    public class OpenningHouresSet : BaseIntEntity
    {
        public int Id { get; set; }
        public TimeSpan From { get; set; }
        public TimeSpan To { get; set; }
        public DayOfWeek Day { get; set; }
        public bool IsWorkingDay { get; set; }
        public CPUser User { get; set; }
        public int? UserId { get; set; }
        public HealthCareProviderEnterprise HealthCareProviderEnterprise { get; set; }
        public int? HealthCareProviderEnterpriseId { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model
{
    public class Surgery : BaseIntEntity
    {
        public Surgery()
        {
            Files = new List<SurgeryFile>();
        }
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<PatientSurgery> PersonSurgers { get; set; }
        public ICollection<SurgeryFile> Files { get; set; }
    }
}

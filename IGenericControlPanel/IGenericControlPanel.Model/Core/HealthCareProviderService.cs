﻿using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.Security;
using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.Model.Core
{
    public class HealthCareProviderService : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public int? CPUserId { get; set; }
        public CPUser CPUser { get; set; }
        public int? EnterpriseId { get; set; }
        public CPUser Enterprise { get; set; }
        public int ServiceId { get; set; }
        public Service Service { get; set; }
        public int? OfferId { get; set; }
        public Offer Offer { get; set; }
        public ICollection<UserServiceSession> UserServiceSession { get; set; }
    }
}
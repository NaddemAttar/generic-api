﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

using CraftLab.Core.Model.Entity;


namespace IGenericControlPanel.Model.Core
{
    public class InformationTranslation : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }

        #region Navigation Properties
        public string CultureCode { get; set; }
      //  [ForeignKey(nameof(CultureCode))]
        public UserLanguage? UserLanguage { get; set; }
        public int InformationId { get; set; }
        public Information Information { get; set; }
        #endregion
    }
}

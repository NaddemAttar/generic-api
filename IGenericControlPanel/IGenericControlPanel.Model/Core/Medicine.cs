﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Content.File;

namespace IGenericControlPanel.Model.Core
{
    public class Medicine : BaseIntEntity
    {
        public Medicine()
        {

            MedicineAllergies = new List<AllergyCausingMedicin>();
            Files = new List<MedicineFile>();
        }
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<AllergyCausingMedicin> MedicineAllergies { get; set; }
        public ICollection<PatientMedicine> PatientMedicine { get; set; }
        public ICollection<MedicineFile> Files { get; set; }

    }
}

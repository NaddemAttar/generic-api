﻿using CraftLab.Core.Model.Entity;
using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.Model.Core
{
    public class ProductSizeColor : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public string ColorHex { get; set; } = string.Empty;
        public string SizeText { get; set; } = string.Empty;        
        [Required]
        public double Price { get; set; }
        [Required]
        public int Quantity { get; set; }
        public Offer Offer { get; set; }
        public int? OfferId { get; set; }
    }
}

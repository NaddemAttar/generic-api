﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Model.Core
{
    public class MedicalTests : BaseIntEntity
    {
        public MedicalTests()
        {
            Files = new List<MedicalTestFiles>();
        }
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<MedicalTestFiles> Files { get; set; }
        public int? UserId { get; set; }
        public CPUser User { get; set; }
        public MedicalTestType MedicalTestType { get; set; }
    }
}

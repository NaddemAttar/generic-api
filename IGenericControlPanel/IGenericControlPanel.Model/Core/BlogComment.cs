﻿using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Model.Core
{
    public class BlogComment: BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string CommentContent { get; set; } = string.Empty;
        public string ImageUrl { get; set; }= string.Empty;
        public int BlogId { get; set; }
        public Blog Blog { get; set; }
        public int CPUserId { get; set; }
        public CPUser CPUser { get; set; }
    }
}

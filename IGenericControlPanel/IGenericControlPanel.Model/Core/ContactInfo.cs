﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Core
{
    public class ContactInfo: BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string key { get; set; }
        public string Value { get; set; }
    }
}

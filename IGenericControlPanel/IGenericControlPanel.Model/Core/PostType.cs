﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Core
{
    public class PostType:BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string TypeName { get; set; }
        public ICollection<Blog> Blogs { get; set; }
        public ICollection<Question> Questions { get; set; }
    }
}

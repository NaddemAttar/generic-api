﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Core
{
    public class ProductSpecification : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public Product Product { get; set; }
        public int ProductId { get; set; }
        public Specification Specification { get; set; }
        public int SpecificationId { get; set; }
        public string Value { get; set; }
    }
}

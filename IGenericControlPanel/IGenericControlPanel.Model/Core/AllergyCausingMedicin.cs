﻿using System.ComponentModel.DataAnnotations;

using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Core
{
    public class AllergyCausingMedicin : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public int MedicineId { get; set; }
        public Medicine Medicine { get; set; }
        public int PatientAllergyId { get; set; }
        public PatientAllergy PatientAllergy { get; set; }
    }
}

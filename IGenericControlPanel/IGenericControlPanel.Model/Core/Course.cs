﻿using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.HR;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.Model.Util;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.Core
{

#if NUGET || RELEASE || DEBUG
    [Table(TableName.Courses)]
#elif DEBUG
    [Table(TableName.Courses, Schema = Schema.Core)]

#endif
    public class Course : BaseIntEntity
    {
        public Course()
        {
            Files = new List<CourseFile>();
            CourseTypeCourses = new List<CourseTypeCourses>();
            UserCourses = new List<UserCourse>();
            Instructors = new List<Instructor>();
        }
        [Key]
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Place { get; set; } = string.Empty;
        public double Price { get; set; }
        public string Link { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string PreRequirements { get; set; } = string.Empty;
        public string YoutubeLink { get; set; } = string.Empty;
        public string Goals { get; set; } = string.Empty;
        public string Modules { get; set; } = string.Empty;
        public bool IsHidden { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        #region Naviagtion Properties
        public int? LocationId { get; set; }
        public Location? Location { get; set; }
        public ICollection<CourseFile> Files { get; set; }
        public ICollection<CourseTypeCourses> CourseTypeCourses { get; set; }
        public ICollection<Instructor> Instructors { get; set; }
        public ICollection<UserCourse> UserCourses { get; set; }
        public CPUser User { get; set; }
        public int? UserId { get; set; }
        public Offer Offer { get; set; }
        public int? OfferId { get; set; }

        #endregion
    }

}

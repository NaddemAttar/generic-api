﻿using System.ComponentModel.DataAnnotations;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Model.Core
{
    public class JobApplication : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public JobOffer JobOffer { get; set; }
        public int JobOfferId { get; set; }
        public int? UserId { get; set; }
        public CPUser User { get; set; }
        public JobStatus JobStatus { get; set; }
        public int? CvFileId { get; set; }
        public UserJobOfferCv CvFile { get; set; }
    }
}

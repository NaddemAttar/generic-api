﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Security;
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Model.Core
{
    public class BlogVoteSet: BaseIntEntity
    {
        #region Properties
        [Key]
        public int Id { get; set; }
        public VoteType VoteType { get; set; }

        #region Navigation Properties
        public Guid PersonId { get; set; }
        public CPUser Person { get; set; }

        public Guid BlogId { get; set; }
        public Blog Blog { get; set; }

        #endregion

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Security;

namespace IGenericControlPanel.Model.Core
{
    public class Answer: BaseIntEntity
    {
        public Answer()
        {
            Files = new List<AnswerFile>();
        }
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public Question Question { get; set; }
        public string Content { get; set; }
        public ICollection<AnswerFile> Files { get; set; }
        public CPUser? User { get; set; }
    }
}

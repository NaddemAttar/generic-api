﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Model.Core
{
    public class JobOffer : BaseIntEntity
    {
        public JobOffer()
        {
            Files = new List<JobOfferImage>();
            JobApplications = new List<JobApplication>();
        }
        [Key]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Responsibilty { get; set; }
        [Required]
        public string Qualification { get; set; }
        [Required]
        public ContractType ContractType { get; set; }
        public int? CityId { get; set; }
        public CitySet City { get; set; }
        public PlaceOfWork? PlaceOfWork { get; set; }
        public SalaryType? SalaryType { get; set; }
        public double? Salary { get; set; }
        public double? MaxSalary { get; set; }
        [Required]
        public DateTime ExpirationDate { get; set; }
        public ICollection<JobOfferImage> Files { get; set; }
        public ICollection<JobApplication> JobApplications { get; set; }
    }
}

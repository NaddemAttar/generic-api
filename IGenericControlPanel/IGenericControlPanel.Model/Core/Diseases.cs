﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Model.Core
{
    public class Diseases : BaseIntEntity
    {
        public Diseases()
        {
            PatientsDiseases = new List<PatientsDiseases>();
            Files = new List<DiseaseFile>();
        }
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public DiseaseType DiseaseType { get; set; }
        public string Description { get; set; } = string.Empty;
        public ICollection<PatientsDiseases> PatientsDiseases { get; set; }
        public ICollection<DiseaseFile> Files { get; set; }
    }
}

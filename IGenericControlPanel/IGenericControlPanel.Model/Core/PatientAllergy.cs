﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Security;

namespace IGenericControlPanel.Model.Core
{
    public class PatientAllergy : BaseIntEntity
    {
        public PatientAllergy()
        {
            Files = new List<PatientAllergyFile>();
        }
        public int Id { get; set; }
        ////[ForeignKey(nameof(User))]
        public int? UserId { get; set; }
        public CPUser User { get; set; }
        public Allergy Allegry { get; set; }
        public int AllegryId { get; set; }
        public string Description { get; set; } = string.Empty;
        public ICollection<PatientMedicine> Medicines { get; set; }
        public ICollection<AllergyCausingMedicin> AllergyCausingMedicins { get; set; }
        public ICollection<PatientAllergyFile> Files { get; set; }

    }
}

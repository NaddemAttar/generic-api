﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Security;

namespace IGenericControlPanel.Model.Core
{
    public class Note : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string Body { get; set; } = string.Empty;
        public ICollection<UserNotes> UserNotes { get; set; }
    }
}

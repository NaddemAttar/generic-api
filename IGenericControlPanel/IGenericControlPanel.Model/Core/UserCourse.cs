﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Security;
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Model.Core
{
    public class UserCourse : BaseIntEntity
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public CPUser User { get; set; }
        public int CourseId { get; set; }
        public Course Course { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
    }
}

﻿using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.Security;
using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.Model.Core
{
    public class HealthCareProviderSpecialty: BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public int HealthCareSpecialtyId { get; set; }
        public HealthCareSpecialty HealthCareSpecialty { get; set; }
        public int CPUserId { get; set; }
        public CPUser CPUser { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Model.Core
{
    public class LoginModel
    {
        public string token { get; set; }
        public string refreshToken { get; set; }
        public string fullName { get; set; }
        public string email { get; set; }
    }
}

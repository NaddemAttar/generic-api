﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Util;

namespace IGenericControlPanel.Model.Core
{

#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.CourseTypeCourses)]
#elif DEBUG
    [Table(TableName.Courses, Schema = Schema.Core)]

#endif
    public class CourseTypeCourses : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public Course Course { get; set; }
        public int CourseId { get; set; }
        public CourseType CourseType { get; set; }
        public int CourseTypeId { get; set; }
    }
}

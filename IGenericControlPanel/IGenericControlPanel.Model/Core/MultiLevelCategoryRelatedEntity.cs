﻿using System.ComponentModel.DataAnnotations;

using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Core
{
    public class MultiLevelCategoryRelatedEntity : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public Blog Blog { get; set; }
        public int? BlogId { get; set; }
        public Question Question { get; set; }
        public int? QuestionId { get; set; }
        public MultiLevelCategory MultiLevelCategory { get; set; }
        public int MultiLevelCategoryId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Security;

namespace IGenericControlPanel.Model.Core
{
    public class ProductReview : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public int? UserId { get; set; }
        public CPUser User { get; set; }
        public string Description { get; set; }
        [Required]
        [Range(0, 5, ErrorMessage = "Please select number between 1 and 5")]
        public double Rating { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }

    }
}

﻿using CraftLab.Core.Model.Entity;
using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.Model.Core
{
    public class HealthCareSpecialtyService : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public int ServiceId { get; set; }
        public Service Service { get; set; }
        public int HealthCareSpecialtyId { get; set; }
        public HealthCareSpecialty HealthCareSpecialty { get; set; }
    }
}
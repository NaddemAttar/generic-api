﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Security;

namespace IGenericControlPanel.Model.Core
{
    public class GroupMember:BaseIntEntity
    {
        public int Id { get; set; }
        public Group Group { get; set; }
        public int GroupId { get; set; }
        public CPUser User { get; set; }
        public int UserId { get; set; }
    }
}

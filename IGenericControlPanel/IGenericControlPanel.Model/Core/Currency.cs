﻿using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.Configuration;
using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.Model.Core
{
    public class Currency : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        public Setting Setting { get; set; }
        public int? SettingId { get; set; }        
    }
}
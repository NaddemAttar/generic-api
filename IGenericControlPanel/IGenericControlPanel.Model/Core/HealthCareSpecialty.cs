﻿using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.Content.File;
using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.Model.Core
{
    public class HealthCareSpecialty : BaseIntEntity
    {
        public HealthCareSpecialty()
        {
            Files = new List<HealthCareSpecialtyImage>();
            HealthCareProviderSpecialties = new List<HealthCareProviderSpecialty>();
            SpecialtiesServices = new List<HealthCareSpecialtyService>();
        }
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int? ParentSpecialtyId { get; set; }
        public virtual HealthCareSpecialty Parent { get; set; }
        public virtual HashSet<HealthCareSpecialty> Children { get; set; }
        public ICollection<HealthCareSpecialtyImage> Files { get; set; }
        public ICollection<HealthCareSpecialtyService> SpecialtiesServices { get; set; }
        public ICollection<HealthCareProviderSpecialty> HealthCareProviderSpecialties { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Security;

namespace IGenericControlPanel.Model.Core
{
    public class Question : BaseIntEntity
    {
        public Question()
        {
            Files = new List<QuestionFile>();
            MultiLevelCategoryRelatedEntities = new List<MultiLevelCategoryRelatedEntity>();
        }

        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public bool ShowFirst { get; set; }
        public bool IsHidden { get; set; }
        public string Content { get; set; }
        public int? UrlNumbering { get; set; }
        public string MetaDescription { get; set; }
        public string CultureCode { get; set; }
        public bool FAQ { get; set; }        

        #region Naviagetion Properties
        public Service Service { get; set; }
        public int? ServiceId { get; set; }
        public int? PostTypeId { get; set; }
        public PostType PostType { get; set; }
        public ICollection<MultiLevelCategoryRelatedEntity> MultiLevelCategoryRelatedEntities { get; set; }
        public ICollection<QuestionFile> Files { get; set; }
        public ICollection<Answer> Answers { get; set; }
        public CPUser User { get; set; }
        public int? UserId { get; set; }
        public int? GroupId { get; set; }
        public Group Group { get; set; }
        #endregion
    }
}

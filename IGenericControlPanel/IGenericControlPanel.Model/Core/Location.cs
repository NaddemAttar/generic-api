﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Util;

namespace IGenericControlPanel.Model.Core
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.Locations)]
#elif DEBUG
    [Table(TableName.Courses, Schema = Schema.Core)]

#endif
    public class Location : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Course> Courses { get; set; }
        public ICollection<JobOffer> JobOffers { get; set; }
        //public ICollection<LocationSchedule> LocationSchedules { get; set; }

    }
}

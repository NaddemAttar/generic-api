﻿using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.Communication;
using IGenericControlPanel.Model.Content;
using IGenericControlPanel.Model.Util;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model.Core
{
#if NUGET || RELEASE || DEBUG
    [Table(TableName.Services)]
#elif DEBUG
    [Table(TableName.Services, Schema = Schema.Core)]

#endif
    public class Service : BaseIntEntity
    {
        public Service()
        {
            Translations = new List<ServiceTranslation>();
            Files = new List<ServiceFile>();
            HealthCareProvidersServices = new List<HealthCareProviderService>();
        }
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; } = 0;
        public int Order { get; set; }
        public bool ShowFirst { get; set; }
        public bool IsHidden { get; set; }

        #region Navigation Properties
        public ICollection<ServiceTranslation> Translations { get; set; }
        public ICollection<ServiceFile> Files { get; set; }
        public ICollection<TipsSet> Tips { get; set; }
        public ICollection<FeedBackSet> FeedBack { get; set; }
        public ICollection<Blog> Blogs { get; set; }
        public ICollection<Question> Questions { get; set; }
        public string CultureCode { get; set; }
        public UserLanguage UserLanguage { get; set; }
        public ICollection<HealthCareSpecialtyService> HealthCareSpecialtiesServices { get; set; }
        public ICollection<HealthCareProviderService> HealthCareProvidersServices { get; set; }
        #endregion
    }
}
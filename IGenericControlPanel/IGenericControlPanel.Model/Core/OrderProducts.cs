﻿
using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Core
{
    public class OrderProducts : BaseIntEntity
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public int ItemId { get; set; }
        public ProductSizeColor Item { get; set; }
        public int OrderId { get; set; }
        public Order Order { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Util;

namespace IGenericControlPanel.Model.Core
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.ServiceCategories)]
#elif DEBUG
    [Table(TableName.ServiceCategories, Schema = Schema.Core)]

#endif
    public class ServiceCategory : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public int ServiceId { get; set; }
        public Service Service { get; set; }


    }
}

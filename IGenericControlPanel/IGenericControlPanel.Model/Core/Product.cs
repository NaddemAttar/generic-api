﻿using CraftLab.Core.Model.Entity;
using IGenericControlPanel.Model.Content;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.Model.Util;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.Products)]
#elif DEBUG
    [Table(TableName.Products, Schema = Schema.Core)]

#endif
    public class Product : BaseIntEntity
    {
        public Product()
        {
            Translations = new List<ProductTranslation>();
            Files = new List<ProductFile>();
            ProductSpecifications = new List<ProductSpecification>();
            ProductReviews = new List<ProductReview>();
            
            ProductSizeColors = new List<ProductSizeColor>();
        }

        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public string CultureCode { get; set; }
        public bool ShowInStore { get; set; }
        #region Navigation Properties
        public ICollection<ProductReview> ProductReviews { get; set; }
        public ICollection<ProductTranslation> Translations { get; set; }
        public ICollection<ProductSpecification> ProductSpecifications { get; set; }
        public Brand Brand { get; set; }
        public int BrandId { get; set; }
        public ICollection<ProductFile> Files { get; set; }
        public MultiLevelCategory Category { get; set; }
        public int CategoryId { get; set; }
        public ICollection<OrderProducts> OrderProducts { get; set; }
        public List<ProductSizeColor> ProductSizeColors { get; set; }

        public CPUser User { get; set; }
        public int? UserId { get; set; }        

        public Offer Offer { get; set; }
        public int? OfferId { get; set; }

        #endregion
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Model.Core
{
    public class Allergy : BaseIntEntity
    {
        public Allergy()
        {
            PatientsAllegry = new List<PatientAllergy>();
            MedicineAllergies = new List<AllergyCausingMedicin>();
            Files = new List<AllergyFile>();
        }
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public AllergyType AllergyType { get; set; }
        public string Description { get; set; } = string.Empty;
        public ICollection<PatientAllergy> PatientsAllegry { get; set; }
        public ICollection<AllergyCausingMedicin> MedicineAllergies { get; set; }
        public ICollection<AllergyFile> Files { get; set; }


    }
}

﻿using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Util;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGenericControlPanel.Model
{
#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.ProductTranslations)]
#elif DEBUG
    [Table(TableName.ProductTranslations, Schema = Schema.Core)]

#endif
    public class ProductTranslation : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }


        #region Navigation Property
        public int ProductId { get; set; }

        [ForeignKey(nameof(ProductId))]
        public Product Product { get; set; }

        public string CultureCode { get; set; }

        #endregion
    }
}

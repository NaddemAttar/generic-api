﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Content.File;


namespace IGenericControlPanel.Model.Core
{
    public class Information : BaseIntEntity
    {
        public Information()
        {
            Translations = new List<InformationTranslation>();
            Files = new List<InformationFile>();
        }
        [Key]
        public int Id { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public DateTime? CurrentDate { get; set; }
        public string CultureCode { get; set; }

        #region Navigation Properties
        public ICollection<InformationTranslation> Translations { get; set; }
        public ICollection<InformationFile> Files { get; set; }
        #endregion

    }
}

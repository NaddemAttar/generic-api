﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Util;

namespace IGenericControlPanel.Model.Core
{

#if (NUGET || RELEASE || DEBUG)
    [Table(TableName.CourseTypes)]
#elif DEBUG
    [Table(TableName.Courses, Schema = Schema.Core)]

#endif
    public class CourseType : BaseIntEntity
    {
        public CourseType()
        {
            CourseTypeCourses = new List<CourseTypeCourses>();
        }
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<CourseTypeCourses> CourseTypeCourses { get; set; }
    }
}

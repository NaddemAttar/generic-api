﻿using CraftLab.Core.Model.Entity;
using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.Model.Core
{
    public class Brand : BaseIntEntity
    {
        public Brand()
        {
        }
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Product> Products { get; set; }
        public Offer Offer { get; set; }
        public int? OfferId { get; set; }
    }
}

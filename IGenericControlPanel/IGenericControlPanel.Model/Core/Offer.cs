﻿using CraftLab.Core.Model.Entity;
using IGenericControlPanel.SharedKernal.Enums.Core;
using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.Model.Core
{
    public class Offer : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public double? Value { get; set; }
        public double? Percentage { get; set; }
        public OfferType OfferType { get; set; }
        public OfferFor OfferFor { get; set; }
        public DateTime StartDate { get; set; } = DateTime.Now;
        public DateTime EndDate { get; set; }
        public ICollection<Product> Products { get; set; }
        public ICollection<ProductSizeColor> ProductsDetails { get; set; }
        public ICollection<Brand> Brands { get; set; }
        public ICollection<MultiLevelCategory> Categories { get; set; }
        public ICollection<HealthCareProviderService> HealthCareProviderService { get; set; }
        public ICollection<Course> Courses { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Core
{
    public class DoctorLocation: BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public double Latitude { get; set; }
        [Required]
        public double Longitude { get; set; }
        public string AddressInfo { get; set; } = string.Empty;
    }
}

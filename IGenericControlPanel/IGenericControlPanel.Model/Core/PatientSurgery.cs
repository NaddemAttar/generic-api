﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Security;

namespace IGenericControlPanel.Model.Core
{
    public class PatientSurgery : BaseIntEntity
    {
        public PatientSurgery()
        {
            Files = new List<PatientSurgeryFile>();
        }
        [Key]
        public int Id { get; set; }
        //[ForeignKey(nameof(User))]
        public int? UserId { get; set; }
        public CPUser User { get; set; }
        public int SurgeryId { get; set; }
        public Surgery Surgery { get; set; }
        public string Description { get; set; } = string.Empty;
        public ICollection<PatientSurgeryFile> Files { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Security;

namespace IGenericControlPanel.Model.Core
{
    public class PatientsDiseases : BaseIntEntity
    {
        public PatientsDiseases()
        {
            Medicines = new List<PatientMedicine>();
            Files = new List<PatientsDiseasesFile>();
        }
        [Key]
        public int Id { get; set; }
        //[ForeignKey(nameof(User))]
        public int? UserId { get; set; }
        public CPUser User { get; set; }
        public int DiseasesId { get; set; }
        public Diseases Diseases { get; set; }
        public string Description { get; set; } = string.Empty;
        public ICollection<PatientMedicine> Medicines { get; set; }
        public ICollection<PatientsDiseasesFile> Files { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Model.Core
{
    public class Blog : BaseIntEntity
    {
        public Blog()
        {
            Translations = new List<BlogTranslation>();
            Files = new List<BlogFile>();
            BlogVotes = new HashSet<BlogVoteSet>();
            MultiLevelCategoryRelatedEntities = new List<MultiLevelCategoryRelatedEntity>();
            BlogComments = new List<BlogComment>();
        }

        [Key]
        public int Id { get; set; }
        public string Title { get; set; } = string.Empty;
        public string Content { get; set; } = string.Empty;
        public bool ShowFirst { get; set; }
        public bool IsHidden { get; set; }
        public BlogType BlogType { get; set; }
        public int? UrlNumbering { get; set; }
        public string? MetaDescription { get; set; } = string.Empty;
        public string? YouTubeLink { get; set; } = string.Empty;
        public string? CultureCode { get; set; } = string.Empty;
        public DateTime BlogDate { get; set; } = DateTime.Now;

        #region Navigation Properties
        public int? PostTypeId { get; set; }
        public PostType PostType { get; set; }
        public ICollection<MultiLevelCategoryRelatedEntity> MultiLevelCategoryRelatedEntities { get; set; }
        public ICollection<BlogTranslation> Translations { get; set; }
        public ICollection<BlogFile> Files { get; set; }
        public ICollection<BlogVoteSet> BlogVotes { get; set; }
        public ICollection<BlogComment>  BlogComments { get; set; }
        public int? ServiceId { get; set; }
        public Service Service { get; set; }
        public CPUser User { get; set; }
        public int? UserId { get; set; }
        public int? GroupId { get; set; }
        public Group Group { get; set; }

        #endregion
    }
}

﻿using CraftLab.Core.Model.Entity;
using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.Model.Core
{
    public class HealthCareProdviderEnterpriseServices : BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public int ServiceId { get; set; }
        public Service Service { get; set; }
        public int HealthCareProviderEnterpriseId { get; set; }
        public HealthCareProviderEnterprise HealthCareProviderEnterprise { get; set; }
    }
}
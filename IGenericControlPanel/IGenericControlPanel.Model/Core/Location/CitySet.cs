﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Model.Core
{
    public class CitySet: BaseIntEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
        public CountrySet Country { get; set; }
        public ICollection<CityTranslationSet> Translations { get; set; }
        public ICollection<LocationSchedule> LocationSchedules { get; set; }
    }
}

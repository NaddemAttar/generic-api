﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Core
{
    public class CityTranslationSet: BaseIntEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Culture { get; set; }
        public CitySet City { get; set; }
        public int CityId { get; set; }
    }
}

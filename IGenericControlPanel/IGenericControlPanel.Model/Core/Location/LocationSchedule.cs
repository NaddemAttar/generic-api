﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Security;

namespace IGenericControlPanel.Model.Core
{
    public class LocationSchedule : BaseIntEntity
    {
        public int Id { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Address { get; set; }
        //public int LocationId { get; set; }
        //public Location Location { get; set; }

        #region Navigation Properties
        public int CityId { get; set; }
        public CitySet City { get; set; }
        ////[ForeignKey(nameof(User))]
        public int? UserId { get; set; }
        public CPUser User { get; set; }
        public string ContactNumber { get; set; }
        
        #endregion
    }
}

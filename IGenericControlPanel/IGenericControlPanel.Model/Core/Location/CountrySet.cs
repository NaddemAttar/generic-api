﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Core
{
    public class CountrySet: BaseIntEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<CountryTranslationSet> Translations { get; set; }
        public ICollection<CitySet> Cities { get; set; }
    }
}

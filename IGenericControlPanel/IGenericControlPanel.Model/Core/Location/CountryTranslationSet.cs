﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Core
{
    public class CountryTranslationSet:BaseIntEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Culture { get; set; }
        public CountrySet Country { get; set; }
        public int CountryId { get; set; }
    }
}

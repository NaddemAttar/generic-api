﻿using IGenericControlPanel.Model.Communication;
using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.Model.Core
{
    public class UserServiceSession
    {
        [Key]
        public int Id { get; set; }

        public HealthCareProviderService HealthCareProviderService { get; set; }
        public int HealthCareProviderServiceId { get; set; }

        public SessionType SessionType { get; set; }
        public int SessionTypeId { get; set; }

        public int Duration { get; set; }
        public double Price { get; set; }
        public ICollection<BookingSet> Bookings { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.SharedKernal.Enums.Security;

namespace IGenericControlPanel.Model.Core
{
    public class Group : BaseIntEntity
    {
        public Group()
        {
            Translations = new List<GroupTranslation>();
            Images = new List<GroupFile>();
        }
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public GroupPrivacyLevel GroupPrivacyLevel { get; set; }

        #region Navigation Properties
        public ICollection<GroupTranslation> Translations { get; set; }
        public ICollection<GroupFile> Images { get; set; }
        public CPUser User { get; set; }
        public int? UserId { get; set; }

        #endregion
        public ICollection<Question> Question { get; set; }
        public ICollection<Blog> Blogs { get; set; }
        public MultiLevelCategory MultiLevelCategory { get; set; }
        public int? MultiLevelCategoryId { get; set; }
        public ICollection<GroupMember> GroupMembers { get; set; }

    }
}

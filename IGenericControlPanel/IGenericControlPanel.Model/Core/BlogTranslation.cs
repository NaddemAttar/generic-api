﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Model.Entity;

namespace IGenericControlPanel.Model.Core
{
   public class BlogTranslation: BaseIntEntity
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        #region Navigation Properties
        public string CultureCode { get; set; }
        public UserLanguage? UserLanguage { get; set; }
        public int BlogId { get; set; }
        public Blog Blog { get; set; }
        #endregion
    }
}

﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Content.Dto.Tips;
using IGenericControlPanel.Content.IData.Interfaces;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.Model.Content;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using IGenericControlPanel.SharedKernal.Utils;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Content.Data.Repositories
{
    public class TipsRepository : BasicRepository<CPDbContext, TipsSet>, ITipsRepository
    {
        #region Properties & Constructor
        private IUserRepository UserRepository { get; }
        private IGamificationMovementsRepository _gamificationMovementRepo { get; }
        public TipsRepository(
            CPDbContext context,
            IUserRepository userRepository) : base(context)
        {
            UserRepository = userRepository;
        }
        #endregion

        #region Action Section
        public async Task<OperationResult<GenericOperationResult, TipsDto>> ActionTip(TipsFormDto formDto)
        {
            var result = new OperationResult<GenericOperationResult, TipsDto>(GenericOperationResult.Failed);
            try
            {
                var entity = await ValidEntities
                    .Where(entity => entity.Id == formDto.Id)
                    .SingleOrDefaultAsync();

                if (entity == null)
                {
                    entity = new TipsSet()
                    {
                        Id = formDto.Id,
                        Title = formDto.Title,
                        Description = formDto.Description,
                        ServiceId = formDto.ServiceId,
                    };
                    await Context.AddAsync(entity);
                }
                else
                {
                    entity.Title = formDto.Title;
                    entity.Description = formDto.Description;
                    entity.ServiceId = formDto.ServiceId;
                    entity.CreatedBy = entity.CreatedBy;
                }
                await Context.SaveChangesAsync();

                if (formDto.Id == 0)
                {
                    var savePointsResult = await _gamificationMovementRepo
                        .SavePoints(new List<string>
                        {
                            GamificationNames.AddTip
                        });

                    if (!savePointsResult.IsSuccess)
                        return result.AddError(savePointsResult.ErrorMessages);
                }

                var data = new TipsDto()
                {
                    Id = entity.Id,
                    Title = entity.Title,
                    Description = entity.Description,
                    ServiceId = entity.ServiceId,
                    ServiceName = Context.Services.Where(service => service.Id == entity.ServiceId).SingleOrDefault().Name
                };
                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Get Section
        public async Task<OperationResult<GenericOperationResult, IEnumerable<TipsDto>>> GetTips(int ServiceId, int pageSize = 6, int pageNumber = 0)
        {
            var result = new OperationResult<GenericOperationResult, IEnumerable<TipsDto>>(GenericOperationResult.Failed);

            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var data = await Context.Tips
                    .Where(entity => userInfo.CurrentUserId == 0
                    || userInfo.IsUserAdmin
                    || (userInfo.IsHealthCareProvider
                    ? userInfo.CurrentUserId == entity.CreatedBy
                    : true)
                    && ServiceId == 0 || entity.ServiceId == ServiceId)
                    .Skip(pageSize * pageNumber)
                    .Take(pageSize)
                    .Select(entity => new TipsDto()
                    {
                        Id = entity.Id,
                        Title = entity.Title,
                        Description = entity.Description,
                        ServiceId = entity.ServiceId,
                        ServiceName = entity.Service.Name
                    }).ToListAsync();
                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Remove Section
        public async Task<OperationResult<GenericOperationResult>> RemoveTip(int TipId)
        {
            var result = new OperationResult<GenericOperationResult>(GenericOperationResult.Failed);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var entity = await Context.Tips
                    .Where(entity => entity.Id == TipId
                    && (userInfo.IsUserAdmin
                    || userInfo.CurrentUserId == entity.CreatedBy))
                    .SingleOrDefaultAsync();

                if (entity is null)
                    return result.AddError(ErrorMessages.TipNotFound).UpdateResultStatus(GenericOperationResult.NotFound);

                entity.ForceDelete = true;
                await Context.SaveChangesAsync();
                Context.Remove(entity);
                await Context.SaveChangesAsync();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion
    }
}
﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Content.Dto.Content;
using IGenericControlPanel.Content.IData;
using IGenericControlPanel.Model.Content;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Enums.Content;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Content.Data.Repositories
{
    public class ContentRepository : BasicRepository<CPDbContext, PrivacyMaterials>, IContentRepository
    {
        public ContentRepository(CPDbContext Context) : base(Context)
        {

        }
        public async Task<OperationResult<GenericOperationResult, PrivacyMaterialDto>> ActionPrivacyMaterial(PrivacyMaterialDto privacyMaterialDto)
        {
            OperationResult<GenericOperationResult, PrivacyMaterialDto> result = new(GenericOperationResult.Failed);
            try
            {
                if (privacyMaterialDto.Id != 0)
                {
                    var entity = await ValidEntities
                        .Where(entity => entity.Id == privacyMaterialDto.Id && entity.Culture == privacyMaterialDto.Culture)
                        .SingleOrDefaultAsync();

                    if (entity == null)
                        return result.AddError(ErrorMessages.ContentNotFound).UpdateResultStatus(GenericOperationResult.NotFound);

                    entity.Content = privacyMaterialDto.Content;
                    return result.UpdateResultData(privacyMaterialDto).UpdateResultStatus(GenericOperationResult.Success);
                }
                else
                {
                    var entity = new PrivacyMaterials()
                    {
                        Type = privacyMaterialDto.Type,
                        Content = privacyMaterialDto.Content,
                        Culture = privacyMaterialDto.Culture
                    };
                    await Context.AddAsync(entity);
                }
                await Context.SaveChangesAsync();
                return result.UpdateResultData(privacyMaterialDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, PrivacyMaterialDto>> GetPrivacyMaterial(PrivacyMaterialsTypes Type, string cutlure)
        {
            OperationResult<GenericOperationResult, PrivacyMaterialDto> result = new(GenericOperationResult.Failed);
            try
            {
                PrivacyMaterialDto entity = await ValidEntities
                    .Where(entity => entity.Type == Type && entity.Culture == cutlure)
                    .Select(entity => new PrivacyMaterialDto()
                    {
                        Id = entity.Id,
                        Content = entity.Content,
                        Type = entity.Type,
                        Culture = entity.Culture
                    }).SingleOrDefaultAsync();
                return entity == null
                        ? result.AddError(ErrorMessages.ContentNotFound).UpdateResultStatus(GenericOperationResult.NotFound)
                        : result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(entity);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, IEnumerable<PrivacyMaterialDto>>> GetPrivacyMaterials()
        {
            OperationResult<GenericOperationResult, IEnumerable<PrivacyMaterialDto>> result = new(GenericOperationResult.Failed);
            try
            {
                var entity = await ValidEntities
                    .Select(entity => new PrivacyMaterialDto()
                    {
                        Id = entity.Id,
                        Content = entity.Content,
                        Type = entity.Type,
                        Culture = entity.Culture
                    }).ToListAsync();
                return entity == null
                    ? result.AddError(ErrorMessages.ContentNotFound).UpdateResultStatus(GenericOperationResult.NotFound)
                    : result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(entity);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
    }
}

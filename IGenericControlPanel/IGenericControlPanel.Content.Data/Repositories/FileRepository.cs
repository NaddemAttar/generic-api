﻿using System;
using System.Linq;

using CraftLab.Core.BoundedContext.Repository;
using CraftLab.Core.Infrastructure;
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Content.IData;
using IGenericControlPanel.Model;
using IGenericControlPanel.MySql.Database;

namespace IGenericControlPanel.Content.Data.Repositories
{
    public class FileRepository : CraftLabRepository<CPDbContext, FileBase>, IFileRepository
    {
        public FileRepository(CPDbContext context) : base(context)
        {

        }

        public FileDetailsDto Get(int id)
        {
            return ValidEntities
                .Where(entity => entity.Id == id)
                .Select(entity => new FileDetailsDto()
                {
                    Id = entity.Id,
                    Checksum = entity.Checksum,
                    Description = entity.Description,
                    Length = entity.Length,
                    Extension = entity.Extension,
                    FileContentType = entity.FileContentType,
                    FileName = entity.FileName,
                    IsValid = entity.IsValid,
                    Order = entity.Order,
                    Name = entity.Name,
                    Url = entity.Url
                }).SingleOrDefault();
        }

        public IPagedList<FileDto> GetAll()
        {
            throw new NotImplementedException();
        }

        public IPagedList<FileDetailsDto> GetDetailedAll()
        {
            throw new NotImplementedException();
        }

        public OperationResult<GenericOperationResult> Remove(int id)
        {
            try
            {
                FileBase entityToDelete = ValidEntities.SingleOrDefault(file => file.Id == id);
                if (entityToDelete != null)
                {
                    _ = Context.Remove(entityToDelete);
                }
                _ = Context.SaveChanges();
                return new OperationResult<GenericOperationResult>(GenericOperationResult.Success);
            }
            catch (Exception ex)
            {
                return new OperationResult<GenericOperationResult>(GenericOperationResult.Failed, exception: ex);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AutoMapper;

using CraftLab.Core.Infrastructure;
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Content.IData;
using IGenericControlPanel.Model.Content;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.HelperMethods;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;

using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Content.Data.Repositories
{
    public class CarouselItemRepository : BasicRepository<CPDbContext, CarouselItem>, ICarouselItemRepository
    {
        #region Properties

        private IUserLanguageRepository UserLanguageRepository { get; }

        #endregion

        #region Constructors

        public CarouselItemRepository(CPDbContext dbContext, IMapper mapper, IUserLanguageRepository userLanguageRepository) : base(dbContext, mapper)
        {
            UserLanguageRepository = userLanguageRepository;
        }

        #endregion


        #region Get

        #region Get

        public OperationResult<GenericOperationResult, CarouselItemDto> Get(int id, string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, CarouselItemDto> result = new(GenericOperationResult.Failed);

            if (id == 0)
            {
                _ = result.AddError("Id is zero");
                return result;
            }

            if (string.IsNullOrEmpty(cultureCode))
            {
                cultureCode ??= UserLanguageRepository.GetDefaultCulture().Name;
            }


            try
            {
                CarouselItemDto data = ValidEntities
                                .Include(entity => entity.Image)
                                .Include(entity => entity.Translations)
                                .OrderBy(entity => entity.Order)
                                .Where(entity => entity.Id == id)
                                .Select(entity => Mapper.Map<CarouselItem, CarouselItemDto>(entity))
                                .SingleOrDefault();

                return new OperationResult<GenericOperationResult, CarouselItemDto>(GenericOperationResult.Success, data);
            }
            catch (AutoMapperMappingException exception)
            {
                return new OperationResult<GenericOperationResult, CarouselItemDto>(GenericOperationResult.Failed, null, exception.InnerException);
            }
            catch (Exception exception)
            {
                return new OperationResult<GenericOperationResult, CarouselItemDto>(GenericOperationResult.Failed, null, exception);
            }
        }

        public OperationResult<GenericOperationResult, IEnumerable<CarouselItemDetailsDto>> GetCarouselItems(int id, string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, IEnumerable<CarouselItemDetailsDto>> result = new(GenericOperationResult.Failed);
            try
            {
                var data = ValidEntities
                    .Include(entity => entity.Image)
                    .Include(entity => entity.Translations)
                    .OrderBy(entity => entity.Order)
                    .Where(entity => entity.CarouselId == id)
                    .ToList()
                    .Select(entity => Mapper.Map<CarouselItem,
                    CarouselItemDetailsDto>(entity,
                    opt => opt.Items.Add(ItemNames.CultureCode,
                    HelperMethods.EnsureCulture(cultureCode,
                    UserLanguageRepository.GetDefaultCulture()))));

                if (data is null)
                    return result.AddError(ErrorMessages.CarouselItemNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public OperationResult<GenericOperationResult, CarouselItemDetailsDto> GetDetailed(int id, string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, CarouselItemDetailsDto> result = new(GenericOperationResult.Failed);

            try
            {
                var data = ValidEntities
                    .Include(entity => entity.Image)
                    .Include(entity => entity.Translations)
                    .OrderBy(entity => entity.Order)
                    .Where(entity => entity.Id == id)
                    .ToList()
                    .Select(entity => new CarouselItemDetailsDto()
                    {
                        Id = entity.Id,
                        Order = entity.Order,
                        BottomSubTitle = entity.BottomSubTitle,
                        ButtonActionUrl = entity.ButtonActionUrl,
                        ButtonText = entity.ButtonText,
                        CarouselId = entity.CarouselId,
                        TextColor = entity.TextColor,
                        Title = entity.Title,
                        TopSubTitle = entity.TopSubTitle,
                        CultureCode = cultureCode,
                        ImageUrl = entity.Image == null ? null : entity.Image.Url
                    })
                    .SingleOrDefault();

                if (data == null)
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.CarouselNotExist);

                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Get All

        public OperationResult<GenericOperationResult, IEnumerable<CarouselItemDto>> GetAll(string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, IEnumerable<CarouselItemDto>> result = new(GenericOperationResult.Failed);

            if (string.IsNullOrEmpty(cultureCode))
            {
                cultureCode ??= UserLanguageRepository.GetDefaultCulture().Name;
            }


            try
            {
                IEnumerable<CarouselItemDto> data = ValidEntities
                              .Include(entity => entity.Image)
                              .Include(entity => entity.Translations)
                              .OrderBy(entity => entity.Order)
                              .ToList()
                              .Select(entity => Mapper.Map<CarouselItem, CarouselItemDto>(entity, opt => opt.Items.Add(ItemNames.CultureCode, HelperMethods.EnsureCulture(cultureCode, UserLanguageRepository.GetDefaultCulture()))));

                result = new OperationResult<GenericOperationResult, IEnumerable<CarouselItemDto>>(GenericOperationResult.Success, data);
            }
            catch (AutoMapperMappingException exception)
            {
                result = new OperationResult<GenericOperationResult, IEnumerable<CarouselItemDto>>(GenericOperationResult.Failed, null, exception.InnerException);
            }
            catch (Exception exception)
            {
                result = new OperationResult<GenericOperationResult, IEnumerable<CarouselItemDto>>(GenericOperationResult.Failed, null, exception);
            }

            return result;
        }
        public OperationResult<GenericOperationResult, IEnumerable<CarouselItemDetailsDto>> GetAllDetailed(string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, IEnumerable<CarouselItemDetailsDto>> result = new(GenericOperationResult.Failed);

            if (string.IsNullOrEmpty(cultureCode))
            {
                cultureCode ??= UserLanguageRepository.GetDefaultCulture().Name;
            }


            try
            {
                IEnumerable<CarouselItemDetailsDto> data = ValidEntities
                              .Include(entity => entity.Image)
                              .Include(entity => entity.Translations)
                              .OrderBy(entity => entity.Order)
                              .ToList()
                              .Select(entity => Mapper.Map<CarouselItem, CarouselItemDetailsDto>(entity, opt => opt.Items.Add(ItemNames.CultureCode, HelperMethods.EnsureCulture(cultureCode, UserLanguageRepository.GetDefaultCulture()))));

                result = new OperationResult<GenericOperationResult, IEnumerable<CarouselItemDetailsDto>>(GenericOperationResult.Success, data);
            }
            catch (AutoMapperMappingException exception)
            {
                result = new OperationResult<GenericOperationResult, IEnumerable<CarouselItemDetailsDto>>(GenericOperationResult.Failed, null, exception.InnerException);
            }
            catch (Exception exception)
            {
                result = new OperationResult<GenericOperationResult, IEnumerable<CarouselItemDetailsDto>>(GenericOperationResult.Failed, null, exception);
            }

            return result;
        }



        #endregion

        #region Get All Paged
        public OperationResult<GenericOperationResult, IPagedList<CarouselItemDto>> GetAllPaged(
            int? pageNumber = 0, int? pageSize = null, string cultureCode = "ar")
        {
            var result = new OperationResult<GenericOperationResult, IPagedList<CarouselItemDto>>(GenericOperationResult.Failed);

            try
            {
                pageSize ??= 10;
                pageNumber ??= 0;

                var data = ValidEntities
                    .Skip(pageNumber.Value * pageSize.Value)
                    .Take(pageSize.Value)
                    .Include(entity => entity.Image)
                    .Include(entity => entity.Translations)
                    .OrderByDescending(entity => entity.CarouselId)
                    .ThenByDescending(entity => entity.CreationDate)
                    .ThenByDescending(entity => entity.Order)
                    .ToList()
                    .Select(entity => Mapper.Map<CarouselItem,
                    CarouselItemDto>(entity, opt => opt.Items.Add(ItemNames.CultureCode,
                    HelperMethods.EnsureCulture(cultureCode, UserLanguageRepository.GetDefaultCulture()))));

                PagedList<CarouselItemDto> paginationResult = new(data, pageNumber.Value, pageSize.Value);

                return result.UpdateResultData(paginationResult).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #endregion
        public OperationResult<GenericOperationResult, IEnumerable<CarouselDetialsDto>> GetCarousels(string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, IEnumerable<CarouselDetialsDto>> result = new(GenericOperationResult.Failed);

            try
            {
                var data = GetValidEntities<Carousel>()
                     .Include(entity => entity.CarouselItems)
                     .ThenInclude(entity => entity.Image)
                     .AsEnumerable()
                     .OrderByDescending(entity => entity.CreationDate)
                     .Select(entity => new CarouselDetialsDto()
                     {
                         Id = entity.Id,
                         IsValid = entity.IsValid,
                         Name = entity.Name,
                         CarouselItems = entity.CarouselItems
                         .Where(c => c.IsValid)
                         .AsEnumerable()
                         .Select(item => Mapper.Map<CarouselItem,
                         CarouselItemDetailsDto>(item,
                         opt => opt.Items.Add(ItemNames.CultureCode,
                         HelperMethods.EnsureCulture(cultureCode, UserLanguageRepository.GetDefaultCulture()))))
                     }).ToList();

                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public IEnumerable<string> GetPages()
        {
            return new List<string> { "hi" };
        }

        #region Remove
        public OperationResult<GenericOperationResult> Remove(int id)
        {
            var result = new OperationResult<GenericOperationResult>(GenericOperationResult.Failed);
            try
            {
                var entityToRemove = ValidEntities
                    .SingleOrDefault(carouselitem => carouselitem.Id == id);

                if (entityToRemove is null)
                    return result.AddError(ErrorMessages.CarouselItemNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                Context.Remove(entityToRemove);
                Context.SaveChanges();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        #endregion

        #region Action
        public OperationResult<GenericOperationResult, CarouselItemFormDto> Action(CarouselItemFormDto dto)
        {
            var result = new OperationResult<GenericOperationResult, CarouselItemFormDto>(GenericOperationResult.Failed);

            try
            {
                var data = (dto != null
                    && dto.Id == 0) ? Create(dto) : Update(dto);

                if (data is null)
                    return result.AddError(ErrorMessages.CarouselItemNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        #region Private methods for Action Service

        private CarouselItemFormDto Create(CarouselItemFormDto formDto)
        {
            // creation of primary service
            CarouselItem entityToAdd = new()
            {
                Title = formDto.Title,
                BottomSubTitle = formDto.BottomSubTitle,
                TopSubTitle = formDto.TopSubTitle,
                ButtonText = formDto.ButtonText,
                ButtonActionUrl = formDto.ButtonActionUrl,
                CarouselId = formDto.CarouselId,
                Order = formDto.Order,
                CultureCode = formDto.CultureCode,
                TextColor = formDto.TextColor,
            };

            if (formDto.Image != null && formDto.Image.Length > 0)
            {
                entityToAdd.Image = new CarouselItemFile()
                {
                    Checksum = formDto.Image.Checksum,
                    FileContentType = formDto.Image.FileContentType,
                    Description = formDto.Image.Description,
                    Extension = formDto.Image.Extension,
                    FileName = formDto.Image.FileName,
                    Name = formDto.Image.Name,
                    Url = formDto.Image.Url,
                    CarouselItemId = formDto.Id,
                };
            }

            Context.Add(entityToAdd);
            Context.SaveChanges();

            formDto.Id = entityToAdd.Id;

            if (formDto.Image != null)
            {
                formDto.Image.SourceId = entityToAdd.Id;
                formDto.Image.Id = entityToAdd.Image?.Id ?? 0;
            }

            return formDto;
        }

        private CarouselItemFormDto Update(CarouselItemFormDto formDto)
        {
            int entityId = formDto?.Id ??
                formDto.TranslationsFormDtos.FirstOrDefault()?.CarouselItemId ?? 0;

            CarouselItem entityToEdit = ValidEntities
                .Include(entity => entity.Image)
                .Where(entity => entity.Id == entityId)
                .SingleOrDefault();

            if (entityToEdit is null)
                return null;

            if (formDto != null)
            {
                FileDetailsDto image = formDto.Image;
                entityToEdit.Title = formDto.Title;
                entityToEdit.BottomSubTitle = formDto.BottomSubTitle;
                entityToEdit.TopSubTitle = formDto.TopSubTitle;
                entityToEdit.ButtonText = formDto.ButtonText;
                entityToEdit.ButtonActionUrl = formDto.ButtonActionUrl;
                entityToEdit.CarouselId = formDto.CarouselId;
                entityToEdit.CultureCode = formDto.CultureCode;
                entityToEdit.Order = formDto.Order;
                entityToEdit.TextColor = formDto.TextColor;
                entityToEdit.IsValid = true;

                if (image != null && !string.IsNullOrEmpty(image.FileName))
                {
                    entityToEdit.Image.Checksum = image.Checksum;
                    entityToEdit.Image.FileContentType = image.FileContentType;
                    entityToEdit.Image.Description = image.Description;
                    entityToEdit.Image.Extension = image.Extension;
                    entityToEdit.Image.FileName = image.FileName;
                    entityToEdit.Image.Name = image.Name;
                    entityToEdit.Image.Url = image.Url;
                }

                Context.Update(entityToEdit);
            }

            if (formDto.TranslationsFormDtos != null && formDto.TranslationsFormDtos.Count() > 0)
            {
                foreach (CarouselItemTranslationFormDto translation in formDto.TranslationsFormDtos)
                {

                    CarouselItemTranslation dictionaryEntity = GetValidEntities<CarouselItemTranslation>()
                        .Where(carouselDic =>
                        carouselDic.CarouselItemId == translation.CarouselItemId &&
                        carouselDic.CultureCode == translation.CultureCode).SingleOrDefault();

                    EntityState entityState = EntityState.Modified;
                    if (dictionaryEntity == null)
                    {
                        dictionaryEntity = new CarouselItemTranslation()
                        {
                            CultureCode = translation.CultureCode,
                            Title = translation.Title,
                            BottomSubTitle = translation.BottomSubTitle,
                            TopSubTitle = translation.TopSubTitle,
                            ButtonText = translation.ButtonText,
                            CarouselItemId = translation.CarouselItemId,
                        };
                        entityState = EntityState.Added;
                    }
                    else
                    {
                        dictionaryEntity.CultureCode = translation.CultureCode;
                        dictionaryEntity.Title = translation.Title;
                        dictionaryEntity.BottomSubTitle = translation.BottomSubTitle;
                        dictionaryEntity.TopSubTitle = translation.TopSubTitle;
                        dictionaryEntity.ButtonText = translation.ButtonText;
                        dictionaryEntity.CarouselItemId = translation.CarouselItemId;
                    }
                    Context.Entry(dictionaryEntity).State = entityState;
                }
            }
            Context.SaveChanges();
            return formDto;
        }

        public async Task<OperationResult<GenericOperationResult, CarouselItemDetailsDto>> ImageAction(CarouselItemDetailsDto detailsDto)
        {
            try
            {
                if (detailsDto is null)
                {
                    OperationResult<GenericOperationResult, CarouselItemDetailsDto> result = new(GenericOperationResult.Failed, detailsDto);
                    _ = result.AddError("Entity is null");
                    return result;
                }
                if (detailsDto.Image is null)
                {
                    OperationResult<GenericOperationResult, CarouselItemDetailsDto> result = new(GenericOperationResult.Failed, detailsDto);
                    _ = result.AddError("There's no image to add");
                    return result;
                }



                CarouselItemFile file = new();
                IQueryable<CarouselItemFile> allCarouselItemFiles = Context.Set<CarouselItemFile>().Include(carouselItem => carouselItem.CarouselItem).Where(y => y.CarouselItem.IsValid);
                file = allCarouselItemFiles.Where(entity => entity.CarouselItemId == detailsDto.Id).SingleOrDefault();

                file.FileName = detailsDto.Image.FileName;
                file.CarouselItemId = detailsDto.Image.SourceId;
                file.Checksum = detailsDto.Image.Checksum;
                file.IsValid = detailsDto.Image.IsValid;
                file.Url = detailsDto.Image.Url;
                file.Order = detailsDto.Image.Order;
                file.Length = (int)detailsDto.Image.Length;
                file.Description = detailsDto.Image.Description;
                file.Name = detailsDto.Image.Name;
                file.FileContentType = detailsDto.Image.FileContentType;
                file.Extension = detailsDto.Image.Extension;

                if (file.Id == 0)
                {
                    _ = Context.Add(file);
                }
                else
                {
                    _ = Context.Update(file);
                }

                _ = await Context.SaveChangesAsync();


                detailsDto.Image.Id = file.Id;

                return new OperationResult<GenericOperationResult, CarouselItemDetailsDto>(GenericOperationResult.Success, entity: detailsDto);
            }
            catch (Exception ex)
            {
                return new OperationResult<GenericOperationResult, CarouselItemDetailsDto>(GenericOperationResult.Failed, detailsDto, ex);
            }
        }

        public OperationResult<GenericOperationResult, IPagedList<CarouselItemDto>> GetSomeLatestData(int? pageNumber = 0, int? pageSize = null, string cultureCode = "ar")
        {
            pageSize ??= 10;
            pageNumber ??= 0;


            IEnumerable<CarouselItemDto> data = ValidEntities
                             .Include(entity => entity.Image)
                             .Include(entity => entity.Translations)
                             .OrderBy(entity => entity.Order)
                             .ToList()
                             .Skip(pageNumber.Value * pageSize.Value)
                             .TakeLast(pageSize.Value)
                             .Select(entity => Mapper.Map<CarouselItem, CarouselItemDto>(entity, opt => opt.Items.Add(ItemNames.CultureCode, HelperMethods.EnsureCulture(cultureCode, UserLanguageRepository.GetDefaultCulture()))));

            PagedList<CarouselItemDto> result = new(data, pageNumber.Value, pageSize.Value);

            return new OperationResult<GenericOperationResult, IPagedList<CarouselItemDto>>(GenericOperationResult.Success, result);

        }

        public OperationResult<GenericOperationResult, CarouselDetialsDto> ActionCarousel(CarouselDetialsDto carouselFormDto)
        {
            OperationResult<GenericOperationResult, CarouselDetialsDto> result = new(GenericOperationResult.Failed);

            try
            {
                var carousel = GetValidEntities<Carousel>()
                    .Where(carousel => carousel.Id == carouselFormDto.Id
                    && carouselFormDto.IsValid == true)
                    .SingleOrDefault();

                if (carousel == null)
                {
                    carousel = new Carousel
                    {
                        Name = carouselFormDto.Name,
                    };

                    Context.Add(carousel);
                }

                else
                {
                    carousel.Name = carouselFormDto.Name;
                    Context.Update(carousel);
                }

                Context.SaveChanges();
                carouselFormDto.Id = carousel.Id;

                return result.UpdateResultData(carouselFormDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public OperationResult<GenericOperationResult> RemoveCarousel(int id)
        {
            var result = new OperationResult<GenericOperationResult>(GenericOperationResult.Failed);

            try
            {
                var entityToRemove = GetValidEntities<Carousel>()
                    .SingleOrDefault(carousel => carousel.Id == id);

                if (entityToRemove is null)
                    return result.AddError(ErrorMessages.CarouselNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                Context.Remove(entityToRemove);
                Context.SaveChanges();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Content.IData;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Model.Content;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.HelperMethods;
using IGenericControlPanel.SharedKernal.Repository;

using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Content.Data.Repositories
{
    public class LabelRepository : BasicRepository<CPDbContext, Label>, ILabelRepository
    {
        #region Properties
        private IUserLanguageRepository UserLanguageRepository { get; }
        #endregion

        #region Constructors
        public LabelRepository(CPDbContext dbContext, IMapper mapper, IUserLanguageRepository userLanguageRepository) : base(dbContext, mapper)
        {
            UserLanguageRepository = userLanguageRepository;
        }
        #endregion

        #region GET
        public OperationResult<GenericOperationResult, LabelDto> Get(string key, string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, LabelDto> result = new(GenericOperationResult.Failed);
            if (key == "")
            {
                _ = result.AddError("key is empty");
                return result;
            }

            try
            {
                LabelDto data = ValidEntities
                              .Include(entity => entity.Translations)
                              .Where(entity => entity.Key == key)
                              .Select(entity => Mapper.Map<Label, LabelDto>(entity, opt => opt.Items.Add(ItemNames.CultureCode, HelperMethods.EnsureCulture(cultureCode, UserLanguageRepository.GetDefaultCulture()))))
                              .SingleOrDefault();

                result = new OperationResult<GenericOperationResult, LabelDto>(GenericOperationResult.Success, data);

            }
            catch (AutoMapperMappingException exception)
            {
                result = new OperationResult<GenericOperationResult, LabelDto>(GenericOperationResult.Failed, null, exception.InnerException);
            }
            catch (Exception exception)
            {
                result = new OperationResult<GenericOperationResult, LabelDto>(GenericOperationResult.Failed, null, exception);
            }
            return result;
        }
        #endregion

        #region GET ALL
        public OperationResult<GenericOperationResult, IEnumerable<LabelDto>> GetAll(string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, IEnumerable<LabelDto>> result = new(GenericOperationResult.Failed);

            if (string.IsNullOrEmpty(cultureCode))
            {
                cultureCode ??= UserLanguageRepository.GetDefaultCulture().Name;
            }


            try
            {
                IQueryable<LabelDto> data = ValidEntities
                              .Include(entity => entity.Translations)
                              .Select(entity => Mapper.Map<Label, LabelDto>(entity, opt => opt.Items.Add(ItemNames.CultureCode, HelperMethods.EnsureCulture(cultureCode, UserLanguageRepository.GetDefaultCulture()))));

                result = new OperationResult<GenericOperationResult, IEnumerable<LabelDto>>(GenericOperationResult.Success, data);
            }
            catch (AutoMapperMappingException exception)
            {
                result = new OperationResult<GenericOperationResult, IEnumerable<LabelDto>>(GenericOperationResult.Failed, null, exception.InnerException);
            }
            catch (Exception exception)
            {
                result = new OperationResult<GenericOperationResult, IEnumerable<LabelDto>>(GenericOperationResult.Failed, null, exception);
            }

            return result;
        }
        #endregion

    }
}

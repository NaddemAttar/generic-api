﻿using System.Collections.Generic;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.HR.Dto;
using IGenericControlPanel.HR.Dto.TeamMember;
using IGenericControlPanel.SharedKernal.Interfaces;
using IGenericControlPanel.SharedKernal.Interfaces.RepositoryNew;

namespace IGenericControlPanel.HR.IData
{
    public interface ITeamMemberRepository
    {
        Task<OperationResult<GenericOperationResult, IEnumerable<TeamMemberDto>>> GetTeamMembersAsync(
            int pageNumber = 0, int pageSize = 5);
        Task<OperationResult<GenericOperationResult, IEnumerable<InstructorDto>>> GetAllTeamMembersAsync();
        Task<OperationResult<GenericOperationResult, TeamMemberDto>> GetTeamMemberByIdAsync(int TeamMemberId);
        Task<OperationResult<GenericOperationResult, TeamMemberFormDto>> ActionTeamMemberAsync(TeamMemberFormDto formDto);
        Task<OperationResult<GenericOperationResult, string>> RemoveTeamMemberAsync(int TeamMemberId);
    }
}

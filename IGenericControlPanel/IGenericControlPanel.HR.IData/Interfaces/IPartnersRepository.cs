﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.HR.Dto.Partners;

namespace IGenericControlPanel.HR.IData.Interfaces
{
    public interface IPartnersRepository
    {
         OperationResult<GenericOperationResult, IEnumerable<PartnersDto>> GetPartners(int pageSize=6,int pageNumber=0);
         OperationResult<GenericOperationResult, PartnersDto> GetPartner(int PartnerId);
         OperationResult<GenericOperationResult, PartnersFormDto> ActionPartner(PartnersFormDto formDto);
         OperationResult<GenericOperationResult> RemovePartner(int PartnersId);
    }
}

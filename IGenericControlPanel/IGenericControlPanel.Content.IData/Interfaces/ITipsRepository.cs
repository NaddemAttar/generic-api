﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Content.Dto.Tips;

namespace IGenericControlPanel.Content.IData.Interfaces
{
    public interface ITipsRepository
    {
        Task<OperationResult<GenericOperationResult, IEnumerable<TipsDto>>> GetTips(int ServiceId,
            int pageSize = 6, int pageNumber = 0);
        Task<OperationResult<GenericOperationResult, TipsDto>> ActionTip(TipsFormDto formDto);
        Task<OperationResult<GenericOperationResult>> RemoveTip(int TipId);
    }
}

﻿using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.SharedKernal.Interfaces.Repository;

namespace IGenericControlPanel.Content.IData
{
    public interface IFileRepository : IBasicFileRepository<int, FileDto, FileDetailsDto>
    {
    }
}

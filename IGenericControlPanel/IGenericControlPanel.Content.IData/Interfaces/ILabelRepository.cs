﻿using System;
using System.Collections.Generic;
using System.Text;

using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.SharedKernal.Interfaces.RepositoryNew;

namespace IGenericControlPanel.Content.IData
{
    public interface ILabelRepository :
        IReadRepository<string, LabelDto>
    {


    }
}

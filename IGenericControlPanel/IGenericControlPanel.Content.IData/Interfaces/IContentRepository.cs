﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Content.Dto.Content;
using IGenericControlPanel.SharedKernal.Enums.Content;

namespace IGenericControlPanel.Content.IData
{
    public interface IContentRepository
    {
        Task<OperationResult<GenericOperationResult, PrivacyMaterialDto>> GetPrivacyMaterial(PrivacyMaterialsTypes Type, string cutlure);
        Task<OperationResult<GenericOperationResult, PrivacyMaterialDto>> ActionPrivacyMaterial(PrivacyMaterialDto privacyMaterialDto);
        Task<OperationResult<GenericOperationResult, IEnumerable<PrivacyMaterialDto>>> GetPrivacyMaterials();
    }
}
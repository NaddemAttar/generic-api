﻿using CraftLab.Core.OperationResults;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.SharedKernal.Interfaces;
using IGenericControlPanel.SharedKernal.Interfaces.RepositoryNew;

using System.Collections.Generic;

namespace IGenericControlPanel.Content.IData
{
    public interface ICarouselItemRepository :
        IReadRepository<int, CarouselItemDto>,
        IReadPagedRepository<int, CarouselItemDto>,
        IReadDetailedRepository<int, CarouselItemDetailsDto>,
        IRemoveRepository<int>,
        IActionRepository<CarouselItemFormDto>,
        IImageFileUploadSupportive<CarouselItemDetailsDto>
    {
        OperationResult<GenericOperationResult, IEnumerable<CarouselDetialsDto>> GetCarousels(string cultureCode = "ar");
        OperationResult<GenericOperationResult, IEnumerable<CarouselItemDetailsDto>> GetCarouselItems(int id, string cultureCode = "ar");
        OperationResult<GenericOperationResult, CarouselDetialsDto> ActionCarousel(CarouselDetialsDto carouselFormDto);
        IEnumerable<string> GetPages();
        public OperationResult<GenericOperationResult> RemoveCarousel(int id);
    }
}

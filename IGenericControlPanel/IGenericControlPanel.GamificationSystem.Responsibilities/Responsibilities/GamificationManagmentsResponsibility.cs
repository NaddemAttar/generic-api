﻿
using IGenericControlPanel.GamificationSystem.Dto.Gamification;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Extension;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.GamificationSystem.Responsibilities.Responsibilities;
public static class GamificationManagmentsResponsibility
{
    public static IQueryable<CPUser> GetUsersRes(
        this CPDbContext context, FiltersUsersDto filtersDto)
    {
        return context.Users
                 .Include(user => user.UserRoles)
                 .ThenInclude(user => user.Role)
                 .Where(user => user.IsValid &&
                 user.UserRoles.Any(
                 userRole => userRole.Role.Name !=
                 EnumsExtensionMethods.RoleToString(Role.Admin) &&
                 filtersDto.UsersHavePointsOnly ? user.TotalOfPoints > 0 : !filtersDto.UsersHavePointsOnly &&
                 string.IsNullOrEmpty(filtersDto.Query) ||
                 user.UserName.Contains(filtersDto.Query) ||
                 user.FirstName.Contains(filtersDto.Query) ||
                 user.LastName.Contains(filtersDto.Query) ||
                 user.TotalOfPoints.ToString().Contains(filtersDto.Query)));
    }
    public static IQueryable<CPUser> GetUsersWithAscendingOrderRes(
        this IQueryable<CPUser> users, bool IsAscendingOrder)
    {
        if (IsAscendingOrder)
        {
            return users.OrderBy(user => user.TotalOfPoints);
        }

        return users.OrderByDescending(user => user.TotalOfPoints);
    }

    public static IQueryable<CPUser> GetUsersWithPaginationRes(
        this IQueryable<CPUser> users, FiltersUsersDto filtersDto)
    {
        if (filtersDto.EnablePagination)
        {
            return users.GetDataWithPagination(filtersDto.PageSize, filtersDto.PageNumber);
        }

        return users;
    }

    public static async Task<IEnumerable<UserWithPointsDto>> GetUsersWithPointsDtoResAsync(
        this IQueryable<CPUser> users)
    {
        return await users.Select(user => new UserWithPointsDto
        {
            Id = user.Id,
            FirstName = user.FirstName,
            LastName = user.LastName,
            UserName = user.UserName,
            Gender = user.Gender,
            PersonalPhotoUrl = user.ProfilePhotoUrl,
            Points = user.TotalOfPoints
        }).ToListAsync();
    }
}

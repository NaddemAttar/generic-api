﻿
using IGenericControlPanel.UnitTests.Fixtures;

namespace IGenericControlPanel.IntegrationTests.Controllers;
public class LectureManagementControllerTest: IntegrationTest
{
    #region Get Test
    [Fact]
    public async Task GetLectureForUser_ShouldReturnSuccessResponse()
    {
        // Arrange  
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.GetAsync(ApiRoutes.LectureManagement.GetLecturesForUser);
        // Assert

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Theory]
    [InlineData(3)]
    public async Task GetOneLecture_ShouldReturnSuccessResponse(int id)
    {
        // Arrange 
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.GetAsync(ApiRoutes.LectureManagement.GetOneLecture
            .Replace("{id}", id.ToString()));
        // Assert

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region Remove Test
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public async Task RemoveLecture_ShouldReturnSuccessResponse(int id)
    {
        // Arrange 
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.DeleteAsync(ApiRoutes.LectureManagement.RemoveLecture
            .Replace("{id}", id.ToString()));
        // Assert

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region Action
    [Fact]
    public async Task AddLecture_ShouldReturnSuccessResponse()
    {
        // Arrange
        await AuthenticateCPUserAsync();
        string fileName = "C:\\Users\\Naddem\\Desktop\\files\\5.jpg";
        var addDto = LectureManagementFixture.GetLectureAddDto();

        var formData = new MultipartFormDataContent()
        {
            {new StringContent(addDto.Title),"Title"},
            {new StringContent(addDto.Link),"Link"},
            {new StringContent(addDto.Description),"Description"},
        };
        var fileContnet = fileName.GetFormFile($"image/{fileName.Split('.')[1]}");
        formData.Add(fileContnet, $"LectureFiles", fileName);
        // Act
        var response = await TestClient.PostAsync(
            ApiRoutes.LectureManagement.AddLecture, formData);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task UpdateLecture_ShouldReturnSuccessResponse()
    {
        // Arrange
        await AuthenticateCPUserAsync();
        string fileName = "C:\\Users\\Naddem\\Desktop\\files\\5.jpg";
        var updateDto = LectureManagementFixture.GetLectureUpdateDto();

        var formData = new MultipartFormDataContent()
        {
            {new StringContent(updateDto.Id.ToString()),"Id"},
            {new StringContent(updateDto.Title),"Title"},
            {new StringContent(updateDto.Description),"Description"},
            {new StringContent(updateDto.Link),"Link"},
        };
        var fileContnet = fileName.GetFormFile($"image/{fileName.Split('.')[1]}");
        formData.Add(fileContnet, $"LectureFiles", fileName);
        // Act
        var response = await TestClient.PutAsync(
            ApiRoutes.LectureManagement.UpdateLecture, formData);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion
}

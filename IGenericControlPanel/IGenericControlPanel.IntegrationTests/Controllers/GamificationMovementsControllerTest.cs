﻿
using IGenericControlPanel.SharedKernal.Extension;

namespace IGenericControlPanel.IntegrationTests.Controllers;
public class GamificationMovementsControllerTest : IntegrationTest
{
    [Theory]
    [InlineData(SocialMediaApps.Facebook, SectionsForPublishOnSocialMedia.Courses)]
    [InlineData(SocialMediaApps.Instagram, SectionsForPublishOnSocialMedia.Courses)]
    public async Task SaveSocialMediaPoints_ShouldReturnResponse(
        SocialMediaApps socialMediaApps,
        SectionsForPublishOnSocialMedia sectionsForPublishOnSocialMedia)
    {
        // Arrange 
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.PutAsync(ApiRoutes.GamificationMovements.SaveSocialMediaPoints
            .Replace("{socialMediaApps}", socialMediaApps.SocialMediaAppsTypeToString())
            .Replace("{sectionsForPublishOnSocialMedia}",
            sectionsForPublishOnSocialMedia.ToString()), null);

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
}

﻿
namespace IGenericControlPanel.IntegrationTests.Controllers;
public class ProjectControllerTest : IntegrationTest
{
    #region GetProjects Test
    [Theory]
    [InlineData(0)]
    [InlineData(1)]
    [InlineData(2)]
    public async Task GetProjects_ShouldReturnResponseSuccess(int pageNumber)
    {
        // Arrange
        int pageSize = 8;

        // Act
        var response = await TestClient.GetAsync(
            ApiRoutes.Project.GetProjects
            .Replace("{pageNumber}", pageNumber.ToString())
            .Replace("{pageSize}", pageSize.ToString()));

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region GetProjectDetails Test
    [Fact]
    public async Task GetProjectDetails_ShouldReturnResponseSuccess()
    {
        // Arrange
        int projectId = 1;

        // Act
        var response = await TestClient.GetAsync(
            ApiRoutes.Project.GetProjectDetails.Replace("{id}", projectId.ToString()));

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region RemoveProject Test
    [Fact]
    public async Task RemoveProject_ShouldReturnInternalServerError()
    {
        // Arrange
        await AuthenticateCPUserAsync();

        int projectId = 2;

        // Act
        var response = await TestClient.DeleteAsync(
            ApiRoutes.Project.RemoveProject.Replace("{id}", projectId.ToString())
            );

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
    }

    [Fact]
    public async Task RemoveProject_ShouldReturnOkSuccess()
    {
        // Arrange
        await AuthenticateCPUserAsync();

        int projectId = 1;

        // Act
        var response = await TestClient.DeleteAsync(
            ApiRoutes.Project.RemoveProject.Replace("{id}", projectId.ToString())
            );

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region AddProject Test
    [Fact]
    public async Task AddProject_ShouldAddProject()
    {
        // Arrange
        await AuthenticateCPUserAsync();

        var formData = ProjectFixture.ProjectFormData();

        // Act
        var response = await TestClient.PostAsync(ApiRoutes.Project.AddProject, formData);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion
}

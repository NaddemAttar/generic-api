﻿namespace IGenericControlPanel.IntegrationTests.Controllers;

public class QuestionControllerTest : IntegrationTest
{
    #region GetQuestions Test
    [Fact]
    public async Task GetQuestions_ShouldReturnSuccessResponse()
    {
        // Act 
        var response = await TestClient.GetAsync(ApiRoutes.Question.GetQuestions);

        if (response.StatusCode == HttpStatusCode.OK)
        {
            // Assert
            var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                IEnumerable<QuestionDetailsDto>>>();

            result.IsSuccess.Should().BeTrue();
        }
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region GetQuestionDetails Test
    [Fact]
    public async Task GetQuestionDetails_ShouldReturnSuccessResponse()
    {
        // Arragne
        int blogId = 2;
        // Act 
        var response = await TestClient.GetAsync(
            ApiRoutes.Question.GetQuestion.Replace("{id}", blogId.ToString()));

        if (response.StatusCode == HttpStatusCode.OK)
        {
            // Assert
            var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                QuestionDetailsDto>>();

            result.IsSuccess.Should().BeTrue();
        }

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region Action Question & Answer Test
    [Fact]
    public async Task Action_ShouldReturnSuccessResponse()
    {
        // Arragne
        await AuthenticateCPUserAsync();
        var questionFormData = QuestionFixture.GetQuestionActionData();

        // Act
        var response = await TestClient.PostAsync(ApiRoutes.Question.ActionQuestion, questionFormData);

        if (response.StatusCode == HttpStatusCode.OK)
        {
            // Assert
            var result = await response.Content
                .ReadAsAsync<OperationResult<GenericOperationResult, QuestionFormDto>>();

            result.IsSuccess.Should().BeTrue();
            result.Result.Should().NotBeNull();
        }

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    [Fact]
    public async Task ActionAnswer_ShouldReturnSuccessResponse()
    {
        // Arragne
        await AuthenticateCPUserAsync();
        var answerFormData = QuestionFixture.GetAnswerActionData();
        // Act
        var response = await TestClient.PostAsync(ApiRoutes.Question.ActionAnswer, answerFormData);

        if (response.StatusCode == HttpStatusCode.OK)
        {
            // Assert
            var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                AnswerFormViewModel>>();

            result.IsSuccess.Should().BeTrue();
            result.Result.Should().NotBeNull();
        }

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region Remove Test

    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    [InlineData(3)]
    public async Task RemoveQuestion_ShouldReturnSuccessResponse(int questionId)
    {
        // Arragne
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.DeleteAsync(
            ApiRoutes.Question.RemoveQuestion.Replace("{id}", questionId.ToString()));

        if (response.StatusCode == HttpStatusCode.OK)
        {
            // Assert
            var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult>>();

            result.IsSuccess.Should().BeTrue();
            result.Should().NotBeNull();

        }
        response.StatusCode.Should().Be(HttpStatusCode.OK);

    }

    [Theory]
    [InlineData(3)]
    [InlineData(4)]
    public async Task RemoveAnswer_ShouldReturnSuccessResponse(int answerId)
    {
        // Arragne
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.DeleteAsync(
            ApiRoutes.Question.RemoveAnswer.Replace("{id}", answerId.ToString()));

        if (response.StatusCode == HttpStatusCode.OK)
        {
            // Assert

            var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult>>();

            result.IsSuccess.Should().BeTrue();
        }

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion
}

﻿namespace IGenericControlPanel.IntegrationTests.Controllers;
public class PartnersControllerTest : IntegrationTest
{
    #region GetPartners Test
    [Fact]
    public async Task GetPartners_ShouldReturnData()
    {
        // Arrange

        // Act
        var response = await TestClient.GetAsync(ApiRoutes.Partners.GetPartners);

        // Assert

        var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
            IEnumerable<PartnersDto>>>();

        response.StatusCode.Should().Be(HttpStatusCode.OK);

        result.IsSuccess.Should().BeTrue();
        result.Result.Should().NotBeNullOrEmpty();
    }
    #endregion

    #region GetOnePartner Test
    [Theory]
    [InlineData(56)]
    [InlineData(57)]
    [InlineData(58)]
    [InlineData(59)]
    [InlineData(60)]
    [InlineData(61)]
    [InlineData(62)]
    public async Task GetOnePartner_ShouldReturnPartnerDetails(int partnerId)
    {
        // Arrange

        // Act
        var response = await TestClient
            .GetAsync($"{ApiRoutes.Partners.GetPartner}?PartnerId={partnerId}");

        // Assert

        var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
            PartnersDto>>();

        response.StatusCode.Should().Be(HttpStatusCode.OK);

        result.IsSuccess.Should().BeTrue();
        result.Result.Should().NotBeNull();
    }
    #endregion

    #region ActionPartner Test
    [Theory]
    [InlineData("C:\\Users\\Naddem\\Desktop\\files\\5.jpg")]
    //[InlineData("C:\\Users\\Naddem\\Desktop\\files\\3.jpg")]
    //[InlineData("C:\\Users\\Naddem\\Desktop\\files\\t1.jpg")]
    //[InlineData("C:\\Users\\Naddem\\Desktop\\files\\photo_2020-03-22_23-48-53.jpg")]
    //[InlineData("C:\\Users\\Naddem\\Desktop\\files\\xx.png")]
    //[InlineData("C:\\Users\\Naddem\\Desktop\\files\\أسعار-اللاب-توب-في-كارفور-الإمارات-2020.jpg")]
    //[InlineData("C:\\Users\\Naddem\\Desktop\\files\\Copy of ICON_composite case AC.018.jpeg")]
    public async Task ActionPartner_ShouldAddNewPartner(string fileName)
    {
        // Arragne
        await AuthenticateCPUserAsync();

        var formData = new MultipartFormDataContent()
        {
            { new StringContent("https://youtube.com/"), "Link" },
            { new StringContent("Test"), "Name" },
        };

        var fileContent = fileName.GetFormFile($"image/{fileName.Split('.')[1]}");

        formData.Add(fileContent, "Image", fileName);

        var response = await TestClient.PostAsync(ApiRoutes.Partners.ActionPartner, formData);

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Theory]
    [InlineData("C:\\Users\\Naddem\\Desktop\\files\\3.jpg", "55")]
    //[InlineData("C:\\Users\\Naddem\\Desktop\\files\\3.jpg", "47")]
    //[InlineData("C:\\Users\\Naddem\\Desktop\\files\\t1.jpg", "48")]
    //[InlineData("C:\\Users\\Naddem\\Desktop\\files\\photo_2020-03-22_23-48-53.jpg", "49")]
    //[InlineData("C:\\Users\\Naddem\\Desktop\\files\\xx.png", "50")]
    //[InlineData("C:\\Users\\Naddem\\Desktop\\files\\أسعار-اللاب-توب-في-كارفور-الإمارات-2020.jpg", "51")]
    //[InlineData("C:\\Users\\Naddem\\Desktop\\files\\Copy of ICON_composite case AC.018.jpeg", "52")]
    public async Task ActionPartner_ShouldUpdatePartner(string fileName, string partnerId)
    {
        // Arragne
        await AuthenticateCPUserAsync();

        var formData = new MultipartFormDataContent()
        {
            { new StringContent(partnerId), "Id" },
            { new StringContent("https://youtube.com/"), "Link" },
            { new StringContent("Update Test"), "Name" },
        };

        var fileContent = fileName.GetFormFile($"image/{fileName.Split('.')[1]}");

        formData.Add(fileContent, "Image", fileName);

        var response = await TestClient.PostAsync(ApiRoutes.Partners.ActionPartner, formData);

        response.StatusCode.Should().Be(HttpStatusCode.OK);

        var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
            PartnersFormDto>>();
        result.IsSuccess.Should().BeTrue();
        result.Result.Should().NotBeNull();
    }
    #endregion

    #region RemovePartner Test
    [Theory]
    [InlineData(56)]
    [InlineData(57)]
    [InlineData(58)]
    [InlineData(59)]
    [InlineData(60)]
    [InlineData(61)]
    [InlineData(62)]
    public async Task RemovePartner_ShouldRemovePartner(int partnerId)
    {
        await AuthenticateCPUserAsync();

        var response = await TestClient.DeleteAsync(
            ApiRoutes.Partners.RemovePartner.Replace("{partnerId}", partnerId.ToString()));

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion
}

﻿namespace IGenericControlPanel.IntegrationTests.Controllers.Basic;
public class IntegrationTest
{
    protected readonly HttpClient TestClient;

    public IntegrationTest()
    {
        var appFactory = new WebApplicationFactory<Program>();
        TestClient = appFactory.CreateDefaultClient();
    }

    protected async Task AuthenticateCPUserAsync()
    {
        TestClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
            "Bearer", await GetJwtAsync(Role.Admin));

        //TestClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
        //"Bearer", await GetJwtAsync(Role.HealthCareProvider));
    }
    protected async Task AuthenticateNormalUserAsync()
    {
        TestClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await GetJwtAsync(Role.NormalUser));
    }
    private async Task<string> GetJwtAsync(Role role)
    {
        HttpResponseMessage response;
        if (role == Role.Admin)
        {
            response = await TestClient.PostAsJsonAsync(ApiRoutes.Account.CPUserLogin, new LoginViewModel
            {
                Username = "admin",
                Password = "Aaa@123",
                RememberMe = true
            });
        }
        else if (role == Role.NormalUser)
        {
            response = await TestClient.PostAsJsonAsync(ApiRoutes.Account.NormalUserLogin, new LoginViewModel
            {
                Username = "sedra",
                Password = "Aaa@123",
                RememberMe = true
            });
        }
        else
        {
            response = await TestClient.PostAsJsonAsync(ApiRoutes.Account.CPUserLogin, new LoginViewModel
            {
                Username = "bero",
                Password = "Aaa@123",
                RememberMe = true
            });
        }
        var registrationResponse = await response.Content.ReadAsAsync<LoginModel>();
        return registrationResponse.token;
    }
}
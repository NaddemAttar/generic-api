﻿using IGenericControlPanel.Core.Dto.Category;
using IGenericControlPanel.Core.Dto.Course;
using IGenericControlPanel.Core.Dto.Offer;
using IGenericControlPanel.Core.Dto.Product;
using IGenericControlPanel.HR.Dto;
using IGenericControlPanel.Model;
using IGenericControlPanel.WebApplication.ViewModels.Product;
using RestSharp;

namespace IGenericControlPanel.IntegrationTests.Controllers
{
    public class OfferControllerTest : IntegrationTest
    {
        #region ActionOffer Test
        [Fact]
        public async Task ActionOfferAllProducts_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();

            //offer of type: All Products.
            OfferDto offerDto = new OfferDto()
            {
                Id = 0,
                Description = "plapla",
                Title = "OfferForAllProducts",
                DetailsIds = new List<int>(),
                StartDate = DateTime.Parse("3-6-2022"),
                EndDate = DateTime.Parse("3-10-2022"),
                ItemIds = new List<int>(),
                OfferFor = 0,
                OfferType = 0,
                Percentage = 3,
                Value = null
            };
            var offerJson = JsonConvert.SerializeObject(offerDto);
            var buffer = Encoding.UTF8.GetBytes(offerJson);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            // Act
            var response = await TestClient.PostAsync(ApiRoutes.Offer.Action, byteContent);//Add Offer

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert
                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult, int>>();//Read the offer after add him (return his Id).

                var x = await TestClient.GetAsync(
                $"{ApiRoutes.Offer.GetOffer.Replace("{offerId}", result.Result.ToString())}"); // test to get the offer which i add him now.

                var removeUrl = ApiRoutes.Offer.Remove.Replace("{offerId}", result.Result.ToString());
                var y = await TestClient.PostAsync(removeUrl, null);//In the end delete the offer which i added him now.

                result.IsSuccess.Should().BeTrue();
                result.Result.Should().BeGreaterThan(0);
                x.IsSuccessStatusCode.Should().BeTrue();
                y.IsSuccessStatusCode.Should().BeTrue();
            }
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task ActionOfferAllServices_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();

            //offer of type: All Services.
            OfferDto offerDto = new OfferDto()
            {
                Id = 0,
                Description = "plapla",
                Title = "OfferForAllServices",
                DetailsIds = new List<int>(),
                StartDate = DateTime.Parse("3-6-2022"),
                EndDate = DateTime.Parse("3-10-2022"),
                ItemIds = new List<int>(),
                OfferFor = OfferFor.Services,
                OfferType = 0,
                Percentage = 5,
                Value = null
            };
            var offerJson = JsonConvert.SerializeObject(offerDto);
            var buffer = Encoding.UTF8.GetBytes(offerJson);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            // Act
            var response = await TestClient.PostAsync(ApiRoutes.Offer.Action, byteContent);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert
                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult, int>>();
                var x = await TestClient.GetAsync(
                $"{ApiRoutes.Offer.GetOffer.Replace("{offerId}", result.Result.ToString())}");

                var removeUrl = ApiRoutes.Offer.Remove.Replace("{offerId}", result.Result.ToString());
                var y = await TestClient.PostAsync(removeUrl, null);

                result.IsSuccess.Should().BeTrue();
                result.Result.Should().BeGreaterThan(0);
                x.IsSuccessStatusCode.Should().BeTrue();
                y.IsSuccessStatusCode.Should().BeTrue();
            }
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task ActionOfferAllCourses_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();

            //offer of type: All Courses.
            OfferDto offerDto = new OfferDto()
            {
                Id = 0,
                Description = "plapla",
                Title = "OfferForAllCourses",
                DetailsIds = new List<int>(),
                StartDate = DateTime.Parse("3-6-2022"),
                EndDate = DateTime.Parse("3-10-2022"),
                ItemIds = new List<int>(),
                OfferFor = OfferFor.Courses,
                OfferType = 0,
                Percentage = 4,
                Value = null
            };
            var offerJson = JsonConvert.SerializeObject(offerDto);
            var buffer = Encoding.UTF8.GetBytes(offerJson);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            // Act
            var response = await TestClient.PostAsync(ApiRoutes.Offer.Action, byteContent);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert
                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult, int>>();
                var x = await TestClient.GetAsync(
                $"{ApiRoutes.Offer.GetOffer.Replace("{offerId}", result.Result.ToString())}");

                var removeUrl = ApiRoutes.Offer.Remove.Replace("{offerId}", result.Result.ToString());
                var y = await TestClient.PostAsync(removeUrl, null);

                result.IsSuccess.Should().BeTrue();
                result.Result.Should().BeGreaterThan(0);
                x.IsSuccessStatusCode.Should().BeTrue();
                y.IsSuccessStatusCode.Should().BeTrue();
            }
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Fact]
        public async Task ActionOfferBrandProduct_ShouldReturnSuccessResponse()
        {
            await AuthenticateCPUserAsync();

            //First we add new Brand.
            var brandDto = await AddBrand();
            //offer of type: Brand Product.
            OfferDto offerDto = new OfferDto()
            {
                Id = 0,
                Description = "plapla",
                Title = "OfferForBrandProduct",
                ItemIds = new List<int>()
                {
                    brandDto.Id
                },
                DetailsIds = new List<int>(),
                StartDate = DateTime.Parse("3-6-2022"),
                EndDate = DateTime.Parse("3-10-2022"),
                OfferFor = 0,
                OfferType = OfferType.Brand,
                Percentage = 3,
                Value = null
            };
            var offerJson = JsonConvert.SerializeObject(offerDto);
            var buffer = Encoding.UTF8.GetBytes(offerJson);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            // Act
            var response = await TestClient.PostAsync(ApiRoutes.Offer.Action, byteContent);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert
                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult, int>>();
                var x = await TestClient.GetAsync(
                $"{ApiRoutes.Offer.GetOffer.Replace("{offerId}", result.Result.ToString())}");

                var brandResponse = await TestClient.DeleteAsync(ApiRoutes.Brand.RemoveBrand + $"?BrandId={brandDto.Id}");

                var removeUrl = ApiRoutes.Offer.Remove.Replace("{offerId}", result.Result.ToString());
                var y = await TestClient.PostAsync(removeUrl, null);

                result.IsSuccess.Should().BeTrue();
                result.Result.Should().BeGreaterThan(0);
                x.IsSuccessStatusCode.Should().BeTrue();
                y.IsSuccessStatusCode.Should().BeTrue();
                brandResponse.IsSuccessStatusCode.Should().BeTrue();
            }
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task ActionOfferCategoryProduct_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();

            //First we add new Category.
            var categoryDto = await AddCategory();

            //offer of type: Category Product.
            OfferDto offerDto = new OfferDto()
            {
                Id = 0,
                Description = "plapla",
                Title = "OfferForCategoryProduct",
                ItemIds = new List<int>()
                {
                    categoryDto.Id
                },
                DetailsIds = new List<int>(),
                StartDate = DateTime.Parse("3-6-2022"),
                EndDate = DateTime.Parse("3-10-2022"),
                OfferFor = 0,
                OfferType = OfferType.Category,
                Percentage = 3,
                Value = null
            };
            var offerJson = JsonConvert.SerializeObject(offerDto);
            var buffer = Encoding.UTF8.GetBytes(offerJson);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            // Act
            var response = await TestClient.PostAsync(ApiRoutes.Offer.Action, byteContent);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert
                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult, int>>();
                var x = await TestClient.GetAsync(
                $"{ApiRoutes.Offer.GetOffer.Replace("{offerId}", result.Result.ToString())}");

                var categoryResponse = await TestClient.DeleteAsync(ApiRoutes.Category.Remove + $"?id={categoryDto.Id}");

                var removeUrl = ApiRoutes.Offer.Remove.Replace("{offerId}", result.Result.ToString());
                var y = await TestClient.PostAsync(removeUrl, null);

                result.IsSuccess.Should().BeTrue();
                result.Result.Should().BeGreaterThan(0);
                x.IsSuccessStatusCode.Should().BeTrue();
                y.IsSuccessStatusCode.Should().BeTrue();
                categoryResponse.IsSuccessStatusCode.Should().BeTrue();
            }
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task ActionOfferMultiSelectedProducts_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();

            //First we add 2 new Products and then one will be the offer and the other will select 2 psc from him.
            var productDto1 = await AddProduct1();
            var productDto2 = await AddProduct2();
            //offer of type: MultiSelected Products.
            OfferDto offerDto = new OfferDto()
            {
                Id = 0,
                Description = "plapla",
                Title = "OfferForMultiSelectedProduct",
                ItemIds = new List<int>()
                {
                    productDto1.Id
                },
                DetailsIds = new List<int>()
                {
                    productDto2.productSizeColors.ElementAt(0).Id
                },
                StartDate = DateTime.Parse("3-6-2022"),
                EndDate = DateTime.Parse("3-10-2022"),
                OfferFor = OfferFor.Products,
                OfferType = OfferType.Multi,
                Percentage = null,
                Value = 5000
            };
            var offerJson = JsonConvert.SerializeObject(offerDto);
            var buffer = Encoding.UTF8.GetBytes(offerJson);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            // Act
            var response = await TestClient.PostAsync(ApiRoutes.Offer.Action, byteContent);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert
                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult, int>>();
                var x = await TestClient.GetAsync(
                $"{ApiRoutes.Offer.GetOffer.Replace("{offerId}", result.Result.ToString())}");

                var productsIds = $"{productDto1}{productDto2}";
                var productsResponse = await TestClient.DeleteAsync(ApiRoutes.Product.RemoveProducts + $"?ids={productsIds}");

                var removeUrl = ApiRoutes.Offer.Remove.Replace("{offerId}", result.Result.ToString());
                var y = await TestClient.PostAsync(removeUrl, null);

                result.IsSuccess.Should().BeTrue();
                result.Result.Should().BeGreaterThan(0);
                x.IsSuccessStatusCode.Should().BeTrue();
                y.IsSuccessStatusCode.Should().BeTrue();
                productsResponse.IsSuccessStatusCode.Should().BeTrue();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        } // still not work i'll fix it.
        [Fact]
        public async Task ActionOfferMultiSelectedCourses_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();

            //First we add a TeamMember.
            var teamMemberId = await AddTeamMember();

            //Second we add 2 new Courses and then Select them in offer.
            var courseDto1 = await AddCourse1(teamMemberId);
            var courseDto2 = await AddCourse2(teamMemberId);

            //offer of type: MultiSelected Courses.
            OfferDto offerDto = new OfferDto()
            {
                Id = 0,
                Description = "plapla",
                Title = "OfferForMultiSelectedCourses",
                ItemIds = new List<int>()
                {
                    courseDto1.Id,
                    courseDto2.Id
                },
                DetailsIds = new List<int>(),
                StartDate = DateTime.Parse("3-6-2022"),
                EndDate = DateTime.Parse("3-10-2022"),
                OfferFor = OfferFor.Courses,
                OfferType = OfferType.Multi,
                Percentage = null,
                Value = 5000
            };
            var offerJson = JsonConvert.SerializeObject(offerDto);
            var buffer = Encoding.UTF8.GetBytes(offerJson);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            // Act
            var response = await TestClient.PostAsync(ApiRoutes.Offer.Action, byteContent);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert
                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult, int>>();
                var x = await TestClient.GetAsync(
                $"{ApiRoutes.Offer.GetOffer.Replace("{offerId}", result.Result.ToString())}");

                var removeUrl = ApiRoutes.Offer.Remove.Replace("{offerId}", result.Result.ToString());
                var y = await TestClient.PostAsync(removeUrl, null);

                result.IsSuccess.Should().BeTrue();
                result.Result.Should().BeGreaterThan(0);
                x.IsSuccessStatusCode.Should().BeTrue();
                y.IsSuccessStatusCode.Should().BeTrue();
            }
            var coursesIds = new List<int>
            {
                courseDto1.Id,
                courseDto2.Id
            };
            var client = new RestClient("https://api.nabdalhayat.com/api/Course/RemoveCourses");// i can't set ApiRoutes.Course.RemoveCourses it didn't see it.
            var request = new RestRequest("", Method.Delete);
            request.AddHeader("accept", "*/*");
            request.AddHeader("Content-Type", "application/json-patch+json");
            request.AddHeader("Authorization", TestClient.DefaultRequestHeaders.Authorization.ToString());
            //string content = System.Text.Json.JsonSerializer.Serialize<List<int>>(coursesIds);
            request.AddBody(coursesIds);
            //request.RequestFormat = DataFormat.Json;
            RestResponse response2 = await client.ExecuteAsync(request).ConfigureAwait(false);
            response2.IsSuccessful.Should().BeTrue();
            //var request = new HttpRequestMessage
            //{
            //    Method = HttpMethod.Delete,
            //    RequestUri = new Uri("https://api.nabdalhayat.com/api/Course/RemoveCourses"),
            //    Headers =
            //    {
            //        { "accept", "*/*" },
            //        { "Content-Type", "application/json-patch+json" },
            //        { "Authorization", $"{TestClient.DefaultRequestHeaders.Authorization}" },
            //    },
            //    Content = new StringContent($"[\n {courseDto1.Id}, \n {courseDto2.Id}]", Encoding.UTF8, MediaTypeNames.Application.Json)
            //};

            //var z = await TestClient.SendAsync(request).ConfigureAwait(false);
            //    z.IsSuccessStatusCode.Should().BeTrue();            
            //var coursesRemoveUrl = ApiRoutes.Course.RemoveAction + "/" + queryString.ToString();
            //var coursesResponse = await TestClient.DeleteAsync(coursesRemoveUrl);
            //coursesResponse.IsSuccessStatusCode.Should().BeTrue();
            var teamMemberResponse = await TestClient.DeleteAsync(ApiRoutes.TeamMember.RemoveTeamMember + $"?id={teamMemberId}");
            teamMemberResponse.IsSuccessStatusCode.Should().BeTrue();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Fact]
        public async Task ActionOfferMultiSelectedServices_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();

            //First we add new Service to make offer on it.
            var serviceId = await AddService();

            //offer of type: MultiSelected Services.
            OfferDto offerDto = new OfferDto()
            {
                Id = 0,
                Description = "plapla",
                Title = "OfferForMultiSelectedServices",
                ItemIds = new List<int>()
                {
                    serviceId
                },
                DetailsIds = new List<int>(),
                StartDate = DateTime.Parse("3-6-2022"),
                EndDate = DateTime.Parse("3-10-2022"),
                OfferFor = OfferFor.Services,
                OfferType = OfferType.Multi,
                Percentage = null,
                Value = 7000
            };
            var offerJson = JsonConvert.SerializeObject(offerDto);
            var buffer = Encoding.UTF8.GetBytes(offerJson);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            // Act
            var response = await TestClient.PostAsync(ApiRoutes.Offer.Action, byteContent);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert
                var readPostResult = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult, int>>();
                var getOfferResponse = await TestClient.GetAsync(
                $"{ApiRoutes.Offer.GetOffer.Replace("{offerId}", readPostResult.Result.ToString())}");

                var removeOfferUrl = ApiRoutes.Offer.Remove.Replace("{offerId}", readPostResult.Result.ToString());
                var removeOfferRequest = await TestClient.PostAsync(removeOfferUrl, null);

                readPostResult.IsSuccess.Should().BeTrue();
                readPostResult.Result.Should().BeGreaterThan(0);
                getOfferResponse.IsSuccessStatusCode.Should().BeTrue();
                removeOfferRequest.IsSuccessStatusCode.Should().BeTrue();
            }
            var removeServiceRequest = await TestClient.DeleteAsync(ApiRoutes.Service.RemoveServiceById.Replace("{serviceId}", serviceId.ToString()));

            var result = await removeServiceRequest.Content.ReadAsAsync<OperationResult<GenericOperationResult>>();

            removeServiceRequest.StatusCode.Should().Be(HttpStatusCode.OK);
            result.IsSuccess.Should().BeTrue();
        }
        private async Task<BrandDto> AddBrand()
        {
            BrandDto brandDto = new BrandDto()
            {
                Id = 0,
                Name = "Pants",
            };
            var brandJson = JsonConvert.SerializeObject(brandDto);
            var brandBuffer = Encoding.UTF8.GetBytes(brandJson);
            var brandByteContent = new ByteArrayContent(brandBuffer);
            brandByteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var brandResponse = await TestClient.PostAsync(ApiRoutes.Brand.ActionBrand, brandByteContent);

            brandResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            var brandResult = await brandResponse.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                BrandDto>>();
            return brandResult.Result;
        }
        private async Task<MultiLevelCategoryDto> AddCategory()
        {
            MultiLevelCategoryDto categoryDto = new MultiLevelCategoryDto()
            {
                Id = 0,
                Name = "Health",
            };

            var multiContent = new MultipartFormDataContent();
            multiContent.Add(new StringContent(categoryDto.Name.ToString()), "Name");

            var categoryResponse = await TestClient.PostAsync(ApiRoutes.Category.Action, multiContent);

            categoryResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            var categoryResult = await categoryResponse.Content.ReadAsAsync<OperationResult<GenericOperationResult, MultiLevelCategoryDto>>();
            return categoryResult.Result;
        }
        private async Task<ProductFormViewModel> AddProduct1()
        {
            ProductFormViewModel productDto = new ProductFormViewModel()
            {
                Name = "Blades",
                Description = "plaplapla",
                CultureCode = "en",
                CategoryId = 21,
                BrandId = 1,
                productSizeColors =
                {
                    new ProductSizeColorFormDto
                    {
                        ColorHex = "#ffff",
                        Price = 10000.0,
                        Quantity = 9,
                        SizeText = "S",
                    },
                    new ProductSizeColorFormDto
                    {
                        ColorHex = "#0000",
                        Price = 12000.0,
                        Quantity = 6,
                        SizeText = "M",
                    }
                },
                ProductSpecifications =
                {
                  new ProductSpecificationDto
                  {
                      SpecificationId = 1,
                      Value = "100",
                      IsValid = true
                  }
                },
                ShowInStore = true
            };
            var multiContent = new MultipartFormDataContent();
            multiContent.Add(new StringContent(productDto.Name.ToString()), "Name");
            multiContent.Add(new StringContent(productDto.Description.ToString()), "Description");
            multiContent.Add(new StringContent(productDto.CultureCode.ToString()), "CultureCode");
            multiContent.Add(new StringContent(productDto.CategoryId.ToString()), "CategoryId");
            multiContent.Add(new StringContent(productDto.ShowInStore.ToString()), "ShowInStore");            
            var i = 0;
            foreach (var item in productDto.productSizeColors)
            {
                multiContent.Add(new StringContent(item.ColorHex), "productDto.productSizeColors[" + i + "].ColorHex");
                multiContent.Add(new StringContent(item.Quantity.ToString()), "productDto.productSizeColors[" + i + "].Quantity");
                multiContent.Add(new StringContent(item.Price.ToString()), "productDto.productSizeColors[" + i + "].Price");
                multiContent.Add(new StringContent(item.SizeText), "productDto.productSizeColors[" + i + "].SizeText");
                i++;
            }
            var j = 0;
            foreach (var item in productDto.ProductSpecifications)
            {
                multiContent.Add(new StringContent(item.Value), "ProductSpecifications[" + j + "].Value");
                multiContent.Add(new StringContent(item.IsValid.ToString()), "ProductSpecifications[" + j + "].IsValid");
                multiContent.Add(new StringContent(item.SpecificationId.ToString()), "ProductSpecifications[" + j + "].SpecificationId");
                j++;
            }

            var productResponse = await TestClient.PostAsync(ApiRoutes.Product.AddProduct, multiContent);

            productResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            var productResult = await productResponse.Content.ReadAsAsync<OperationResult<GenericOperationResult, ProductFormViewModel>>();
            return productResult.Result;
        }
        private async Task<ProductFormViewModel> AddProduct2()
        {
            ProductFormViewModel productDto = new ProductFormViewModel()
            {
                Name = "شامبو",
                Description = "بلابلابلابلا",
                CultureCode = "ar",
                CategoryId = 21,
                BrandId = 1,
                productSizeColors =
                {
                    new ProductSizeColorFormDto
                    {
                        ColorHex = "#ffff",
                        Price = 20000.0,
                        Quantity = 20,
                        SizeText = "صغير",
                    },
                    new ProductSizeColorFormDto
                    {
                        ColorHex = "#0000",
                        Price = 25000.0,
                        Quantity = 15,
                        SizeText = "وسط",
                    },
                    new ProductSizeColorFormDto
                    {
                        ColorHex = "#ff00",
                        Price = 30000.0,
                        Quantity = 10,
                        SizeText = "كبير",
                    }
                },
                ProductSpecifications =
                {
                  new ProductSpecificationDto
                  {
                      SpecificationId = 1,
                      Value = "100",
                      IsValid = true
                  }
                },
                ShowInStore = true
            };
            var multiContent = new MultipartFormDataContent();
            multiContent.Add(new StringContent(productDto.Name.ToString()), "Name");
            multiContent.Add(new StringContent(productDto.Description.ToString()), "Description");
            multiContent.Add(new StringContent(productDto.CultureCode.ToString()), "CultureCode");
            multiContent.Add(new StringContent(productDto.CategoryId.ToString()), "CategoryId");
            multiContent.Add(new StringContent(productDto.ShowInStore.ToString()), "ShowInStore");
            var i = 0;
            foreach (var item in productDto.productSizeColors)
            {
                multiContent.Add(new StringContent(item.ColorHex), "productSizeColors[" + i + "].ColorHex");
                multiContent.Add(new StringContent(item.Quantity.ToString()), "productSizeColors[" + i + "].Quantity");
                multiContent.Add(new StringContent(item.Price.ToString()), "productSizeColors[" + i + "].Price");
                multiContent.Add(new StringContent(item.SizeText), "productSizeColors[" + i + "].SizeText");
                i++;
            }
            var j = 0;
            foreach (var item in productDto.ProductSpecifications)
            {
                multiContent.Add(new StringContent(item.Value), "ProductSpecifications[" + j + "].Value");
                multiContent.Add(new StringContent(item.IsValid.ToString()), "ProductSpecifications[" + j + "].IsValid");
                multiContent.Add(new StringContent(item.SpecificationId.ToString()), "ProductSpecifications[" + j + "].SpecificationId");
                j++;
            }

            var productResponse = await TestClient.PostAsync(ApiRoutes.Product.AddProduct, multiContent);

            productResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            var productResult = await productResponse.Content.ReadAsAsync<OperationResult<GenericOperationResult, ProductFormViewModel>>();
            return productResult.Result;
        }
        private async Task<CourseFormDto> AddCourse1(int instructorId)
        {
            CourseFormDto courseDto = new CourseFormDto()
            {
                Id = 0,
                Name = "TestCourse1",
                Description = "plaplpalpa",
                Price = 25000.0,
                StartDate = DateTime.MinValue,
                EndDate = DateTime.MaxValue,
                IsHidden = false,
                CourseTypeIds =
                {
                    4
                },
                InstructorIds =
                {
                    instructorId
                },
            };

            var multiContent = new MultipartFormDataContent();
            multiContent.Add(new StringContent(courseDto.Name.ToString()), "Name");
            multiContent.Add(new StringContent(courseDto.Description.ToString()), "Description");
            multiContent.Add(new StringContent(courseDto.Price.ToString()), "Price");
            multiContent.Add(new StringContent(courseDto.StartDate.ToString()), "StartDate");
            multiContent.Add(new StringContent(courseDto.EndDate.ToString()), "EndDate");
            multiContent.Add(new StringContent(courseDto.IsHidden.ToString()), "IsHidden");
            multiContent.Add(new StringContent(courseDto.InstructorIds[0].ToString()), "InstructorIds[" + 0 + "]");
            multiContent.Add(new StringContent(courseDto.CourseTypeIds[0].ToString()), "CourseTypeIds[" + 0 + "]");


            var courseResponse = await TestClient.PostAsync(ApiRoutes.Course.ActionCourse, multiContent);

            courseResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            var courseResult = await courseResponse.Content.ReadAsAsync<OperationResult<GenericOperationResult, CourseFormDto>>();
            return courseResult.Result;
        }
        private async Task<CourseFormDto> AddCourse2(int instructorId)
        {
            CourseFormDto courseDto = new CourseFormDto()
            {
                Name = "TestCourse2",
                Description = "plaplpalpa",
                Price = 30000.0,
                StartDate = DateTime.MinValue,
                EndDate = DateTime.MaxValue,
                IsHidden = false,
                CourseTypeIds =
                {
                    4
                },
                InstructorIds =
                {
                    instructorId
                },
            };

            var multiContent = new MultipartFormDataContent();
            multiContent.Add(new StringContent(courseDto.Name.ToString()), "Name");
            multiContent.Add(new StringContent(courseDto.Description.ToString()), "Description");
            multiContent.Add(new StringContent(courseDto.Price.ToString()), "Price");
            multiContent.Add(new StringContent(courseDto.StartDate.ToString()), "StartDate");
            multiContent.Add(new StringContent(courseDto.EndDate.ToString()), "EndDate");
            multiContent.Add(new StringContent(courseDto.IsHidden.ToString()), "IsHidden");
            multiContent.Add(new StringContent(courseDto.InstructorIds[0].ToString()), "InstructorIds[" + 0 + "]");
            multiContent.Add(new StringContent(courseDto.CourseTypeIds[0].ToString()), "CourseTypeIds[" + 0 + "]");

            var courseResponse = await TestClient.PostAsync(ApiRoutes.Course.ActionCourse, multiContent);

            courseResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            var courseResult = await courseResponse.Content.ReadAsAsync<OperationResult<GenericOperationResult, CourseFormDto>>();
            return courseResult.Result;
        }
        private async Task<int> AddService()
        {
            var data = new ServiceViewModel
            {
                Name = "TestService",
                Description = "plaplapla",
                //Price = 15000.0,
            };

            var multiContent = new MultipartFormDataContent()
            {
                {new StringContent(data.Name),"Name"},
                {new StringContent(data.Description),"Description"},
                //{new StringContent(data.Price.ToString()),"Price"},
            };

            var serviceResponse = await TestClient.PostAsync(ApiRoutes.Service.ServiceAction, multiContent);

            serviceResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            var serviceResult = await serviceResponse.Content.ReadAsAsync<OperationResult<GenericOperationResult, ServiceViewModel>>();
            return serviceResult.Result.Id;
        }
        private async Task<int> AddTeamMember()
        {
            var teamMemberFormDto = new TeamMemberFormDto()
            {
                Name = "TestTeamMember",
                PhoneNumber = "9999999999",
                LastName = "LastNameTest",
                CultureCode = "en",
                Description = "plaplapla",
                About = "nananana",
                Email = "test@gmail.com",
                Position = "janna",
            };
            var multiContent = new MultipartFormDataContent();
            multiContent.Add(new StringContent(teamMemberFormDto.Name.ToString()), "Name");
            multiContent.Add(new StringContent(teamMemberFormDto.Description.ToString()), "Description");
            multiContent.Add(new StringContent(teamMemberFormDto.PhoneNumber.ToString()), "PhoneNumber");
            multiContent.Add(new StringContent(teamMemberFormDto.LastName.ToString()), "LastName");
            multiContent.Add(new StringContent(teamMemberFormDto.CultureCode.ToString()), "CultureCode");
            multiContent.Add(new StringContent(teamMemberFormDto.About.ToString()), "About");
            multiContent.Add(new StringContent(teamMemberFormDto.Email.ToString()), "Email");
            multiContent.Add(new StringContent(teamMemberFormDto.Position.ToString()), "Position");


            var teamMemberResponse = await TestClient.PostAsync(ApiRoutes.TeamMember.ActionTeamMember, multiContent);

            teamMemberResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            var teamMemberResult = await teamMemberResponse.Content.ReadAsAsync<OperationResult<GenericOperationResult, TeamMemberFormDto>>();
            return teamMemberResult.Result.Id;
        }
        #endregion
        #region GetOfferDetails Test
        [Theory]
        [InlineData(33)]
        //[InlineData(3)]
        //[InlineData(4)]
        //[InlineData(5)]
        //[InlineData(6)]
        public async Task GetOfferDetails_ShouldReturnSuccessResponse(int offerId)
        {
            // Arragne
            await AuthenticateCPUserAsync();
            // Act 
            var response = await TestClient.GetAsync(
                $"{ApiRoutes.Offer.GetOffer.Replace("{offerId}", offerId.ToString())}");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    OfferDetailsDto>>();

                result.IsSuccess.Should().BeTrue();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion
        #region GetAllOffer Test
        [Fact]
        public async Task GetAllOffer_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();
            // Act 
            var response = await TestClient.GetAsync(
                $"{ApiRoutes.Offer.GetAllOffers}");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    List<AllOfferDto>>>();

                result.IsSuccess.Should().BeTrue();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion
    }
}
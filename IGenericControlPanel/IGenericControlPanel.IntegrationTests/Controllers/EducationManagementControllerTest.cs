﻿
using IGenericControlPanel.UnitTests.Fixtures;

namespace IGenericControlPanel.IntegrationTests.Controllers;
public class EducationManagementControllerTest : IntegrationTest
{
    #region Get Test
    [Fact]
    public async Task GetEducationsForUser_ShouldReturnSuccessResponse()
    {
        // Arrange 
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.GetAsync(ApiRoutes.EducationManagement.GetEducationsForUser);
        // Assert

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Theory]
    [InlineData(3)]
    public async Task GetOneEducation_ShouldReturnSuccessResponse(int id)
    {
        // Arrange 
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.GetAsync(ApiRoutes.EducationManagement.GetOneEducation
            .Replace("{id}", id.ToString()));
        // Assert

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region Remove Test
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public async Task RemoveEducation_ShouldReturnSuccessResponse(int id)
    {
        // Arrange 
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.DeleteAsync(ApiRoutes.EducationManagement.RemoveEducation
            .Replace("{id}", id.ToString()));
        // Assert

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region Action
    [Fact]
    public async Task AddEducation_ShouldReturnSuccessResponse()
    {
        // Arrange
        await AuthenticateCPUserAsync();
        string fileName = "C:\\Users\\Naddem\\Desktop\\files\\5.jpg";
        var addDto = EducationManagementFixture.GetEducationAddDto();

        var formData = new MultipartFormDataContent()
        {
            {new StringContent(addDto.SchoolName),"SchoolName"},
            {new StringContent(addDto.Description),"Description"},
            {new StringContent(addDto.StartDate.ToString()),"StartDate"},
            {new StringContent(addDto.EndDate.ToString()),"EndDate"},
            {new StringContent(addDto.StudyingField),"StudyingField"},
            {new StringContent(addDto.ScientificDegree),"ScientificDegree"},
            {new StringContent(addDto.Link),"Link"},
            {new StringContent(addDto.Grade),"Grade"},
        };
        var fileContnet = fileName.GetFormFile($"image/{fileName.Split('.')[1]}");
        formData.Add(fileContnet, $"EducationFiles", fileName);
        // Act
        var response = await TestClient.PostAsync(
            ApiRoutes.EducationManagement.AddEducation, formData);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task UpdateEducation_ShouldReturnSuccessResponse()
    {
        // Arrange
        await AuthenticateCPUserAsync();
        string fileName = "C:\\Users\\Naddem\\Desktop\\files\\5.jpg";
        var updateDto = EducationManagementFixture.GetEducationUpdateDto();

        var formData = new MultipartFormDataContent()
        {
            {new StringContent(updateDto.Id.ToString()),"Id"},
            {new StringContent(updateDto.SchoolName),"SchoolName"},
            {new StringContent(updateDto.Description),"Description"},
            {new StringContent(updateDto.StartDate.ToString()),"StartDate"},
            {new StringContent(updateDto.EndDate.ToString()),"EndDate"},
            {new StringContent(updateDto.StudyingField),"StudyingField"},
            {new StringContent(updateDto.ScientificDegree),"ScientificDegree"},
            {new StringContent(updateDto.Link),"Link"},
            {new StringContent(updateDto.Grade),"Grade"},
        };
        var fileContnet = fileName.GetFormFile($"image/{fileName.Split('.')[1]}");
        formData.Add(fileContnet, $"EducationFiles", fileName);
        // Act
        var response = await TestClient.PutAsync(
            ApiRoutes.EducationManagement.UpdateEducation, formData);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion
}

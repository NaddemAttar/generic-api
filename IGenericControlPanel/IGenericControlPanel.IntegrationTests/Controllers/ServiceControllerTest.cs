﻿
namespace IGenericControlPanel.IntegrationTests.Controllers;
public class ServiceControllerTest : IntegrationTest
{
    #region GetServices Test
    [Theory]
    [InlineData(null, true)]
    [InlineData(null, false)]
    public async Task GetServices_ShouldReturnServicesWithData(string query, bool withData)
    {
        // Act
        var response = await TestClient.GetAsync(
            ApiRoutes.Service.GetServices
            .Replace("{query}", query)
            .Replace("{with_data}", withData.ToString()));

        var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
            IEnumerable<ServiceDetailsDto>>>();

        response.StatusCode.Should().Be(HttpStatusCode.OK);

        result.IsSuccess.Should().BeTrue();
        result.Result.Should().NotBeEmpty();

        // Assert
    }
    #endregion

    #region GetServiceDetails Test
    [Theory]
    [InlineData(10)]
    public async Task GetServiceDetails_ShouldReturnServiceDetails(int serviceId)
    {
        // Act
        var response = await TestClient.GetAsync(
            ApiRoutes.Service.GetServiceDetails.Replace("{serviceId}", serviceId.ToString()));

        var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
            ServiceDetailsDto>>();

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        result.Result.Should().NotBeNull();
    }

    [Fact]
    public async Task GetServiceDetails_ShouldReturn404NotFound()
    {
        // Arrange
        int serviceId = 4;

        // Act
        var response = await TestClient.GetAsync(
            ApiRoutes.Service.GetServiceDetails.Replace("{serviceId}", serviceId.ToString()));

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }
    #endregion

    #region RemoveServiceById Test
    [Theory]
    [InlineData(10)]
    public async Task RemoveServiceById_ShouldRemoveService(int serviceId)
    {
        // Arrange
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.DeleteAsync(
            ApiRoutes.Service.RemoveServiceById.Replace("{serviceId}", serviceId.ToString()));

        var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult>>();

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        result.IsSuccess.Should().BeTrue();
    }

    [Fact]
    public async Task RemoveServiceById_ShouldReturn404NotFound()
    {
        // Arrange
        await AuthenticateCPUserAsync();
        int serviceId = 3;

        // Act
        var response = await TestClient.DeleteAsync(
            ApiRoutes.Service.RemoveServiceById.Replace("{serviceId}", serviceId.ToString()));

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }
    #endregion

    #region ServiceAction Test
    [Fact]
    public async Task ServiceAction_ShouldAddNewService()
    {
        // Arrange
        await AuthenticateCPUserAsync();
        string fileName = "C:\\Users\\Naddem\\Desktop\\files\\5.jpg";
        string fileName2 = "C:\\Users\\Naddem\\Desktop\\files\\3.jpg";
        string fileName3 = "C:\\Users\\Naddem\\Desktop\\files\\t1.jpg";
        var data = new ServiceViewModel
        {
            Name = "asd",
            Description = "Description",
            //Price = 2000,
        };

        var formData = new MultipartFormDataContent()
        {
            {new StringContent(data.Name),"Name"},
            {new StringContent(data.Description),"Description"},
            //{new StringContent(data.Price.ToString()),"Price"},
        };

        var fileContnet = fileName.GetFormFile($"image/{fileName.Split('.')[1]}");
        var fileContnet2 = fileName2.GetFormFile($"image/{fileName2.Split('.')[1]}");
        var fileContnet3 = fileName3.GetFormFile($"image/{fileName3.Split('.')[1]}");

        formData.Add(fileContnet, $"Images", fileName);
        formData.Add(fileContnet2, "Images", fileName2);
        formData.Add(fileContnet3, "Images", fileName3);

        // Act
        var response = await TestClient.PostAsync(ApiRoutes.Service.ServiceAction, formData);
        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion
}

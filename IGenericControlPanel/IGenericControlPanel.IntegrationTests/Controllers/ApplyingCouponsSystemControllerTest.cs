﻿
namespace IGenericControlPanel.IntegrationTests.Controllers;
public class ApplyingCouponsSystemControllerTest : IntegrationTest
{
    #region GetAvailableCoupons Test
    [Fact]
    public async Task GetAvailableCoupons_ShouldReturnSuccessResponse()
    {
        // Arrange
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.GetAsync(ApiRoutes.ApplyingCouponsSystem.GetAvailableCoupons);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region CouponBooking Test
    [Theory]
    [InlineData(2)]
    public async Task CouponBooking_ShouldReturnSuccessResponse(int couponId)
    {
        // Arrange
        await AuthenticateNormalUserAsync();

        // Act
        var response = await TestClient.PutAsync(
            ApiRoutes.ApplyingCouponsSystem.CouponBooking
            .Replace("{couponId}", couponId.ToString()), null);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region ActivateCouponBooking Test
    [Fact]
    public async Task ActivateCouponBooking_ShouldReturnSuccessResponse()
    {
        // Arrange
        string code = "MEXLVZ";
        await AuthenticateNormalUserAsync();

        // Act
        var response = await TestClient.PutAsync(
            ApiRoutes.ApplyingCouponsSystem.ActivateCouponBooking
            .Replace("{code}", code), null);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region DeActivateCouponBooking Test
    [Fact]
    public async Task DeActivateCouponBooking_ShouldReturnSuccessResponse()
    {
        // Arrange
        await AuthenticateNormalUserAsync();

        // Act
        var response = await TestClient.PutAsync(
           ApiRoutes.ApplyingCouponsSystem.DeActivateCouponBooking, null);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region GetUserCoupons Test

    [Theory]
    [InlineData(false, true)]
    [InlineData(false, false)]
    [InlineData(true, false)]
    [InlineData(true, true)]
    public async Task GetUserCoupons_ShouldReturnSuccessResponse(bool isBeneficiary, bool isActivate)
    {
        // Arrange
        await AuthenticateNormalUserAsync();

        // Act
        var response = await TestClient.GetAsync(
           ApiRoutes.ApplyingCouponsSystem.GetUserCoupons
           .Replace("{isBeneficiary}", isBeneficiary.ToString())
           .Replace("{isActivate}", isActivate.ToString()));

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    #endregion

    #region PointsDeductionFromUser Test
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public async Task PointsDeductionFromUser_ShouldReturnSuccessResponse(int userCouponId)
    {
        // Arrang
        await AuthenticateNormalUserAsync();

        // Act
        var response = await TestClient.PutAsync(ApiRoutes.ApplyingCouponsSystem.PointsDeductionFromUser
            .Replace("{userCouponId}", userCouponId.ToString()), null);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion
}

﻿
namespace IGenericControlPanel.IntegrationTests.Controllers
{
    public class MedicineControllerTest : IntegrationTest
    {
        #region GetAllMidicines Test
        [Fact]
        public async Task GetAllMidicines_ShouldReturnSuccessResponse()
        {
            // Arragne

            // Act 
            var response = await TestClient.GetAsync(ApiRoutes.Midicine.GetAllMidicines);

            if (response.StatusCode == HttpStatusCode.OK)
            {

                // Assert

                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    IEnumerable<MedicineFormDto>>>();

                result.IsSuccess.Should().BeTrue();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion

        #region GetMidicineAutoComplete Test
        [Fact]
        public async Task GetMidicineAutoComplete_ShouldReturnSuccessResponse()
        {
            // Arragne

            // Act
            var response = await TestClient.GetAsync(ApiRoutes.Midicine.GetMedicineAutoComplete);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert
                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    IEnumerable<MedicineFormDto>>>();

                result.IsSuccess.Should().BeTrue();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion

        #region ActionMedicine Test
        [Fact]
        public async Task ActionMidicine_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();
            MedicineFormDto formDto = new MedicineFormDto()
            {
                Name = "asdasdasd",
            };
            var multiContent = new MultipartFormDataContent();
            multiContent.Add(new StringContent(formDto.Name.ToString()), "Name");
            // Act
            var response = await TestClient.PostAsync(ApiRoutes.Midicine.Action, multiContent);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert

                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,

                    MedicineFormDto>>();

                result.IsSuccess.Should().BeTrue();
                result.Result.Should().NotBeNull();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion

        #region RemoveMedicine Test
        [Fact]
        public async Task RemoveMidicine_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();

            // Act
            var response = await TestClient.DeleteAsync(ApiRoutes.Midicine.RemoveMedicine + "?Id=1");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert
                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult>>();

                result.IsSuccess.Should().BeTrue();
                result.Should().NotBeNull();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion
    }
}

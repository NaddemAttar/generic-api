﻿
using IGenericControlPanel.GamificationSystem.Dto.Gamification;

namespace IGenericControlPanel.IntegrationTests.Controllers;
public class GamificationManagmentsControllerTest : IntegrationTest
{
    #region GetAllGamificationSections Test
    [Fact]
    public async Task GetAllGamificationSections_ShouldReturnData()
    {
        // Act
        var response = await TestClient.GetAsync(ApiRoutes.GamificationManagments.GetAllGamificationSections);

        // Assert
        if (response.StatusCode == HttpStatusCode.OK)
        {

            var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                IEnumerable<GamificationDto>>>();


            result.IsSuccess.Should().BeTrue();
            result.Result.Should().NotBeNullOrEmpty();
        }

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region UpdatePointsForGamificactionSections Test
    [Fact]
    public async Task UpdatePointsForGamificactionSections_ShouldUpdateSomeGamificationSections()
    {
        // Arrange
        await AuthenticateCPUserAsync();

        var someGamificationSections = GamificationFixture.GetSomeGamificationSections();

        var formBody = someGamificationSections.CreateBodyData();

        // Act
        var response = await TestClient.PutAsync(
            ApiRoutes.GamificationManagments.UpdatePointsForGamificactionSections, formBody);

        if (response.StatusCode == HttpStatusCode.OK)
        {
            // Assert
            var result = await response.Content
                .ReadAsAsync<OperationResult<GenericOperationResult, IEnumerable<GamificationDto>>>();

            result.IsSuccess.Should().BeTrue();
            result.Result.Should().NotBeEmpty();
        }

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region AddNewGamificationSection Test
    public static IEnumerable<object[]> GetGamifications()
    {
        return GamificationFixture.GetNewGamificationSections();
    }

    [Theory]
    [MemberData(nameof(GetGamifications))]
    public async Task AddNewGamificationSection_ShouldAddNewGamification(GamificationDto gamification)
    {
        // Arrange
        await AuthenticateCPUserAsync();
        var formData = gamification.CreateBodyData();

        // Act
        var response = await TestClient.PostAsync(ApiRoutes.GamificationManagments.AddNewGamificationSection, formData);

        if (response.StatusCode == HttpStatusCode.OK)
        {
            // Assert
            var result = await response.Content
                .ReadAsAsync<OperationResult<GenericOperationResult, GamificationDto>>();

            result.IsSuccess.Should().BeTrue();

            result.Result.Should().NotBeNull();

        }

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region GetUsersWithTheirPoints Test
    [Theory]
    [InlineData(false, true, true, null, 8, 0)]
    public async Task GetUsersWithTheirPoints_ShouldReturnSuccessResponse(
        bool enablePagination, bool usersHavePointsOnly, bool isAscendingOrder,
        string query, int pageSize, int pageNumber)
    {
        // Arrange
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.GetAsync(
           ApiRoutes.GamificationManagments.GetUsersWithTheirPoints
           .Replace("{enablePagination}", enablePagination.ToString())
           .Replace("{usersHavePointsOnly}", usersHavePointsOnly.ToString())
           .Replace("{isAscendingOrder}", isAscendingOrder.ToString())
           .Replace("{query}", query)
           .Replace("{pageSize}", pageSize.ToString())
           .Replace("{pageNumber}", pageNumber.ToString()));

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion
}
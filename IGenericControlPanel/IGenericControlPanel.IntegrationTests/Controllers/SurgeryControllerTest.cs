﻿
namespace IGenericControlPanel.IntegrationTests.Controllers
{
    public class SurgeryControllerTest : IntegrationTest
    {
        #region GetAllSurgeries Test
        [Fact]
        public async Task GetAllSurgeries_ShouldReturnSuccessResponse()
        {
            // Arragne

            // Act 
            var response = await TestClient.GetAsync(ApiRoutes.Surgery.GetAllSurgeries);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert

                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    IEnumerable<SurgeryDto>>>();

                result.IsSuccess.Should().BeTrue();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion

        #region GetSurgeryAutoComplete Test
        [Fact]
        public async Task GetSurgeryAutoComplete_ShouldReturnSuccessResponse()
        {
            // Arragne

            // Act
            var response = await TestClient.GetAsync(ApiRoutes.Surgery.GetSurgeryAutoComplete);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert

                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    IEnumerable<SurgeryDto>>>();

                result.IsSuccess.Should().BeTrue();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion

        #region ActionSurgery Test
        [Fact]
        public async Task ActionSurgery_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();
            SurgeryDto formDto = new SurgeryDto()
            {
                Name = "asdasdasd",
                Description = "asdasdasdasd",
            };
            var multiContent = new MultipartFormDataContent();
            multiContent.Add(new StringContent(formDto.Name.ToString()), "Name");
            multiContent.Add(new StringContent(formDto.Description.ToString()), "Description");
            // Act
            var response = await TestClient.PostAsync(ApiRoutes.Surgery.Action, multiContent);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert

                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    DiseasesFormDto>>();

                result.IsSuccess.Should().BeTrue();
                result.Result.Should().NotBeNull();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion

        #region ActionPatient Test
        [Fact]
        public async Task ActionPatientSurgry_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateNormalUserAsync();
            PatientSurgeryDto formDto = new PatientSurgeryDto()
            {
                SurgeryId = 1
            };

            // Arragne
            var multiContent = new MultipartFormDataContent();
            multiContent.Add(new StringContent(formDto.SurgeryId.ToString()), "SurgeryId");

            // Act
            var response = await TestClient.PutAsync(ApiRoutes.Surgery.ActionPatientSurgery, multiContent);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert

                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    PatientSurgeryDto>>();

                result.IsSuccess.Should().BeTrue();
                result.Result.Should().NotBeNull();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion

        #region RemoveSurgery
        [Fact]
        public async Task RemoveSurgery_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();

            // Act
            var response = await TestClient.DeleteAsync(ApiRoutes.Surgery.RemoveSurgery + "?surgeryId=1");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert
                response.StatusCode.Should().Be(HttpStatusCode.OK);

                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult>>();

                result.IsSuccess.Should().BeTrue();
                result.Should().NotBeNull();
            }
        }
        #endregion
    }
}

﻿namespace IGenericControlPanel.IntegrationTests.Controllers;
public class BrandControllerTest : IntegrationTest
{

    #region GetAllBrands Test
    [Fact]
    public async Task GetAllBrands_ShouldReturnSuccessResponse()
    {
        // Arragne

        // Act
        var response = await TestClient.GetAsync(ApiRoutes.Brand.GetAllBrands);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);

        var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
            IEnumerable<BrandDto>>>();

        result.IsSuccess.Should().BeTrue();
    }
    #endregion

    #region ActionBrand Test
    [Fact]
    public async Task ActionBrand_ShouldReturnSuccessResponse()
    {
        // Arragne
        await AuthenticateCPUserAsync();
        BrandDto brandDto = new BrandDto()
        {
            Id = 10,
            Name = "asdasd"
        };
        var brandJson = JsonConvert.SerializeObject(brandDto);
        var buffer = System.Text.Encoding.UTF8.GetBytes(brandJson);
        var byteContent = new ByteArrayContent(buffer);
        byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        // Act
        var response = await TestClient.PostAsync(ApiRoutes.Brand.ActionBrand, byteContent);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);

        var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
            BrandDto>>();

        result.IsSuccess.Should().BeTrue();
        result.Result.Should().NotBeNull();
    }
    #endregion

    #region RemoveBrand Test
    [Fact]
    public async Task RemoveBrand_ShouldReturnSuccessResponse()
    {
        // Arragne
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.DeleteAsync(ApiRoutes.Brand.RemoveBrand + $"?BrandId=2");

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);

        var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult>>();

        result.IsSuccess.Should().BeTrue();
        result.Should().NotBeNull();
    }
    #endregion
}

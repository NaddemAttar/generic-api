﻿
namespace IGenericControlPanel.IntegrationTests.Controllers;

public class BlogControllerTest : IntegrationTest
{
    #region GetBlogs Test
    [Fact]
    public async Task GetBlogs_ShouldReturnSuccessResponse()
    {
        // Arragne

        // Act 
        var response = await TestClient.GetAsync(ApiRoutes.Blog.GetBlogs);

        var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
           IEnumerable<BlogDetailsDto>>>();

        // Assert

        response.StatusCode.Should().Be(HttpStatusCode.OK);

        result.IsSuccess.Should().BeTrue();
    }
    #endregion

    #region GetBlogDetails Test
    [Theory]
    [InlineData(2)]
    [InlineData(3)]
    [InlineData(4)]
    [InlineData(5)]
    [InlineData(6)]
    public async Task GetBlogDetails_ShouldReturnSuccessResponse(int blogId)
    {
        // Arragne

        // Act 
        var response = await TestClient.GetAsync(
            $"{ApiRoutes.Blog.GetBlogDetails.Replace("{id}", blogId.ToString())}");

        if (response.StatusCode == HttpStatusCode.OK)
        {
            var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                BlogDetailsDto>>();

            result.IsSuccess.Should().BeTrue();
        }

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region ActionBlog Test
    [Fact]
    public async Task ActionBlog_ShouldReturnSuccessResponse()
    {
        // Arragne
        await AuthenticateCPUserAsync();

        var formData = BlogFixture.GetDataForActionBlog();
        // Act
        var response = await TestClient.PostAsync(ApiRoutes.Blog.Action, formData);

        if (response.StatusCode == HttpStatusCode.OK)
        {
            var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
            BlogFormViewModel>>();

            // Assert
            result.IsSuccess.Should().BeTrue();
            result.Result.Should().NotBeNull();

        }

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region RemoveBlog Test
    [Theory]
    [InlineData(2)]
    [InlineData(3)]
    [InlineData(4)]
    [InlineData(5)]
    [InlineData(6)]
    public async Task RemoveBlog_ShouldReturnSuccessResponse(int blogId)
    {
        // Arragne
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.DeleteAsync(
            $"{ApiRoutes.Blog.RemoveBlog.Replace("{id}", blogId.ToString())}");

        if (response.StatusCode == HttpStatusCode.OK)
        {
            // Assert
            var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult>>();
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            result.IsSuccess.Should().BeTrue();
        }
    }
    #endregion
}

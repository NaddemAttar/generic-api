﻿namespace IGenericControlPanel.IntegrationTests.Controllers
{
    public class DiseasesControllerTest : IntegrationTest
    {
        #region GetAllDiseases Test
        [Fact]
        public async Task GetAllDiseases_ShouldReturnSuccessResponse()
        {
            // Arragne

            // Act 
            var response = await TestClient.GetAsync(ApiRoutes.Disease.GetAllDiseases);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert
                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    IEnumerable<DiseasesFormDto>>>();

                result.IsSuccess.Should().BeTrue();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion

        #region GetDiseasesAutoComplete Test
        [Fact]
        public async Task GetDiseasesAutoComplete_ShouldReturnSuccessResponse()
        {
            // Act
            var response = await TestClient.GetAsync(ApiRoutes.Disease.GetDiseasesAutoComplete);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert

                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    IEnumerable<DiseasesFormDto>>>();

                result.IsSuccess.Should().BeTrue();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion

        #region ActionDiseases Test
        [Fact]
        public async Task ActionDiseases_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();
            DiseasesFormDto formDto = new DiseasesFormDto()
            {
                Name = "asdasdasd",
                DiseaseType = DiseaseType.ChronicDisease,
                Description = "asdasdasdasd",
            };
            var multiContent = new MultipartFormDataContent();
            multiContent.Add(new StringContent(formDto.Name.ToString()), "Name");
            multiContent.Add(new StringContent(formDto.DiseaseType.ToString()), "DiseaseType");
            multiContent.Add(new StringContent(formDto.Description.ToString()), "Description");
            // Act
            var response = await TestClient.PostAsync(ApiRoutes.Disease.Action, multiContent);

            if (response.StatusCode == HttpStatusCode.OK)
            {


                // Assert

                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    DiseasesFormDto>>();

                result.IsSuccess.Should().BeTrue();
                result.Result.Should().NotBeNull();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion

        #region ActionPatientDiseases Test
        [Fact]
        public async Task ActionPatientDiseases_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateNormalUserAsync();
            PatientDiseasesDto formDto = new PatientDiseasesDto()
            {
                DiseaseId = 1,
            };

            // Arragne
            var multiContent = new MultipartFormDataContent();
            multiContent.Add(new StringContent(formDto.DiseaseId.ToString()), "DiseaseId");

            // Act
            var response = await TestClient.PostAsync(ApiRoutes.Disease.ActionPatientDiseases, multiContent);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert

                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    PatientDiseasesDto>>();

                result.IsSuccess.Should().BeTrue();
                result.Result.Should().NotBeNull();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion

        #region RemoveDisease Test
        [Fact]
        public async Task RemoveDisease_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();

            // Act
            var response = await TestClient.DeleteAsync(ApiRoutes.Disease.RemoveDisease + "?Id=1");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert

                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult>>();

                result.IsSuccess.Should().BeTrue();
                result.Should().NotBeNull();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion
    }
}


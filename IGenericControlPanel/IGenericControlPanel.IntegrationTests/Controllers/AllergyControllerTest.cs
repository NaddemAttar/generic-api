﻿namespace IGenericControlPanel.IntegrationTests.Controllers
{
    public class AllergyControllerTest : IntegrationTest
    {
        #region GetAllAllergies Test
        [Fact]
        public async Task GetAllAllergies_ShouldReturnSuccessResponse()
        {
            // Act 
            var response = await TestClient.GetAsync(ApiRoutes.Allergy.GetAllAllergies);

            if (response.StatusCode == HttpStatusCode.OK)
            {

                // Assert

                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    IEnumerable<AllergyFormDto>>>();

                result.IsSuccess.Should().BeTrue();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion

        #region GetAllergiesAutoComplete Test
        [Fact]
        public async Task GetAllergiesAutoComplete_ShouldReturnSuccessResponse()
        {
            // Act
            var response = await TestClient.GetAsync(ApiRoutes.Allergy.GetAllergiesAutoComplete);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert

                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    IEnumerable<AllergyFormDto>>>();

                result.IsSuccess.Should().BeTrue();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion

        #region ActionAllergy Test
        [Fact]
        public async Task ActionAllergy_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();
            AllergyFormDto formDto = new AllergyFormDto()
            {
                Id = 0,
                Name = "asdasdasd",
                AllergyType = AllergyType.NonDrugAllergy,
                Description = "asdasdasdasd",
            };
            var multiContent = new MultipartFormDataContent();
            multiContent.Add(new StringContent(formDto.Name.ToString()), "Name");
            multiContent.Add(new StringContent(formDto.AllergyType.ToString()), "AllergyType");
            multiContent.Add(new StringContent(formDto.Description.ToString()), "Description");
            // Act
            var response = await TestClient.PostAsync(ApiRoutes.Allergy.ActionAllergies, multiContent);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert

                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    AllergyFormDto>>();

                result.IsSuccess.Should().BeTrue();
                result.Result.Should().NotBeNull();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion

        #region ActionPatientAllegries Test
        [Fact]
        public async Task ActionPatientAllegries_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateNormalUserAsync();
            PatientAllergyFormDto formDto = new PatientAllergyFormDto()
            {
                AllergyId = 1,
            };

            // Arragne
            var multiContent = new MultipartFormDataContent();
            multiContent.Add(new StringContent(formDto.AllergyId.ToString()), "AllergyId");

            // Act
            var response = await TestClient.PostAsync(ApiRoutes.Allergy.ActionPatientAllegries, multiContent);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert

                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    PatientAllergyFormDto>>();

                result.IsSuccess.Should().BeTrue();
                result.Result.Should().NotBeNull();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion

        #region RemoveAllergy Test
        [Fact]
        public async Task RemoveAllergy_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();

            // Act
            var response = await TestClient.DeleteAsync(ApiRoutes.Allergy.RemoveAllegry + "?Id=3");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert
                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult>>();
                result.IsSuccess.Should().BeTrue();
                result.Should().NotBeNull();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion
    }
}

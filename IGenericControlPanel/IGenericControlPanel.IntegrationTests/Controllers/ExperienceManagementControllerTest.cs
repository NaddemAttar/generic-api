﻿
using IGenericControlPanel.UnitTests.Fixtures;

namespace IGenericControlPanel.IntegrationTests.Controllers;
public class ExperienceManagementControllerTest: IntegrationTest
{
    #region Get Test
    [Fact]
    public async Task GetExperiencesForUser_ShouldReturnSuccessResponse()
    {
        // Arrange
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.GetAsync(ApiRoutes.ExperienceManagement.GetExperiencesForUser);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Theory]
    [InlineData(5)]
    [InlineData(6)]
    public async Task GetOneExperience_ShouldReturnSuccessResponse(int id)
    {
        // Arrange
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.GetAsync(ApiRoutes.ExperienceManagement.GetOneExperience
            .Replace("{id}", id.ToString()));

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region Remove
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public async Task RemoveExperience_ShouldReturnSuccessResponse(int id)
    {
        // Arrange
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.DeleteAsync(ApiRoutes.ExperienceManagement.RemoveExperience
            .Replace("{id}", id.ToString()));

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region Action
    [Fact]
    public async Task AddExperience_ShouldReturnSuccessResponse()
    {
        // Arrange
        await AuthenticateCPUserAsync();
        string fileName = "C:\\Users\\Naddem\\Desktop\\files\\5.jpg";
        var addDto = ExperienceManagementFixture.GetExperienceAddDto();

        var formData = new MultipartFormDataContent()
        {
            {new StringContent(addDto.Name),"Name"},
            {new StringContent(addDto.Description),"Description"},
            {new StringContent(addDto.StartDate.ToString()),"StartDate"},
            {new StringContent(addDto.EndDate.ToString()),"EndDate"},
            {new StringContent(addDto.Institution),"Institution"},
        };
        var fileContnet = fileName.GetFormFile($"image/{fileName.Split('.')[1]}");
        formData.Add(fileContnet, $"ExperienceFiles", fileName);
        // Act
        var response = await TestClient.PostAsync(
            ApiRoutes.ExperienceManagement.AddExperience, formData);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task UpdateExperience_ShouldReturnSuccessResponse()
    {
        // Arrange
        await AuthenticateCPUserAsync();
        string fileName = "C:\\Users\\Naddem\\Desktop\\files\\5.jpg";
        var updateDto = ExperienceManagementFixture.GetExperienceUpdateDto();

        var formData = new MultipartFormDataContent()
        {
            {new StringContent(updateDto.Id.ToString()),"Id"},
            {new StringContent(updateDto.Name),"Name"},
            {new StringContent(updateDto.Description),"Description"},
            {new StringContent(updateDto.StartDate.ToString()),"StartDate"},
            {new StringContent(updateDto.EndDate.ToString()),"EndDate"},
            {new StringContent(updateDto.Institution),"Institution"},
        };
        var fileContnet = fileName.GetFormFile($"image/{fileName.Split('.')[1]}");
        formData.Add(fileContnet, $"ExperienceFiles", fileName);
        // Act
        var response = await TestClient.PutAsync(
            ApiRoutes.ExperienceManagement.UpdateExperience, formData);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion
}

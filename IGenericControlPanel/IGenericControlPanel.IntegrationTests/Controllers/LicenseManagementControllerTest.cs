﻿
using IGenericControlPanel.UnitTests.Fixtures;

namespace IGenericControlPanel.IntegrationTests.Controllers;
public class LicenseManagementControllerTest : IntegrationTest
{
    #region Get Test
    [Fact]
    public async Task GetLicenseForUser_ShouldReturnSuccessResponse()
    {
        // Arrange 
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.GetAsync(ApiRoutes.LicenseManagement.GetLicensesForUser);
        // Assert

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Theory]
    [InlineData(3)]
    public async Task GetOneLicense_ShouldReturnSuccessResponse(int id)
    {
        // Arrange 
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.GetAsync(ApiRoutes.LicenseManagement.GetOneLicense
            .Replace("{id}", id.ToString()));
        // Assert

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region Remove Test
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public async Task RemoveLicense_ShouldReturnSuccessResponse(int id)
    {
        // Arrange 
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.DeleteAsync(ApiRoutes.LicenseManagement.RemoveLicense
            .Replace("{id}", id.ToString()));
        // Assert

        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region Action
    [Fact]
    public async Task AddLicense_ShouldReturnSuccessResponse()
    {
        // Arrange
        await AuthenticateCPUserAsync();
        string fileName = "C:\\Users\\Naddem\\Desktop\\files\\5.jpg";
        var addDto = LicenseManagementFixture.GetLicenseAddDto();

        var formData = new MultipartFormDataContent()
        {
            {new StringContent(addDto.Name),"Name"},
            {new StringContent(addDto.Description),"Description"},
        };
        var fileContnet = fileName.GetFormFile($"image/{fileName.Split('.')[1]}");
        formData.Add(fileContnet, $"LicenseFiles", fileName);
        // Act
        var response = await TestClient.PostAsync(
            ApiRoutes.LicenseManagement.AddLicense, formData);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task UpdateLicense_ShouldReturnSuccessResponse()
    {
        // Arrange
        await AuthenticateCPUserAsync();
        string fileName = "C:\\Users\\Naddem\\Desktop\\files\\5.jpg";
        var updateDto = LicenseManagementFixture.GetLicenseUpdateDto();

        var formData = new MultipartFormDataContent()
        {
            {new StringContent(updateDto.Id.ToString()),"Id"},
            {new StringContent(updateDto.Name),"Name"},
            {new StringContent(updateDto.Description),"Description"},
        };
        var fileContnet = fileName.GetFormFile($"image/{fileName.Split('.')[1]}");
        formData.Add(fileContnet, $"LicenseFiles", fileName);
        // Act
        var response = await TestClient.PutAsync(
            ApiRoutes.LicenseManagement.UpdateLicense, formData);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion
}

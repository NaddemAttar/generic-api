﻿
namespace IGenericControlPanel.IntegrationTests.Controllers;
public class CouponManagementControllerTest : IntegrationTest
{
    #region Get Section Test
    [Theory]
    [InlineData(true, 0, 8, null)]
    [InlineData(false, 2, 8, null)]
    public async Task GetCoupons_ShouldReturnSuccessResonse(
        bool enablePagination, int pageNumber, int pageSize, string query)
    {
        // Act
        var response = await TestClient.GetAsync(
            ApiRoutes.CouponManagement.GetCoupons
            .Replace("{enablePagination}", enablePagination.ToString())
            .Replace("{pageNumber}", pageNumber.ToString())
            .Replace("{pageSize}", pageSize.ToString())
            .Replace("{query}", query)
            );

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Theory]
    [InlineData(7)]
    public async Task GetCouponDetails_ShouldReturnSuccessResonse(int id)
    {
        // Act
        var response = await TestClient.GetAsync(
            ApiRoutes.CouponManagement.GetCouponDetails
            .Replace("{id}", id.ToString())
            );

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region Remove Coupon Test
    [Theory]
    [InlineData(8)]
    public async Task RemoveCoupon_ShouldReturnSuccessResonse(int id)
    {
        // Arrange
        await AuthenticateCPUserAsync();
        // Act
        var response = await TestClient.DeleteAsync(
            ApiRoutes.CouponManagement.RemoveCoupon
            .Replace("{id}", id.ToString())
            );

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region Add Coupon Test
    [Fact]
    public async Task AddCoupon_ShouldReturnSuccessResponse()
    {
        // Arrange
        await AuthenticateCPUserAsync();
        var getOneCoupon = CouponManagementFixture.GetOneCoupon();
        var formBodyData = getOneCoupon.CreateBodyData();
        // Act
        var result = await TestClient.PostAsync(
            ApiRoutes.CouponManagement.AddCoupon, formBodyData);

        // Assert
        result.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region Update Coupon Test
    [Fact]
    public async Task UpdateCoupon_ShouldReturnSuccessResponse()
    {
        // Arrange
        await AuthenticateCPUserAsync();
        var getOneCoupon = CouponManagementFixture.GetOneCoupon();
        var formBodyData = getOneCoupon.CreateBodyData();
        // Act
        var result = await TestClient.PutAsync(
            ApiRoutes.CouponManagement.UpdateCoupon, formBodyData);

        // Assert
        result.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion
}

﻿
namespace IGenericControlPanel.IntegrationTests.Controllers;
public class BuildResumeControllerTest: IntegrationTest
{

    [Fact]
    public async Task GetResumeForHcpUser_ShouldReturnSuccessResponse()
    {
        // Arrange
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.GetAsync(ApiRoutes.BuildResume.GetResumeForHcpUser
            .Replace("{hcpId}", 0.ToString()));

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

}

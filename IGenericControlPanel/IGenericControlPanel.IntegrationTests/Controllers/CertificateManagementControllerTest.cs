﻿
using IGenericControlPanel.UnitTests.Fixtures;

namespace IGenericControlPanel.IntegrationTests.Controllers;
public class CertificateManagementControllerTest: IntegrationTest
{
    #region Get Test
    [Fact]
    public async Task GetCertificatesForUser_ShouldReturnSuccessResponse()
    {
        // Arrange
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.GetAsync(ApiRoutes.CertificateManagement.GetCertificateForUser);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Theory]
    [InlineData(4)]
    public async Task GetOneCertificate_ShouldReturnSuccessResponse(int id)
    {
        // Arrange
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.GetAsync(ApiRoutes.CertificateManagement.GetOneLCertificate
            .Replace("{id}", id.ToString()));

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region Remove
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public async Task RemoveCertificate_ShouldReturnSuccessResponse(int id)
    {
        // Arrange
        await AuthenticateCPUserAsync();

        // Act
        var response = await TestClient.DeleteAsync(ApiRoutes.CertificateManagement.RemoveCertificate
            .Replace("{id}", id.ToString()));

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion

    #region Action
    [Fact]
    public async Task AddCertificate_ShouldReturnSuccessResponse()
    {
        // Arrange
        await AuthenticateCPUserAsync();
        string fileName = "C:\\Users\\Naddem\\Desktop\\files\\5.jpg";
        var addDto = CertificateManagementFixture.GetCertificateAddDto();

        var formData = new MultipartFormDataContent()
        {
            {new StringContent(addDto.Title),"Title"},
            {new StringContent(addDto.Description),"Description"},
            {new StringContent(addDto.Institution),"Institution"},
        };
        var fileContnet = fileName.GetFormFile($"image/{fileName.Split('.')[1]}");
        formData.Add(fileContnet, $"CertificateFiles", fileName);
        // Act
        var response = await TestClient.PostAsync(
            ApiRoutes.CertificateManagement.AddCertificate, formData);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task UpdateCertificate_ShouldReturnSuccessResponse()
    {
        // Arrange
        await AuthenticateCPUserAsync();
        string fileName = "C:\\Users\\Naddem\\Desktop\\files\\5.jpg";
        var updateDto = CertificateManagementFixture.GetCertificateUpdateDto();

        var formData = new MultipartFormDataContent()
        {
            {new StringContent(updateDto.Id.ToString()),"Id"},
            {new StringContent(updateDto.Title),"Title"},
            {new StringContent(updateDto.Description),"Description"},
            {new StringContent(updateDto.Institution),"Institution"},
        };
        var fileContnet = fileName.GetFormFile($"image/{fileName.Split('.')[1]}");
        formData.Add(fileContnet, $"CertificateFiles", fileName);
        // Act
        var response = await TestClient.PutAsync(
            ApiRoutes.CertificateManagement.UpdateCertificate, formData);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    #endregion
}

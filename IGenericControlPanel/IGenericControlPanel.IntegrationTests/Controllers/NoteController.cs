﻿
namespace IGenericControlPanel.IntegrationTests.Controllers
{
    public class NoteController : IntegrationTest
    {
        #region GetAllNotes Test
        [Fact]
        public async Task GetAllNotes_ShouldReturnSuccessResponse()
        {
            // Act 
            var response = await TestClient.GetAsync(ApiRoutes.Note.GetAllNotes);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert

                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    IEnumerable<NoteDto>>>();

                result.IsSuccess.Should().BeTrue();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion

        #region ActionNote Test
        [Fact]
        public async Task ActionNote_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();
            NoteDto brandDto = new NoteDto()
            {
                Body = "asdasdsad"
            };
            var brandJson = JsonConvert.SerializeObject(brandDto);
            var buffer = System.Text.Encoding.UTF8.GetBytes(brandJson);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            // Act
            var response = await TestClient.PostAsync(ApiRoutes.Note.Action, byteContent);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert

                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult,
                    NoteDto>>();

                result.IsSuccess.Should().BeTrue();
                result.Result.Should().NotBeNull();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion

        #region RemoveNote Test
        [Fact]
        public async Task RemoveNote_ShouldReturnSuccessResponse()
        {
            // Arragne
            await AuthenticateCPUserAsync();

            // Act
            var response = await TestClient.DeleteAsync(ApiRoutes.Note.RemoveNote + "?Id=1");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Assert
                var result = await response.Content.ReadAsAsync<OperationResult<GenericOperationResult>>();

                result.IsSuccess.Should().BeTrue();
                result.Should().NotBeNull();
            }

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        #endregion
    }
}

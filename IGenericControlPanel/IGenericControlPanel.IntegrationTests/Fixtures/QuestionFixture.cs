﻿
namespace IGenericControlPanel.IntegrationTests.Fixtures;
public static class QuestionFixture
{
    public static MultipartFormDataContent GetAnswerActionData()
    {
        AnswerFormViewModel answerFromViewModel = new()
        {
            Id = 3,
            QuestionId = 1,
            Content = "Hellooooooooooooooooo"
        };

        var answerData = new Dictionary<string, string>
        {
            {nameof(answerFromViewModel.Id),answerFromViewModel.Id.ToString()},
            {nameof(answerFromViewModel.QuestionId),answerFromViewModel.QuestionId.ToString()},
            {nameof(answerFromViewModel.Content ),answerFromViewModel.Content}
        };
        var formData = new MultipartFormDataContent();

        foreach (var data in answerData)
        {
            formData.Add(new StringContent(data.Value), data.Key);
        }

        return formData;
    }
    public static MultipartFormDataContent GetQuestionActionData()
    {
        var questionData = GetEssentialDataForQuestion();
        var questionFiles = GetFiles();
        var removeFileIds = GetRemoveFileIds();
        var multiLevelCategoryIds = GetMultiLevelCategoryIds();
        MultipartFormDataContent formData = new();

        foreach (var data in questionData)
        {
            formData.Add(new StringContent(data.Value), data.Key);
        }

        foreach (var fileName in questionFiles)
        {
            var fileContent = fileName.GetFormFile($"image/{fileName.Split('.')[1]}");
            formData.Add(fileContent, "AttachedFiles", fileName);
        }

        //foreach (var fileId in removeFileIds)
        //{
        //    formData.Add(new StringContent(fileId.ToString()), "RemovedFilesIds");
        //}

        foreach (var categoryId in multiLevelCategoryIds)
        {
            formData.Add(new StringContent(categoryId.ToString()), "MultiLevelCategoryIds");
        }

        return formData;
    }

    private static Dictionary<string, string> GetEssentialDataForQuestion()
    {
        QuestionFormViewModel questionFormViewModel = new()
        {
            //Id = 2,
            FAQ = true,
            CultureCode = "ar",
            Content = "asdasdasd",
        };

        return new Dictionary<string, string>
        {
            //{ nameof(questionFormViewModel.Id) ,questionFormViewModel.Id.ToString() }
            { nameof(questionFormViewModel.FAQ) ,questionFormViewModel.FAQ.ToString() },
            { nameof(questionFormViewModel.CultureCode) ,questionFormViewModel.CultureCode.ToString() },
            { nameof(questionFormViewModel.Content) ,questionFormViewModel.Content.ToString() },
        };
    }

    private static IEnumerable<int> GetMultiLevelCategoryIds() =>
        new List<int> { 2, 3 };

    private static IEnumerable<string> GetFiles() =>
        new List<string>
        {
            "C:\\Users\\Naddem\\Desktop\\files\\3.jpg",
            "C:\\Users\\Naddem\\Desktop\\files\\5.jpg"
        };
    private static IEnumerable<int> GetRemoveFileIds() =>
        new List<int> { 1, 2 };

}

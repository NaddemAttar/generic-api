﻿using IGenericControlPanel.GamificationSystem.Dto.Gamification;
using IGenericControlPanel.SharedKernal.Enums.GamificationSystem;

namespace IGenericControlPanel.IntegrationTests.Fixtures;
public static class GamificationFixture
{
    public static IEnumerable<GamificationDto> GetSomeGamificationSections()
    {
        return new List<GamificationDto>
        {
            new()
            {
                Key = "AddAllergy",
                Value = 2,
                Enable = true,
                GamificationType = GamificationType.NormalUser
            }
        };
    }
    public static GamificationDto GetOneGamification()
    {
        return new GamificationDto
        {
            Key = "key1",
            Value = 12,
            Enable = true,
            GamificationType = GamificationType.Hcp
        };
    }
    public static IEnumerable<object[]> GetNewGamificationSections()
    {
        return new[]
         {
            new object[] {
                new GamificationDto {
                Key = "PublishBlogOnFB",
                Value = 1,
                Enable = true,
                GamificationType = GamificationType.Hcp}
            },
            new object[] {
                new GamificationDto {
                Key = "PublishBlogOnINSTA",
                Value = 1,
                Enable = true,
                GamificationType = GamificationType.Hcp}
            },
            new object[] {
                new GamificationDto {
                Key = "PublisTipOnFB",
                Value = 1,
                Enable = true,
                GamificationType = GamificationType.Hcp}
            },
            new object[] {
                new GamificationDto {
                Key = "PublishQuestionOnFB",
                Value = 1,
                Enable = true,
                GamificationType = GamificationType.Hcp}
            },
            new object[] {
                new GamificationDto {
                Key = "PublishQuestionOnINSTA",
                Value = 1,
                Enable = true,
                GamificationType = GamificationType.Hcp}
            },
            new object[] {
                new GamificationDto {
                Key = "PublishCourseOnFB",
                Value = 1,
                Enable = true,
                GamificationType = GamificationType.Hcp}
            },
            new object[] {
                new GamificationDto {
                Key = "PublishCourseOnINSTA",
                Value = 1,
                Enable = true,
                GamificationType = GamificationType.Hcp}
            },
            new object[] {
                new GamificationDto {
                Key = "PublishProductOnFB",
                Value = 1,
                Enable = true,
                GamificationType = GamificationType.Hcp}
            },
            new object[] {
                new GamificationDto {
                Key = "PublishProductOnINSTA",
                Value = 1,
                Enable = true,
                GamificationType = GamificationType.Hcp}
            },
            new object[] {
                new GamificationDto {
                Key = "PublishLocationScheduleOnFB",
                Value = 1,
                Enable = true,
                GamificationType = GamificationType.Hcp}
            },
            new object[] {
                new GamificationDto {
                Key = "PublishTeamMemberOnFB",
                Value = 1,
                Enable = true,
                GamificationType = GamificationType.Hcp}
            },
            new object[] {
                new GamificationDto {
                Key = "PublishTeamMemberOnINSTA",
                Value = 1,
                Enable = true,
                GamificationType = GamificationType.Hcp}
            },
            new object[] {
                new GamificationDto {
                Key = "PublishFeedBackOnFB",
                Value = 1,
                Enable = true,
                GamificationType = GamificationType.Hcp}
            },
             new object[] {
                new GamificationDto {
                Key = "PublishFeedBackOnINSTA",
                Value = 1,
                Enable = true,
                GamificationType = GamificationType.Hcp}
            },
         };
    }
}

﻿
namespace IGenericControlPanel.IntegrationTests.Fixtures;
public static class ProjectFixture
{
    public static MultipartFormDataContent ProjectFormData()
    {

        var listOfDictionary = new Dictionary<string, string>()
        {
            {"Title", "title"},
            {"Description", "descriptiondescriptiondescription"},
        };

        var keys = new List<string>() { "key1", "key2", "key3" };

        var editions = new List<EditionDto>()
        {
            new()
            {
                Title = "Title",
                Description = "Description",
                Link = "https://www.tutorialsteacher.com/csharp/csharp-dictionary"
            },
            new()
            {
                Title = "Title2",
                Description = "Description2",
                Link = "https://www.tutorialsteacher.com/csharp/csharp-dictionary"
            },
            new()
            {
                Title = "Title3",
                Description = "Description3",
                Link = "https://www.tutorialsteacher.com/csharp/csharp-dictionary"
            },
        };

        var formData = new MultipartFormDataContent();

        foreach (var item in listOfDictionary)
        {
            formData.Add(new StringContent(item.Value), item.Key);
        }

        foreach (var key in keys)
        {
            formData.Add(new StringContent(key), "keys");
        }

        foreach (var (edition, index) in editions.Select((value, i) => (value, i)))
        {
            formData.Add(new StringContent(content: edition.Title),
                $"Editions[{index}].{nameof(edition.Title)}");

            formData.Add(new StringContent(content: edition.Description),
                $"Editions[{index}].{nameof(edition.Description)}");

            formData.Add(new StringContent(content: edition.Link),
                $"Editions[{index}].{nameof(edition.Link)}");
        }

        return formData;
    }
}

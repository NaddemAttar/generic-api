﻿
namespace IGenericControlPanel.IntegrationTests.Fixtures;
public static class BlogFixture
{
    public static MultipartFormDataContent GetDataForActionBlog()
    {
        var formData = new MultipartFormDataContent();

        var files = GetFiles();

        var multiLevelCategoryIds = GetMultiLevelCategoryIds();

        var blogsData = GetEssentialBlogDataInDictionary();

        foreach (var fileName in files)
        {
            var fileContnet = fileName.GetFormFile($"image/{fileName.Split('.')[1]}");
            formData.Add(fileContnet, "AttachedFiles", fileName);
        }

        //foreach (var fileId in RemoveFileIds())
        //{
        //    formData.Add(new StringContent(fileId.ToString()), "RemovedFilesIds");
        //}

        foreach (var blogData in blogsData)
        {
            formData.Add(new StringContent(blogData.Value), blogData.Key);
        }

        foreach (var category in multiLevelCategoryIds)
        {
            formData.Add(new StringContent(category.ToString()), "MultiLevelCategoryIds");
        }

        return formData;
    }

    private static Dictionary<string, string> GetEssentialBlogDataInDictionary()
    {
        var blogViewModel = new BlogFormViewModel()
        {
            //Id = 5,
            Content = "aaaaaaaaaaaaaaaa",
            BlogType = BlogType.Documents,
            CultureCode = "ar",
            Title = "aaaaaaaaaaaaaaaaaaaaa"
        };

        return new Dictionary<string, string>()
        {
            {nameof(blogViewModel.Id), blogViewModel.Id.ToString() },
            {nameof(blogViewModel.Content), blogViewModel.Content },
            {nameof(blogViewModel.CultureCode), blogViewModel.CultureCode },
            {nameof(blogViewModel.BlogType), blogViewModel.BlogType.ToString() },
            {nameof(blogViewModel.Title), blogViewModel.Title },
        };
    }

    private static IEnumerable<string> GetFiles()
    {
        return new List<string>
        {
            "C:\\Users\\Naddem\\Desktop\\files\\5.jpg",
        };
    }

    private static IEnumerable<int> RemoveFileIds()
    {
        return new List<int> { 35, 36, 37, 38 };
    }

    private static IEnumerable<int> GetMultiLevelCategoryIds()
    {
        return new List<int> { 2 };
    }

}

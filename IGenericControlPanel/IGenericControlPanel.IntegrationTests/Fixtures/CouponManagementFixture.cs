﻿
using IGenericControlPanel.GamificationSystem.Dto.Coupon;

namespace IGenericControlPanel.IntegrationTests.Fixtures;
public static class CouponManagementFixture
{
    public static CouponDto GetOneCoupon()
    {
        return new CouponDto
        {
            //Id = 14,
            CouponPeriod = 4,
            Percentage = 20,
            Points = 200
        };
    }
}

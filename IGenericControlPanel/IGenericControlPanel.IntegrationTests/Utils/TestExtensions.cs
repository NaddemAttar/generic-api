﻿namespace IGenericControlPanel.IntegrationTests.Utils;
public static class TestExtensions
{
    public static ByteArrayContent CreateBodyData(this object obj)
    {
        var jsonObject = JsonConvert.SerializeObject(obj);
        var buffer = Encoding.UTF8.GetBytes(jsonObject);
        var byteContent = new ByteArrayContent(buffer);
        byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        return byteContent;
    }

    public static ByteArrayContent GetFormFile(this string fileName, string contentType)
    {
        var fileContent = new ByteArrayContent(File.ReadAllBytes(fileName));
        fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse(contentType); // image/jpg
        return fileContent;
    }
}

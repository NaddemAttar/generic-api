﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Security.Dto.Permissions
{
    public class RoleDto
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
        public IEnumerable<WebContentDto> WebContents { get; set; }
    }
}

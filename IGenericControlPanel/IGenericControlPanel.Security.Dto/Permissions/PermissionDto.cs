﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Security.Dto.Permissions
{
    public class PermissionDto
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public IEnumerable<string> WebContents { get; set; }
    }
}

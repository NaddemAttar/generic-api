﻿using IGenericControlPanel.SharedKernal.Enums.Core;
using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.Security.Dto.User
{
    public class UserFormDto
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime? Birthdate { get; set; }
        public Guid ArachnoitId { get; set; }
        public Gender Gender { get; set; }
        public IFormFile ProfilePhoto { get; set; }
        public string ProfilePhotoUrl { get; set; }
    }
}
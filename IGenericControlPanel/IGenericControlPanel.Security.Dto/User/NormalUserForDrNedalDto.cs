﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Security.Dto.User
{
    public class NormalUserForDrNedalDto : NormalUserDto
    {
        public int DiseasesCount { get; set; }
        public int AllergiesCount { get; set; }
        public int SurgeriesCount { get; set; }
        public int MedicinesCount { get; set; }
        public int MedicalTestsCount { get; set; }
        public List<string> Notes { get; set; }
    }
}

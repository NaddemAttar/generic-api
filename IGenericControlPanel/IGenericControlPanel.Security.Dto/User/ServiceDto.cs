﻿using CraftLab.Core.BoundedContext;

namespace IGenericControlPanel.Security.Dto
{
    public class ServiceDto 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        
    }
}

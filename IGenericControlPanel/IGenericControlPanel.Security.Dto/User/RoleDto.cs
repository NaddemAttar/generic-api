﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Security.Dto.User
{
    public class RoleDto
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Security.Dto.User
{
    public class NormalUserForBladesDto: NormalUserDto
    {
        public PaymentStatus PaymentStatus { get; set; }
        public int CoursesCount { get; set; }
        public int OrdersCount { get; set; }
    }
}

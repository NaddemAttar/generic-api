﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Security.Dto
{
    public class HealthCareProviderSpecialtyDto
    {
        public int HealthCareProviderId { get; set; }
        public string HealthCareProviderName { get; set; }
      
    }
}

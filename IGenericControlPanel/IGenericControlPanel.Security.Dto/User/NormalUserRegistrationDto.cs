﻿using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Security.Dto.User
{
    public class CPUserRegistrationDto : UserFormDto
    {
        public CPUserRegistrationDto()
        {
            HealthCareSpecialtyIds = new List<int>();
            ServicesIds = new List<int>();
        }
        public RoleDto Role { get; set; }
        public List<int> HealthCareSpecialtyIds { get; set; }
        public List<int> ServicesIds { get; set; }
        public HcpType HcpType { get; set; }
    }
}
﻿using CraftLab.Core.BoundedContext;

using IGenericControlPanel.Security.Dto.User;
using IGenericControlPanel.SharedKernal.Enums.Core;
using System.Collections.Generic;
using System.Linq;
namespace IGenericControlPanel.Security.Dto
{
    public class UserDto : IFormDto
    {
        public UserDto()
        {
            HealthCareProviderSpecialties = new List<HealthCareProviderSpecialtyDto>();
            Services = new List<ServiceDto>();
        }
        public int Id { get; set; }
        public bool IsValid { get; set; }
        public string UserName { get; set; }
        public string InTouchPointName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool RememberMe { get; set; }
        public string Password { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public RoleDto Role { get; set; }
        public string RoleName { get; set; }
        public int RoleId { get; set; }
        public ActionOperationType OperationType { get; }
        public string CultureCode { get; set; }
        public IEnumerable<HealthCareProviderSpecialtyDto> HealthCareProviderSpecialties { get; set; }
        public IEnumerable<ServiceDto> Services { get; set; }
        public string ProfilePhotoUrl { get; set; }
        public bool isHealthCareProvider { get; set; }
        public Gender Gender { get; set; }
        public HcpType userType { get; set; }
        public DateTime? Birthdate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Security.Dto.User
{
    public class UserInfoDto
    {
        public int CurrentUserId { get; set; }
        public bool IsUserAdmin { get; set; }
        public bool IsHealthCareProvider { get; set; }
        public bool IsNormalUser { get; set; }
    }
}

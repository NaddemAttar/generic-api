﻿namespace IGenericControlPanel.Security.Dto.User
{
    public class HealthCareProviderDto
    {
        public HealthCareProviderDto()
        {
            HealthCareSpecialtyIds = new List<int>();
        }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<int> HealthCareSpecialtyIds { get; set; }
    }
}

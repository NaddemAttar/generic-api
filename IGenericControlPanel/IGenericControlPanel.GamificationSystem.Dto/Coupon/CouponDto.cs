﻿
namespace IGenericControlPanel.GamificationSystem.Dto.Coupon;
public class CouponDto
{
    public int Id { get; set; }
    public int Points { get; set; }
    public int Percentage { get; set; }
    public int CouponPeriod { get; set; }
}

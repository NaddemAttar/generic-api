﻿
namespace IGenericControlPanel.GamificationSystem.Dto.Coupon;
public class UserCouponDto
{
    public int Id { get; set; }
    public int Percentage { get; set; }
    public int Points { get; set; }
}

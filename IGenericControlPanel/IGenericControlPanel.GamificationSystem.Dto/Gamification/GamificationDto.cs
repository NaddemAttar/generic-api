﻿using IGenericControlPanel.SharedKernal.Enums.GamificationSystem;

namespace IGenericControlPanel.GamificationSystem.Dto.Gamification;
public class GamificationDto
{
    public string Key { get; set; }
    public int Value { get; set; }
    public bool Enable { get; set; }
    public GamificationType GamificationType { get; set; }
}

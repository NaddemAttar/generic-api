﻿
using IGenericControlPanel.Configuration.Dto.Pagination;

namespace IGenericControlPanel.GamificationSystem.Dto.Gamification;
public class FiltersUsersDto : PaginationDto
{
    public bool UsersHavePointsOnly { get; set; }
    public bool IsAscendingOrder { get; set; }

}

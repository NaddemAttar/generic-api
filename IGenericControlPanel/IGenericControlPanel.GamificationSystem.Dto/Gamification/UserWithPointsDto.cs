﻿
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.GamificationSystem.Dto.Gamification;
public class UserWithPointsDto
{
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string UserName { get; set; }
    public Gender Gender { get; set; }
    public string PersonalPhotoUrl { get; set; }
    public int Points { get; set; }
}

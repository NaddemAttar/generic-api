﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Communications.Dto.Feedback;
using IGenericControlPanel.Communications.IData.Interfaces;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.Model.Communication;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using IGenericControlPanel.SharedKernal.Utils;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Communications.Data.Repositories
{
    public class FeedbackRepository : BasicRepository<CPDbContext, FeedBackSet>, IFeedbackRepository
    {
        #region Properties & Constructor
        private IUserRepository UserRepository { get; }
        private IGamificationMovementsRepository _gamificationMovementRepo { get; }
        public FeedbackRepository(
            CPDbContext context,
            IUserRepository userRepository,
            IGamificationMovementsRepository gamificationMovementRepo) : base(context)
        {
            UserRepository = userRepository;
            _gamificationMovementRepo = gamificationMovementRepo;
        }
        #endregion

        #region Action Section
        public async Task<OperationResult<GenericOperationResult, FeedbackDto>> ActionFeedback(FeedbackDto formDto)
        {
            var result = new OperationResult<GenericOperationResult,
                FeedbackDto>(GenericOperationResult.ValidationError);
            try
            {
                var Entity = Context.FeedBack.Where(entity => entity.Id == formDto.Id)
                    .Include(feed => feed.Service)
                    .SingleOrDefault();

                if (Entity == null)
                {
                    Entity = new FeedBackSet()
                    {
                        ClientName = formDto.ClientName,
                        Title = formDto.Title,
                        ImageUrl = formDto.ImageUrl,
                        ServiceId = formDto.ServiceId,
                        Description = formDto.Description,
                        YouTubeLink = formDto.YouTubeLink
                    };
                    await Context.AddAsync(Entity);
                    await Context.SaveChangesAsync();


                    var savePointsResult = await _gamificationMovementRepo
                        .SavePoints(new List<string>
                        {
                            GamificationNames.AddFeedback
                        });

                    if (!savePointsResult.IsSuccess)
                    {
                        return result.AddError(savePointsResult.ErrorMessages)
                            .UpdateResultStatus(GenericOperationResult.Failed);
                    }

                    return result.UpdateResultData(formDto)
                        .UpdateResultStatus(GenericOperationResult.Success);
                }
                else
                {
                    Entity.ClientName = formDto.ClientName;
                    Entity.Title = formDto.Title;
                    Entity.ImageUrl = formDto.ImageUrl ?? Entity.ImageUrl;
                    Entity.ServiceId = formDto.ServiceId;
                    Entity.Description = formDto.Description;
                    Entity.YouTubeLink = formDto.YouTubeLink;

                    Context.Update(Entity);
                    await Context.SaveChangesAsync();

                }
                var data = new FeedbackDto()
                {
                    Id = Entity.Id,
                    ServiceId = Entity.ServiceId,
                    ClientName = Entity.ClientName,
                    Description = Entity.Description,
                    ImageUrl = Entity.ImageUrl,
                    ServiceName = Entity.Service?.Name,
                    Title = Entity.Title,
                    YouTubeLink = Entity.YouTubeLink
                };

                return result.UpdateResultData(data)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed)
                    .AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Get Section
        public async Task<OperationResult<GenericOperationResult, IEnumerable<FeedbackDto>>> GetFeedbacks(int ServiceId, int pageSize = 6, int pageNumber = 0)
        {
            var result = new OperationResult<GenericOperationResult, IEnumerable<FeedbackDto>>(GenericOperationResult.ValidationError);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var data = ValidEntities
                    .Where(entity => userInfo.CurrentUserId == 0
                    || userInfo.IsUserAdmin
                    || (userInfo.IsHealthCareProvider
                    ? userInfo.CurrentUserId == entity.CreatedBy
                    : true)
                    && ServiceId == 0 || entity.ServiceId == ServiceId)
                    .Skip(pageSize * pageNumber)
                    .Take(pageSize)
                    .Select(entity => new FeedbackDto()
                    {
                        Id = entity.Id,
                        ClientName = entity.ClientName,
                        Description = entity.Description,
                        ImageUrl = entity.ImageUrl,
                        ServiceId = entity.ServiceId,
                        Title = entity.Title,
                        YouTubeLink = entity.YouTubeLink,
                        ServiceName = entity.Service.Name
                    }).ToList();
                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);

            }
        }
        public OperationResult<GenericOperationResult, FeedbackDto> GetFeedback(int FeedbackId)
        {
            var result = new OperationResult<GenericOperationResult, FeedbackDto>(GenericOperationResult.ValidationError);
            try
            {
                var data = ValidEntities.Where(entity => entity.Id == FeedbackId).Select(entity => new FeedbackDto()
                {
                    Id = entity.Id,
                    ClientName = entity.ClientName,
                    Description = entity.Description,
                    ImageUrl = entity.ImageUrl,
                    ServiceId = entity.ServiceId,
                    Title = entity.Title,
                    YouTubeLink = entity.YouTubeLink,
                    ServiceName = entity.Service.Name
                }).SingleOrDefault();
                return data is null
                    ? result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.FeedBackNotFound)
                    : result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);

            }
        }
        #endregion

        #region Remove Section
        public async Task<OperationResult<GenericOperationResult>> RemoveFeedback(int FeedbackId)
        {
            var result = new OperationResult<GenericOperationResult>(GenericOperationResult.ValidationError);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var Entity = ValidEntities
                    .Where(entity => entity.Id == FeedbackId
                    && (userInfo.IsUserAdmin
                    || userInfo.CurrentUserId == entity.CreatedBy))
                    .SingleOrDefault();
                if (Entity == null)
                {
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.FeedBackNotFound);
                }
                Context.Remove(Entity);
                Context.SaveChanges();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion
    }
}

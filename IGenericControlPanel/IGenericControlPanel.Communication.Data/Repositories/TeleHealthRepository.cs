﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Communications.Dto.TeleHealth;
using IGenericControlPanel.Communications.IData.Interfaces;
using IGenericControlPanel.Core.Dto.HealthCareProvider;
using IGenericControlPanel.Model.Communication;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace IGenericControlPanel.Communications.Data.Repositories
{
    public class TeleHealthRepository : BasicRepository<CPDbContext, BookingSet>, ITeleHealthRpository
    {
        private readonly IUserRepository UserRepository;
        private readonly IConfiguration configuration;
        private readonly UserManager<CPUser> UserManager;
        public TeleHealthRepository(
            CPDbContext dbContext, IUserRepository userRepository, UserManager<CPUser> UserManager, IConfiguration configuration) : base(dbContext)
        {
            UserRepository = userRepository;
            this.UserManager = UserManager;
            this.configuration = configuration;
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<SessionTypeDto>>> GetHealtCareProviderSessionTypesAsync(int? serviceId, int? HealthCareProviderId)
        {
            OperationResult<GenericOperationResult, IEnumerable<SessionTypeDto>> result = new(GenericOperationResult.Failed);

            var userInfo = await UserRepository.GetCurrentUserInfo();
            try
            {
                List<SessionTypeDto> data = Context.SessionTypes
                    .Where(sessType => (userInfo.CurrentUserId == 0
                    || userInfo.IsUserAdmin
                    || (userInfo.IsHealthCareProvider ? userInfo.CurrentUserId == sessType.CreatedBy : !userInfo.IsHealthCareProvider))
                    && (serviceId == null
                    || sessType.UserServiceSession.Any(uss => uss.HealthCareProviderService.ServiceId == serviceId.Value))
                    && (HealthCareProviderId == null
                    || sessType.UserServiceSession.Any(uss => uss.HealthCareProviderService.CPUserId == HealthCareProviderId.Value))
                    && sessType.IsValid)
                    .Select(sessType => new SessionTypeDto()
                    {
                        Name = sessType.Name,
                        SessionTypeId = sessType.Id,
                    }).ToList();
                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(data);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, BookingSettingsDto>> SetHealthcareProviderBookingInfoAsync(BookingSettingsDto bookingSettingsDto)
        {
            OperationResult<GenericOperationResult, BookingSettingsDto> result = new(GenericOperationResult.Failed);

            try
            {
                Model.Configuration.Setting StartOfPatientReception = await Context.Settings
                    .Where(entity => entity.Name == "StartOfPatientReception")
                    .SingleOrDefaultAsync();

                Model.Configuration.Setting EndOfPatientReception = await Context.Settings
                   .Where(entity => entity.Name == "EndOfPatientReception")
                   .SingleOrDefaultAsync();

                StartOfPatientReception.Value = bookingSettingsDto.StartOfPatientReception.ToString();
                EndOfPatientReception.Value = bookingSettingsDto.EndOfPatientReception.ToString();

                await Context.SaveChangesAsync();

                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(bookingSettingsDto);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, bool>> SetHealthCareProviderSessionTypeAsync(
            SessionTypeDto sessionType)
        {
            OperationResult<GenericOperationResult, bool> result = new(GenericOperationResult.Failed);
            try
            {
                if (sessionType.SessionTypeId == 0)
                {
                    SessionType hcpSessionTypeEntity = new()
                    {
                        //Duration = sessionType.Duration,
                        Name = sessionType.Name,
                        //Price = sessionType.Price,
                        //ServiceId = sessionType.ServiceId,
                        CreatedBy = (await UserRepository.GetCurrentUserInfo()).CurrentUserId
                    };
                    await Context.SessionTypes.AddAsync(hcpSessionTypeEntity);
                }
                else
                {
                    SessionType hcpSessionTypeEntity = Context.SessionTypes
                        .Where(hcpSession => hcpSession.Id == sessionType.SessionTypeId)
                        .SingleOrDefault();

                    //hcpSessionTypeEntity.Duration = sessionType.Duration;
                    hcpSessionTypeEntity.Name = sessionType.Name;
                    //hcpSessionTypeEntity.ServiceId = sessionType.ServiceId;
                    //hcpSessionTypeEntity.Price = sessionType.Price;

                }
                await Context.SaveChangesAsync();

                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(true);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, bool>> RemoveSessionAsync(int SessionId)
        {
            OperationResult<GenericOperationResult, bool> result = new(GenericOperationResult.Failed);

            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                SessionType sessionEntity = await Context.SessionTypes
                    .Where(session => session.Id == SessionId && session.IsValid
                    && (userInfo.IsUserAdmin || userInfo.CurrentUserId == session.CreatedBy))
                    .SingleOrDefaultAsync();

                if (sessionEntity == null)
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.SessionTypeNotFound);

                Context.Remove(sessionEntity);
                await Context.SaveChangesAsync();

                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(true);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, bool>> RemoveBookingAsync(int BookingId, string Token)
        {
            OperationResult<GenericOperationResult, bool> result = new(GenericOperationResult.Failed);

            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var Booking = Context.Bookings
                    .Where(b => b.Id == BookingId && b.IsValid && b.ConfirmToken == Token)
                    .SingleOrDefault();

                if (Booking == null)
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.BookingNotFound);

                Context.Remove(Booking);
                await Context.SaveChangesAsync();

                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(true);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, BookingSettingsDto>> GetbookingSettingsAsync()
        {
            OperationResult<GenericOperationResult, BookingSettingsDto> result = new(GenericOperationResult.Failed);

            try
            {
                Model.Configuration.Setting StartOfPatientReceptionEntity = await Context.Settings
                    .Where(entity => entity.Name == "StartOfPatientReception")
                    .SingleOrDefaultAsync();

                Model.Configuration.Setting EndOfPatientReceptionEntity = await Context.Settings
                    .Where(entity => entity.Name == "EndOfPatientReception")
                    .SingleOrDefaultAsync();

                BookingSettingsDto bookingSettings = new()
                {
                    StartOfPatientReception = int.Parse(StartOfPatientReceptionEntity.Value),
                    EndOfPatientReception = int.Parse(EndOfPatientReceptionEntity.Value)
                };

                return result.UpdateResultData(bookingSettings).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, BookingDto>> BookAppointmentAsync(BookingDto bookingDto)
        {
            OperationResult<GenericOperationResult, BookingDto> result = new(GenericOperationResult.Failed);
            try
            {
                var userServiceSession = await Context.UserServiceSession
                    .Include(uss => uss.HealthCareProviderService)
                    .FirstOrDefaultAsync(ues => ues.Id == bookingDto.UserServiceSessionId);
                if (userServiceSession == null)
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.SessionNotFound);

                //var totalBooking = await Context.Bookings
                //    .Where(b => b.UserServiceSessionId == bookingDto.UserServiceSessionId && b.UserServiceSession.HealthCareProviderService.EnterpriseId == bookingDto.EnterpriseId)
                //    .GroupBy(b => b.CPUserId)
                //    .Select(s => new { CPUserId = s.Key, BookingsCount = s.Count() })
                //    .OrderByDescending(tb => tb.BookingsCount)
                //    .ToListAsync();

                var users = await Context.Users
                    .Include(u => u.HcpBookings)
                    .Where(u => u.UserServices.Any(us => us.EnterpriseId == userServiceSession.HealthCareProviderService.CPUserId && us.ServiceId == userServiceSession.HealthCareProviderService.ServiceId))
                    .ToListAsync();

                var minCpUserId = users[0].Id;
                var minCountOfBook = users[0].HcpBookings.Count();
                foreach (var user in users)
                    if (user.HcpBookings.Count < minCountOfBook)
                        minCpUserId = user.Id;

                BookingSet bookingentity = new()
                {
                    UserServiceSessionId = bookingDto.UserServiceSessionId,
                    Appointment = bookingDto.AppointmentStart,
                    DurationInMinutes = userServiceSession.Duration,
                    Birthdate = bookingDto.Birthdate,
                    Email = bookingDto.Email,
                    FirstName = bookingDto.FirstName,
                    LastName = bookingDto.LastName,
                    Gender = bookingDto.Gender,
                    PhoneNumber = bookingDto.PhoneNumber,
                    IsConfirmed = bookingDto.IsConfirm,
                    IsValid = true,
                    ConfirmToken = bookingDto.Token,
                    CPUserId = minCpUserId,
                    UserId = (await UserRepository.GetCurrentUserInfo()).CurrentUserId,
                };

                await Context.Bookings.AddAsync(bookingentity);
                await Context.SaveChangesAsync();
                bookingDto.Id = bookingentity.Id;
                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(bookingDto);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, bool>> ConfirmingBookingAsync(int BookingId, string Token)
        {
            OperationResult<GenericOperationResult, bool> result = new(GenericOperationResult.Failed);
            try
            {
                var Booking = await Context.Bookings.FirstOrDefaultAsync(b => b.Id == BookingId && b.ConfirmToken == Token && b.IsValid);
                if (Booking != null)
                {
                    Booking.IsConfirmed = true;
                    await Context.SaveChangesAsync();
                    return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(true);
                }
                else
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.BookingNotFound);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, List<DateTime>>> GetAvailableAppointmentsAsync(
            DateTime Date, int UserServiceSessionId, int? HealthCareProviderId, int? EnterpriseId)
        {
            OperationResult<GenericOperationResult, List<DateTime>> result = new(GenericOperationResult.ValidationError);
            var availableAppointmentsType = configuration.GetSection("AvailableAppointmentsKinds").Value;
            //var dictionary = new Dictionary<int, List<DateTime>>();
            var frequencyDictionary = new Dictionary<DateTime, int>();
            // SessionType SessionTypeModel = await Context.SessionTypes
            //.Include(st => st.User).ThenInclude(u => u.UsersEnterprise)
            //.Include(st => st.Service)
            //.Where(sessionT => sessionT.Id == SessionTypeId)
            //.SingleOrDefaultAsync();

            // here  we get the sessionInfo and frome it i know which enterprise produce this service and i know the service.
            var userServiceSession = await Context.UserServiceSession
                .Include(uss => uss.HealthCareProviderService)
                .FirstOrDefaultAsync(ues => ues.Id == UserServiceSessionId);
            if (userServiceSession == null)
                return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.SessionNotFound);

            // here we get users who are working in this enterprise and produce this service(we get it from the session) and get with thire OpeningHours.
            var users = await Context.Users
                .Include(u => u.OpeningHours.Where(op => op.HealthCareProviderEnterprise.EnterpriseId == userServiceSession.HealthCareProviderService.CPUserId && op.Day == Date.DayOfWeek && op.IsWorkingDay))
                //.Include(u => u.HcpBookings.Where(b => b.UserServiceSession.HealthCareProviderService.EnterpriseId == userServiceSession.HealthCareProviderService.CPUserId))
                .Where(u => u.UserServices.Any(hcps => hcps.EnterpriseId == userServiceSession.HealthCareProviderService.CPUserId && hcps.ServiceId == userServiceSession.HealthCareProviderService.ServiceId))
                .ToListAsync();

            //var minHour = users[0].OpeningHours.ElementAt(0).From;
            //var maxHour = users[0].OpeningHours.ElementAt(0).To;
            //for (int i = 0; i < users.Count; i++)
            //{
            //    minHour = users[i].OpeningHours.ElementAt(0).From < minHour ? users[i].OpeningHours.ElementAt(i).From : minHour;
            //    maxHour = users[i].OpeningHours.ElementAt(0).To > maxHour ? users[i].OpeningHours.ElementAt(i).To : maxHour;
            //}

            DateTime startTime;
            DateTime endtime;
            List<DateTime> AvailableAppointments = new();
            foreach (var user in users)
            {
                startTime = Date.Add(user.OpeningHours.ElementAt(0).From);
                endtime = Date.Add(user.OpeningHours.ElementAt(0).To);

                var DBAppointments = await Context.Bookings
                    .Where(b => b.IsValid && b.Appointment >= startTime && b.Appointment <= endtime
                    && b.CPUserId == user.Id)
                    .OrderBy(booking => booking.Appointment)
                    .ToListAsync();

                DateTime testCase = startTime;

                int i = 0;
                if (DBAppointments.Count == 0)
                {
                    while (testCase.AddMinutes(userServiceSession.Duration) <= endtime)
                    {
                        frequencyDictionary[testCase]++;
                        //AvailableAppointments.Add(testCase);
                        testCase = testCase.AddMinutes(userServiceSession.Duration);
                    }
                }
                else
                {
                    while (testCase.AddMinutes(userServiceSession.Duration) <= endtime)
                    {
                        if (i < DBAppointments.Count - 1 && DBAppointments[i].Appointment.AddMinutes(DBAppointments[i].DurationInMinutes) == DBAppointments[i + 1].Appointment)
                        {
                            i++;
                            testCase = DBAppointments[i].Appointment.AddMinutes(DBAppointments[i].DurationInMinutes);
                        }
                        else if (testCase.AddMinutes(userServiceSession.Duration) <= DBAppointments[i].Appointment)
                        {
                            frequencyDictionary[testCase]++;
                            //AvailableAppointments.Add(testCase);
                            testCase = testCase.AddMinutes(userServiceSession.Duration);
                        }
                        else if (i == DBAppointments.Count - 1 && testCase >= DBAppointments[i].Appointment.AddMinutes(DBAppointments[i].DurationInMinutes))
                        {
                            frequencyDictionary[testCase]++;
                            //AvailableAppointments.Add(testCase);
                            testCase = testCase.AddMinutes(userServiceSession.Duration);
                        }
                        else if (testCase >= DBAppointments[i].Appointment.AddMinutes(DBAppointments[i].DurationInMinutes))
                        {
                            if (testCase.AddMinutes(userServiceSession.Duration) <= DBAppointments[i + 1].Appointment)
                            {
                                frequencyDictionary[testCase]++;
                                //AvailableAppointments.Add(testCase);
                                testCase = testCase.AddMinutes(userServiceSession.Duration);
                            }
                            i++;
                        }
                        else
                        {
                            testCase = testCase.AddMinutes(userServiceSession.Duration);
                        }
                    }
                }
                foreach (var freq in frequencyDictionary)
                    if (freq.Value > 0)
                        AvailableAppointments.Add(freq.Key);

                //dictionary[user.Id] = AvailableAppointments;
            }
            return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(AvailableAppointments);

            //if (EnterpriseId != null && HealthCareProviderId != null)
            //{
            //    var hcpUserInEnterprise = await Context.HealthCareProviderEnterprise
            //     .FirstOrDefaultAsync(hcpe => hcpe.EnterpriseId == EnterpriseId.Value && hcpe.UserId == HealthCareProviderId.Value);

            //    if (hcpUserInEnterprise == null)
            //        throw new Exception();

            //    int SessionDuration = SessionTypeModel.Duration;
            //    var startOfPatientReception = await GetReceptionTimeAsync(true, Date, HealthCareProviderId.Value, EnterpriseId);
            //    var endOfPatientReception = await GetReceptionTimeAsync(false, Date, HealthCareProviderId.Value, EnterpriseId);

            //    //DateTime startTime = Date.Add(startOfPatientReception);
            //    //DateTime endtime = Date.Add(endOfPatientReception);

            //    var DBAppointments = await Context.Bookings
            //        .Where(b => b.IsValid && b.Appointment >= startTime && b.Appointment <= endtime
            //        && b.HealthCareProviderEnterpriseId == hcpUserInEnterprise.Id)
            //        .OrderBy(booking => booking.Appointment)
            //        .ToListAsync();

            //    DateTime testCase = startTime;
            //    List<DateTime> AvailableAppointments = new();
            //    int i = 0;
            //    if (DBAppointments.Count == 0)
            //    {
            //        while (testCase.AddMinutes(SessionDuration) <= endtime)
            //        {
            //            AvailableAppointments.Add(testCase);
            //            testCase = testCase.AddMinutes(SessionDuration);
            //        }
            //    }
            //    else
            //    {
            //        while (testCase.AddMinutes(SessionDuration) <= endtime)
            //        {
            //            if (i < DBAppointments.Count - 1 && DBAppointments[i].Appointment.AddMinutes(DBAppointments[i].DurationInMinutes) == DBAppointments[i + 1].Appointment)
            //            {
            //                i++;
            //                testCase = DBAppointments[i].Appointment.AddMinutes(DBAppointments[i].DurationInMinutes);
            //            }
            //            else if (testCase.AddMinutes(SessionDuration) <= DBAppointments[i].Appointment)
            //            {
            //                AvailableAppointments.Add(testCase);
            //                testCase = testCase.AddMinutes(SessionDuration);
            //            }
            //            else if (i == DBAppointments.Count - 1 && testCase >= DBAppointments[i].Appointment.AddMinutes(DBAppointments[i].DurationInMinutes))
            //            {
            //                AvailableAppointments.Add(testCase);
            //                testCase = testCase.AddMinutes(SessionDuration);
            //            }
            //            else if (testCase >= DBAppointments[i].Appointment.AddMinutes(DBAppointments[i].DurationInMinutes))
            //            {
            //                if (testCase.AddMinutes(SessionDuration) <= DBAppointments[i + 1].Appointment)
            //                {
            //                    AvailableAppointments.Add(testCase);
            //                    testCase = testCase.AddMinutes(SessionDuration);
            //                }
            //                i++;
            //            }
            //            else
            //            {
            //                testCase = testCase.AddMinutes(SessionDuration);
            //            }
            //        }
            //    }
            //    dictionary[HealthCareProviderId.Value] = AvailableAppointments;
            //    return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(dictionary);
            //}
            //else if (EnterpriseId != null && HealthCareProviderId == null)
            //{
            //    int SessionDuration = SessionTypeModel.Duration;

            //    //get all Hcps in this Enterprise.
            //    var hcpUsersInEnterprise = await Context.Users
            //        .Where(u => u.UsersEnterprise.Any(hcpe => hcpe.EnterpriseId == EnterpriseId.Value
            //        && hcpe.HealthCareProdviderEnterpriseService.Any(hcpes => hcpes.ServiceId == SessionTypeModel.ServiceId)))
            //        .Include(u => u.UsersEnterprise)
            //        .ToListAsync();

            //    foreach (var hcpUser in hcpUsersInEnterprise)
            //    {
            //        var startOfPatientReception = await GetReceptionTimeAsync(true, Date, hcpUser.Id, EnterpriseId);
            //        var endOfPatientReception = await GetReceptionTimeAsync(false, Date, hcpUser.Id, EnterpriseId);

            //        DateTime startTime = Date.Add(startOfPatientReception);
            //        DateTime endtime = Date.Add(endOfPatientReception);

            //        var DBAppointments = await Context.Bookings
            //        .Where(b => b.IsValid && b.Appointment >= startTime && b.Appointment <= endtime
            //        && b.HealthCareProviderEnterpriseId == hcpUser.UsersEnterprise.ElementAt(0).Id)
            //        .OrderBy(booking => booking.Appointment)
            //        .ToListAsync();

            //        DateTime testCase = startTime;
            //        List<DateTime> AvailableAppointments = new();
            //        int i = 0;
            //        if (DBAppointments.Count == 0)
            //        {
            //            while (testCase.AddMinutes(SessionDuration) <= endtime)
            //            {
            //                AvailableAppointments.Add(testCase);
            //                testCase = testCase.AddMinutes(SessionDuration);
            //            }
            //        }
            //        else
            //        {
            //            while (testCase.AddMinutes(SessionDuration) <= endtime)
            //            {
            //                if (i < DBAppointments.Count - 1 && DBAppointments[i].Appointment.AddMinutes(DBAppointments[i].DurationInMinutes) == DBAppointments[i + 1].Appointment)
            //                {
            //                    i++;
            //                    testCase = DBAppointments[i].Appointment.AddMinutes(DBAppointments[i].DurationInMinutes);
            //                }
            //                else if (testCase.AddMinutes(SessionDuration) <= DBAppointments[i].Appointment)
            //                {
            //                    AvailableAppointments.Add(testCase);
            //                    testCase = testCase.AddMinutes(SessionDuration);
            //                }
            //                else if (i == DBAppointments.Count - 1 && testCase >= DBAppointments[i].Appointment.AddMinutes(DBAppointments[i].DurationInMinutes))
            //                {
            //                    AvailableAppointments.Add(testCase);
            //                    testCase = testCase.AddMinutes(SessionDuration);
            //                }
            //                else if (testCase >= DBAppointments[i].Appointment.AddMinutes(DBAppointments[i].DurationInMinutes))
            //                {
            //                    if (testCase.AddMinutes(SessionDuration) <= DBAppointments[i + 1].Appointment)
            //                    {
            //                        AvailableAppointments.Add(testCase);
            //                        testCase = testCase.AddMinutes(SessionDuration);
            //                    }
            //                    i++;
            //                }
            //                else
            //                {
            //                    testCase = testCase.AddMinutes(SessionDuration);
            //                }
            //            }
            //        }
            //        dictionary[hcpUser.Id] = AvailableAppointments;
            //    }
            //    return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(dictionary);
            //}

            //else
            //{
            //    var user = (await UserManager.GetUsersInRoleAsync(nameof(Role.Admin))).FirstOrDefault();

            //    if (HealthCareProviderId == null && EnterpriseId == null)
            //        HealthCareProviderId = user.Id;

            //    int SessionDuration = SessionTypeModel.Duration;
            //    dictionary = new Dictionary<int, List<DateTime>>();
            //    var startOfPatientReception = await GetReceptionTimeAsync(true, Date, HealthCareProviderId.Value, EnterpriseId);
            //    var endOfPatientReception = await GetReceptionTimeAsync(false, Date, HealthCareProviderId.Value, EnterpriseId);

            //    DateTime startTime = Date.Add(startOfPatientReception);
            //    DateTime endtime = Date.Add(endOfPatientReception);

            //    List<BookingSet> DBAppointments = await Context.Bookings
            //    .Where(booking => booking.IsValid && booking.Appointment >= startTime && booking.Appointment <= endtime
            //    && ((HealthCareProviderId != null && EnterpriseId == null) ?
            //    (booking.SessionType.UserId == HealthCareProviderId)
            //    : true))
            //    .OrderBy(booking => booking.Appointment)
            //    .ToListAsync();

            //    DateTime testCase = startTime;
            //    List<DateTime> AvailableAppointments = new();
            //    int i = 0;
            //    if (DBAppointments.Count == 0)
            //    {
            //        while (testCase.AddMinutes(SessionDuration) <= endtime)
            //        {
            //            AvailableAppointments.Add(testCase);
            //            testCase = testCase.AddMinutes(SessionDuration);
            //        }
            //    }
            //    else
            //    {
            //        while (testCase.AddMinutes(SessionDuration) <= endtime)
            //        {
            //            if (i < DBAppointments.Count - 1 && DBAppointments[i].Appointment.AddMinutes(DBAppointments[i].DurationInMinutes) == DBAppointments[i + 1].Appointment)
            //            {
            //                i++;
            //                testCase = DBAppointments[i].Appointment.AddMinutes(DBAppointments[i].DurationInMinutes);
            //            }
            //            else if (testCase.AddMinutes(SessionDuration) <= DBAppointments[i].Appointment)
            //            {
            //                AvailableAppointments.Add(testCase);
            //                testCase = testCase.AddMinutes(SessionDuration);
            //            }
            //            else if (i == DBAppointments.Count - 1 && testCase >= DBAppointments[i].Appointment.AddMinutes(DBAppointments[i].DurationInMinutes))
            //            {
            //                AvailableAppointments.Add(testCase);
            //                testCase = testCase.AddMinutes(SessionDuration);
            //            }
            //            else if (testCase >= DBAppointments[i].Appointment.AddMinutes(DBAppointments[i].DurationInMinutes))
            //            {
            //                if (testCase.AddMinutes(SessionDuration) <= DBAppointments[i + 1].Appointment)
            //                {
            //                    AvailableAppointments.Add(testCase);
            //                    testCase = testCase.AddMinutes(SessionDuration);
            //                }
            //                i++;
            //            }
            //            else
            //            {
            //                testCase = testCase.AddMinutes(SessionDuration);
            //            }
            //        }
            //    }
            //    dictionary[HealthCareProviderId.Value] = AvailableAppointments;
            //    return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(dictionary);
            //}
        }

        public async Task<TimeSpan> GetReceptionTimeAsync(bool isStartTime, DateTime date, int HealthCareProviderId, int? EnterpriseId)
        {
            var hcpUserInEnterprise = await Context.HealthCareProviderEnterprise
                 .FirstOrDefaultAsync(hcpe => (EnterpriseId != null) ?
                 (hcpe.EnterpriseId == EnterpriseId.Value && hcpe.UserId == HealthCareProviderId)
                 : false);

            var result = await Context.OpenningHoures
              .Where(entity => entity.IsValid
              && entity.IsWorkingDay
              && (hcpUserInEnterprise != null) ? entity.HealthCareProviderEnterpriseId == hcpUserInEnterprise.Id : entity.UserId == HealthCareProviderId
              && entity.Day == date.DayOfWeek)
              .Select(entity => isStartTime ? entity.From : entity.To)
              .SingleOrDefaultAsync();

            return result;
        }

        public async Task<OperationResult<GenericOperationResult, IEnumerable<BookingDto>>> GetBookingsAsync(bool MyBookings)
        {
            OperationResult<GenericOperationResult, IEnumerable<BookingDto>> result = new(GenericOperationResult.Failed);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();
                var user = UserRepository.GetUser(userInfo.CurrentUserId);
                IQueryable<BookingDto> data = Context.Bookings
                    .Where(booking => booking.IsValid
                    && (MyBookings ? booking.Email == user.Email : (userInfo.IsUserAdmin
                    || booking.CPUserId == userInfo.CurrentUserId
                    )))
                    .Select(booking => new BookingDto()
                    {
                        Id = booking.Id,
                        AppointmentStart = booking.Appointment,
                        AppointmentEnd = booking.Appointment.AddMinutes(booking.UserServiceSession.Duration),
                        SessionName = booking.UserServiceSession.SessionType.Name,
                        Birthdate = booking.Birthdate,
                        Email = booking.Email,
                        FirstName = booking.FirstName,
                        LastName = booking.LastName,
                        Gender = booking.Gender,
                        PhoneNumber = booking.PhoneNumber,
                        UserServiceSessionId = booking.UserServiceSessionId,
                        IsConfirm = booking.IsConfirmed,
                        ServiceName = booking.UserServiceSession.HealthCareProviderService.Service.Name,
                        HealthCareProviderName = booking.CpUser.FirstName + " " + booking.CpUser.LastName
                    });

                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult>> RemoveAppointmentAsync(int AppointmentId)
        {
            OperationResult<GenericOperationResult> operationResult = new(
                GenericOperationResult.ValidationError);
            try
            {
                var Entity = Context.Bookings
                    .Where(booking => booking.Id == AppointmentId && booking.IsValid)
                    .SingleOrDefault();

                if (Entity == null)
                {
                    return operationResult
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                Context.Remove(Entity);
                await Context.SaveChangesAsync();

                return operationResult
                     .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return operationResult
                                    .UpdateResultStatus(GenericOperationResult.Failed)
                                    .AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult>> AddServicesForHcpInEnterpriseAsync(int userId, List<AddServicesForHcpDto> services)
        {
            var transaction = await Context.Database.BeginTransactionAsync();
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.Failed);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                //here check if the user exists.
                var userExist = await Context.Users.AnyAsync(u => u.Id == userId);
                if (!userExist)
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.UserNotExist);

                //here check if the user exists in this enterprise(userInfo.CurrentId).
                var userEnterprise = await Context.HealthCareProviderEnterprise
                    .FirstOrDefaultAsync(hcpe => hcpe.EnterpriseId == userInfo.CurrentUserId && hcpe.UserId == userId);
                if (userEnterprise == null)
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.UserNotFoundInEnterprise);

                foreach (var service in services)
                {
                    //here check if the service exists.
                    var serviceExist = await Context.Services.AnyAsync(s => s.Id == service.ServiceId);
                    if (!serviceExist)
                        return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.ServiceNotExist);

                    //here we add the service for the user in this(userInfo.CurrentId) enterprise.
                    var healthCareProviderServiceModel = new HealthCareProviderService()
                    {
                        ServiceId = service.ServiceId,
                        CreatedBy = userInfo.CurrentUserId,
                        CPUserId = userId,
                        EnterpriseId = userInfo.CurrentUserId,
                        IsValid = true,
                        LastUpdatedBy = userInfo.CurrentUserId,
                        LastUpdateDate = DateTime.Now,
                        CreationDate = DateTime.Now
                    };
                    await Context.HealthCareProviderServices.AddAsync(healthCareProviderServiceModel);
                    await Context.SaveChangesAsync();
                    foreach (var session in service.SessionTypes)
                    {
                        await Context.UserServiceSession.AddAsync(new UserServiceSession
                        {
                            Duration = session.Duration,
                            SessionTypeId = session.SessionTypeId,
                            Price = session.Price,
                            HealthCareProviderServiceId = healthCareProviderServiceModel.Id
                        });
                    }
                }
                await Context.SaveChangesAsync();
                await transaction.CommitAsync();
                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                await transaction.RollbackAsync();
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
    }
}
﻿using System;

using IGenericControlPanel.Communications.Dto;
using IGenericControlPanel.Communications.IData;
using IGenericControlPanel.Model.Communication;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Repository;

namespace IGenericControlPanel.Communications.Data.Repositories
{
    public class ContactRepository : BasicRepository<CPDbContext, ContactMessage>, IContactRepository
    {
        public ContactRepository(CPDbContext dbContext) : base(dbContext)
        {
        }

        public bool SubmitContactForm(ContactFormDto contactForm)
        {
            try
            {
                ContactMessage contactFormEntity = FillBaseContactForm(contactForm);
                _ = Context.Add(contactFormEntity);
                _ = Context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private ContactMessage FillBaseContactForm(ContactFormDto contactForm)
        {
            ContactMessage contactFormEntity = new()
            {
                FullName = contactForm.FullName,
                Email = contactForm.Email,
                Address = contactForm.Address,
                PhoneNumber = contactForm.PhoneNumber,
                Message = contactForm.Message,
                Seen = false,
                Subject = contactForm.Subject,
                ContactMessageType = ContactMessageType.Normal,
            };
            return contactFormEntity;
        }
    }
}

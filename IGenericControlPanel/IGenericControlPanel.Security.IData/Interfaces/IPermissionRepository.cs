﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Security.Dto.Permissions;

namespace IGenericControlPanel.Security.IData.Interfaces
{
    public interface IPermissionRepository
    {
        IEnumerable<string> GetSideBarContent();
        IEnumerable<string> GetWebContent(string controllerName);
        IEnumerable<string> GetWebContent(string controllerName,string actionName);
        IEnumerable<RoleDto> GetAllPermission();
        Task<bool> RemovePermission(int roleId);
        IEnumerable<int> GetWebContents(int roleId);
        IEnumerable<string> GetWebContentsNames(int roleId);
        IEnumerable<WebContentDto> GetAllWebContents();
        IEnumerable<WebContentDto> GetAllWebContents(int RoleId);
        OperationResult<GenericOperationResult, IEnumerable<WebContentDto>> ActionPermission(IEnumerable<WebContentDto> webContents, int roleId);

        RoleDto GetWebContentsForRole(int RoleId);
        Task<RawPermissionsDto> GetPermissionsForWebContentId(int WebContentId);

    }
}

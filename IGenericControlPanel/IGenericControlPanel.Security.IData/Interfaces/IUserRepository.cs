﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Model.Security;
using IGenericControlPanel.Security.Dto;
using IGenericControlPanel.Security.Dto.User;

namespace IGenericControlPanel.Security.IData.Interfaces
{
    public interface IUserRepository
    {
        int CurrentUserIdentityId();
        Task<UserInfoDto> GetCurrentUserInfo();
        Task<CPRole> CurrentUserRole();
        UserDto GetUser(int id);
        UserDto GetCurrentUserInformation();
        public  Task<string> GetRoleByCurrentUser();
        string GetRole(int roleId);
    }
}

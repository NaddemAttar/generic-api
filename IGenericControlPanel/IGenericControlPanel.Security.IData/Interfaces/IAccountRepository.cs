﻿using CraftLab.Core.Infrastructure;
using CraftLab.Core.OperationResults;
using IGenericControlPanel.Security.Dto;
using IGenericControlPanel.Security.Dto.External.Facebook;
using IGenericControlPanel.Security.Dto.External.Google;
using IGenericControlPanel.Security.Dto.User;
using IGenericControlPanel.SharedKernal.Enums.Security;

namespace IGenericControlPanel.Security.IData
{
    public interface IAccountRepository
    {
        Task<OperationResult<GenericLoginResult, UserDto>> Login(UserDto loginDto);
        Task<OperationResult<GenericLoginResult, UserDto>> ExternalLogin(UserDto loginDto);
        Task Logout();
        Task<UserDto> GetUserDto(string Username, string Email);
        Task<OperationResult<GenericOperationResult, IEnumerable<UserDto>>> GetAllUsers(int? usresType);
        OperationResult<GenericOperationResult, IPagedList<NormalUserDto>> GetNormalUsers(int courseId, string qurey, IEnumerable<int> instructorIds,
           int locationId, IEnumerable<int> courseTypeIds, DateTime? startDate, DateTime? endDate, int? pageNumber, int? pageSize);
        OperationResult<GenericOperationResult, IPagedList<NormalUserDto>> GetNormalUsers(string query, int? pageNumber = 0, int? pageSize = 4);
        Task<OperationResult<GenericOperationResult, UserFormDto>> ActionNormalUser(UserFormDto userFormDto);
        Task<OperationResult<GenericOperationResult, UserFormDto>> ActionCPUser(CPUserRegistrationDto userFormDto);
        Task<OperationResult<GenericOperationResult>> RemoveNormalUser(int userId, RemoveAccountType removeAccountType);
        OperationResult<GenericOperationResult, IEnumerable<RoleDto>> GetRoles();
        OperationResult<GenericOperationResult, UserDto> GetUserDetails(int userId);
        Task<OperationResult<GenericOperationResult>> ActionArchive(int userId, bool Activate);
        Task<OperationResult<GenericOperationResult, FacebookUserInfoResult>> GetFacebookUserInfo(string accessToken);
        Task<OperationResult<GenericOperationResult, FacebookTokenValidationResult>> ValidateFacebookAccessToken(string accessToken);
        Task<OperationResult<GenericOperationResult, GoogleUserInfo>> GetGoogleUserInfo(string accessToken);
        Task<OperationResult<GenericOperationResult, NormalUserDto>> GetprofileInfo(int userId);
        OperationResult<GenericOperationResult, IEnumerable<HealthCareProviderDto>> GetHeathCareProviders();
        OperationResult<GenericOperationResult> AddRefreshTokenToUser(int UserId, string RefreshToken);
        Task<OperationResult<GenericOperationResult, bool>> AddCpUserToEnterprise(List<int> usersId, int enterpriseId);
    }
}
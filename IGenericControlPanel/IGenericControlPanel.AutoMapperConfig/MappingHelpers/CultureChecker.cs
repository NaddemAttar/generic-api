﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace IGenericControlPanel.AutoMapperConfig.MappingHelpers
{
    public static class CultureChecker
    {
        public static object GetTranslation<TSource, TProperty>(string cultureCode, Expression<Func<TSource, TProperty>> property)
        {
            Type type = typeof(TSource);
            MemberExpression member = property.Body as MemberExpression;
            if (member is null)
            {
                throw new ArgumentException($"Expression '{property}' refers to a method not property.");
            }
            PropertyInfo propertyInfo = member.Member as PropertyInfo;
            if (propertyInfo is null)
            {
                throw new ArgumentException($"Expression '{propertyInfo}' refers to a field, not a property.");
            }

            if (type != propertyInfo.ReflectedType && !type.IsSubclassOf(propertyInfo.ReflectedType))
            {
                throw new ArgumentException($"Expression '{property}' refers to a property that is not from type {type}.");
            }
            var propertyName = propertyInfo.Name;
            var propertyValue = Expression.Convert(member, typeof(string));
            var gett = Expression.Lambda<Func<object>>(propertyValue);
            //var s = gett.Compile();
            if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
            {
                return property.Name;
            }
            //var result = property.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Name ?? "No Translation";
            return "";
        }
    }
}

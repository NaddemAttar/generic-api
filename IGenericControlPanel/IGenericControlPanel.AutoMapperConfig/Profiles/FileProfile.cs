﻿using AutoMapper;

using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Model;
using IGenericControlPanel.Model.Content;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums.File;
using System;

namespace IGenericControlPanel.AutoMapperConfig.Profiles
{
    public class FileProfile : Profile
    {
        public FileProfile()
        {
            CreateMap<FileBase, Content.Dto.FileDto>().ReverseMap();
            CreateMap<FileBase, FileDetailsDto>().ReverseMap();

            CreateMap<ProductFile, FileDetailsDto>()
                .ForMember(dest => dest.SourceId, options =>
                {
                    options.MapFrom((src, dest) =>
                    {
                        return src.ProductId;
                    });
                })
                .ReverseMap();

            CreateMap<CarouselItemFile, FileDetailsDto>()
                .ForMember(dest => dest.SourceId, options =>
                {
                    options.MapFrom((src, dest) =>
                    {
                        return src.CarouselItemId;
                    });
                })
                .ReverseMap();

            CreateMap<ServiceFile, FileDetailsDto>()
                .ForMember(dest => dest.SourceId, options =>
                {
                    options.MapFrom((src, dest) =>
                    {
                        return src.ServiceId;
                    });
                })
                .ReverseMap();

            CreateMap<TeamMemberFile, FileDetailsDto>()
                .ForMember(dest => dest.SourceId, options =>
                {
                    options.MapFrom((src, dest) =>
                    {
                        return src.TeamMemberId;
                    });
                })
                .ReverseMap();

            CreateMap<InformationFile, FileDetailsDto>()
                .ForMember(dest => dest.SourceId, options =>
                {
                    options.MapFrom((src, dest) =>
                    {
                        return src.InformationId;
                    });
                })
                .ReverseMap();


            CreateMap<IFileDto, FileDetailsDto>()
                .ForMember(dest => dest.FileContentType, options =>
                {
                    options.MapFrom((src, dest, s, c) =>
                    {
                        // The types in the cast represents
                        // FileContentType, FileSourceType, SourceName, SourceEntityId, Name, Description
                        var data = c.Items[ItemNames.FileItems] as ValueTuple<FileContentType, FileSourceType, string, int, string, string>?;
                        return data.Value.Item1;
                    });
                })
                .ForMember(dest => dest.FileSourceType, options =>
                {
                    options.MapFrom((src, dest, s, c) =>
                    {
                        // The types in the cast represents
                        // FileContentType, FileSourceType, SourceName, SourceEntityId, Name, Description
                        var data = c.Items[ItemNames.FileItems] as ValueTuple<FileContentType, FileSourceType, string, int, string, string>?;
                        return data.Value.Item2;
                    });
                })
                .ForMember(dest => dest.SourceName, options =>
                {
                    options.MapFrom((src, dest, s, c) =>
                    {
                        // The types in the cast represents
                        // FileContentType, FileSourceType, SourceName, SourceEntityId, Name, Description
                        var data = c.Items[ItemNames.FileItems] as ValueTuple<FileContentType, FileSourceType, string, int, string, string>?;
                        return data.Value.Item3;
                    });
                })
                .ForMember(dest => dest.SourceId, options =>
                {
                    options.MapFrom((src, dest, s, c) =>
                    {
                        // The types in the cast represents
                        // FileContentType, FileSourceType, SourceName, SourceEntityId, Name, Description
                        var data = c.Items[ItemNames.FileItems] as ValueTuple<FileContentType, FileSourceType, string, int, string, string>?;
                        return data.Value.Item4;
                    });
                })
                .ForMember(dest => dest.Name, options =>
                {
                    options.MapFrom((src, dest, s, c) =>
                    {
                        // The types in the cast represents
                        // FileContentType, FileSourceType, SourceName, SourceEntityId, Name, Description
                        var data = c.Items[ItemNames.FileItems] as ValueTuple<FileContentType, FileSourceType, string, int, string, string>?;
                        return data.Value.Item5;
                    });
                })
                .ForMember(dest => dest.Description, options =>
                {
                    options.MapFrom((src, dest, s, c) =>
                    {
                        // The types in the cast represents
                        // FileContentType, FileSourceType, SourceName, SourceEntityId, Name, Description
                        var data = c.Items[ItemNames.FileItems] as ValueTuple<FileContentType, FileSourceType, string, int, string, string>?;
                        return data.Value.Item6;
                    });
                });
        }
    }
}

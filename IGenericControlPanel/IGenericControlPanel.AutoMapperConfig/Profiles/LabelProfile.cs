﻿using System.Linq;
using AutoMapper;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Model.Content;
using IGenericControlPanel.SharedKernal.Constants;

namespace IGenericControlPanel.AutoMapperConfig.Profiles
{
    public class LabelProfile : Profile
    {

        public LabelProfile()
        {
            ConfigureLabelDto();
            CreateMap<LabelTranslation, LabelTranslationDto>().ReverseMap();
        }

        private void ConfigureLabelDto()
        {
            CreateMap<Label, LabelDto>()
                            .ForMember(dest => dest.CultureCode, opt =>
                            {
                                opt.MapFrom((src, dest, s, c) =>
                                {
                                    var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                    if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                    {
                                        return src.CultureCode;
                                    }

                                    return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.CultureCode ?? "UNKOWN CULTURE";
                                });
                            })
                            .ForMember(dest => dest.Key, opt =>
                            {
                                opt.MapFrom((src, dest, s, c) =>
                                {
                                    var cultureCode = c.Items[ItemNames.CultureCode] as string;
                                    return src.Key;

                                });
                            })
                            .ForMember(dest => dest.Text, opt =>
                            {
                                opt.MapFrom((src, dest, s, c) =>
                                {
                                    var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                    if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                    {
                                        return src.Text;
                                    }

                                    return src.Translations.Where(tran => tran.CultureCode .Contains( cultureCode)).FirstOrDefault()?.Text ?? "No Translation";
                                });
                            });


        }
    }
}

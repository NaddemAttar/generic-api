﻿using AutoMapper;

using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.Dto.Product;
using IGenericControlPanel.Model;
using IGenericControlPanel.SharedKernal.Constants;

using System.Linq;

namespace IGenericControlPanel.AutoMapperConfig.Profiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            ConfigureProductDetailsDto();
            ConfigureProductDto();
            ConfigureProductTranslationDto();
            ConfigureProductPrimaryLanguageFormDto();
        }

        private void ConfigureProductPrimaryLanguageFormDto()
        {
        }

        private void ConfigureProductDto()
        {
            CreateMap<Product, ProductDto>()
                                                  .ForMember(dest => dest.Name, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.Name;
                                                          }

                                                          return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Name ?? "No translation";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.CultureCode, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.CultureCode;
                                                          }

                                                          return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.CultureCode ?? "No translation";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.CategoryName, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.Category != null ? src.Category.Name??"" : "";
                                                          }
                                                          return src.Category != null ? src.Category.Name??"" : "";
                                                      });
                                                  });

        }

        private void ConfigureProductDetailsDto()
        {
            CreateMap<Product, ProductDetailsDto>()
                                                  .ForMember(dest => dest.Name, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.Name;
                                                          }

                                                          return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Name ?? "No translation";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.CultureCode, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.CultureCode;
                                                          }

                                                          return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.CultureCode ?? "No translation";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.Description, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.Description;
                                                          }

                                                          return src.Description;
                                                      });
                                                  })
                                                  .ForMember(dest => dest.CategoryName, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.Category != null ? src.Category.Name : "";
                                                          }


                                                          return src.Category != null ? src.Category.Name : "";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.Attachments, opt =>
                                                  {
                                                      opt.MapFrom(src => src.Files != null || src.Files.Count > 0 ? src.Files.Where(entity => entity.IsValid && entity.FileContentType == FileContentType.Attachement) : null);
                                                  });
        }

        private void ConfigureProductTranslationDto()
        {
            CreateMap<ProductTranslation, ProductTranslationDto>().ReverseMap();
        }

    }
}

﻿using AutoMapper;
using IGenericControlPanel.Configuration.Dto;
using IGenericControlPanel.Model;
using System.Globalization;

namespace IGenericControlPanel.AutoMapperConfig.Profiles
{
    public class UserLanguageProfile : Profile
    {
        public UserLanguageProfile()
        {
            ConfigureUserLangaugeTranslationDto();
            ConfigureUserLanguageDetailsDto();
        }

        private void ConfigureUserLangaugeTranslationDto()
        {
            CreateMap<UserLanguageTranslation, UserLanguageTranslationDto>();
            CreateMap<UserLanguageTranslationDto, UserLanguageTranslation>().ForMember(dest => dest.UserLanguage, opt => opt.Ignore());
        }

        private void ConfigureUserLanguageDetailsDto()
        {
            CreateMap<UserLanguage, UserLanguageDto>()
                .ForMember(dest => dest.CultureInfo, 
                           option => option.MapFrom((src) => new CultureInfo(src.CultureCode)))
                .ReverseMap();
        }

    }
}

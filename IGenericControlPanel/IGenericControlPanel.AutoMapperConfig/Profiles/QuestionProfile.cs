﻿using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Core.Dto.Group;
using IGenericControlPanel.Core.Dto.Question;
using IGenericControlPanel.Core.Dto.Service;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.Security.Data.Repositories;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;

namespace IGenericControlPanel.AutoMapperConfig.Profiles
{
    public class QuestionProfile : Profile
    {
        public QuestionProfile()
        {
            ConfigureQuestionDto();
            ConfigureQuestionDetailsDto();
        }

        private void ConfigureQuestionDto()
        {
            _ = CreateMap<Question, QuestionDto>()
                                                    .ForMember(dest => dest.Title, opt =>
                                                    {
                                                        opt.MapFrom((src, dest, s, c) =>
                                                        {
                                                            string cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                            return string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar") ? src.Title : src.Title;
                                                            //return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Title ?? "No translation";
                                                        });
                                                    })
                                                .ForMember(dest => dest.Content, opt =>
                                                {
                                                    opt.MapFrom((src, dest, s, c) =>
                                                    {
                                                        string cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                        return string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar") ? src.Content : src.Content;

                                                        //return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Content ?? "No translation";
                                                    });
                                                });

        }
        private void ConfigureQuestionDetailsDto()
        {
            _ = CreateMap<Question, QuestionDetailsDto>()

                                                  .ForMember(dest => dest.MultiLevelCategoryIds, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          string cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          return string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar")
                                                              ? src.MultiLevelCategoryRelatedEntities.Select(ml => ml.MultiLevelCategoryId).ToList()
                                                              : src.MultiLevelCategoryRelatedEntities.Select(ml => ml.MultiLevelCategoryId).ToList();

                                                          //return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Title ?? "No translation";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.Title, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          string cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          return string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar") ? src.Title : src.Title;

                                                          //return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Title ?? "No translation";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.Service, opt =>
                                                  {
                                                      opt.MapFrom((src, dest) =>
                                                      {
                                                          if (src.Service != null)
                                                          {
                                                              ServiceDto service = new()
                                                              {
                                                                  Name = src.Service.Name,
                                                                  Id = src.ServiceId.Value
                                                              };
                                                              return service;
                                                          }
                                                          return null;
                                                      });
                                                  })
                                                  .ForMember(dest => dest.Group, opt =>
                                                  {
                                                      opt.MapFrom((src, dest) =>
                                                      {
                                                          if (src.Group != null)
                                                          {

                                                              GroupDto group = new()
                                                              {
                                                                  Name = src.Group.Name,
                                                                  Id = src.GroupId.Value
                                                              };
                                                              return group;
                                                          }
                                                          return null;
                                                      });
                                                  })
                                                  .ForMember(dest => dest.Answers, opt =>
                                                  {
                                                      opt.MapFrom((src, dest) =>
                                                      {
                                                          if (src.Answers != null)
                                                          {
                                                              var answers = src.Answers
                                                              .Where(answer => answer.IsValid).OrderByDescending(answer => answer.CreationDate).Take(1).Select(entity => new AnswerFormDto()
                                                              {
                                                                  Content = entity.Content
                                                              }).ToList();
                                                              return answers;
                                                          }
                                                          return null;
                                                      });
                                                  })
                                                  .ForMember(dest => dest.Content, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          string cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          return string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar") ? src.Content : src.Content;

                                                          //return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Content ?? "No translation";
                                                      });
                                                  })
                                                .ForMember(dest => dest.Attachments, opt =>
                                                {
                                                    opt.MapFrom(src => src.Files != null || src.Files.Count > 0 ? src.Files.Where(entity => entity.IsValid && entity.FileContentType == FileContentType.Attachement) : null);
                                                }).ForMember(dest => dest.Date, opt =>
                                                {
                                                    opt.MapFrom(src => src.CreationDate);
                                                });

        }
    }
}

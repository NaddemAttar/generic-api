﻿using AutoMapper;

using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.Dto.Service;
using IGenericControlPanel.Model;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.SharedKernal.Constants;

using Microsoft.AspNetCore.Html;

using System.Linq;
using System.Web;

namespace IGenericControlPanel.AutoMapperConfig.Profiles
{
    public class ServiceProfile : Profile
    {
        public ServiceProfile()
        {
            ConfigureServiceDetailsDto();           
            ConfigureServiceDto();
            ConfigureServiceTranslationDto();
        }
        private void ConfigureServiceDto()
        {
            CreateMap<Service, ServiceDto>()
                                                .ForMember(dest => dest.ImageUrl, opt =>
                                                {
                                                    opt.MapFrom((src, dest) =>
                                                    {
                                                        if (src.Files != null || src.Files.Count > 0)
                                                        {
                                                            var imageFile = src.Files.Where(e => e.IsValid && e.FileContentType == FileContentType.Image).FirstOrDefault();

                                                            return !(imageFile is null) ? imageFile.Url : "";
                                                        }

                                                        return "";
                                                    });
                                                })
                                                 .ForMember(dest => dest.Name, opt =>
                                                 {
                                                     opt.MapFrom((src, dest, s, c) =>
                                                     {
                                                         var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                         if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                         {
                                                             return src.Name;
                                                         }

                                                         return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Name ?? "No translation";
                                                     });
                                                 })
                                                 .ForMember(dest => dest.Description, opt =>
                                                 {
                                                     opt.MapFrom((src, dest, s, c) =>
                                                     {
                                                         var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                         if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                         {
                                                             return src.Description;

                                                         }

                                                         return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Description ?? "No translation";
                                                     });
                                                 });

        }

        private void ConfigureServiceDetailsDto()
        {
            CreateMap<Service, ServiceDetailsDto>()
                                                  .ForMember(dest => dest.ImageUrl, opt =>
                                                  {
                                                      opt.MapFrom((src, dest) =>
                                                      {
                                                          if (src.Files != null || src.Files.Count > 0)
                                                          {
                                                              var imageFile = src.Files.Where(e => e.IsValid && e.FileContentType == FileContentType.Image).FirstOrDefault();

                                                              return !(imageFile is null) ? imageFile.Url : "";
                                                          }

                                                          return "";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.Images, opt =>
                                                  {
                                                      opt.MapFrom(src => src.Files != null || src.Files.Count > 0 ? src.Files.Where(entity => entity.IsValid && entity.FileContentType == FileContentType.Image).ToList() : null);
                                                  });
        }

        private void ConfigureServiceTranslationDto()
        {
            CreateMap<ServiceTranslation, ServiceTranslationDto>().ReverseMap();
        }
    }
}

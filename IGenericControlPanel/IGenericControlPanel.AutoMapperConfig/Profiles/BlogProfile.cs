﻿using System.Linq;

using AutoMapper;

using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.Dto.Service;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.SharedKernal.Constants;

namespace IGenericControlPanel.AutoMapperConfig.Profiles
{
    public class BlogProfile : Profile
    {
        public BlogProfile()
        {
            ConfigureBlogDto();
            ConfigureBlogDetailsDto();
            ConfigureBlogTranslationDto();
        }

        private void ConfigureBlogDto()
        {
            _ = CreateMap<Blog, BlogDto>()
                                                 .ForMember(dest => dest.ImageUrl, opt =>
                                                 {
                                                     opt.MapFrom((src, dest) =>
                                                     {
                                                         if (src.Files != null || src.Files.Count > 0)
                                                         {
                                                             Model.Content.File.BlogFile imageFile = src.Files.Where(e => e.IsValid && e.FileContentType == FileContentType.Image).FirstOrDefault();

                                                             return imageFile is not null ? imageFile.Url : "";
                                                         }


                                                         return "";
                                                     });
                                                 })
                                                 .ForMember(dest => dest.Service, opt =>
                                                 {
                                                     opt.MapFrom((src, dest) =>
                                                     {
                                                         if (src.Service != null)
                                                         {

                                                             ServiceDto service = new()
                                                             {
                                                                 Name = src.Service.Name,
                                                                 Id = src.ServiceId.Value
                                                             };
                                                             return service;
                                                         }
                                                         return null;
                                                     });
                                                 })
                                                    .ForMember(dest => dest.Title, opt =>
                                                    {
                                                        opt.MapFrom((src, dest, s, c) =>
                                                        {
                                                            string cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                            return string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar")
                                                                ? src.Title
                                                                : src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Title ?? "No translation";
                                                        });
                                                    })
                                                .ForMember(dest => dest.Content, opt =>
                                                {
                                                    opt.MapFrom((src, dest, s, c) =>
                                                    {
                                                        string cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                        return string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar")
                                                            ? src.Content
                                                            : src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Content ?? "No translation";
                                                    });
                                                }).ForMember(dest => dest.Date, opt =>
                                                {
                                                    opt.MapFrom(src => src.BlogDate);
                                                });

        }
        private void ConfigureBlogDetailsDto()
        {
            _ = CreateMap<Blog, BlogDetailsDto>()

                                               .ForMember(dest => dest.ImageUrl, opt =>
                                               {
                                                   opt.MapFrom((src, dest) =>
                                                   {
                                                       if (src.Files != null || src.Files.Count > 0)
                                                       {
                                                           Model.Content.File.BlogFile imageFile = src.Files.Where(e => e.IsValid && e.FileContentType == FileContentType.Image).FirstOrDefault();

                                                           return imageFile is not null ? imageFile.Url : "";
                                                       }


                                                       return "";
                                                   });
                                               })

                                                  .ForMember(dest => dest.Title, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          string cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          return string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar")
                                                              ? src.Title
                                                              : src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Title ?? "No translation";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.Content, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          string cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          return string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar")
                                                              ? src.Content
                                                              : src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Content ?? "No translation";
                                                      });
                                                  })
                                                .ForMember(dest => dest.Attachments, opt =>
                                                {
                                                    opt.MapFrom(src => src.Files != null || src.Files.Count > 0 ? src.Files.Where(entity => entity.IsValid && entity.FileContentType == FileContentType.Attachement) : null);
                                                }).ForMember(dest => dest.Date, opt =>
                                                {
                                                    opt.MapFrom(src => src.BlogDate);
                                                });

        }
        private void ConfigureBlogTranslationDto()
        {
            _ = CreateMap<BlogTranslation, BlogTranslationDto>().ReverseMap();
        }
    }
}

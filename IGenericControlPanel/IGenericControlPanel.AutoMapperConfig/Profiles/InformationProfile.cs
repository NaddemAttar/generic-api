﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AutoMapper;

using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.Dto.Information;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.SharedKernal.Constants;

namespace IGenericControlPanel.AutoMapperConfig.Profiles
{
   public class InformationProfile: Profile
    {
        public InformationProfile()
        {
            ConfigureInfoDto();
            ConfigureInfoDetailsDto();
            ConfigureInfoTranslationDto();
        }

        private void ConfigureInfoDto()
        {
            CreateMap<Information, InfoDto>()
                                                .ForMember(dest => dest.ImageUrl, opt =>
                                                {
                                                    opt.MapFrom((src, dest) =>
                                                    {
                                                        if (src.Files != null || src.Files.Count > 0)
                                                        {
                                                            var imageFile = src.Files.Where(e => e.IsValid && e.FileContentType == FileContentType.Image).FirstOrDefault();

                                                            return !(imageFile is null) ? imageFile.Url : "";
                                                        }

                                                        return "";
                                                    });
                                                })
                                                    .ForMember(dest => dest.Address, opt =>
                                                    {
                                                        opt.MapFrom((src, dest, s, c) =>
                                                        {
                                                            var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                            if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                            {
                                                                return src.Address;
                                                            }

                                                            return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Address ?? "No translation";
                                                        });
                                                    })
                                                  .ForMember(dest => dest.CultureCode, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.CultureCode;
                                                          }

                                                          return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.CultureCode ?? "No translation";
                                                      });
                                                  })
                                                .ForMember(dest => dest.Description, opt =>
                                                {
                                                    opt.MapFrom((src, dest, s, c) =>
                                                    {
                                                        var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                        if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                        {
                                                            return src.Description;
                                                        }

                                                        return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Description ?? "No translation";
                                                    });
                                                });

        }
        private void ConfigureInfoDetailsDto()
        {
            CreateMap<Information, InfoDetailsDto>()
                                                  .ForMember(dest => dest.ImageUrl, opt =>
                                                  {
                                                      opt.MapFrom((src, dest) =>
                                                      {
                                                          if (src.Files != null || src.Files.Count > 0)
                                                          {
                                                              var imageFile = src.Files.Where(e => e.IsValid && e.FileContentType == FileContentType.Image).FirstOrDefault();

                                                              return !(imageFile is null) ? imageFile.Url : "";
                                                          }

                                                          return "";
                                                      });
                                                  })

                                                  .ForMember(dest => dest.CultureCode, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.CultureCode;
                                                          }

                                                          return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.CultureCode ?? "No translation";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.Address, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.Address;
                                                          }

                                                          return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Address ?? "No translation";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.Description, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.Description;
                                                          }

                                                          return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Description ?? "No translation";
                                                      });
                                                  })
                                                .ForMember(dest => dest.Images, opt =>
                                                {
                                                    opt.MapFrom(src => src.Files != null || src.Files.Count > 0 ? src.Files.Where(entity => entity.IsValid && entity.FileContentType == FileContentType.Image).ToList() : null);
                                                });
        }
        private void ConfigureInfoTranslationDto()
        {
            CreateMap<InformationTranslation, InfoTranslationDto>().ReverseMap();
        }
    }
}

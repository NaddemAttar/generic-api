﻿using AutoMapper;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Model.Content;
using IGenericControlPanel.SharedKernal.Constants;
using System.Linq;

namespace IGenericControlPanel.AutoMapperConfig.Profiles
{
    public class CarouselItemProfile : Profile
    {
        public CarouselItemProfile()
        {
            ConfigureCarouselItemDetailsDto();

            ConfigureCarouselItemDto();
            ConfigureCarouselItemTranslationDto();
            CreateMap<CarouselItemDetailsDto, CarouselItem>();
        }



        private void ConfigureCarouselItemDto()
        {
            CreateMap<CarouselItem, CarouselItemDto>()
                                                 .ForMember(dest => dest.Title, opt =>
                                                 {
                                                     opt.MapFrom((src, dest, s, c) =>
                                                     {
                                                         var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                         if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                         {
                                                             return src.Title;
                                                         }

                                                         return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Title ?? "No translation";
                                                     });
                                                 })
                                                  .ForMember(dest => dest.TopSubTitle, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.TopSubTitle;
                                                          }

                                                          return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.TopSubTitle ?? "No translation";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.BottomSubTitle, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.BottomSubTitle;
                                                          }

                                                          return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.BottomSubTitle ?? "No translation";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.ButtonText, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.ButtonText;
                                                          }

                                                          return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.ButtonText ?? "No translation";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.CultureCode, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.CultureCode;
                                                          }

                                                          return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.CultureCode ?? "No translation";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.ImageUrl, opt =>
                                                  {
                                                      opt.MapFrom((src, dest) =>
                                                      {
                                                          if (src.Image != null && src.Image.IsValid)
                                                          {
                                                              var imageFile = src.Image;

                                                              return !(imageFile is null) ? imageFile.Url : "";
                                                          }

                                                          return "";
                                                      });
                                                  });

        }

        private void ConfigureCarouselItemDetailsDto()
        {
                    CreateMap<CarouselItem, CarouselItemDetailsDto>()
                                                    .ForMember(dest => dest.Title, opt =>
                                                    {
                                                        opt.MapFrom((src, dest, s, c) =>
                                                        {
                                                            var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                            if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                            {
                                                                return src.Title;
                                                            }

                                                            return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.Title ?? "No translation";
                                                        });
                                                    })
                                                  .ForMember(dest => dest.TopSubTitle, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.TopSubTitle;
                                                          }

                                                          return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.TopSubTitle ?? "No translation";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.BottomSubTitle, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.BottomSubTitle;
                                                          }

                                                          return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.BottomSubTitle ?? "No translation";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.ButtonText, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.ButtonText;
                                                          }

                                                          return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.ButtonText ?? "No translation";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.CultureCode, opt =>
                                                  {
                                                      opt.MapFrom((src, dest, s, c) =>
                                                      {
                                                          var cultureCode = c.Items[ItemNames.CultureCode] as string;

                                                          if (string.IsNullOrEmpty(cultureCode) || cultureCode.Contains("ar"))
                                                          {
                                                              return src.CultureCode;
                                                          }

                                                          return src.Translations.Where(tran => tran.CultureCode.Contains(cultureCode)).FirstOrDefault()?.CultureCode ?? "No translation";
                                                      });
                                                  })
                                                  .ForMember(dest => dest.ImageUrl, opt =>
                                                  {
                                                      opt.MapFrom((src, dest) =>
                                                      {
                                                          if (src.Image != null && src.Image.IsValid)
                                                          {
                                                              var imageFile = src.Image;

                                                              return !(imageFile is null) ? imageFile.Url : "";
                                                          }

                                                          return "";
                                                      });
                                                  })
                                                 .ForMember(dest => dest.Image, opt =>
                                                 {
                                                     opt.MapFrom((src, dest, s, c) =>
                                                     {
                                                         if (src.Image is null)
                                                         {
                                                             return null;
                                                         }
                                                         return src.Image.IsValid ? src.Image : null;
                                                     });
                                                 }).ForMember(dest => dest.ImageUrl, opt =>
                                                 {
                                                     opt.MapFrom((src, dest) =>
                                                     {
                                                         if (src.Image != null && src.Image.IsValid)
                                                         {
                                                             var imageFile = src.Image;

                                                             return !(imageFile is null) ? imageFile.Url : "";
                                                         }

                                                         return "";
                                                     });
                                                 }); ;
        }

        private void ConfigureCarouselItemTranslationDto()
        {
            CreateMap<CarouselItemTranslation, CarouselItemTranslationDto>().ReverseMap();
        }
    }
}

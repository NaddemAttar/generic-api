﻿
using IGenericControlPanel.GamificationSystem.IData.Interfaces;

namespace IGenericControlPanel.UnitTests.Controllers;
public class GamificationManagmentsControllerTest
{
    #region Constructor & Properties
    private Mock<IGamificationManagmentsRepository> _gamificationRepository { get; }
    private Mock<IStringLocalizer<GamificationManagmentsController>> _localizer { get; }
    private GamificationManagmentsController _gamificationController;
    public GamificationManagmentsControllerTest()
    {
        _gamificationRepository = new Mock<IGamificationManagmentsRepository>();
        _localizer = new Mock<IStringLocalizer<GamificationManagmentsController>>();
        _gamificationController = new GamificationManagmentsController(
            _gamificationRepository.Object,
            _localizer.Object);
    }
    #endregion

    #region GetAllGamificationSections Test
    [Fact]
    public async Task GetAllGamificationSections_ShouldReturnDataWithSuccessResponse()
    {
        // Arrange
        var gamificationSections = GamificationManagmentsFixture.GetGamificationSectionsData();

        _gamificationRepository.Setup(repo => repo.GetAllGamificationSectionsAsync())
            .ReturnsAsync(GamificationManagmentsFixture.GetGamificationSectionsData());

        // Act
        var result = await _gamificationController.GetAllGamificationSections();
        var objectResult = (result as ObjectResult).Value;

        //Assert
        result.Should().BeOfType<OkObjectResult>();
        gamificationSections.Should().BeEquivalentTo(objectResult);
    }
    #endregion

    #region UpdatePointsForGamificactionSections Test
    [Fact]
    public async Task UpdatePointsForGamificactionSections_ShouldReturnSuccessResponse()
    {
        // Arrange
        var gamificationSections = GamificationManagmentsFixture.GetGamificationSectionsData();

        _gamificationRepository.Setup(repo => repo.UpdatePointsForGamificactionSectionsAsync(
            gamificationSections.Result))
            .ReturnsAsync(GamificationManagmentsFixture.GetGamificationSectionsData());

        // Act
        var result = await _gamificationController.UpdatePointsForGamificactionSections(
            gamificationSections.Result);
        var objectResult = (result as ObjectResult).Value;

        // Asert
        result.Should().BeOfType<OkObjectResult>();
        gamificationSections.Should().BeEquivalentTo(objectResult);
    }
    #endregion

    #region AddNewGamificationSection Test
    [Fact]
    public async Task AddNewGamificationSection_ShouldReturnSuccessResponse()
    {
        // Arrange
        var getOneGamification = GamificationManagmentsFixture.GetOneGamification();
        _gamificationRepository.Setup(repo => repo.AddNewGamificationSectionAsync(getOneGamification.Result))
            .ReturnsAsync(GamificationManagmentsFixture.GetOneGamification());

        // Act
        var result = await _gamificationController.AddNewGamificationSection(getOneGamification.Result);
        var objectResult = (result as ObjectResult).Value;

        // Assert
        result.Should().BeOfType<OkObjectResult>();
        getOneGamification.Should().BeEquivalentTo(objectResult);
    }
    #endregion

    #region GetUsersWithTheirPoints Test
    [Fact]
    public async Task GetUsersWithTheirPoints_ShouldReturnSuccessResponse()
    {
        // Arrange
        var filtersDto = GamificationManagmentsFixture.InitializeFiltersData();

        //_gamificationRepository.Setup(repo => repo.GetUsersWithTheirPointsAsync(
        //    It.Is<FiltersUsersDto>(dto => dto == filtersDto)))
        //    .ReturnsAsync(GamificationManagmentsFixture.GetUsersPointsDto());

        _gamificationRepository.Setup(repo => repo.GetUsersWithTheirPointsAsync(
            It.IsAny<FiltersUsersDto>()))
            .ReturnsAsync(GamificationManagmentsFixture.GetUsersPointsDto());

        // Act
        var result = await _gamificationController.GetUsersWithTheirPoints(
            filtersDto.EnablePagination, filtersDto.UsersHavePointsOnly,
            filtersDto.IsAscendingOrder, filtersDto.Query, filtersDto.PageSize,
            filtersDto.PageNumber);

        // Assert

        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion
}

﻿using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Resources;
using System.Drawing.Printing;

namespace IGenericControlPanel.UnitTests.Controllers;
public class BrandControllerTest
{
    #region Constructor & Properties
    private Mock<IBrandRepository> _brandRepository;
    private Mock<IStringLocalizer<BrandController>> _localizer;
    private Mock<IDistributedCache> _cache;
    private BrandController _brandController;
    private IStringLocalizer<SharedResource> sharedLocalizer;
    private IStringLocalizer<BrandResource> brandLocalizer;

    public BrandControllerTest()
    {
        _brandRepository = new Mock<IBrandRepository>();
        _localizer = new Mock<IStringLocalizer<BrandController>>();
        _cache = new Mock<IDistributedCache>();
        _brandController = new BrandController(
            _brandRepository.Object,
            _localizer.Object,
            _cache.Object,
            sharedLocalizer,
            brandLocalizer);
    }
    #endregion

    #region Get One Brand Test
    [Fact]
    public async Task GetBrand_ShouldReturnBrandDetails()
    {
        // Arrange
        var brandResultExpected = BrandFixture.GetOneBrand();

        _brandRepository.Setup(repo => repo.GetBrandByIdAsync(It.IsAny<int>()))
            .ReturnsAsync(BrandFixture.GetOneBrand());
        // Act
        var result = await _brandController.GetBrand(It.IsAny<int>());

        var brandResultActual = (result as ObjectResult)?.Value;
        var res = brandResultActual as OperationResult<GenericOperationResult, BrandDto>;

        // Assert
        Assert.Equal(res.Result.Id, brandResultExpected.Result.Id);
        Assert.Equal(res.Result.Name, brandResultExpected.Result.Name);
        Assert.True(res.IsSuccess);
    }
    #endregion

    #region Get All Brands Test
    [Fact]
    public async Task GetAllBrands_ShouldReturnData()
    {
        // Arrange
        var resultExpected = BrandFixture.GetBrands();

        _brandRepository.Setup(repo => repo.GetAllBrandsAsync())
            .ReturnsAsync(BrandFixture.GetBrands());

        // Act
        var result = await _brandController.GetAllBrands();

        var brandsResultActual = (result as ObjectResult)?.Value;
        var res = brandsResultActual as
            OperationResult<GenericOperationResult, IEnumerable<BrandDto>>;

        // Assert
        res.Result.Should().BeEquivalentTo(resultExpected.Result);
        Assert.True(res.IsSuccess);
        Assert.NotEmpty(res.Result);
    }
    #endregion

    #region Action Brand Test
    [Fact]
    public async Task ActionBrand_ShouldAddNewBrand()
    {
        // Arrange
        BrandFormDto newBrand = new()
        {
            Name = "Nadim"
        };

        _brandRepository.Setup(repo => repo.ActionBrandAsync(newBrand))
            .ReturnsAsync(BrandFixture.GetOneBrand());

        // Act
        var result = await _brandController.ActionBrand(newBrand);

        var brandActionResultActual = (result as ObjectResult)?.Value;
        var res = brandActionResultActual as OperationResult<GenericOperationResult, BrandDto>;

        // Assert
        res.Result.Name.Should().BeEquivalentTo(newBrand.Name);
        res.IsSuccess.Should().BeTrue();
    }

    [Fact]
    public async Task ActionBrand_ShouldUpdateBrand()
    {
        // Arrange
        BrandFormDto updateBrand = new()
        {
            Id = 1,
            Name = "Nadim"
        };

        _brandRepository.Setup(repo => repo.ActionBrandAsync(updateBrand))
            .ReturnsAsync(BrandFixture.GetOneBrand());

        // Act
        var result = await _brandController.ActionBrand(updateBrand);
        var brandObjectResult = (result as ObjectResult)?.Value;
        var brandResultActual = brandObjectResult as
            OperationResult<GenericOperationResult, BrandDto>;

        // Assert
        brandResultActual.Result.Name.Should().BeEquivalentTo(updateBrand.Name);
        brandResultActual.Result.Id.Should().Be(updateBrand.Id);
        brandResultActual.IsSuccess.Should().BeTrue();
        brandResultActual.IsFailure.Should().BeFalse();
    }
    #endregion

    #region Remove Brand Test
    [Fact]
    public async Task RemoveBrand_ShouldRemoveBrnad()
    {
        // Arrange
        _brandRepository.Setup(repo => repo.RemoveBrandAsync(It.IsAny<int>()))
            .ReturnsAsync(new OperationResult<GenericOperationResult>());

        // Act
        var result = await _brandController.RemoveBrand(It.IsAny<int>());

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion
}

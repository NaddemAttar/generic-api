﻿
using AutoMapper;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.UserExperiences.Dto.Experience;
using IGenericControlPanel.UserExperiences.IData.Interfaces;

namespace IGenericControlPanel.UnitTests.Controllers;
public class ExperienceManagementControllerTest
{
    #region Constructor & Properties
    private readonly Mock<IExperienceRepository> _experienceRepository;
    private readonly Mock<IHttpFileService> _httpFileService;
    private readonly Mock<IMapper> _mapper;
    private readonly Mock<IConfiguration> _configuration;
    private Mock<IStringLocalizer<ExperienceManagementController>> _localizer;
    private ExperienceManagementController experienceManagementController;

    public ExperienceManagementControllerTest()
    {
        _experienceRepository = new Mock<IExperienceRepository>();
        _httpFileService = new Mock<IHttpFileService>();
        _mapper = new Mock<IMapper>();
        _localizer = new Mock<IStringLocalizer<ExperienceManagementController>>();
        _configuration = new Mock<IConfiguration>();
        experienceManagementController = new ExperienceManagementController(
            _localizer.Object,
            _experienceRepository.Object,
            _httpFileService.Object,
            _mapper.Object,
            _configuration.Object);
    }
    #endregion

    #region Get Test
    [Fact]
    public async Task GetExperiencesForUser_ShouldReturnSuccessRespose()
    {
        // Arrange 
        _experienceRepository.Setup(repo => repo.GetMultipleAsync())
            .ReturnsAsync(ExperienceManagementFixture.GetExperienecDtos());

        // Act
        var result = await experienceManagementController.GetExperiencesForUser();

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public async Task GetOneExperience_ShouldReturnSuccessRespose(int id)
    {
        // Arrange 
        _experienceRepository.Setup(repo => repo.GetDetailsAsync(id))
            .ReturnsAsync(ExperienceManagementFixture.GetOneExperienecDto());

        // Act
        var result = await experienceManagementController.GetOneExperience(id);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion

    #region Remove Test
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public async Task RemoveExperience_ShouldReturnSuccessRespose(int id)
    {
        // Arrange 
        _experienceRepository.Setup(repo => repo.RemoveAsync(id))
            .ReturnsAsync(new OperationResult<GenericOperationResult>());

        // Act
        var result = await experienceManagementController.RemoveExperience(id);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion

    #region Action Test
    [Fact]
    public async Task AddExperience_ShouldReturnSuccessResponse()
    {
        // Arrange
        var dto = ExperienceManagementFixture.GetExperienceAddDto();
        var fileDtos = new List<FileDetailsDto>();

        _experienceRepository.Setup(repo => repo.AddAsync(dto, fileDtos))
            .ReturnsAsync(new OperationResult<GenericOperationResult, ExperienceBasicDto>());

        // Act

        var result = await experienceManagementController.AddExperience(dto);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }

    [Fact]
    public async Task UpdateExperience_ShouldReturnSuccessResponse()
    {
        // Arrange
        var dto = ExperienceManagementFixture.GetExperienceUpdateDto();
        var fileDtos = new List<FileDetailsDto>();

        _experienceRepository.Setup(repo => repo.UpdateAsync(dto, fileDtos, dto.RemoveFileIds))
            .ReturnsAsync(new OperationResult<GenericOperationResult, ExperienceBasicDto>());

        // Act

        var result = await experienceManagementController.UpdateExperience(dto);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion
}

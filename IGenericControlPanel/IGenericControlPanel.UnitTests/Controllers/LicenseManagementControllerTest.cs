﻿
using AutoMapper;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.UserExperiences.Dto.License;
using IGenericControlPanel.UserExperiences.IData.Interfaces;

namespace IGenericControlPanel.UnitTests.Controllers;
public class LicenseManagementControllerTest
{
    #region Constructor & Properties
    private readonly Mock<ILicenseRepository> _licenseRepository;
    private readonly Mock<IHttpFileService> _httpFileService;
    private readonly Mock<IConfiguration> _configuration;
    private readonly Mock<IMapper> _mapper;
    private readonly Mock<IStringLocalizer<LicenseManagementController>> _localizer;
    private readonly LicenseManagementController licenseManagementController;
    public LicenseManagementControllerTest()
    {
        _licenseRepository = new Mock<ILicenseRepository>();
        _httpFileService = new Mock<IHttpFileService>();
        _configuration = new Mock<IConfiguration>();
        _mapper = new Mock<IMapper>();
        _localizer = new Mock<IStringLocalizer<LicenseManagementController>>();
        licenseManagementController = new LicenseManagementController(
            _licenseRepository.Object,
            _localizer.Object,
            _httpFileService.Object,
            _mapper.Object,
            _configuration.Object);
    }
    #endregion

    #region Get Test
    [Fact]
    public async Task GetLicenseForUser_ShouldReturnSuccessResponse()
    {
        // Arrange 
        _licenseRepository.Setup(repo => repo.GetMultipleAsync())
            .ReturnsAsync(LicenseManagementFixture.GetLicenses());

        // Act
        var result = await licenseManagementController.GetLicensesForUser();

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }

    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public async Task GetOneLicense_ShouldReturnSuccessResponse(int id)
    {
        // Arrange 
        _licenseRepository.Setup(repo => repo.GetDetailsAsync(id))
            .ReturnsAsync(LicenseManagementFixture.GetOneLicense());

        // Act
        var result = await licenseManagementController.GetOneLicense(id);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion

    #region Remove Test
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public async Task RemoveLicense_ShouldReturnSuccessResponse(int id)
    {
        // Arrange 
        _licenseRepository.Setup(repo => repo.RemoveAsync(id))
            .ReturnsAsync(new OperationResult<GenericOperationResult>());

        // Act
        var result = await licenseManagementController.RemoveLicense(id);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion

    #region Action
    [Fact]
    public async Task AddLicense_ShouldReturnSuccessResponse()
    {
        // Arrange 
        List<FileDetailsDto> files = new List<FileDetailsDto>();
        var addDto = LicenseManagementFixture.GetLicenseAddDto();

        _licenseRepository.Setup(repo => repo.AddAsync(
            It.IsAny<LicenseBasicDto>(),
            It.IsAny<List<FileDetailsDto>>()))
            .ReturnsAsync(LicenseManagementFixture.GetLicenseBasicDto());

        // Act
        var result = await licenseManagementController.AddLicense(addDto);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }

    [Fact]
    public async Task UpdateLicense_ShouldReturnSuccessResponse()
    {
        // Arrange 
        List<FileDetailsDto> files = new();
        List<int> removeFileIds = new();
        var updateDto = LicenseManagementFixture.GetLicenseUpdateDto();

        _licenseRepository.Setup(repo => repo.UpdateAsync(
            It.IsAny<LicenseUpdateDto>(),
            It.IsAny<List<FileDetailsDto>>(),
            It.IsAny<List<int>>()))
            .ReturnsAsync(LicenseManagementFixture.GetLicenseBasicDto());

        // Act
        var result = await licenseManagementController.UpdateLicense(updateDto);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion
}

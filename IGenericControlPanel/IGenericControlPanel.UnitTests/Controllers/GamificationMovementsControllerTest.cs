﻿

namespace IGenericControlPanel.UnitTests.Controllers;
public class GamificationMovementsControllerTest
{
    #region Properties & Constructor
    private Mock<IGamificationMovementsRepository> _gamificationMovementsReposiory { get; }
    private Mock<IStringLocalizer<GamificationMovementsController>> _localizer { get; }
    private GamificationMovementsController _gamificationMovementsController { get; }
    public GamificationMovementsControllerTest()
    {
        _gamificationMovementsReposiory = new Mock<IGamificationMovementsRepository>();
        _localizer = new Mock<IStringLocalizer<GamificationMovementsController>>();
        _gamificationMovementsController = new GamificationMovementsController(
            _localizer.Object,
            _gamificationMovementsReposiory.Object);
    }
    #endregion

    #region Save Social Media Points Test
    [Fact]
    public async Task SaveSocialMediaPoints_ShouldReturnSuccessResponse()
    {
        // Arrange 
        var gamificationKeys = GamificationManagmentsFixture.GetGamificationKeys();
        _gamificationMovementsReposiory.Setup(repo => repo.SavePoints(gamificationKeys))
            .ReturnsAsync(new OperationResult<GenericOperationResult>());

        // Act
        var result = await _gamificationMovementsController.SaveSocialMediaPoints(
            SocialMediaApps.Facebook,
            SectionsForPublishOnSocialMedia.Courses);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion
}

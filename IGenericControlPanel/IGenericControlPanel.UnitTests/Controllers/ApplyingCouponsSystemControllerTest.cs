﻿
namespace IGenericControlPanel.UnitTests.Controllers;
public class ApplyingCouponsSystemControllerTest
{
    #region Constructor & Properties
    private readonly Mock<IApplyingCouponsSystemRepository> _applyingCouponsSystemRepository;
    private readonly Mock<IStringLocalizer<ApplyingCouponsSystemController>> _localizer;
    private readonly ApplyingCouponsSystemController _applyingCouponsSystemController;
    public ApplyingCouponsSystemControllerTest()
    {
        _applyingCouponsSystemRepository = new Mock<IApplyingCouponsSystemRepository>();
        _localizer = new Mock<IStringLocalizer<ApplyingCouponsSystemController>>();
        _applyingCouponsSystemController = new ApplyingCouponsSystemController(
            _applyingCouponsSystemRepository.Object,
            _localizer.Object);
    }
    #endregion

    #region GetAvailableCoupons Test
    [Fact]
    public async Task GetAvailableCoupons_ShouldReturnSuccessResponse()
    {
        // Arrange
        _applyingCouponsSystemRepository.Setup(repo => repo.GetAvailableCouponsAsync())
            .ReturnsAsync(ApplyingCouponsSystemFixture.GetCouponsDto());

        // Act
        var result = await _applyingCouponsSystemController.GetAvailableCoupons();

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion

    #region CouponBooking Test

    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public async Task CouponBooking_ShouldReturnSuccessResponse(int couponId)
    {
        // Arrange
        _applyingCouponsSystemRepository.Setup(repo => repo.CouponBookingAsync(couponId))
            .ReturnsAsync(new OperationResult<GenericOperationResult, string>());
        // Act
        var result = await _applyingCouponsSystemController.CouponBooking(couponId);

        // Assert

        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion

    #region Activate & DeActivate CouponBooking Test

    [Fact]
    public async Task ActivateCouponBooking_ShouldReturnSuccessResponse()
    {
        // Arrange
        string code = "MEXLVZ";
        _applyingCouponsSystemRepository.Setup(repo => repo.ActivateCouponBookingAsync(code))
            .ReturnsAsync(new OperationResult<GenericOperationResult>());

        // Act
        var result = await _applyingCouponsSystemController.ActivateCouponBooking(code);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }

    [Fact]
    public async Task DeActivateCouponBooking_ShouldReturnSuccessResponse()
    {
        // Arrange
        _applyingCouponsSystemRepository.Setup(repo => repo.DeActivateCouponBookingAsync())
            .ReturnsAsync(new OperationResult<GenericOperationResult>());

        // Act
        var result = await _applyingCouponsSystemController.DeActivateCouponBooking();

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion

    #region GetUserCoupons Test
    [Theory]
    [InlineData(true, true)]
    public async Task GetUserCoupons_ShouldReturnSuccessResponse(bool isBeneficiary, bool isActivate)
    {
        // Arrange
        _applyingCouponsSystemRepository.Setup(repo => repo.GetUserCouponsAsync(isBeneficiary, isActivate))
            .ReturnsAsync(ApplyingCouponsSystemFixture.GetUserCoupons());

        // Act
        var result = await _applyingCouponsSystemController.GetUserCoupons(isBeneficiary, isActivate);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion 

    #region PointsDeductionFromUser Test
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    [InlineData(3)]
    public async Task PointsDeductionFromUser_ShouldReturnSuccessResponse(int userCouponId)
    {
        // Arrange
        _applyingCouponsSystemRepository.Setup(repo => repo.PointsDeductionFromUserAsync(userCouponId))
            .ReturnsAsync(new OperationResult<GenericOperationResult>());

        // Act
        var result = await _applyingCouponsSystemController.PointsDeductionFromUser(userCouponId);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion
}

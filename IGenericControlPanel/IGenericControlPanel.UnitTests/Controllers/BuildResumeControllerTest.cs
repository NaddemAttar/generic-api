﻿
using IGenericControlPanel.Security.Dto.User;
using IGenericControlPanel.Security.IData;
using IGenericControlPanel.UserExperiences.Dto.Certificate;
using IGenericControlPanel.UserExperiences.Dto.Education;
using IGenericControlPanel.UserExperiences.Dto.Experience;
using IGenericControlPanel.UserExperiences.Dto.Lecture;
using IGenericControlPanel.UserExperiences.Dto.License;
using IGenericControlPanel.UserExperiences.IData.Interfaces;

namespace IGenericControlPanel.UnitTests.Controllers;
public class BuildResumeControllerTest
{
    private readonly Mock<IStringLocalizer<BuildResumeController>> _localizer;
    private readonly Mock<IConfiguration> _configuration;
    private readonly Mock<IExperienceRepository> _experienceRepository;
    private readonly Mock<IEducationRepository> _educationRepository;
    private readonly Mock<ICertificateRepository> _certificateRepository;
    private readonly Mock<ILectureRepository> _lectureRepository;
    private readonly Mock<ILicenseRepository> _licenseRepository;
    private readonly Mock<IAccountRepository> _accountRepository;
    private readonly BuildResumeController buildResumeController;
    public BuildResumeControllerTest()
    {
        _localizer = new Mock<IStringLocalizer<BuildResumeController>>();
        _configuration = new Mock<IConfiguration>();
        _experienceRepository = new Mock<IExperienceRepository>();
        _educationRepository = new Mock<IEducationRepository>();
        _certificateRepository = new Mock<ICertificateRepository>();
        _lectureRepository = new Mock<ILectureRepository>();
        _licenseRepository = new Mock<ILicenseRepository>();
        _accountRepository = new Mock<IAccountRepository>();
        buildResumeController = new BuildResumeController(
            _localizer.Object,
            _configuration.Object,
            _experienceRepository.Object,
            _educationRepository.Object,
            _certificateRepository.Object,
            _lectureRepository.Object,
            _licenseRepository.Object,
            _accountRepository.Object);
    }

    [Fact]
    public async Task GetResumeForHcpUser_ShouldReturnSuccessResponse()
    {
        // Arrange
        _experienceRepository.Setup(repo => repo.GetExperienceDataForResumeAsync(0))
            .ReturnsAsync(new OperationResult<GenericOperationResult, IEnumerable<ExperienceDetailsDto>>());
        _educationRepository.Setup(repo => repo.GetEducationDataForResumeAsync(0))
            .ReturnsAsync(new OperationResult<GenericOperationResult, IEnumerable<EducationDetailsDto>>());
        _certificateRepository.Setup(repo => repo.GetCertificateDataForResumeAsync(0))
            .ReturnsAsync(new OperationResult<GenericOperationResult, IEnumerable<CertificateDetailsDto>>());
        _lectureRepository.Setup(repo => repo.GetLectureDataForResumeAsync(0))
            .ReturnsAsync(new OperationResult<GenericOperationResult, IEnumerable<LectureDetailsDto>>());
        _licenseRepository.Setup(repo => repo.GetLicenseDataForResumeAsync(0))
            .ReturnsAsync(new OperationResult<GenericOperationResult, IEnumerable<LicenseDetailsDto>>());
        _accountRepository.Setup(repo => repo.GetprofileInfo(0))
            .ReturnsAsync(new OperationResult<GenericOperationResult, NormalUserDto>());

        // Act
        var result = await buildResumeController.GetResumeForHcpUser(0);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
}

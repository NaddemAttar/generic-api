﻿
namespace IGenericControlPanel.UnitTests.Controllers;
public class PartnersControllerTests
{
    #region Properties & Constructor
    private Mock<IPartnersRepository> _partnerRepository;
    private Mock<IHttpFileService> _httpFileService;
    private Mock<IConfiguration> _configuration;
    private Mock<IStringLocalizer<PartnersController>> _localizer;
    private PartnersController _partnersController;
    public PartnersControllerTests()
    {
        _partnerRepository = new Mock<IPartnersRepository>();
        _httpFileService = new Mock<IHttpFileService>();
        _configuration = new Mock<IConfiguration>();
        _localizer = new Mock<IStringLocalizer<PartnersController>>();
        _partnersController = new PartnersController(
            _partnerRepository.Object,
            _httpFileService.Object,
            _configuration.Object,
            _localizer.Object);
    }
    #endregion

    #region Get Partners Test
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    [InlineData(3)]
    public void GetPartners_ShouldReturnPartnersWithPagination(int pageNumber)
    {
        // Arrange 
        int pageSize = 6;
        _partnerRepository.Setup(repo => repo.GetPartners(pageSize, pageNumber))
            .Returns(PartnersFixture.GetPartners());

        // Act
        var result = _partnersController.GetPartners(pageNumber, pageSize);
        var partnersObjectResult = (result as ObjectResult)?.Value;
        var partnersResultActual = (
            partnersObjectResult as OperationResult<GenericOperationResult, IEnumerable<PartnersDto>>
            );

        // Assert   
        partnersResultActual.IsSuccess.Should().BeTrue();
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion

    #region Get One Partner Test
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    [InlineData(3)]
    public void GetPartner_ShouldReturnPartnerDetails(int partnerId)
    {
        // Arrange

        var result = PartnersFixture.GetOnePartner();

        var partner = PartnersFixture.GetOnePartner().Result;

        _partnerRepository.Setup(repo => repo.GetPartner(partnerId))
            .Returns(PartnersFixture.GetOnePartner());

        // Act
        var objectResult = _partnersController.GetPartner(partnerId);
        var partnerResultActual = (objectResult as ObjectResult)?.Value;

        // Assert
        partnerResultActual.Should().BeEquivalentTo(result);
        objectResult.Should().BeOfType<OkObjectResult>();
    }

    #endregion

    #region Action Partner Test
    [Fact]
    public async Task ActionPartner_ShouldAddPartner()
    {
        // Arrange 
        var partnerFormDto = PartnersFixture.GetPartnerAfterAction().Result;

        _partnerRepository.Setup(repo => repo.ActionPartner(partnerFormDto))
            .Returns(PartnersFixture.GetPartnerAfterAction());

        // Act 
        var result = (ObjectResult)await _partnersController.ActionPartner(partnerFormDto);
        var partnerActionActual = (result.Value as OperationResult<GenericOperationResult, PartnersFormDto>);

        // Assert
        partnerActionActual.Result.Should().BeEquivalentTo(partnerFormDto);
        result.Should().BeOfType<OkObjectResult>();
        partnerActionActual.IsSuccess.Should().BeTrue();
    }
    [Fact]
    public async Task ActionPartner_ShouldUpdatePartner()
    {
        // Arrange
        var partnerFormDto = PartnersFixture.GetPartnerAfterAction().Result;

        _partnerRepository.Setup(repo => repo.ActionPartner(partnerFormDto))
            .Returns(PartnersFixture.GetPartnerAfterAction());

        // Act
        var result = await _partnersController.ActionPartner(partnerFormDto);
        var partnerObjectResult = (ObjectResult)await _partnersController.ActionPartner(partnerFormDto);
        var partnerActionResult = (partnerObjectResult.Value as OperationResult<GenericOperationResult,
            PartnersFormDto>);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
        partnerActionResult.Result.Should().BeEquivalentTo(partnerFormDto);
        partnerActionResult.IsSuccess.Should().BeTrue();
    }
    #endregion

    #region Remove Partner Test
    [Fact]
    public void RemovePartner_ShouldRemovePartner()
    {
        // Arrange
        _partnerRepository.Setup(repo => repo.RemovePartner(It.IsAny<int>()))
            .Returns(new OperationResult<GenericOperationResult>());

        // Act
        var result = _partnersController.RemovePartner(It.IsAny<int>());
        var removePartnerResultActual = ((ObjectResult)result).Value;

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion
}

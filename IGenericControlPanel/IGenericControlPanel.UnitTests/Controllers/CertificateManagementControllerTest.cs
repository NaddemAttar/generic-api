﻿
using AutoMapper;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.UserExperiences.Dto.Certificate;
using IGenericControlPanel.UserExperiences.IData.Interfaces;

namespace IGenericControlPanel.UnitTests.Controllers;
public class CertificateManagementControllerTest
{
    #region Constructor & Properties
    private readonly Mock<ICertificateRepository> _certificateRepository;
    private readonly Mock<IHttpFileService> _httpFileService;
    private readonly Mock<IMapper> _mapper;
    private readonly Mock<IConfiguration> _configuration;
    private Mock<IStringLocalizer<CertificateManagementController>> _localizer;
    private CertificateManagementController certificateManagementController;

    public CertificateManagementControllerTest()
    {
        _certificateRepository = new Mock<ICertificateRepository>();
        _httpFileService = new Mock<IHttpFileService>();
        _mapper = new Mock<IMapper>();
        _localizer = new Mock<IStringLocalizer<CertificateManagementController>>();
        _configuration = new Mock<IConfiguration>();
        certificateManagementController = new CertificateManagementController(
            _certificateRepository.Object,
            _localizer.Object,
            _configuration.Object,
            _httpFileService.Object,
            _mapper.Object);
    }
    #endregion

    #region Get Test
    [Fact]
    public async Task GetCertificateForUser_ShouldReturnSuccessRespose()
    {
        // Arrange 
        _certificateRepository.Setup(repo => repo.GetMultipleAsync())
            .ReturnsAsync(CertificateManagementFixture.GetCertificates());

        // Act
        var result = await certificateManagementController.GetCertificatesForUser();

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public async Task GetOneCertificate_ShouldReturnSuccessRespose(int id)
    {
        // Arrange 
        _certificateRepository.Setup(repo => repo.GetDetailsAsync(id))
            .ReturnsAsync(CertificateManagementFixture.GetOneCertificate());

        // Act
        var result = await certificateManagementController.GetCertificate(id);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion

    #region Remove Test
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public async Task RemoveCertificate_ShouldReturnSuccessRespose(int id)
    {
        // Arrange 
        _certificateRepository.Setup(repo => repo.RemoveAsync(id))
            .ReturnsAsync(new OperationResult<GenericOperationResult>());

        // Act
        var result = await certificateManagementController.RemoveCertificate(id);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion

    #region Action Test
    [Fact]
    public async Task AddCertificate_ShouldReturnSuccessResponse()
    {
        // Arrange
        var dto = CertificateManagementFixture.GetCertificateAddDto();
        var fileDtos = new List<FileDetailsDto>();

        _certificateRepository.Setup(repo => repo.AddAsync(dto, fileDtos))
            .ReturnsAsync(new OperationResult<GenericOperationResult, CertificateBasicDto>());

        // Act

        var result = await certificateManagementController.AddCertificate(dto);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }

    [Fact]
    public async Task UpdateCertificate_ShouldReturnSuccessResponse()
    {
        // Arrange
        var dto = CertificateManagementFixture.GetCertificateUpdateDto();
        var fileDtos = new List<FileDetailsDto>();

        _certificateRepository.Setup(repo => repo.UpdateAsync(dto, fileDtos, dto.RemoveFileIds))
            .ReturnsAsync(new OperationResult<GenericOperationResult, CertificateBasicDto>());

        // Act

        var result = await certificateManagementController.UpdateCertificate(dto);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion
}

﻿namespace IGenericControlPanel.UnitTests.Controllers;
public class CouponManagementControllerTest
{
    #region Constructor & Properties
    private Mock<ICouponManagementRepository> _couponManagementRepository;
    private Mock<IStringLocalizer<CouponManagementController>> _localizer;
    private CouponManagementController _couponManagementController;

    public CouponManagementControllerTest()
    {
        _couponManagementRepository = new Mock<ICouponManagementRepository>();
        _localizer = new Mock<IStringLocalizer<CouponManagementController>>();
        _couponManagementController = new CouponManagementController(
            _couponManagementRepository.Object,
            _localizer.Object);
    }
    #endregion

    #region Get Section Test
    [Theory]
    [InlineData(true, 0, 8, null)]
    public async Task GetCoupons_ShouldReturnSuccessResponse(
        bool pagination, int pageNumebr, int pageSize, string query)
    {
        // Arrange
        var couponsData = CouponManagementFixture.GetCouponsData();
        _couponManagementRepository.Setup(repo => repo.GetCouponsAsync(It.IsAny<PaginationDto>()))
            .ReturnsAsync(couponsData);

        // Act
        var result = await _couponManagementController.GetCoupons(
            pagination, pageNumebr, pageSize, query);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }

    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    [InlineData(3)]
    public async Task GetCouponDetails_ShouldReturnSuccessResponse(int id)
    {
        // Arrange
        var getOneCoupon = CouponManagementFixture.GetOneCoupon();
        _couponManagementRepository.Setup(repo => repo.GetCouponDetailsAsync(id))
            .ReturnsAsync(getOneCoupon);

        // Act
        var result = await _couponManagementController.GetCouponDetails(id);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }

    #endregion

    #region Remove Coupon Test
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    [InlineData(3)]
    public async Task RemoveCoupon_ShouldReturnSuccessResponse(int id)
    {
        // Arrange
        _couponManagementRepository.Setup(repo => repo.RemoveCouponAsync(id))
            .ReturnsAsync(new OperationResult<GenericOperationResult>());

        // Act
        var result = await _couponManagementController.RemoveCoupon(id);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion

    #region Add New Coupon Test
    [Fact]
    public async Task AddCoupon_ShouldReturnSuccessResponse()
    {
        // Arrange
        var couponData = CouponManagementFixture.GetOneCoupon();
        _couponManagementRepository.Setup(repo => repo.AddCouponAsync(couponData.Result))
            .ReturnsAsync(couponData);

        // Act
        var result = await _couponManagementController.AddCoupon(couponData.Result);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion

    #region Update Coupon Test
    [Fact]
    public async Task UpdateCoupon_ShouldReturnSuccessResponse()
    {
        // Arrange
        var couponData = CouponManagementFixture.GetOneCoupon();
        _couponManagementRepository.Setup(repo => repo.UpdateCouponAsync(couponData.Result))
            .ReturnsAsync(couponData);

        // Act
        var result = await _couponManagementController.UpdateCoupon(couponData.Result);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion
}

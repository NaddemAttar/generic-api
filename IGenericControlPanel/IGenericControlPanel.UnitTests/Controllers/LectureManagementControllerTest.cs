﻿
using AutoMapper;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.UserExperiences.Dto.Lecture;
using IGenericControlPanel.UserExperiences.IData.Interfaces;

namespace IGenericControlPanel.UnitTests.Controllers;
public class LectureManagementControllerTest
{
    #region Constructor & Properties
    private readonly Mock<ILectureRepository> _lectureRepository;
    private readonly Mock<IHttpFileService> _httpFileService;
    private readonly Mock<IConfiguration> _configuration;
    private readonly Mock<IMapper> _mapper;
    private readonly Mock<IStringLocalizer<LectureManagementController>> _localizer;
    private readonly LectureManagementController lectureManagementController;
    public LectureManagementControllerTest()
    {
        _lectureRepository = new Mock<ILectureRepository>();
        _httpFileService = new Mock<IHttpFileService>();
        _configuration = new Mock<IConfiguration>();
        _mapper = new Mock<IMapper>();
        _localizer = new Mock<IStringLocalizer<LectureManagementController>>();
        lectureManagementController = new LectureManagementController(
            _localizer.Object,
            _configuration.Object,
            _lectureRepository.Object,
            _httpFileService.Object,
            _mapper.Object);
    }
    #endregion

    #region Get Test
    [Fact]
    public async Task GetLectureForUser_ShouldReturnSuccessResponse()
    {
        // Arrange 
        _lectureRepository.Setup(repo => repo.GetMultipleAsync())
            .ReturnsAsync(LectureManagementFixture.GetLectureDtos());

        // Act
        var result = await lectureManagementController.GetLecturesForUser();

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }

    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public async Task GetOneLecture_ShouldReturnSuccessResponse(int id)
    {
        // Arrange 
        _lectureRepository.Setup(repo => repo.GetDetailsAsync(id))
            .ReturnsAsync(LectureManagementFixture.GetOneLectureDto());

        // Act
        var result = await lectureManagementController.GetOneLecture(id);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion

    #region Remove Test
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public async Task RemoveLecture_ShouldReturnSuccessResponse(int id)
    {
        // Arrange 
        _lectureRepository.Setup(repo => repo.RemoveAsync(id))
            .ReturnsAsync(new OperationResult<GenericOperationResult>());

        // Act
        var result = await lectureManagementController.RemoveLecture(id);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion

    #region Action
    [Fact]
    public async Task AddLecture_ShouldReturnSuccessResponse()
    {
        // Arrange 
        List<FileDetailsDto> files = new List<FileDetailsDto>();
        var addDto = LectureManagementFixture.GetLectureAddDto();

        _lectureRepository.Setup(repo => repo.AddAsync(
            It.IsAny<LectureBasicDto>(),
            It.IsAny<List<FileDetailsDto>>()))
            .ReturnsAsync(LectureManagementFixture.GetLectureBasicDto());

        // Act
        var result = await lectureManagementController.AddLecture(addDto);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }

    [Fact]
    public async Task UpdateLecture_ShouldReturnSuccessResponse()
    {
        // Arrange 
        List<FileDetailsDto> files = new();
        List<int> removeFileIds = new();
        var updateDto = LectureManagementFixture.GetLectureUpdateDto();

        _lectureRepository.Setup(repo => repo.UpdateAsync(
            It.IsAny<LectureUpdateDto>(),
            It.IsAny<List<FileDetailsDto>>(),
            It.IsAny<List<int>>()))
            .ReturnsAsync(LectureManagementFixture.GetLectureBasicDto());

        // Act
        var result = await lectureManagementController.UpdateLecture(updateDto);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion
}

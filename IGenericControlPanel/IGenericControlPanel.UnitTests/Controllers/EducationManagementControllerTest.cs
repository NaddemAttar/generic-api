﻿
using AutoMapper;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.UserExperiences.Dto.Education;
using IGenericControlPanel.UserExperiences.IData.Interfaces;

namespace IGenericControlPanel.UnitTests.Controllers;
public class EducationManagementControllerTest
{
    #region Constructor & Properties
    private readonly Mock<IEducationRepository> _educationRepository;
    private readonly Mock<IHttpFileService> _httpFileService;
    private readonly Mock<IConfiguration> _configuration;
    private readonly Mock<IMapper> _mapper;
    private readonly Mock<IStringLocalizer<EducationManagementController>> _localizer;
    private readonly EducationManagementController educationManagementController;
    public EducationManagementControllerTest()
    {
        _educationRepository = new Mock<IEducationRepository>();
        _httpFileService = new Mock<IHttpFileService>();
        _configuration = new Mock<IConfiguration>();
        _mapper = new Mock<IMapper>();
        _localizer = new Mock<IStringLocalizer<EducationManagementController>>();
        educationManagementController = new EducationManagementController(
            _localizer.Object,
            _educationRepository.Object,
            _httpFileService.Object,
            _mapper.Object,
            _configuration.Object);
    }
    #endregion

    #region Get Test
    [Fact]
    public async Task GetEducationsForUser_ShouldReturnSuccessResponse()
    {
        // Arrange 
        _educationRepository.Setup(repo => repo.GetMultipleAsync())
            .ReturnsAsync(EducationManagementFixture.GetEducations());

        // Act
        var result = await educationManagementController.GetEducationsForUser();

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }

    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public async Task GetOneEducation_ShouldReturnSuccessResponse(int id)
    {
        // Arrange 
        _educationRepository.Setup(repo => repo.GetDetailsAsync(id))
            .ReturnsAsync(EducationManagementFixture.GetOneEducation());

        // Act
        var result = await educationManagementController.GetOneEducation(id);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion

    #region Remove Test
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public async Task RemoveEducation_ShouldReturnSuccessResponse(int id)
    {
        // Arrange 
        _educationRepository.Setup(repo => repo.RemoveAsync(id))
            .ReturnsAsync(new OperationResult<GenericOperationResult>());

        // Act
        var result = await educationManagementController.RemoveEducation(id);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion

    #region Action
    [Fact]
    public async Task AddEducation_ShouldReturnSuccessResponse()
    {
        // Arrange 
        List<FileDetailsDto> files = new List<FileDetailsDto>();
        var addDto = EducationManagementFixture.GetEducationAddDto();
        _educationRepository.Setup(repo => repo.AddAsync(
            It.IsAny<EducationBasicDto>(),
            It.IsAny<List<FileDetailsDto>>()))
            .ReturnsAsync(EducationManagementFixture.GetEducationBasicDto());

        // Act
        var result = await educationManagementController.AddEducation(addDto);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }

    [Fact]
    public async Task UpdateEducation_ShouldReturnSuccessResponse()
    {
        // Arrange 
        List<FileDetailsDto> files = new();
        List<int> removeFileIds = new();
        var updateDto = EducationManagementFixture.GetEducationUpdateDto();
        _educationRepository.Setup(repo => repo.UpdateAsync(
            It.IsAny<EducationUpdateDto>(),
            It.IsAny<List<FileDetailsDto>>(),
            It.IsAny<List<int>>()))
            .ReturnsAsync(EducationManagementFixture.GetEducationBasicDto());

        // Act
        var result = await educationManagementController.UpdateEducation(updateDto);

        // Assert
        result.Should().BeOfType<OkObjectResult>();
    }
    #endregion
}

﻿
using IGenericControlPanel.UserExperiences.Dto.License;
using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.UnitTests.Fixtures;
public class LicenseManagementFixture
{
    public static OperationResult<GenericOperationResult, IEnumerable<LicenseDto>> GetLicenses()
    {
        return new OperationResult<GenericOperationResult, IEnumerable<LicenseDto>>
        {
            Result = new List<LicenseDto>()
            {
                new()
                {
                   Id = 1,
                   Name = "name",
                   Description = "desc",
                   LicenseUrl = "image-url"
                },
                new()
                {
                   Id = 1,
                   Name = "name2",
                   Description = "desc2",
                   LicenseUrl = "image-url2"
                },
            }
        };
    }
    public static OperationResult<GenericOperationResult, LicenseDetailsDto> GetOneLicense()
    {
        return new OperationResult<GenericOperationResult, LicenseDetailsDto>
        {
            Result = new LicenseDetailsDto
            {
                Id = 1,
                Name = "name",
                Description = "desc",
            }
        };
    }
    public static LicenseAddDto GetLicenseAddDto()
    {
        return new LicenseAddDto
        {
            Name = "new-name",
            Description = "description",
            LicenseFiles = new List<IFormFile>()
        };
    }
    public static LicenseUpdateDto GetLicenseUpdateDto()
    {
        return new LicenseUpdateDto
        {
            Id = 4,
            Name = "edit-name22222",
            Description = "description2222222",
            LicenseFiles = new List<IFormFile>()
        };
    }
    public static OperationResult<GenericOperationResult, LicenseBasicDto> GetLicenseBasicDto()
    {
        return new OperationResult<GenericOperationResult, LicenseBasicDto>
        {
            Result = new LicenseBasicDto
            {
                Name = "name",
                Description = "description",
            }
        };
    }
}

﻿
using IGenericControlPanel.UserExperiences.Dto.Lecture;
using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.UnitTests.Fixtures;
public class LectureManagementFixture
{
    public static OperationResult<GenericOperationResult, IEnumerable<LectureDto>> GetLectureDtos()
    {
        return new OperationResult<GenericOperationResult, IEnumerable<LectureDto>>
        {
            Result = new List<LectureDto>
            {
                new()
                {
                    Id = 1,
                    Title = "Title",
                    Description = "dfg",
                    Link = "dfs",
                },
                new()
                {
                    Id = 2,
                    Title = "Titledfgdf",
                    Description = "dfgfff",
                    Link = "dffffffffs",
                },
            }
        };
    }

    public static OperationResult<GenericOperationResult, LectureDetailsDto> GetOneLectureDto()
    {
        return new OperationResult<GenericOperationResult, LectureDetailsDto>
        {
            Result = new LectureDetailsDto
            {
                Id = 1,
                Description = "dfg",
                Link = "dfs",
                Title = "Title"
            }
        };
    }

    public static LectureAddDto GetLectureAddDto()
    {
        return new LectureAddDto
        {
            Title = "Title",
            Description = "dsaadas",
            Link = "asd",
        };
    }

    public static LectureUpdateDto GetLectureUpdateDto()
    {
        return new LectureUpdateDto
        {
            Id = 4,
            Description = "aaaaaaaaaaaaaaaaaaaaaa",
            Link = "aaaaaaaaaaaaaaaaaaa",
            Title = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            LectureFiles = new List<IFormFile>(),
        };
    }
    public static OperationResult<GenericOperationResult, LectureBasicDto> GetLectureBasicDto()
    {
        return new OperationResult<GenericOperationResult, LectureBasicDto>
        {
            Result = new LectureBasicDto
            {
                Id = 1,
                Link = "link",
                Title = "title",
                Description = "desc"
            }
        };
    }
}

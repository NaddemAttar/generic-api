﻿
using IGenericControlPanel.UserExperiences.Dto.Education;
using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.UnitTests.Fixtures;
public class EducationManagementFixture
{
    public static OperationResult<GenericOperationResult, IEnumerable<EducationDto>> GetEducations()
    {
        return new OperationResult<GenericOperationResult, IEnumerable<EducationDto>>
        {
            Result = new List<EducationDto>()
            {
                new()
                {
                    Id = 1,
                    SchoolName = "school name",
                    Description = "description",
                    Link = "https://www.youtube.com/watch?v=0J0sbZDimjE&list=RDEM0NJ2DTM97Kfk6Pbzo5_OCA&index=2",
                    StudyingField = "studing-field-id",
                    ScientificDegree = "degree",
                    Grade = "grade",
                    StartDate = DateTime.UtcNow,
                    EndDate = DateTime.UtcNow.AddDays(1)
                },
                new()
                {
                    Id = 2,
                    SchoolName = "school name2",
                    Description = "description2",
                    Link = "https://youtube.com/",
                    StudyingField = "studing-field-id2",
                    ScientificDegree = "degree2",
                    Grade = "grade2",
                    StartDate = DateTime.UtcNow,
                    EndDate = DateTime.UtcNow.AddDays(3)
                }
            }
        };
    }
    public static OperationResult<GenericOperationResult, EducationDetailsDto> GetOneEducation()
    {
        return new OperationResult<GenericOperationResult, EducationDetailsDto>
        {
            Result = new EducationDetailsDto
            {
                Id = 1,
                SchoolName = "school name",
                Description = "description",
                Link = "https://www.youtube.com/watch?v=0J0sbZDimjE&list=RDEM0NJ2DTM97Kfk6Pbzo5_OCA&index=2",
                StudyingField = "studing-field-id",
                ScientificDegree = "degree",
                Grade = "grade",
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(1)
            }
        };
    }
    public static EducationAddDto GetEducationAddDto()
    {
        return new EducationAddDto
        {
            Description = "description",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow.AddDays(2),
            Link = "https://www.youtube.com/",
            Grade = "grade",
            SchoolName = "school-name",
            ScientificDegree = "scientific-degree",
            StudyingField = "studing-field",
            EducationFiles = new List<IFormFile>()
        };
    }
    public static EducationUpdateDto GetEducationUpdateDto()
    {
        return new EducationUpdateDto
        {
            Id = 4,
            Description = "description2",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow.AddDays(5),
            Link = "https://www.youtube.com/",
            Grade = "grade2",
            SchoolName = "school-name2",
            ScientificDegree = "scientific-degree2",
            StudyingField = "studing-field2",
            EducationFiles = new List<IFormFile>()
        };
    }
    public static OperationResult<GenericOperationResult, EducationBasicDto> GetEducationBasicDto()
    {
        return new OperationResult<GenericOperationResult, EducationBasicDto>
        {
            Result = new EducationBasicDto
            {
                Description = "description"
            }
        };
    }
}


﻿
namespace IGenericControlPanel.UnitTests.Fixtures;
public static class ApplyingCouponsSystemFixture
{
    public static OperationResult<GenericOperationResult, IEnumerable<CouponDto>> GetCouponsDto()
    {
        return new OperationResult<GenericOperationResult, IEnumerable<CouponDto>>
        {
            Result = new List<CouponDto>
            {
                new()
                {
                    CouponPeriod = 12,
                    Id = 2,
                    Percentage = 12,
                    Points = 100
                }
            }
        };
    }

    public static OperationResult<GenericOperationResult, IEnumerable<UserCouponDto>> GetUserCoupons()
    {
        return new OperationResult<GenericOperationResult, IEnumerable<UserCouponDto>>
        {
            Result = new List<UserCouponDto>
            {
                new()
                {
                    Id = 1,
                    Percentage = 10,
                    Points = 100
                },
                 new()
                {
                    Id = 2,
                    Percentage = 20,
                    Points = 200
                },
                  new()
                {
                    Id = 3,
                    Percentage = 30,
                    Points = 30
                },
            }
        };
    }
}

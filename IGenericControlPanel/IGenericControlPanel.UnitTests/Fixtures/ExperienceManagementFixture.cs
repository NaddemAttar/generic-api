﻿
using IGenericControlPanel.UserExperiences.Dto.Experience;
using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.UnitTests.Fixtures;
public static class ExperienceManagementFixture
{
    public static OperationResult<GenericOperationResult, IEnumerable<ExperienceDto>> GetExperienecDtos()
    {
        return new OperationResult<GenericOperationResult, IEnumerable<ExperienceDto>>
        {
            Result = new List<ExperienceDto>
            {
                new()
                {
                    Id = 1,
                    Description = "dfg",
                    StartDate = DateTime.UtcNow,
                    EndDate = null,
                    ExperienceImageUrl = null,
                    Institution = "asd",
                    Link = "dfs",
                    Name = "gfgd"
                },
                new()
                {
                    Id = 2,
                    Description = "ggggg",
                    StartDate = DateTime.UtcNow,
                    EndDate = DateTime.UtcNow,
                    ExperienceImageUrl = null,
                    Institution = "ggggg",
                    Link = "ggg",
                    Name = "vxcxvxcvx"
                },
            }
        };
    }

    public static OperationResult<GenericOperationResult, ExperienceDetailsDto> GetOneExperienecDto()
    {
        return new OperationResult<GenericOperationResult, ExperienceDetailsDto>
        {
            Result = new ExperienceDetailsDto
            {
                Id = 1,
                Description = "dfg",
                StartDate = DateTime.UtcNow,
                EndDate = null,
                Institution = "asd",
                Link = "dfs",
                Name = "gfgd"
            }
        };
    }

    public static ExperienceAddDto GetExperienceAddDto()
    {
        return new ExperienceAddDto
        {
            Description = "dsaadas",
            StartDate = DateTime.UtcNow,
            EndDate = null,
            Institution = "gdfgdf",
            Link = "asd",
            Name = "sdfdgdfg",
            ExperienceFiles = new List<IFormFile>()
        };
    }

    public static ExperienceUpdateDto GetExperienceUpdateDto()
    {
        return new ExperienceUpdateDto
        {
            Id = 7,
            Description = "aaaaaaaaaaaaaaaaaaaaaa",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow,
            Institution = "aaaaaaaaaaaa",
            Link = "aaaaaaaaaaaaaaaaaaa",
            Name = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            ExperienceFiles = new List<IFormFile>(),
            RemoveFileIds = new List<int>()
        };
    }
}

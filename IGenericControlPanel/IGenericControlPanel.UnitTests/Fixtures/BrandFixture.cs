﻿namespace IGenericControlPanel.UnitTests.Fixtures;
public static class BrandFixture
{
    public static OperationResult<GenericOperationResult, BrandDto> GetOneBrand()
    {
        return new OperationResult<GenericOperationResult, BrandDto>()
        {
            Result = new BrandDto
            {
                Id = 1,
                Name = "Nadim",
            }
        };
    }
    public static OperationResult<GenericOperationResult, IEnumerable<BrandDto>> GetBrands()
    {
        return new OperationResult<GenericOperationResult, IEnumerable<BrandDto>>()
        {
            Result = new List<BrandDto>
            {
                new BrandDto
                {
                    Id = 1,
                    Name = "name"
                },
                new BrandDto
                {
                    Id = 2,
                    Name = "name2"
                },
                new BrandDto
                {
                    Id = 3,
                    Name = "name3"
                },
                new BrandDto
                {
                    Id = 3,
                    Name = "name3"
                },
            }
        };
    }
}

﻿
using IGenericControlPanel.UserExperiences.Dto.Certificate;
using IGenericControlPanel.UserExperiences.Dto.License;

namespace IGenericControlPanel.UnitTests.Fixtures;
public class CertificateManagementFixture
{
    public static OperationResult<GenericOperationResult, IEnumerable<CertificateDto>> GetCertificates()
    {
        return new OperationResult<GenericOperationResult, IEnumerable<CertificateDto>>
        {
            Result = new List<CertificateDto>()
            {
                new()
                {
                    Id = 1,
                    Title = "title",
                    Institution = "instituation1",
                    Description = "description",
                    CertificateImageUrl = "image-url"
                },
                 new()
                {
                    Id = 1,
                    Title = "title",
                    Institution = "instituation1",
                    Description = "description",
                    CertificateImageUrl = "image-url"
                },
            }
        };
    }
    public static OperationResult<GenericOperationResult, CertificateDetailsDto> GetOneCertificate()
    {
        return new OperationResult<GenericOperationResult, CertificateDetailsDto>
        {
            Result = new CertificateDetailsDto
            {
                Id = 1,
                Title = "title",
                Description = "description",
                Institution = "Instis"
            }
        };
    }
    public static CertificateAddDto GetCertificateAddDto()
    {
        return new CertificateAddDto
        {
            Id = 1,
            Title = "name",
            Institution = "instis",
            Description = "description",
        };
    }
    public static CertificateUpdateDto GetCertificateUpdateDto()
    {
        return new CertificateUpdateDto
        {
            Id = 4,
            Title = "title222222",
            Description = "description2222222",
            Institution = "instit222222"
        };
    }
    public static OperationResult<GenericOperationResult, CertificateBasicDto> GetCertificateBasicDto()
    {
        return new OperationResult<GenericOperationResult, CertificateBasicDto>
        {
            Result = new CertificateBasicDto
            {
                Title = "name",
                Description = "description",
                Institution = "Institation"
            }
        };
    }
}

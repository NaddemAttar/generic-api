﻿
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.UnitTests.Fixtures;
public static class GamificationManagmentsFixture
{
    public static OperationResult<GenericOperationResult, IEnumerable<GamificationDto>> GetGamificationSectionsData()
    {
        return new OperationResult<GenericOperationResult, IEnumerable<GamificationDto>>
        {
            Result = new List<GamificationDto>
            {
                new()
                {
                    Enable = true,
                    Key = "key1",
                    Value = 12,
                    GamificationType = GamificationType.Hcp
                },
                new()
                {
                    Enable = false,
                    Key = "key2",
                    Value = 10,
                    GamificationType = GamificationType.Hcp
                },
                new()
                {
                    Enable = true,
                    Key = "key3",
                    Value = 4,
                    GamificationType = GamificationType.NormalUser
                },
                new()
                {
                    Enable = false,
                    Key = "key4",
                    Value = 2,
                    GamificationType = GamificationType.Hcp
                },
                new()
                {
                    Enable = true,
                    Key = "key5",
                    Value = 1,
                    GamificationType = GamificationType.Hcp
                },
                new()
                {
                    Enable = false,
                    Key = "key6",
                    Value = 30,
                    GamificationType = GamificationType.NormalUser
                },
            }
        };
    }
    public static OperationResult<GenericOperationResult, GamificationDto> GetOneGamification()
    {
        return new OperationResult<GenericOperationResult, GamificationDto>
        {
            Result = new()
            {
                Key = "key1",
                Value = 12,
                Enable = true,
                GamificationType = GamificationType.Hcp
            }
        };
    }
    public static IEnumerable<string> GetGamificationKeys()
    {
        return new List<string>
        {
            GamificationNames.PublishCourseOnFB 
        };
    }
    public static OperationResult<GenericOperationResult, IEnumerable<UserWithPointsDto>> GetUsersPointsDto()
    {
        return new OperationResult<GenericOperationResult, IEnumerable<UserWithPointsDto>>
        {
            Result = new List<UserWithPointsDto>
            {
                new()
                {
                    Id = 1,
                    FirstName = "nadim",
                    LastName = "attar",
                    UserName = "nadim-cr7",
                    Gender = Gender.Male,
                    Points = 500
                },
                 new()
                {
                    Id = 2,
                    FirstName = "samer",
                    LastName = "hamood",
                    UserName = "had",
                    Gender = Gender.Male,
                    Points = 20
                },
                  new()
                {
                    Id = 3,
                    FirstName = "sedra",
                    LastName = "mohamed",
                    UserName = "sadora",
                    Gender = Gender.Female,
                    Points = 200
                },
                   new()
                {
                    Id = 4,
                    FirstName = "Rand",
                    LastName = "Dabit",
                    UserName = "Ranood",
                    Gender = Gender.Female,
                    Points = 450
                },
            }
        };
    }

    public static FiltersUsersDto InitializeFiltersData()
    {
        return new FiltersUsersDto
        {
            EnablePagination = false,
            IsAscendingOrder = true,
            UsersHavePointsOnly = true,
            PageNumber = 0,
            PageSize = 8
        };
    }
}

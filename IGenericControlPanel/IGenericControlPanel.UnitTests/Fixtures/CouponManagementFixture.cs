﻿
namespace IGenericControlPanel.UnitTests.Fixtures;
public static class CouponManagementFixture
{
    public static OperationResult<GenericOperationResult, IEnumerable<CouponDto>> GetCouponsData()
    {
        return new OperationResult<GenericOperationResult, IEnumerable<CouponDto>>
        {
            Result = new List<CouponDto>
            {
                new()
                {
                    Id = 1,
                    CouponPeriod = 3,
                    Percentage = 10,
                    Points = 2000
                },
                new()
                {
                    Id = 1,
                    CouponPeriod = 3,
                    Percentage = 10,
                    Points = 2000
                },
            }
        };
    }

    public static OperationResult<GenericOperationResult, CouponDto> GetOneCoupon()
    {
        return new OperationResult<GenericOperationResult, CouponDto>
        {
            Result = new()
            {
                Id = 1,
                CouponPeriod = 3,
                Percentage = 10,
                Points = 2000
            }
        };
    }

}

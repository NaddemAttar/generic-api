﻿namespace IGenericControlPanel.UnitTests.Fixtures;
public static class PartnersFixture
{
    public static OperationResult<GenericOperationResult, IEnumerable<PartnersDto>> GetPartners()
    {
        return new OperationResult<GenericOperationResult, IEnumerable<PartnersDto>>
        {
            Result = new List<PartnersDto>
            {
                new()
                {
                   Id = 2,
                   ImageUrl = "url2",
                   Link = "link2",
                   Name = "name2"
                },
                new()
                {
                   Id = 2,
                   ImageUrl = "url2",
                   Link = "link2",
                   Name = "name2"
                },
                new()
                {
                   Id = 3,
                   ImageUrl = "url3",
                   Link = "link3",
                   Name = "name3"
                },
            }
        };
    }
    public static OperationResult<GenericOperationResult, PartnersDto> GetOnePartner()
    {
        return new OperationResult<GenericOperationResult, PartnersDto>
        {
            Result = new()
            {
                Id = 1,
                ImageUrl = "image1",
                Link = "link1",
                Name = "name1"
            }
        };
    }
    public static OperationResult<GenericOperationResult, PartnersFormDto> GetPartnerAfterAction()
    {
        return new OperationResult<GenericOperationResult, PartnersFormDto>
        {
            Result = new()
            {
                Id = 1,
                ImageUrl = "image1",
                Link = "link1",
                Name = "name1"
            }
        };
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Order;
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Core.IData
{
    public interface IOrderRepository
    {
        OperationResult<GenericOperationResult, OrderDto> GetOrder(int id);
        OperationResult<GenericOperationResult, IEnumerable<OrderDto>> GetOrders(
           int pageNumber = 0, int pageSize = 8, int userId = 0);
        Task<OperationResult<GenericOperationResult, OrderFormDto>> ActionOrder(OrderFormDto formDto);
        OperationResult<GenericOperationResult> RemoveOrder(int id);
        OperationResult<GenericOperationResult> ChangeOrderStatus(
            int orderId, PaymentStatus paymentStatus);
    }
}

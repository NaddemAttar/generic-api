﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Brand;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface IBrandRepository
    {
        Task<OperationResult<GenericOperationResult, IEnumerable<BrandDto>>> GetBrandsAsync(
            int pageNumber = 0, int pageSize = 5);
        Task<OperationResult<GenericOperationResult, IEnumerable<BrandDto>>> GetAllBrandsAsync();
        Task<OperationResult<GenericOperationResult, BrandDto>> GetBrandByIdAsync(int BrandId);
        Task<OperationResult<GenericOperationResult, BrandDto>> ActionBrandAsync(
            BrandFormDto formDto);
        Task<OperationResult<GenericOperationResult>> RemoveBrandAsync(int BrandId);
    }
}

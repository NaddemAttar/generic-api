﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Question;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface IQuestionRepository
    {
        FileDetailsDto GetFile(string Url);
        //int GetTotalCountForBlogs();
        Task<OperationResult<GenericOperationResult, IEnumerable<QuestionDetailsDto>>> GetAllDetailedPaged(
            bool? showFirst, bool? isHidden, int? PostTypeId, int? MultiLevelCategoryId, int? UserId,
            List<int> serviceIds, int? GroupId, bool WithAnswersOnly, bool FAQ, string query, int pageNumber,
            int pageSize, string cultureCode = "ar", bool enablePagination = true);
        Task<OperationResult<GenericOperationResult,IEnumerable<QuestionDetailsDto>>> GetQuestionAutoComplete(string query);
        Task<OperationResult<GenericOperationResult, AnswerFormDto>> ActionAnswerQuestion(AnswerFormDto formDto);
        Task<OperationResult<GenericOperationResult>> RemoveAnswer(int id);
        Task<OperationResult<GenericOperationResult, QuestionFormDto>> Action(QuestionFormDto formDto);
        OperationResult<GenericOperationResult, QuestionDetailsDto> GetDetailed(int id, string cultureCode = "ar");
        Task<OperationResult<GenericOperationResult>> Remove(int id);
    }
}
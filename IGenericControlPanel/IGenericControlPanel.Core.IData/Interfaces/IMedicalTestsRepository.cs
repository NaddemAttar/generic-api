﻿using System.Collections.Generic;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface IMedicalTestsRepository
    {
        OperationResult<GenericOperationResult, MedicalTestsDto> Action(MedicalTestsDto medicalTestsDto);
        OperationResult<GenericOperationResult, IEnumerable<MedicalTestsDto>> GetAllMedicalTests();
        Task<OperationResult<GenericOperationResult, IEnumerable<MedicalTestsDto>>> GetAllUserMedicalTests(
            int? userId, bool enablePagination, int pageSize, int pageNumber);
        OperationResult<GenericOperationResult, MedicalTestsDto> GetMedicalTest(int Id);
        Task<OperationResult<GenericOperationResult>> RemoveMedicalTest(int Id);
    }
}

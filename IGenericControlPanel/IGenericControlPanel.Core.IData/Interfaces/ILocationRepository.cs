﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Course;
using IGenericControlPanel.Core.Dto.Location;
using IGenericControlPanel.Core.Dto.Location.LocationSchedule;
using IGenericControlPanel.SharedKernal.Interfaces.RepositoryNew;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface ILocationRepository
    {
        Task<OperationResult<GenericOperationResult, IEnumerable<LocationScheduleDto>>> GetLocationsSchedule(
            DateTime? fromDate, DateTime? toDate, int cityId, bool ignoreOldDates = false, bool orderFromNewestToOldest = false);
        Task<OperationResult<GenericOperationResult, LocationScheduleDto>> ActionLocationSchedule(LocationScheduleFormDto locationSchedule);
        OperationResult<GenericOperationResult, LocationScheduleDto> GetLocationScheduleForDate(DateTime date);
        OperationResult<GenericOperationResult, IEnumerable<CountryDto>> GetCountries();
        OperationResult<GenericOperationResult, IEnumerable<CityDto>> GetCities(int countryId);
        Task<OperationResult<GenericOperationResult>> RemoveLocationSchedule(int LocationScheduleId);
        OperationResult<GenericOperationResult, IEnumerable<LocationDto>> GetAll(string cultureCode = "ar");
        OperationResult<GenericOperationResult, LocationDto> Action(LocationDto formDto);
        OperationResult<GenericOperationResult> Remove(int id);
    }
}

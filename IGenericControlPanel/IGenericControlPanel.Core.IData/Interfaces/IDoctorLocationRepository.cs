﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Location;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface IDoctorLocationRepository
    {
        public OperationResult<GenericOperationResult, DoctorLocationDto> ActionDoctorLocation(DoctorLocationDto formDto);
        public OperationResult<GenericOperationResult, IEnumerable<DoctorLocationDto>> GetAllDoctorLocations();
        public OperationResult<GenericOperationResult, DoctorLocationDto> GetDoctorLocation(int Id);
        public OperationResult<GenericOperationResult> RemoveDoctorLocation(int Id);
    }
}

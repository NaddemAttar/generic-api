﻿
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.Dto.Blog;

namespace IGenericControlPanel.Core.IData
{
    public interface IBlogRepository
    {
        Task<OperationResult<GenericOperationResult, BlogFormDto>> ActionAsync(BlogFormDto formDto);
        Task<OperationResult<GenericOperationResult, BlogDto>> GetAsync(
            int id, string cultureCode = "ar");
        Task<OperationResult<GenericOperationResult, IEnumerable<MultiLevelCategoryEntityDto>>> GetBlogCategoriesAsync(int BlogId);
        Task<OperationResult<GenericOperationResult, IEnumerable<BlogDetailsDto>>> GetAllDetailedPagedAsync
            (bool? showFirst, bool? isHidden,
            int? PostTypeId, int? MultiLevelCategoryId, int? ServiceId, int? GroupId,
            string query, int? pageNumber = 0, int? pageSize = null, string cultureCode = "ar");
        Task<OperationResult<GenericOperationResult>> RemoveAsync(int id);
        Task<OperationResult<GenericOperationResult, FileDetailsDto>> ActionFileAsync(
            FileDetailsDto file);
    }
}


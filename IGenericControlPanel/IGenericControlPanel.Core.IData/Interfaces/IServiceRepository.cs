﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.Dto.HealthCareProvider;
using IGenericControlPanel.Core.Dto.Service;

namespace IGenericControlPanel.Core.IData
{
    public interface IServiceRepository
    {
        Task<OperationResult<GenericOperationResult, IEnumerable<ServiceDto>>> GetAllAsync(
              bool? showFirst, bool? isHidden,int? SpecialtyId, string query, int pageSize, int pageNumber, bool enablePagination, int? UserId, int? EnterpriseId, string cultureCode = "ar");
        Task<OperationResult<GenericOperationResult, IEnumerable<ServiceDetailsDto>>> GetAllDetailedAsync(int? EnterpriseId,
            bool? showFirst, bool? isHidden,int? SpecialtyId, string query, int pageSize, int pageNumber, int? UserId, bool enablePagination, string cultureCode = "ar");
        Task<OperationResult<GenericOperationResult, ServiceFormDto>> ActionAsync(ServiceFormDto dto);
        Task<OperationResult<GenericOperationResult>> AddToMyServices(int ServiceId);
        Task<OperationResult<GenericOperationResult>> RemoveFromMyServices(int ServiceId);
        public Task<OperationResult<GenericOperationResult, IEnumerable<HealthCareProviderServicesDto>>> GetHealthCareProvidersByServiceId(int ServiceId);
        Task<OperationResult<GenericOperationResult>> RemoveAsync(int id);
        Task<OperationResult<GenericOperationResult, ServiceDetailsDto>> GetDetailedAsync(int id, int? EnterpriseId, int? UserId);
    }
}
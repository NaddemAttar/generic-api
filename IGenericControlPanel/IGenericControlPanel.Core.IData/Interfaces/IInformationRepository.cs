﻿using System;
using System.Collections.Generic;
using System.Text;

using IGenericControlPanel.Core.Dto.Information;
using IGenericControlPanel.SharedKernal.Interfaces;
using IGenericControlPanel.SharedKernal.Interfaces.RepositoryNew;

namespace IGenericControlPanel.Core.IData
{
    public interface IInformationRepository :
         IReadRepository<int, InfoDto>,
         IReadPagedRepository<int, InfoDto>,
         IReadPagedDetailedRepository<int, InfoDetailsDto>,
         IReadDetailedRepository<int, InfoDetailsDto>,
         IRemoveRepository<int>,
         IActionRepository<InfoFormDto>
    { }
}

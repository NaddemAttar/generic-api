﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Core.Dto.Mail;
using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface IMailRepository
    {
        public Task SendEmailAsync(MailRequest mailRequest);
        public Task SendEmailSubscriptions(MailSubscriptionsDto formDto);
        public Task SendEmailSubscriptionsByIds(MailUsersDto formDto);

    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Note;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface INoteRepository
    {
        OperationResult<GenericOperationResult, NoteDto> Action(NoteDto formDto);
        OperationResult<GenericOperationResult> ActionUserNote(IEnumerable<UserNoteDto> formDto, int UserId);
        OperationResult<GenericOperationResult, IEnumerable<NoteDto>> GetAllNotes();
        Task<OperationResult<GenericOperationResult, IEnumerable<NoteDto>>> GetAllUserNotes(int? userId);
        OperationResult<GenericOperationResult, NoteDto> GetNote(int Id);
        OperationResult<GenericOperationResult> RemoveNote(int Id);
        OperationResult<GenericOperationResult> RemoveUserNote(int Id);
    }
}

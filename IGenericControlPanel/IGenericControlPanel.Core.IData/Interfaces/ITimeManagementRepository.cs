﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Core.Dto.OpeningHoures;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface ITimeManagementRepository
    {
        Task<OperationResult<GenericOperationResult, bool>> EditOpeningHouresAsync(IEnumerable<OpeningHouresDto> openingHouresDtoList);
        Task<OperationResult<GenericOperationResult, IEnumerable<OpeningHouresDto>>> GetOpeningHouresAsync(int? userId, int? enterpriseId);
        Task<OperationResult<GenericOperationResult, IEnumerable<OpeningHouresDto>>> GetOpeningHouresForHealthProviderAsync(int? userId);
    }
}
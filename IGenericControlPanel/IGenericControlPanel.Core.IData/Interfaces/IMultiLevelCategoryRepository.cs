﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Core.Dto.Category;

namespace IGenericControlPanel.Core.IData
{
    public interface IMultiLevelCategoryRepository
    {
        Task<OperationResult<GenericOperationResult, MultiLevelCategoryDto>> ActionMultiLevelCategory(MultiLevelCategoryDto formDto);
        Task<OperationResult<GenericOperationResult, IEnumerable<MultiLevelCategoryDetailsDto>>> GetMultiLevelCategoriesNest();
        Task<OperationResult<GenericOperationResult, IEnumerable<MultiLevelCategoryDetailsDto>>> GetMultiLevelCategories(string query, bool WithProducts);
        Task<OperationResult<GenericOperationResult, IEnumerable<MultiLevelCategoryDto>>> GetMultiLevelCategoriesLeafs();
        Task<OperationResult<GenericOperationResult>> RemoveMultiLevelCategory(int id);
        Task<OperationResult<GenericOperationResult, IEnumerable<CategoryWithProductCountL>>> GetCategoriesWithChildrenCount();
        Task<OperationResult<GenericOperationResult, string>> GetMultiLevelCategoryPhotoById(int multiLevelCategoryId);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Core.Dto.Specifiation;
using IGenericControlPanel.SharedKernal.Interfaces.RepositoryNew;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface ISpecificationRepository : IReadRepository<int, SpecificationDto>,
        IReadDetailedRepository<int, SpecificationDto>,
        IRemoveRepository<int>,
        IActionRepository<SpecificationDto>
    { }
}

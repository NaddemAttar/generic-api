﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Core.Dto.BlogComment;
using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface IBlogCommentRepository
    {
        public Task<OperationResult<GenericOperationResult, BlogCommentDto>> AddComment(BlogCommentDto formDto);
        public Task<OperationResult<GenericOperationResult, BlogCommentDto>> UpdateComment(BlogCommentDto formDto);
        Task<OperationResult<GenericOperationResult, BlogComment>> RemoveComment(int CommentId);
        public Task<OperationResult<GenericOperationResult, IEnumerable<BlogCommentDto>>> GetBlogComments(int BlogId);
        public Task<OperationResult<GenericOperationResult, IEnumerable<BlogCommentDto>>> GetUserComments(int UserId);
        Task<OperationResult<GenericOperationResult, BlogComment>> GetComment(int commentId);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Subscription;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface ISubscriberRepository
    {
        public OperationResult<GenericOperationResult, SubscriberDto> AddSubscriber(SubscriberDto formDto);
        public OperationResult<GenericOperationResult, SubscriberDto> UpdateSubscriber(SubscriberDto formDto);
        public OperationResult<GenericOperationResult, IEnumerable<SubscriberDto>> GetAllSubscribers(string query);
        public OperationResult<GenericOperationResult,SubscriberDto> GetSubscriber(int id);
        public OperationResult<GenericOperationResult> RemoveSubscriber(int id);
    }
}

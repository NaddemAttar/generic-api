﻿using System.Collections.Generic;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;


using IGenericControlPanel.Core.Dto.Group;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface IGroupRepository
    {
        Task<OperationResult<GenericOperationResult, GroupMemberFormDto>> ActionGroupMemberAsync(GroupMemberFormDto groupMemberFormDto);
        Task<OperationResult<GenericOperationResult, IEnumerable<GroupMemberDto>>> GetAllGroupMembersAsync(
            int GroupId, int? pageNumber = 0, int? pageSize = null, bool enablePagination = true);
        Task<OperationResult<GenericOperationResult, IEnumerable<GroupDto>>> GetAllPagedAsync(
            int? pageNumber = 0, int? pageSize = null, string cultureCode = "ar");
        Task<OperationResult<GenericOperationResult, GroupDetailsDto>> GetDetailedAsync(int id, string cultureCode = "ar");
        Task<OperationResult<GenericOperationResult, GroupFormDto>> ActionAsync(GroupFormDto groupFormDto);
        Task<OperationResult<GenericOperationResult>> RemoveAsync(int id);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.PostType;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface IPostTypeRepository
    {
        public OperationResult<GenericOperationResult, PostTypeDto> ActionPostType(PostTypeDto formDto);
        public OperationResult<GenericOperationResult, IEnumerable<PostTypeDetailsDto>> GetPostTypes(bool WithBlogs, bool WithQustions, int pageNumber = 0, int pageSize = 5);
        public OperationResult<GenericOperationResult> RemovePostType(int Id);
        public OperationResult<GenericOperationResult, IEnumerable<PostTypeDto>> GetAllPostType();
        public OperationResult<GenericOperationResult,PostTypeDto> GetPostType(int Id);

    }
}

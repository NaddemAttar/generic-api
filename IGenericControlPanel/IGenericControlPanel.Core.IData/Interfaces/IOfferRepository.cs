﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Core.Dto.Offer;
using IGenericControlPanel.Security.Dto.User;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface IOfferRepository
    {
        public Task<OperationResult<GenericOperationResult, MinMaxPrice>> GetMinMaxPrice(List<int> BrandsIds, List<int> CategoriesIds);
        Task<OperationResult<GenericOperationResult, int>> ActionCreate(OfferDto offerDto);
        Task<OperationResult<GenericOperationResult, int>> ActionUpdate(OfferDto offerDto);
        Task<OperationResult<GenericOperationResult, IEnumerable<AllOfferDto>>> GetAllAsync(int pageNumber = 0, int pageSize = 5);
        Task<OperationResult<GenericOperationResult, OfferDetailsDto>> GetAsync(int Id);
        Task<OperationResult<GenericOperationResult, bool>> Remove(int Id);
    }
}
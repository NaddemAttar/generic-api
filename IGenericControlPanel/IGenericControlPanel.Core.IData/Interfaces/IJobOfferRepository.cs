﻿using System.Collections.Generic;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.JobOffer;
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface IJobOfferRepository
    {
        OperationResult<GenericOperationResult> ChangeJobStatus(int jobApplicationId, JobStatus status);
        OperationResult<GenericOperationResult, JobOfferFormDto> Action(JobOfferFormDto formDto);
        Task<OperationResult<GenericOperationResult, IEnumerable<JobOfferDto>>> GetAllJobOffers(JobOfferOptions jobOfferOptions);
        OperationResult<GenericOperationResult, JobOfferDto> GetJobOffer(int id);
        Task<OperationResult<GenericOperationResult, JobOfferDto>> RemoveJobOffer(int id);
        OperationResult<GenericOperationResult, JobApplicationsDto> ApplyToJobOffer(JobApplicationsDto formDto);
    }
}

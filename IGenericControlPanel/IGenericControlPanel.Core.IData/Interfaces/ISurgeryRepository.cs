﻿using System.Collections.Generic;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Surger;
using IGenericControlPanel.Core.Dto.Surgery;
using IGenericControlPanel.Model.Core;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface ISurgeryRepository
    {
        OperationResult<GenericOperationResult, SurgeryDto> Action(SurgeryDto formDto);
        OperationResult<GenericOperationResult, PatientSurgeryDto> ActionPatientSurgery(PatientSurgeryDto formDto);
        OperationResult<GenericOperationResult, IEnumerable<SurgeryDto>> GetAllSurgeries(
           int? userId, bool enablePagination, int pageSize, int pageNumber);
        Task<OperationResult<GenericOperationResult, IEnumerable<PatientSurgeryDto>>> GetAllPatientSurgeries(
           int? userId, bool enablePagination, int pageSize, int pageNumber);
        OperationResult<GenericOperationResult, IEnumerable<SurgerySelectDto>> GetSurgeriesAutoComplete(string query);

        Task<OperationResult<GenericOperationResult, SurgeryInfoDto>> GetSurgery(int Id);
        OperationResult<GenericOperationResult, PatientSurgeryDto> GetPatientSurgery(int id);
        OperationResult<GenericOperationResult> RemoveSurgery(int Id);
        Task<OperationResult<GenericOperationResult>> RemovePatientSurgery(int patientSurgeryId);
    }
}

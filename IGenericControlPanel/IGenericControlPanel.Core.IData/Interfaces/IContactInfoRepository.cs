﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.ContactInfo;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface IContactInfoRepository
    {
        OperationResult<GenericOperationResult, IEnumerable<ContactInfoDto>> GetContactInfo();
        OperationResult<GenericOperationResult, ContactInfoDto> ActionContactInfo(ContactInfoDto contactInfoDto);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Medicine;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface IMedicineRepository
    {
        OperationResult<GenericOperationResult, MedicineFormDto> Action(MedicineFormDto fromDto);
        OperationResult<GenericOperationResult> AddPatientMedicine(int medicineId, int userId);
        OperationResult<GenericOperationResult, IEnumerable<MedicineFormDto>> GetAllMedicines(
           int? userId, bool enablePagination, int pageSize, int pageNumber);
        Task<OperationResult<GenericOperationResult, IEnumerable<PatientMedicineDto>>> GetAllPatientMedicines(
           int? userId, bool enablePagination, int pageSize, int pageNumber);
        OperationResult<GenericOperationResult, IEnumerable<MedicinSelectDto>> GetMedicineAutoComplete(string query);
        OperationResult<GenericOperationResult, MedicineInfoDto> GetMedicine(int Id);
        OperationResult<GenericOperationResult> RemoveMedicine(int id);
        Task<OperationResult<GenericOperationResult>> RemovePatientMedicine(int patientMedicineId);
    }
}

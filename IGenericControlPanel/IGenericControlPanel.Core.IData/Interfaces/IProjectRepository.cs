﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.Infrastructure;
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Project;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface IProjectRepository
    {
        Task<OperationResult<GenericOperationResult, IPagedList<ProjectDto>>> GetAllProjectsWithPagination(
            int? pageNumber = 0, int? pageSize = 6);
        OperationResult<GenericOperationResult, ProjectDetailsDto> GetProjectDetails(int id);
        Task<OperationResult<GenericOperationResult>> RemoveProject(int id);
        Task<OperationResult<GenericOperationResult, ProjectFormDto>> AddProject(ProjectFormDto projectFormDto);
        OperationResult<GenericOperationResult, ProjectFormDto> UpdateProject(ProjectFormDto projectFormDto);
    }
}

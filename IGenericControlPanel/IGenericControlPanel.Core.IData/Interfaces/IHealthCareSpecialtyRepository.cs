﻿
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.HealthCareSpecialty;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface IHealthCareSpecialtyRepository
    {
        public OperationResult<GenericOperationResult, HealthCareSpecialtyDto> ActionHealthCareSpecialty(HealthCareSpecialtyDto formDto);

        public OperationResult<GenericOperationResult, IEnumerable<HealthCareSpecialtyDetailsDto>> GetAllSpecialty(string query, int pageNumber, int pageSize, bool enablePagination);
        public OperationResult<GenericOperationResult, HealthCareSpecialtyDetailsDto> GetSpecialty(int Id);
        public OperationResult<GenericOperationResult> RemoveSpecialty(int Id);
        public Task<OperationResult<GenericOperationResult, IEnumerable<HealthCareSpecialtyServiceDto>>> GetSpecialtyServicesByHealthCareSpecialtyId(int HealthCareSpecialtyId);
        public Task<OperationResult<GenericOperationResult, IEnumerable<HealthCareSpecialtyWithProvidersDto>>> GetSpecialtyByServiceId(int ServiceId);
    }
}

﻿using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.Dto.Product;

namespace IGenericControlPanel.Core.IData
{
    public interface IProductRepository
    {
        Task<OperationResult<GenericOperationResult, IEnumerable<ProductDetailsDto>>> GetAllDetailedPagedAsync(
            bool? TopRated,
            int? MinPrice, int? MaxPrice,
            int? CategoryId, int? BrandId,
            IEnumerable<int> SpecificationsIds, string query = "", int? pageNumber = 0,
            int? pageSize = null, string cultureCode = "ar", bool? showInStore = null, int? OfferIdToOrderBy = null);
        Task<OperationResult<GenericOperationResult, ProductFormDto>> AddProductAsync(ProductFormDto formDto);
        Task<OperationResult<GenericOperationResult, IEnumerable<ProductSizeColorDto>>> GetProductSizesColorsAsync(int productId);
        Task<OperationResult<GenericOperationResult, ProductUpdateFormDto>> UpdateProductAsync(ProductUpdateFormDto formDto);
        Task<OperationResult<GenericOperationResult>> ShowInStoreAsync(int productId, bool showInStore);
        Task<OperationResult<GenericOperationResult, ProductReviewDto>> ActionProductReviewAsync(ProductReviewDto formDto);
        Task<OperationResult<GenericOperationResult>> RemoveProductReviewAsync(int Id);
        Task<OperationResult<GenericOperationResult, ProductSizeColorDto>> AddProductSizeColorAsync(ProductSizeColorDto formDto);
        Task<OperationResult<GenericOperationResult, ProductSizeColorDto>> UpdateProductSizeColorAsync(ProductSizeColorDto formDto);
        Task<OperationResult<GenericOperationResult>> RemoveProductSizeColorAsync(int Id);
        Task<OperationResult<GenericOperationResult, string>> AddingProductsAfterReadingExcelFileAsync(IEnumerable<ProductExcelDto> products);
        Task<OperationResult<GenericOperationResult, IEnumerable<ProductExportDataDto>>> GetProductsByIdToExportToExcelFileAsync(IEnumerable<int> productIds);
        Task<OperationResult<GenericOperationResult, ProductDetailsDto>> GetDetailedAsync(int id, string cultureCode = "ar");
        Task<OperationResult<GenericOperationResult>> RemoveAsync(List<int> ids);
    }
}

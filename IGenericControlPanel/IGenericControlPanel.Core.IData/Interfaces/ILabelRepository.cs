﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto;

namespace IGenericControlPanel.Core.IData
{
    public interface ILabelRepository<TItem, TKey>
    {
        IList<string> GetLabelsKeys { get; set; }
        OperationResult<GenericOperationResult, LabelDto> UpdateOneLabel(LabelDto formDto);

        OperationResult<GenericOperationResult, LabelDto> SetLabel(LabelDto formDto);
        IEnumerable<LabelDto> GetAllLabels();
        TItem Get(TKey key);
        TCustomReturn Get<TCustomReturn>(TKey key) where TCustomReturn : struct, IConvertible;
        KeyValuePair<TItem, TKey> GetPair(TKey key);

        IEnumerable<TItem> GetAll();
        IEnumerable<FileDetailsDto> GetFiles();
        IDictionary<TKey, TItem> GetAllAsDictionary();

        IEnumerable<TItem> GetSet(IEnumerable<TKey> keys);
        IDictionary<TKey, TItem> GetSetAsDictionary(IEnumerable<TKey> keys);
        IDictionary<TKey, TItem> GetTranslationsSetAsDictionary(IEnumerable<TKey> keys);
    }
}

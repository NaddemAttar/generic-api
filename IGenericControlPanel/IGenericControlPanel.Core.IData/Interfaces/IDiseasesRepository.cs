﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Core.Dto.Diseases;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface IDiseasesRepository
    {
        Task<OperationResult<GenericOperationResult, PatientDiseasesDto>> ActionPatientDiseases(PatientDiseasesDto patientDiseasesDto);
        Task<OperationResult<GenericOperationResult, DiseasesFormDto>> Action(DiseasesFormDto diseasesFormDto);
        Task<OperationResult<GenericOperationResult, IEnumerable<PatientDiseasesDto>>> GetAllPatientDisease(
           int? userId, bool enablePagination, int pageSize, int pageNumber);
        Task<OperationResult<GenericOperationResult, PatientDiseasesDto>> GetPatientDisease(int patientDiseaseId);
        Task<OperationResult<GenericOperationResult, IEnumerable<DiseasesFormDto>>> GetAllDiseases(int? userId, int pageSize, int pageNumber);
        OperationResult<GenericOperationResult, IEnumerable<DiseaseSelectDto>> GetDiseasesAutoComplete(string query);
        Task<OperationResult<GenericOperationResult, DiseasesInfoDto>> GetDisease(int Id);
        Task<OperationResult<GenericOperationResult, IEnumerable<string>>> RemoveDiseases(int Id);
        Task<OperationResult<GenericOperationResult, IEnumerable<string>>> RemovePatientDiseases(int patientDiseaseId);
    }
}
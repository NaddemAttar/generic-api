﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.CourseType;
using IGenericControlPanel.SharedKernal.Interfaces.RepositoryNew;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface ICourseTypeRepository
    {
        Task<OperationResult<GenericOperationResult, CourseTypeDto>> ActionAsync(
            CourseTypeDto courseTypeDto);
        Task<OperationResult<GenericOperationResult, IEnumerable<CourseTypeDto>>> GetAllAsync(
            string cultureCode = "ar");
        Task<OperationResult<GenericOperationResult>> RemoveAsync(int id);
    }
}

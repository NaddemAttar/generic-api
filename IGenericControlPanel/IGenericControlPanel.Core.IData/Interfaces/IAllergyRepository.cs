﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Core.Dto.Allegry;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface IAllergyRepository
    {
        Task<OperationResult<GenericOperationResult, AllergyFormDto>> Action(AllergyFormDto allergyFormDto);
        Task<OperationResult<GenericOperationResult, PatientAllergyDto>> ActionPatientAllergy(PatientAllergyFormDto patientAllergyFormDto);

        Task<OperationResult<GenericOperationResult, IEnumerable<AllergyFormDto>>> GetAllAllergy(
           int? userId, bool enablePagination, int pageSize, int pageNumber);
        Task<OperationResult<GenericOperationResult, AllergyInfoDto>> GetAllergy(int Id);
        Task<OperationResult<GenericOperationResult, IEnumerable<AllergySelectDto>>> GetAllergiesAutoComplete(string query);
        Task<OperationResult<GenericOperationResult, PatientAllergyDto>> GetPatientAllergyDetails(int PatientAllergyId);
        Task<OperationResult<GenericOperationResult, IEnumerable<PatientAllergyDto>>> GetPatientAllergies(
           int? userId, bool enablePagination, int pageSize, int pageNumber);
        Task<OperationResult<GenericOperationResult>> RemoveAllergy(int Id);
        Task<OperationResult<GenericOperationResult>> RemovePatientAllergy(int PatientAllergyId);
    }
}
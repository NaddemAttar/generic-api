﻿using System.Collections.Generic;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Course;
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Core.IData.Interfaces
{
    public interface ICourseRepository
    {
        Task<OperationResult<GenericOperationResult>> HideCourseByIdAsync(int courseId, bool isHidden);
        Task<OperationResult<GenericOperationResult>> CourseEnrollmentAsync(int CourseId);
        Task<OperationResult<GenericOperationResult, IEnumerable<CourseDto>>> GetCoursesWithPaginationAsync(
            int locationId, IEnumerable<int> courseTypeIds, bool MyCourses, IEnumerable<int> instructorIds,
            string courseName, int? pageNumber, int? pageSize, int userId = 0, bool? hiddenCourse = null);
        Task<OperationResult<GenericOperationResult, IEnumerable<CourseDto>>> GetCoursesSelectAsync();
        Task<OperationResult<GenericOperationResult, CourseDetailsDto>> GetDetailedAsync(
            int id, string cultureCode = "ar");
        Task<OperationResult<GenericOperationResult>> RemoveAsync(IEnumerable<int> ids);
        Task<OperationResult<GenericOperationResult, CourseFormDto>> ActionAsync(
            CourseFormDto courseFormDto);
        Task<OperationResult<GenericOperationResult>> ActionCoursePaymentAsync(
            int userId, int courseId, PaymentStatus paymentStatus);
    }
}

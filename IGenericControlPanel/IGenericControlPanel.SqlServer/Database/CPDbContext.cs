﻿using CraftLab.Core.Database.EFCore;
using CraftLab.Core.Database.Identity.Interfaces;
using CraftLab.Core.Model.Interface;
using IGenericControlPanel.Model;
using IGenericControlPanel.Model.Communication;
using IGenericControlPanel.Model.Configuration;
using IGenericControlPanel.Model.Consultations;
using IGenericControlPanel.Model.Content;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.Model.Gamification;
using IGenericControlPanel.Model.HR;
using IGenericControlPanel.Model.Projects;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.Model.Security.Permissions;
using IGenericControlPanel.Model.UserExperiences;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Currency = IGenericControlPanel.Model.Core.Currency;

namespace IGenericControlPanel.MySql.Database
{
    /// <summary>
    /// Represents the IGeneric Control Panel Database Context.
    /// </summary>
    public class CPDbContext : CraftLabIdentityDbContext<CPUser, CPRole, int, CPUserClaim, CPUserRole, CPUserLogin, CPRoleClaim, CPUserToken>
    {
        public CPDbContext(DbContextOptions<CPDbContext> options, IUserService userService) : base(options, userService)
        { }
        public CPDbContext(DbContextOptions<CPDbContext> options) : base(options)
        { }
        #region Core Schema

        #region Location
        public DbSet<Location> Location { get; set; }
        public DbSet<LocationSchedule> LocationSchedule { get; set; }
        public DbSet<CitySet> Cities { get; set; }
        public DbSet<CityTranslationSet> CityTranslations { get; set; }
        public DbSet<CountrySet> Countries { get; set; }
        public DbSet<CountryTranslationSet> CountryTranslations { get; set; }
        #endregion

        public DbSet<Product> Products { get; set; }
        public DbSet<ProductReview> ProductReviews { get; set; }
        public DbSet<ProductSizeColor> ProductSizesColors { get; set; }
        public DbSet<Specification> Specifications { get; set; }
        public DbSet<ProductSpecification> ProductSpecifications { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Information> Informations { get; set; }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<PostType> PostTypes { get; set; }
        public DbSet<Question> Question { get; set; }
        public DbSet<Answer> Answer { get; set; }
        public DbSet<MultiLevelCategory> MultiLevelCategories { get; set; }
        public DbSet<ContactInfo> ContactInfo { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupMember> GroupMembers { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<UserCourse> UserCourses { get; set; }
        public DbSet<CourseType> CourseTypes { get; set; }
        public DbSet<CourseTypeCourses> CourseTypeCourses { get; set; }
        public DbSet<Instructor> Instructors { get; set; }
        public DbSet<Brand> Brand { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderProducts> OrderProducts { get; set; }
        public DbSet<OpenningHouresSet> OpenningHoures { get; set; }
        public DbSet<DoctorLocation> DoctorLocations { get; set; }
        public DbSet<JobOffer> JobOffers { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<JobApplication> JobApplications { get; set; }
        public DbSet<Allergy> Allegries { get; set; }
        public DbSet<PatientAllergy> PatientAllergies { get; set; }
        public DbSet<Medicine> Medicines { get; set; }
        public DbSet<PatientMedicine> PatientMedicines { get; set; }
        public DbSet<PatientSurgery> PatientSurgeries { get; set; }
        public DbSet<Surgery> Surgeries { get; set; }
        public DbSet<Diseases> Diseases { get; set; }
        public DbSet<PatientsDiseases> PatientsDiseases { get; set; }
        public DbSet<HealthCareSpecialty> HealthCareSpecialties { get; set; }
        public DbSet<Subscriber> Subscribers { get; set; }
        public DbSet<AllergyCausingMedicin> AllergyCausingMedicins { get; set; }

        public DbSet<Note> Notes { get; set; }
        public DbSet<UserNotes> UserNotes { get; set; }

        public DbSet<MedicalTests> MedicalTests { get; set; }
        public DbSet<MultiLevelCategoryRelatedEntity> MultiLevelCategoryEntities { get; set; }
        public DbSet<HealthCareProviderSpecialty> HealthCareProviderSpecialties { get; set; }
        public DbSet<HealthCareSpecialtyService> HealthCareSpecialtyServices { get; set; }
        public DbSet<HealthCareProviderService> HealthCareProviderServices { get; set; }
        public DbSet<BlogComment> BlogComments { get; set; }
        public DbSet<HealthCareProviderEnterprise> HealthCareProviderEnterprise { get; set; }
        public DbSet<HealthCareProdviderEnterpriseServices> HealthCareProdviderEnterpriseServices { get; set; }

        #endregion

        #region Projects
        public DbSet<Project> Projects { get; set; }
        #endregion

        #region HR
        public DbSet<TeamMember> TeamMembers { get; set; }

        public DbSet<PartnerSet> Partners { get; set; }
        #endregion

        #region Gamification

        public DbSet<Gamification> Gamifications { get; set; }

        public DbSet<UsersPoints> UsersPoints { get; set; }

        public DbSet<Coupon> Coupons { get; set; }

        public DbSet<UserCoupon> UserCoupons { get; set; }


        #endregion

        #region Content

        public DbSet<FileBase> Files { get; set; }
        public DbSet<Carousel> Carousels { get; set; }
        public DbSet<CarouselItem> CarouselItems { get; set; }
        public DbSet<CarouselItemTranslation> CarouselItemTranslations { get; set; }
        public DbSet<Label> Labels { get; set; }
        public DbSet<PrivacyMaterials> PrivacyMaterials { get; set; }
        public DbSet<TipsSet> Tips { get; set; }
        public DbSet<UserLanguage> UserLanguages { get; set; }
        #endregion

        #region Configuration

        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Setting> Settings { get; set; }

        #endregion

        #region Communication
        public DbSet<ContactMessage> ContactMessages { get; set; }
        public DbSet<BookingSet> Bookings { get; set; }
        public DbSet<SessionType> SessionTypes { get; set; }
        public DbSet<SessionServices> SessionServices { get; set; }
        public DbSet<FeedBackSet> FeedBack { get; set; }
        public DbSet<UserServiceSession> UserServiceSession { get; set; }
        #endregion

        #region Permission

        public DbSet<WebContentSet> WebContents { get; set; }
        public DbSet<WebContentRoleSet> WebContentRoles { get; set; }
        #endregion

        #region Consultation
        public DbSet<ConsultationEntity> Consultations { get; set; }
        public DbSet<ConsultationAnswers> ConsultationAnswers { get; set; }
        public DbSet<ConsultationChoices> ConsultationChoices { get; set; }
        public DbSet<ConsultationQuestionChoices> ConsultationQuestionChoices { get; set; }
        public DbSet<HealthProviderQuestions> HealthProviderQuestions { get; set; }
        #endregion

        #region User_Experiences
        public DbSet<Education> Educations { get; set; }
        public DbSet<Experience> Experiences { get; set; }
        public DbSet<Certificate> Certificates { get; set; }
        public DbSet<License> Licenses { get; set; }
        public DbSet<Lecture> Lectures { get; set; }
        #endregion
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            modelBuilder.Entity<Location>()
                .HasMany(entity => entity.Courses)
                .WithOne(entity => entity.Location)
                .HasForeignKey(entity => entity.LocationId)
                .HasPrincipalKey(entity => entity.Id);

            modelBuilder.Entity<MultiLevelCategory>()
                        .HasOne(x => x.Parent)
                        .WithMany(x => x.Children)
                        .HasForeignKey(x => x.ParentCategoryId);
            modelBuilder.Entity<HealthCareSpecialty>()
                       .HasOne(x => x.Parent)
                       .WithMany(x => x.Children)
                       .HasForeignKey(x => x.ParentSpecialtyId);


            modelBuilder.Entity<CPUser>(u =>
            {
                u.HasMany(e => e.UserRoles)
                .WithOne(e => e.User)
                .HasForeignKey(ur => ur.UserId)
                .IsRequired();
            });

            modelBuilder.Entity<CPRole>(u =>
            {
                u.HasMany(e => e.UserRoles)
                .WithOne(e => e.Role)
                .HasForeignKey(ur => ur.RoleId)
                .IsRequired();
            });
            modelBuilder.Entity<Setting>()
            .HasOne(a => a.Currency)
            .WithOne(a => a.Setting)
            .HasForeignKey<Setting>(c => c.CurrencyId);

            modelBuilder.Entity<CPUser>()
                .HasMany(u => u.UsersEnterprise)
                .WithOne(hcpe => hcpe.User)
                .HasForeignKey(hcpe => hcpe.UserId);

            modelBuilder.Entity<CPUser>()
                .HasMany(u => u.Enterprises)
                .WithOne(hcpe => hcpe.Enterprise)
                .HasForeignKey(hcpe => hcpe.EnterpriseId);

            modelBuilder.Entity<CPUser>()
                .HasMany(u => u.UserBookings)
                .WithOne(b => b.User)
                .HasForeignKey(b => b.UserId);

            modelBuilder.Entity<CPUser>()
                .HasMany(u => u.HcpBookings)
                .WithOne(b => b.CpUser)
                .HasForeignKey(b => b.CPUserId);

            modelBuilder.Entity<CPUser>()
                .HasMany(u => u.UserServices)
                .WithOne(hcps => hcps.CPUser)
                .HasForeignKey(hcps => hcps.CPUserId);

            modelBuilder.Entity<CPUser>()
                .HasMany(u => u.EnterpriseServices)
                .WithOne(hcps => hcps.Enterprise)
                .HasForeignKey(hcps => hcps.EnterpriseId);

            //modelBuilder.Entity<ContactInfo>()
            //    .ToTable("ContactInfo", entity => entity.ExcludeFromMigrations());

            //modelBuilder.Entity<Label>()
            //   .ToTable("Labels", entity => entity.ExcludeFromMigrations());

            //modelBuilder.Entity<LabelTranslation>()
            //   .ToTable("LabelTranslations", entity => entity.ExcludeFromMigrations());

            //modelBuilder.Entity<Setting>()
            //   .ToTable("Settings", entity => entity.ExcludeFromMigrations());

        }

        public override int SaveChanges()
        {
            BeforeSaving();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            BeforeSaving();
            return base.SaveChangesAsync(cancellationToken);
        }
        protected new void BeforeSaving()
        {
            int key = default;
            if (UserService != null)
                key = UserService.GetCurrentUserId<int>();
            IEnumerable<EntityEntry> entityEntries = ChangeTracker.Entries();
            DateTimeOffset utcNow = DateTimeOffset.UtcNow;
            foreach (EntityEntry entityEntry in entityEntries)
            {
                if (entityEntry.Entity is IBaseEntity<int> entity)
                {
                    switch (entityEntry.State)
                    {
                        case EntityState.Deleted:
                            if (!entity.ForceDelete)
                            {
                                entity.IsValid = false;
                                entity.LastUpdateDate = utcNow;
                                entity.LastUpdatedBy = key;
                                entityEntry.State = EntityState.Modified;
                                break;
                            }
                            break;
                        case EntityState.Modified:
                            entity.LastUpdateDate = utcNow;
                            entity.LastUpdatedBy = key;
                            break;
                        case EntityState.Added:
                            entity.IsValid = true;
                            entity.CreationDate = utcNow;
                            entity.CreatedBy = key;
                            entity.LastUpdateDate = utcNow;
                            entity.LastUpdatedBy = key;
                            break;
                    }
                }
            }
        }

    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class Group_questions_blogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GroupId",
                table: "Question",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "GroupId",
                table: "Blogs",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Question_GroupId",
                table: "Question",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Blogs_GroupId",
                table: "Blogs",
                column: "GroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_Blogs_Groups_GroupId",
                table: "Blogs",
                column: "GroupId",
                principalTable: "Groups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Question_Groups_GroupId",
                table: "Question",
                column: "GroupId",
                principalTable: "Groups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Blogs_Groups_GroupId",
                table: "Blogs");

            migrationBuilder.DropForeignKey(
                name: "FK_Question_Groups_GroupId",
                table: "Question");

            migrationBuilder.DropIndex(
                name: "IX_Question_GroupId",
                table: "Question");

            migrationBuilder.DropIndex(
                name: "IX_Blogs_GroupId",
                table: "Blogs");

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "Question");

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "Blogs");
        }
    }
}

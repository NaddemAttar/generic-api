﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class add_hcpType_hcpEnterprise_opeiningHours_hcpEnterprise : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "HealthCareProviderEnterpriseId",
                table: "OpenningHoures",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HcpType",
                table: "AspNetUsers",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "HealthCareProviderEnterprise",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    EnterpriseId = table.Column<int>(type: "int", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HealthCareProviderEnterprise", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HealthCareProviderEnterprise_AspNetUsers_EnterpriseId",
                        column: x => x.EnterpriseId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HealthCareProviderEnterprise_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_OpenningHoures_HealthCareProviderEnterpriseId",
                table: "OpenningHoures",
                column: "HealthCareProviderEnterpriseId");

            migrationBuilder.CreateIndex(
                name: "IX_HealthCareProviderEnterprise_EnterpriseId",
                table: "HealthCareProviderEnterprise",
                column: "EnterpriseId");

            migrationBuilder.CreateIndex(
                name: "IX_HealthCareProviderEnterprise_UserId",
                table: "HealthCareProviderEnterprise",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_OpenningHoures_HealthCareProviderEnterprise_HealthCareProvid~",
                table: "OpenningHoures",
                column: "HealthCareProviderEnterpriseId",
                principalTable: "HealthCareProviderEnterprise",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OpenningHoures_HealthCareProviderEnterprise_HealthCareProvid~",
                table: "OpenningHoures");

            migrationBuilder.DropTable(
                name: "HealthCareProviderEnterprise");

            migrationBuilder.DropIndex(
                name: "IX_OpenningHoures_HealthCareProviderEnterpriseId",
                table: "OpenningHoures");

            migrationBuilder.DropColumn(
                name: "HealthCareProviderEnterpriseId",
                table: "OpenningHoures");

            migrationBuilder.DropColumn(
                name: "HcpType",
                table: "AspNetUsers");
        }
    }
}

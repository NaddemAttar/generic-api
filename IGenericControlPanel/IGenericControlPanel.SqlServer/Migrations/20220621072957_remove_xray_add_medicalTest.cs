﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class remove_xray_add_medicalTest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Files_X_Ray_Pictures_X_Ray_PicturesId",
                table: "Files");

            migrationBuilder.DropTable(
                name: "X_Ray_Pictures");

            migrationBuilder.DropIndex(
                name: "IX_Files_X_Ray_PicturesId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "X_Ray_PicturesId",
                table: "Files");

            migrationBuilder.AddColumn<int>(
                name: "MedicalTestId",
                table: "Files",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MedicalTests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsValid = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(nullable: false),
                    LastUpdatedBy = table.Column<int>(nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    PersonId = table.Column<int>(nullable: false),
                    MedicalTestType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MedicalTests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MedicalTests_People_PersonId",
                        column: x => x.PersonId,
                        principalTable: "People",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Files_MedicalTestId",
                table: "Files",
                column: "MedicalTestId");

            migrationBuilder.CreateIndex(
                name: "IX_MedicalTests_PersonId",
                table: "MedicalTests",
                column: "PersonId");

            migrationBuilder.AddForeignKey(
                name: "FK_Files_MedicalTests_MedicalTestId",
                table: "Files",
                column: "MedicalTestId",
                principalTable: "MedicalTests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Files_MedicalTests_MedicalTestId",
                table: "Files");

            migrationBuilder.DropTable(
                name: "MedicalTests");

            migrationBuilder.DropIndex(
                name: "IX_Files_MedicalTestId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "MedicalTestId",
                table: "Files");

            migrationBuilder.AddColumn<int>(
                name: "X_Ray_PicturesId",
                table: "Files",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "X_Ray_Pictures",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    Description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    PersonId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_X_Ray_Pictures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_X_Ray_Pictures_People_PersonId",
                        column: x => x.PersonId,
                        principalTable: "People",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Files_X_Ray_PicturesId",
                table: "Files",
                column: "X_Ray_PicturesId");

            migrationBuilder.CreateIndex(
                name: "IX_X_Ray_Pictures_PersonId",
                table: "X_Ray_Pictures",
                column: "PersonId");

            migrationBuilder.AddForeignKey(
                name: "FK_Files_X_Ray_Pictures_X_Ray_PicturesId",
                table: "Files",
                column: "X_Ray_PicturesId",
                principalTable: "X_Ray_Pictures",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

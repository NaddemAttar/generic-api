﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class add_price_to_sessionType_insteadOf_service : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Price",
                table: "SessionTypes",
                type: "double",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Price",
                table: "SessionTypes");
        }
    }
}

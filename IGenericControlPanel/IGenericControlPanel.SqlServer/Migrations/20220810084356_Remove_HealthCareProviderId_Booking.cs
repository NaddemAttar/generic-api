﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class Remove_HealthCareProviderId_Booking : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bookings_AspNetUsers_HealthCareProviderId",
                table: "Bookings");

            migrationBuilder.RenameColumn(
                name: "HealthCareProviderId",
                table: "Bookings",
                newName: "CPUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Bookings_HealthCareProviderId",
                table: "Bookings",
                newName: "IX_Bookings_CPUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bookings_AspNetUsers_CPUserId",
                table: "Bookings",
                column: "CPUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bookings_AspNetUsers_CPUserId",
                table: "Bookings");

            migrationBuilder.RenameColumn(
                name: "CPUserId",
                table: "Bookings",
                newName: "HealthCareProviderId");

            migrationBuilder.RenameIndex(
                name: "IX_Bookings_CPUserId",
                table: "Bookings",
                newName: "IX_Bookings_HealthCareProviderId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bookings_AspNetUsers_HealthCareProviderId",
                table: "Bookings",
                column: "HealthCareProviderId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

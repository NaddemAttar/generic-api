﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class add_speciality : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "HealthCareSpecialtyId",
                table: "Files",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "HealthCareSpecialties",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsValid = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(nullable: false),
                    LastUpdatedBy = table.Column<int>(nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    ParentSpecialtyId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HealthCareSpecialties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HealthCareSpecialties_HealthCareSpecialties_ParentSpecialtyId",
                        column: x => x.ParentSpecialtyId,
                        principalTable: "HealthCareSpecialties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Files_HealthCareSpecialtyId",
                table: "Files",
                column: "HealthCareSpecialtyId");

            migrationBuilder.CreateIndex(
                name: "IX_HealthCareSpecialties_ParentSpecialtyId",
                table: "HealthCareSpecialties",
                column: "ParentSpecialtyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Files_HealthCareSpecialties_HealthCareSpecialtyId",
                table: "Files",
                column: "HealthCareSpecialtyId",
                principalTable: "HealthCareSpecialties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Files_HealthCareSpecialties_HealthCareSpecialtyId",
                table: "Files");

            migrationBuilder.DropTable(
                name: "HealthCareSpecialties");

            migrationBuilder.DropIndex(
                name: "IX_Files_HealthCareSpecialtyId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "HealthCareSpecialtyId",
                table: "Files");
        }
    }
}

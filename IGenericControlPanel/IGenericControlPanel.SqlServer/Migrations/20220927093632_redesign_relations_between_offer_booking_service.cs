﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class redesign_relations_between_offer_booking_service : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bookings_AspNetUsers_CPUserId",
                table: "Bookings");

            migrationBuilder.DropForeignKey(
                name: "FK_Bookings_HealthCareProviderEnterprise_HealthCareProviderEnte~",
                table: "Bookings");

            migrationBuilder.DropForeignKey(
                name: "FK_Bookings_SessionTypes_SessionTypeId",
                table: "Bookings");

            migrationBuilder.DropForeignKey(
                name: "FK_HealthCareProviderServices_AspNetUsers_CPUserId",
                table: "HealthCareProviderServices");

            migrationBuilder.DropForeignKey(
                name: "FK_Services_Offers_OfferId",
                table: "Services");

            migrationBuilder.DropForeignKey(
                name: "FK_SessionTypes_AspNetUsers_UserId",
                table: "SessionTypes");

            migrationBuilder.DropForeignKey(
                name: "FK_SessionTypes_Services_ServiceId",
                table: "SessionTypes");

            migrationBuilder.DropIndex(
                name: "IX_SessionTypes_ServiceId",
                table: "SessionTypes");

            migrationBuilder.DropIndex(
                name: "IX_Services_OfferId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Bookings_HealthCareProviderEnterpriseId",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "Duration",
                table: "SessionTypes");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "SessionTypes");

            migrationBuilder.DropColumn(
                name: "ServiceId",
                table: "SessionTypes");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "HealthCareProviderEnterpriseId",
                table: "Bookings");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "SessionTypes",
                newName: "CPUserId");

            migrationBuilder.RenameIndex(
                name: "IX_SessionTypes_UserId",
                table: "SessionTypes",
                newName: "IX_SessionTypes_CPUserId");

            migrationBuilder.RenameColumn(
                name: "SessionTypeId",
                table: "Bookings",
                newName: "UserServiceSessionId");

            migrationBuilder.RenameIndex(
                name: "IX_Bookings_SessionTypeId",
                table: "Bookings",
                newName: "IX_Bookings_UserServiceSessionId");

            migrationBuilder.AlterColumn<int>(
                name: "CPUserId",
                table: "HealthCareProviderServices",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "EnterpriseId",
                table: "HealthCareProviderServices",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HealthCareProviderEnterpriseId",
                table: "HealthCareProviderServices",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OfferId",
                table: "HealthCareProviderServices",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Bookings",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "UserServiceSession",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    HealthCareProviderServiceId = table.Column<int>(type: "int", nullable: false),
                    SessionTypeId = table.Column<int>(type: "int", nullable: false),
                    Duration = table.Column<int>(type: "int", nullable: false),
                    Price = table.Column<double>(type: "double", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserServiceSession", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserServiceSession_HealthCareProviderServices_HealthCareProv~",
                        column: x => x.HealthCareProviderServiceId,
                        principalTable: "HealthCareProviderServices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserServiceSession_SessionTypes_SessionTypeId",
                        column: x => x.SessionTypeId,
                        principalTable: "SessionTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_HealthCareProviderServices_EnterpriseId",
                table: "HealthCareProviderServices",
                column: "EnterpriseId");

            migrationBuilder.CreateIndex(
                name: "IX_HealthCareProviderServices_HealthCareProviderEnterpriseId",
                table: "HealthCareProviderServices",
                column: "HealthCareProviderEnterpriseId");

            migrationBuilder.CreateIndex(
                name: "IX_HealthCareProviderServices_OfferId",
                table: "HealthCareProviderServices",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_Bookings_UserId",
                table: "Bookings",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserServiceSession_HealthCareProviderServiceId",
                table: "UserServiceSession",
                column: "HealthCareProviderServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_UserServiceSession_SessionTypeId",
                table: "UserServiceSession",
                column: "SessionTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bookings_AspNetUsers_CPUserId",
                table: "Bookings",
                column: "CPUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Bookings_AspNetUsers_UserId",
                table: "Bookings",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Bookings_UserServiceSession_UserServiceSessionId",
                table: "Bookings",
                column: "UserServiceSessionId",
                principalTable: "UserServiceSession",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_HealthCareProviderServices_AspNetUsers_CPUserId",
                table: "HealthCareProviderServices",
                column: "CPUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_HealthCareProviderServices_AspNetUsers_EnterpriseId",
                table: "HealthCareProviderServices",
                column: "EnterpriseId",
                principalTable: "AspNetUsers",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_HealthCareProviderServices_HealthCareProviderEnterprise_Heal~",
                table: "HealthCareProviderServices",
                column: "HealthCareProviderEnterpriseId",
                principalTable: "HealthCareProviderEnterprise",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_HealthCareProviderServices_Offers_OfferId",
                table: "HealthCareProviderServices",
                column: "OfferId",
                principalTable: "Offers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SessionTypes_AspNetUsers_CPUserId",
                table: "SessionTypes",
                column: "CPUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bookings_AspNetUsers_CPUserId",
                table: "Bookings");

            migrationBuilder.DropForeignKey(
                name: "FK_Bookings_AspNetUsers_UserId",
                table: "Bookings");

            migrationBuilder.DropForeignKey(
                name: "FK_Bookings_UserServiceSession_UserServiceSessionId",
                table: "Bookings");

            migrationBuilder.DropForeignKey(
                name: "FK_HealthCareProviderServices_AspNetUsers_CPUserId",
                table: "HealthCareProviderServices");

            migrationBuilder.DropForeignKey(
                name: "FK_HealthCareProviderServices_AspNetUsers_EnterpriseId",
                table: "HealthCareProviderServices");

            migrationBuilder.DropForeignKey(
                name: "FK_HealthCareProviderServices_HealthCareProviderEnterprise_Heal~",
                table: "HealthCareProviderServices");

            migrationBuilder.DropForeignKey(
                name: "FK_HealthCareProviderServices_Offers_OfferId",
                table: "HealthCareProviderServices");

            migrationBuilder.DropForeignKey(
                name: "FK_SessionTypes_AspNetUsers_CPUserId",
                table: "SessionTypes");

            migrationBuilder.DropTable(
                name: "UserServiceSession");

            migrationBuilder.DropIndex(
                name: "IX_HealthCareProviderServices_EnterpriseId",
                table: "HealthCareProviderServices");

            migrationBuilder.DropIndex(
                name: "IX_HealthCareProviderServices_HealthCareProviderEnterpriseId",
                table: "HealthCareProviderServices");

            migrationBuilder.DropIndex(
                name: "IX_HealthCareProviderServices_OfferId",
                table: "HealthCareProviderServices");

            migrationBuilder.DropIndex(
                name: "IX_Bookings_UserId",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "EnterpriseId",
                table: "HealthCareProviderServices");

            migrationBuilder.DropColumn(
                name: "HealthCareProviderEnterpriseId",
                table: "HealthCareProviderServices");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "HealthCareProviderServices");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Bookings");

            migrationBuilder.RenameColumn(
                name: "CPUserId",
                table: "SessionTypes",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_SessionTypes_CPUserId",
                table: "SessionTypes",
                newName: "IX_SessionTypes_UserId");

            migrationBuilder.RenameColumn(
                name: "UserServiceSessionId",
                table: "Bookings",
                newName: "SessionTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Bookings_UserServiceSessionId",
                table: "Bookings",
                newName: "IX_Bookings_SessionTypeId");

            migrationBuilder.AddColumn<int>(
                name: "Duration",
                table: "SessionTypes",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "Price",
                table: "SessionTypes",
                type: "double",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "ServiceId",
                table: "SessionTypes",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OfferId",
                table: "Services",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CPUserId",
                table: "HealthCareProviderServices",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HealthCareProviderEnterpriseId",
                table: "Bookings",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SessionTypes_ServiceId",
                table: "SessionTypes",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_Services_OfferId",
                table: "Services",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_Bookings_HealthCareProviderEnterpriseId",
                table: "Bookings",
                column: "HealthCareProviderEnterpriseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bookings_AspNetUsers_CPUserId",
                table: "Bookings",
                column: "CPUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Bookings_HealthCareProviderEnterprise_HealthCareProviderEnte~",
                table: "Bookings",
                column: "HealthCareProviderEnterpriseId",
                principalTable: "HealthCareProviderEnterprise",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Bookings_SessionTypes_SessionTypeId",
                table: "Bookings",
                column: "SessionTypeId",
                principalTable: "SessionTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_HealthCareProviderServices_AspNetUsers_CPUserId",
                table: "HealthCareProviderServices",
                column: "CPUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Services_Offers_OfferId",
                table: "Services",
                column: "OfferId",
                principalTable: "Offers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SessionTypes_AspNetUsers_UserId",
                table: "SessionTypes",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SessionTypes_Services_ServiceId",
                table: "SessionTypes",
                column: "ServiceId",
                principalTable: "Services",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

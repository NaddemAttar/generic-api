﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class change_offer_logic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BrandId",
                table: "OfferEntities",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MultiLevelCategoryId",
                table: "OfferEntities",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_OfferEntities_BrandId",
                table: "OfferEntities",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferEntities_MultiLevelCategoryId",
                table: "OfferEntities",
                column: "MultiLevelCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_OfferEntities_Brand_BrandId",
                table: "OfferEntities",
                column: "BrandId",
                principalTable: "Brand",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OfferEntities_MultiLevelCategories_MultiLevelCategoryId",
                table: "OfferEntities",
                column: "MultiLevelCategoryId",
                principalTable: "MultiLevelCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OfferEntities_Brand_BrandId",
                table: "OfferEntities");

            migrationBuilder.DropForeignKey(
                name: "FK_OfferEntities_MultiLevelCategories_MultiLevelCategoryId",
                table: "OfferEntities");

            migrationBuilder.DropIndex(
                name: "IX_OfferEntities_BrandId",
                table: "OfferEntities");

            migrationBuilder.DropIndex(
                name: "IX_OfferEntities_MultiLevelCategoryId",
                table: "OfferEntities");

            migrationBuilder.DropColumn(
                name: "BrandId",
                table: "OfferEntities");

            migrationBuilder.DropColumn(
                name: "MultiLevelCategoryId",
                table: "OfferEntities");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class move_NewPrice_to_offer_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NewPrice",
                table: "OfferEntities");

            migrationBuilder.AddColumn<double>(
                name: "NewPrice",
                table: "Offers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NewPrice",
                table: "Offers");

            migrationBuilder.AddColumn<double>(
                name: "NewPrice",
                table: "OfferEntities",
                type: "double",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}

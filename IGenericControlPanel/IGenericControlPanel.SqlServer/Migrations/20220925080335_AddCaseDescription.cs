﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class AddCaseDescription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Text",
                table: "Consultations",
                newName: "HealthProviderAnswer");

            migrationBuilder.AddColumn<string>(
                name: "CaseDescription",
                table: "Consultations",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CaseDescription",
                table: "Consultations");

            migrationBuilder.RenameColumn(
                name: "HealthProviderAnswer",
                table: "Consultations",
                newName: "Text");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class post_initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Files_Apartments_ApartmentId",
                table: "Files");

            migrationBuilder.DropForeignKey(
                name: "FK_Files_Constructions_ConstructionId",
                table: "Files");

            migrationBuilder.DropForeignKey(
                name: "FK_Files_Clients_ClientId",
                table: "Files");

            migrationBuilder.DropForeignKey(
                name: "FK_Files_Floors_FloorId",
                table: "Files");

            migrationBuilder.DropForeignKey(
                name: "FK_Files_Projects_ProjectId",
                table: "Files");

            migrationBuilder.DropForeignKey(
                name: "FK_Files_People_UserJobId",
                table: "Files");

            migrationBuilder.DropForeignKey(
                name: "FK_JobApplications_People_UserId",
                table: "JobApplications");

            migrationBuilder.DropForeignKey(
                name: "FK_Order_AspNetUsers_PersonId",
                table: "Order");

            migrationBuilder.DropForeignKey(
                name: "FK_People_AspNetUsers_UserId1",
                table: "People");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_Departments_DepartmentId",
                table: "Products");

            migrationBuilder.DropTable(
                name: "ApartmentDirections");

            migrationBuilder.DropTable(
                name: "ApartmentFeatures");

            migrationBuilder.DropTable(
                name: "ApartmentTranslations");

            migrationBuilder.DropTable(
                name: "ConstructionTranslations");

            migrationBuilder.DropTable(
                name: "DepartmentCategories");

            migrationBuilder.DropTable(
                name: "DepartmentTranslations");

            migrationBuilder.DropTable(
                name: "DirectionTranslations");

            migrationBuilder.DropTable(
                name: "FeatureTranslations");

            migrationBuilder.DropTable(
                name: "FloorTranslations");

            migrationBuilder.DropTable(
                name: "ProjectTranslations");

            migrationBuilder.DropTable(
                name: "ServiceOffers");

            migrationBuilder.DropTable(
                name: "Apartments");

            migrationBuilder.DropTable(
                name: "Directions");

            migrationBuilder.DropTable(
                name: "Features");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "Floors");

            migrationBuilder.DropTable(
                name: "Departments");

            migrationBuilder.DropTable(
                name: "Constructions");

            migrationBuilder.DropIndex(
                name: "IX_Products_DepartmentId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_People_UserId1",
                table: "People");

            migrationBuilder.DropIndex(
                name: "IX_JobApplications_UserId",
                table: "JobApplications");

            migrationBuilder.DropIndex(
                name: "IX_Files_ApartmentId",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_ConstructionId",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_ClientId",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_FloorId",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_ProjectId",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_UserJobId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "WebsitePages");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "WebContents");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "WebContentRoles");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "UserLanguageTranslations");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "UserLanguages");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Tips");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "TeamMembers");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Specification");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "SessionTypes");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "SessionServices");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "ServiceTranslations");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Question");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "ProductTranslations");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "ProductSpecifications");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "ProductSizes");

            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "ProductReviews");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "ProductColors");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "PrivacyMaterials");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "PostTypes");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "PersonCourses");

            migrationBuilder.DropColumn(
                name: "CvFileId",
                table: "People");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "People");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "People");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Partners");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "OrderProducts");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "OpenningHoures");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "MultiLevelCategories");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "LocationSchedule");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "LabelTranslations");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "JobOffers");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "JobApplications");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "JobApplications");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Instructors");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "InformationTranslation");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Informations");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "GroupTranslation");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Groups");

            migrationBuilder.DropColumn(
                name: "ApartmentId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "ConstructionId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "ClientId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "FloorId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "UserJobId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "FeedBack");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "DoctorLocations");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Currencies");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "CourseTypes");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "CourseTypeCourses");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Courses");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "CountryTranslations");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Countries");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "ContactMessages");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "ContactInfo");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "ClientTranslation");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "CityTranslations");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Cities");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "CategoryTranslations");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "CarouselTranslations");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Carousels");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "CarouselItemTranslations");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "CarouselItems");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Brand");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "BlogVoteSet");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "BlogTranslation");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Blogs");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "AspNetRoles");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Answer");

            migrationBuilder.AddColumn<int>(
                name: "PaymentStatus",
                table: "PersonCourses",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "ContractType",
                table: "JobOffers",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4");

            migrationBuilder.AddColumn<int>(
                name: "CityId",
                table: "JobOffers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LocationId",
                table: "JobOffers",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "MaxSalary",
                table: "JobOffers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PlaceOfWork",
                table: "JobOffers",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Salary",
                table: "JobOffers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SalaryType",
                table: "JobOffers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CvFileId",
                table: "JobApplications",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "JobApplications",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "JobApplications",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "JobApplications",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PersonId",
                table: "JobApplications",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "JobApplications",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AllergyId",
                table: "Files",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DiseasesId",
                table: "Files",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MedicineId",
                table: "Files",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PatientAllergyId",
                table: "Files",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PatientSurgeryId",
                table: "Files",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PatientsDiseasesId",
                table: "Files",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SurgeryId",
                table: "Files",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "JobApplicationId",
                table: "Files",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "X_Ray_PicturesId",
                table: "Files",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Allegries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsValid = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(nullable: false),
                    LastUpdatedBy = table.Column<int>(nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    AllergyType = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Allegries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Diseases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsValid = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(nullable: false),
                    LastUpdatedBy = table.Column<int>(nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    DiseaseType = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Diseases", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Medicines",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsValid = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(nullable: false),
                    LastUpdatedBy = table.Column<int>(nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Medicines", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Notes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsValid = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(nullable: false),
                    LastUpdatedBy = table.Column<int>(nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(nullable: false),
                    Body = table.Column<string>(nullable: true),
                    PersonId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Notes_People_PersonId",
                        column: x => x.PersonId,
                        principalTable: "People",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Offers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsValid = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(nullable: false),
                    LastUpdatedBy = table.Column<int>(nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Descrption = table.Column<string>(nullable: true),
                    Percentage = table.Column<double>(nullable: false),
                    OfferType = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Offers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Surgeries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsValid = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(nullable: false),
                    LastUpdatedBy = table.Column<int>(nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Surgeries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "X_Ray_Pictures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsValid = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(nullable: false),
                    LastUpdatedBy = table.Column<int>(nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    PersonId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_X_Ray_Pictures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_X_Ray_Pictures_People_PersonId",
                        column: x => x.PersonId,
                        principalTable: "People",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PatientAllergies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsValid = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(nullable: false),
                    LastUpdatedBy = table.Column<int>(nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(nullable: false),
                    PersonId = table.Column<int>(nullable: false),
                    AllegryId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PatientAllergies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PatientAllergies_Allegries_AllegryId",
                        column: x => x.AllegryId,
                        principalTable: "Allegries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PatientAllergies_People_PersonId",
                        column: x => x.PersonId,
                        principalTable: "People",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PatientsDiseases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsValid = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(nullable: false),
                    LastUpdatedBy = table.Column<int>(nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(nullable: false),
                    PersonId = table.Column<int>(nullable: false),
                    DiseasesId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PatientsDiseases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PatientsDiseases_Diseases_DiseasesId",
                        column: x => x.DiseasesId,
                        principalTable: "Diseases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PatientsDiseases_People_PersonId",
                        column: x => x.PersonId,
                        principalTable: "People",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OfferEntities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsValid = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(nullable: false),
                    LastUpdatedBy = table.Column<int>(nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(nullable: false),
                    ProductId = table.Column<int>(nullable: true),
                    ServiceId = table.Column<int>(nullable: true),
                    CourseId = table.Column<int>(nullable: true),
                    OfferId = table.Column<int>(nullable: true),
                    NewPrice = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfferEntities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OfferEntities_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OfferEntities_Offers_OfferId",
                        column: x => x.OfferId,
                        principalTable: "Offers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OfferEntities_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OfferEntities_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PatientSurgeries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsValid = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(nullable: false),
                    LastUpdatedBy = table.Column<int>(nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(nullable: false),
                    PersonId = table.Column<int>(nullable: false),
                    SurgeryId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PatientSurgeries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PatientSurgeries_People_PersonId",
                        column: x => x.PersonId,
                        principalTable: "People",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PatientSurgeries_Surgeries_SurgeryId",
                        column: x => x.SurgeryId,
                        principalTable: "Surgeries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AllergyCausingMedicins",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsValid = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(nullable: false),
                    LastUpdatedBy = table.Column<int>(nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(nullable: false),
                    MedicineId = table.Column<int>(nullable: false),
                    PatientAllergyId = table.Column<int>(nullable: false),
                    AllergyId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AllergyCausingMedicins", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AllergyCausingMedicins_Allegries_AllergyId",
                        column: x => x.AllergyId,
                        principalTable: "Allegries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AllergyCausingMedicins_Medicines_MedicineId",
                        column: x => x.MedicineId,
                        principalTable: "Medicines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AllergyCausingMedicins_PatientAllergies_PatientAllergyId",
                        column: x => x.PatientAllergyId,
                        principalTable: "PatientAllergies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PatientMedicines",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsValid = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(nullable: false),
                    LastUpdatedBy = table.Column<int>(nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(nullable: false),
                    MedicineId = table.Column<int>(nullable: false),
                    PersonId = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    PatientAllergyId = table.Column<int>(nullable: true),
                    PatientsDiseasesId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PatientMedicines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PatientMedicines_Medicines_MedicineId",
                        column: x => x.MedicineId,
                        principalTable: "Medicines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PatientMedicines_PatientAllergies_PatientAllergyId",
                        column: x => x.PatientAllergyId,
                        principalTable: "PatientAllergies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PatientMedicines_PatientsDiseases_PatientsDiseasesId",
                        column: x => x.PatientsDiseasesId,
                        principalTable: "PatientsDiseases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PatientMedicines_People_PersonId",
                        column: x => x.PersonId,
                        principalTable: "People",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_JobOffers_CityId",
                table: "JobOffers",
                column: "CityId");

            migrationBuilder.CreateIndex(
                name: "IX_JobOffers_LocationId",
                table: "JobOffers",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_JobApplications_PersonId",
                table: "JobApplications",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Files_AllergyId",
                table: "Files",
                column: "AllergyId");

            migrationBuilder.CreateIndex(
                name: "IX_Files_DiseasesId",
                table: "Files",
                column: "DiseasesId");

            migrationBuilder.CreateIndex(
                name: "IX_Files_MedicineId",
                table: "Files",
                column: "MedicineId");

            migrationBuilder.CreateIndex(
                name: "IX_Files_PatientAllergyId",
                table: "Files",
                column: "PatientAllergyId");

            migrationBuilder.CreateIndex(
                name: "IX_Files_PatientSurgeryId",
                table: "Files",
                column: "PatientSurgeryId");

            migrationBuilder.CreateIndex(
                name: "IX_Files_PatientsDiseasesId",
                table: "Files",
                column: "PatientsDiseasesId");

            migrationBuilder.CreateIndex(
                name: "IX_Files_SurgeryId",
                table: "Files",
                column: "SurgeryId");

            migrationBuilder.CreateIndex(
                name: "IX_Files_JobApplicationId",
                table: "Files",
                column: "JobApplicationId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Files_X_Ray_PicturesId",
                table: "Files",
                column: "X_Ray_PicturesId");

            migrationBuilder.CreateIndex(
                name: "IX_AllergyCausingMedicins_AllergyId",
                table: "AllergyCausingMedicins",
                column: "AllergyId");

            migrationBuilder.CreateIndex(
                name: "IX_AllergyCausingMedicins_MedicineId",
                table: "AllergyCausingMedicins",
                column: "MedicineId");

            migrationBuilder.CreateIndex(
                name: "IX_AllergyCausingMedicins_PatientAllergyId",
                table: "AllergyCausingMedicins",
                column: "PatientAllergyId");

            migrationBuilder.CreateIndex(
                name: "IX_Notes_PersonId",
                table: "Notes",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferEntities_CourseId",
                table: "OfferEntities",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferEntities_OfferId",
                table: "OfferEntities",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferEntities_ProductId",
                table: "OfferEntities",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferEntities_ServiceId",
                table: "OfferEntities",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientAllergies_AllegryId",
                table: "PatientAllergies",
                column: "AllegryId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientAllergies_PersonId",
                table: "PatientAllergies",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientMedicines_MedicineId",
                table: "PatientMedicines",
                column: "MedicineId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientMedicines_PatientAllergyId",
                table: "PatientMedicines",
                column: "PatientAllergyId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientMedicines_PatientsDiseasesId",
                table: "PatientMedicines",
                column: "PatientsDiseasesId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientMedicines_PersonId",
                table: "PatientMedicines",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientsDiseases_DiseasesId",
                table: "PatientsDiseases",
                column: "DiseasesId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientsDiseases_PersonId",
                table: "PatientsDiseases",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientSurgeries_PersonId",
                table: "PatientSurgeries",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientSurgeries_SurgeryId",
                table: "PatientSurgeries",
                column: "SurgeryId");

            migrationBuilder.CreateIndex(
                name: "IX_X_Ray_Pictures_PersonId",
                table: "X_Ray_Pictures",
                column: "PersonId");

            migrationBuilder.AddForeignKey(
                name: "FK_Files_Allegries_AllergyId",
                table: "Files",
                column: "AllergyId",
                principalTable: "Allegries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Files_Diseases_DiseasesId",
                table: "Files",
                column: "DiseasesId",
                principalTable: "Diseases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Files_Medicines_MedicineId",
                table: "Files",
                column: "MedicineId",
                principalTable: "Medicines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Files_PatientAllergies_PatientAllergyId",
                table: "Files",
                column: "PatientAllergyId",
                principalTable: "PatientAllergies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Files_PatientSurgeries_PatientSurgeryId",
                table: "Files",
                column: "PatientSurgeryId",
                principalTable: "PatientSurgeries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Files_PatientsDiseases_PatientsDiseasesId",
                table: "Files",
                column: "PatientsDiseasesId",
                principalTable: "PatientsDiseases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Files_Surgeries_SurgeryId",
                table: "Files",
                column: "SurgeryId",
                principalTable: "Surgeries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Files_JobApplications_JobApplicationId",
                table: "Files",
                column: "JobApplicationId",
                principalTable: "JobApplications",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Files_X_Ray_Pictures_X_Ray_PicturesId",
                table: "Files",
                column: "X_Ray_PicturesId",
                principalTable: "X_Ray_Pictures",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JobApplications_People_PersonId",
                table: "JobApplications",
                column: "PersonId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JobOffers_Cities_CityId",
                table: "JobOffers",
                column: "CityId",
                principalTable: "Cities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JobOffers_Locations_LocationId",
                table: "JobOffers",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order_People_PersonId",
                table: "Order",
                column: "PersonId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Files_Allegries_AllergyId",
                table: "Files");

            migrationBuilder.DropForeignKey(
                name: "FK_Files_Diseases_DiseasesId",
                table: "Files");

            migrationBuilder.DropForeignKey(
                name: "FK_Files_Medicines_MedicineId",
                table: "Files");

            migrationBuilder.DropForeignKey(
                name: "FK_Files_PatientAllergies_PatientAllergyId",
                table: "Files");

            migrationBuilder.DropForeignKey(
                name: "FK_Files_PatientSurgeries_PatientSurgeryId",
                table: "Files");

            migrationBuilder.DropForeignKey(
                name: "FK_Files_PatientsDiseases_PatientsDiseasesId",
                table: "Files");

            migrationBuilder.DropForeignKey(
                name: "FK_Files_Surgeries_SurgeryId",
                table: "Files");

            migrationBuilder.DropForeignKey(
                name: "FK_Files_JobApplications_JobApplicationId",
                table: "Files");

            migrationBuilder.DropForeignKey(
                name: "FK_Files_X_Ray_Pictures_X_Ray_PicturesId",
                table: "Files");

            migrationBuilder.DropForeignKey(
                name: "FK_JobApplications_People_PersonId",
                table: "JobApplications");

            migrationBuilder.DropForeignKey(
                name: "FK_JobOffers_Cities_CityId",
                table: "JobOffers");

            migrationBuilder.DropForeignKey(
                name: "FK_JobOffers_Locations_LocationId",
                table: "JobOffers");

            migrationBuilder.DropForeignKey(
                name: "FK_Order_People_PersonId",
                table: "Order");

            migrationBuilder.DropTable(
                name: "AllergyCausingMedicins");

            migrationBuilder.DropTable(
                name: "Notes");

            migrationBuilder.DropTable(
                name: "OfferEntities");

            migrationBuilder.DropTable(
                name: "PatientMedicines");

            migrationBuilder.DropTable(
                name: "PatientSurgeries");

            migrationBuilder.DropTable(
                name: "X_Ray_Pictures");

            migrationBuilder.DropTable(
                name: "Offers");

            migrationBuilder.DropTable(
                name: "Medicines");

            migrationBuilder.DropTable(
                name: "PatientAllergies");

            migrationBuilder.DropTable(
                name: "PatientsDiseases");

            migrationBuilder.DropTable(
                name: "Surgeries");

            migrationBuilder.DropTable(
                name: "Allegries");

            migrationBuilder.DropTable(
                name: "Diseases");

            migrationBuilder.DropIndex(
                name: "IX_JobOffers_CityId",
                table: "JobOffers");

            migrationBuilder.DropIndex(
                name: "IX_JobOffers_LocationId",
                table: "JobOffers");

            migrationBuilder.DropIndex(
                name: "IX_JobApplications_PersonId",
                table: "JobApplications");

            migrationBuilder.DropIndex(
                name: "IX_Files_AllergyId",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_DiseasesId",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_MedicineId",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_PatientAllergyId",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_PatientSurgeryId",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_PatientsDiseasesId",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_SurgeryId",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_JobApplicationId",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_X_Ray_PicturesId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "PaymentStatus",
                table: "PersonCourses");

            migrationBuilder.DropColumn(
                name: "CityId",
                table: "JobOffers");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "JobOffers");

            migrationBuilder.DropColumn(
                name: "MaxSalary",
                table: "JobOffers");

            migrationBuilder.DropColumn(
                name: "PlaceOfWork",
                table: "JobOffers");

            migrationBuilder.DropColumn(
                name: "Salary",
                table: "JobOffers");

            migrationBuilder.DropColumn(
                name: "SalaryType",
                table: "JobOffers");

            migrationBuilder.DropColumn(
                name: "CvFileId",
                table: "JobApplications");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "JobApplications");

            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "JobApplications");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "JobApplications");

            migrationBuilder.DropColumn(
                name: "PersonId",
                table: "JobApplications");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "JobApplications");

            migrationBuilder.DropColumn(
                name: "AllergyId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "DiseasesId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "MedicineId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "PatientAllergyId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "PatientSurgeryId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "PatientsDiseasesId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "SurgeryId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "JobApplicationId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "X_Ray_PicturesId",
                table: "Files");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "WebsitePages",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "WebContents",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "WebContentRoles",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "UserLanguageTranslations",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "UserLanguages",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Tips",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "TeamMembers",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Specification",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Settings",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "SessionTypes",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "SessionServices",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "ServiceTranslations",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Services",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Question",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "ProductTranslations",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "ProductSpecifications",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "ProductSizes",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "DepartmentId",
                table: "Products",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Products",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "ProductReviews",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "ProductColors",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "PrivacyMaterials",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "PostTypes",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "PersonCourses",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "CvFileId",
                table: "People",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "People",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "UserId1",
                table: "People",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Partners",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "OrderProducts",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Order",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "OpenningHoures",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "MultiLevelCategories",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "LocationSchedule",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Locations",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "LabelTranslations",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AlterColumn<string>(
                name: "ContractType",
                table: "JobOffers",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "JobOffers",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "JobApplications",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "JobApplications",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Instructors",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "InformationTranslation",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Informations",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "GroupTranslation",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Groups",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "ApartmentId",
                table: "Files",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ConstructionId",
                table: "Files",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ClientId",
                table: "Files",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FloorId",
                table: "Files",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProjectId",
                table: "Files",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserJobId",
                table: "Files",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Files",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "FeedBack",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "DoctorLocations",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Currencies",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "CourseTypes",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "CourseTypeCourses",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Courses",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "CountryTranslations",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Countries",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "ContactMessages",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "ContactInfo",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "ClientTranslation",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Clients",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "CityTranslations",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Cities",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "CategoryTranslations",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Categories",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "CarouselTranslations",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Carousels",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "CarouselItemTranslations",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "CarouselItems",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Brand",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Bookings",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "BlogVoteSet",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "BlogTranslation",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Blogs",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "AspNetUsers",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "AspNetRoles",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Answer",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "Constructions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CategoryId = table.Column<int>(type: "int", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    CultureCode = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Guid = table.Column<Guid>(type: "char(36)", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    Location = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    LocationLatitude = table.Column<decimal>(type: "decimal(65,30)", nullable: true),
                    LocationLongitude = table.Column<decimal>(type: "decimal(65,30)", nullable: true),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Order = table.Column<int>(type: "int", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Constructions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Constructions_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Departments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    CultureCode = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Guid = table.Column<Guid>(type: "char(36)", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Directions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    CultureCode = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Guid = table.Column<Guid>(type: "char(36)", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Order = table.Column<int>(type: "int", nullable: false),
                    UserLanguageId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Directions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Directions_UserLanguages_UserLanguageId",
                        column: x => x.UserLanguageId,
                        principalTable: "UserLanguages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Features",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    CultureCode = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Guid = table.Column<Guid>(type: "char(36)", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    UserLanguageId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Features", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Features_UserLanguages_UserLanguageId",
                        column: x => x.UserLanguageId,
                        principalTable: "UserLanguages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServiceOffers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    Description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Guid = table.Column<Guid>(type: "char(36)", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    NewPrice = table.Column<double>(type: "double", nullable: false),
                    ServiceId = table.Column<int>(type: "int", nullable: false),
                    Title = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    percentage = table.Column<double>(type: "double", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceOffers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServiceOffers_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ConstructionTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ConstructionId = table.Column<int>(type: "int", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    CultureCode = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Guid = table.Column<Guid>(type: "char(36)", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    Location = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    UserLanguageId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConstructionTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ConstructionTranslations_Constructions_ConstructionId",
                        column: x => x.ConstructionId,
                        principalTable: "Constructions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ConstructionTranslations_UserLanguages_UserLanguageId",
                        column: x => x.UserLanguageId,
                        principalTable: "UserLanguages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Floors",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ConstructionId = table.Column<int>(type: "int", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    CultureCode = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    FloorNumber = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Guid = table.Column<Guid>(type: "char(36)", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Floors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Floors_Constructions_ConstructionId",
                        column: x => x.ConstructionId,
                        principalTable: "Constructions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DepartmentCategories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CategoryId = table.Column<int>(type: "int", nullable: false),
                    DepartmentId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartmentCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DepartmentCategories_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DepartmentCategories_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DepartmentTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    CultureCode = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    DepartmentId = table.Column<int>(type: "int", nullable: false),
                    Description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Guid = table.Column<Guid>(type: "char(36)", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartmentTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DepartmentTranslations_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CategoryId = table.Column<int>(type: "int", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    CultureCode = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Customer = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    DepartmentId = table.Column<int>(type: "int", nullable: true),
                    Description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Guid = table.Column<Guid>(type: "char(36)", nullable: false),
                    IsShow = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    Location = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    LocationLatitude = table.Column<double>(type: "double", nullable: true),
                    LocationLongitude = table.Column<double>(type: "double", nullable: true),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Order = table.Column<int>(type: "int", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Projects_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DirectionTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    CultureCode = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    DirectionId = table.Column<int>(type: "int", nullable: false),
                    Guid = table.Column<Guid>(type: "char(36)", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    UserLanguageId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DirectionTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DirectionTranslations_Directions_DirectionId",
                        column: x => x.DirectionId,
                        principalTable: "Directions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DirectionTranslations_UserLanguages_UserLanguageId",
                        column: x => x.UserLanguageId,
                        principalTable: "UserLanguages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FeatureTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    CultureCode = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    FeatureId = table.Column<int>(type: "int", nullable: false),
                    Guid = table.Column<Guid>(type: "char(36)", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    UserLanguageId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeatureTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FeatureTranslations_Features_FeatureId",
                        column: x => x.FeatureId,
                        principalTable: "Features",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeatureTranslations_UserLanguages_UserLanguageId",
                        column: x => x.UserLanguageId,
                        principalTable: "UserLanguages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Apartments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    CultureCode = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    FloorId = table.Column<int>(type: "int", nullable: false),
                    Guid = table.Column<Guid>(type: "char(36)", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    NumberOfBalconies = table.Column<int>(type: "int", nullable: false),
                    NumberOfBathrooms = table.Column<int>(type: "int", nullable: false),
                    NumberOfRooms = table.Column<int>(type: "int", nullable: false),
                    Space = table.Column<double>(type: "double", nullable: false),
                    UserLanguageId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Apartments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Apartments_Floors_FloorId",
                        column: x => x.FloorId,
                        principalTable: "Floors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Apartments_UserLanguages_UserLanguageId",
                        column: x => x.UserLanguageId,
                        principalTable: "UserLanguages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FloorTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    CultureCode = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    FloorId = table.Column<int>(type: "int", nullable: false),
                    Guid = table.Column<Guid>(type: "char(36)", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FloorTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FloorTranslations_Floors_FloorId",
                        column: x => x.FloorId,
                        principalTable: "Floors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProjectTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    CultureCode = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Customer = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Guid = table.Column<Guid>(type: "char(36)", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    Location = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    UserLanguageId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectTranslations_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProjectTranslations_UserLanguages_UserLanguageId",
                        column: x => x.UserLanguageId,
                        principalTable: "UserLanguages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ApartmentDirections",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ApartmentId = table.Column<int>(type: "int", nullable: false),
                    DirectionId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApartmentDirections", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApartmentDirections_Apartments_ApartmentId",
                        column: x => x.ApartmentId,
                        principalTable: "Apartments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ApartmentDirections_Directions_DirectionId",
                        column: x => x.DirectionId,
                        principalTable: "Directions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ApartmentFeatures",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ApartmentId = table.Column<int>(type: "int", nullable: false),
                    FeatureId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApartmentFeatures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApartmentFeatures_Apartments_ApartmentId",
                        column: x => x.ApartmentId,
                        principalTable: "Apartments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ApartmentFeatures_Features_FeatureId",
                        column: x => x.FeatureId,
                        principalTable: "Features",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ApartmentTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ApartmentId = table.Column<int>(type: "int", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    CultureCode = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Guid = table.Column<Guid>(type: "char(36)", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    UserLanguageId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApartmentTranslations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApartmentTranslations_Apartments_ApartmentId",
                        column: x => x.ApartmentId,
                        principalTable: "Apartments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ApartmentTranslations_UserLanguages_UserLanguageId",
                        column: x => x.UserLanguageId,
                        principalTable: "UserLanguages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Products_DepartmentId",
                table: "Products",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_People_UserId1",
                table: "People",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_JobApplications_UserId",
                table: "JobApplications",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Files_ApartmentId",
                table: "Files",
                column: "ApartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Files_ConstructionId",
                table: "Files",
                column: "ConstructionId");

            migrationBuilder.CreateIndex(
                name: "IX_Files_ClientId",
                table: "Files",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Files_FloorId",
                table: "Files",
                column: "FloorId");

            migrationBuilder.CreateIndex(
                name: "IX_Files_ProjectId",
                table: "Files",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Files_UserJobId",
                table: "Files",
                column: "UserJobId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ApartmentDirections_ApartmentId",
                table: "ApartmentDirections",
                column: "ApartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_ApartmentDirections_DirectionId",
                table: "ApartmentDirections",
                column: "DirectionId");

            migrationBuilder.CreateIndex(
                name: "IX_ApartmentFeatures_ApartmentId",
                table: "ApartmentFeatures",
                column: "ApartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_ApartmentFeatures_FeatureId",
                table: "ApartmentFeatures",
                column: "FeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_Apartments_FloorId",
                table: "Apartments",
                column: "FloorId");

            migrationBuilder.CreateIndex(
                name: "IX_Apartments_UserLanguageId",
                table: "Apartments",
                column: "UserLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_ApartmentTranslations_ApartmentId",
                table: "ApartmentTranslations",
                column: "ApartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_ApartmentTranslations_UserLanguageId",
                table: "ApartmentTranslations",
                column: "UserLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_Constructions_CategoryId",
                table: "Constructions",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ConstructionTranslations_ConstructionId",
                table: "ConstructionTranslations",
                column: "ConstructionId");

            migrationBuilder.CreateIndex(
                name: "IX_ConstructionTranslations_UserLanguageId",
                table: "ConstructionTranslations",
                column: "UserLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_DepartmentCategories_CategoryId",
                table: "DepartmentCategories",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_DepartmentCategories_DepartmentId",
                table: "DepartmentCategories",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_DepartmentTranslations_DepartmentId",
                table: "DepartmentTranslations",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Directions_UserLanguageId",
                table: "Directions",
                column: "UserLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_DirectionTranslations_DirectionId",
                table: "DirectionTranslations",
                column: "DirectionId");

            migrationBuilder.CreateIndex(
                name: "IX_DirectionTranslations_UserLanguageId",
                table: "DirectionTranslations",
                column: "UserLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_Features_UserLanguageId",
                table: "Features",
                column: "UserLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureTranslations_FeatureId",
                table: "FeatureTranslations",
                column: "FeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureTranslations_UserLanguageId",
                table: "FeatureTranslations",
                column: "UserLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_Floors_ConstructionId",
                table: "Floors",
                column: "ConstructionId");

            migrationBuilder.CreateIndex(
                name: "IX_FloorTranslations_FloorId",
                table: "FloorTranslations",
                column: "FloorId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_CategoryId",
                table: "Projects",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_DepartmentId",
                table: "Projects",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectTranslations_ProjectId",
                table: "ProjectTranslations",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectTranslations_UserLanguageId",
                table: "ProjectTranslations",
                column: "UserLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceOffers_ServiceId",
                table: "ServiceOffers",
                column: "ServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Files_Apartments_ApartmentId",
                table: "Files",
                column: "ApartmentId",
                principalTable: "Apartments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Files_Constructions_ConstructionId",
                table: "Files",
                column: "ConstructionId",
                principalTable: "Constructions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Files_Clients_ClientId",
                table: "Files",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Files_Floors_FloorId",
                table: "Files",
                column: "FloorId",
                principalTable: "Floors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Files_Projects_ProjectId",
                table: "Files",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Files_People_UserJobId",
                table: "Files",
                column: "UserJobId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JobApplications_People_UserId",
                table: "JobApplications",
                column: "UserId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order_AspNetUsers_PersonId",
                table: "Order",
                column: "PersonId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_People_AspNetUsers_UserId1",
                table: "People",
                column: "UserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Departments_DepartmentId",
                table: "Products",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class add_multiLevelCategory_groups : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Groups");

            migrationBuilder.DropColumn(
                name: "CategoryName",
                table: "Groups");

            migrationBuilder.DropColumn(
                name: "SubCategoryId",
                table: "Groups");

            migrationBuilder.DropColumn(
                name: "SubCategoryName",
                table: "Groups");

            migrationBuilder.AddColumn<int>(
                name: "MultiLevelCategoryId",
                table: "Groups",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Groups_MultiLevelCategoryId",
                table: "Groups",
                column: "MultiLevelCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Groups_MultiLevelCategories_MultiLevelCategoryId",
                table: "Groups",
                column: "MultiLevelCategoryId",
                principalTable: "MultiLevelCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Groups_MultiLevelCategories_MultiLevelCategoryId",
                table: "Groups");

            migrationBuilder.DropIndex(
                name: "IX_Groups_MultiLevelCategoryId",
                table: "Groups");

            migrationBuilder.DropColumn(
                name: "MultiLevelCategoryId",
                table: "Groups");

            migrationBuilder.AddColumn<Guid>(
                name: "CategoryId",
                table: "Groups",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "CategoryName",
                table: "Groups",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "SubCategoryId",
                table: "Groups",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "SubCategoryName",
                table: "Groups",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);
        }
    }
}

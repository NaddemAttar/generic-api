﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class add_isHidden_ShowFirst_Blogs_Questions_Service : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsHidden",
                table: "Services",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "ShowFirst",
                table: "Services",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsHidden",
                table: "Question",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "ShowFirst",
                table: "Question",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsHidden",
                table: "Blogs",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "ShowFirst",
                table: "Blogs",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsHidden",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "ShowFirst",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "IsHidden",
                table: "Question");

            migrationBuilder.DropColumn(
                name: "ShowFirst",
                table: "Question");

            migrationBuilder.DropColumn(
                name: "IsHidden",
                table: "Blogs");

            migrationBuilder.DropColumn(
                name: "ShowFirst",
                table: "Blogs");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class link_cpuser_with_health_profile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MedicalTests_People_PersonId",
                table: "MedicalTests");

            migrationBuilder.DropForeignKey(
                name: "FK_PatientAllergies_People_PersonId",
                table: "PatientAllergies");

            migrationBuilder.DropForeignKey(
                name: "FK_PatientMedicines_People_PersonId",
                table: "PatientMedicines");

            migrationBuilder.DropForeignKey(
                name: "FK_PatientsDiseases_People_PersonId",
                table: "PatientsDiseases");

            migrationBuilder.DropForeignKey(
                name: "FK_PatientSurgeries_People_PersonId",
                table: "PatientSurgeries");

            migrationBuilder.DropTable(
                name: "PersonNotes");

            migrationBuilder.DropIndex(
                name: "IX_PatientSurgeries_PersonId",
                table: "PatientSurgeries");

            migrationBuilder.DropIndex(
                name: "IX_PatientsDiseases_PersonId",
                table: "PatientsDiseases");

            migrationBuilder.DropIndex(
                name: "IX_PatientMedicines_PersonId",
                table: "PatientMedicines");

            migrationBuilder.DropIndex(
                name: "IX_PatientAllergies_PersonId",
                table: "PatientAllergies");

            migrationBuilder.DropIndex(
                name: "IX_MedicalTests_PersonId",
                table: "MedicalTests");

            migrationBuilder.DropColumn(
                name: "PersonId",
                table: "PatientSurgeries");

            migrationBuilder.DropColumn(
                name: "PersonId",
                table: "PatientsDiseases");

            migrationBuilder.DropColumn(
                name: "PersonId",
                table: "PatientMedicines");

            migrationBuilder.DropColumn(
                name: "PersonId",
                table: "PatientAllergies");

            migrationBuilder.DropColumn(
                name: "PersonId",
                table: "MedicalTests");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Tips",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "TeamMembers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "SessionTypes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Question",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Projects",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Products",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "PatientSurgeries",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "PatientsDiseases",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "PatientMedicines",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "PatientAllergies",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "OpenningHoures",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Notes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "MedicalTests",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "LocationSchedule",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Groups",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "FeedBack",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Courses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HealthCareProviderId",
                table: "Bookings",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Blogs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Birthdate",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Gender",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PersonalPhoto",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Answer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tips_UserId",
                table: "Tips",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_TeamMembers_UserId",
                table: "TeamMembers",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_SessionTypes_UserId",
                table: "SessionTypes",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Services_UserId",
                table: "Services",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Question_UserId",
                table: "Question",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_UserId",
                table: "Projects",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_UserId",
                table: "Products",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientSurgeries_UserId",
                table: "PatientSurgeries",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientsDiseases_UserId",
                table: "PatientsDiseases",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientMedicines_UserId",
                table: "PatientMedicines",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientAllergies_UserId",
                table: "PatientAllergies",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_OpenningHoures_UserId",
                table: "OpenningHoures",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Notes_UserId",
                table: "Notes",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_MedicalTests_UserId",
                table: "MedicalTests",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_LocationSchedule_UserId",
                table: "LocationSchedule",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Groups_UserId",
                table: "Groups",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBack_UserId",
                table: "FeedBack",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Courses_UserId",
                table: "Courses",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Bookings_HealthCareProviderId",
                table: "Bookings",
                column: "HealthCareProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_Blogs_UserId",
                table: "Blogs",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Answer_UserId",
                table: "Answer",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Answer_AspNetUsers_UserId",
                table: "Answer",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Blogs_AspNetUsers_UserId",
                table: "Blogs",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Bookings_AspNetUsers_HealthCareProviderId",
                table: "Bookings",
                column: "HealthCareProviderId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Courses_AspNetUsers_UserId",
                table: "Courses",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FeedBack_AspNetUsers_UserId",
                table: "FeedBack",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Groups_AspNetUsers_UserId",
                table: "Groups",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_LocationSchedule_AspNetUsers_UserId",
                table: "LocationSchedule",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MedicalTests_AspNetUsers_UserId",
                table: "MedicalTests",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Notes_AspNetUsers_UserId",
                table: "Notes",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OpenningHoures_AspNetUsers_UserId",
                table: "OpenningHoures",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PatientAllergies_AspNetUsers_UserId",
                table: "PatientAllergies",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PatientMedicines_AspNetUsers_UserId",
                table: "PatientMedicines",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PatientsDiseases_AspNetUsers_UserId",
                table: "PatientsDiseases",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PatientSurgeries_AspNetUsers_UserId",
                table: "PatientSurgeries",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_AspNetUsers_UserId",
                table: "Products",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_AspNetUsers_UserId",
                table: "Projects",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Question_AspNetUsers_UserId",
                table: "Question",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Services_AspNetUsers_UserId",
                table: "Services",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SessionTypes_AspNetUsers_UserId",
                table: "SessionTypes",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TeamMembers_AspNetUsers_UserId",
                table: "TeamMembers",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tips_AspNetUsers_UserId",
                table: "Tips",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Answer_AspNetUsers_UserId",
                table: "Answer");

            migrationBuilder.DropForeignKey(
                name: "FK_Blogs_AspNetUsers_UserId",
                table: "Blogs");

            migrationBuilder.DropForeignKey(
                name: "FK_Bookings_AspNetUsers_HealthCareProviderId",
                table: "Bookings");

            migrationBuilder.DropForeignKey(
                name: "FK_Courses_AspNetUsers_UserId",
                table: "Courses");

            migrationBuilder.DropForeignKey(
                name: "FK_FeedBack_AspNetUsers_UserId",
                table: "FeedBack");

            migrationBuilder.DropForeignKey(
                name: "FK_Groups_AspNetUsers_UserId",
                table: "Groups");

            migrationBuilder.DropForeignKey(
                name: "FK_LocationSchedule_AspNetUsers_UserId",
                table: "LocationSchedule");

            migrationBuilder.DropForeignKey(
                name: "FK_MedicalTests_AspNetUsers_UserId",
                table: "MedicalTests");

            migrationBuilder.DropForeignKey(
                name: "FK_Notes_AspNetUsers_UserId",
                table: "Notes");

            migrationBuilder.DropForeignKey(
                name: "FK_OpenningHoures_AspNetUsers_UserId",
                table: "OpenningHoures");

            migrationBuilder.DropForeignKey(
                name: "FK_PatientAllergies_AspNetUsers_UserId",
                table: "PatientAllergies");

            migrationBuilder.DropForeignKey(
                name: "FK_PatientMedicines_AspNetUsers_UserId",
                table: "PatientMedicines");

            migrationBuilder.DropForeignKey(
                name: "FK_PatientsDiseases_AspNetUsers_UserId",
                table: "PatientsDiseases");

            migrationBuilder.DropForeignKey(
                name: "FK_PatientSurgeries_AspNetUsers_UserId",
                table: "PatientSurgeries");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_AspNetUsers_UserId",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_Projects_AspNetUsers_UserId",
                table: "Projects");

            migrationBuilder.DropForeignKey(
                name: "FK_Question_AspNetUsers_UserId",
                table: "Question");

            migrationBuilder.DropForeignKey(
                name: "FK_Services_AspNetUsers_UserId",
                table: "Services");

            migrationBuilder.DropForeignKey(
                name: "FK_SessionTypes_AspNetUsers_UserId",
                table: "SessionTypes");

            migrationBuilder.DropForeignKey(
                name: "FK_TeamMembers_AspNetUsers_UserId",
                table: "TeamMembers");

            migrationBuilder.DropForeignKey(
                name: "FK_Tips_AspNetUsers_UserId",
                table: "Tips");

            migrationBuilder.DropIndex(
                name: "IX_Tips_UserId",
                table: "Tips");

            migrationBuilder.DropIndex(
                name: "IX_TeamMembers_UserId",
                table: "TeamMembers");

            migrationBuilder.DropIndex(
                name: "IX_SessionTypes_UserId",
                table: "SessionTypes");

            migrationBuilder.DropIndex(
                name: "IX_Services_UserId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Question_UserId",
                table: "Question");

            migrationBuilder.DropIndex(
                name: "IX_Projects_UserId",
                table: "Projects");

            migrationBuilder.DropIndex(
                name: "IX_Products_UserId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_PatientSurgeries_UserId",
                table: "PatientSurgeries");

            migrationBuilder.DropIndex(
                name: "IX_PatientsDiseases_UserId",
                table: "PatientsDiseases");

            migrationBuilder.DropIndex(
                name: "IX_PatientMedicines_UserId",
                table: "PatientMedicines");

            migrationBuilder.DropIndex(
                name: "IX_PatientAllergies_UserId",
                table: "PatientAllergies");

            migrationBuilder.DropIndex(
                name: "IX_OpenningHoures_UserId",
                table: "OpenningHoures");

            migrationBuilder.DropIndex(
                name: "IX_Notes_UserId",
                table: "Notes");

            migrationBuilder.DropIndex(
                name: "IX_MedicalTests_UserId",
                table: "MedicalTests");

            migrationBuilder.DropIndex(
                name: "IX_LocationSchedule_UserId",
                table: "LocationSchedule");

            migrationBuilder.DropIndex(
                name: "IX_Groups_UserId",
                table: "Groups");

            migrationBuilder.DropIndex(
                name: "IX_FeedBack_UserId",
                table: "FeedBack");

            migrationBuilder.DropIndex(
                name: "IX_Courses_UserId",
                table: "Courses");

            migrationBuilder.DropIndex(
                name: "IX_Bookings_HealthCareProviderId",
                table: "Bookings");

            migrationBuilder.DropIndex(
                name: "IX_Blogs_UserId",
                table: "Blogs");

            migrationBuilder.DropIndex(
                name: "IX_Answer_UserId",
                table: "Answer");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Tips");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "TeamMembers");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "SessionTypes");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Question");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "PatientSurgeries");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "PatientsDiseases");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "PatientMedicines");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "PatientAllergies");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "OpenningHoures");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Notes");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "MedicalTests");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "LocationSchedule");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Groups");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "FeedBack");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Courses");

            migrationBuilder.DropColumn(
                name: "HealthCareProviderId",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Blogs");

            migrationBuilder.DropColumn(
                name: "Birthdate",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Gender",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PersonalPhoto",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Answer");

            migrationBuilder.AddColumn<int>(
                name: "PersonId",
                table: "PatientSurgeries",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PersonId",
                table: "PatientsDiseases",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PersonId",
                table: "PatientMedicines",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PersonId",
                table: "PatientAllergies",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PersonId",
                table: "MedicalTests",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "PersonNotes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    NoteId = table.Column<int>(type: "int", nullable: false),
                    PersonId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonNotes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PersonNotes_Notes_NoteId",
                        column: x => x.NoteId,
                        principalTable: "Notes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PersonNotes_People_PersonId",
                        column: x => x.PersonId,
                        principalTable: "People",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PatientSurgeries_PersonId",
                table: "PatientSurgeries",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientsDiseases_PersonId",
                table: "PatientsDiseases",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientMedicines_PersonId",
                table: "PatientMedicines",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientAllergies_PersonId",
                table: "PatientAllergies",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_MedicalTests_PersonId",
                table: "MedicalTests",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_PersonNotes_NoteId",
                table: "PersonNotes",
                column: "NoteId");

            migrationBuilder.CreateIndex(
                name: "IX_PersonNotes_PersonId",
                table: "PersonNotes",
                column: "PersonId");

            migrationBuilder.AddForeignKey(
                name: "FK_MedicalTests_People_PersonId",
                table: "MedicalTests",
                column: "PersonId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PatientAllergies_People_PersonId",
                table: "PatientAllergies",
                column: "PersonId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PatientMedicines_People_PersonId",
                table: "PatientMedicines",
                column: "PersonId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PatientsDiseases_People_PersonId",
                table: "PatientsDiseases",
                column: "PersonId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PatientSurgeries_People_PersonId",
                table: "PatientSurgeries",
                column: "PersonId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

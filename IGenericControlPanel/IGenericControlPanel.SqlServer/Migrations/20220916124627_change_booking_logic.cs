﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class change_booking_logic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "CPUserId",
                table: "Bookings",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HealthCareProviderEnterpriseId",
                table: "Bookings",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "HealthCareProdviderEnterpriseServices",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ServiceId = table.Column<int>(type: "int", nullable: false),
                    HealthCareProviderEnterpriseId = table.Column<int>(type: "int", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HealthCareProdviderEnterpriseServices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HealthCareProdviderEnterpriseServices_HealthCareProviderEnte~",
                        column: x => x.HealthCareProviderEnterpriseId,
                        principalTable: "HealthCareProviderEnterprise",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HealthCareProdviderEnterpriseServices_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_Bookings_HealthCareProviderEnterpriseId",
                table: "Bookings",
                column: "HealthCareProviderEnterpriseId");

            migrationBuilder.CreateIndex(
                name: "IX_HealthCareProdviderEnterpriseServices_HealthCareProviderEnte~",
                table: "HealthCareProdviderEnterpriseServices",
                column: "HealthCareProviderEnterpriseId");

            migrationBuilder.CreateIndex(
                name: "IX_HealthCareProdviderEnterpriseServices_ServiceId",
                table: "HealthCareProdviderEnterpriseServices",
                column: "ServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bookings_HealthCareProviderEnterprise_HealthCareProviderEnte~",
                table: "Bookings",
                column: "HealthCareProviderEnterpriseId",
                principalTable: "HealthCareProviderEnterprise",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bookings_HealthCareProviderEnterprise_HealthCareProviderEnte~",
                table: "Bookings");

            migrationBuilder.DropTable(
                name: "HealthCareProdviderEnterpriseServices");

            migrationBuilder.DropIndex(
                name: "IX_Bookings_HealthCareProviderEnterpriseId",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "HealthCareProviderEnterpriseId",
                table: "Bookings");

            migrationBuilder.AlterColumn<int>(
                name: "CPUserId",
                table: "Bookings",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class change_currency_logic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Currencies_CurrencyId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_CurrencyId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "CurrencyId",
                table: "Products");

            migrationBuilder.AddColumn<int>(
                name: "CurrencyId",
                table: "Settings",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SettingId",
                table: "Currencies",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Settings_CurrencyId",
                table: "Settings",
                column: "CurrencyId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Settings_Currencies_CurrencyId",
                table: "Settings",
                column: "CurrencyId",
                principalTable: "Currencies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Settings_Currencies_CurrencyId",
                table: "Settings");

            migrationBuilder.DropIndex(
                name: "IX_Settings_CurrencyId",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "CurrencyId",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "SettingId",
                table: "Currencies");

            migrationBuilder.AddColumn<int>(
                name: "CurrencyId",
                table: "Products",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_CurrencyId",
                table: "Products",
                column: "CurrencyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Currencies_CurrencyId",
                table: "Products",
                column: "CurrencyId",
                principalTable: "Currencies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

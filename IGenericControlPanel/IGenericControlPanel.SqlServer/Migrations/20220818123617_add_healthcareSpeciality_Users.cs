﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class add_healthcareSpeciality_Users : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Services_HealthCareSpecialties_HealthCareSpecialtyId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Services_HealthCareSpecialtyId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "HealthCareSpecialtyId",
                table: "Services");

            migrationBuilder.CreateTable(
                name: "HealthCareProviderSpecialties",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    HealthCareSpecialtyId = table.Column<int>(type: "int", nullable: false),
                    CPUserId = table.Column<int>(type: "int", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HealthCareProviderSpecialties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HealthCareProviderSpecialties_AspNetUsers_CPUserId",
                        column: x => x.CPUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HealthCareProviderSpecialties_HealthCareSpecialties_HealthCa~",
                        column: x => x.HealthCareSpecialtyId,
                        principalTable: "HealthCareSpecialties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "HealthCareSpecialtyServices",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ServiceId = table.Column<int>(type: "int", nullable: false),
                    HealthCareSpecialtyId = table.Column<int>(type: "int", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HealthCareSpecialtyServices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HealthCareSpecialtyServices_HealthCareSpecialties_HealthCare~",
                        column: x => x.HealthCareSpecialtyId,
                        principalTable: "HealthCareSpecialties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HealthCareSpecialtyServices_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_HealthCareProviderSpecialties_CPUserId",
                table: "HealthCareProviderSpecialties",
                column: "CPUserId");

            migrationBuilder.CreateIndex(
                name: "IX_HealthCareProviderSpecialties_HealthCareSpecialtyId",
                table: "HealthCareProviderSpecialties",
                column: "HealthCareSpecialtyId");

            migrationBuilder.CreateIndex(
                name: "IX_HealthCareSpecialtyServices_HealthCareSpecialtyId",
                table: "HealthCareSpecialtyServices",
                column: "HealthCareSpecialtyId");

            migrationBuilder.CreateIndex(
                name: "IX_HealthCareSpecialtyServices_ServiceId",
                table: "HealthCareSpecialtyServices",
                column: "ServiceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HealthCareProviderSpecialties");

            migrationBuilder.DropTable(
                name: "HealthCareSpecialtyServices");

            migrationBuilder.AddColumn<int>(
                name: "HealthCareSpecialtyId",
                table: "Services",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Services_HealthCareSpecialtyId",
                table: "Services",
                column: "HealthCareSpecialtyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Services_HealthCareSpecialties_HealthCareSpecialtyId",
                table: "Services",
                column: "HealthCareSpecialtyId",
                principalTable: "HealthCareSpecialties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

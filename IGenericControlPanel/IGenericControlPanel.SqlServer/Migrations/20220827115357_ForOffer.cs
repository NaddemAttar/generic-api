﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class ForOffer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OfferEntities");

            migrationBuilder.AddColumn<int>(
                name: "OfferId",
                table: "Services",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OfferId",
                table: "ProductSizesColors",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OfferId",
                table: "Products",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OfferFor",
                table: "Offers",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CategoryType",
                table: "MultiLevelCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OfferId",
                table: "MultiLevelCategories",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OfferId",
                table: "Courses",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OfferId",
                table: "Brand",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Services_OfferId",
                table: "Services",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductSizesColors_OfferId",
                table: "ProductSizesColors",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_OfferId",
                table: "Products",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_MultiLevelCategories_OfferId",
                table: "MultiLevelCategories",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_Courses_OfferId",
                table: "Courses",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_Brand_OfferId",
                table: "Brand",
                column: "OfferId");

            migrationBuilder.AddForeignKey(
                name: "FK_Brand_Offers_OfferId",
                table: "Brand",
                column: "OfferId",
                principalTable: "Offers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Courses_Offers_OfferId",
                table: "Courses",
                column: "OfferId",
                principalTable: "Offers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MultiLevelCategories_Offers_OfferId",
                table: "MultiLevelCategories",
                column: "OfferId",
                principalTable: "Offers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Offers_OfferId",
                table: "Products",
                column: "OfferId",
                principalTable: "Offers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductSizesColors_Offers_OfferId",
                table: "ProductSizesColors",
                column: "OfferId",
                principalTable: "Offers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Services_Offers_OfferId",
                table: "Services",
                column: "OfferId",
                principalTable: "Offers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Brand_Offers_OfferId",
                table: "Brand");

            migrationBuilder.DropForeignKey(
                name: "FK_Courses_Offers_OfferId",
                table: "Courses");

            migrationBuilder.DropForeignKey(
                name: "FK_MultiLevelCategories_Offers_OfferId",
                table: "MultiLevelCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_Offers_OfferId",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductSizesColors_Offers_OfferId",
                table: "ProductSizesColors");

            migrationBuilder.DropForeignKey(
                name: "FK_Services_Offers_OfferId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Services_OfferId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_ProductSizesColors_OfferId",
                table: "ProductSizesColors");

            migrationBuilder.DropIndex(
                name: "IX_Products_OfferId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_MultiLevelCategories_OfferId",
                table: "MultiLevelCategories");

            migrationBuilder.DropIndex(
                name: "IX_Courses_OfferId",
                table: "Courses");

            migrationBuilder.DropIndex(
                name: "IX_Brand_OfferId",
                table: "Brand");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "ProductSizesColors");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "OfferFor",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "CategoryType",
                table: "MultiLevelCategories");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "MultiLevelCategories");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "Courses");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "Brand");

            migrationBuilder.CreateTable(
                name: "OfferEntities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    BrandId = table.Column<int>(type: "int", nullable: true),
                    CourseId = table.Column<int>(type: "int", nullable: true),
                    MultiLevelCategoryId = table.Column<int>(type: "int", nullable: true),
                    OfferId = table.Column<int>(type: "int", nullable: true),
                    ProductId = table.Column<int>(type: "int", nullable: true),
                    ProductSizeColorId = table.Column<int>(type: "int", nullable: true),
                    ServiceId = table.Column<int>(type: "int", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    IsValid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LastUpdateDate = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: false),
                    LastUpdatedBy = table.Column<int>(type: "int", nullable: false),
                    WithCourses = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    WithProducts = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    WithServices = table.Column<bool>(type: "tinyint(1)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfferEntities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OfferEntities_Brand_BrandId",
                        column: x => x.BrandId,
                        principalTable: "Brand",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OfferEntities_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OfferEntities_MultiLevelCategories_MultiLevelCategoryId",
                        column: x => x.MultiLevelCategoryId,
                        principalTable: "MultiLevelCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OfferEntities_Offers_OfferId",
                        column: x => x.OfferId,
                        principalTable: "Offers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OfferEntities_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OfferEntities_ProductSizesColors_ProductSizeColorId",
                        column: x => x.ProductSizeColorId,
                        principalTable: "ProductSizesColors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OfferEntities_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_OfferEntities_BrandId",
                table: "OfferEntities",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferEntities_CourseId",
                table: "OfferEntities",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferEntities_MultiLevelCategoryId",
                table: "OfferEntities",
                column: "MultiLevelCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferEntities_OfferId",
                table: "OfferEntities",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferEntities_ProductId",
                table: "OfferEntities",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferEntities_ProductSizeColorId",
                table: "OfferEntities",
                column: "ProductSizeColorId");

            migrationBuilder.CreateIndex(
                name: "IX_OfferEntities_ServiceId",
                table: "OfferEntities",
                column: "ServiceId");
        }
    }
}

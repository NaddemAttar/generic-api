﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class add_service_price : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "price",
                table: "Courses",
                newName: "Price");

            migrationBuilder.AddColumn<double>(
                name: "Price",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "WithCourses",
                table: "OfferEntities",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "WithProducts",
                table: "OfferEntities",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "WithServices",
                table: "OfferEntities",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Price",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "WithCourses",
                table: "OfferEntities");

            migrationBuilder.DropColumn(
                name: "WithProducts",
                table: "OfferEntities");

            migrationBuilder.DropColumn(
                name: "WithServices",
                table: "OfferEntities");

            migrationBuilder.RenameColumn(
                name: "Price",
                table: "Courses",
                newName: "price");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class change_discrption_Description : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Descrption",
                table: "Offers");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Offers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Offers");

            migrationBuilder.AddColumn<string>(
                name: "Descrption",
                table: "Offers",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IGenericControlPanel.SqlServer.Migrations
{
    public partial class remove_arachnoit_category_subcategory_add_productSizeColor_Relations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArachnoitCategoryId",
                table: "Question");

            migrationBuilder.DropColumn(
                name: "ArachnoitSubCategoryId",
                table: "Question");

            migrationBuilder.AddColumn<int>(
                name: "ProductSizeColorId",
                table: "OfferEntities",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_OfferEntities_ProductSizeColorId",
                table: "OfferEntities",
                column: "ProductSizeColorId");

            migrationBuilder.AddForeignKey(
                name: "FK_OfferEntities_ProductSizesColors_ProductSizeColorId",
                table: "OfferEntities",
                column: "ProductSizeColorId",
                principalTable: "ProductSizesColors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OfferEntities_ProductSizesColors_ProductSizeColorId",
                table: "OfferEntities");

            migrationBuilder.DropIndex(
                name: "IX_OfferEntities_ProductSizeColorId",
                table: "OfferEntities");

            migrationBuilder.DropColumn(
                name: "ProductSizeColorId",
                table: "OfferEntities");

            migrationBuilder.AddColumn<Guid>(
                name: "ArachnoitCategoryId",
                table: "Question",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ArachnoitSubCategoryId",
                table: "Question",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }
    }
}

﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Model.UserExperiences;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.UserExperiences.Dto.Experience;
using IGenericControlPanel.UserExperiences.IData.Interfaces;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers;
[Route("api/[controller]")]
[ApiController]
public class ExperienceManagementController : LocalizedBaseController<ExperienceManagementController>
{
    #region Constructor & Properties
    private readonly IExperienceRepository _experienceRepository;
    private readonly IHttpFileService _httpFileService;
    private readonly IMapper _mapper;
    public ExperienceManagementController(
        IStringLocalizer<ExperienceManagementController> localizer,
        IExperienceRepository experienceRepository,
        IHttpFileService httpFileService,
        IMapper mapper,
        IConfiguration configuration) : base(localizer,configuration)
    {
        _experienceRepository = experienceRepository;
        _httpFileService = httpFileService;
        _mapper = mapper;
    }
    #endregion

    #region Get

    [HttpGet]
    [Route("[action]")]
    public async Task<IActionResult> GetExperiencesForUser()
    {
        var result = await _experienceRepository.GetMultipleAsync();

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }

    [HttpGet]
    [Route("[action]")]
    public async Task<IActionResult> GetOneExperience(int id)
    {
        var result = await _experienceRepository.GetDetailsAsync(id);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion

    #region Remove
    [HttpDelete]
    [Route("[action]")]
    public async Task<IActionResult> RemoveExperience(int id)
    {
        var result = await _experienceRepository.RemoveAsync(id);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion

    #region Action
    [HttpPost]
    [Route("[action]")]
    public async Task<IActionResult> AddExperience([FromForm] ExperienceAddDto addDto)
    {
        List<FileDetailsDto> fileDtos = new();
        if (addDto.ExperienceFiles.Any())
        {
            fileDtos = await FilesExtensionMethods.SaveFiles(new Util.FileOptions
            {
                FileContentType = FileContentType.Image,
                FileSourceType = FileSourceType.Experience,
                FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                Files = addDto.ExperienceFiles,
                HttpFileService = _httpFileService,
                Mapper = _mapper,
                SourceId = addDto.Id,
                ControllerName = nameof(Experience)
            });

            if (fileDtos is null)
            {
                throw new BadRequestException(FromResx["FilesError"].Value);
            }
        }

        var result = await _experienceRepository.AddAsync(addDto, fileDtos);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }

    [HttpPut]
    [Route("[action]")]
    public async Task<IActionResult> UpdateExperience([FromForm] ExperienceUpdateDto updateDto)
    {
        List<FileDetailsDto> fileDtos = new();
        if (updateDto.ExperienceFiles.Any())
        {
            fileDtos = await FilesExtensionMethods.SaveFiles(new Util.FileOptions
            {
                FileContentType = FileContentType.Image,
                FileSourceType = FileSourceType.Experience,
                FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                Files = updateDto.ExperienceFiles,
                HttpFileService = _httpFileService,
                Mapper = _mapper,
                SourceId = updateDto.Id,
                ControllerName = nameof(Experience)
            });

            if (fileDtos is null)
            {
                throw new BadRequestException(FromResx["FilesError"].Value);
            }
        }

        var result = await _experienceRepository.UpdateAsync(
            updateDto, fileDtos, updateDto.RemoveFileIds);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion
}

﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers;
[Route("api/[controller]")]
[ApiController]
public class ApplyingCouponsSystemController :
    LocalizedBaseController<ApplyingCouponsSystemController>
{
    #region Constructor & Properties
    private readonly IApplyingCouponsSystemRepository _applyingCouponsSystemRepository;
    public ApplyingCouponsSystemController(
        IApplyingCouponsSystemRepository applyingCouponsSystemRepository,
        IStringLocalizer<ApplyingCouponsSystemController> localizer) : base(localizer)
    {
        _applyingCouponsSystemRepository = applyingCouponsSystemRepository;
    }
    #endregion

    #region Get Available Coupons
    [HttpGet]
    [AllowAnonymous]
    [Route("[action]")]
    public async Task<IActionResult> GetAvailableCoupons()
    {
        var result = await _applyingCouponsSystemRepository.GetAvailableCouponsAsync();

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion

    #region Coupon Booking

    [HttpPut]
    [Route("[action]")]
    public async Task<IActionResult> CouponBooking(int couponId)
    {
        var result = await _applyingCouponsSystemRepository.CouponBookingAsync(couponId);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion

    #region Activate & DeActivate the Coupon Booking

    [HttpPut]
    [Route("[action]")]
    public async Task<IActionResult> ActivateCouponBooking(string code)
    {
        var result = await _applyingCouponsSystemRepository.ActivateCouponBookingAsync(code);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }

    [HttpPut]
    [Route("[action]")]
    public async Task<IActionResult> DeActivateCouponBooking()
    {
        var result = await _applyingCouponsSystemRepository.DeActivateCouponBookingAsync();

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion

    #region Get User Coupons
    [HttpGet]
    [Route("[action]")]
    public async Task<IActionResult> GetUserCoupons(bool isBeneficiary, bool isActivate)
    {
        var result = await _applyingCouponsSystemRepository.GetUserCouponsAsync(isBeneficiary, isActivate);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion

    #region Api To Deduct the Points from user after using Coupon

    [HttpPut]
    [Route("[action]")]
    public async Task<IActionResult> PointsDeductionFromUser(int userCouponId)
    {
        var result = await _applyingCouponsSystemRepository.PointsDeductionFromUserAsync(userCouponId);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion
}

﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.UserExperiences.Dto.License;
using IGenericControlPanel.UserExperiences.IData.Interfaces;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System.ComponentModel;

namespace IGenericControlPanel.WebApplication.Controllers;
[Route("api/[controller]")]
[ApiController]
public class LicenseManagementController : LocalizedBaseController<LicenseManagementController>
{
    #region CTOR
    private readonly ILicenseRepository _licenseRepository;
    private readonly IHttpFileService _httpFileService;
    private readonly IMapper _mapper;
    public LicenseManagementController(
        ILicenseRepository licenseRepository,
        IStringLocalizer<LicenseManagementController> localizer,
        IHttpFileService httpFileService,
        IMapper mapper,
        IConfiguration configuration) : base(localizer, configuration)
    {
        _licenseRepository = licenseRepository;
        _httpFileService = httpFileService;
        _mapper = mapper;
    }
    #endregion

    #region GET
    [HttpGet]
    [Route("[action]")]
    public async Task<IActionResult> GetLicensesForUser()
    {
        var result = await _licenseRepository.GetMultipleAsync();

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }

    [HttpGet]
    [Route("[action]")]
    public async Task<IActionResult> GetOneLicense(int id)
    {
        var result = await _licenseRepository.GetDetailsAsync(id);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion

    #region REMOVE
    [HttpDelete]
    [Route("[action]")]
    public async Task<IActionResult> RemoveLicense(int id)
    {
        var result = await _licenseRepository.RemoveAsync(id);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion

    #region ACTION
    [HttpPost]
    [Route("[action]")]
    public async Task<IActionResult> AddLicense([FromForm] LicenseAddDto addDto)
    {
        List<FileDetailsDto> fileDtos = new();
        if (addDto.LicenseFiles != null && addDto.LicenseFiles.Any())
        {
            fileDtos = await FilesExtensionMethods.SaveFiles(new Util.FileOptions
            {
                FileContentType = FileContentType.Image,
                FileSourceType = FileSourceType.License,
                FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                Files = addDto.LicenseFiles,
                HttpFileService = _httpFileService,
                Mapper = _mapper,
                SourceId = addDto.Id,
                ControllerName = nameof(License)
            });

            if (fileDtos is null)
            {
                throw new BadRequestException(FromResx["FilesError"].Value);
            }
        }

        var result = await _licenseRepository.AddAsync(addDto, fileDtos);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }

    [HttpPut]
    [Route("[action]")]
    public async Task<IActionResult> UpdateLicense([FromForm] LicenseUpdateDto updateDto)
    {
        List<FileDetailsDto> fileDtos = new();
        if (updateDto.LicenseFiles != null && updateDto.LicenseFiles.Any())
        {
            fileDtos = await FilesExtensionMethods.SaveFiles(new Util.FileOptions
            {
                FileContentType = FileContentType.Image,
                FileSourceType = FileSourceType.License,
                FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                Files = updateDto.LicenseFiles,
                HttpFileService = _httpFileService,
                Mapper = _mapper,
                SourceId = updateDto.Id,
                ControllerName = nameof(License)
            });

            if (fileDtos is null)
            {
                throw new BadRequestException(FromResx["FilesError"].Value);
            }
        }

        var result = await _licenseRepository.UpdateAsync(
            updateDto, fileDtos, updateDto.RemoveFileIds);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion
}

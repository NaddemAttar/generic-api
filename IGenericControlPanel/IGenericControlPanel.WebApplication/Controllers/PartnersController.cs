﻿
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.HR.Dto.Partners;
using IGenericControlPanel.HR.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Util;
using IGenericControlPanel.WebApplication.ViewModels.Partners;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class PartnersController : LocalizedBaseController<PartnersController>
    {
        private IPartnersRepository PartnerRepository { get; set; }
        private IHttpFileService HttpFileService { get; }
        public PartnersController(
            IPartnersRepository _partnersRepository,
            IHttpFileService httpFileService,
            IConfiguration configuration,
            IStringLocalizer<PartnersController> localizer) : base(localizer, configuration)
        {
            PartnerRepository = _partnersRepository;
            HttpFileService = httpFileService;

        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetPartners(int pageNumber = 0, int pageSize = 6)
        {
            var result = PartnerRepository.GetPartners(pageSize, pageNumber);

            return result.EnumResult == GenericOperationResult.Success
                  ? Ok(result)
                  : (IActionResult)(result.EnumResult switch
                  {
                      GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                      GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                      GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                      _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                  });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetPartner(int PartnerId)
        {
            var result = PartnerRepository.GetPartner(PartnerId);

            return result.EnumResult == GenericOperationResult.Success
                 ? Ok(result)
                 : (IActionResult)(result.EnumResult switch
                 {
                     GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                     GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                     GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                     _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                 });
        }

        [HttpPost]
        // //[DynamicAuthorization("Partners", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionPartner([FromForm] PartnersFormDto formDto)
        {
            if (formDto.Image != null)
            {

                var fileDto = await FilesExtensionMethods.SavingFileInWWWRootFolder(formDto.Image,
                        Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                        HttpFileService);

                if (fileDto.IsSuccess)
                {
                    formDto.ImageUrl = fileDto.Result.Url;
                }
                else
                {
                    return BadRequest("couldn't save the image try again later");
                }
            }

            var result = PartnerRepository.ActionPartner(formDto);

            return result.EnumResult == GenericOperationResult.Success
                 ? Ok(result)
                 : (IActionResult)(result.EnumResult switch
                 {
                     GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                     GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                     GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                     _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                 });
        }

        [HttpDelete]
        //[DynamicAuthorization("Partners", nameof(ActionType.Remove))]
        [Route("[action]")]
        public IActionResult RemovePartner(int PartnerId)
        {
            var result = PartnerRepository.RemovePartner(PartnerId);

            return result.EnumResult == GenericOperationResult.Success
                 ? Ok(result)
                 : (IActionResult)(result.EnumResult switch
                 {
                     GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                     GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                     GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                     _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                 });
        }
    }
}

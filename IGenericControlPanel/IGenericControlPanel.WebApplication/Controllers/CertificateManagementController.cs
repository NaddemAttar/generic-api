﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Model.UserExperiences;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.UserExperiences.Dto.Certificate;
using IGenericControlPanel.UserExperiences.IData.Interfaces;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers;
[Route("api/[controller]")]
[ApiController]
public class CertificateManagementController : LocalizedBaseController<CertificateManagementController>
{
    private readonly ICertificateRepository _certificateRepository;
    private readonly IHttpFileService _httpFileService;
    private readonly IMapper _mapper;

    public CertificateManagementController(
        ICertificateRepository certificateRepository,
        IStringLocalizer<CertificateManagementController> localizer,
        IConfiguration configuration,
        IHttpFileService httpFileService,
        IMapper mapper) : base(localizer, configuration)
    {
        _certificateRepository = certificateRepository;
        _httpFileService = httpFileService;
        _mapper = mapper;
    }

    [HttpGet]
    [Route("[action]")]
    public async Task<IActionResult> GetCertificatesForUser()
    {
        var result = await _certificateRepository.GetMultipleAsync();

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }

    [HttpGet]
    [Route("[action]")]
    public async Task<IActionResult> GetCertificate(int id)
    {
        var result = await _certificateRepository.GetDetailsAsync(id);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }

    [HttpDelete]
    [Route("[action]")]
    public async Task<IActionResult> RemoveCertificate(int id)
    {
        var result = await _certificateRepository.RemoveAsync(id);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }

    [HttpPost]
    [Route("[action]")]
    public async Task<IActionResult> AddCertificate([FromForm] CertificateAddDto addDto)
    {
        List<FileDetailsDto> fileDtos = new();
        if (addDto.CertificateFiles != null && addDto.CertificateFiles.Any())
        {
            fileDtos = await FilesExtensionMethods.SaveFiles(new Util.FileOptions
            {
                FileContentType = FileContentType.Image,
                FileSourceType = FileSourceType.Certificate,
                FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                Files = addDto.CertificateFiles,
                HttpFileService = _httpFileService,
                Mapper = _mapper,
                SourceId = addDto.Id,
                ControllerName = nameof(Certificate)
            });

            if (fileDtos is null)
            {
                throw new BadRequestException(FromResx["FilesError"].Value);
            }
        }

        var result = await _certificateRepository.AddAsync(addDto, fileDtos);
        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }

    [HttpPut]
    [Route("[action]")]
    public async Task<IActionResult> UpdateCertificate([FromForm] CertificateUpdateDto updateDto)
    {
        List<FileDetailsDto> fileDtos = new();
        if (updateDto.CertificateFiles != null && updateDto.CertificateFiles.Any())
        {
            fileDtos = await FilesExtensionMethods.SaveFiles(new Util.FileOptions
            {
                FileContentType = FileContentType.Image,
                FileSourceType = FileSourceType.Certificate,
                FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                Files = updateDto.CertificateFiles,
                HttpFileService = _httpFileService,
                Mapper = _mapper,
                SourceId = updateDto.Id,
                ControllerName = nameof(Certificate)
            });

            if (fileDtos is null)
            {
                throw new BadRequestException(FromResx["FilesError"].Value);
            }
        }

        var result = await _certificateRepository.UpdateAsync(
            updateDto, fileDtos, updateDto.RemoveFileIds);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }

}

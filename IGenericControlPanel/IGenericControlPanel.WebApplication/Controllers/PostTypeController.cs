﻿
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.PostType;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class PostTypeController : LocalizedBaseController<PostTypeController>
    {
        private readonly IPostTypeRepository _IPostTypeRepository;

        public PostTypeController(IPostTypeRepository IPostTypeRepository)
        {
            _IPostTypeRepository = IPostTypeRepository;
        }
        #region GET
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetPostTypes(bool WithBlogs, bool WithQustions, int pageNumber = 0, int pageSize = 5)
        {
            var result = _IPostTypeRepository.GetPostTypes(WithBlogs, WithQustions, pageNumber, pageSize);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetAllPostType()
        {
            var result = _IPostTypeRepository.GetAllPostType();

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetPostType(int Id)
        {
            var result = _IPostTypeRepository.GetPostType(Id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion
        #region Action
        [HttpPost]
        //[DynamicAuthorization("PostType", nameof(ActionType.Action))]
        [Route("[action]")]
        public IActionResult PostTypeAction(PostTypeDto formDto)
        {
            var result = _IPostTypeRepository.ActionPostType(formDto);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion
        #region Remove
        [HttpDelete]
        //[DynamicAuthorization("PostType", nameof(ActionType.Remove))]
        [Route("[action]")]
        public IActionResult RemovePostType(int Id)
        {
            var result = _IPostTypeRepository.RemovePostType(Id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });

        }
        #endregion
    }
}

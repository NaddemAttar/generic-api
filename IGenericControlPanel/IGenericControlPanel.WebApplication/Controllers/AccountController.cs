﻿using AutoMapper;
using CraftLab.Core.Database.Identity.Interfaces;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.Security.Dto;
using IGenericControlPanel.Security.Dto.User;
using IGenericControlPanel.Security.IData;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Enums.Security;
using IGenericControlPanel.SharedKernal.Logger.Classes;
using IGenericControlPanel.SharedKernal.Logger.Enums;
using IGenericControlPanel.SharedKernal.Logger.Interface;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Util;
using IGenericControlPanel.WebApplication.ViewModels.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Mail;
using System.Security.Claims;
using FileOptions = IGenericControlPanel.WebApplication.Util.FileOptions;
using HttpPostAttribute = Microsoft.AspNetCore.Mvc.HttpPostAttribute;

namespace IGenericControlPanel.WebApplication.Controllers
{
    // Adding a commit  
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : LocalizedBaseController<AccountController>
    {
        #region Properties
        private IAccountRepository AccountRepository { get; }
        private IPermissionRepository PermissionRepository { get; }
        private readonly UserManager<CPUser> userManager;
        private readonly UserManager<CPUser> _userManagerGoogle;
        private readonly IUserService userService;
        private readonly IStringLocalizer<AccountResource> accountLocalizer;
        private readonly IStringLocalizer<SharedResource> sharedLocalizer;
        private readonly ITokenService tokenService;
        private readonly IHttpFileService _httpFileService;
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;
        private IHttpContextAccessor HttpContextAccessor { get; }
        private readonly SignInManager<CPUser> _signInManager;
        private readonly RoleManager<CPRole> _roleManager;
        private const string GoogleUserInfoUrl = "https://www.googleapis.com/oauth2/v2/userinfo";
        //protected IConfiguration Configuration { get; }
        //protected IStringLocalizer<AccountController> FromResx { get; }
        //protected string RequestCultureCode
        //{
        //    get
        //    {
        //        var requestCutlureFeature = Request.HttpContext.Features.Get<IRequestCultureFeature>();
        //        return requestCutlureFeature.RequestCulture.UICulture.Name;
        //    }
        //}
        private ILogService LogService { get; }

        #endregion

        #region Constructors
        public AccountController(IAccountRepository accountRepository,
                                      IPermissionRepository permissionRepository,
                                      IUserLanguageRepository userLanguageRepository,
                                      SignInManager<CPUser> signInManager,
                                      UserManager<CPUser> userManagerGoogle,
                                      ISettingRepository settingRepository,
                                      IStringLocalizer<AccountController> stringLocalizer, // not used.
                                      IStringLocalizer<AccountResource> accountLocalizer,
                                      IStringLocalizer<SharedResource> sharedLocalizer,
                                      IConfiguration configuration,
                                      UserManager<CPUser> _userManager,
                                      IUserService UserService,
                                      IHttpContextAccessor httpContextAccessor,
                                      ILogService logService,
                                      ITokenService _tokenService,
                                      RoleManager<CPRole> roleManager,
                                      IHttpFileService httpFileService,
                                      IMapper mapper,
                                      IUserRepository userRepository)
            : base(configuration, userLanguageRepository, settingRepository, stringLocalizer)
        {
            this.accountLocalizer = accountLocalizer;
            this.sharedLocalizer = sharedLocalizer;
            _signInManager = signInManager;
            _userManagerGoogle = userManagerGoogle;
            AccountRepository = accountRepository;
            userManager = _userManager;
            userService = UserService;
            HttpContextAccessor = httpContextAccessor;
            LogService = logService;
            _roleManager = roleManager;
            PermissionRepository = permissionRepository;
            tokenService = _tokenService;
            _httpFileService = httpFileService;
            _mapper = mapper;
            _userRepository = userRepository;
            //Configuration = configuration;
            //FromResx = stringLocalizer;
        }

        #endregion

        #region Methods

        #region Routes

        #region Get
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetHeathCareProviders()
        {
            var result = AccountRepository.GetHeathCareProviders();

            return result.EnumResult == GenericOperationResult.Success ? Ok(result)
                 : (IActionResult)(result.EnumResult switch
                 {
                     GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                     GenericOperationResult.ValidationError => throw new BadRequestException(accountLocalizer[result.ErrorMessages].Value),
                     GenericOperationResult.NotFound => throw new NotFoundException(accountLocalizer[result.ErrorMessages].Value),
                     _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                 });
        }

        [HttpGet]
        // [Authorize(AuthenticationSchemes = "Bearer")]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetUsers(int? usresType)
        {
            var result = await AccountRepository.GetAllUsers(usresType);
            return result.EnumResult == GenericOperationResult.Success ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(accountLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(accountLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public async Task<IActionResult> GetProfileInfo(int UserId)
        {
            var result = await AccountRepository.GetprofileInfo(UserId);
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(accountLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(accountLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }

        #region Get Normal Users

        #region Blades Users
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetEcommerceUsers(int courseId, string qurey, string instructorIds,
           int locationId, string courseTypeIds, DateTime? startDate = null, DateTime? endDate = null,
           int? pageNumber = 0, int? pageSize = 4)
        {
            List<int> listIntegerForCourseTypeIds = new();
            List<int> listIntegerForInstructorIds = new();
            if (instructorIds != null)
            {
                listIntegerForInstructorIds = instructorIds.ConvertJsonStringToObjectModal<int>();
            }
            if (courseTypeIds != null)
            {
                listIntegerForCourseTypeIds = courseTypeIds.ConvertJsonStringToObjectModal<int>();
            }

            var result = AccountRepository.GetNormalUsers(
                courseId, qurey, listIntegerForInstructorIds, locationId, listIntegerForCourseTypeIds,
                startDate, endDate, pageNumber, pageSize);

            return result.EnumResult == GenericOperationResult.Success ? Ok(result)
                 : (IActionResult)(result.EnumResult switch
                 {
                     GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                     GenericOperationResult.ValidationError => throw new BadRequestException(accountLocalizer[result.ErrorMessages].Value),
                     GenericOperationResult.NotFound => throw new NotFoundException(accountLocalizer[result.ErrorMessages].Value),
                     _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                 });
        }
        #endregion

        #region Dr Nedal Users
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetHealthProfileUsers(string query, int? pageNumber = 0, int? pageSize = 4)
        {
            var result = AccountRepository.GetNormalUsers(query, pageNumber, pageSize);
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                });
        }
        #endregion

        #endregion

        #endregion



        #region Login / Logout
        [HttpPost]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> LoginWithFacebook(string accessToken)
        {
            var validationResult = await AccountRepository.ValidateFacebookAccessToken(accessToken);
            if (!validationResult.IsSuccess)
                throw new BadRequestException(accountLocalizer[ErrorMessages.InvalidFacebookToken].Value);

            var facebookUserInfo = (await AccountRepository.GetFacebookUserInfo(accessToken)).Result;
            CPUser ExistingUser = await userManager.FindByEmailAsync(facebookUserInfo.Email);
            if (ExistingUser == null)
            {
                var action = await AccountRepository.ActionNormalUser(new UserFormDto()
                {
                    Email = facebookUserInfo.Email,
                    FirstName = facebookUserInfo.FirstName,
                    LastName = facebookUserInfo.LastName,
                    UserName = facebookUserInfo.Email
                });

                if (action.EnumResult == GenericOperationResult.ValidationError)
                    throw new BadRequestException(accountLocalizer[action.ErrorMessages].Value);

                if (action.EnumResult == GenericOperationResult.Failed)
                    throw new InternalServerException(sharedLocalizer[action.ErrorMessages].Value);
            }
            var LoginResult = await AccountRepository.ExternalLogin(new UserDto()
            {
                Email = facebookUserInfo.Email,
                RememberMe = true
            });
            if (!LoginResult.IsSuccess)
            {
                //ModelState.AddModelError(string.Empty, "Wrong username or password");
                //return BadRequest();
                throw new BadRequestException(accountLocalizer[LoginResult.ErrorMessages].Value);
            }
            CPUser user = await userManager.FindByEmailAsync(facebookUserInfo.Email);
            IList<string> userRoles = await userManager.GetRolesAsync(user);

            List<Claim> authClaims = new()
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            foreach (string userRole in userRoles)
            {
                authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                authClaims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
            }

            //SymmetricSecurityKey authSigningKey = new(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"]));

            //JwtSecurityToken token = new(
            //    issuer: Configuration["JWT:ValidIssuer"],
            //    audience: Configuration["JWT:ValidAudience"],
            //    expires: DateTime.Now.AddDays(30),
            //    claims: authClaims,
            //    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
            //    );
            var token = tokenService.GenerateAccessToken(authClaims);
            var refreshToken = tokenService.GenerateRefreshToken();
            var refreshTokenResult = AccountRepository.AddRefreshTokenToUser(LoginResult.Result.Id, refreshToken);
            if (refreshTokenResult.EnumResult == GenericOperationResult.NotFound)
            {
                throw new NotFoundException(accountLocalizer[ErrorMessages.UserNotExist].Value);
            }
            if (refreshTokenResult.EnumResult == GenericOperationResult.Failed)
            {
                throw new NotFoundException(sharedLocalizer[refreshTokenResult.ErrorMessages].Value);
            }
            //if (refreshokenResult.EnumResult == GenericOperationResult.ValidationError)
            //{
            //    throw new NotFoundException(FromResx[refreshokenResult.ErrorMessages].Value);
            //}

            CPRole Role = await _roleManager.FindByNameAsync(userRoles[0]);
            Security.Dto.Permissions.RoleDto userPermissions = PermissionRepository.GetWebContentsForRole(Role.Id);
            return Json(new
            {
                token,
                refreshToken,
                TokenExpirationDate = DateTime.UtcNow.AddDays(3),
                LoginResult.Result.UserName,
                LoginResult.Result.FullName,
                LoginResult.Result.Email,
                LoginResult.Result.FirstName,
                LoginResult.Result.LastName,
                LoginResult.Result.Id,
                LoginResult.Result.ProfilePhotoUrl,
                LoginResult.Result.Birthdate,
                LoginResult.Result.Gender,
                LoginResult.Result.PhoneNumber,
                Permissions = userPermissions,

            });
        }
        [HttpPost, Authorize]
        [Route("revoke")]
        public async Task<IActionResult> RevokeAsync()
        {
            var username = User.Identity.Name;
            var user = await userManager.FindByNameAsync(username);
            if (user == null)
                throw new BadRequestException(accountLocalizer[ErrorMessages.UserNotExist].Value);

            var refreshokenResult = AccountRepository.AddRefreshTokenToUser(user.Id, null);
            return NoContent();
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("refresh")]
        public async Task<IActionResult> Refresh(string refreshToken, string accessToken)
        {
            if (refreshToken is null)
                throw new BadRequestException(accountLocalizer[ErrorMessages.InvalidRefreshToken].Value);

            var principal = tokenService.GetPrincipalFromExpiredToken(accessToken);
            var username = principal.Identity.Name; //this is mapped to the Name claim by default
            var user = await userManager.FindByNameAsync(username);
            if (user is null || user.RefreshToken != refreshToken || user.RefreshTokenExpiryTime <= DateTime.UtcNow)
                throw new BadRequestException(accountLocalizer[ErrorMessages.InvalidRefreshToken].Value);

            var newAccessToken = tokenService.GenerateAccessToken(principal.Claims);
            var newRefreshToken = tokenService.GenerateRefreshToken();
            var refreshokenResult = AccountRepository.AddRefreshTokenToUser(user.Id, newRefreshToken);
            return refreshokenResult.EnumResult == GenericOperationResult.Success
                ? Json(new
                {
                    Token = newAccessToken,
                    RefreshToken = newRefreshToken,
                    TokenExpirationDate = DateTime.UtcNow.AddDays(3),
                })
                : (IActionResult)(refreshokenResult.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[refreshokenResult.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(accountLocalizer[refreshokenResult.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(accountLocalizer[refreshokenResult.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[refreshokenResult.ErrorMessages].Value)
                });
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public IActionResult checkLogin()
        {
            return Json("");
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> LoginWithGoogle(string accessToken)
        {
            var userinfo = await AccountRepository.GetGoogleUserInfo(accessToken);
            if (!userinfo.IsSuccess)
            {
                return userinfo.EnumResult switch
                {
                    GenericOperationResult.ValidationError => throw new BadRequestException(accountLocalizer[ErrorMessages.InvalidGoogleToken].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(accountLocalizer[ErrorMessages.UserNotExist].Value),
                    GenericOperationResult.Failed => throw new InternalServerException(accountLocalizer[ErrorMessages.InternalServerError].Value),
                };
            }

            CPUser ExistingUser = await userManager.FindByEmailAsync(userinfo.Result.email);
            if (ExistingUser == null)
            {
                var action = await AccountRepository.ActionNormalUser(new UserFormDto()
                {
                    Email = userinfo.Result.email,
                    FirstName = userinfo.Result.given_name,
                    LastName = userinfo.Result.family_name,
                    UserName = userinfo.Result.email,
                });
                if (action.EnumResult == GenericOperationResult.ValidationError)
                    throw new BadRequestException(accountLocalizer[action.ErrorMessages].Value);

                if (action.EnumResult == GenericOperationResult.Failed)
                    throw new InternalServerException(sharedLocalizer[action.ErrorMessages].Value);
            }
            OperationResult<GenericLoginResult, UserDto> LoginResult = await AccountRepository.ExternalLogin(new UserDto()
            {
                UserName = userinfo.Result.email,
                Email = userinfo.Result.email,
                RememberMe = true
            });
            if (!LoginResult.IsSuccess)
            {
                //ModelState.AddModelError(string.Empty, "Wrong username or password");
                //return BadRequest();
                throw new BadRequestException(accountLocalizer[LoginResult.ErrorMessages].Value);
            }
            CPUser user = await userManager.FindByEmailAsync(userinfo.Result.email);
            IList<string> userRoles = await userManager.GetRolesAsync(user);

            List<Claim> authClaims = new()
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            foreach (string userRole in userRoles)
            {
                authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                authClaims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
            }

            var token = tokenService.GenerateAccessToken(authClaims);
            var refreshToken = tokenService.GenerateRefreshToken();
            var refreshTokenResult = AccountRepository.AddRefreshTokenToUser(LoginResult.Result.Id, refreshToken);
            if (refreshTokenResult.EnumResult == GenericOperationResult.NotFound)
            {
                throw new NotFoundException(accountLocalizer[ErrorMessages.UserNotExist].Value);
            }
            if (refreshTokenResult.EnumResult == GenericOperationResult.Failed)
            {
                throw new NotFoundException(sharedLocalizer[refreshTokenResult.ErrorMessages].Value);
            }

            CPRole Role = await _roleManager.FindByNameAsync(userRoles[0]);
            Security.Dto.Permissions.RoleDto userPermissions = PermissionRepository.GetWebContentsForRole(Role.Id);
            return Json(new
            {
                token,
                refreshToken,
                TokenExpirationDate = DateTime.UtcNow.AddDays(3),
                LoginResult.Result.UserName,
                LoginResult.Result.FullName,
                LoginResult.Result.Email,
                FirstName = LoginResult.Result.FirstName,
                LastName = LoginResult.Result.LastName,
                LoginResult.Result.ProfilePhotoUrl,
                LoginResult.Result.Birthdate,
                LoginResult.Result.Gender,
                LoginResult.Result.PhoneNumber,
                LoginResult.Result.Id,
                Permissions = userPermissions
            });
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> NormalUserLogin(LoginViewModel loginViewModel)
        {
            try
            {
                CPUser user = loginViewModel.Username.Contains("@")
                    ? await userManager.FindByEmailAsync(loginViewModel.Username)
                    : await userManager.FindByNameAsync(loginViewModel.Username);

                if (user == null)
                    throw new NotFoundException(accountLocalizer[ErrorMessages.UserNotExist].Value);

                if (user != null && await userManager.CheckPasswordAsync(user, loginViewModel.Password))
                {
                    OperationResult<GenericLoginResult, UserDto> loginData = await AccountRepository.Login(new UserDto()
                    {
                        UserName = loginViewModel.Username.Contains("@") ? null : loginViewModel.Username,
                        Email = loginViewModel.Username.Contains("@") ? loginViewModel.Username : null,
                        Password = loginViewModel.Password,
                        RememberMe = loginViewModel.RememberMe
                    }).ConfigureAwait(false);
                    if (!loginData.IsSuccess)
                    {
                        //ModelState.AddModelError(string.Empty, "Wrong username or password");
                        //return BadRequest();
                        throw new NotFoundException(accountLocalizer[ErrorMessages.WrongPasswordOrUsername].Value);
                    }
                    IList<string> userRoles = await userManager.GetRolesAsync(user);

                    List<Claim> authClaims = new()
                    {
                        new Claim(ClaimTypes.Name, user.UserName),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    };

                    foreach (string userRole in userRoles)
                    {
                        if (userRole == nameof(SharedKernal.Enums.Role.Admin))
                        {
                            throw new NotFoundException(accountLocalizer[ErrorMessages.UserNotExist].Value);
                        }
                        authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                        authClaims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
                    }

                    var token = tokenService.GenerateAccessToken(authClaims);
                    var refreshToken = tokenService.GenerateRefreshToken();
                    var refreshTokenResult = AccountRepository.AddRefreshTokenToUser(loginData.Result.Id, refreshToken);
                    if (refreshTokenResult.EnumResult == GenericOperationResult.NotFound)
                    {
                        throw new NotFoundException(accountLocalizer[ErrorMessages.UserNotExist].Value);
                    }
                    if (refreshTokenResult.EnumResult == GenericOperationResult.Failed)
                    {
                        throw new NotFoundException(sharedLocalizer[refreshTokenResult.ErrorMessages].Value);
                    }

                    CPRole Role = await _roleManager.FindByNameAsync(userRoles[0]);
                    Security.Dto.Permissions.RoleDto userPermissions = PermissionRepository.GetWebContentsForRole(Role.Id);
                    return Json(new
                    {
                        token,
                        TokenExpirationDate = DateTime.UtcNow.AddDays(3),
                        refreshToken,
                        loginData.Result.UserName,
                        loginData.Result.FullName,
                        loginData.Result.Email,
                        loginData.Result.Id,
                        loginData.Result.Birthdate,
                        loginData.Result.Gender,
                        loginData.Result.ProfilePhotoUrl,
                        loginData.Result.FirstName,
                        loginData.Result.LastName,
                        loginData.Result.PhoneNumber,
                        //ArachnoitToken = ArachnoitAccessToken,
                        Permissions = userPermissions
                    });
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                LogService.Log(new HITLog()
                {
                    Exception = ex,
                    Message = ex.Message
                });
                return StatusCode(500, ex);

            }
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> CPUserLogin(LoginViewModel loginViewModel)
        {
            try
            {
                CPUser user = loginViewModel.Username.Contains("@")
                    ? await userManager.FindByEmailAsync(loginViewModel.Username)
                    : await userManager.FindByNameAsync(loginViewModel.Username);

                if (user == null)
                    throw new NotFoundException(accountLocalizer[ErrorMessages.UserNotExist].Value);

                if (user != null && await userManager.CheckPasswordAsync(user, loginViewModel.Password))
                {
                    OperationResult<GenericLoginResult, UserDto> loginData = await AccountRepository.Login(new UserDto()
                    {
                        UserName = loginViewModel.Username.Contains("@") ? null : loginViewModel.Username,
                        Email = loginViewModel.Username.Contains("@") ? loginViewModel.Username : null,
                        Password = loginViewModel.Password,
                        RememberMe = loginViewModel.RememberMe
                    }).ConfigureAwait(false);
                    if (!loginData.IsSuccess)
                    {
                        //ModelState.AddModelError(string.Empty, "Wrong username or password");
                        //return BadRequest();
                        throw new NotFoundException(accountLocalizer[ErrorMessages.WrongPasswordOrUsername].Value);
                    }
                    IList<string> userRoles = await userManager.GetRolesAsync(user);

                    List<Claim> authClaims = new()
                    {
                        new Claim(ClaimTypes.Name, user.UserName),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    };

                    foreach (string userRole in userRoles)
                    {
                        if (userRole == nameof(SharedKernal.Enums.Role.NormalUser))
                        {
                            throw new NotFoundException(accountLocalizer[ErrorMessages.UserNotExist].Value);
                        }
                        authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                        authClaims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
                    }

                    var token = tokenService.GenerateAccessToken(authClaims);
                    var refreshToken = tokenService.GenerateRefreshToken();
                    var refreshTokenResult = AccountRepository.AddRefreshTokenToUser(loginData.Result.Id, refreshToken);
                    if (refreshTokenResult.EnumResult == GenericOperationResult.NotFound)
                    {
                        throw new NotFoundException(accountLocalizer[ErrorMessages.UserNotExist].Value);
                    }
                    if (refreshTokenResult.EnumResult == GenericOperationResult.Failed)
                    {
                        throw new NotFoundException(sharedLocalizer[refreshTokenResult.ErrorMessages].Value);
                    }

                    CPRole Role = await _roleManager.FindByNameAsync(userRoles[0]);
                    Security.Dto.Permissions.RoleDto userPermissions = PermissionRepository.GetWebContentsForRole(Role.Id);
                    return Json(new
                    {
                        token,
                        refreshToken,
                        TokenExpirationDate=DateTime.UtcNow.AddDays(3),
                        Id = loginData.Result.Id,
                        loginData.Result.UserName,
                        loginData.Result.FullName,
                        loginData.Result.Email,
                        loginData.Result.Birthdate,
                        loginData.Result.Gender,
                        loginData.Result.ProfilePhotoUrl,
                        loginData.Result.FirstName,
                        loginData.Result.LastName,
                        //ArachnoitToken = ArachnoitAccessToken,
                        Permissions = userPermissions
                    });
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                LogService.Log(new HITLog()
                {
                    Exception = ex,
                    Message = ex.Message
                });
                return StatusCode(500, ex);

            }
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public async Task<IActionResult> Logout()
        {
            int id = userService.GetCurrentUserId<int>();
            await AccountRepository.Logout();
            return Ok(nameof(AccountController.Logout));
        }
        private LoginViewModel GetLoginData()
        {
            string pageTitle = "LoginTitle"; /*FromResx["LoginTitle"];*/
            LoginViewModel loginViewModel = new(pageTitle)
            { };
            return loginViewModel;
        }

        #endregion

        #region Reset Password Section
        [HttpPost]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> SendResetPasswordEmail(string userEmail)
        {
            try
            {
                MailMessage mailMessage = new()
                {
                    From = new MailAddress("hussammorjan00@gmail.com")
                };
                mailMessage.To.Add(new MailAddress(userEmail));

                mailMessage.Subject = "Password Reset";
                mailMessage.IsBodyHtml = true;
                CPUser user = await userManager.FindByEmailAsync(userEmail);
                if (user == null)
                    throw new NotFoundException(accountLocalizer[ErrorMessages.UserNotExist].Value);

                string token = await userManager.GeneratePasswordResetTokenAsync(user);
                mailMessage.Body = string.Format(Configuration.GetValue<string>("ResetPasswordLink"), token, userEmail);

                SmtpClient client = new()
                {
                    Credentials = new System.Net.NetworkCredential("hussammorjan00@gmail.com", "hussammorjan002"),
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true
                };

                client.Send(mailMessage);
                return Ok(new OperationResult<GenericOperationResult>(GenericOperationResult.Success));
            }
            catch (Exception ex)
            {
                // log exception
                LogService.Log(new HITLog()
                {
                    Exception = ex
                }, HITLogType.Error);
            }
            return BadRequest();
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> ResetPasswordConfirmation(string email, string token, string password)
        {
            CPUser user = await userManager.FindByEmailAsync(email);
            IdentityResult resetPassResult = await userManager.ResetPasswordAsync(user, token, password);
            if (!resetPassResult.Succeeded)
                return BadRequest(resetPassResult.Errors);

            return Ok(new OperationResult<GenericOperationResult>(GenericOperationResult.Success));
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> ChangePassword(string email, string oldPassword, string newPassword)
        {
            CPUser user = await userManager.FindByEmailAsync(email);
            if (user == null)
                throw new NotFoundException(accountLocalizer[ErrorMessages.UserNotExist].Value);

            IdentityResult changePassResult = await userManager.ChangePasswordAsync(user, oldPassword, newPassword);
            if (!changePassResult.Succeeded)
                return BadRequest(changePassResult.Errors);

            return Ok(new OperationResult<GenericOperationResult>(GenericOperationResult.Success));
        }
        #endregion

        #region Action User
        [HttpPost]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> ActionNormalUser([FromForm] UserFormDto userDto)
        {
            var oldPhotoProfile = "";
            if (userDto.ProfilePhoto != null)
            {
                if (userDto.Id != 0)
                {
                    var user = _userRepository.GetCurrentUserInformation();

                    if (user == null)
                        throw new NotFoundException(accountLocalizer[ErrorMessages.UserNotExist].Value);

                    oldPhotoProfile = user.ProfilePhotoUrl;
                }
                var saveImaegInServiceFolder = await FilesExtensionMethods.SaveFile(new FileOptions
                {
                    FileContentType = FileContentType.Image,
                    FileSourceType = FileSourceType.UserProfile,
                    FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                    File = userDto.ProfilePhoto,
                    HttpFileService = _httpFileService,
                    Mapper = _mapper,
                    SourceId = userDto.Id,
                    ControllerName = nameof(AccountController)
                });

                if (saveImaegInServiceFolder is null)
                    throw new BadRequestException(sharedLocalizer[ErrorMessages.FailedSavingPhoto].Value);

                userDto.ProfilePhotoUrl = saveImaegInServiceFolder.Url;
            }
            var result = await AccountRepository.ActionNormalUser(userDto);

            if (result.IsSuccess)
                await FilesExtensionMethods.RemoveFileFromWWWRoot(oldPhotoProfile, _httpFileService);

            if (!result.IsSuccess)
                await FilesExtensionMethods.RemoveFileFromWWWRoot(userDto.ProfilePhotoUrl, _httpFileService);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(accountLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(accountLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }
        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public async Task<IActionResult> ActionCPUser([FromForm] CPUserRegistrationDto userDto)
        {
            var oldPhotoProfile = "";
            if (userDto.ProfilePhoto != null)
            {
                if (userDto.Id != 0)
                {
                    var user = _userRepository.GetCurrentUserInformation();

                    if (user == null)
                        throw new NotFoundException(accountLocalizer[ErrorMessages.UserNotExist].Value);

                    oldPhotoProfile = user.ProfilePhotoUrl;
                }
                var saveImaegInServiceFolder = await FilesExtensionMethods.SaveFile(new FileOptions
                {
                    FileContentType = FileContentType.Image,
                    FileSourceType = FileSourceType.CpUserProfile,
                    FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                    File = userDto.ProfilePhoto,
                    HttpFileService = _httpFileService,
                    Mapper = _mapper,
                    SourceId = userDto.Id,
                    ControllerName = nameof(AccountController)
                });

                if (saveImaegInServiceFolder is null)
                    throw new BadRequestException(sharedLocalizer[ErrorMessages.FailedSavingPhoto].Value);

                userDto.ProfilePhotoUrl = saveImaegInServiceFolder.Url;
            }
            OperationResult<GenericOperationResult, UserFormDto> result = await AccountRepository.ActionCPUser(userDto);

            if (result.IsSuccess)
                await FilesExtensionMethods.RemoveFileFromWWWRoot(oldPhotoProfile, _httpFileService);

            if (!result.IsSuccess)
                await FilesExtensionMethods.RemoveFileFromWWWRoot(userDto.ProfilePhotoUrl, _httpFileService);

            /* HttpContext.Response.StatusCode = IGenericControlPanel.WebApplication.Util
                 .ExtensionMethods.GetStatusCodeType(result.EnumResult);*/
            return result.EnumResult == GenericOperationResult.Success
                 ? Ok(result)
                 : (IActionResult)(result.EnumResult switch
                 {
                     GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                     GenericOperationResult.ValidationError => throw new BadRequestException(accountLocalizer[result.ErrorMessages].Value),
                     GenericOperationResult.NotFound => throw new NotFoundException(accountLocalizer[result.ErrorMessages].Value),
                     _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                 });
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> AddCpUserToEnterprise(List<int> usersId, int enterpriseId)
        {
            var result = await AccountRepository.AddCpUserToEnterprise(usersId, enterpriseId);
            return result.EnumResult == GenericOperationResult.Success
            ? Ok(result)
            : (IActionResult)(result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(accountLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(accountLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
            });
        }
        #endregion

        #region Remove Account Soft & Hard
        [HttpDelete]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public async Task<IActionResult> SoftRemovedAccount(int accountId = 0)
        {
            if (accountId == 0)
                throw new NotFoundException(accountLocalizer[ErrorMessages.UserNotExist].Value);

            OperationResult<GenericOperationResult> result = await AccountRepository.RemoveNormalUser(accountId, RemoveAccountType.SoftRemove);
            return result is null ? GetModelStateErrorAjaxResult("Error Happend When Remove User") : (IActionResult)Json(result);
        }

        [HttpDelete]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public async Task<IActionResult> HardRemovedAccount(int accountId = 0)
        {
            var user = _userRepository.GetUser(accountId);

            if (user == null)
                throw new NotFoundException(accountLocalizer[ErrorMessages.UserNotExist].Value);

            if (!string.IsNullOrEmpty(user.ProfilePhotoUrl))
                await FilesExtensionMethods.RemoveFileFromWWWRoot(user.ProfilePhotoUrl, _httpFileService);

            OperationResult<GenericOperationResult> result = await AccountRepository.RemoveNormalUser(accountId, RemoveAccountType.HardRemove);
            if (!(result.EnumResult == GenericOperationResult.Success))
                throw new InternalServerException(sharedLocalizer[ErrorMessages.InternalServerError].Value);

            return Json(result);
        }
        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public async Task<IActionResult> ActionArchive(int userId, bool activate = false)
        {
            if (userId == 0)
                throw new NotFoundException(accountLocalizer[ErrorMessages.UserNotExist].Value);

            OperationResult<GenericOperationResult> result = await AccountRepository.ActionArchive(userId, activate);
            return result.IsSuccess ? Json(result)
                : throw new InternalServerException(sharedLocalizer[ErrorMessages.InternalServerError].Value);
        }
        #endregion

        #region Get Roles
        [HttpGet]
        [Authorize]
        [Route("[action]")]
        public IActionResult GetRoles()
        {
            OperationResult<GenericOperationResult, IEnumerable<RoleDto>> allRoles = AccountRepository.GetRoles();
            return Json(allRoles);
        }

        #endregion

        #endregion

        #endregion
    }
}
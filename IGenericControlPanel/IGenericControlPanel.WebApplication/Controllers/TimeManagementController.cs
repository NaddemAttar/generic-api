﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Core.Dto.OpeningHoures;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Util;
using IGenericControlPanel.WebApplication.ViewModels.TimeManagement;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class TimeManagementController : LocalizedBaseController<TimeManagementController>
    {
        #region Properties & Constructor
        private ITimeManagementRepository TimeManagementRepository { get; }
        private readonly IStringLocalizer<TimeManagementResource> timeLocalizer;
        private readonly IStringLocalizer<SharedResource> sharedLocalizer;
        private readonly IStringLocalizer<AccountResource> accountLocalizer;
        public TimeManagementController(
            ITimeManagementRepository _timeManagementRepository,
            IDistributedCache cache,
            IStringLocalizer<TimeManagementResource> timeLocalizer,
            IStringLocalizer<SharedResource> sharedLocalizer,
            IStringLocalizer<AccountResource> accountLocalizer) : base(cache)
        {
            TimeManagementRepository = _timeManagementRepository;
            this.timeLocalizer = timeLocalizer;
            this.sharedLocalizer = sharedLocalizer;
            this.accountLocalizer = accountLocalizer;
        }
        #endregion

        #region Get Opening Hours For Health Care Providers In WebSites 
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetOpeningHoures(int? userId, int? enterpriseId)
        {
            IEnumerable<OpeningHouresDto> data = null;
            OperationResult<GenericOperationResult, IEnumerable<OpeningHouresDto>> result = null;
            string recordKey = $"{CacheKeys.GetOpeningHours}_{userId}";

            data = await _cache.GetRecordAsync<IEnumerable<OpeningHouresDto>>(recordKey);

            if (data is null)
            {
                result = await TimeManagementRepository.GetOpeningHouresAsync(userId, enterpriseId);

                await _cache.SetRecordAsync(recordKey, result.Result, result.IEnumerableDataIsValid());
            }
            else
            {
                result = new();
                result.Result = data;
            }

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(timeLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(timeLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }
        #endregion

        #region Get Opening Hours For Health Care Providers In DashBoard 
        [HttpGet]
        //[DynamicAuthorization("TimeManagement", nameof(ActionType.Get))]
        [Route("[action]")]
        public async Task<IActionResult> GetOpeningHouresForHealthProvider(int? userId)
        {
            var result = await TimeManagementRepository.GetOpeningHouresForHealthProviderAsync(userId);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(timeLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(accountLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }
        #endregion

        #region Opening Hours Action
        [HttpPost]
        [Route("[action]")]
        //[DynamicAuthorization("TimeManagement", nameof(ActionType.Action))]
        public async Task<IActionResult> SetOpeningHours(string openingHouresString)
        {
            string currentUserId = User.Identity.GetUserId();

            await _cache.RemoveRecordAsync($"{CacheKeys.GetOpeningHours}_{currentUserId}");

            var openingHouresViewModelList = openingHouresString.ConvertJsonStringToObjectModal<OpeningHouresViewModel>();

            var openingHouresDtoList = openingHouresViewModelList
                .Select(op => new OpeningHouresDto()
                {
                    Id = op.Id,
                    IsWorkingDay = op.IsWorkingDay,
                    To = new TimeSpan(op.To.Houres, op.To.Minutes, 0),
                    From = new TimeSpan(op.From.Houres, op.From.Minutes, 0),
                });
            var result = await TimeManagementRepository.EditOpeningHouresAsync(openingHouresDtoList);

            return result.EnumResult == GenericOperationResult.Success ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                });
        }
        #endregion
    }
}
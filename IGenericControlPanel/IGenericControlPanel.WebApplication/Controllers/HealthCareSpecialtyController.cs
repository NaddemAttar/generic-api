﻿
using AutoMapper;

using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Content.IData;
using IGenericControlPanel.Core.Dto.HealthCareSpecialty;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Util;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    
    public class HealthCareSpecialtyController : LocalizedBaseController<HealthCareSpecialtyController>
    {
        #region Properties & Constructor
        private readonly IHealthCareSpecialtyRepository repository;
        private IHttpFileService HttpFileService { get; }
        private IFileRepository FileRepository { get; }
        private IMapper Mapper { get; }
        public HealthCareSpecialtyController(IHealthCareSpecialtyRepository repository,
            IHttpFileService HttpFileService,
            IFileRepository FileRepository,
            IMapper Mapper,
            IConfiguration configuration) : base(configuration)
        {
            this.repository = repository;
            this.HttpFileService = HttpFileService;
            this.FileRepository = FileRepository;
            this.Mapper = Mapper;
        }

        #endregion

        #region Actions
        [HttpPost]
        //[DynamicAuthorization("HealthCareSpecialty", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> Action([FromForm] HealthCareSpecialtyDto formDto)
        {
            if (formDto.ImagesForm.Count != 0)
            {
                var saveImaegInServiceFolder = await FilesExtensionMethods.SaveFiles(new Util.FileOptions
                {
                    FileContentType = FileContentType.Image,
                    FileSourceType = FileSourceType.Allergy,
                    FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                    Files = formDto.ImagesForm,
                    HttpFileService = HttpFileService,
                    Mapper = Mapper,
                    SourceId = formDto.Id,
                    ControllerName = nameof(Model.Core.Allergy)
                });

                if (saveImaegInServiceFolder is null)
                {
                    throw new BadRequestException(FromResx["FilesError"].Value);
                }

                formDto.Images = saveImaegInServiceFolder;
            }

            var result = repository.ActionHealthCareSpecialty(formDto);

            if (result.EnumResult == GenericOperationResult.Success)
            {
                if (result.Result.RemovedFilesUrls != null)
                {
                    _ = HttpFileService.RemoveFiles(result.Result.RemovedFilesUrls);
                }

                return Ok(result);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                _ => throw new Exception(FromResx["InternalServerError"].Value)
            };
        }


        #endregion

        #region Get
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetAllSpecialty(string query, int pageNumber = 0, int pageSize = 5, bool enablePagination = false)
        {
            var result = repository.GetAllSpecialty(query, pageNumber, pageSize, enablePagination);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetSpecialty(int Id)
        {
            var result = repository.GetSpecialty(Id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetSpecialtyServicesByHealthCareSpecialtyId(int HealthCareSpecialtyId)
        {
            var result =await repository.GetSpecialtyServicesByHealthCareSpecialtyId(HealthCareSpecialtyId);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetSpecialtyByServiceId(int ServiceId)
        {
            var result = await repository.GetSpecialtyByServiceId(ServiceId);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion

        #region Remove
        [HttpDelete]
        //[DynamicAuthorization("HealthCareSpecialty", nameof(ActionType.Remove))]
        [Route("[action]")]
        public IActionResult RemoveSpecialty(int Id)
        {
            var result = repository.RemoveSpecialty(Id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        #endregion
    }
}

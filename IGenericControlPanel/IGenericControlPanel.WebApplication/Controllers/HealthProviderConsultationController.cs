﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Consultation.Dto.ConsultationDto;
using IGenericControlPanel.Consultation.IData.Interfaces;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.SharedKernal.Messages;

namespace IGenericControlPanel.WebApplication.Controllers;
[Route("api/[controller]")]
[ApiController]
public class HealthProviderConsultationController : LocalizedBaseController<HealthProviderConsultationController>
{
    #region Constructor & Properties
    private IHealthProviderQuestionsRepository HealthProviderQuestionsRepository { get; }
    private readonly IStringLocalizer<ConsultationResource> consultationLocalizer;
    private readonly IStringLocalizer<SharedResource> sharedLocalizer;
    public HealthProviderConsultationController(
        IStringLocalizer<HealthProviderConsultationController> localizer,
        IHealthProviderQuestionsRepository healthProviderQuestionsRepository,
        IStringLocalizer<ConsultationResource> consultationLocalizer,
        IStringLocalizer<SharedResource> sharedLocalizer) : base(localizer)
    {
        HealthProviderQuestionsRepository = healthProviderQuestionsRepository;
        this.consultationLocalizer = consultationLocalizer;
        this.sharedLocalizer = sharedLocalizer;
    }
    #endregion 

    #region Get Section
    [HttpGet]
    [AllowAnonymous]
    [Route("[action]")]
    public async Task<IActionResult> GetHealthProviderQuestions(bool enablePagination, int pageNumber = 0, int pageSize = 8)
    {
        var result = await HealthProviderQuestionsRepository.GetHealthProviderQuestionsAsync(enablePagination, pageNumber, pageSize);

        return result.EnumResult == GenericOperationResult.Success
            ? Ok(result)
            : (IActionResult)(result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(consultationLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(consultationLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            });
    }

    [HttpGet]
    [AllowAnonymous]
    [Route("[action]")]
    public async Task<IActionResult> GetOneHealthProviderQuestion(int id)
    {
        var result = await HealthProviderQuestionsRepository.GetOneHealthProviderQuestionAsync(id);

        return result.EnumResult == GenericOperationResult.Success
            ? Ok(result)
            : (IActionResult)(result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(consultationLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(consultationLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            });
    }
    #endregion

    #region Action Section
    [HttpPost]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("[action]")]
    public async Task<IActionResult> HealthProviderQuestionAction(HealthProviderQuestionsFormDto healthProviderQuestionsFormDto)
    {
        var result = await HealthProviderQuestionsRepository.HealthProviderQuestionActionAsync(healthProviderQuestionsFormDto);

        return result.EnumResult == GenericOperationResult.Success
            ? Ok(result)
            : (IActionResult)(result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(consultationLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(consultationLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            });
    }
    #endregion

    #region Remove Section 
    [HttpDelete]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("[action]")]
    public async Task<IActionResult> RemoveHealthCareProviderQuestion(int id)
    {
        var result = await HealthProviderQuestionsRepository.RemoveHealthProviderQuestionsAsync(id);

        return result.EnumResult == GenericOperationResult.Success
            ? Ok(result)
            : (IActionResult)(result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(consultationLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(consultationLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            });
    }

    #endregion 

    #region Health Provider Answer Section
    [HttpPut]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("[action]")]
    public async Task<IActionResult> HealthProviderAnswer(HealthProviderAnswerDto healthProviderAnswerDto)
    {
        var result = await HealthProviderQuestionsRepository.AnswerAsync(healthProviderAnswerDto);

        return result.EnumResult == GenericOperationResult.Success
            ? Ok(result)
            : (IActionResult)(result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(consultationLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(consultationLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            });
    }
    #endregion 
}
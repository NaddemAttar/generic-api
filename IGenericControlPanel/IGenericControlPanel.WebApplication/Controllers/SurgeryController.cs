﻿
using AutoMapper;

using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Core.Dto.Surger;
using IGenericControlPanel.Core.Dto.Surgery;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Util;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class SurgeryController : LocalizedBaseController<SurgeryController>
    {
        #region Properties & Constructor
        private ISurgeryRepository SurgerRepository { get; }
        private IHttpFileService HttpFileService { get; }
        private IMapper Mapper { get; }
        public SurgeryController(
            ISurgeryRepository surgerRepository,
            IHttpFileService httpFileService,
            IMapper mapper,
            IConfiguration configuration,
            IStringLocalizer<SurgeryController> localizer) : base(localizer, configuration)
        {
            SurgerRepository = surgerRepository;
            HttpFileService = httpFileService;
            Mapper = mapper;
        }
        #endregion

        #region Action
        [HttpPost]
        //[DynamicAuthorization("Surgery", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> Action([FromForm] SurgeryDto formDto)
        {
            if (formDto.ImagesForm.Count != 0)
            {
                var saveFiles = await FilesExtensionMethods
                  .SaveFiles(new Util.FileOptions
                  {
                      FileContentType = FileContentType.Image,
                      FileSourceType = FileSourceType.Surgery,
                      FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                      Files = formDto.ImagesForm,
                      HttpFileService = HttpFileService,
                      Mapper = Mapper,
                      ControllerName = nameof(Model.Surgery)
                  });

                if (saveFiles is null)
                {
                    throw new BadRequestException(FromResx["FilesError"].Value);
                }

                formDto.Images = saveFiles;
            }
            var result = SurgerRepository.Action(formDto);
            if (result.EnumResult == GenericOperationResult.Success)
            {
                if (result.Result.RemovedFilesUrls != null)
                {
                    _ = HttpFileService.RemoveFiles(result.Result.RemovedFilesUrls);
                }

                return Ok(result);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                _ => throw new Exception(FromResx["InternalServerError"].Value)
            };
        }

        [HttpPut]
        //[DynamicAuthorization("Surgery", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionPatientSurgery([FromForm] PatientSurgeryDto formDto)
        {
            if (formDto.ImagesForm.Count != 0)
            {
                var saveFiles = await FilesExtensionMethods
               .SaveFiles(new Util.FileOptions
               {
                   FileContentType = FileContentType.Image,
                   FileSourceType = FileSourceType.Surgery,
                   FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                   Files = formDto.ImagesForm,
                   HttpFileService = HttpFileService,
                   Mapper = Mapper,
                   ControllerName = nameof(Model.Surgery)
               });

                if (saveFiles is null)
                {
                    throw new BadRequestException(FromResx["FilesError"].Value);
                }

                formDto.Images = saveFiles;
            }
            var result = SurgerRepository.ActionPatientSurgery(formDto);

            if (result.EnumResult == GenericOperationResult.Success)
            {
                if (result.Result.RemovedFilesUrls != null)
                {
                    _ = HttpFileService.RemoveFiles(result.Result.RemovedFilesUrls);
                }

                return Ok(result);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                _ => throw new Exception(FromResx["InternalServerError"].Value)
            };
        }

        #endregion

        #region Get
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetAllSurgeries(
            int? userId, bool enablePagination = true, int pageSize = 10, int pageNumber = 0)
        {
            var result = SurgerRepository.GetAllSurgeries(
                userId, enablePagination, pageSize, pageNumber);
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetSurgeriesAutoComplete(string query = "")
        {
            var result = SurgerRepository.GetSurgeriesAutoComplete(query);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetAllPatientSurgeries(
            int? userId, bool enablePagination = true, int pageSize = 10, int pageNumber = 0)
        {
            var result = await SurgerRepository.GetAllPatientSurgeries(
                userId, enablePagination, pageSize, pageNumber);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetPatientSurgery(int patientSurgeryId)
        {
            var result = SurgerRepository.GetPatientSurgery(patientSurgeryId);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetSurgery(int surgeryId)
        {
            var result = await SurgerRepository.GetSurgery(surgeryId);
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion

        #region Remove
        [HttpDelete]
        //[DynamicAuthorization("Surgery", nameof(ActionType.Remove))]
        [Route("[action]")]
        public IActionResult RemoveSurger(int surgeryId)
        {
            var result = SurgerRepository.RemoveSurgery(surgeryId);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        [HttpDelete]
        //[DynamicAuthorization("Surgery", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemovePatientSurgery(int patientSurgeryId)
        {
            var result = await SurgerRepository.RemovePatientSurgery(patientSurgeryId);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion
    }
}

﻿
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Seeder;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SeedController : LocalizedBaseController<SeedController>
    {
        private ISeederService SeedService { get; set; }
        public SeedController(ISeederService seedService)
        {
            SeedService = seedService;
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> SeedAsync()
        {
            //await SeedService.CreateSettings();
            // await SeedService.CreateRolesAsync();
            //await SeedService.CreateUsers();
            //await SeedService.SeedWebContentAsync();
            //await SeedService.SeedCountriesAndCities();
            //await SeedService.SeedOpeningHoures();
            //await SeedService.SeedHcpAndNormalUserPoints();
            return Ok();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> SeedPrivacyPolicyMaterialsAsync()
        {
            await SeedService.SeedPrivacyMaterials();
            return Ok();
        }

    }
}

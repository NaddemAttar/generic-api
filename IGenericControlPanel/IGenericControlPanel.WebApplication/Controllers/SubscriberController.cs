﻿
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Subscription;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class SubscriberController : LocalizedBaseController<SubscriberController>
    {
        private ISubscriberRepository repository { get; }
        public SubscriberController(ISubscriberRepository repository)
        {
            this.repository = repository;
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult AddSubscriber(SubscriberDto formDto)
        {
            var result = repository.AddSubscriber(formDto);

            return result.EnumResult == GenericOperationResult.Success
                ? (IActionResult)Ok(result)
                : result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                };
        }
        [HttpPut]
        //[DynamicAuthorization("Subscriber", nameof(ActionType.Action))]
        [Route("[action]")]
        public IActionResult UpdateSubscriber(SubscriberDto formDto)
        {
            var result = repository.UpdateSubscriber(formDto);

            return result.EnumResult == GenericOperationResult.Success
                ? (IActionResult)Ok(result)
                : result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                };
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetAllSubscribers(string query)
        {
            var result = repository.GetAllSubscribers(query);

            return result.EnumResult == GenericOperationResult.Success
                ? (IActionResult)Ok(result)
                : result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                };
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetSubscriber(int id)
        {
            var result = repository.GetSubscriber(id);

            return result.EnumResult == GenericOperationResult.Success
                ? (IActionResult)Ok(result)
                : result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                };
        }
        [HttpDelete]
        //[DynamicAuthorization("Subscriber", nameof(ActionType.Remove))]
        [Route("[action]")]
        public IActionResult RemoveSubscriber(int id)
        {
            var result = repository.RemoveSubscriber(id);

            return result.EnumResult == GenericOperationResult.Success
                ? (IActionResult)Ok(result)
                : result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                };
        }
    }
}

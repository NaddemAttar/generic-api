﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums.GamificationSystem;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers;
[Route("api/[controller]")]
[ApiController]
public class GamificationMovementsController : LocalizedBaseController<GamificationMovementsController>
{
    #region Properties & Constructor
    private IGamificationMovementsRepository _gamificationMovementsReposiory { get; }
    public GamificationMovementsController(
        IStringLocalizer<GamificationMovementsController> localizer,
        IGamificationMovementsRepository gamificationMovementsReposiory) : base(localizer)
    {
        _gamificationMovementsReposiory = gamificationMovementsReposiory;
    }
    #endregion

    #region Save Social Media Points Section
    [HttpPut]
    [Route("[action]")]
    public async Task<IActionResult> SaveSocialMediaPoints(
        SocialMediaApps socialMediaApps,
        SectionsForPublishOnSocialMedia sectionsForPublishOnSocialMedia)
    {
        var result = await _gamificationMovementsReposiory.SavePoints(
            GamificationKeys.GetGamificationKeys(socialMediaApps, sectionsForPublishOnSocialMedia));

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages]);

        return Ok(result);
    }
    #endregion
}

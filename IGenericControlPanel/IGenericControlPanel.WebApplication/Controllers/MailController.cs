﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using IGenericControlPanel.Core.Dto.Mail;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Core;

using Microsoft.AspNetCore.Mvc;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MailController : Controller
    {
        private readonly IMailRepository mailService;
        public MailController(IMailRepository mailService)
        {
            this.mailService = mailService;
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> SendMail([FromForm] MailRequest request)
        {
            try
            {
                await mailService.SendEmailAsync(request);
                return Ok();
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> SendEmailSubscriptions([FromForm]MailSubscriptionsDto formDto)
        {
            try
            {
                await mailService.SendEmailSubscriptions(formDto);
                return Ok();
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult>  SendEmailSubscriptionsByIds([FromForm] MailUsersDto formDto)
        {
            try
            {
                await mailService.SendEmailSubscriptionsByIds(formDto);
                return Ok();
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}

﻿
using AutoMapper;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.IData;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Extension;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.ViewModels.Label;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers;

[ApiController]
[Route("api/[controller]")]

public class LabelController : LocalizedBaseController<LabelController>
{
    #region Properties & Constructor
    private IHttpFileService HttpFileService { get; }
    private ILabelRepository<string, string> LabelRepository { get; }
    private IMapper Mapper { get; }

    public LabelController(IConfiguration configuration,
       ILabelRepository<string, string> labelRepository,
       ISettingRepository settingRepository,
       IUserLanguageRepository userLanguageRepository,
       IStringLocalizer<LabelController> stringLocalizer,
       IHttpFileService httpFileService,
       IMapper mapper) :
       base(configuration, userLanguageRepository, settingRepository, stringLocalizer)
    {
        LabelRepository = labelRepository;
        HttpFileService = httpFileService;
        Mapper = mapper;
    }

    #endregion

    #region GetLabels
    [HttpGet]
    [AllowAnonymous]
    [Route("[action]")]
    public IActionResult GetLabels()
    {
        var getLabelsKeys = LabelRepository.GetAll();

        IndexViewModel result = new()
        {
            Labels = LabelRepository
            .GetSetAsDictionary(getLabelsKeys)
            .Where(d => getLabelsKeys.Contains(d.Key))
            .Select(label => new LabelDto()
            {
                Key = label.Key,
                Text = label.Value,
                RenderedHtmlText = ExtensionMethods.HTMLToText(label.Value)
            })
        };

        try
        {
            return Json(result);
        }
        catch (Exception)
        {
            return StatusCode(500);
        }
    }


    #endregion 

    #region UpdateLabel

    [HttpPost]
        //[DynamicAuthorization("Lable", nameof(ActionType.Action))]
    [Route("[action]")]
        public IActionResult UpdateLabel(LabelDto LabelDto)
    {
            if (LabelDto == null)
        {
            return GetFailureAjaxResult("حدثت مشكلة", "يرجى المحاولة في وقت لاحق");
        }

            OperationResult<GenericOperationResult, LabelDto> LabelFormDto = LabelRepository.UpdateOneLabel(LabelDto);

        return LabelFormDto.IsSuccess ? Json(LabelFormDto) : StatusCode(500);
    }

    #endregion

    #region Set New Label 

    [HttpPost]
        //[DynamicAuthorization("Lable", nameof(ActionType.Action))]
    [Route("[action]")]
        public IActionResult AddLabel(LabelDto LabelDto) 
    {
            OperationResult<GenericOperationResult, LabelDto> setNewLabelResult = LabelRepository.SetLabel(LabelDto);

        return !setNewLabelResult.IsSuccess ? GetFailureAjaxResult("Errorr", "this label is Exist already") : (IActionResult)Json(setNewLabelResult);
    }

    #endregion

    #region Get One Label
    [HttpGet]
    //[Authorize(AuthenticationSchemes = "Bearer")]
    [AllowAnonymous]
    [Route("[action]")]
    public IActionResult GetOneLabel(string key)
    {

        if (string.IsNullOrEmpty(key))
        {
            return BadRequest();
        }

        string result = LabelRepository.Get(key);

        return result is null ? StatusCode(500) : Json(result);
    }

    #endregion
}

﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Core.Dto.Offer;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class OfferController : LocalizedBaseController<OfferController>
    {
        private IStringLocalizer<OfferController> localizer { get; }
        private readonly IOfferRepository OfferRepository;
        private readonly IStringLocalizer<OfferResource> offerLocalizer;
        private readonly IStringLocalizer<SharedResource> sharedLocalizer;
        private readonly IStringLocalizer<ProductResource> productLocalizer;
        private readonly IStringLocalizer<BrandResource> brandLocalizer;
        private readonly IStringLocalizer<ServiceResource> serviceLocalizer;
        private readonly IStringLocalizer<CourseResource> courseLocalizer;
        private readonly IUserRepository _userRepository;
        public OfferController(IStringLocalizer<OfferController> localizer, IOfferRepository OfferRepository, IUserRepository _userRepository,
            IStringLocalizer<OfferResource> offerLocalizer,
            IStringLocalizer<SharedResource> sharedLocalizer,
            IStringLocalizer<ProductResource> productLocalizer,
            IStringLocalizer<BrandResource> brandLocalizer,
            IStringLocalizer<ServiceResource> serviceLocalizer,
            IStringLocalizer<CourseResource> courseLocalizer) : base(localizer)
        {
            this.OfferRepository = OfferRepository;
            this.localizer = localizer;
            this._userRepository = _userRepository;
            this.offerLocalizer = offerLocalizer;
            this.sharedLocalizer = sharedLocalizer;
            this.productLocalizer = productLocalizer;
            this.brandLocalizer = brandLocalizer;
            this.serviceLocalizer = serviceLocalizer;
            this.courseLocalizer = courseLocalizer;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> Action(OfferDto offerDto)
        {
            var result = offerDto.Id == 0
                ? await OfferRepository.ActionCreate(offerDto)
                : await OfferRepository.ActionUpdate(offerDto);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(offerLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => result.ErrorMessages == ErrorMessages.OfferNotFound ? throw new NotFoundException(offerLocalizer[result.ErrorMessages].Value) : result.ErrorMessages == ErrorMessages.ProductNotFound ? throw new NotFoundException(productLocalizer[result.ErrorMessages].Value) : result.ErrorMessages == ErrorMessages.ProductSizeColorNotFound ? throw new NotFoundException(productLocalizer[result.ErrorMessages].Value) : result.ErrorMessages == ErrorMessages.MultiLevelCategoriesNotFound ? throw new NotFoundException(productLocalizer[result.ErrorMessages].Value) : result.ErrorMessages == ErrorMessages.BrandNotExist ? throw new NotFoundException(brandLocalizer[result.ErrorMessages].Value) : result.ErrorMessages == ErrorMessages.ServiceNotExist ? throw new NotFoundException(serviceLocalizer[result.ErrorMessages].Value) : result.ErrorMessages == ErrorMessages.ServiceNotExist ? throw new NotFoundException(courseLocalizer[result.ErrorMessages].Value) : throw new Exception(),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetAllOffers(int pageNumber = 0, int pageSize = 5)
        {
            OperationResult<GenericOperationResult, IEnumerable<AllOfferDto>> result = await OfferRepository.GetAllAsync(pageNumber, pageSize);
            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(sharedLocalizer[result.ErrorMessages].Value);

            return Ok(result);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetOffer(int offerId)
        {
            var user = await _userRepository.GetCurrentUserInfo();
            OperationResult<GenericOperationResult, OfferDetailsDto> result = await OfferRepository.GetAsync(offerId);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(offerLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(offerLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> Remove(int offerId)
        {
            var user = await _userRepository.GetCurrentUserInfo();
            var result = await OfferRepository.Remove(offerId);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(offerLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(offerLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetMinMaxPrice(string BrandsIds, string CategoriesIds)
        {
            List<int> brands = BrandsIds.ConvertJsonStringToObjectModal<int>();
            List<int> category = CategoriesIds.ConvertJsonStringToObjectModal<int>();
            OperationResult<GenericOperationResult, MinMaxPrice> result = await OfferRepository.GetMinMaxPrice(brands, category);
            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(sharedLocalizer[result.ErrorMessages].Value);

            return Ok(result);
        }
    }
}
﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using IGenericControlPanel.Core.Dto.Allegry;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AllergyController : LocalizedBaseController<AllergyController>
    {
        #region Properties & Constructor
        private IAllergyRepository AllergyRepository;
        private IStringLocalizer<AllergyResource> sharedLocalizer;
        private IStringLocalizer<SharedResource> allergyLocalizer;
        private IHttpFileService HttpFileService;
        private IMapper Mapper;
        public AllergyController(
            IAllergyRepository allergyRepository,
            IHttpFileService httpFileService,
            IMapper mapper,
            IConfiguration configuration,
            IStringLocalizer<AllergyController> localizer,
            IStringLocalizer<AllergyResource> sharedLocalizer,
            IStringLocalizer<SharedResource> allergyLocalizer) : base(localizer, configuration)
        {
            AllergyRepository = allergyRepository;
            HttpFileService = httpFileService;
            Mapper = mapper;
            this.sharedLocalizer = sharedLocalizer;
            this.allergyLocalizer = allergyLocalizer;
        }
        #endregion

        #region Action Section
        [HttpPost]
        //[DynamicAuthorization("Allergy", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> Action([FromForm] AllergyFormDto formDto)
        {
            if (formDto.ImagesForm.Count != 0)
            {
                var saveImaegInServiceFolder = await FilesExtensionMethods.SaveFiles(new Util.FileOptions
                {
                    FileContentType = FileContentType.Image,
                    FileSourceType = FileSourceType.Allergy,
                    FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                    Files = formDto.ImagesForm,
                    HttpFileService = HttpFileService,
                    Mapper = Mapper,
                    SourceId = formDto.Id,
                    ControllerName = nameof(Allergy)
                });

                if (saveImaegInServiceFolder is null)
                    throw new BadRequestException(sharedLocalizer[ErrorMessages.FailedSavingPhoto].Value);

                formDto.Images = saveImaegInServiceFolder;
            }
            var result = await AllergyRepository.Action(formDto);

            if (result.EnumResult == GenericOperationResult.Success)
            {
                if (result.Result.RemovedFilesUrls != null)
                    _ = HttpFileService.RemoveFiles(result.Result.RemovedFilesUrls);

                return Ok(result);
            }
            else
            {
                if (formDto.ImagesForm != null && formDto.ImagesForm.Count > 0)
                    foreach (var image in formDto.Images)
                        await FilesExtensionMethods.RemoveFileFromWWWRoot(image.Url, HttpFileService);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(allergyLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(allergyLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            };
        }

        [HttpPost]
        //[DynamicAuthorization("Allergy", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionPatientAllegries([FromForm] PatientAllergyFormDto formDto)
        {
            if (formDto.ImagesForm.Count != 0)
            {
                var saveFiles = await FilesExtensionMethods
                    .SaveFiles(new Util.FileOptions
                    {
                        FileContentType = FileContentType.Image,
                        FileSourceType = FileSourceType.Allergy,
                        FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                        Files = formDto.ImagesForm,
                        HttpFileService = HttpFileService,
                        Mapper = Mapper,
                        SourceId = formDto.Id,
                        ControllerName = nameof(Model.Core.Allergy)
                    });

                if (saveFiles is null)
                    throw new BadRequestException(sharedLocalizer[ErrorMessages.FailedSavingPhoto].Value);

                formDto.Images = saveFiles;
            }

            var result = await AllergyRepository.ActionPatientAllergy(formDto);

            if (result.EnumResult == GenericOperationResult.Success)
            {
                if (result.Result.RemovedFilesUrls != null)
                    _ = HttpFileService.RemoveFiles(result.Result.RemovedFilesUrls);

                return Ok(result);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(allergyLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(allergyLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            };
        }
        #endregion

        #region Get
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetAllAllergies(
            int? userId, bool enablePagination = true, int pageSize = 10, int pageNumber = 0)
        {
            var result = await AllergyRepository.GetAllAllergy(userId, enablePagination, pageSize, pageNumber);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(allergyLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(allergyLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetAllergiesAutoComplete(string query = "")
        {
            var result = await AllergyRepository.GetAllergiesAutoComplete(query);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(allergyLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(allergyLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetPatientAllergies(
            int? userId, bool enablePagination = true, int pageSize = 10, int pageNumber = 0)
        {
            var result = await AllergyRepository.GetPatientAllergies(userId,
                enablePagination, pageSize, pageNumber);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(allergyLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(allergyLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetPersonAllergyDetails(int patientAllergyId)
        {
            var result = await AllergyRepository.GetPatientAllergyDetails(patientAllergyId);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(allergyLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(allergyLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetAllergy(int Id)
        {
            var result = await AllergyRepository.GetAllergy(Id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(allergyLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(allergyLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }
        #endregion

        #region Remove
        [HttpDelete]
        //[DynamicAuthorization("Allergy", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveAllegry(int Id)
        {
            var result = await AllergyRepository.RemoveAllergy(Id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        [HttpDelete]
        //[DynamicAuthorization("Allergy", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemovePatientAllegry(int PatientAllergyId)
        {
            var result = await AllergyRepository.RemovePatientAllergy(PatientAllergyId);
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion
    }
}

﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using IGenericControlPanel.Core.Dto.Diseases;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class DiseasesController : LocalizedBaseController<DiseasesController>
    {
        #region Properties & Constuctor
        private IDiseasesRepository DiseasesRepository { get; }
        private IHttpFileService HttpFileService { get; }
        private IMapper Mapper { get; }
        private readonly IStringLocalizer<SharedResource> sharedLocalizer;
        private readonly IStringLocalizer<DiseasesResource> diseasesLocalizer;
        public DiseasesController(
            IDiseasesRepository diseasesRepository,
            IHttpFileService httpFileService,
            IMapper mapper,
            IConfiguration configuration,
            IStringLocalizer<DiseasesController> localizer,
            IStringLocalizer<SharedResource> sharedLocalizer,
            IStringLocalizer<DiseasesResource> diseasesLocalizer) : base(localizer, configuration)
        {
            DiseasesRepository = diseasesRepository;
            HttpFileService = httpFileService;
            Mapper = mapper;
            this.sharedLocalizer = sharedLocalizer;
            this.diseasesLocalizer = diseasesLocalizer;
        }
        #endregion

        #region Actios
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> Action([FromForm] DiseasesFormDto formDto)
        {
            if (formDto.ImagesForm.Count > 0)
            {
                var saveFiles = await FilesExtensionMethods
                  .SaveFiles(new Util.FileOptions
                  {
                      FileContentType = FileContentType.Image,
                      FileSourceType = FileSourceType.Disease,
                      FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                      Files = formDto.ImagesForm,
                      HttpFileService = HttpFileService,
                      Mapper = Mapper,
                      SourceId = formDto.Id,
                      ControllerName = nameof(Model.Core.Diseases)
                  });

                if (saveFiles is null)
                    throw new BadRequestException(sharedLocalizer[ErrorMessages.FailedSavingPhoto].Value);

                formDto.Images = saveFiles;
            }
            var result = await DiseasesRepository.Action(formDto);

            if (result.EnumResult == GenericOperationResult.Success)
            {
                if (result.Result.RemovedFilesUrls != null)
                    _ = HttpFileService.RemoveFiles(result.Result.RemovedFilesUrls);

                return Ok(result);
            }
            else
            {
                if (formDto.ImagesForm != null && formDto.ImagesForm.Count > 0)
                    foreach (var image in formDto.Images)
                        await FilesExtensionMethods.RemoveFileFromWWWRoot(image.Url, HttpFileService);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(diseasesLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(diseasesLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            };
        }

        [HttpPost]
        //[DynamicAuthorization("Diseases", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionPatientDiseases([FromForm] PatientDiseasesDto formDto)
        {
            if (formDto.ImagesForm.Count != 0)
            {
                var saveFiles = await FilesExtensionMethods
                 .SaveFiles(new Util.FileOptions
                 {
                     FileContentType = FileContentType.Image,
                     FileSourceType = FileSourceType.Disease,
                     FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                     Files = formDto.ImagesForm,
                     HttpFileService = HttpFileService,
                     Mapper = Mapper,
                     SourceId = formDto.Id,
                     ControllerName = nameof(Model.Core.Diseases)
                 });

                if (saveFiles is null)
                    throw new BadRequestException(sharedLocalizer[ErrorMessages.FailedSavingPhoto].Value);

                formDto.Images = saveFiles;
            }

            var result = await DiseasesRepository.ActionPatientDiseases(formDto);

            if (result.EnumResult == GenericOperationResult.Success)
            {
                if (result.Result.RemovedFilesUrls != null)
                    _ = HttpFileService.RemoveFiles(result.Result.RemovedFilesUrls);

                return Ok(result);
            }
            else
            {
                if (formDto.ImagesForm != null && formDto.ImagesForm.Count > 0)
                    foreach (var image in formDto.Images)
                        await FilesExtensionMethods.RemoveFileFromWWWRoot(image.Url, HttpFileService);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(diseasesLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(diseasesLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            };
        }

        #endregion

        #region Get
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetAllDiseases(int? userId, int pageSize = 10, int pageNumber = 0)
        {
            var result = await DiseasesRepository.GetAllDiseases(userId, pageSize, pageNumber);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(diseasesLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(diseasesLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetDiseasesAutoComplete(string query = "")
        {
            var result = DiseasesRepository.GetDiseasesAutoComplete(query);

            return result.IsSuccess
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(diseasesLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(diseasesLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetDisease(int Id)
        {
            var result = await DiseasesRepository.GetDisease(Id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(diseasesLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(diseasesLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetAllPatientDisease(
            int? userId, bool enablePagination, int pageSize, int pageNumber)
        {
            var result = await DiseasesRepository.GetAllPatientDisease(userId, enablePagination, pageSize, pageNumber);

            return result.IsSuccess
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(diseasesLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(diseasesLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetPatientDisease(int PatientDiseaseId)
        {
            var result = await DiseasesRepository.GetPatientDisease(PatientDiseaseId);

            return result.IsSuccess
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(diseasesLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(diseasesLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }
        #endregion

        #region Remove
        [HttpDelete]
        //[DynamicAuthorization("Diseases", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveDisease(int Id)
        {
            var result = await DiseasesRepository.RemoveDiseases(Id);

            if (result.EnumResult == GenericOperationResult.Success)
            {
                _ = HttpFileService.RemoveFiles(result.Result);
                return Ok(result);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(diseasesLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(diseasesLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            };
        }

        [HttpDelete]
        //[DynamicAuthorization("Diseases", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemovePatientDisease(int Id)
        {
            var result = await DiseasesRepository.RemovePatientDiseases(Id);

            if (result.EnumResult == GenericOperationResult.Success)
            {
                _ = HttpFileService.RemoveFiles(result.Result);
                return Ok(result);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(diseasesLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(diseasesLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            };
        }
        #endregion
    }
}

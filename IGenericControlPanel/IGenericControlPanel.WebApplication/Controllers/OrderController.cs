﻿using System.Net.Http.Headers;
using System.Web;
using System.Text.Json;
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Order;
using IGenericControlPanel.Core.IData;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Enums.Core;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.ViewModels.Order;
using IGenericControlPanel.WebApplication.ViewModels.Payment;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

using Newtonsoft.Json;

using RestSharp;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class OrderController : LocalizedBaseController<OrderController>
    {
        private IOrderRepository OrderRepository { get; }
        private ICourseRepository CourseRepository { get; }
        public OrderController(
            IOrderRepository orderRepository,
            ICourseRepository courseRepository,
            IStringLocalizer<OrderController> stringLocalizer,
            IConfiguration configuration) : base(configuration)
        {
            OrderRepository = orderRepository;
            CourseRepository = courseRepository;
        }
        [HttpGet]
        //[DynamicAuthorization("Order", nameof(ActionType.Get))]
        [Route("[action]")]
        public IActionResult GetOrder(int OrderId)
        {
            var result = OrderRepository.GetOrder(OrderId);
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                });
        }
        [HttpGet]
        //[DynamicAuthorization("Order", nameof(ActionType.Get))]
        [Route("[action]")]
        public IActionResult GetOrders(int pageNumber = 0, int pageSize = 8, int normalUserId = 0)
        {
            var result = OrderRepository.GetOrders(pageNumber, pageSize, normalUserId);
            return result.EnumResult == GenericOperationResult.Success
               ? Ok(result)
               : (IActionResult)(result.EnumResult switch
               {
                   GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                   GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                   GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                   _ => throw new Exception(FromResx[result.ErrorMessages].Value)
               });
        }

        /// <summary>
        /// Add an order with the products and it's quantities
        /// item1 : the product id
        /// item2 : the quantity of this product
        /// </summary>
        /// 
        [HttpPost]
        //[DynamicAuthorization("Order", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionOrder(OrderFormDto orderFormDto)
        {
            var result = await OrderRepository.ActionOrder(orderFormDto);

            return result.EnumResult == GenericOperationResult.Success
               ? Ok(result)
               : (IActionResult)(result.EnumResult switch
               {
                   GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                   GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                   GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                   _ => throw new Exception(FromResx[result.ErrorMessages].Value)
               });
        }

        [HttpPost]
        //[DynamicAuthorization("Order", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionOrderPayment(PaymentOrderDto orderFormDto)
        {
            try
            {
                var OutletRef = Configuration["OutletRef"];
                var ApiKey = Configuration["APIKey"];
                var client = new HttpClient();
                client.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/vnd.ni-identity.v1+json"));//ACCEPT header
                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri("https://api-gateway.ngenius-payments.com/identity/auth/access-token"),
                    Headers =
                    {
                    { "accept", "*/*" },
                    { "Authorization", "Basic "+ ApiKey},
                    },
                };
                var AccessTokenResult = new PaymentAccessToken();
                using (var response = await client.SendAsync(request))
                {
                    _ = response.EnsureSuccessStatusCode();
                    var body = await response.Content.ReadAsStringAsync();
                    AccessTokenResult = JsonConvert.DeserializeObject<PaymentAccessToken>(body);
                }


                var client2 = new RestClient($"https://api-gateway.ngenius-payments.com/transactions/outlets/" + OutletRef + "/orders");
                var request2 = new RestRequest("", Method.Post);
                _ = request2.AddHeader("accept", "application/vnd.ni-payment.v2+json");
                _ = request2.AddHeader("Content-Type", "application/vnd.ni-payment.v2+json");
                _ = request2.AddHeader("Authorization", "Bearer " + AccessTokenResult.access_token);
                string content = System.Text.Json.JsonSerializer.Serialize<PaymentOrderDto>(orderFormDto);
                Console.WriteLine(content);
                // _ = request2.AddParameter("application/vnd.ni-payment.v2+json", content, ParameterType.RequestBody);
                request2.AddBody(orderFormDto);
                request2.RequestFormat = DataFormat.Json;
                Console.WriteLine(System.Text.Json.JsonSerializer.Serialize(request2));
                RestResponse response2 = await client2.ExecuteAsync(request2);
                Uri myUri = new(orderFormDto.merchantAttributes.redirectUrl);
                if (myUri.ToString().Contains("courseId"))
                {
                    var CourseId = int.Parse(HttpUtility.ParseQueryString(myUri.Query).Get("courseId"));
                }
                else if (myUri.ToString().Contains("orderId"))
                {
                    var OrderId = int.Parse(HttpUtility.ParseQueryString(myUri.Query).Get("orderId"));
                    _ = OrderRepository.ChangeOrderStatus(OrderId, PaymentStatus.PENDING);
                }
                var result = JsonConvert.DeserializeObject<PaymentResponse>(response2.Content);
                return Json(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpDelete]
        //[DynamicAuthorization("Order", nameof(ActionType.Remove))]
        [Route("[action]")]
        public IActionResult RemoveOrder(int OrderId)
        {
            var result = OrderRepository.RemoveOrder(OrderId);
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                });
        }
    }
}

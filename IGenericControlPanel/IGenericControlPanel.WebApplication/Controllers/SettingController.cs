﻿using System.Collections.Generic;
using System.Linq;

using IGenericControlPanel.Configuration.Dto;
using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.ViewModels.Setting;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [Authorize(Roles = nameof(Role.Admin) + "," + nameof(Role.Modifier))]
    [ApiController]
    [Route("api/[controller]")]
    public class SettingController : BaseController
    {
        #region Properties

        private List<string> SupportedSettings { get; }
        #endregion

        #region Constructor

        public SettingController(ISettingRepository settingRepository, IUserLanguageRepository userLanguageRepository) :
            base(userLanguageRepository, settingRepository)
        {

            SupportedSettings = new List<string>
            {
                SettingName.DefaultCultureCode,
                SettingName.Email,
                SettingName.FacebookLink,
                SettingName.LinkedInLink,
                SettingName.YoutubeLink,
                SettingName.Telegram,
                SettingName.Whatsapp,
                SettingName.InstagramLink,
                SettingName.LatestProductsCount,
                SettingName.PhoneNumber,
                SettingName.WebsiteFont,
                SettingName.longitude,
                SettingName.latitude,
                SettingName.WorkTime,
                SettingName.Location
            };

        }

        #endregion

        #region Index
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public IActionResult Index()
        {
            return Json(GetIndexViewModel());
        }


        #region private get index view model
        private IndexViewModel GetIndexViewModel()
        {
            IRequestCultureFeature rqf = Request.HttpContext.Features.Get<IRequestCultureFeature>();
            // Culture contains the information of the requested culture
            System.Globalization.CultureInfo culture = rqf.RequestCulture.Culture;
            bool isArabic = culture.Name == "ar-SY";
            string primaryLanguageCultureCode = SettingRepository.Get(SettingName.DefaultCultureCode);
            List<string> edibleSettings = SettingRepository.EdibleSettings.ToList();
            IEnumerable<SettingDto> settingsDtos = SettingRepository
                .GetSetAsDictionary(SupportedSettings)
                .Where(d => edibleSettings.Contains(d.Key))
                .Select(setting => new SettingDto()
                {
                    Name = setting.Key,
                    Value = setting.Value,
                    IsValid = true,
                });
            string PageTitle = isArabic ? "إدارة الإعدادات" : "Settings Management";
            IndexViewModel viewModel = new(PageTitle)
            {
                Settings = settingsDtos,
                SettingSearchViewModel = new SettingSearchViewModel()
                {
                    Settings = settingsDtos,
                },
                SettingTableResultsViewModel = new SettingTableResultsViewModel()
                {
                    SettingTableResultsBodyViewModel = new SettingTableResultsBodyViewModel()
                    {
                        SettingsTableResultsBodyRowViewModels = SettingRepository.GetSetAsDictionary(SupportedSettings)
                        .Where(d => edibleSettings.Contains(d.Key))
                        .Select(settings => new SettingTableResultsBodyRowViewModel()
                        {
                            PrimaryLanguageCultureCode = primaryLanguageCultureCode,
                            Setting = new SettingDto()
                            {
                                Name = settings.Key,
                                Value = settings.Value,
                                IsValid = true,
                            }
                        }),
                    },
                    SettingTableResultsHeaderViewModel = new SettingTableResultsHeaderViewModel()
                    {
                        PrimaryLanguageCultureCode = primaryLanguageCultureCode,
                    },
                }
            };
            return viewModel;
        }
        #endregion

        #endregion 

        #region GET ACTION METHOD

        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public IActionResult Action()
        {
            IRequestCultureFeature rqf = Request.HttpContext.Features.Get<IRequestCultureFeature>();
            // Culture contains the information of the requested culture
            System.Globalization.CultureInfo culture = rqf.RequestCulture.Culture;
            bool isArabic = culture.Name == "ar-SY";
            string primaryCultureCode = SettingRepository.Get(SettingName.DefaultCultureCode);
            List<string> edibleSettings = SettingRepository.EdibleSettings.ToList();
            IEnumerable<SettingDto> settingsDtos = SettingRepository.GetSetAsDictionary(SupportedSettings)
                .Where(d => edibleSettings.Contains(d.Key))
                .Select(setting => new SettingDto()
                {
                    Name = setting.Key,
                    Value = setting.Value,
                    IsValid = true,
                });
            string PageTitle = isArabic ? "تعديل الإعدادات" : "Updates Settings";
            SettingsFormViewModel settingViewModel = new(PageTitle)
            {
                LocationLatitude = settingsDtos
                .Where(lattitude => lattitude.Name == "latitude")
                .Select(lattitude => lattitude.Value)
                .SingleOrDefault(),

                LocationLongitude = settingsDtos
                .Where(longtitude => longtitude.Name == "longitude")
                .Select(longtitude => longtitude.Value)
                .SingleOrDefault(),

                PrimaryLanguageFormDto = new SettingFormDto()
                {
                    CultureCode = primaryCultureCode,
                    Settings = settingsDtos.ToList(),
                    IsValid = true
                },

            };
            return Json(settingViewModel);
        }
        #endregion

        #region POST ACTION METHOD

        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public IActionResult Action(SettingsFormViewModel settingsFormViewModel)
        {
            IRequestCultureFeature rqf = Request.HttpContext.Features.Get<IRequestCultureFeature>();
            // Culture contains the information of the requested culture
            var culture = rqf.RequestCulture.Culture;
            bool isArabic = culture.Name == "ar-SY";

            string operationName = isArabic ? "حفظ الإعدادات" : "Save settings";
            string errorTitle = isArabic ? "حدث خطأ ما" : "An error has occured";
            string errorMsg = isArabic ? "حدث خطأ داخلي، يرجى مراجعة المطور" : "Internal error, contact developer";
            string success = isArabic ? "تم حفظ الإعدادات بنجاح" : "Setting has been saved successfully!";
            string fail = isArabic ? "فشلت عملية الحفظ" : "Operation failed!";
            if (settingsFormViewModel == null)
            {
                return GetFailureAjaxResult(errorTitle, errorMsg);
            }
            Dictionary<string, string> dicCreation = settingsFormViewModel.PrimaryLanguageFormDto.Settings.ToDictionary(key => key.Name, value => value.Value);
            dicCreation.Add("longitude", settingsFormViewModel.LocationLongitude);
            dicCreation.Add("latitude", settingsFormViewModel.LocationLatitude);
            var resultPrimary = SettingRepository.Action(dicCreation);

            return resultPrimary.IsSuccess
                ? GetSuccessAjaxResult(operationName, success, new
                {
                    PrimaryId = 0,
                    PrimaryName = "",
                    PrimaryEntity = resultPrimary.Result,
                    TranslationsEntity = "",
                    Successed = resultPrimary.IsSuccess,
                })
                : (IActionResult)GetModelStateErrorAjaxResult(fail);
        }

        #endregion

    }
}

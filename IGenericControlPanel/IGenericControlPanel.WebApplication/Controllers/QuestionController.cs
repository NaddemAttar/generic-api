﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Core.Dto.Question;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Security.Data.Repositories;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Util;
using IGenericControlPanel.WebApplication.ViewModels.Question;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class QuestionController : LocalizedBaseController<QuestionController>
    {
        #region Properties & Constructor
        private IHttpFileService HttpFileService { get; }
        public IQuestionRepository QuestionRepository { get; }
        public IUserRepository userRepository { get; }
        private IMapper Mapper;
        private IStringLocalizer<QuestionResource> questionLocalizer;
        private IStringLocalizer<SharedResource> sharedLocalizer;
        private readonly string blob = nameof(blob);
        private readonly string wwwRootBath = "wwwroot/";
        public QuestionController(IConfiguration configuration,
             IUserLanguageRepository userLanguageRepository,
             ISettingRepository settingRepository,
             IStringLocalizer<QuestionController> stringLocalizer,
             IQuestionRepository questionRepository,
             IHttpFileService httpFileService,
             IMapper mapper,
             IStringLocalizer<QuestionResource> questionLocalizer,
             IStringLocalizer<SharedResource> sharedLocalizer,
             IUserRepository userRepository)
            : base(configuration, userLanguageRepository, settingRepository, stringLocalizer)
        {
            QuestionRepository = questionRepository;
            HttpFileService = httpFileService;
            Mapper = mapper;
            this.questionLocalizer = questionLocalizer;
            this.sharedLocalizer = sharedLocalizer;
            this.userRepository = userRepository;
        }
        #endregion

        #region Index Section
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetQuestions(
            bool? showFirst, bool? isHidden,
            int? PostTypeId, int? MultiLevelCategoryId,
            int? UserId,
            string query, string serviceIds, int? GroupId, bool FAQ, bool WithAnswersOnly,
            int pageNumber = 0, int pageSize = 6, bool enablePagination = true)
        {
            List<int> ServiceIds = serviceIds.ConvertJsonStringToObjectModal<int>();

            var result = await QuestionRepository.GetAllDetailedPaged(showFirst, isHidden,
                PostTypeId, MultiLevelCategoryId, UserId, ServiceIds,
                GroupId, WithAnswersOnly, FAQ, query, pageNumber, pageSize, "ar", enablePagination);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(questionLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(questionLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetQuestionDetails(int id)
        {
            var result = QuestionRepository.GetDetailed(id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(questionLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(questionLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetQuestionAutoComplete(string query)
        {
            var result = await QuestionRepository.GetQuestionAutoComplete(query);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(questionLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(questionLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }
        #endregion

        #region Remove Question
        [HttpDelete]
        //[DynamicAuthorization("Question", nameof(ActionType.Remove))]
        [Route("[action]")]

        public async Task<IActionResult> RemoveQuestion(int id)
        {
            var result = await QuestionRepository.Remove(id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(questionLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(questionLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        #endregion

        #region GetFile

        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public JsonResult GetFile(int id)
        {
            QuestionDetailsDto image = QuestionRepository.GetDetailed(id).Result;

            return Json(image);
        }
        #endregion

        #region Action
        [HttpPost]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> ActionQuestion([FromForm] QuestionFormViewModel viewModel)
        {
            if (viewModel == null)
                throw new InternalServerException(sharedLocalizer[ErrorMessages.InternalServerError].Value);

            QuestionFormDto formDto = new()
            {
                Id = viewModel.Id,
                Attachments = viewModel.Attachments,
                Content = viewModel.Content,
                CultureCode = viewModel.CultureCode,
                SendEmail = viewModel.SendEmail,
                RemovedFilesIds = viewModel.RemovedFilesIds,
                ServiceId = viewModel.ServiceId,
                FAQ = viewModel.FAQ,
                PostTypeId = viewModel.PostTypeId == 0 ? null : viewModel.PostTypeId,
                MultiLevelCategoryIds = viewModel.MultiLevelCategoryIds,
                GroupId = viewModel.GroupId,
                IsHidden = viewModel.IsHidden,
                ShowFirst = viewModel.ShowFirst
            };

            if (viewModel.AttachedFiles != null && viewModel.AttachedFiles.Count > 0)
            {
                var saveFiles = await FilesExtensionMethods
                    .SaveFiles(new Util.FileOptions
                    {
                        FileContentType = FileContentType.Attachement,
                        FileSourceType = FileSourceType.Question,
                        FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                        Files = viewModel.AttachedFiles,
                        HttpFileService = HttpFileService,
                        Mapper = Mapper,
                        SourceId = formDto.Id,
                        ControllerName = nameof(Model.Core.Question)
                    });
                if (saveFiles is null)
                    throw new BadRequestException(sharedLocalizer[ErrorMessages.FailedSavingPhoto].Value);

                formDto.Attachments = saveFiles;
            }

            var result = await QuestionRepository.Action(formDto);

            if (result.IsSuccess)
            {
                _ = HttpFileService.RemoveFiles(result.Result.RemovedFilesUrls);
                return Ok(result);
            }
            else
            {
                if (viewModel.AttachedFiles != null && viewModel.AttachedFiles.Count > 0)
                    foreach (var image in formDto.Attachments)
                        await FilesExtensionMethods.RemoveFileFromWWWRoot(image.Url, HttpFileService);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(questionLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(questionLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            };
        }
        [HttpPost]
        //[DynamicAuthorization("Question", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionAnswer([FromForm] AnswerFormViewModel viewModel)
        {
            if (viewModel == null)
                throw new InternalServerException(sharedLocalizer[ErrorMessages.InternalServerError].Value);

            AnswerFormDto formdto = new()
            {
                Id = viewModel.Id,
                QuestionId = viewModel.QuestionId,
                Content = viewModel.Content,
                RemovedFilesIds = viewModel.RemovedFilesIds,
                Files = viewModel.Attachments
            };
            if (viewModel.AttachedFiles != null && viewModel.AttachedFiles.Count > 0)
            {
                var saveFiles = await FilesExtensionMethods
                   .SaveFiles(new Util.FileOptions
                   {
                       FileContentType = FileContentType.Attachement,
                       FileSourceType = FileSourceType.Question,
                       FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                       Files = viewModel.AttachedFiles,
                       HttpFileService = HttpFileService,
                       Mapper = Mapper,
                       SourceId = viewModel.Id,
                       ControllerName = nameof(Model.Core.Question)
                   });

                formdto.Files = saveFiles;
            }

            var result = await QuestionRepository.ActionAnswerQuestion(formdto);

            if(!result.IsSuccess)
            {
                if (viewModel.AttachedFiles != null && viewModel.AttachedFiles.Count > 0)
                    foreach (var image in formdto.Files)
                        await FilesExtensionMethods.RemoveFileFromWWWRoot(image.Url, HttpFileService);
            }

            OperationResult<GenericOperationResult> httpRemoveResult = HttpFileService.RemoveFiles(result.Result.RemovedFilesData.Select(file => file.Url));
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(questionLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(questionLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        [HttpDelete]
        //[DynamicAuthorization("Question", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveAnswer(int AnswerId)
        {
            var result = await QuestionRepository.RemoveAnswer(AnswerId);
            // We should delete the photots from server & DB but we can't reach to DB Files to get ImageUrl.
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(questionLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(questionLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }
        #endregion
    }
}
﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.IData;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Util;
using IGenericControlPanel.WebApplication.ViewModels.Blog;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BlogController : LocalizedBaseController<BlogController>
    {
        private readonly IHttpFileService HttpFileService;
        private readonly IBlogRepository _blogRepos;
        private readonly IStringLocalizer<SharedResource> sharedLocalizer;
        private readonly IStringLocalizer<BlogResource> bolgLocalizer;
        private readonly IMapper Mapper;
        private readonly string blob = nameof(blob);
        //private readonly string wwwRootBath = "wwwroot/";
        public BlogController(IConfiguration configuration,
             IUserLanguageRepository userLanguageRepository,
             ISettingRepository settingRepository,
             IStringLocalizer<BlogController> stringLocalizer, // no need it.
             IStringLocalizer<BlogResource> bolgLocalizer,
             IStringLocalizer<SharedResource> sharedLocalizer,
             IBlogRepository blogRepos,
             IHttpFileService httpFileService,
             IMapper mapper,
             IUserRepository userRepository,
             IDistributedCache cache)
            : base(configuration, userLanguageRepository, settingRepository, stringLocalizer, cache, userRepository)
        {
            _blogRepos = blogRepos;
            HttpFileService = httpFileService;
            Mapper = mapper;
            this.sharedLocalizer = sharedLocalizer;
            this.bolgLocalizer = bolgLocalizer;
        }

        #region Index Section
        [HttpGet]
        [AllowAnonymous]

        [Route("[action]")]
        public async Task<IActionResult> GetBlogs(bool? showFirst, bool? isHidden,
            int? PostTypeId, int? MultiLevelCategoryId, int? ServiceId, int? GroupId,
            string query, int pageNumber = 0, int pageSize = 6)
        {
            var result = await _blogRepos.GetAllDetailedPagedAsync(showFirst, isHidden, PostTypeId,
                MultiLevelCategoryId, ServiceId, GroupId,
                query, pageNumber, pageSize);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(bolgLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(bolgLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetBlogCategories(int BlogId)
        {
            var result = await _blogRepos.GetBlogCategoriesAsync(BlogId);

            return result.EnumResult == GenericOperationResult.Success
                 ? Ok(result)
                 : (IActionResult)(result.EnumResult switch
                 {
                     GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                     GenericOperationResult.ValidationError => throw new BadRequestException(bolgLocalizer[result.ErrorMessages].Value),
                     GenericOperationResult.NotFound => throw new NotFoundException(bolgLocalizer[result.ErrorMessages].Value),
                     _ => throw new Exception(sharedLocalizer["InternalServerError"].Value)
                 });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetBlogDetails(int id)
        {
            BlogDto data = null;
            OperationResult<GenericOperationResult, BlogDto> result = null;
            string recordKey = $"{CacheKeys.GetOneBrand}_{id}";

            data = await _cache.GetRecordAsync<BlogDto>(recordKey);

            if (data is null)
            {
                result = await _blogRepos.GetAsync(id);

                await _cache.SetRecordAsync(recordKey, result.Result,
                    result.DataIsValid());
            }
            else
            {
                result = new();
                result.Result = data;
            }

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(bolgLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(bolgLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer["InternalServerError"].Value)
                });
        }
        #endregion

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> Action([FromForm] BlogFormViewModel viewModel)
        {
            if (viewModel.Id != 0)
            {
                await _cache.RemoveRecordAsync(
                    $"{CacheKeys.GetOneBlog}_{viewModel.Id}");
            }

            if (viewModel.AttachedFiles.Count != 0)
            {
                var saveImaegInServiceFolder = await FilesExtensionMethods.SaveFiles(new Util.FileOptions
                {
                    FileContentType = FileContentType.Image,
                    FileSourceType = FileSourceType.Allergy,
                    FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                    Files = viewModel.AttachedFiles,
                    HttpFileService = HttpFileService,
                    Mapper = Mapper,
                    SourceId = viewModel.Id,
                    ControllerName = nameof(Model.Core.Allergy)
                });

                if (saveImaegInServiceFolder is null)
                    throw new BadRequestException(sharedLocalizer[ErrorMessages.FailedSavingPhoto].Value);

                viewModel.Attachments = saveImaegInServiceFolder;
            }

            var result = await _blogRepos.ActionAsync(new BlogFormDto()
            {
                Id = viewModel.Id,
                Attachments = viewModel.Attachments,
                //Date = viewModel.Date,
                Content = viewModel.Content,
                CultureCode = viewModel.CultureCode,
                CurrentImgs = viewModel.CurrentImgs,
                Images = viewModel.Images,
                Title = viewModel.Title,
                MultiLevelCategoryIds = viewModel.MultiLevelCategoryIds,
                BlogType = viewModel.BlogType,
                RemovedFilesIds = viewModel.RemovedFilesIds,
                ServiceId = viewModel.ServiceId,
                YouTubeLink = viewModel.YouTubeLink,
                SendEmail = viewModel.SendEmail,
                PostTypeId = viewModel.PostTypeId == 0 ? null : viewModel.PostTypeId,
                GroupId = viewModel.GroupId,
                IsHidden = viewModel.IsHidden,
                ShowFirst = viewModel.ShowFirst
            });

            if (result.EnumResult == GenericOperationResult.Success)
            {
                if (result.Result.RemovedFilesUrls != null)
                    _ = HttpFileService.RemoveFiles(result.Result.RemovedFilesUrls);

                return Ok(result);
            }
            else
            {
                if (viewModel.Attachments != null && viewModel.Attachments.Count > 0)
                    foreach (var image in viewModel.Attachments)
                        await FilesExtensionMethods.RemoveFileFromWWWRoot(image.Url, HttpFileService);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(bolgLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(bolgLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer["InternalServerError"].Value)
            };
        }

        #region Remove Blog
        [HttpDelete]
        [Route("[action]")]

        public async Task<IActionResult> RemoveBlog(int id)
        {
            if (id != 0)
            {
                await _cache.RemoveRecordAsync(
                    $"{CacheKeys.GetOneBlog}_{id}");
            }

            var result = await _blogRepos.RemoveAsync(id);            
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(bolgLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(bolgLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer["InternalServerError"].Value)
                });
        }

        #endregion

        #region files
        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public async Task<IActionResult> ActionFile(IFormFile file)
        {
            LocalizedString operationName = FromResx[LocalizerStrings.OperationName_Save];
            {
                if (file != null)
                {
                    try
                    {
                        var fileToAdd = await FilesExtensionMethods.SaveFile(new Util.FileOptions
                        {
                            FileContentType = FileContentType.Attachement,
                            FileSourceType = FileSourceType.Blog,
                            FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                            File = file,
                            HttpFileService = HttpFileService,
                            Mapper = Mapper,
                            ControllerName = nameof(Model.Core.Blog)
                        });

                        var resultPrimary = await _blogRepos.ActionFileAsync(fileToAdd);
                        return Json(resultPrimary);
                    }
                    catch (Exception)
                    {
                        return GetFailureAjaxResult(FromResx[LocalizerStrings.OperationTitle_Failure_Try_Again],
                            FromResx[LocalizerStrings.OperationMessage_Failure_Try_Again]);
                    }
                }
            }

            return GetModelStateErrorAjaxResult(FromResx[LocalizerStrings.OperationMessage_Failure_Logic_Add_Update]);
        }
        #endregion
    }
}
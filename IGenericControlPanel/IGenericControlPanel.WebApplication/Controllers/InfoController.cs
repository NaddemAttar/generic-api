﻿
using AutoMapper;

using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Content.IData;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.Dto.Information;
using IGenericControlPanel.Core.IData;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Util;
using IGenericControlPanel.WebApplication.Util.Enums;
using IGenericControlPanel.WebApplication.ViewModels.Info;
using IGenericControlPanel.WebApplication.ViewModels.Info.Partials.TableResults;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class InfoController : LocalizedBaseController<InfoController>
    {
        #region Properties & Constructes
        private IHttpFileService HttpFileService { get; }
        private IFileRepository FileRepository { get; }
        private IInformationRepository InformationRepository { get; }
        private IMapper Mapper { get; }
        public InfoController(IConfiguration configuration,
             IUserLanguageRepository userLanguageRepository,
             ISettingRepository settingRepository,
             IStringLocalizer<InfoController> stringLocalizer,
             IHttpFileService httpFileService,
             IFileRepository fileRepository,
             IInformationRepository informationRepository,
             IMapper mapper)
             : base(configuration, userLanguageRepository, settingRepository, stringLocalizer)
        {
            HttpFileService = httpFileService;
            FileRepository = fileRepository;
            InformationRepository = informationRepository;
            Mapper = mapper;
        }
        #endregion

        #region Index Section
        [HttpGet]
        //[DynamicAuthorization("Info", nameof(ActionType.Get))]
        [Route("[action]")]
        public IActionResult Index()
        {
            return Json(GetIndexViewModel());
        }
        private IndexViewModel GetIndexViewModel()
        {
            string primaryLanguageCultureCode = SettingRepository.Get(SettingName.DefaultCultureCode);
            System.Collections.Generic.IEnumerable<Configuration.Dto.UserLanguageDto> userSpecifiedCultures = UserLanguageRepository.GetAll().Result;
            LocalizedString PageTitle = FromResx[LocalizerStrings.PageTitle_Index];
            IndexViewModel viewModel = new(PageTitle)
            {
                InfoSearchViewModel = new InfoSearchViewModel()
                {
                    Informations = InformationRepository.GetAll().Result,
                },
                InfoTableResultsViewModel = new InfoTableResultsViewModel()
                {
                    InfoTableResultsBodyViewModel = new InfoTableResultsBodyViewModel()
                    {
                        InfoTableResultsBodyRowViewModel = InformationRepository.GetAllDetailed().Result
                        .Select(info => new InfoTableResultsBodyRowViewModel()
                        {
                            PrimaryLanguageCultureCode = primaryLanguageCultureCode,
                            InfoDetailsDto = info,
                            UserSpecifiedCultures = userSpecifiedCultures,
                        }),
                    },

                    InfoTableResultsHeaderViewModel = new InfoTableResultsHeaderViewModel()
                    {
                        PrimaryLanguageCultureCode = primaryLanguageCultureCode,
                        UserSpecifiedCultures = userSpecifiedCultures
                    },
                }
            };
            return viewModel;
        }
        #endregion

        #region Action Get Section
        [HttpGet]
        //[DynamicAuthorization("Info", nameof(ActionType.Action))]
        [Route("[action]")]
        public IActionResult Action(int id)
        {
            InfoFormViewModel viewModel = GetActionViewModel(id);
            return viewModel == null
                ? GetFailureAjaxResult(FromResx[LocalizerStrings.OperationTitle_Failure_Try_Again], FromResx[LocalizerStrings.OperationMessage_Failure_Try_Again])
                : (IActionResult)Json(viewModel);
        }

        private InfoFormViewModel GetActionViewModel(int id)
        {
            InfoFormViewModel viewModel = null;
            ActionOperationType operationType = id == 0 ? ActionOperationType.Create : ActionOperationType.Update;
            switch (operationType)
            {
                case ActionOperationType.Create:
                    viewModel = GetCreateViewModel();
                    break;
                case ActionOperationType.Update:
                    viewModel = GetUpdateViewModel(id);
                    break;
            }

            return viewModel;
        }

        private InfoFormViewModel GetCreateViewModel()
        {
            LocalizedString PageTitle = FromResx[LocalizerStrings.PageTitle_Action_Add];
            InfoFormViewModel viewModel = new(PageTitle)
            {
               

                TranslationForms = NonDefaultUserLanguages
                .Select(languages => new InfoTranslationFormDto()
                {
                    CultureCode = languages.CultureCode
                }).ToList(),
            };

            return viewModel;
        }

        private InfoFormViewModel GetUpdateViewModel(int id)
        {
            CraftLab.Core.OperationResults.OperationResult<CraftLab.Core.OperationResults.GenericOperationResult, InfoDetailsDto> infoOperationResults = InformationRepository.GetDetailed(id);
            if (!infoOperationResults.IsSuccess)
            {
                return new InfoFormViewModel("")
                {
                    Errors = infoOperationResults.ErrorMessages
                };
            }
            InfoDetailsDto infoToUpdate = infoOperationResults.Result;
            LocalizedString PageTitle = FromResx[LocalizerStrings.PageTitle_Action_Edit];
            InfoFormViewModel viewModel = new(PageTitle)
            {
                Id = infoToUpdate.Id,
                Address = infoToUpdate.Address,
                Description = infoToUpdate.Description,
                CurrentDate = infoToUpdate.CurrentDate.FormatString(),
                Images = infoToUpdate.Images,
                CultureCode = infoToUpdate.CultureCode
            };

            viewModel.TranslationForms = infoToUpdate.Translations != null && infoToUpdate.Translations.Any()
                ? infoToUpdate.Translations.Select(translation => new InfoTranslationFormDto()
                {
                    Id = translation.Id,
                    Address = translation.Address,
                    Description = translation.Description,
                    CultureCode = translation.CultureCode,
                    InfoId = infoToUpdate.Id,
                    IsValid = true
                }).ToList()
                : (System.Collections.Generic.IList<InfoTranslationFormDto>)NonDefaultUserLanguages.Select(translation => new InfoTranslationFormDto()
                {
                    CultureCode = translation.CultureCode,
                    InfoId = infoToUpdate.Id,
                }).ToList();

            return viewModel;
        }
        #endregion

        #region Action Post Section
        [HttpPost]
        //[DynamicAuthorization("Info", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> Action(InfoFormViewModel viewModel)
        {
            if (!ModelState.IsValid || viewModel is null)
            {
                return GetFailureAjaxResult(FromResx[LocalizerStrings.OperationTitle_Failure_Try_Again],
                    FromResx[LocalizerStrings.OperationMessage_Failure_Try_Again]);
            }

            InfoFormDto infoFormDto = new()
            {
                Id=viewModel.Id,
                Address=viewModel.Address,
                CultureCode=viewModel.CultureCode,
                CurrentDate=viewModel.CurrentDate,
                CurrentImgs=viewModel.CurrentImgs,
                Description=viewModel.Description,
                Images=viewModel.Images,
                TranslationsFormDto = viewModel.TranslationForms
            };

            // Checking for images
            if (infoFormDto != null)
            {
                if (infoFormDto.Id != 0)
                {
                    var project = InformationRepository.GetDetailed(infoFormDto.Id);
                    if (!project.IsSuccess)
                    {
                        return GetFailureAjaxResult(FromResx[LocalizerStrings.OperationTitle_Failure_Try_Again], FromResx[LocalizerStrings.OperationMessage_Failure_Try_Again]);
                    }

                    if (project.Result.Images is null || (project.Result.Images != null && project.Result.Images.Count == 0))
                    {
                        return GetFailureAjaxResult(FromResx[LocalizerStrings.OperationTitle_Failure_Image_Save], FromResx[LocalizerStrings.OperationMessage_Failure_Image_Is_Required]);
                    }
                }
                else
                {
                    return GetFailureAjaxResult(FromResx[LocalizerStrings.OperationTitle_Failure_Image_Save], FromResx[LocalizerStrings.OperationMessage_Failure_Image_Is_Required]);
                }
            }


            var resultPrimary = InformationRepository.Action(infoFormDto);

            return resultPrimary.IsSuccess
                ? GetSuccessAjaxResult(FromResx[LocalizerStrings.OperationName_Save], FromResx[LocalizerStrings.OperationMessage_Success_Add_Update], new
                {
                    PrimaryId = resultPrimary.Result?.Id ??
                    resultPrimary.Result.TranslationsFormDto?.FirstOrDefault()?.InfoId ?? 0,
                    PrimaryName = resultPrimary.Result?.Address,
                    PrimaryEntity = resultPrimary.Result,
                    TranslationsEntity = resultPrimary.Result.TranslationsFormDto,
                    Successed = resultPrimary.IsSuccess,
                })
                : (IActionResult)GetFailureAjaxResult(FromResx[LocalizerStrings.OperationTitle_Failure_Try_Again], FromResx[LocalizerStrings.OperationMessage_Failure_Logic_Add_Update]);
        }
        #endregion

        #region Remove Section
        [HttpDelete]
        //[DynamicAuthorization("Info", nameof(ActionType.Remove))]
        [Route("[action]")]
        public IActionResult RemoveInfo(int id)
        {
            string operationName = FromResx[LocalizerStrings.OperationName_Delete];
            try
            {
                CraftLab.Core.OperationResults.OperationResult<CraftLab.Core.OperationResults.GenericOperationResult> removeResult = InformationRepository.Remove(id);
                if (removeResult.IsSuccess)
                {
                    return GetSuccessAjaxResult(operationName, FromResx[LocalizerStrings.OperationMessage_Success_Remove]);
                }
            }
            catch
            {
                return GetFailureAjaxResult(operationName, FromResx[LocalizerStrings.OperationMessage_Failure_Exception_Remove]);

            }
            return GetFailureAjaxResult(operationName, FromResx[LocalizerStrings.OperationMessage_Failure_Logic_Remove]);
        }
        #endregion

        #region Get All Files For Info
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public JsonResult GetAllFiles(int id)
        {
            InfoDetailsDto files = InformationRepository.GetDetailed(id).Result;
            return Json(files.Images);
        }
        #endregion
    }
}

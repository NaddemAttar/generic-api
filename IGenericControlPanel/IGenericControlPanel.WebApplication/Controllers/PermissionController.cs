﻿using System;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Security.Dto.Permissions;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.ViewModels.Permissions;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [Authorize(Roles = nameof(Role.Admin) + "," + nameof(Role.Modifier))]
    [ApiController]
    [Route("api/[controller]")]
    public class PermissionController : LocalizedBaseController<PermissionController>
    {
        #region Properties And Constructor

        private readonly IUserRepository _userRepository;
        private readonly IPermissionRepository _permissionRepository;
        public PermissionController(IStringLocalizer<PermissionController> stringLocalizer,
                                    ISettingRepository settingRepository,
                                    IUserLanguageRepository userLanguageRepository,
                                    IPermissionRepository permissionRepository,
                                    IUserRepository userRepository,
                                    IConfiguration configuration)
            : base(configuration, userLanguageRepository, settingRepository, stringLocalizer)
        {
            _permissionRepository = permissionRepository;
            _userRepository = userRepository;
        }
        #endregion

        #region Index

        [HttpGet]
        [Route("[action]")]
        public IActionResult Index()
        {
            System.Collections.Generic.IEnumerable<RoleDto> data = _permissionRepository.GetAllPermission();
            return Json(data);
        }
        #endregion

        #region Post

        [HttpPost]
        [Route("[action]")]
        public IActionResult Action(PermissionViewModel viewModel)
        {
            try
            {
                OperationResult<GenericOperationResult, System.Collections.Generic.IEnumerable<WebContentDto>> actionResult = _permissionRepository.ActionPermission(viewModel.WebContents, viewModel.RoleId);
                if (actionResult.IsSuccess)
                {
                    return Json(actionResult);
                }
                else
                {
                    return actionResult.EnumResult == GenericOperationResult.ValidationError ? BadRequest() : (IActionResult)StatusCode(500);
                }
            }
            catch
            {
                return StatusCode(500);
            }

        }
        #endregion

        #region Get

        [HttpGet]
        [Route("[action]")]
        public IActionResult GetWebContentsForRole(int RoleId)
        {
            //var roleName = _userRepository.GetRole(RoleId);
            //var data = _permissionRepository.GetWebContents(RoleId);
            //var allData = _permissionRepository.GetAllWebContents();

            //var vm = new PermissionViewModel()
            //{
            //    RoleName = EnumHelper.GetEnumName<Role>((Role)Enum.Parse(typeof(Role), roleName)),
            //    RoleId = RoleId,
            //    WebContents = allData.Select(ent => new WebContentDto()
            //    {
            //        Id = ent.Id,
            //        WebContentRoles=ent.WebContentRoles,
            //        //ArName = EnumHelper.GetEnumName<ControllersNames>((ControllersNames)Enum.Parse(typeof(ControllersNames), ent.ArName))
            //    }),
            //    WebContentsId = data
            //};
            RoleDto result = _permissionRepository.GetWebContentsForRole(RoleId);
            return Json(result);
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult GetAllWebContent()
        {
            try
            {
                System.Collections.Generic.IEnumerable<WebContentDto> data = _permissionRepository.GetAllWebContents();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }

        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetPermissionsForWebContentId(int WebContentId)
        {
            try
            {
                RawPermissionsDto data = await _permissionRepository.GetPermissionsForWebContentId(WebContentId);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }

        }

        #endregion

        #region Remove

        [HttpDelete]
        [Route("[action]")]
        public async Task<IActionResult> RemovePermission(int id)
        {
            try
            {
                bool result = await _permissionRepository.RemovePermission(id);
                return result ? Ok() : BadRequest();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
            #endregion

        }
    }
}
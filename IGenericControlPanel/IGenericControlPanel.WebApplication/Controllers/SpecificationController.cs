﻿
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Specifiation;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class SpecificationController : LocalizedBaseController<SpecificationController>
    {
        private ISpecificationRepository SpecificationRepository { get; }
        public SpecificationController(ISpecificationRepository specificationRepository)
        {
            SpecificationRepository = specificationRepository;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetSpecifications()
        {
            var result = SpecificationRepository.GetAll();

            return result.EnumResult == GenericOperationResult.Success
                  ? Ok(result)
                  : (IActionResult)(result.EnumResult switch
                  {
                      GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                      GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                      GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                      _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                  });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetOneSpecification(int specificationId)
        {


            var result = SpecificationRepository.GetDetailed(specificationId);

            return result.EnumResult == GenericOperationResult.Success
                  ? Ok(result)
                  : (IActionResult)(result.EnumResult switch
                  {
                      GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                      GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                      GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                      _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                  });
        }

        [HttpDelete]
        //[DynamicAuthorization("Specification", nameof(ActionType.Remove))]
        [Route("[action]")]

        public IActionResult RemoveSpecification(int specificationId)
        {

            var result = SpecificationRepository.Remove(specificationId);

            return result.EnumResult == GenericOperationResult.Success
                  ? Ok(result)
                  : (IActionResult)(result.EnumResult switch
                  {
                      GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                      GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                      GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                      _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                  });
        }

        [HttpPost]
        //[DynamicAuthorization("Specification", nameof(ActionType.Action))]
        [Route("[action]")]
        public IActionResult SpecificationAction(SpecificationDto specificationDto)
        {


            var result = SpecificationRepository.Action(specificationDto);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                });
        }

    }
}

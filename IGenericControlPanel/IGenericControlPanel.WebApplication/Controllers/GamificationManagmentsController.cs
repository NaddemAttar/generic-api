﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.GamificationSystem.Dto.Gamification;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers;
[Route("api/[controller]")]
[ApiController]
public class GamificationManagmentsController : LocalizedBaseController<GamificationManagmentsController>
{
    #region Properties & Constructor
    private IGamificationManagmentsRepository _gamificationManagementsRepo { get; }
    public GamificationManagmentsController(
        IGamificationManagmentsRepository gamificationManagementsRepo,
        IStringLocalizer<GamificationManagmentsController> localizer) : base(localizer)
    {
        _gamificationManagementsRepo = gamificationManagementsRepo;
    }
    #endregion

    #region Get Section
    [HttpGet]
    [AllowAnonymous]
    [Route("[action]")]
    public async Task<IActionResult> GetAllGamificationSections()
    {
        var result = await _gamificationManagementsRepo.GetAllGamificationSectionsAsync();

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages]);

        return Ok(result);
    }
    #endregion

    #region Update Points For Gamifications Sections
    [HttpPut]
    [Route("[action]")]
    public async Task<IActionResult> UpdatePointsForGamificactionSections(
        IEnumerable<GamificationDto> gamifications)
    {
        var result = await _gamificationManagementsRepo.UpdatePointsForGamificactionSectionsAsync(gamifications);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages]);

        return Ok(result);
    }
    #endregion

    #region Add New Gamification Section
    [HttpPost]
    [Route("[action]")]
    public async Task<IActionResult> AddNewGamificationSection(GamificationDto gamificationDto)
    {
        var result = await _gamificationManagementsRepo.AddNewGamificationSectionAsync(gamificationDto);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages]);

        return Ok(result);
    }
    #endregion

    #region Get Users With Their Points
    [HttpGet]
    [Route("[action]")]
    public async Task<IActionResult> GetUsersWithTheirPoints(
        bool enablePagination, bool usersHavePointsOnly, bool isAscendingOrder,
        string query, int pageSize = 8, int pageNumber = 0)
    {
        var result = await _gamificationManagementsRepo.GetUsersWithTheirPointsAsync(
            new FiltersUsersDto
            {
                EnablePagination = enablePagination,
                IsAscendingOrder = isAscendingOrder,
                PageNumber = pageNumber,
                PageSize = pageSize,
                Query = query,
                UsersHavePointsOnly = usersHavePointsOnly
            });

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion
}

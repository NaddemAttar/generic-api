﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using IGenericControlPanel.Core.Dto.BlogComment;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using FileOptions = IGenericControlPanel.WebApplication.Util.FileOptions;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BlogCommentController : LocalizedBaseController<BlogCommentController>
    {
        private IBlogCommentRepository repository;
        private readonly IHttpFileService _httpFileService;
        private readonly IUserRepository userRepository;
        private readonly IStringLocalizer<BlogResource> blogLocalizer;
        private readonly IStringLocalizer<SharedResource> sharedLocalizer;
        private readonly IHttpFileService HttpFileService;
        private readonly IMapper _mapper;
        public BlogCommentController(IBlogCommentRepository repository, IHttpFileService httpFileService, IConfiguration configuration,
            IMapper mapper, IStringLocalizer<SharedResource> sharedLocalizer, IStringLocalizer<BlogResource> blogLocalizer, IUserRepository userRepository)
            : base(configuration)
        {
            this.repository = repository;
            _httpFileService = httpFileService;
            _mapper = mapper;
            this.sharedLocalizer = sharedLocalizer;
            this.blogLocalizer = blogLocalizer;
            this.userRepository = userRepository;
        }
        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public async Task<IActionResult> AddComment([FromForm] BlogCommentDto formDto)
        {
            if (formDto.Image != null)
            {
                var saveImaegInServiceFolder = await FilesExtensionMethods.SaveFile(new Util.FileOptions
                {
                    FileContentType = FileContentType.Image,
                    FileSourceType = FileSourceType.BlogComment,
                    FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                    File = formDto.Image,
                    HttpFileService = _httpFileService,
                    Mapper = _mapper,
                    SourceId = formDto.Id,
                    ControllerName = nameof(BlogCommentController)
                });

                if (saveImaegInServiceFolder is null)
                    throw new BadRequestException(sharedLocalizer[ErrorMessages.FailedSavingPhoto].Value);

                formDto.ImageUrl = saveImaegInServiceFolder.Url;
            }
            var result = await repository.AddComment(formDto);

            if (!result.IsSuccess)
                await FilesExtensionMethods.RemoveFileFromWWWRoot(formDto.ImageUrl, _httpFileService);

            return (result.EnumResult == GenericOperationResult.Success) ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(blogLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(blogLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }
        [HttpPut]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public async Task<IActionResult> UpdateComment([FromForm] BlogCommentDto formDto)
        {
            var userInfo = await userRepository.GetCurrentUserInfo();

            var blogComment = await repository.GetComment(formDto.Id);
            var oldCpmmentImage = blogComment.Result.ImageUrl;
            if (blogComment.EnumResult == GenericOperationResult.NotFound)
                throw new NotFoundException(blogLocalizer[ErrorMessages.BlogCommentNotFound].Value);

            if (blogComment.EnumResult == GenericOperationResult.Failed)
                throw new InternalServerException(sharedLocalizer[ErrorMessages.InternalServerError].Value);

            if (!(blogComment.Result.CreatedBy == userRepository.CurrentUserIdentityId() || userInfo.IsUserAdmin))
                throw new BadRequestException(sharedLocalizer[ErrorMessages.YouDoNotHavePermissionToEdit].Value);

            if (formDto.Image != null)
            {
                var saveImaegInServiceFolder = await FilesExtensionMethods.SaveFile(new FileOptions
                {
                    FileContentType = FileContentType.Image,
                    FileSourceType = FileSourceType.BlogComment,
                    FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                    File = formDto.Image,
                    HttpFileService = _httpFileService,
                    Mapper = _mapper,
                    SourceId = formDto.Id,
                    ControllerName = nameof(BlogCommentController)
                });

                if (saveImaegInServiceFolder is null)
                    throw new BadRequestException(sharedLocalizer[ErrorMessages.FailedSavingPhoto].Value);

                formDto.ImageUrl = saveImaegInServiceFolder.Url;
            }
            var result = await repository.UpdateComment(formDto);

            if (result.IsSuccess)
                await FilesExtensionMethods.RemoveFileFromWWWRoot(oldCpmmentImage, _httpFileService);

            if (!result.IsSuccess)
                await FilesExtensionMethods.RemoveFileFromWWWRoot(formDto.ImageUrl, HttpFileService);

            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(sharedLocalizer[ErrorMessages.InternalServerError].Value);

            return Ok(result);
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetBlogComments(int BlogId)
        {
            var result = await repository.GetBlogComments(BlogId);
            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(sharedLocalizer[ErrorMessages.InternalServerError].Value);

            return Ok(result);
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetUserComments(int UserId)
        {
            var result = await repository.GetUserComments(UserId);
            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(sharedLocalizer[ErrorMessages.InternalServerError].Value);

            return Ok(result);
        }
        [HttpDelete]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public async Task<IActionResult> RemoveComment(int CommentId)
        {
            var result = await repository.RemoveComment(CommentId);

            if (!string.IsNullOrEmpty(result.Result.ImageUrl))
                await FilesExtensionMethods.RemoveFileFromWWWRoot(result.Result.ImageUrl, _httpFileService);

            return result.EnumResult == GenericOperationResult.Success ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(blogLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(blogLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }
    }
}
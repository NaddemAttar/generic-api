﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Configuration.Dto.Pagination;
using IGenericControlPanel.GamificationSystem.Dto.Coupon;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers;
[Route("api/[controller]")]
[ApiController]
public class CouponManagementController :
    LocalizedBaseController<CouponManagementController>
{
    #region Constructor & Properties
    private ICouponManagementRepository _couponManagementRepository { get; }
    public CouponManagementController(
        ICouponManagementRepository couponManagementRepository,
        IStringLocalizer<CouponManagementController> localizer) : base(localizer)
    {
        _couponManagementRepository = couponManagementRepository;
    }
    #endregion

    #region Get Section
    [HttpGet]
    [AllowAnonymous]
    [Route("[action]")]
    public async Task<IActionResult> GetCoupons(
        bool enablePagination, int pageNumber, int pageSize, string query)
    {
        var result = await _couponManagementRepository.GetCouponsAsync(new PaginationDto
        {
            EnablePagination = enablePagination,
            PageNumber = pageNumber,
            PageSize = pageSize,
            Query = query
        });

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages]);

        return Ok(result);
    }

    [HttpGet]
    [AllowAnonymous]
    [Route("[action]")]
    public async Task<IActionResult> GetCouponDetails(int id)
    {
        var result = await _couponManagementRepository.GetCouponDetailsAsync(id);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages]);

        return Ok(result);
    }
    #endregion

    #region Remove Coupon
    [HttpDelete]
    [Route("[action]")]
    public async Task<IActionResult> RemoveCoupon(int id)
    {
        var result = await _couponManagementRepository.RemoveCouponAsync(id);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages]);

        return Ok(result);
    }
    #endregion

    #region Add New Coupon
    [HttpPost]
    [Route("[action]")]
    public async Task<IActionResult> AddCoupon(CouponDto createNewCoupon)
    {
        var result = await _couponManagementRepository.AddCouponAsync(createNewCoupon);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages]);

        return Ok(result);
    }
    #endregion

    #region Update Coupon
    [HttpPut]
    [Route("[action]")]
    public async Task<IActionResult> UpdateCoupon(CouponDto updateCouponDto)
    {
        var result = await _couponManagementRepository.UpdateCouponAsync(updateCouponDto);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages]);

        return Ok(result);
    }
    #endregion
}

﻿
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.ContactInfo;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class ContactInfoController : LocalizedBaseController<ContactInfoController>
    {
        public IContactInfoRepository ContactInfoRepository { get; set; }
        public ContactInfoController(IContactInfoRepository contactInfoRepository)
        {
            ContactInfoRepository = contactInfoRepository;
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetContactInfo()
        {
            var result = ContactInfoRepository.GetContactInfo();
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        [HttpPost]
        //[DynamicAuthorization("ContactInfo", nameof(ActionType.Action))]
        [Route("[action]")]
        public IActionResult ActionContacInfo(ContactInfoDto viewModel)
        {
            OperationResult<GenericOperationResult, ContactInfoDto> result = ContactInfoRepository.ActionContactInfo(viewModel);
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
    }
}

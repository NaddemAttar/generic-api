﻿
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Communications.Dto.Feedback;
using IGenericControlPanel.Communications.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Util;
using IGenericControlPanel.WebApplication.ViewModels.Feedback;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class FeedbackController : LocalizedBaseController<FeedbackController>
    {
        #region Properties & Constructor
        private IFeedbackRepository FeedbackRepository { get; }
        private IHttpFileService HttpFileService { get; }
        public FeedbackController(
            IConfiguration configuration,
            IFeedbackRepository _feedBackrepository,
            IHttpFileService _httpFileService) : base(configuration)
        {
            FeedbackRepository = _feedBackrepository;
            HttpFileService = _httpFileService;
        }
        #endregion

        #region Get Section
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetFeedback(int FeedbackId)
        {
            var result = FeedbackRepository.GetFeedback(FeedbackId);
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetFeedbacks(int ServiceId, int pageSize = 6, int pageNumber = 0)
        {
            var result = await FeedbackRepository.GetFeedbacks(ServiceId, pageSize, pageNumber);
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion

        #region Action Section
        [HttpPost]
        //[DynamicAuthorization("Feedback", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionFeedback([FromForm] FeedbackViewModel formViewModel)
        {
            FeedbackDto formDto = new()
            {
                ClientName = formViewModel.ClientName,
                ServiceId = formViewModel.ServiceId,
                Description = formViewModel.Description,
                Title = formViewModel.Title,
                YouTubeLink = formViewModel.YouTubeLink,
                Id = formViewModel.Id
            };
            if (formViewModel.Image != null)
            {

                var fileDto =
                        await FilesExtensionMethods.SavingFileInWWWRootFolder(formViewModel.Image,
                        Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath), HttpFileService);

                if (fileDto.IsSuccess)
                {
                    formDto.ImageUrl = fileDto.Result.Url;
                }
                else
                {
                    return BadRequest("couldn't save the image try again later");
                }
            }

            var result = await FeedbackRepository.ActionFeedback(formDto);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion

        #region Remove Section
        [HttpDelete]
        //[DynamicAuthorization("Feedback", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveFeedback(int FeedbackId)
        {
            var result = await FeedbackRepository.RemoveFeedback(FeedbackId);
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion
    }
}

﻿
using Microsoft.AspNetCore.Mvc;

namespace IGenericControlPanel.WebApplication.Controllers
{
    public class ForgetPasswordController : Controller
    {
        public IActionResult ForgetPassword()
        {
            return View();
        }
        public IActionResult ForgetPasswordConfirmation()
        {
            return View();
        }
    }
}

﻿
using AutoMapper;

using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Content.IData;
using IGenericControlPanel.Core.Dto.JobOffer;
using IGenericControlPanel.Core.Dto.Pagination;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Enums.Core;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Util;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class JobOfferController : LocalizedBaseController<JobOfferController>
    {
        #region Properties & Constructor
        private readonly IJobOfferRepository JobOfferRepository;
        private IHttpFileService HttpFileService { get; }
        private IFileRepository FileRepository { get; }
        private IMapper Mapper { get; }
        public JobOfferController(IJobOfferRepository JobOfferRepository, IHttpFileService httpFileService,
            IFileRepository fileRepository,
            IStringLocalizer<JobOfferController> stringLocalizer,
            IMapper mapper,
            IConfiguration configuration
            ) : base(configuration)
        {
            this.JobOfferRepository = JobOfferRepository;
            HttpFileService = httpFileService;
            FileRepository = fileRepository;
            Mapper = mapper;
        }
        #endregion

        #region Change Job Status
        [HttpPost]
        //[DynamicAuthorization("JobOffer", nameof(ActionType.Action))]
        [Route("[action]")]
        public IActionResult ChangeJobStatus(int JobApplicationId, JobStatus status)
        {
            var result = JobOfferRepository.ChangeJobStatus(JobApplicationId, status);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion

        #region Apply to Job 
        [HttpPost]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> ApplyToJob([FromForm] JobApplicationsDto jobApplicationsDto)
        {
            if (!ModelState.IsValid || jobApplicationsDto is null)
            {
                return BadRequest(jobApplicationsDto);
            }
            if (jobApplicationsDto.AttachedCv != null)
            {
                try
                {
                    var fileToAdd = await FilesExtensionMethods
                              .SaveFile(new Util.FileOptions
                              {
                                  FileContentType = FileContentType.Attachement,
                                  FileSourceType = FileSourceType.JobOffer,
                                  FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                                  File = jobApplicationsDto.AttachedCv,
                                  HttpFileService = HttpFileService,
                                  Mapper = Mapper,
                                  ControllerName = nameof(Model.Core.JobOffer)
                              });

                    jobApplicationsDto.CvFile = fileToAdd;
                }
                catch (Exception)
                {
                    return GetFailureAjaxResult(FromResx[LocalizerStrings.OperationTitle_Failure_Try_Again],
                        FromResx[LocalizerStrings.OperationMessage_Failure_Try_Again]);
                }

            }

            var resultPrimary = JobOfferRepository.ApplyToJobOffer(jobApplicationsDto);

            if (resultPrimary.IsSuccess)
            {
                _ = HttpFileService.RemoveFile(resultPrimary.Result.RemovedFilesUrls);
                return Json(jobApplicationsDto);
            }

            return StatusCode(500);
        }
        #endregion

        #region Job Offer Action
        [HttpPost]
        //[DynamicAuthorization("JobOffer", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionJobOffer([FromForm] JobOfferFormDto jobOfferFormDto)
        {
            if (jobOfferFormDto.AttachedFiles.Count != 0)
            {
                var saveImaegInServiceFolder = await FilesExtensionMethods.SaveFiles(new Util.FileOptions
                {
                    FileContentType = FileContentType.Image,
                    FileSourceType = FileSourceType.Allergy,
                    FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                    Files = jobOfferFormDto.AttachedFiles,
                    HttpFileService = HttpFileService,
                    Mapper = Mapper,
                    SourceId = jobOfferFormDto.Id,
                    ControllerName = nameof(Model.Core.Allergy)
                });

                if (saveImaegInServiceFolder is null)
                {
                    throw new BadRequestException(FromResx["FilesError"].Value);
                }

                jobOfferFormDto.Attachments = saveImaegInServiceFolder;
            }

            var result = JobOfferRepository.Action(jobOfferFormDto);

            if (result.EnumResult == GenericOperationResult.Success)
            {
                if (result.Result.RemovedFilesUrls != null)
                {
                    _ = HttpFileService.RemoveFiles(result.Result.RemovedFilesUrls);
                }

                return Ok(result);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                _ => throw new Exception(FromResx["InternalServerError"].Value)
            };
        }
        #endregion

        #region Get
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetJobOffer(int Id)
        {
            var result = JobOfferRepository.GetJobOffer(Id);

            return result.EnumResult == GenericOperationResult.Success
                 ? Ok(result)
                 : (IActionResult)(result.EnumResult switch
                 {
                     GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                     GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                     GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                     _ => throw new Exception(FromResx["InternalServerError"].Value)
                 });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetAllJobOffers(
            string query, int? CityId, int? CountryId, ContractType? ContractType, PlaceOfWork? placeOfWork,
            double startSalary, double endSalary, int pageNumber = 0, int pageSize = 5, bool enablePagination = false)
        {
            var result = await JobOfferRepository.GetAllJobOffers(new JobOfferOptions
            {
                Query = query,
                CityId = CityId,
                CountryId = CountryId,
                ContractType = ContractType,
                PlaceOfWork = placeOfWork,
                StartSalary = startSalary,
                EndSalary = endSalary,
                PaginationOptions = new PaginationOptions
                {
                    PageNumber = pageNumber,
                    PageSize = pageSize,
                    EnablePagination = enablePagination
                }
            });

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion

        #region Remove
        [HttpDelete]
        //[DynamicAuthorization("JobOffer", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveJobOffer(int Id)
        {
            var result = await JobOfferRepository.RemoveJobOffer(Id);
            return result.EnumResult == GenericOperationResult.Success
                  ? Ok(result)
                  : (IActionResult)(result.EnumResult switch
                  {
                      GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                      GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                      GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                      _ => throw new Exception(FromResx["InternalServerError"].Value)
                  });
        }
        #endregion
    }
}

﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Consultation.Dto.ConsultationDto;
using IGenericControlPanel.Consultation.IData.Interfaces;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Util;

namespace IGenericControlPanel.WebApplication.Controllers;
[Route("api/[controller]")]
[ApiController]
public class PatientConsultationController : LocalizedBaseController<PatientConsultationController>
{
    #region Properties & Constructor
    private IUserConsultationRepository UserConsultationRepository { get; }
    private readonly IStringLocalizer<ConsultationResource> consultationLocalizer;
    private readonly IStringLocalizer<SharedResource> sharedLocalizer;
    public PatientConsultationController(
        IStringLocalizer<PatientConsultationController> localizer,
        IUserConsultationRepository userConsultationRepository,
        IStringLocalizer<ConsultationResource> consultationLocalizer,
        IStringLocalizer<SharedResource> sharedLocalizer) : base(localizer)
    {
        UserConsultationRepository = userConsultationRepository;
        this.consultationLocalizer = consultationLocalizer;
        this.sharedLocalizer = sharedLocalizer;
    }
    #endregion

    #region Consultation Order
    [HttpPost]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("[action]")]
    public async Task<IActionResult> ConsultationOrder(
        IEnumerable<UserConsultationFormDto> userConsultationFormDtos,
        int healthCareProvider, string caseDescription)
    {
        var result = await UserConsultationRepository
            .ConsultationOrderAsync(userConsultationFormDtos, healthCareProvider,caseDescription);

        return result.EnumResult == GenericOperationResult.Success
            ? Ok(result)
            : (IActionResult)(result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(consultationLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(consultationLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            });
    }
    #endregion

    #region Get Consultation Answers

    [HttpGet]
    [Route("[action]")]
    public async Task<IActionResult> GetConsultationAnswers(int healthProviderId = 0)
    {
        var result = await UserConsultationRepository.GetConsultationAnswersAsync(healthProviderId);

        return result.EnumResult == GenericOperationResult.Success
            ? Ok(result)
            : (IActionResult)(result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(consultationLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(consultationLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            });
    }

    [HttpGet]
    [Route("[action]")]
    public async Task<IActionResult> GetConsultationAnswersByHcpId(int healthProviderId)
    {
        var result = await UserConsultationRepository
            .GetConsultationAnswersByHcpIdAsync(healthProviderId);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages]);

        return Ok(result);
    }
    #endregion

    #region Get Health Provider Questions For Patient

    [HttpGet]
    [AllowAnonymous]
    [Route("[action]")]
    public async Task<IActionResult> GetHealthProviderQuestionsForPatient(int healthProviderId)
    {
        var result = await UserConsultationRepository.GetHealthProviderQuestionsByIdAsync(healthProviderId);

        return result.EnumResult == GenericOperationResult.Success
            ? Ok(result)
            : (IActionResult)(result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(consultationLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(consultationLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            });
    }
    #endregion
    #region Get Total Consultation For Patient
    [HttpGet]
    [Route("[action]")]
    public async Task<IActionResult> GetTotalConsultationForPatient()
    {
        var result = await UserConsultationRepository.GetTotalConsultationForPatientAsync();

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages]);

        return Ok(result);
    }
    #endregion
}
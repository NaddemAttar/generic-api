﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Communications.Dto.TeleHealth;
using IGenericControlPanel.Communications.IData.Interfaces;
using IGenericControlPanel.Core.Dto.HealthCareProvider;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Util;
using IGenericControlPanel.WebApplication.ViewModels.TeleHealth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Localization;
using IGenericControlPanel.WebApplication.Middleware;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class TeleHealthController : LocalizedBaseController<TeleHealthController>
    {
        #region Constructor & Properties
        private ITeleHealthRpository TeleHealthRpository { get; }
        private readonly IMailRepository mailService;
        private readonly IConfiguration configuration;
        private readonly IStringLocalizer<SharedResource> sharedLocalizer;
        private readonly IStringLocalizer<TeleHealthResource> teleHealthLocalizer;
        public TeleHealthController(
            ITeleHealthRpository teleHealthRpository,
            IDistributedCache cache,
            IUserRepository userRepository,
            IMailRepository mailService,
            IConfiguration configuration,
            IStringLocalizer<SharedResource> sharedLocalizer,
            IStringLocalizer<TeleHealthResource> teleHealthLocalizer) : base(cache, userRepository)
        {
            TeleHealthRpository = teleHealthRpository;
            this.mailService = mailService;
            this.configuration = configuration;
            this.sharedLocalizer = sharedLocalizer;
            this.teleHealthLocalizer = teleHealthLocalizer;
        }

        #endregion

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetSessionTypes(int? serviceId, int? HealthCareProviderId)
        {
            var userInfo = await _userRepository.GetCurrentUserInfo();
            string key = $"{CacheKeys.GetSessionTypes}_{serviceId}";

            string recordKey = key.GetKey(userInfo.IsHealthCareProvider, userInfo.CurrentUserId);

            IEnumerable<SessionTypeDto> data = await _cache.GetRecordAsync<IEnumerable<SessionTypeDto>>(recordKey);
            OperationResult<GenericOperationResult, IEnumerable<SessionTypeDto>> result;
            if (data is null)
            {
                result = await TeleHealthRpository.GetHealtCareProviderSessionTypesAsync(serviceId, HealthCareProviderId);

                await _cache.SetRecordAsync(recordKey, result.Result, result.IEnumerableDataIsValid(), TimeSpan.FromSeconds(10));
            }
            else
            {
                result = new();
                result.Result = data;
            }

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(teleHealthLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(teleHealthLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetPublicSessionTypes()
        {
            var userInfo = await _userRepository.GetCurrentUserInfo();
            string key = $"{CacheKeys.GetSessionTypes}";

            string recordKey = key.GetKey(userInfo.IsHealthCareProvider, userInfo.CurrentUserId);

            IEnumerable<SessionTypeDto> data = await _cache.GetRecordAsync<IEnumerable<SessionTypeDto>>(recordKey);
            OperationResult<GenericOperationResult, IEnumerable<SessionTypeDto>> result;
            if (data is null)
            {
                result = await TeleHealthRpository.GetHealtCareProviderSessionTypesAsync();

                await _cache.SetRecordAsync(recordKey, result.Result, result.IEnumerableDataIsValid());
            }
            else
            {
                result = new();
                result.Result = data;
            }

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(teleHealthLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(teleHealthLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetAvailableAppointments(
            DateTime Date, int SessionTypeId, int? HeathCareProviderId, int? EnterpriseId)
        {
            var result = await TeleHealthRpository
                .GetAvailableAppointmentsAsync(
                Date, SessionTypeId, HeathCareProviderId, EnterpriseId);

            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

            return Ok(result);
        }

        [HttpGet]
        //[DynamicAuthorization("TeleHealth", nameof(ActionType.Get))]
        [Route("[action]")]
        public async Task<IActionResult> GetBookingSettings()
        {
            BookingSettingsDto data = null;
            string recordKey = $"{CacheKeys.GetBookingSettings}";

            OperationResult<GenericOperationResult, BookingSettingsDto> result;
            if (data is null)
            {
                result = await TeleHealthRpository.GetbookingSettingsAsync();

                await _cache.SetRecordAsync(recordKey, result, result.DataIsValid());
            }
            else
            {
                result = new();
                result.Result = data;
            }

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(teleHealthLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(teleHealthLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }

        [HttpGet]
        //[DynamicAuthorization("TeleHealth", nameof(ActionType.Get))]
        [Route("[action]")]
        public async Task<IActionResult> GetBookings(bool MyBookings = false)
        {
            var userInfo = await _userRepository.GetCurrentUserInfo();
            string key = $"{CacheKeys.GetBookings}";

            string recordKey = key.GetKey(userInfo.IsHealthCareProvider, userInfo.CurrentUserId);

            IEnumerable<BookingDto> data = await _cache.GetRecordAsync<IEnumerable<BookingDto>>(recordKey);
            OperationResult<GenericOperationResult, IEnumerable<BookingDto>> result;
            if (data is null)
            {
                result = await TeleHealthRpository.GetBookingsAsync(MyBookings);
                await _cache.SetRecordAsync(recordKey, result.Result, result.DataIsValid());
            }
            else
            {
                result = new();
                result.Result = data;
            }

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(teleHealthLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(teleHealthLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }

        [HttpPost]
        //[DynamicAuthorization("TeleHealth", nameof(ActionType.Action))]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> ActionBookingSetting(BookingSettingViewModel viewModel)
        {
            await _cache.RemoveRecordAsync($"{CacheKeys.GetBookingSettings}");

            BookingSettingsDto formdto = new()
            {
                Id = viewModel.Id,
                StartOfPatientReception = viewModel.StartOfPatientReception,
                EndOfPatientReception = viewModel.EndOfPatientReception
            };

            var result = await TeleHealthRpository.SetHealthcareProviderBookingInfoAsync(formdto);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(teleHealthLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(teleHealthLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }

        [HttpPost]
        //[DynamicAuthorization("TeleHealth", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionSessionTypes(SessionTypeViewModel viewModel)
        {
            SessionTypeDto formdto = new()
            {
                SessionTypeId = viewModel.Id,
                Duration = viewModel.Duration,
                Name = viewModel.Name,
                Price = viewModel.Price,
                //ServiceId = viewModel.ServiceId
            };

            var result = await TeleHealthRpository.SetHealthCareProviderSessionTypeAsync(formdto);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(teleHealthLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(teleHealthLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }

        [HttpPost]
        //[DynamicAuthorization("TeleHealth", nameof(ActionType.Action))]
        [Route("[action]")]
        [AllowAnonymous]
        public async Task<IActionResult> ActionBooking(BookingDto BookingDto)
        {
            var userInfo = await _userRepository.GetCurrentUserInfo();

            if (userInfo.CurrentUserId == 0)
                BookingDto.IsConfirm = false;
            else
                BookingDto.IsConfirm = true;

            var recordKey = CacheKeys.GetBookings.GetKey(
                userInfo.IsHealthCareProvider, userInfo.CurrentUserId);

            await _cache.RemoveRecordAsync(recordKey);

            var token = SharedKernal.Extension.ExtensionMethods.GenerateUniqueCode(7);
            BookingDto.Token = token;
            var result = await TeleHealthRpository.BookAppointmentAsync(
                BookingDto);
            if (userInfo.CurrentUserId == 0 && result.EnumResult == GenericOperationResult.Success)
            {
                var sendEmail = new Model.Core.MailRequest();
                sendEmail.ToEmails = new List<string>();
                sendEmail.ToEmails.Add(BookingDto.Email);
                sendEmail.Subject = "Confrirming the booking";
                sendEmail.Body =
                    "<p>to confirm your booking please click here</p>" +
                    $"<a href = '{configuration.GetSection("BaseDomain").Value}+/HandleBooking?BookingId={BookingDto.Id}&Token={token}&type=1'><button>Click here to confirm</button></a> <br/> <br/> <br/>" +
                    "<p>it's not your booking?</p>" +
                    $"<a href = '{configuration.GetSection("BaseDomain").Value}+/HandleBooking?BookingId={BookingDto.Id}&Token={token}&type=0'><button>Click here to remove the booking</button></a>";
                await mailService.SendEmailAsync(sendEmail);
            }
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(teleHealthLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(teleHealthLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }

        [HttpGet]
        //[DynamicAuthorization("TeleHealth", nameof(ActionType.Action))]
        [Route("[action]")]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmingBooking(int BookingId, string Token)
        {
            var result = await TeleHealthRpository.ConfirmingBookingAsync(BookingId, Token);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(teleHealthLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(teleHealthLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }

        [HttpDelete]
        //[DynamicAuthorization("TeleHealth", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveSession(int SessionId)
        {
            var result = await TeleHealthRpository.RemoveSessionAsync(SessionId);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(teleHealthLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(teleHealthLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }

        [HttpDelete]
        //[DynamicAuthorization("TeleHealth", nameof(ActionType.Remove))]
        [Route("[action]")]
        [AllowAnonymous]
        public async Task<IActionResult> RemoveBooking(int BookingId, string Token)
        {
            var result = await TeleHealthRpository.RemoveBookingAsync(BookingId, Token);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(teleHealthLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(teleHealthLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> AddServicesForHcpInEnterprise(int hcpId , List<AddServicesForHcpDto> services)
        {
            var result = await TeleHealthRpository.AddServicesForHcpInEnterpriseAsync(hcpId, services);

            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

            return Ok(result);
        }
    }
}
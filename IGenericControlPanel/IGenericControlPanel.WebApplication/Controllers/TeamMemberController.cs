﻿
using AutoMapper;

using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Content.IData;
using IGenericControlPanel.HR.Dto;
using IGenericControlPanel.HR.Dto.TeamMember;
using IGenericControlPanel.HR.IData;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Util;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class TeamMemberController : LocalizedBaseController<TeamMemberController>
    {
        #region Properties & Constructor
        private ITeamMemberRepository TeamMemberRepository { get; }
        private IHttpFileService HttpFileService { get; }
        private IFileRepository FileRepository { get; }
        private IMapper Mapper { get; }
        public TeamMemberController(ITeamMemberRepository teamMemberRepository,
            ISettingRepository settingRepository,
            IUserLanguageRepository userLanguageRepository,
            IHttpFileService httpFileService,
            IMapper mapper,
            IFileRepository fileRepository,
            IStringLocalizer<TeamMemberController> stringLocalizer,
            IConfiguration configuration,
            IDistributedCache cache,
            IUserRepository userRepository) :
            base(
                configuration,
                userLanguageRepository,
                settingRepository,
                stringLocalizer,
                cache,
                userRepository
                )
        {
            TeamMemberRepository = teamMemberRepository;
            HttpFileService = httpFileService;
            FileRepository = fileRepository;
            Mapper = mapper;
        }

        #endregion

        #region REMOVE

        [HttpDelete]
        //[DynamicAuthorization("TeamMember", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveTeamMember(int id)
        {
            var userInfo = await _userRepository.GetCurrentUserInfo();

            string recordKey = CacheKeys.GetTeamMembers
                .GetKey(userInfo.IsHealthCareProvider,
                userInfo.CurrentUserId);

            await _cache.RemoveRecordAsync(recordKey);

            if (id != 0)
            {
                await _cache.RemoveRecordAsync(
                    $"{CacheKeys.GetOneTeamMember}_{id}");
            }

            var result = await TeamMemberRepository.RemoveTeamMemberAsync(id);
            if (result.EnumResult == GenericOperationResult.Success)
            {
                if (result.Result != null)
                {
                    _ = HttpFileService.RemoveFile(result.Result);
                }
                return Ok(result);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                _ => throw new Exception(FromResx[result.ErrorMessages].Value)
            };
        }

        #endregion

        #region GET METHOD
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetTeamMembers(
            int pageNumber = 0, int pageSize = 5)
        {
            var userInfo = await _userRepository.GetCurrentUserInfo();
            IEnumerable<TeamMemberDto> data = null;
            OperationResult<GenericOperationResult,
                IEnumerable<TeamMemberDto>> result = null;

            string recordKey = CacheKeys.GetTeamMembers
                .GetKeyWithPagesAsync(userInfo.IsHealthCareProvider, userInfo.CurrentUserId,
                pageNumber, pageSize);

            data = await _cache.GetRecordAsync<IEnumerable<TeamMemberDto>>(recordKey);

            if (data is null)
            {
                result = await TeamMemberRepository
               .GetTeamMembersAsync(pageNumber, pageSize);

                await _cache.SetRecordAsync(
                    recordKey, result.Result, result.IEnumerableDataIsValid(),
                    TimeSpan.FromSeconds(30));
            }
            else
            {
                result = new();
                result.Result = data;
            }

            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

            return Ok(result);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetAllTeamMembers()
        {
            var userInfo = await _userRepository.GetCurrentUserInfo();

            IEnumerable<InstructorDto> data = null;
            OperationResult<GenericOperationResult,
                IEnumerable<InstructorDto>> result = null;
            string recordKey = CacheKeys.GetTeamMembers
               .GetKey(userInfo.IsHealthCareProvider, userInfo.CurrentUserId);

            data = await _cache.GetRecordAsync<IEnumerable<InstructorDto>>(recordKey);

            if (data is null)
            {
                result = await TeamMemberRepository.GetAllTeamMembersAsync();

                await _cache.SetRecordAsync(recordKey, result.Result,
                    result.IEnumerableDataIsValid());
            }

            else
            {
                result = new();
                result.Result = data;
            }

            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

            return Ok(result);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetTeamMemberById(int TeamMemberId)
        {
            TeamMemberDto data = null;
            OperationResult<GenericOperationResult,
                TeamMemberDto> result = null;
            string recordKey = $"{CacheKeys.GetOneTeamMember}_{TeamMemberId}";

            data = await _cache.GetRecordAsync<TeamMemberDto>(recordKey);

            if (data is null)
            {
                result = await TeamMemberRepository.GetTeamMemberByIdAsync(TeamMemberId);

                await _cache.SetRecordAsync(recordKey, result.Result,
                    result.DataIsValid());
            }

            else
            {
                result = new();
                result.Result = data;
            }

            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

            return Ok(result);
        }

        #endregion

        #region POST ACTION METHOD

        [HttpPost]
        //[DynamicAuthorization("TeamMember", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionTeamMember(
            [FromForm] TeamMemberFormDto formDto)
        {
            var userInfo = await _userRepository.GetCurrentUserInfo();
            string recordKey = CacheKeys.GetTeamMembers.GetKey(
                userInfo.IsHealthCareProvider, userInfo.CurrentUserId);

            await _cache.RemoveRecordAsync(recordKey);

            if (formDto.Id != 0)
            {
                await _cache.RemoveRecordAsync(
                    $"{CacheKeys.GetOneTeamMember}_{formDto.Id}");
            }

            if (formDto.ImageFormFile != null)
            {
                var saveFile = await FilesExtensionMethods
                  .SaveFile(new Util.FileOptions
                  {
                      FileContentType = FileContentType.Attachement,
                      FileSourceType = FileSourceType.TeamMember,
                      FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                      File = formDto.ImageFormFile,
                      HttpFileService = HttpFileService,
                      Mapper = Mapper,
                      SourceId = formDto.Id,
                      ControllerName = nameof(Model.TeamMember)
                  });

                if (saveFile != null)
                {
                    formDto.ImageDetails = saveFile;
                }
            }
            var result = await TeamMemberRepository.ActionTeamMemberAsync(formDto);
            if (result.EnumResult == GenericOperationResult.Success)
            {
                formDto.ImageDetails = null;
                formDto.ImageFormFile = null;
                return Ok(result);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                _ => throw new Exception(FromResx[result.ErrorMessages].Value)
            };
        }
        #endregion
    }
}

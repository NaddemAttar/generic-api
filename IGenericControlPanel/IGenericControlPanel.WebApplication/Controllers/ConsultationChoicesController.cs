﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Consultation.Dto.ConsultationDto;
using IGenericControlPanel.Consultation.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers;
[Route("api/[controller]")]
[ApiController]
public class ConsultationChoicesController : LocalizedBaseController<ConsultationChoicesController>
{
    #region Constructor & Properties
    private IConsultationChoicesRepository ConsultationChoicesRepository { get; }
    private readonly IStringLocalizer<ConsultationResource> consultationLocalizer;
    private readonly IStringLocalizer<SharedResource> sharedLocalizer;
    public ConsultationChoicesController(
        IStringLocalizer<ConsultationChoicesController> localizer,
        IConsultationChoicesRepository consultationRepository,
        IStringLocalizer<ConsultationResource> consultationChoicesLocalizer,
        IStringLocalizer<SharedResource> sharedLocalizer) : base(localizer)
    {
        ConsultationChoicesRepository = consultationRepository;
        this.consultationLocalizer = consultationChoicesLocalizer;
        this.sharedLocalizer = sharedLocalizer;
    }
    #endregion 

    #region Get Section

    [HttpGet]
    [AllowAnonymous]
    [Route("[action]")]
    public async Task<IActionResult> GetConsultationChoices(bool enablePagination, string query, int pageNumber = 0, int pageSize = 8)
    {
        var result = await ConsultationChoicesRepository.GetChoicesAsync(enablePagination, pageNumber, pageSize, query);

        return result.EnumResult == GenericOperationResult.Success
            ? Ok(result)
            : (IActionResult)(result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(consultationLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(consultationLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            });
    }

    #endregion 

    #region Action Section
    [HttpPost]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("[action]")]
    public async Task<IActionResult> Action(ConsultationChoicesDto dto)
    {
        var result = await ConsultationChoicesRepository.ActionAsync(dto);

        return result.EnumResult == GenericOperationResult.Success
            ? Ok(result)
            : (IActionResult)(result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(consultationLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(consultationLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            });
    }
    #endregion

    #region Remove Section
    [HttpDelete]
    //[Authorize(AuthenticationSchemes = "Bearer")]
    [Route("[action]")]
    public async Task<IActionResult> Remove(int id)
    {
        var result = await ConsultationChoicesRepository.RemoveAsync(id);

        return result.EnumResult == GenericOperationResult.Success
            ? Ok(result)
            : (IActionResult)(result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(consultationLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(consultationLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
            });
    }
    #endregion
}
﻿
using AutoMapper;

using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Util;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class MedicalTestsController : LocalizedBaseController<MedicalTestsController>
    {
        #region Properties & Constructor
        private IMedicalTestsRepository MedicalTestsRepository { get; }
        private IHttpFileService HttpFileService { get; }
        private IMapper Mapper { get; }
        public MedicalTestsController(
            IMedicalTestsRepository medicalTestsRepository,
            IHttpFileService httpFileService,
            IMapper mapper,
            IConfiguration configuration,
            IStringLocalizer<MedicalTestsController> localizer) : base(localizer, configuration)
        {
            MedicalTestsRepository = medicalTestsRepository;
            HttpFileService = httpFileService;
            Mapper = mapper;
        }
        #endregion

        #region Action
        [HttpPost]
        //[DynamicAuthorization("MedicalTests", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> Action([FromForm] MedicalTestsDto formDto)
        {
            if (formDto.ImagesForm.Count != 0)
            {
                var saveImaegInServiceFolder = await FilesExtensionMethods
                  .SaveFiles(new Util.FileOptions
                  {
                      FileContentType = FileContentType.Image,
                      FileSourceType = FileSourceType.MedicalTests,
                      FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                      Files = formDto.ImagesForm,
                      HttpFileService = HttpFileService,
                      Mapper = Mapper,
                      SourceId = formDto.Id,
                      ControllerName = nameof(Model.Core.MedicalTests)
                  });

                if (saveImaegInServiceFolder is null)
                {
                    throw new BadRequestException(FromResx["FilesError"].Value);
                }

                formDto.Images = saveImaegInServiceFolder;
            }

            var result = MedicalTestsRepository.Action(formDto);

            if (result.EnumResult == GenericOperationResult.Success)
            {
                if (result.Result.RemovedFilesUrls != null)
                {
                    _ = HttpFileService.RemoveFiles(result.Result.RemovedFilesUrls);
                }
                return Ok(result);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                _ => throw new Exception(FromResx["InternalServerError"].Value)
            };
        }
        #endregion

        #region Get Section
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetAllUserMedicalTests(
            int? userId, bool enablePagination, int pageSize = 8, int pageNumber = 0)
        {
            var result = await MedicalTestsRepository.GetAllUserMedicalTests(
                userId, enablePagination, pageSize, pageNumber);
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetMedicalTest(int Id)
        {
            var result = MedicalTestsRepository.GetMedicalTest(Id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetAllMedicalTests()
        {
            var result = MedicalTestsRepository.GetAllMedicalTests();

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion

        #region Remove Section
        [HttpDelete]
        //[DynamicAuthorization("MedicalTests", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveMedicalTest(int Id)
        {
            var result = await MedicalTestsRepository.RemoveMedicalTest(Id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion
    }
}

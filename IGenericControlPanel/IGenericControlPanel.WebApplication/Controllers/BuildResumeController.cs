﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Security.IData;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.UserExperiences.Dto.Resume;
using IGenericControlPanel.UserExperiences.IData.Interfaces;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers;
[Route("api/[controller]")]
[ApiController]
public class BuildResumeController : LocalizedBaseController<BuildResumeController>
{
    private readonly IExperienceRepository _experienceRepository;
    private readonly IEducationRepository _educationRepository;
    private readonly ICertificateRepository _certificateRepository;
    private readonly ILectureRepository _lectureRepository;
    private readonly ILicenseRepository _licenseRepository;
    private readonly IAccountRepository _accountRepository;
    public BuildResumeController(
        IStringLocalizer<BuildResumeController> localizer,
        IConfiguration configuration,
        IExperienceRepository experienceRepository,
        IEducationRepository educationRepository,
        ICertificateRepository certificateRepository,
        ILectureRepository lectureRepository,
        ILicenseRepository licenseRepository,
        IAccountRepository accountRepository) : base(localizer, configuration)
    {
        _experienceRepository = experienceRepository;
        _educationRepository = educationRepository;
        _certificateRepository = certificateRepository;
        _lectureRepository = lectureRepository;
        _licenseRepository = licenseRepository;
        _accountRepository = accountRepository;
    }

    [HttpGet]
    [Route("[action]")]
    public async Task<IActionResult> GetResumeForHcpUser(int hcpId = 0)
    {
        var expResult = await _experienceRepository.GetExperienceDataForResumeAsync(hcpId);
        if (expResult.EnumResult != GenericOperationResult.Success)
            expResult.EnumResult.GenericThrowExceptionError(FromResx[expResult.ErrorMessages].Value);

        var eduResult = await _educationRepository.GetEducationDataForResumeAsync(hcpId);
        if (eduResult.EnumResult != GenericOperationResult.Success)
            eduResult.EnumResult.GenericThrowExceptionError(FromResx[eduResult.ErrorMessages].Value);

        var cerResult = await _certificateRepository.GetCertificateDataForResumeAsync(hcpId);
        if (cerResult.EnumResult != GenericOperationResult.Success)
            cerResult.EnumResult.GenericThrowExceptionError(FromResx[cerResult.ErrorMessages].Value);

        var lecResult = await _lectureRepository.GetLectureDataForResumeAsync(hcpId);
        if (lecResult.EnumResult != GenericOperationResult.Success)
            lecResult.EnumResult.GenericThrowExceptionError(FromResx[lecResult.ErrorMessages].Value);

        var licResult = await _licenseRepository.GetLicenseDataForResumeAsync(hcpId);
        if (licResult.EnumResult != GenericOperationResult.Success)
            licResult.EnumResult.GenericThrowExceptionError(FromResx[licResult.ErrorMessages].Value);

        var accountResult = await _accountRepository.GetprofileInfo(hcpId);
        if (accountResult.EnumResult != GenericOperationResult.Success)
            accountResult.EnumResult.GenericThrowExceptionError(FromResx[accountResult.ErrorMessages].Value);

        OperationResult<GenericOperationResult,
            ResumeDto> result = new(GenericOperationResult.Success);

        result.Result = new ResumeDto
        {
            ProfileInfo = accountResult.Result,
            Experiences = expResult.Result,
            Educations = eduResult.Result,
            Certificates = cerResult.Result,
            Lectures = lecResult.Result,
            Licenses = licResult.Result
        };

        return Ok(result);
    }

}

﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Content.Dto.Content;
using IGenericControlPanel.Content.Dto.Tips;
using IGenericControlPanel.Content.IData;
using IGenericControlPanel.Content.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums.Content;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class ContentController : LocalizedBaseController<MultiLevelCategoryController>
    {
        #region Properties & Constructor
        private IContentRepository ContentRepository { get; }
        private ITipsRepository TipsRepository { get; }
        private IStringLocalizer<ContentResource> contentLocalizer;
        private IStringLocalizer<SharedResource> sharedLocalizer;
        public ContentController(
            IContentRepository _contentRepository,
            ITipsRepository _tipsRepository,
            IStringLocalizer<ContentResource> contentLocalizer,
            IStringLocalizer<SharedResource> sharedLocalizer)
        {
            ContentRepository = _contentRepository;
            TipsRepository = _tipsRepository;
            this.contentLocalizer = contentLocalizer;
            this.sharedLocalizer = sharedLocalizer;
        }
        #endregion 

        #region Get Privay_Materials Section
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetPrivacyMaterial(PrivacyMaterialsTypes type, string cutlure)
        {
            var result = await ContentRepository.GetPrivacyMaterial(type, cutlure);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(contentLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(contentLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetPrivacyMaterials()
        {
            var result = await ContentRepository.GetPrivacyMaterials();
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(contentLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(contentLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }
        #endregion

        #region Get Tip By Id
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetTips(int ServiceId, int pageSize = 6, int pageNumber = 0)
        {
            var result = await TipsRepository.GetTips(ServiceId, pageSize, pageNumber);
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }
        #endregion

        #region Privacy_Materials Section
        [HttpPost]
        //[DynamicAuthorization("Content", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionPrivacyMaterials(PrivacyMaterialDto privacyMaterialDto)
        {
            var result = await ContentRepository.ActionPrivacyMaterial(privacyMaterialDto);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(contentLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(contentLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }
        #endregion

        #region Tips Action
        [HttpPost]
        //[DynamicAuthorization("Content", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionTips(TipsFormDto formDto)
        {
            var result = await TipsRepository.ActionTip(formDto);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(contentLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(contentLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }
        #endregion

        #region Remove Tip
        [HttpDelete]
        //[DynamicAuthorization("Content", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveTip(int TipId)
        {
            var result = await TipsRepository.RemoveTip(TipId);
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(contentLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(contentLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }
        #endregion
    }
}
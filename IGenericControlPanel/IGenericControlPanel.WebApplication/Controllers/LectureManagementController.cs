﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Model.UserExperiences;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.UserExperiences.Dto.Lecture;
using IGenericControlPanel.UserExperiences.IData.Interfaces;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers;
[Route("api/[controller]")]
[ApiController]
public class LectureManagementController : LocalizedBaseController<LectureManagementController>
{
    #region CTOR
    private readonly ILectureRepository _lectureRepository;
    private readonly IHttpFileService _httpFileService;
    private readonly IMapper _mapper;
    public LectureManagementController(
        IStringLocalizer<LectureManagementController> localizer,
        IConfiguration configuration,
        ILectureRepository lectureRepository,
        IHttpFileService httpFileService,
        IMapper mapper) : base(localizer, configuration)
    {
        _lectureRepository = lectureRepository;
        _httpFileService = httpFileService;
        _mapper = mapper;
    }
    #endregion

    #region GET

    [HttpGet]
    [Route("[action]")]
    public async Task<IActionResult> GetLecturesForUser()
    {
        var result = await _lectureRepository.GetMultipleAsync();

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }

    [HttpGet]
    [Route("[action]")]
    public async Task<IActionResult> GetOneLecture(int id)
    {
        var result = await _lectureRepository.GetDetailsAsync(id);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion

    #region REMOVE
    [HttpDelete]
    [Route("[action]")]
    public async Task<IActionResult> RemoveLecture(int id)
    {
        var result = await _lectureRepository.RemoveAsync(id);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion

    #region ACTION

    [HttpPost]
    [Route("[action]")]
    public async Task<IActionResult> AddLecture([FromForm] LectureAddDto addDto)
    {
        List<FileDetailsDto> fileDtos = new();
        if (addDto.LectureFiles != null && addDto.LectureFiles.Any())
        {
            fileDtos = await FilesExtensionMethods.SaveFiles(new Util.FileOptions
            {
                FileContentType = FileContentType.Image,
                FileSourceType = FileSourceType.Lecture,
                FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                Files = addDto.LectureFiles,
                HttpFileService = _httpFileService,
                Mapper = _mapper,
                SourceId = addDto.Id,
                ControllerName = nameof(Lecture)
            });

            if (fileDtos is null)
            {
                throw new BadRequestException(FromResx["FilesError"].Value);
            }
        }

        var result = await _lectureRepository.AddAsync(addDto, fileDtos);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }

    [HttpPut]
    [Route("[action]")]
    public async Task<IActionResult> UpdateLecture([FromForm] LectureUpdateDto updateDto)
    {
        List<FileDetailsDto> fileDtos = new();
        if (updateDto.LectureFiles != null && updateDto.LectureFiles.Any())
        {
            fileDtos = await FilesExtensionMethods.SaveFiles(new Util.FileOptions
            {
                FileContentType = FileContentType.Image,
                FileSourceType = FileSourceType.Lecture,
                FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                Files = updateDto.LectureFiles,
                HttpFileService = _httpFileService,
                Mapper = _mapper,
                SourceId = updateDto.Id,
                ControllerName = nameof(Lecture)
            });

            if (fileDtos is null)
            {
                throw new BadRequestException(FromResx["FilesError"].Value);
            }
        }

        var result = await _lectureRepository.UpdateAsync(
            updateDto, fileDtos, updateDto.RemoveFileIds);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion

}

﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Content.IData;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.Dto.Service;
using IGenericControlPanel.Core.IData;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Util;
using IGenericControlPanel.WebApplication.ViewModels.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class ServiceController : LocalizedBaseController<ServiceController>
    {
        #region Properties
        private IServiceRepository ServiceRepository { get; }
        private IStringLocalizer<ServiceResource> serviceLocalizer;
        private IStringLocalizer<SharedResource> sharedLocalizer;
        private IHttpFileService HttpFileService { get; }
        private IFileRepository FileRepository { get; }
        private IMapper Mapper { get; }
        #endregion

        #region Constructors
        public ServiceController(IServiceRepository serviceRepository,
                                    ISettingRepository settingRepository,
                                    IUserLanguageRepository userLanguageRepository,
                                     IHttpFileService httpFileService,
                                     IFileRepository fileRepository,
                                     IStringLocalizer<ServiceController> stringLocalizer,
                                     IMapper mapper,
                                     IConfiguration configuration,
                                     IDistributedCache cache,
                                     IUserRepository userRepository,
                                     IStringLocalizer<ServiceResource> serviceLocalizer,
                                     IStringLocalizer<SharedResource> sharedLocalizer)
                      : base(
                            configuration,
                            userLanguageRepository,
                            settingRepository,
                            stringLocalizer,
                            cache,
                            userRepository
                            )
        {
            ServiceRepository = serviceRepository;
            HttpFileService = httpFileService;
            FileRepository = fileRepository;
            Mapper = mapper;
            this.sharedLocalizer = sharedLocalizer;
            this.serviceLocalizer = serviceLocalizer;
        }

        #endregion

        #region GetServices
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetServices(int? EnterpriseId, bool? showFirst, bool? isHidden, string query, int? UserId, int? SpecialtyId, int pageSize = 6, int pageNumber = 0, bool enablePagination = false, bool WithData = false)
        {
            var userInfo = await _userRepository.GetCurrentUserInfo();

            if (WithData)
            {
                IEnumerable<ServiceDetailsDto> data = null;
                OperationResult<GenericOperationResult, IEnumerable<ServiceDetailsDto>> result = null;

                string recordKey = CacheKeys.GetServicesWithData.GetKey(userInfo.IsHealthCareProvider, userInfo.CurrentUserId);

                data = await _cache.GetRecordAsync<IEnumerable<ServiceDetailsDto>>(recordKey);

                if (data is null)
                {
                    result = await ServiceRepository.GetAllDetailedAsync(EnterpriseId, showFirst, isHidden, SpecialtyId, query, pageSize, pageNumber, UserId, enablePagination);
                    await _cache.SetRecordAsync(recordKey, result.Result,
                        result.IEnumerableDataIsValid());
                }
                else
                {
                    result = new();
                    result.Result = data;
                }

                if (result.EnumResult != GenericOperationResult.Success)
                    result.EnumResult.GenericThrowExceptionError(sharedLocalizer[result.ErrorMessages].Value);

                return Ok(result);
            }

            else
            {
                IEnumerable<ServiceDetailsDto> data = null;
                OperationResult<GenericOperationResult, IEnumerable<ServiceDto>> result = null;

                string recordKey = CacheKeys.GetServicesWithoutData.GetKey(userInfo.IsHealthCareProvider, userInfo.CurrentUserId);

                data = await _cache.GetRecordAsync<IEnumerable<ServiceDetailsDto>>(recordKey);

                if (data is null)
                {
                    result = await ServiceRepository.GetAllAsync(showFirst, isHidden, SpecialtyId, query, pageSize, pageNumber, enablePagination, UserId, EnterpriseId);

                    await _cache.SetRecordAsync(recordKey, result.Result, result.IEnumerableDataIsValid());
                }
                else
                {
                    result = new();
                    result.Result = data;
                }

                if (result.EnumResult != GenericOperationResult.Success)
                    result.EnumResult.GenericThrowExceptionError(sharedLocalizer[result.ErrorMessages].Value);

                return Ok(result);
            }
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetHealthCareProvidersByServiceId(int ServiceId)
        {
            var result = await ServiceRepository.GetHealthCareProvidersByServiceId(ServiceId);

            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(sharedLocalizer[result.ErrorMessages].Value);

            return Ok(result);
        }
        #endregion

        #region Remove

        [HttpDelete]
        //[DynamicAuthorization("Service", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveServiceById(int serviceId)
        {
            var userInfo = await _userRepository.GetCurrentUserInfo();

            string recordKey1 = CacheKeys.GetServicesWithData.GetKey(userInfo.IsHealthCareProvider, userInfo.CurrentUserId);

            string recordKey2 = CacheKeys.GetServicesWithoutData.GetKey(userInfo.IsHealthCareProvider, userInfo.CurrentUserId);

            await _cache.RemoveRecordAsync(recordKey1);

            await _cache.RemoveRecordAsync(recordKey2);

            if (serviceId != 0)
                await _cache.RemoveRecordAsync($"{CacheKeys.GetOneService}_{serviceId}");

            var result = await ServiceRepository.RemoveAsync(serviceId);

            return result.EnumResult == GenericOperationResult.Success ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.NotFound => throw new NotFoundException(serviceLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }

        [HttpDelete]
        //[DynamicAuthorization("Service", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveMyService(int serviceId)
        {

            var result = await ServiceRepository.RemoveFromMyServices(serviceId);

            return result.EnumResult == GenericOperationResult.Success ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.NotFound => throw new NotFoundException(serviceLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }

        #endregion

        #region Get Service Details
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetServiceDetails(int serviceId, int? UserId, int? EnterpriseId)
        {
            ServiceDetailsDto data = null;
            OperationResult<GenericOperationResult, ServiceDetailsDto> result = null;
            string recordKey = $"{CacheKeys.GetOneService}_{serviceId}";

            data = await _cache.GetRecordAsync<ServiceDetailsDto>(recordKey);

            if (data is null)
            {
                result = await ServiceRepository.GetDetailedAsync(serviceId, UserId, EnterpriseId);

                await _cache.SetRecordAsync(recordKey, result.Result, result.DataIsValid());
            }
            else
            {
                result = new();
                result.Result = data;
            }

            return result.EnumResult == GenericOperationResult.Success ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.NotFound => throw new NotFoundException(serviceLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }
        #endregion

        #region POST Service Action
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> ServiceAction([FromForm] ServiceViewModel serviceViewModel)
        {
            if (serviceViewModel is null)
                throw new InternalServerException(sharedLocalizer[ErrorMessages.NullObject].Value);

            var userInfo = await _userRepository.GetCurrentUserInfo();

            string recordKey1 = CacheKeys.GetServicesWithData.GetKey(userInfo.IsHealthCareProvider, userInfo.CurrentUserId);
            string recordKey2 = CacheKeys.GetServicesWithoutData.GetKey(userInfo.IsHealthCareProvider, userInfo.CurrentUserId);

            await _cache.RemoveRecordAsync(recordKey1);
            await _cache.RemoveRecordAsync(recordKey2);

            if (serviceViewModel.Id != 0)
                await _cache.RemoveRecordAsync($"{CacheKeys.GetOneService}_{serviceViewModel.Id}");

            if (serviceViewModel.Images.Count != 0)
            {
                var saveFiles = await FilesExtensionMethods
                  .SaveFiles(new Util.FileOptions
                  {
                      FileContentType = FileContentType.Image,
                      FileSourceType = FileSourceType.Service,
                      FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                      Files = serviceViewModel.Images,
                      HttpFileService = HttpFileService,
                      Mapper = Mapper,
                      SourceId = serviceViewModel.Id,
                      ControllerName = nameof(Model.Core.Service)
                  });

                serviceViewModel.ImagesDetails = saveFiles;
            }
            ServiceFormDto serviceForm = new()
            {
                Id = serviceViewModel.Id,
                Name = serviceViewModel.Name,
                Description = serviceViewModel.Description,
                ImagesDetails = serviceViewModel.ImagesDetails,
                CultureCode = serviceViewModel.CultureCode,
                Order = serviceViewModel.Order,
                //Price = serviceViewModel.Price,
                Images = serviceViewModel.Images,
                RemovedFilesIdss = serviceViewModel.RemovedFilesIdss,
                RemovedFilesUrls = serviceViewModel.RemovedFilesUrls,
                ShowFirst = serviceViewModel.ShowFirst,
                IsHidden = serviceViewModel.IsHidden
            };
            serviceForm.HealthCareSpecialtyIds.AddRange(serviceViewModel.HealthCareSpecialtyIds);
            var result = await ServiceRepository.ActionAsync(serviceForm);

            if (result.EnumResult == GenericOperationResult.Success)
            {
                if (result.Result.RemovedFilesUrls != null)
                    _ = HttpFileService.RemoveFiles(result.Result.RemovedFilesUrls);

                _ = HttpFileService.RemoveFiles(result.Result.RemovedFilesUrls);

                return Ok(result);
            }
            else
            {
                if (serviceViewModel.Images != null && serviceViewModel.Images.Count > 0)
                    foreach (var image in serviceViewModel.ImagesDetails)
                        await FilesExtensionMethods.RemoveFileFromWWWRoot(image.Url, HttpFileService);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                _ => throw new Exception(FromResx[result.ErrorMessages].Value)
            };
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> AddToMyServices(int ServiceId)
        {
            var result = await ServiceRepository.AddToMyServices(ServiceId);

            if (result.EnumResult == GenericOperationResult.Success)
                return Ok(result);

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(serviceLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(serviceLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
            };
        }
        #endregion
    }
}

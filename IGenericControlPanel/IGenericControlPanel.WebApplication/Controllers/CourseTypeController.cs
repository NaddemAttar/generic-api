﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Core.Dto.CourseType;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class CourseTypeController : LocalizedBaseController<CourseTypeController>
    {
        #region Constructor & Properties
        private ICourseTypeRepository CourseTypeRepository { get; }
        private IStringLocalizer<CourseResource> courseLocalizer;
        private IStringLocalizer<SharedResource> sharedLocalizer;
        public CourseTypeController(
            IStringLocalizer<CourseTypeController> localizer,
            IStringLocalizer<CourseResource> courseLocalizer,
            IStringLocalizer<SharedResource> sharedLocalizer,
            ICourseTypeRepository courseTypeRepository,
            IDistributedCache cache) : base(localizer, cache)
        {
            CourseTypeRepository = courseTypeRepository;
            this.sharedLocalizer = sharedLocalizer;
            this.courseLocalizer = courseLocalizer;
        }   
        #endregion

        #region Get Course Types
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetCourseTypes()
        {
            IEnumerable<CourseTypeDto> data = null;
            OperationResult<GenericOperationResult, IEnumerable<CourseTypeDto>> result = null;
            string recordKey = $"{CacheKeys.GetCourseTypes}";

            data = await _cache.GetRecordAsync<IEnumerable<CourseTypeDto>>(recordKey);

            if (data is null)
            {
                result = await CourseTypeRepository.GetAllAsync();
                await _cache.SetRecordAsync(recordKey, result.Result, result.IEnumerableDataIsValid());
            }
            else
            {
                result = new();
                result.Result = data;
            }

            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(sharedLocalizer[result.ErrorMessages].Value);

            return Ok(result);
        }

        #endregion

        #region Course Type Action
        [HttpPost]
        //[DynamicAuthorization("CourseType", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> CourseTypeAction(CourseTypeDto courseTypeDto)
        {
            await _cache.RemoveRecordAsync($"{CacheKeys.GetCourseTypes}");

            if (courseTypeDto.Id != 0)
                await _cache.RemoveRecordAsync($"{CacheKeys.GetOneCourseType}_{courseTypeDto.Id}");

            var result = await CourseTypeRepository.ActionAsync(courseTypeDto);

            return result.EnumResult == GenericOperationResult.Success
                   ? Ok(result)
                   : (IActionResult)(result.EnumResult switch
                   {
                       GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                       GenericOperationResult.ValidationError => throw new BadRequestException(courseLocalizer[result.ErrorMessages].Value),
                       GenericOperationResult.NotFound => throw new NotFoundException(courseLocalizer[result.ErrorMessages].Value),
                       _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                   });
        }
        #endregion

        #region Remove Course Type
        [HttpDelete]
        //[DynamicAuthorization("CourseType", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveCourseType(int courseTypeId)
        {
            await _cache.RemoveRecordAsync($"{CacheKeys.GetCourseTypes}");

            if (courseTypeId != 0)
                await _cache.RemoveRecordAsync($"{CacheKeys.GetOneCourseType}_{courseTypeId}");

            var result = await CourseTypeRepository.RemoveAsync(courseTypeId);

            return result.EnumResult == GenericOperationResult.Success
                 ? Ok(result)
                 : (IActionResult)(result.EnumResult switch
                 {
                     GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                     GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                     GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                     _ => throw new Exception(FromResx["InternalServerError"].Value)
                 });
        }
        #endregion
    }
}

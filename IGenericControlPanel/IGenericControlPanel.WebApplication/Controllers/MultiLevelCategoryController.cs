﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using CraftLab.Core.Services.Renders;
using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Core.Dto.Category;
using IGenericControlPanel.Core.IData;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using IGenericControlPanel.SharedKernal.Messages;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class MultiLevelCategoryController : LocalizedBaseController<MultiLevelCategoryController>
    {
        #region Properties

        private IMultiLevelCategoryRepository MultiLevelCategoryRepository { get; }
        private IViewRenderService ViewRenderService { get; }
        private readonly IHttpFileService _httpFileService;
        private readonly IMapper _mapper;
        private readonly IStringLocalizer<ProductResource> productLocalizer;
        private readonly IStringLocalizer<SharedResource> sharedLocalizer;

        #endregion

        #region Constructor

        public MultiLevelCategoryController(IMultiLevelCategoryRepository MultiLevelCategoryRepository,
                IUserLanguageRepository userLanguageRepository,
                ISettingRepository settingRepository,
                IViewRenderService viewRenderService,
                IStringLocalizer<MultiLevelCategoryController> stringLocalizer, IConfiguration configuration,
                IHttpFileService httpFileService, IMapper mapper,
                IStringLocalizer<ProductResource> productLocalizer,
                IStringLocalizer<SharedResource> sharedLocalizer)
            : base(configuration, userLanguageRepository, settingRepository, stringLocalizer)
        {
            this.MultiLevelCategoryRepository = MultiLevelCategoryRepository;
            ViewRenderService = viewRenderService;
            _httpFileService = httpFileService;
            _mapper = mapper;
            this.productLocalizer = productLocalizer;
            this.sharedLocalizer = sharedLocalizer;
        }

        #endregion

        #region Index

        [HttpGet]
        // [Authorize(AuthenticationSchemes = "Bearer")]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetMultiLevelCategoriesNest()
        {
            var result = await MultiLevelCategoryRepository.GetMultiLevelCategoriesNest();

            return result.EnumResult == GenericOperationResult.Success
                ? (IActionResult)Ok(result)
                : result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(productLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(productLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                };
        }
        [HttpGet]
        //[Authorize(AuthenticationSchemes = "Bearer")]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetMultiLevelCategories(string query, bool WithProducts = false)
        {
            var result = await MultiLevelCategoryRepository.GetMultiLevelCategories(query, WithProducts);

            return result.EnumResult == GenericOperationResult.Success
                ? (IActionResult)Ok(result)
                : result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(productLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(productLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                };
        }
        [HttpGet]
        //[Authorize(AuthenticationSchemes = "Bearer")]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetMultiLevelCategoriesLeafs()
        {
            var result = await MultiLevelCategoryRepository.GetMultiLevelCategoriesLeafs();

            return result.EnumResult == GenericOperationResult.Success
                ? (IActionResult)Ok(result)
                : result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(productLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(productLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                };
        }
        #endregion

        #region POST ACTION METHOD

        [HttpPost]
        //[DynamicAuthorization("MultiLevelCategory", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionMultiLevelCategory([FromForm] MultiLevelCategoryDto formDto)
        {
            var oldImage = "";
            if (formDto.Photo != null)
            {
                if (formDto.Id != 0)
                    oldImage = (await MultiLevelCategoryRepository.GetMultiLevelCategoryPhotoById(formDto.Id)).Result;

                var saveImaegInServiceFolder = await FilesExtensionMethods.SaveFile(new Util.FileOptions
                {
                    FileContentType = FileContentType.Image,
                    FileSourceType = FileSourceType.Category,
                    FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                    File = formDto.Photo,
                    HttpFileService = _httpFileService,
                    Mapper = _mapper,
                    SourceId = formDto.Id,
                    ControllerName = nameof(MultiLevelCategoryController)
                });

                if (saveImaegInServiceFolder is null)
                    throw new BadRequestException(sharedLocalizer[ErrorMessages.FailedSavingPhoto].Value);

                formDto.PhotoUrl = saveImaegInServiceFolder.Url;
            }
            var result = await MultiLevelCategoryRepository.ActionMultiLevelCategory(formDto);
            if (result.IsSuccess)
            {
                if (!string.IsNullOrEmpty(oldImage))
                    await FilesExtensionMethods.RemoveFileFromWWWRoot(oldImage, _httpFileService);
            }
            if (!result.IsSuccess)
            {
                if (!string.IsNullOrEmpty(formDto.PhotoUrl))
                    await FilesExtensionMethods.RemoveFileFromWWWRoot(formDto.PhotoUrl, _httpFileService);
            }
            return result.EnumResult == GenericOperationResult.Success
                ? (IActionResult)Ok(result)
                : result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(productLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(productLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                };

        }
        #endregion

        #region REMOVE

        [HttpDelete]
        //[DynamicAuthorization("MultiLevelCategory", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveMultiLevelCategory(int id)
        {
            var oldImage = (await MultiLevelCategoryRepository.GetMultiLevelCategoryPhotoById(id)).Result;

            var result = await MultiLevelCategoryRepository.RemoveMultiLevelCategory(id);

            if (!string.IsNullOrEmpty(oldImage) && result.IsSuccess)
                await FilesExtensionMethods.RemoveFileFromWWWRoot(oldImage, _httpFileService);

            return result.EnumResult == GenericOperationResult.Success
                ? (IActionResult)Ok(result)
                : result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(productLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(productLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                };
        }
        #endregion
    }
}
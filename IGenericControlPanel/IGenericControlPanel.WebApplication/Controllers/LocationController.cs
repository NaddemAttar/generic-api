﻿
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Course;
using IGenericControlPanel.Core.Dto.Location;
using IGenericControlPanel.Core.Dto.Location.LocationSchedule;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class LocationController : LocalizedBaseController<LocationController>
    {
        #region Properties & Constructor
        private ILocationRepository LocationRepository { get; }
        private IDoctorLocationRepository DoctorLocationRepository { get; }
        public LocationController(
            ILocationRepository locationRepository,
            IDoctorLocationRepository DoctorLocationRepository,
            IStringLocalizer<LocationController> localizer) : base(localizer)
        {
            LocationRepository = locationRepository;
            this.DoctorLocationRepository = DoctorLocationRepository;
        }
        #endregion

        #region Get Locations
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetLocations()
        {
            var result = LocationRepository.GetAll();

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetAllDoctorLocations()
        {
            var result = DoctorLocationRepository.GetAllDoctorLocations();

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetDoctorLocation(int Id)
        {
            var result = DoctorLocationRepository.GetDoctorLocation(Id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetLocationSchedule(
            DateTime? fromDate, DateTime? toDate, int cityId,
            bool IgnoreOldDates, bool OrderFromOldestToNewest)
        {
            var result = await LocationRepository
                .GetLocationsSchedule(fromDate, toDate, cityId, IgnoreOldDates, OrderFromOldestToNewest);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetCountries()
        {
            var result = LocationRepository.GetCountries();

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetCities(int countryId)
        {
            var result = LocationRepository.GetCities(countryId);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                });
        }

        #endregion

        #region Location Action
        [HttpPost]
        //[DynamicAuthorization("Location", nameof(ActionType.Action))]
        [Route("[action]")]
        public IActionResult LocationAction(LocationDto locationDto)
        {
            var result = LocationRepository.Action(locationDto);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                });
        }

        [HttpPost]
        //[DynamicAuthorization("Location", nameof(ActionType.Action))]
        [Route("[action]")]
        public IActionResult DoctorLocationAction(DoctorLocationDto locationDto)
        {
            var result = DoctorLocationRepository.ActionDoctorLocation(locationDto);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        [HttpPost]
        //[DynamicAuthorization("Location", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionLocationSchedule(LocationScheduleFormDto locationDto)
        {
            var result = await LocationRepository.ActionLocationSchedule(locationDto);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {

                GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion

        #region Remove Action
        [HttpDelete]
        //[DynamicAuthorization("Location", nameof(ActionType.Remove))]
        [Route("[action]")]
        public IActionResult RemoveLocation(int locationId)
        {
            var result = LocationRepository.Remove(locationId);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        [HttpDelete]
        //[DynamicAuthorization("Location", nameof(ActionType.Remove))]
        [Route("[action]")]
        public IActionResult RemoveDoctorLocation(int Id)
        {
            var result = DoctorLocationRepository.RemoveDoctorLocation(Id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        [HttpDelete]
        //[DynamicAuthorization("Location", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveLocationSchedule(int locationScheduleId)
        {
            var result = await LocationRepository.RemoveLocationSchedule(locationScheduleId);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                });
        }
        #endregion
    }
}

﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using IGenericControlPanel.Content.IData;
using IGenericControlPanel.Core.Dto.Course;
using IGenericControlPanel.Core.IData;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums.Core;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Logger.Classes;
using IGenericControlPanel.SharedKernal.Logger.Interface;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Util;
using IGenericControlPanel.WebApplication.ViewModels.Payment;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System.Web;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class CourseController : LocalizedBaseController<CourseController>
    {
        #region Properties
        private ICourseRepository CourseRepository { get; }
        private IOrderRepository OrderRepository { get; }
        private IHttpFileService HttpFileService { get; }
        private IFileRepository FileRepository { get; }
        private IMapper Mapper { get; }
        private readonly UserManager<CPUser> userManager;
        private ILogService LogService { get; }
        private readonly IStringLocalizer<CourseResource> courseLocalizer;
        private readonly IStringLocalizer<SharedResource> sharedLocalizer;
        private readonly IStringLocalizer<AccountResource> accountLocalizer;
        #endregion

        #region Constructor
        public CourseController(ICourseRepository courseRepository,
            IOrderRepository orderRepository,
            IHttpFileService httpFileService,
            IFileRepository fileRepository,
            IMapper mapper,
            IConfiguration configuration,
            UserManager<CPUser> _userManager,
            ILogService logService,
            IDistributedCache cache,
            IStringLocalizer<SharedResource> sharedLocalizer,
            IStringLocalizer<CourseResource> courseLocalizer,
            IStringLocalizer<AccountResource> accountLocalizer) : base(configuration, cache)
        {
            CourseRepository = courseRepository;
            OrderRepository = orderRepository;
            HttpFileService = httpFileService;
            FileRepository = fileRepository;
            Mapper = mapper;
            LogService = logService;
            userManager = _userManager;
            this.sharedLocalizer = sharedLocalizer;
            this.courseLocalizer = courseLocalizer;
            this.accountLocalizer = accountLocalizer;
        }

        #endregion 

        #region Get Courses
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetCoursesSelect()
        {
            IEnumerable<CourseDto> data = null;
            OperationResult<GenericOperationResult, IEnumerable<CourseDto>> result = null;
            string recordKey = $"{CacheKeys.GetCourses}";

            data = await _cache.GetRecordAsync<IEnumerable<CourseDto>>(recordKey);

            if (data is null)
            {
                result = await CourseRepository.GetCoursesSelectAsync();
                await _cache.SetRecordAsync(recordKey, result.Result, result.IEnumerableDataIsValid());
            }
            else
            {
                result = new();
                result.Result = data;
            }

            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(sharedLocalizer[result.ErrorMessages].Value);

            return Ok(result);
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetCourses(int locationId, string instructorIds, string courseTypeIds,
            string courseName, bool MyCourses = false, int? pageNumber = 0, int? pageSize = 4,
            int studentId = 0, bool? hiddenCourse = null)
        {
            List<int> listIntegerForInstructorIds = instructorIds.ConvertJsonStringToObjectModal<int>();
            List<int> listIntegerForCourseTypeIds = courseTypeIds.ConvertJsonStringToObjectModal<int>();

            var result = await CourseRepository.GetCoursesWithPaginationAsync(locationId, listIntegerForCourseTypeIds, MyCourses,
                listIntegerForInstructorIds, courseName, pageNumber, pageSize, studentId, hiddenCourse);

            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(sharedLocalizer[result.ErrorMessages].Value);

            return Ok(result);
        }
        #endregion

        #region Get Course Details
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetCourseDetails(int id)
        {
            CourseDetailsDto data = null;
            OperationResult<GenericOperationResult, CourseDetailsDto> result = null;
            string recordKey = $"{CacheKeys.GetOneCourse}_{id}";

            data = await _cache.GetRecordAsync<CourseDetailsDto>(recordKey);

            if (data is null)
            {
                result = await CourseRepository.GetDetailedAsync(id);

                await _cache.SetRecordAsync(recordKey, result.Result,
                    result.DataIsValid());
            }

            else
            {
                result = new();
                result.Result = data;
            }

            return result.EnumResult == GenericOperationResult.Success ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(courseLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(courseLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value),
                });
        }
        #endregion

        #region Remove Courses
        [HttpDelete]
        //[DynamicAuthorization("Course", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveCourses(IEnumerable<int> courseIds)
        {
            await _cache.RemoveRecordAsync($"{CacheKeys.GetCourses}");

            if (courseIds.Count() != 0)
                foreach (var id in courseIds)
                    await _cache.RemoveRecordAsync($"{CacheKeys.GetOneCourse}_{id}");

            var result = await CourseRepository.RemoveAsync(courseIds);

            return result.EnumResult == GenericOperationResult.Success ? Ok(result)
                : result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(courseLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(courseLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                };
        }

        #endregion

        #region Action Course
        [HttpPost]
        //[DynamicAuthorization("Course", nameof(ActionType.Action))]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public async Task<IActionResult> CourseEnrollment(int CourseId)
        {
            var result = await CourseRepository.CourseEnrollmentAsync(CourseId);

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(courseLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(courseLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
            };
        }

        [HttpPost]
        //[DynamicAuthorization("Course", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionCourse([FromForm] CourseFormDto courseFormDto)
        {
            await _cache.RemoveRecordAsync($"{CacheKeys.GetCourses}");

            if (courseFormDto.Id != 0)
                await _cache.RemoveRecordAsync($"{CacheKeys.GetOneCourse}_{courseFormDto.Id}");

            if (courseFormDto.AttachedFiles.Count != 0)
            {
                var saveImaegInServiceFolder = await FilesExtensionMethods.SaveFiles(new Util.FileOptions
                {
                    FileContentType = FileContentType.Image,
                    FileSourceType = FileSourceType.Course,
                    FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                    Files = courseFormDto.AttachedFiles,
                    HttpFileService = HttpFileService,
                    Mapper = Mapper,
                    SourceId = courseFormDto.Id,
                    ControllerName = nameof(Course)
                });

                if (saveImaegInServiceFolder is null)
                    throw new BadRequestException(sharedLocalizer[ErrorMessages.FailedSavingPhoto].Value);

                courseFormDto.Attachments = saveImaegInServiceFolder;
            }

            var result = await CourseRepository.ActionAsync(courseFormDto);

            if (result.EnumResult == GenericOperationResult.Success)
            {
                if (result.Result.RemoveFileUrls != null)
                    _ = HttpFileService.RemoveFiles(result.Result.RemoveFileUrls);

                return Ok(result);
            }
            else
            {
                if (courseFormDto.AttachedFiles != null && courseFormDto.AttachedFiles.Count > 0)
                    foreach (var image in courseFormDto.Attachments)
                        await FilesExtensionMethods.RemoveFileFromWWWRoot(image.Url, HttpFileService);
            }
            return result.EnumResult == GenericOperationResult.Success ? Ok(result)
            : (IActionResult)(result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(courseLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(courseLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
            });
        }
        #endregion

        #region Hide Course By Id
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("[action]")]
        public async Task<IActionResult> HideCourseById(int courseId, bool isHidden)
        {
            await _cache.RemoveRecordAsync($"{CacheKeys.GetCourses}");
            await _cache.RemoveRecordAsync($"{CacheKeys.GetOneCourse}_{courseId}");

            var result = await CourseRepository.HideCourseByIdAsync(courseId, isHidden);

            return result.EnumResult == GenericOperationResult.Success ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(courseLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                });
        }
        #endregion

        #region Webhooks
        [HttpPost]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> PaymentWebHook(PaymentViewModel viewModel)
        {
            LogService.Log(new HITLog() { Message = JsonConvert.SerializeObject(viewModel) });
            LogService.Log(new HITLog() { Message = "after payment log" });
            Uri myUri = new(viewModel.order.merchantAttributes.redirectUrl);

            int? userId = userManager.FindByEmailAsync(viewModel.order.emailAddress).Result.Id;
            if (userId == null)
                throw new NotFoundException(accountLocalizer[ErrorMessages.UserNotExist].Value);
            
            if (myUri.Query.Contains("courseId"))
            {
                int CourseId = int.Parse(HttpUtility.ParseQueryString(myUri.Query).Get("courseId"));
                var result = await CourseRepository.ActionCoursePaymentAsync(
                    userId.Value, CourseId, PaymentStatus.SUCCESS);

                return result.IsSuccess ? Ok(result) : throw new BadRequestException(sharedLocalizer[ErrorMessages.PaymentFailed].Value);
            }
            else if (myUri.Query.Contains("orderId"))
            {
                int OrderId = int.Parse(HttpUtility.ParseQueryString(myUri.Query).Get("orderId"));
                OperationResult<GenericOperationResult> result = OrderRepository.ChangeOrderStatus(OrderId, PaymentStatus.SUCCESS);
                return result.IsSuccess ? Ok(result) : throw new BadRequestException(sharedLocalizer[ErrorMessages.PaymentFailed].Value);
            }
            //int CourseId = int.Parse(HttpUtility.ParseQueryString(myUri.Query).Get("courseId"));

            throw new InternalServerException(sharedLocalizer[ErrorMessages.InternalServerError].Value);
        }
        #endregion
    }
}
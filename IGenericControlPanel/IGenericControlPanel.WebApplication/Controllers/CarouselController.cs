﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Content.IData;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Util;
using IGenericControlPanel.WebApplication.ViewModels.Carousel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CarouselController : LocalizedBaseController<CarouselController>
    {
        #region Properties & Constructor
        private ICarouselItemRepository CarouselItemRepository { get; }
        private IHttpFileService HttpFileService { get; }
        private IFileRepository FileRepository { get; }
        private IMapper Mapper { get; }
        private readonly IStringLocalizer<CarouselResource> carouselLocalizer;
        private readonly IStringLocalizer<SharedResource> sharedLocalizer;

        public CarouselController(ICarouselItemRepository carouselItemRepository,
            ISettingRepository settingRepository,
            IUserLanguageRepository userLanguageRepository,
            IHttpFileService httpFileService,
            IStringLocalizer<CarouselController> stringLocalizer,
            IFileRepository fileRepository,
            IMapper mapper, IConfiguration configuration, IStringLocalizer<SharedResource> sharedLocalizer, IStringLocalizer<CarouselResource> carouselLocalizer)
            : base(configuration, userLanguageRepository, settingRepository, stringLocalizer)
        {
            CarouselItemRepository = carouselItemRepository;
            HttpFileService = httpFileService;
            FileRepository = fileRepository;
            Mapper = mapper;
            this.sharedLocalizer = sharedLocalizer;
            this.carouselLocalizer = carouselLocalizer;
        }
        #endregion

        #region Remove Method

        [HttpDelete]
        //[DynamicAuthorizationAttribute("Carousel", nameof(ActionType.Remove))]
        [Route("[action]")]
        public IActionResult RemoveCarouselItem(int id)
        {
            var result = CarouselItemRepository.Remove(id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(carouselLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(carouselLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        [HttpDelete]
        //[DynamicAuthorizationAttribute("Carousel", nameof(ActionType.Remove))]
        [Route("[action]")]
        public IActionResult RemoveCarousel(int id)
        {
            var result = CarouselItemRepository.RemoveCarousel(id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(carouselLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(carouselLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }
        #endregion

        #region GET METHOD

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetAllCarouselItems(int pageNumber = 0, int pageSize = 6)
        {
            var result = CarouselItemRepository.GetAllPaged(pageNumber, pageSize);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(carouselLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(carouselLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetCarouselItemDetails(int id)
        {
            var result = CarouselItemRepository.GetDetailed(id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(carouselLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(carouselLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetAllCarousels()
        {
            var result = CarouselItemRepository.GetCarousels();

            return result.EnumResult == GenericOperationResult.Success
                ? Json(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(carouselLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(carouselLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetCarouselItems(int id)
        {
            var result = CarouselItemRepository.GetCarouselItems(id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(carouselLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(carouselLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        #endregion

        #region POST METHOD

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> ActionCarouselItem([FromForm] CarouselFormViewModel CarouselForm)
        {
            CarouselItemFormDto formDto = new()
            {
                Id = CarouselForm.Id,
                Title = CarouselForm.Title,
                BottomSubTitle = CarouselForm.BottomSubTitle,
                ButtonActionUrl = CarouselForm.ButtonActionUrl,
                ButtonText = CarouselForm.ButtonText,
                CarouselId = CarouselForm.CarouselId,
                CultureCode = CarouselForm.CultureCode,
                ImageUrl = CarouselForm.ImageUrl,
                Order = CarouselForm.Order,
                TextColor = CarouselForm.TextColor,
                TopSubTitle = CarouselForm.TopSubTitle,
                TranslationsFormDtos = CarouselForm.TranslationsFormDtos,
            };

            if (CarouselForm.Image != null)
            {
                var fileToAdd = await FilesExtensionMethods
                     .SaveFile(new Util.FileOptions
                     {
                         FileContentType = FileContentType.Image,
                         FileSourceType = FileSourceType.Carousel,
                         FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                         File = CarouselForm.Image,
                         HttpFileService = HttpFileService,
                         Mapper = Mapper,
                         SourceId = CarouselForm.Id,
                         ControllerName = nameof(Model.Content.Carousel)
                     });

                if (fileToAdd is null)
                    throw new BadRequestException(sharedLocalizer[ErrorMessages.FailedSavingPhoto].Value);

                formDto.Image = fileToAdd;
            }

            var result = CarouselItemRepository.Action(formDto);

            if (!result.IsSuccess)
                await FilesExtensionMethods.RemoveFileFromWWWRoot(formDto.Image.Url, HttpFileService);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(carouselLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(carouselLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult ActionCarousel(CarouselDetialsDto CarouselForm)
        {
            var result = CarouselItemRepository.ActionCarousel(CarouselForm);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(carouselLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(carouselLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }
        #endregion

        #region Get All Files For Carousel
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public JsonResult GetAllFiles(int id)
        {
            var result = CarouselItemRepository.GetDetailed(id);

            return result.EnumResult == GenericOperationResult.Success ? Json(result.Result.Image)
                : (result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(carouselLocalizer[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(carouselLocalizer[result.ErrorMessages].Value),
                    _ => throw new Exception(sharedLocalizer[ErrorMessages.InternalServerError].Value)
                });
        }
        #endregion
    }
}
﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Model.UserExperiences;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.UserExperiences.Dto.Education;
using IGenericControlPanel.UserExperiences.IData.Interfaces;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers;
[Route("api/[controller]")]
[ApiController]
public class EducationManagementController : LocalizedBaseController<EducationManagementController>
{
    #region Constructor & Properties
    private readonly IEducationRepository _educationRepository;
    private readonly IHttpFileService _httpFileService;
    private readonly IMapper _mapper;
    public EducationManagementController(
         IStringLocalizer<EducationManagementController> localizer,
         IEducationRepository educationRepository,
         IHttpFileService httpFileService,
         IMapper mapper,
         IConfiguration configuration) : base(localizer, configuration)
    {
        _educationRepository = educationRepository;
        _httpFileService = httpFileService;
        _mapper = mapper;
    }
    #endregion

    #region Get
    [HttpGet]
    [Route("[action]")]
    public async Task<IActionResult> GetEducationsForUser()
    {
        var result = await _educationRepository.GetMultipleAsync();

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }

    [HttpGet]
    [Route("[action]")]
    public async Task<IActionResult> GetOneEducation(int id)
    {
        var result = await _educationRepository.GetDetailsAsync(id);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion

    #region Remove
    [HttpDelete]
    [Route("[action]")]
    public async Task<IActionResult> RemoveEducation(int id)
    {
        var result = await _educationRepository.RemoveAsync(id);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion

    #region Action
    [HttpPost]
    [Route("[action]")]
    public async Task<IActionResult> AddEducation([FromForm] EducationAddDto addDto)
    {
        List<FileDetailsDto> fileDtos = new();
        if (addDto.EducationFiles != null && addDto.EducationFiles.Any())
        {
            fileDtos = await FilesExtensionMethods.SaveFiles(new Util.FileOptions
            {
                FileContentType = FileContentType.Image,
                FileSourceType = FileSourceType.Education,
                FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                Files = addDto.EducationFiles,
                HttpFileService = _httpFileService,
                Mapper = _mapper,
                SourceId = addDto.Id,
                ControllerName = nameof(Education)
            });

            if (fileDtos is null)
            {
                throw new BadRequestException(FromResx["FilesError"].Value);
            }
        }

        var result = await _educationRepository.AddAsync(addDto, fileDtos);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }

    [HttpPut]
    [Route("[action]")]
    public async Task<IActionResult> UpdateEducation([FromForm] EducationUpdateDto updateDto)
    {
        List<FileDetailsDto> fileDtos = new();
        if (updateDto.EducationFiles != null && updateDto.EducationFiles.Any())
        {
            fileDtos = await FilesExtensionMethods.SaveFiles(new Util.FileOptions
            {
                FileContentType = FileContentType.Image,
                FileSourceType = FileSourceType.Experience,
                FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                Files = updateDto.EducationFiles,
                HttpFileService = _httpFileService,
                Mapper = _mapper,
                SourceId = updateDto.Id,
                ControllerName = nameof(Experience)
            });

            if (fileDtos is null)
            {
                throw new BadRequestException(FromResx["FilesError"].Value);
            }
        }

        var result = await _educationRepository.UpdateAsync(
            updateDto, fileDtos, updateDto.RemoveFileIds);

        if (result.EnumResult != GenericOperationResult.Success)
            result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion
}

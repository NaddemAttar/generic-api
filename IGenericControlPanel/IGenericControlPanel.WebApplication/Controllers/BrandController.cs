﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Core.Dto.Brand;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers;

[ApiController]
[Route("api/[controller]")]
public class BrandController : LocalizedBaseController<BrandController>
{
    #region Properties & Constructor
    private IBrandRepository BrandRepository { get; }
    private IStringLocalizer<SharedResource> sharedLocalizer;
    private IStringLocalizer<BrandResource> brandLocalizer;
    public BrandController(
        IBrandRepository brandRepository,
        IStringLocalizer<BrandController> localizer,
        IDistributedCache cache,
        IStringLocalizer<SharedResource> sharedLocalizer,
        IStringLocalizer<BrandResource> brandLocalizer) : base(localizer, cache)
    {
        BrandRepository = brandRepository;
        this.sharedLocalizer = sharedLocalizer;
        this.brandLocalizer = brandLocalizer;
    }

    #endregion

    #region Get Section
    [HttpGet]
    [AllowAnonymous]
    [Route("[action]")]
    public async Task<IActionResult> GetBrands(int pageNumber = 0, int pageSize = 5)
    {
        IEnumerable<BrandDto> data = null;
        OperationResult<GenericOperationResult, IEnumerable<BrandDto>> result = null;
        string recordKey = $"{CacheKeys.GetBrands}_{pageNumber}_{pageSize}";

        data = await _cache.GetRecordAsync<IEnumerable<BrandDto>>(recordKey);

        if (data is null)
        {
            result = await BrandRepository.GetBrandsAsync(pageNumber, pageSize);

            await _cache.SetRecordAsync(
                recordKey, result.Result,
                result.IEnumerableDataIsValid(),
                TimeSpan.FromSeconds(30));
        }
        else
        {
            result = new();
            result.Result = data;
        }

        if (result.EnumResult != GenericOperationResult.Success)
            throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value);

        return Ok(result);
    }

    [HttpGet]
    [AllowAnonymous]
    [Route("[action]")]
    public async Task<IActionResult> GetAllBrands()
    {
        OperationResult<GenericOperationResult,
            IEnumerable<BrandDto>> result = null;
        IEnumerable<BrandDto> data = null;
        string recordKey = $"{CacheKeys.GetBrands}";

        data = await _cache
            .GetRecordAsync<IEnumerable<BrandDto>>(recordKey);

        if (data is null)
        {
            result = await BrandRepository.GetAllBrandsAsync();

            await _cache.SetRecordAsync(recordKey, result.Result,
                result.IEnumerableDataIsValid());
        }
        else
        {
            result = new();
            result.Result = data;
        }

        if (result.EnumResult != GenericOperationResult.Success)
            throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value);

        return Ok(result);
    }

    [HttpGet]
    [AllowAnonymous]
    [Route("[action]")]
    public async Task<IActionResult> GetBrand(int BrandId)
    {
        OperationResult<GenericOperationResult, BrandDto> result = null;
        BrandDto data = null;
        string recordKey = $"{CacheKeys.GetOneBrand}_{BrandId}";

        data = await _cache.GetRecordAsync<BrandDto>(recordKey);

        if (data is null)
        {
            result = await BrandRepository.GetBrandByIdAsync(BrandId);

            await _cache.SetRecordAsync(recordKey, result.Result,
                result.DataIsValid());
        }
        else
        {
            result = new();
            result.Result = data;
        }

        if (result.EnumResult == GenericOperationResult.NotFound)
            throw new NotFoundException(brandLocalizer[result.ErrorMessages].Value);
        else if (result.EnumResult == GenericOperationResult.Failed)
            throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value);

        return Ok(result);
    }
    #endregion

    #region Action Section
    [HttpPost]
    ////[DynamicAuthorization("Brand", nameof(ActionType.Action))]
    //[Authorize(Roles ="Admin")]
    //[Authorize(AuthenticationSchemes = "Bearer")]
    [Route("[action]")]
    public async Task<IActionResult> ActionBrand(BrandFormDto formDto)
    {
        await _cache.RemoveRecordAsync($"{CacheKeys.GetBrands}");

        if (formDto.Id != 0)
        {
            await _cache.RemoveRecordAsync($"{CacheKeys.GetOneBrand}_{formDto.Id}");
        }

        var result = await BrandRepository.ActionBrandAsync(formDto);

        if (result.EnumResult != GenericOperationResult.Success)
        {
            return result.EnumResult switch{
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(brandLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(brandLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
            };
        }

        await _cache
            .SetRecordAsync($"{CacheKeys.GetOneBrand}_{result.Result.Id}", result.Result,
            (result.IsSuccess && result.Result != null));

        return Ok(result);

    }
    #endregion

    #region Remove Section
    [HttpDelete]
    ////[DynamicAuthorization("Brand", nameof(ActionType.Remove))]
    [Route("[action]")]
    public async Task<IActionResult> RemoveBrand(int BrandId)
    {
        await _cache.RemoveRecordAsync($"{CacheKeys.GetBrands}");

        if (BrandId != 0)
        {
            await _cache.RemoveRecordAsync($"{CacheKeys.GetOneBrand}_{BrandId}");
        }

        var result = await BrandRepository.RemoveBrandAsync(BrandId);


        if (result.EnumResult != GenericOperationResult.Success)
        {
            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(brandLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(brandLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
            };
        }

        return Ok(result);
    }
    #endregion
}

﻿
using AutoMapper;

using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Core.Dto.Project;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Util;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class ProjectController : LocalizedBaseController<ProjectController>
    {
        #region Properties & Constructor
        private IProjectRepository ProjectRepository { get; }
        private IHttpFileService HttpFileService { get; }
        private IMapper Mapper { get; }

        public ProjectController(
            IProjectRepository projectRepository,
            IHttpFileService httpFileService,
            IMapper mapper,
            IConfiguration configuration,
            IStringLocalizer<ProjectController> localizer) : base(localizer, configuration)
        {
            ProjectRepository = projectRepository;
            Mapper = mapper;
            HttpFileService = httpFileService;
        }
        #endregion

        #region Get Projects With Pagination
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetProjects(
            int? pageNumber = 0, int? pageSize = 6)
        {
            var result = await ProjectRepository.GetAllProjectsWithPagination(pageNumber, pageSize);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                });
        }
        #endregion

        #region Get Project Details By Id
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetProjectDetails(int id)
        {
            var result = ProjectRepository.GetProjectDetails(id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                });
        }
        #endregion

        #region Remove Project By Id

        [HttpDelete]
        //[DynamicAuthorization("Project", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveProject(int id)
        {
            var result = await ProjectRepository.RemoveProject(id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                });
        }
        #endregion

        #region Add New Project
        [HttpPost]
        //[DynamicAuthorization("Project", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> AddProject([FromForm] ProjectFormDto projectFormDto)
        {
            if (projectFormDto.ImagesForm != null)
            {
                var saveFiles = await FilesExtensionMethods
                      .SaveFiles(new Util.FileOptions
                      {
                          FileContentType = FileContentType.Image,
                          FileSourceType = FileSourceType.Project,
                          FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                          Files = projectFormDto.ImagesForm,
                          HttpFileService = HttpFileService,
                          Mapper = Mapper,
                          SourceId = projectFormDto.Id,
                          ControllerName = nameof(Model.Projects.Project)
                      });

                if (saveFiles != null)
                {
                    projectFormDto.Images = saveFiles;
                }
            }

            projectFormDto = await ConvertComponentFileToUrlString(projectFormDto);

            if (projectFormDto is null)
            {
                throw new BadRequestException(FromResx["FilesError"].Value);
            }

            var result = await ProjectRepository.AddProject(projectFormDto);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                });
        }
        #endregion

        #region Update Project (// HTTPPUT //)
        [HttpPut]
        // //[DynamicAuthorization("Project", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> UpdateProject([FromForm] ProjectFormDto projectFormDto)
        {
            if (projectFormDto.ImagesForm != null)
            {
                var saveFiles = await FilesExtensionMethods
                     .SaveFiles(new Util.FileOptions
                     {
                         FileContentType = FileContentType.Image,
                         FileSourceType = FileSourceType.Project,
                         FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                         Files = projectFormDto.ImagesForm,
                         HttpFileService = HttpFileService,
                         Mapper = Mapper,
                         SourceId = projectFormDto.Id,
                         ControllerName = nameof(Model.Projects.Project)
                     });
                if (saveFiles != null)
                {
                    projectFormDto.Images = saveFiles;
                }
            }

            projectFormDto = await ConvertComponentFileToUrlString(projectFormDto);

            if (projectFormDto is null)
            {
                throw new BadRequestException(FromResx["FilesError"].Value);
            }

            var result = ProjectRepository.UpdateProject(projectFormDto);

            if (result.EnumResult == GenericOperationResult.Success &&
                result.Result.FileUrlsMustRemoveItFromServer != null)
            {
                _ = HttpFileService.RemoveFiles(result.Result.FileUrlsMustRemoveItFromServer);
            }

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx[result.ErrorMessages].Value)
                });
        }
        #endregion

        #region Convert Files To FileDtos
        private async Task<ProjectFormDto> ConvertComponentFileToUrlString(ProjectFormDto projectFormDto)
        {
            foreach (ComponentDto component in projectFormDto.Components)
            {
                if (component.ComponentImageFile != null)
                {
                    var fileDto =
                        await FilesExtensionMethods.SavingFileInWWWRootFolder(component.ComponentImageFile,
                        Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath), HttpFileService);
                    try
                    {
                        component.ComponentImageUrl = fileDto.Result.Url;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }
            return projectFormDto;
        }
        #endregion
    }

}
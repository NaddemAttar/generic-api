﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Content.IData;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.Dto.Product;
using IGenericControlPanel.Core.IData;
using IGenericControlPanel.Model;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Logger.Classes;
using IGenericControlPanel.SharedKernal.Logger.Interface;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Resources;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.Util;
using IGenericControlPanel.WebApplication.ViewModels.Payment;
using IGenericControlPanel.WebApplication.ViewModels.Product;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using FileOptions = IGenericControlPanel.WebApplication.Util.FileOptions;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class ProductController : LocalizedBaseController<ProductController>
    {
        #region Properties

        private IProductRepository ProductRepository { get; }
        private IMultiLevelCategoryRepository MultiLevelCategoryRepository { get; set; }
        private IHttpFileService HttpFileService { get; }
        private IFileRepository FileRepository { get; }
        private IMapper Mapper { get; }
        private ILogService LogService { get; }
        private readonly IStringLocalizer<ProductResource> productLocalizer;
        private readonly IStringLocalizer<SharedResource> sharedLocalizer;
        private readonly string API_KEY = "OWVjNjVkOTktYTg0Zi00OWVkLWFmZmItM2NmYWQ4NzdkYjlkOmIxODg3OWFkLTAxYzQtNGE1YS05ODdlLThmZWIzZWY2MDM4Yw==";
        private readonly string OUTLET_REF = "10a841ff-dbb7-4ce4-bd90-717b9728bf79";

        #endregion

        #region Constructors

        public ProductController(IProductRepository productsRepository,
            ISettingRepository settingRepository,
            IUserLanguageRepository userLanguageRepository,
            IMultiLevelCategoryRepository MultiLevelCategoryRepository,
            IHttpFileService httpFileService,
            IFileRepository fileRepository,
            IStringLocalizer<ProductController> stringLocalizer,
            IMapper mapper,
            IConfiguration configuration,
            ILogService logService,
            IDistributedCache cache,
            IUserRepository userRepository,
            IStringLocalizer<ProductResource> productLocalizer,
            IStringLocalizer<SharedResource> sharedLocalizer)
            : base(
                  configuration,
                  userLanguageRepository,
                  settingRepository,
                  stringLocalizer,
                  cache,
                  userRepository
                  )
        {
            ProductRepository = productsRepository;
            this.MultiLevelCategoryRepository = MultiLevelCategoryRepository;
            HttpFileService = httpFileService;
            FileRepository = fileRepository;
            Mapper = mapper;
            LogService = logService;
            this.productLocalizer = productLocalizer;
            this.sharedLocalizer = sharedLocalizer;
        }

        #endregion

        #region Get

        [HttpGet]
        //[Authorize(AuthenticationSchemes = "Bearer")]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetCategoriesWithProductCount()
        {
            var result = await MultiLevelCategoryRepository.GetCategoriesWithChildrenCount();

            return result.EnumResult == GenericOperationResult.Success
               ? Ok(result)
               : (IActionResult)(result.EnumResult switch
               {
                   GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                   _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
               });
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetProductSizesColors(int Id)
        {
            var result = await ProductRepository.GetProductSizesColorsAsync(Id);
            return result.EnumResult == GenericOperationResult.Success
               ? Ok(result)
               : (IActionResult)(result.EnumResult switch
               {
                   GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                   GenericOperationResult.ValidationError => throw new BadRequestException(productLocalizer[result.ErrorMessages].Value),
                   GenericOperationResult.NotFound => throw new NotFoundException(productLocalizer[result.ErrorMessages].Value),
                   _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
               });
        }

        [HttpGet]
        //[Authorize(AuthenticationSchemes = "Bearer")]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetAllProducts(bool? TopRated,
            int? MinPrice, int? MaxPrice,
            int? categroyId, int? BrandId, string JsonSpecificationsIds, string query,
            int? pageNumber = 0, int? pageSize = 6, string culturCode = "ar", bool? showInStore = null, int? OfferIdToOrderBy = null)
        {
            List<int> SpecificationsIds = new();
            if (JsonSpecificationsIds != null)
            {
                SpecificationsIds = JsonConvert.DeserializeObject<List<int>>(JsonSpecificationsIds);
            }
            var result = await ProductRepository.GetAllDetailedPagedAsync(
                TopRated, MinPrice, MaxPrice, categroyId, BrandId, SpecificationsIds,
                query, pageNumber, pageSize, culturCode, showInStore, OfferIdToOrderBy);
            return result.EnumResult == GenericOperationResult.Success
               ? Ok(result)
               : (IActionResult)(result.EnumResult switch
               {
                   GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                   _ => throw new Exception(FromResx[result.ErrorMessages].Value)
               });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetProductDetails(int id)
        {
            ProductDetailsDto data = null;
            OperationResult<GenericOperationResult, ProductDetailsDto> result = null;
            string recordKey = $"{CacheKeys.GetOneProduct}_{id}";

            data = await _cache.GetRecordAsync<ProductDetailsDto>(recordKey);

            if (data is null)
            {
                result = await ProductRepository.GetDetailedAsync(id);

                await _cache.SetRecordAsync(recordKey, result.Result, result.DataIsValid());
            }
            else
            {
                result = new();
                result.Result = data;
            }

            return result.EnumResult == GenericOperationResult.Success
               ? Ok(result)
               : (IActionResult)(result.EnumResult switch
               {
                   GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                   GenericOperationResult.ValidationError => throw new BadRequestException(productLocalizer[result.ErrorMessages].Value),
                   GenericOperationResult.NotFound => throw new NotFoundException(productLocalizer[result.ErrorMessages].Value),
                   _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
               });
        }
        #endregion

        #region REMOVE

        [HttpDelete]
        // //[DynamicAuthorization("Product", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveProducts(string ids)
        {
            List<int> productIds = ids.ConvertJsonStringToObjectModal<int>();

            foreach (var productId in productIds)
                await _cache.RemoveRecordAsync($"{CacheKeys.GetOneProduct}_{productId}");
            
            var result = await ProductRepository.RemoveAsync(productIds);

            return result.EnumResult == GenericOperationResult.Success
               ? Ok(result)
               : (IActionResult)(result.EnumResult switch
               {
                   GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                   GenericOperationResult.ValidationError => throw new BadRequestException(productLocalizer[result.ErrorMessages].Value),
                   GenericOperationResult.NotFound => throw new NotFoundException(productLocalizer[result.ErrorMessages].Value),
                   _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
               });
        }
        [HttpDelete]
        //[DynamicAuthorization("Product", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveProductSizeColor(int Id)
        {
            var result = await ProductRepository.RemoveProductSizeColorAsync(Id);

            return result.EnumResult == GenericOperationResult.Success
               ? Ok(result)
               : (IActionResult)(result.EnumResult switch
               {
                   GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                   GenericOperationResult.ValidationError => throw new BadRequestException(productLocalizer[result.ErrorMessages].Value),
                   GenericOperationResult.NotFound => throw new NotFoundException(productLocalizer[result.ErrorMessages].Value),
                   _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
               });
        }
        #endregion

        #region POST ACTION PRODUCT

        [HttpPost]
        //[DynamicAuthorization("Product", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> AddProduct([FromForm] ProductFormViewModel viewModel)
        {
            if (viewModel.AttachedFiles.Count != 0)
            {
                var saveImaegInServiceFolder = await FilesExtensionMethods.SaveFiles(new FileOptions
                {
                    FileContentType = FileContentType.Image,
                    FileSourceType = FileSourceType.Product,
                    FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                    Files = viewModel.AttachedFiles,
                    HttpFileService = HttpFileService,
                    Mapper = Mapper,
                    SourceId = viewModel.Id,
                    ControllerName = nameof(Product)
                });

                if (saveImaegInServiceFolder is null)
                    throw new BadRequestException(sharedLocalizer[ErrorMessages.FailedSavingPhoto].Value);

                viewModel.Attachments = saveImaegInServiceFolder;
            }

            ProductFormDto formdto = new()
            {
                CategoryId = viewModel.CategoryId,
                CultureCode = viewModel.CultureCode,
                Name = viewModel.Name,
                Description = viewModel.Description,
                ProductSpecifications = viewModel.ProductSpecifications,
                BrandId = viewModel.BrandId,
                ShowInStore = viewModel.ShowInStore,
                Attachments = viewModel.Attachments,
                ProductSizeColors = viewModel.productSizeColors,
            };
            var result = await ProductRepository.AddProductAsync(formdto);
            if (result.EnumResult == GenericOperationResult.Success)
            {
                if (result.Result.RemovedFilesUrls != null)
                    _ = HttpFileService.RemoveFiles(result.Result.RemovedFilesUrls);

                return Ok(result);
            }
            else
            {
                if (viewModel.AttachedFiles != null && viewModel.AttachedFiles.Count > 0)
                    foreach (var image in formdto.Attachments)
                        await FilesExtensionMethods.RemoveFileFromWWWRoot(image.Url, HttpFileService);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(productLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(productLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
            };
        }
        [HttpPut]
        // //[DynamicAuthorization("Product", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> UpdateProduct([FromForm] ProductUpdateFormViewModel viewModel)
        {
            if (viewModel.Id != 0)
            {
                await _cache.RemoveRecordAsync($"{CacheKeys.GetOneProduct}_{viewModel.Id}");
            }

            if (viewModel.AttachedFiles.Count != 0)
            {
                var saveImaegInServiceFolder = await FilesExtensionMethods.SaveFiles(new FileOptions
                {
                    FileContentType = FileContentType.Image,
                    FileSourceType = FileSourceType.Product,
                    FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                    Files = viewModel.AttachedFiles,
                    HttpFileService = HttpFileService,
                    Mapper = Mapper,
                    SourceId = viewModel.Id,
                    ControllerName = nameof(Product)
                });

                viewModel.Attachments = saveImaegInServiceFolder;
            }

            ProductUpdateFormDto formdto = new()
            {
                Id = viewModel.Id,
                CategoryId = viewModel.CategoryId,
                CultureCode = viewModel.CultureCode,
                Name = viewModel.Name,
                Description = viewModel.Description,
                Currency = viewModel.Currency,
                RemovedFilesIds = viewModel.RemovedFilesIds,
                ProductSpecifications = viewModel.ProductSpecifications,
                BrandId = viewModel.BrandId,
                ShowInStore = viewModel.ShowInStore,
                Attachments = viewModel.Attachments,
            };
            var result = await ProductRepository.UpdateProductAsync(formdto);
            if (result.EnumResult == GenericOperationResult.Success)
            {
                if (result.Result.RemovedFilesUrls != null)
                    _ = HttpFileService.RemoveFiles(result.Result.RemovedFilesUrls);

                return Ok(result);
            }
            else
            {
                if (viewModel.AttachedFiles != null && viewModel.AttachedFiles.Count > 0)
                    foreach (var image in formdto.Attachments)
                        await FilesExtensionMethods.RemoveFileFromWWWRoot(image.Url, HttpFileService);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(productLocalizer[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(productLocalizer[result.ErrorMessages].Value),
                _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
            };
        }

        [HttpPost]
        //[DynamicAuthorization("Product", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> AddProductColorSize(ProductSizeColorDto formDto)
        {
            var result = await ProductRepository.AddProductSizeColorAsync(formDto);

            return result.EnumResult == GenericOperationResult.Success
               ? Ok(result)
               : (IActionResult)(result.EnumResult switch
               {
                   GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                   GenericOperationResult.ValidationError => throw new BadRequestException(productLocalizer[result.ErrorMessages].Value),
                   GenericOperationResult.NotFound => throw new NotFoundException(productLocalizer[result.ErrorMessages].Value),
                   _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
               });
        }

        [HttpPut]
        // //[DynamicAuthorization("Product", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> UpdateProductSizeColor(ProductSizeColorDto formDto)
        {
            var result = await ProductRepository.UpdateProductSizeColorAsync(formDto);

            return result.EnumResult == GenericOperationResult.Success
                 ? Ok(result)
                 : (IActionResult)(result.EnumResult switch
                 {
                     GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                     GenericOperationResult.ValidationError => throw new BadRequestException(productLocalizer[result.ErrorMessages].Value),
                     GenericOperationResult.NotFound => throw new NotFoundException(productLocalizer[result.ErrorMessages].Value),
                     _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
                 });
        }

        [HttpPost]
        //[DynamicAuthorization("Product", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionProductReview(ProductReviewDto formDto)
        {
            var result = await ProductRepository.ActionProductReviewAsync(formDto);

            return result.EnumResult == GenericOperationResult.Success
              ? Ok(result)
              : (IActionResult)(result.EnumResult switch
              {
                  GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                  GenericOperationResult.ValidationError => throw new BadRequestException(productLocalizer[result.ErrorMessages].Value),
                  GenericOperationResult.NotFound => throw new NotFoundException(productLocalizer[result.ErrorMessages].Value),
                  _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
              });
        }

        [HttpDelete]
        // //[DynamicAuthorization("Product", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveProductReview(int PersonId)
        {
            var result = await ProductRepository.RemoveProductReviewAsync(PersonId);

            return result.EnumResult == GenericOperationResult.Success
              ? Ok(result)
              : (IActionResult)(result.EnumResult switch
              {
                  GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                  GenericOperationResult.ValidationError => throw new BadRequestException(productLocalizer[result.ErrorMessages].Value),
                  GenericOperationResult.NotFound => throw new NotFoundException(productLocalizer[result.ErrorMessages].Value),
                  _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
              });
        }
        #endregion

        #region Show or Hide Product 

        [HttpGet]
        //[DynamicAuthorization("Product", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ShowInStore(int productId, bool showInStore)
        {
            if (productId != 0)
                await _cache.RemoveRecordAsync($"{CacheKeys.GetOneProduct}_{productId}");

            var result = await ProductRepository.ShowInStoreAsync(productId, showInStore);

            return result.EnumResult == GenericOperationResult.Success
               ? Ok(result)
               : (IActionResult)(result.EnumResult switch
               {
                   GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                   GenericOperationResult.ValidationError => throw new BadRequestException(productLocalizer[result.ErrorMessages].Value),
                   GenericOperationResult.NotFound => throw new NotFoundException(productLocalizer[result.ErrorMessages].Value),
                   _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
               });
        }
        #endregion

        #region Adding Products After Reading from Excel File
        [HttpPost]
        [Route("[action]")]
        //[DynamicAuthorization("Product", nameof(ActionType.Action))]
        public async Task<IActionResult> AddingProducts(IEnumerable<ProductExcelDto> productList)
        {
            var result = await ProductRepository.AddingProductsAfterReadingExcelFileAsync(productList);

            return result.EnumResult == GenericOperationResult.Success
               ? Ok(result)
               : (IActionResult)(result.EnumResult switch
               {
                   GenericOperationResult.Failed => throw new InternalServerException(sharedLocalizer[result.ErrorMessages].Value),
                   GenericOperationResult.ValidationError => throw new BadRequestException(productLocalizer[result.ErrorMessages].Value),
                   GenericOperationResult.NotFound => throw new NotFoundException(productLocalizer[result.ErrorMessages].Value),
                   _ => throw new Exception(sharedLocalizer[result.ErrorMessages].Value)
               });
        }

        #endregion

        #region Get Products By Ids
        [HttpGet]
        [Route("[action]")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> GetProductsByIds(string ids)
        {
            List<int> productIds = ids.ConvertJsonStringToObjectModal<int>();

            var result = await ProductRepository.GetProductsByIdToExportToExcelFileAsync(productIds);

            return result.EnumResult == GenericOperationResult.Success
               ? Ok(result)
               : (IActionResult)(result.EnumResult switch
               {
                   GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                   GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                   GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                   _ => throw new Exception(FromResx[result.ErrorMessages].Value)
               });
        }
        #endregion

        #region WebHook
        [HttpPost]
        [Route("[action]")]
        public IActionResult PaymentWebHook(PaymentViewModel viewModel)
        {
            LogService.Log(new HITLog() { Message = JsonConvert.SerializeObject(viewModel) });
            LogService.Log(new HITLog() { Message = "after payment log" });
            return Ok();
        }
        #endregion
    }
}

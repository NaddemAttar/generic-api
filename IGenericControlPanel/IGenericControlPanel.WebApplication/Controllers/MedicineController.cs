﻿
using AutoMapper;

using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Core.Dto.Medicine;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Util;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class MedicineController : LocalizedBaseController<MedicineController>
    {
        #region Properties & Constructor
        private IMedicineRepository MedicineRepository { get; }
        private IHttpFileService HttpFileService { get; }
        private IMapper Mapper { get; }
        public MedicineController(
            IMedicineRepository medicineRepository,
            IHttpFileService httpFileService,
            IMapper mapper,
            IConfiguration configuration,
            IStringLocalizer<MedicineController> localizer) : base(localizer, configuration)
        {
            MedicineRepository = medicineRepository;
            HttpFileService = httpFileService;
            Mapper = mapper;
        }
        #endregion

        #region Action
        [HttpPost]
        //[DynamicAuthorization("Medicine", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> Action([FromForm] MedicineFormDto formDto)
        {
            if (formDto.ImagesForm.Count != 0)
            {
                var saveFiles = await FilesExtensionMethods
                  .SaveFiles(new Util.FileOptions
                  {
                      FileContentType = FileContentType.Image,
                      FileSourceType = FileSourceType.Medicine,
                      FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                      Files = formDto.ImagesForm,
                      HttpFileService = HttpFileService,
                      Mapper = Mapper,
                      SourceId = formDto.Id,
                      ControllerName = nameof(Model.Core.Medicine)
                  });

                if (saveFiles is null)
                {
                    throw new BadRequestException(FromResx["FilesError"].Value);
                }

                formDto.Images = saveFiles;
            }

            var result = MedicineRepository.Action(formDto);

            if (result.EnumResult == GenericOperationResult.Success)
            {
                if (result.Result.RemovedFilesUrls != null)
                {
                    _ = HttpFileService.RemoveFiles(result.Result.RemovedFilesUrls);
                }

                return Ok(result);
            }

            return result.EnumResult switch
            {
                GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                _ => throw new Exception(FromResx["InternalServerError"].Value)
            };
        }

        [HttpPost]
        //[DynamicAuthorization("Medicine", nameof(ActionType.Action))]
        [Route("[action]")]
        public IActionResult AddPatientMedicine(int medicineId, int userId = 0)
        {
            var result = MedicineRepository.AddPatientMedicine(medicineId, userId);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        #endregion

        #region Get
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetAllMedicines(
            int? userId, bool enablePagination = true, int pageSize = 10, int pageNumber = 0)
        {
            var result = MedicineRepository.GetAllMedicines(userId, enablePagination, pageSize, pageNumber);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetAllPatientMedicines(
            int? userId, bool enablePagination = true, int pageSize = 10, int pageNumber = 0)
        {
            var result = await MedicineRepository.GetAllPatientMedicines(
                userId, enablePagination, pageSize, pageNumber);
            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetMedicineAutoComplete(string query = "")
        {
            var result = MedicineRepository.GetMedicineAutoComplete(query);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetMedicine(int Id)
        {
            var result = MedicineRepository.GetMedicine(Id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion

        #region Remove
        [HttpDelete]
        //[DynamicAuthorization("Medicine", nameof(ActionType.Remove))]
        [Route("[action]")]
        public IActionResult RemoveMedicine(int Id)
        {
            var result = MedicineRepository.RemoveMedicine(Id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        [HttpDelete]
        //[DynamicAuthorization("Medicine", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemovePatientMedicine(int Id)
        {
            var result = await MedicineRepository.RemovePatientMedicine(Id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion

    }
}

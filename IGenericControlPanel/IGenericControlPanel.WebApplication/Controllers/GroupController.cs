﻿
using AutoMapper;

using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Core.Dto.Group;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Util;
using IGenericControlPanel.WebApplication.ViewModels.Group;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class GroupController : LocalizedBaseController<GroupController>
    {
        #region Properties and Constructor
        private IGroupRepository GroupRepository { get; }
        private IHttpFileService HttpFileService { get; }
        private IMapper Mapper { get; }

        public GroupController(IGroupRepository groupRepository,
            IHttpFileService httpFileService,
             IMapper mapper,
             IConfiguration configuration,
             IDistributedCache cache,
            IUserRepository userRepository) : base(configuration, cache, userRepository)
        {
            GroupRepository = groupRepository;
            HttpFileService = httpFileService;
            Mapper = mapper;
        }
        #endregion

        #region Get ALL Groups
        [HttpGet]
        //  [Authorize(AuthenticationSchemes = "Bearer")]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetGroups(int? pageNumber = 0, int? pageSize = 6)
        {
            var userInfo = await _userRepository.GetCurrentUserInfo();
            IEnumerable<GroupDto> data = null;
            OperationResult<GenericOperationResult,
                IEnumerable<GroupDto>> result = null;
            string recordKey = CacheKeys.GetGroups.GetKeyWithPagesAsync(
                userInfo.IsHealthCareProvider, userInfo.CurrentUserId, pageNumber, pageSize);

            data = await _cache.GetRecordAsync<IEnumerable<GroupDto>>(recordKey);

            if (data is null)
            {
                result = await GroupRepository.GetAllPagedAsync(pageNumber, pageSize);

                await _cache.SetRecordAsync(
                    recordKey, result.Result, result.IEnumerableDataIsValid(),
                    TimeSpan.FromSeconds(30));
            }
            else
            {
                result = new();
                result.Result = data;
            }

            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

            return Ok(result);
        }
        [HttpGet]
        //  [Authorize(AuthenticationSchemes = "Bearer")]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetAllGroupMembers(
            int GroupId, int? pageNumber = 0, int? pageSize = 6, bool enablePagination = false)
        {
            var result = await GroupRepository.GetAllGroupMembersAsync(
                GroupId, pageNumber, pageSize, enablePagination);

            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

            return Ok(result);
        }
        #endregion

        #region Get Group Details
        [HttpGet]
        // [Authorize(AuthenticationSchemes = "Bearer")]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetGroupDetails(int groupId)
        {
            GroupDetailsDto data = null;
            OperationResult<GenericOperationResult, GroupDetailsDto> result = null;
            string recordKey = $"{CacheKeys.GetOneGroup}_{groupId}";

            data = await _cache.GetRecordAsync<GroupDetailsDto>(recordKey);

            if (data is null)
            {
                result = await GroupRepository.GetDetailedAsync(groupId);

                await _cache.SetRecordAsync(recordKey, result.Result,
                    result.DataIsValid());
            }
            else
            {
                result = new();
                result.Result = data;
            }

            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

            return Ok(result);
        }
        #endregion

        #region Action Group
        [HttpPost]
        //[DynamicAuthorization("Group", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> GroupAction([FromForm] GroupViewModel groupViewModel)
        {
            if (groupViewModel.Id != 0)
            {
                await _cache.RemoveRecordAsync($"{CacheKeys.GetOneGroup}_{groupViewModel.Id}");
            }

            var groupImage = await FilesExtensionMethods
                  .SaveFile(new Util.FileOptions
                  {
                      FileContentType = FileContentType.Image,
                      FileSourceType = FileSourceType.Group,
                      FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                      File = groupViewModel.GroupImage,
                      HttpFileService = HttpFileService,
                      Mapper = Mapper,
                      ControllerName = nameof(Model.Core.Group)
                  });

            var coverImage = await FilesExtensionMethods
                  .SaveFile(new Util.FileOptions
                  {
                      FileContentType = FileContentType.Attachement,
                      FileSourceType = FileSourceType.Group,
                      FilePath = Configuration.GetValue<string>(ConfigurationNames.AttachmentsPath),
                      File = groupViewModel.CoverImage,
                      HttpFileService = HttpFileService,
                      Mapper = Mapper,
                      ControllerName = nameof(Model.Core.Group)
                  });

            GroupFormDto groupFormDto = new()
            {
                Name = groupViewModel.Name,
                Description = groupViewModel.Description,
                GroupPrivacyLevel = groupViewModel.GroupPrivacyLevel,
                GroupImage = groupImage,
                CoverImage = coverImage,
                MultiLevelCategoryId = groupViewModel.MultiLevelCategoryId,
                Id = groupViewModel.Id ?? 0
            };

            var result = await GroupRepository.ActionAsync(groupFormDto);

            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

            return Ok(result);
        }

        [HttpPost]
        //[DynamicAuthorization("Group", nameof(ActionType.Action))]
        [Route("[action]")]
        public async Task<IActionResult> ActionGroupMember([FromForm] GroupMemberFormDto groupMemberFormDto)
        {
            var result = await GroupRepository.ActionGroupMemberAsync(groupMemberFormDto);
            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

            return Ok(result);
        }
        #endregion

        #region Remove Group

        [HttpDelete]
        //[DynamicAuthorization("Group", nameof(ActionType.Remove))]
        [Route("[action]")]
        public async Task<IActionResult> RemoveGroup(int? id)
        {
            await _cache.RemoveRecordAsync($"{CacheKeys.GetOneGroup}_{id}");

            var result = await GroupRepository.RemoveAsync(id ?? 0);

            if (result.EnumResult != GenericOperationResult.Success)
                result.EnumResult.GenericThrowExceptionError(FromResx[result.ErrorMessages].Value);

            return Ok(result);
        }
        #endregion
    }
}

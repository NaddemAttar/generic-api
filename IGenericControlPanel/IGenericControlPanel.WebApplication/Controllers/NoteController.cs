﻿
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Note;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Utils;
using IGenericControlPanel.SharedUI.Controllers;
using IGenericControlPanel.WebApplication.Middleware;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class NoteController : LocalizedBaseController<NoteController>
    {
        #region Properties & Constructor
        private INoteRepository NoteRepository { get; }
        public NoteController(
            INoteRepository NoteRepository,
            IStringLocalizer<NoteController> localizer) : base(localizer)
        {
            this.NoteRepository = NoteRepository;
        }
        #endregion

        #region Action
        [HttpPost]
        //[DynamicAuthorization("Note", nameof(ActionType.Action))]
        [Route("[action]")]
        public IActionResult Action(NoteDto formDto)
        {
            var result = NoteRepository.Action(formDto);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        [HttpPost]
        //[DynamicAuthorization("Note", nameof(ActionType.Action))]
        [Route("[action]")]
        public IActionResult ActionUserNote(IEnumerable<UserNoteDto> formDto, int UserId)
        {
            var result = NoteRepository.ActionUserNote(formDto, UserId);

            return result.IsSuccess ? Ok(result) : result.IsFailure ? BadRequest(result) : StatusCode(500);
        }
        #endregion

        #region Get
        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetAllNotes()
        {
            var result = NoteRepository.GetAllNotes();

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetAllPersonNotes(int? userId)
        {
            var result = await NoteRepository.GetAllUserNotes(userId);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("[action]")]
        public IActionResult GetNote(int Id)
        {
            var result = NoteRepository.GetNote(Id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion

        #region Remove
        [HttpDelete]
        //[DynamicAuthorization("Note", nameof(ActionType.Remove))]
        [Route("[action]")]
        public IActionResult Remove(int Id)
        {
            var result = NoteRepository.RemoveNote(Id);

            return result.IsSuccess ? Ok(result) : result.IsFailure ? BadRequest(result) : StatusCode(500);
        }
        [HttpDelete]
        //[DynamicAuthorization("Note", nameof(ActionType.Remove))]
        [Route("[action]")]
        public IActionResult RemoveUserNote(int Id)
        {
            var result = NoteRepository.RemoveUserNote(Id);

            return result.EnumResult == GenericOperationResult.Success
                ? Ok(result)
                : (IActionResult)(result.EnumResult switch
                {
                    GenericOperationResult.Failed => throw new InternalServerException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.ValidationError => throw new BadRequestException(FromResx[result.ErrorMessages].Value),
                    GenericOperationResult.NotFound => throw new NotFoundException(FromResx[result.ErrorMessages].Value),
                    _ => throw new Exception(FromResx["InternalServerError"].Value)
                });
        }
        #endregion
    }
}

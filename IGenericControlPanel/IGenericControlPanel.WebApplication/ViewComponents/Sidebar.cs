﻿using IGenericControlPanel.WebApplication.Util;
using IGenericControlPanel.WebApplication.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.WebApplication.ViewComponents
{
    public class Sidebar : ViewComponent
    {
        public Sidebar()
        {

        }


        public IViewComponentResult Invoke()
        {
            return View(PartialViewPaths.Sidebar, new SidebarViewModel(""));
        }
    }
}

﻿using IGenericControlPanel.Security.IData;
using IGenericControlPanel.WebApplication.Util;
using IGenericControlPanel.WebApplication.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.WebApplication.ViewComponents
{
    public class Header : ViewComponent
    {
        private IAccountRepository AccountRepository { get; }
        public Header(IAccountRepository accountRepository)
        {
            AccountRepository = accountRepository;
        }

    }
}

﻿using IGenericControlPanel.Security.IData;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.WebApplication.ViewComponents
{
    public class User : ViewComponent
    {
        private IAccountRepository AccountRepository { get; }

        public User(IAccountRepository accountRepository)
        {
            AccountRepository = accountRepository;
        }
    }
}

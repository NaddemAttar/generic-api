﻿using AutoMapper;
using FluentValidation.AspNetCore;
using IGenericControlPanel.Core.Data.Repositories;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.WebApplication.Configurations;
using IGenericControlPanel.WebApplication.Middleware;
using IGenericControlPanel.WebApplication.Util;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Globalization;
using System.Text;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration; // allows both to access and to set up the config

builder.Services.AddTransient<ExceptionHandlingMiddleware>();
builder.Services.AddTransient<DynamicAuthorizationMiddleware>();
builder.Logging.AddLog4Net();
IMapper mapper = builder.Services.ConfigureMapping().CreateMapper();
builder.Services.AddSingleton(mapper);

builder.Services.AddCors(options =>
{
});

builder.Services.AddHttpContextAccessor();
builder.Services.AddHttpClient();
builder.Services.AddControllersWithViews().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore)
                .AddFluentValidation(fv =>
                {
                    fv.RegisterValidatorsFromAssemblyContaining<Program>();
                });

builder.Services.Configure<DataProtectionTokenProviderOptions>(opts => opts.TokenLifespan = TimeSpan.FromHours(10));
builder.Services.Configure<CookiePolicyOptions>(options =>
{
    options.CheckConsentNeeded = context => true;
    options.MinimumSameSitePolicy = SameSiteMode.None;
});
builder.Services.Configure<MailSettings>(configuration.GetSection("MailSettings"));
string cachingIsAllowed = builder.Configuration.GetSection("CachingIsAllowed").Value;
DistributedCacheExtensions.CachingIsAllowed = bool.Parse(cachingIsAllowed);

builder.Services.AddTransient<IMailRepository, MailRepository>();
builder.Services.AddLocalization();
builder.Services.Configure<RequestLocalizationOptions>(options =>
{
    var supportedCuture = new[]
    {
                    new CultureInfo("en"),
                    new CultureInfo("ar"),
    };

    options.DefaultRequestCulture = new RequestCulture(culture: "en", uiCulture: "en");
    options.SupportedCultures = supportedCuture;
    options.SupportedUICultures = supportedCuture;

});

builder.Services
   .AddDbContext<CPDbContext>(options =>
   {
       var connectionString = builder.Configuration.GetConnectionString("DevelopmentConnection");
       options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
   });

builder.Services.BeginCustomConfiguration()
                      .AddDatabaseContextConfiguration(configuration)
                      .AddIdentityConfiguration()
                      .AddApplicationDependenciesConfiguration()
                      .AddLoggerConfiguration();
builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;

}).AddJwtBearer(options =>
 {
     options.SaveToken = true;
     options.RequireHttpsMetadata = false;
     options.Events = new JwtBearerEvents
     {   
         OnChallenge = context =>
         {
             context.Response.StatusCode = 401;
             return Task.CompletedTask;
         }
     };
     options.TokenValidationParameters = new TokenValidationParameters()
     {
         ValidateIssuer = true,
         ValidateAudience = true,
         ValidAudience = configuration["JWT:ValidAudience"],
         ValidIssuer = configuration["JWT:ValidIssuer"],
         IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]))
     };
 });
builder.Services.ConfigureApplicationCookie(options =>
{
    options.Events.OnRedirectToLogin = context =>
    {
        context.Response.StatusCode = 401;

        return Task.CompletedTask;
    };
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.CustomSchemaIds(type => type.ToString());
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "HIT.GenericDashboard API",
        Description = "An ASP.NET Core Web API for managing HIT Dashboard"
    });
    OpenApiSecurityScheme jwtSecurityScheme = new()
    {
        Scheme = "bearer",
        BearerFormat = "JWT",
        Name = "JWT Authentication",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.Http,
        Description = "Put **_ONLY_** your JWT Bearer token on textbox below!",

        Reference = new OpenApiReference
        {
            Id = JwtBearerDefaults.AuthenticationScheme,
            Type = ReferenceType.SecurityScheme
        }

    };

    options.AddSecurityDefinition(jwtSecurityScheme.Reference.Id, jwtSecurityScheme);

    options.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        { jwtSecurityScheme, Array.Empty<string>() }
    });
});

builder.Services.AddStackExchangeRedisCache(options =>
{
    options.Configuration = configuration.GetConnectionString("Redis");
    options.InstanceName = "IGenericControlPanel_";
});

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var dbContext = scope.ServiceProvider.GetRequiredService<CPDbContext>();
    dbContext.Database.Migrate();
}

app.UseForwardedHeaders(new ForwardedHeadersOptions
{
    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
});

_ = app.UseDeveloperExceptionPage();
_ = app.UseSwagger();
_ = app.UseSwaggerUI(options =>
{
    options.SwaggerEndpoint("/swagger/v1/swagger.json", " v1");
});
if (!app.Environment.IsDevelopment())
{
    app.UseHsts();
}

string dir = Path.Combine(app.Environment.ContentRootPath + "/wwwroot", configuration.GetValue<string>
                         (ConfigurationNames.CarouselPath));

if (!Directory.Exists(dir))
{
    Directory.CreateDirectory(dir);
}

dir = Path.Combine(app.Environment.ContentRootPath + "/wwwroot", configuration.GetValue<string>
                        (ConfigurationNames.BlogPath));
if (!Directory.Exists(dir))
{
    _ = Directory.CreateDirectory(dir);
}

dir = Path.Combine(app.Environment.ContentRootPath + "/wwwroot", configuration.GetValue<string>
             (ConfigurationNames.AttachmentsPath));
if (!Directory.Exists(dir))
{
    _ = Directory.CreateDirectory(dir);
}

dir = Path.Combine(app.Environment.ContentRootPath + "/wwwroot", configuration.GetValue<string>
             (ConfigurationNames.QuestionPath));
if (!Directory.Exists(dir))
{
    Directory.CreateDirectory(dir);
}

app.UseCors(cors => cors
              .AllowAnyMethod()
              .AllowAnyHeader()
              .SetIsOriginAllowed(origin => true) // allow any origin
              .AllowCredentials()); // allow credentials

app.UseHttpsRedirection();

app.UseStaticFiles();


var locOptions = ((IApplicationBuilder)app).ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();

app.UseRequestLocalization(locOptions.Value);


//app.ConfigureCustomDynamicAuthMiddleware();
app.ConfigureCustomExceptionMiddleware();

app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();
app.UseMiddleware<DynamicAuthorizationMiddleware>();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});

app.Run();
public partial class Program { }
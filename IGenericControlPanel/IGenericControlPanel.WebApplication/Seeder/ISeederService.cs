﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.WebApplication.Seeder
{
    public interface ISeederService
    {
        Task CreateRolesAsync();
        Task CreateUsers();
        Task SeedWebContentAsync();
        Task CreateSettings();
        Task SeedPrivacyMaterials();
        Task SeedCountriesAndCities();
        Task SeedOpeningHoures();
        Task SeedHcpAndNormalUserPoints();
    }
}

﻿using FluentValidation;

using IGenericControlPanel.Consultation.Dto.ConsultationDto;
using IGenericControlPanel.SharedKernal.Enums.Consultations;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Controllers;
using IGenericControlPanel.WebApplication.FluentValidation.CustomValidators;

using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation;
public class HealthProviderQuestionsFormDtoValidator :
AbstractValidator<HealthProviderQuestionsFormDto>
{
    public HealthProviderQuestionsFormDtoValidator(IStringLocalizer<HealthProviderConsultationController> FromResx)
    {
        RuleFor(rule => rule.ConsultationChoiceIds.Count())
            .NotEqual(0)
            .When(rule => rule.ConsultationQuestionType
            == ConsultationQuestionType.MultiChoices)
            .WithMessage(FromResx[ErrorMessages.MustChooseConsultationChoices]);

        RuleFor(rule => rule.ConsultationChoiceIds.Count())
            .Equal(0)
            .When(rule => rule.ConsultationQuestionType
            != ConsultationQuestionType.MultiChoices && rule.ConsultationQuestionType!=ConsultationQuestionType.SingleChoice)
            .WithMessage(FromResx[ErrorMessages.MustNotChooseConsultationChoices]);

        RuleFor(rule => rule.QuestionText)
            .NotEmptyCustom(FromResx[ErrorMessages.QuestionTextRequired]);
    }
}

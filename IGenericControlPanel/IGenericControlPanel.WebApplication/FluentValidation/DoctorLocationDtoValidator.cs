﻿using FluentValidation;

using IGenericControlPanel.Core.Dto.Location;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Controllers;

using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class DoctorLocationDtoValidator : AbstractValidator<DoctorLocationDto>
    {
        public DoctorLocationDtoValidator(IStringLocalizer<LocationController> FromResx)
        {
            RuleFor(dl => dl.Longitude).NotEmpty().WithMessage(FromResx[ErrorMessages.MustChooseLongitude]);
            RuleFor(dl => dl.Latitude).NotEmpty().WithMessage(FromResx[ErrorMessages.MustChooseLatidude]);
            RuleFor(dl => dl.AddressInfo).NotEmpty().WithMessage(FromResx[ErrorMessages.MustAddingAddressInfo]);
        }
    }
}

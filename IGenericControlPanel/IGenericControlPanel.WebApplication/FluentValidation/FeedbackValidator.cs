﻿using FluentValidation;

using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Controllers;
using IGenericControlPanel.WebApplication.ViewModels.Feedback;

using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class FeedbackValidator : AbstractValidator<FeedbackViewModel>
    {
        public FeedbackValidator(IStringLocalizer<FeedbackController> FromResx)
        {
            RuleFor(b => b.ServiceId).NotEmpty()
               .WithMessage(FromResx[ErrorMessages.FeedbackServcieIdRequired]);
            RuleFor(b => b.ServiceId).NotEqual(0)
               .WithMessage(FromResx[ErrorMessages.FeedbackServiceIdNotEquleZero]);
        }
    }
}

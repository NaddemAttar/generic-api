﻿using FluentValidation;

using IGenericControlPanel.Core.Dto.Surger;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Controllers;

using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class SurgeryValidator : AbstractValidator<SurgeryDto>
    {
        public SurgeryValidator(IStringLocalizer<SurgeryController> FromResx)
        {
            RuleFor(b => b.Name).NotEmpty()
                .WithMessage(FromResx[ErrorMessages.SurgeryNameRequired]);
            RuleFor(b => b.Description).NotEmpty()
                .WithMessage(FromResx[ErrorMessages.SurgeryDescriptionRequired]);

        }
    }
}

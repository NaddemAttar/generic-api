﻿using FluentValidation;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.ViewModels.Service;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class ServiceValidator : AbstractValidator<ServiceViewModel>
    {
        public ServiceValidator(IStringLocalizer<ServiceResource> serviceLocalizer)
        {
            RuleFor(b => b.Description).NotEmpty()
                .WithMessage(serviceLocalizer[ErrorMessages.ServiceDescriptionRequired]);

            RuleFor(b => b.Name).NotEmpty()
                .WithMessage(serviceLocalizer[ErrorMessages.ServiceNameRequired]);

            RuleFor(b => b.Images).NotNull()
                .WithMessage(serviceLocalizer[ErrorMessages.ServiceImageRequired]);
        }
    }
}
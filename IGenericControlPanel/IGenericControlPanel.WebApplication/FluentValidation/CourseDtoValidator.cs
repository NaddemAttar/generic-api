﻿using FluentValidation;
using IGenericControlPanel.Core.Dto.Course;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class CourseDtoValidator : AbstractValidator<CourseFormDto>
    {
        public CourseDtoValidator(IStringLocalizer<CourseResource> courseLocalizer)
        {
            RuleFor(b => b.Name).NotEmpty()
                .WithMessage(courseLocalizer[ErrorMessages.CourseNameRequired]);

            RuleFor(b => b.Description).NotEmpty()
                .WithMessage(courseLocalizer[ErrorMessages.CourseDescriptionRequired]);

            RuleFor(b => b.CourseTypeIds).NotEmpty()
                .WithMessage(courseLocalizer[ErrorMessages.CourseTypeRequired]);
        }
    }
}
﻿using FluentValidation;
using IGenericControlPanel.Core.Dto.Diseases;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class CourseTypeValidator : AbstractValidator<DiseasesFormDto>
    {
        public CourseTypeValidator(IStringLocalizer<CourseResource> courseLocalizer)
        {
            RuleFor(b => b.Name).NotEmpty()
                .WithMessage(courseLocalizer[ErrorMessages.CourseTypeNameRequired].Value);
        }
    }
}
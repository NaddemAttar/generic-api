﻿using FluentValidation;
using IGenericControlPanel.Core.Dto.Diseases;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class DiseasesValidator : AbstractValidator<DiseasesFormDto>
    {
        public DiseasesValidator(IStringLocalizer<DiseasesResource> diseasesLocalizer)
        {
            RuleFor(b => b.Name).NotEmpty()
                .WithMessage(diseasesLocalizer[ErrorMessages.DiseaseNameRequired].Value);

            RuleFor(b => b.Name).NotEmpty()
                .WithMessage(diseasesLocalizer[ErrorMessages.DiseaseDescriptionRequired].Value);
        }
    }
}
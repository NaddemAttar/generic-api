﻿using FluentValidation;

using IGenericControlPanel.HR.Dto;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Controllers;
using IGenericControlPanel.WebApplication.FluentValidation.CustomValidators;

using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class TeamMemberFormDtoValidator : AbstractValidator<TeamMemberFormDto>
    {
        public string PhoneNumberMatcher = "\\(?\\d{3}\\)?[-\\.]? *\\d{3}[-\\.]? *[-\\.]?\\d{4}";
        public TeamMemberFormDtoValidator(IStringLocalizer<TeamMemberController> FromResx)
        {
            RuleFor(tm => tm.Name).NotEmptyCustom(FromResx[ErrorMessages.FirstNameIsRequired]);
            RuleFor(tm => tm.LastName).NotEmptyCustom(FromResx[ErrorMessages.LastNameIsRequired]);
            RuleFor(tm => tm.Name).NotEqual(tm => tm.LastName)
                .WithMessage(FromResx[ErrorMessages.FirstNameMustNotEqualLastName]);
            RuleFor(tm => tm.About).NotEmptyCustom(FromResx[ErrorMessages.AboutIsRequired]);
            RuleFor(tm => tm.PhoneNumber).NotEmptyCustom(FromResx[ErrorMessages.PhoneNumberRequired])
                .Matches(PhoneNumberMatcher).WithMessage(FromResx[ErrorMessages.PhoneNumberNotValid]);
            RuleFor(tm => tm.Email).NotEmptyCustom(FromResx[ErrorMessages.EmailIsRequired])
                .EmailAddress().WithMessage(FromResx[ErrorMessages.EmailNotValid]);
            RuleFor(tm => tm.Description).NotEmptyCustom(FromResx[ErrorMessages.DescriptionIsRequired]);
            RuleFor(tm => tm.Position).NotEmptyCustom(FromResx[ErrorMessages.PositionIsRequired]);
        }
    }

}

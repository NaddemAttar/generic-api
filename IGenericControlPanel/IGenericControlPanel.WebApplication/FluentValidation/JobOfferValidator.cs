﻿using FluentValidation;

using IGenericControlPanel.Core.Dto.JobOffer;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Controllers;

using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class JobOfferValidator : AbstractValidator<JobOfferFormDto>
    {
        public JobOfferValidator(IStringLocalizer<JobOfferController> FromResx)
        {
            RuleFor(b => b.Title).NotEmpty()
                .WithMessage(FromResx[ErrorMessages.JobOfferTitleRequired]);
            RuleFor(b => b.Responsibilty).NotEmpty()
               .WithMessage(FromResx[ErrorMessages.JobOfferResponsibiltyRequired]);
            RuleFor(b => b.ContractType).NotNull()
               .WithMessage(FromResx[ErrorMessages.JobOfferContractTypeRequired]);
            RuleFor(b => b.ExpirationDate).NotEmpty()
               .WithMessage(FromResx[ErrorMessages.JobOfferExpirationDateRequired]);
            RuleFor(b => b.Qualification).NotEmpty()
               .WithMessage(FromResx[ErrorMessages.JobOfferQualificationRequired]);
        }
    }
}

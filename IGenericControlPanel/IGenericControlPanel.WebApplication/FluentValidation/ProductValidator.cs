﻿using FluentValidation;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.ViewModels.Product;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class ProductValidator : AbstractValidator<ProductFormViewModel>
    {
        public ProductValidator(IStringLocalizer<ProductResource> productLocalizer)
        {
            RuleFor(b => b.Name).NotEmpty()
                .WithMessage(productLocalizer[ErrorMessages.ProductNameRequired]);

            RuleFor(b => b.productSizeColors).NotEmpty()
               .WithMessage(productLocalizer[ErrorMessages.ProductSizeColorRequired]);

            RuleFor(b => b.BrandId).NotEmpty()
               .WithMessage(productLocalizer[ErrorMessages.ProductBrandIdRequired]);

            RuleFor(b => b.BrandId).NotEqual(0)
               .WithMessage(productLocalizer[ErrorMessages.ProductBrandIdNotEquleZero]);

            RuleFor(b => b.CategoryId).NotEmpty()
               .WithMessage(productLocalizer[ErrorMessages.ProductCategoryIdRequired]);

            RuleFor(b => b.CategoryId).NotEqual(0)
               .WithMessage(productLocalizer[ErrorMessages.ProductCategoryIdNotEquleZero]);
        }
    }
}
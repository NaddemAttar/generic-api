﻿using FluentValidation;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.ViewModels.Account;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class AccountLoginValidator : AbstractValidator<LoginViewModel>
    {
        public AccountLoginValidator(IStringLocalizer<AccountResource> accountLocalizer)
        {
            RuleFor(b => b.Username).NotEmpty()
                .WithMessage(accountLocalizer[ErrorMessages.UsernameCanNotBeNull].Value);

            RuleFor(b => b.Username).MinimumLength(3)
                .WithMessage(accountLocalizer[ErrorMessages.UsernameMustNotBeLessThan3Characters].Value);

            RuleFor(b => b.Password).NotEmpty()
                .WithMessage(accountLocalizer[ErrorMessages.PasswordCanNotBeNull].Value);

            RuleFor(b => b.Password).MinimumLength(6)
                .WithMessage(accountLocalizer[ErrorMessages.PasswordMustNotBeLessThan6Characters].Value);
        }
    }
}
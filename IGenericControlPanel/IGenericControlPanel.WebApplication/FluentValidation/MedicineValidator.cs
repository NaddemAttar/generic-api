﻿using FluentValidation;

using IGenericControlPanel.Core.Dto.Medicine;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Controllers;

using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class MedicineValidator : AbstractValidator<MedicineFormDto>
    {
        public MedicineValidator(IStringLocalizer<MedicineController> FromResx)
        {
            RuleFor(b => b.Name).NotEmpty()
                .WithMessage(FromResx[ErrorMessages.MedicineNameRequired]);


        }
    }
}

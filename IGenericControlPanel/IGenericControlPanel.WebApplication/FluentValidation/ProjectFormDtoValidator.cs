﻿using FluentValidation;

using IGenericControlPanel.Core.Dto.Project;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Controllers;

using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class ProjectFormDtoValidator : AbstractValidator<ProjectFormDto>
    {
        public string YoutubeLinkMatcher = "^(https?\\:\\/\\/)?(www\\.youtube\\.com|youtu\\.be)\\/.+$";
        public ProjectFormDtoValidator(IStringLocalizer<ProjectController> FromResx)
        {
            RuleFor(p => p.Title).NotEmpty().WithMessage(FromResx[ErrorMessages.ProjectTitleRequired]);
            RuleFor(p => p.Description)
                .NotEmpty()
                .WithMessage(FromResx[ErrorMessages.ProjectDescriptionRequired])
                .MinimumLength(25)
                .WithName(FromResx[ErrorMessages.ProjectDescriptionLengthMustBeAtLeast25Char]);

            When(p => !string.IsNullOrEmpty(p.YoutubeLink), () =>
            {
                RuleFor(p => p.YoutubeLink).Matches(YoutubeLinkMatcher)
               .WithMessage(FromResx[ErrorMessages.YoutubeLinkNotValid]);
            });
        }
    }
}

﻿using FluentValidation;
using IGenericControlPanel.Core.Dto.Brand;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class BrandFormDtoValidator : AbstractValidator<BrandFormDto>
    {
        public BrandFormDtoValidator(IStringLocalizer<BrandResource> brandLocalizer)
        {
            RuleFor(b => b.Name).NotEmpty()
                .WithMessage(brandLocalizer[ErrorMessages.BrandNameRequired].Value);
        }
    }
}

﻿using FluentValidation;
using IGenericControlPanel.Core.Dto.Allegry;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class AllergyDtoValidator : AbstractValidator<AllergyFormDto>
    {
        public AllergyDtoValidator(IStringLocalizer<AllergyResource> allergyLocalizer)
        {
            RuleFor(b => b.Name).NotEmpty()
                .WithMessage(allergyLocalizer[ErrorMessages.AllergyNameRequired].Value);
        }
    }
}
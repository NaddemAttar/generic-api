﻿using FluentValidation;
using IGenericControlPanel.Consultation.Dto.ConsultationDto;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.FluentValidation.CustomValidators;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation;
public class ConsultationChoicesDtoValidator : AbstractValidator<ConsultationChoicesDto>
{
    public ConsultationChoicesDtoValidator(IStringLocalizer<ConsultationResource> consultationLocalizer)
    {
        RuleFor(b => b.Name)
            .NotEmptyCustom(consultationLocalizer[ErrorMessages.ConsultationChoiceNameRequired]);
    }
}
﻿using FluentValidation;

using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Controllers;
using IGenericControlPanel.WebApplication.ViewModels.Group;

using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class GroupValidator : AbstractValidator<GroupViewModel>
    {
        public GroupValidator(IStringLocalizer<GroupController> FromResx)
        {
            RuleFor(b => b.Name).NotEmpty()
                .WithMessage(FromResx[ErrorMessages.GroubNameRequired]);
            RuleFor(b => b.MultiLevelCategoryId).NotEmpty()
                .WithMessage(FromResx[ErrorMessages.GroubNameRequired]);
            RuleFor(b => b.Description).NotEmpty()
                .WithMessage(FromResx[ErrorMessages.GroubNameRequired]);

        }
    }
}

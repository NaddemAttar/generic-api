﻿using FluentValidation;
using IGenericControlPanel.GamificationSystem.Dto.Gamification;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Controllers;
using IGenericControlPanel.WebApplication.FluentValidation.CustomValidators;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation;

public class GamificationDtoValidator : AbstractValidator<GamificationDto>
{
    public GamificationDtoValidator(IStringLocalizer<GamificationManagmentsController> FromResx)
    {
        RuleFor(gm => gm.Key).NotEmptyCustom(FromResx[ErrorMessages.KeyIsRequired]);
        RuleFor(gm => gm.Enable).Equal(toCompare: true).When(x => x.Value != 0)
            .WithMessage(ErrorMessages.ValueCanNotBeZero);
    }
}

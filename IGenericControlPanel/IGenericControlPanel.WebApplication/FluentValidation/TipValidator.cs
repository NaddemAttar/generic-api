﻿using FluentValidation;
using IGenericControlPanel.Content.Dto.Tips;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class TipValidator : AbstractValidator<TipsFormDto>
    {
        public TipValidator(IStringLocalizer<ContentResource> contentLocalizer)
        {
            RuleFor(b => b.Title).NotEmpty()
                .WithMessage(contentLocalizer[ErrorMessages.TipTitleRequired].Value);
            RuleFor(b => b.ServiceId).NotEmpty()
                .WithMessage(contentLocalizer[ErrorMessages.TipServiceIdRequired].Value);
            RuleFor(b => b.Description).NotEmpty()
                .WithMessage(contentLocalizer[ErrorMessages.TipDescriptionRequired].Value);
        }
    }
}
﻿using FluentValidation;
using IGenericControlPanel.Core.Dto.Offer;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class OfferValidatorB : AbstractValidator<OfferDto>
    {
        public OfferValidatorB(IStringLocalizer<OfferResource> offerLocalizer)
        {
            RuleFor(o => o.Title).NotEmpty()
                .WithMessage(offerLocalizer[ErrorMessages.OfferTitleRequired]);

            RuleFor(o => o.Description).NotEmpty()
               .WithMessage(offerLocalizer[ErrorMessages.OfferDescriptionRequired]);

            //RuleFor(o => o.OfferFor != OfferFor.Products)
            //    .Must((dto, o) => dto.DetailsIds.Count() == 0);

            //RuleFor(o => o.Percentage.Value != 0.0)
            //    .Must((dto, o) => dto.Percentage <= 100 && dto.Percentage >= 1)
            //    .WithMessage(FromResx[ErrorMessages.OfferPercentageError]);
        }
    }
}

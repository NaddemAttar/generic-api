﻿using FluentValidation;
using IGenericControlPanel.Core.Dto.Product;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class ProductSizeColorValidator : AbstractValidator<ProductSizeColorDto>
    {
        public ProductSizeColorValidator(IStringLocalizer<ProductResource> productLocalizer)
        {
            RuleFor(b => b.ProductId).NotEmpty()
                .WithMessage(productLocalizer[ErrorMessages.ProductIdRequired]);

            RuleFor(b => b.Price).NotNull()
               .WithMessage(productLocalizer[ErrorMessages.ProductPriceRequired]);

            RuleFor(b => b.Quantity).NotNull()
               .WithMessage(productLocalizer[ErrorMessages.ProductQuantityRequired]);
        }
    }
}
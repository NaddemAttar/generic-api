﻿using FluentValidation;

using IGenericControlPanel.Security.Dto.User;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Controllers;
using IGenericControlPanel.WebApplication.FluentValidation.CustomValidators;

using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class AccountSignUpCPUserValidator : AbstractValidator<CPUserRegistrationDto>
    {
        public string PhoneNumberMatcher = "\\(?\\d{3}\\)?[-\\.]? *\\d{3}[-\\.]? *[-\\.]?\\d{4}";
        public AccountSignUpCPUserValidator(IStringLocalizer<AccountController> FromResx)
        {
            RuleFor(b => b.UserName).NotEmpty()
                .WithMessage(FromResx[ErrorMessages.UsernameCanNotBeNull]);
            RuleFor(b => b.UserName).MinimumLength(3)
                .WithMessage(FromResx[ErrorMessages.UsernameMustNotBeLessThan3Characters]);
            RuleFor(b => b.Password).Must((dto, password) => (dto.Id != 0 || password.Length > 6))
                .WithMessage(FromResx[ErrorMessages.PasswordMustNotBeLessThan6Characters]);
            RuleFor(b => b.Password).Must((dto, password) => (dto.Id != 0 || password!=null))
                .WithMessage(FromResx[ErrorMessages.PasswordCanNotBeNull]);
            RuleFor(tm => tm.Email).NotEmptyCustom(FromResx[ErrorMessages.EmailIsRequired])
               .EmailAddress().WithMessage(FromResx[ErrorMessages.EmailNotValid]);
            RuleFor(tm => tm.PhoneNumber).NotEmptyCustom(FromResx[ErrorMessages.PhoneNumberRequired])
                .Matches(PhoneNumberMatcher).WithMessage(FromResx[ErrorMessages.PhoneNumberNotValid]);
            RuleFor(tm => tm.FirstName).NotEmptyCustom(FromResx[ErrorMessages.FirstNameIsRequired]);
            RuleFor(tm => tm.LastName).NotEmptyCustom(FromResx[ErrorMessages.LastNameIsRequired]);

        }
    }
}

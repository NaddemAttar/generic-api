﻿using FluentValidation;
using IGenericControlPanel.GamificationSystem.Dto.Coupon;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Controllers;
using IGenericControlPanel.WebApplication.FluentValidation.CustomValidators;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation;

public class CouponManagementValidator : AbstractValidator<CouponDto>
{
    public CouponManagementValidator(IStringLocalizer<CouponManagementController> FromResx)
    {
        RuleFor(cp => cp.Points)
            .NotEmptyCustom(FromResx[ErrorMessages.ConsultationChoiceNameRequired]);

        RuleFor(cp => cp.Percentage)
            .NotEmptyCustom(FromResx[ErrorMessages.PercentageIsRequired]);

        RuleFor(cp => cp.CouponPeriod)
            .NotEmptyCustom(FromResx[ErrorMessages.CouponPeriodIsRequired]);

        RuleFor(cp => cp.Percentage)
            .Must(percentage => percentage > 0 && percentage <= 100)
            .WithMessage(ErrorMessages.PercentageMustBeBetweenZer0And100);
    }
}

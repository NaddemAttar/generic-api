﻿using FluentValidation;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.ViewModels.Blog;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class BlogDtoValidator : AbstractValidator<BlogFormViewModel>
    {
        public BlogDtoValidator(IStringLocalizer<BlogResource> bolgLocalizer)
        {
            RuleFor(b => b.Content).NotEmpty()
                .WithMessage(bolgLocalizer[ErrorMessages.BlogContentRequired]);
            RuleFor(b => b.Title).NotEmpty()
                .WithMessage(bolgLocalizer[ErrorMessages.BlogTitleRequired]);
            RuleFor(b => b.MultiLevelCategoryIds).NotEmpty()
                .WithMessage(bolgLocalizer[ErrorMessages.BlogCategoryRequired]);
        }
    }
}
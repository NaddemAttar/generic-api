﻿using FluentValidation;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Resources.Controllers;
using IGenericControlPanel.WebApplication.ViewModels.Question;
using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class QuestionValidator : AbstractValidator<QuestionFormViewModel>
    {
        public QuestionValidator(IStringLocalizer<QuestionResource> questionLocalizer)
        {
            RuleFor(b => b.Content).NotEmpty()
                .WithMessage(questionLocalizer[ErrorMessages.QuestionContentRequired]);
            
            RuleFor(b => b.MultiLevelCategoryIds).NotEmpty()
                .WithMessage(questionLocalizer[ErrorMessages.QuestionCategoryRequired]);
        }
    }
}
﻿using FluentValidation;

namespace IGenericControlPanel.WebApplication.FluentValidation.CustomValidators
{
    public static class NotEmptyCustomValidator
    {
        public static IRuleBuilderOptions<T, TElement> NotEmptyCustom<T, TElement>(
            this IRuleBuilder<T, TElement> ruleBuilder, string errorMessage)
        {
            return ruleBuilder.NotEmpty().WithMessage(errorMessage);
        }
    }
}

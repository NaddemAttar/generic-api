﻿using FluentValidation;

using IGenericControlPanel.Core.Dto.Location.LocationSchedule;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Controllers;

using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.WebApplication.FluentValidation
{
    public class LocationScheduleFormDtoValidator : AbstractValidator<LocationScheduleFormDto>
    {
        public LocationScheduleFormDtoValidator(IStringLocalizer<LocationController> FromResx)
        {
            RuleFor(ls => ls.CityId).NotEqual(0).WithMessage(FromResx[ErrorMessages.CityIdMustNotBeZero]);
            RuleFor(ls => ls.FromDate).NotEmpty().WithMessage(FromResx[ErrorMessages.FromDateRequired]);
            RuleFor(ls => ls.ToDate).NotEmpty().WithMessage(FromResx[ErrorMessages.ToDateRequired])
                .GreaterThanOrEqualTo(ls => ls.FromDate).WithMessage(FromResx[ErrorMessages.ToDateMustAfterFromDate]);
        }
    }
}

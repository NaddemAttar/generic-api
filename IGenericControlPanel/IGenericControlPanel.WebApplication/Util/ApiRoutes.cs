﻿namespace IGenericControlPanel.WebApplication.Util;

public static class ApiRoutes
{
    private static string _baseUrl => "https://localhost:44318/api/";
    private static int pageSize => 8;
    private static int pageNumber => 0;
    private static bool enablePagination => true;
    public static class Allergy
    {
        private static string _allergyController => string.Concat(_baseUrl,
            ControllersActions.AllergyController.Allergy);
        public static string GetAllAllergies => string.Concat(_allergyController,
            $"/{ControllersActions.AllergyController.GetAllAllergies}?enablePagination={enablePagination}" +
            $"&pageSize={pageSize}&pageNumber={pageNumber}");
        public static string GetAllergiesAutoComplete => string.Concat(_allergyController,
            $"/{ControllersActions.AllergyController.GetAllergiesAutoComplete}");
        public static string GetPatientAllergies => string.Concat(_allergyController,
            $"/{ControllersActions.AllergyController.GetPatientAllergies}");
        public static string GetPersonAllergyDetails => string.Concat(_allergyController,
            $"/{ControllersActions.AllergyController.GetPersonAllergyDetails}");
        public static string GetAllergy => string.Concat(_allergyController,
            $"/{ControllersActions.AllergyController.GetAllergy}");
        public static string ActionAllergies => string.Concat(_allergyController,
            $"/{ControllersActions.AllergyController.Action}");
        public static string ActionPatientAllegries => string.Concat(_allergyController,
            $"/{ControllersActions.AllergyController.ActionPatientAllegries}");
        public static string RemoveAllegry => string.Concat(_allergyController,
            $"/{ControllersActions.AllergyController.RemoveAllegry}");
        public static string RemovePatientAllegry => string.Concat(_allergyController,
            $"/{ControllersActions.AllergyController.RemovePatientAllegry}");
    }
    public static class Offer
    {
        private static string _offerController => string.Concat(_baseUrl,
            ControllersActions.OfferController.Offer);
        public static string GetAllOffers => string.Concat(_offerController,
            $"/{ControllersActions.OfferController.GetAllOffers}?" +
            $"pageNumber={pageNumber}&pageSize={pageSize}");
        public static string GetOffer => string.Concat(_offerController,
            $"/{ControllersActions.OfferController.GetOffer}", "?offerId={offerId}");
        public static string Remove => string.Concat(_offerController,
            $"/{ControllersActions.OfferController.Remove}", "?offerId={offerId}");
        public static string GetMinMaxPrice => string.Concat(_offerController,
            $"/{ControllersActions.OfferController.GetMinMaxPrice}", "?BrandsIds={BrandId}&CategoriesIds={CategoriesId}");
        public static string Action => string.Concat(_offerController,
            $"/{ControllersActions.OfferController.Action}");
    }
    public static class Blog
    {
        private static string _blogController => string.Concat(_baseUrl,
            ControllersActions.BlogController.Blog);
        public static string GetBlogs => string.Concat(_blogController,
            $"/{ControllersActions.BlogController.GetBlogs}");
        public static string GetBlogDetails => string.Concat(_blogController,
            $"/{ControllersActions.BlogController.GetBlogDetails}", "?id={id}");
        public static string Action => string.Concat(_blogController,
            $"/{ControllersActions.BlogController.Action}");
        public static string RemoveBlog => string.Concat(_blogController,
            $"/{ControllersActions.BlogController.RemoveBlog}", "?id={id}");
    }
    public static class Product
    {
        private static string _productController => string.Concat(_baseUrl,
            ControllersActions.ProductController.Product);
        public static string AddProduct => string.Concat(_productController,
            $"/{ControllersActions.ProductController.AddProduct}");
        public static string RemoveProducts => string.Concat(_productController,
            $"/{ControllersActions.ProductController.RemoveProducts}", "?ids={ids}");
    }
    public static class TeamMember
    {
        private static string _teamMemberController => string.Concat(_baseUrl,
            ControllersActions.TeamMemberController.TeamMember);
        public static string ActionTeamMember => string.Concat(_teamMemberController,
            $"/{ControllersActions.TeamMemberController.ActionTeamMember}");
        public static string RemoveTeamMember => string.Concat(_teamMemberController,
            $"/{ControllersActions.TeamMemberController.RemoveTeamMember}");
    }
    public static class Course
    {
        private static string _courseController => string.Concat(_baseUrl,
            ControllersActions.CourseController.Course);
        public static string ActionCourse => string.Concat(_courseController,
            $"/{ControllersActions.CourseController.ActionCourse}");
        public static string RemoveAction => string.Concat(_courseController,
            $"/{ControllersActions.CourseController.RemoveCourses}");
    }
    public static class Question
    {
        private static string _questionController => string.Concat(_baseUrl,
           ControllersActions.QuestionController.Question);
        public static string GetQuestions => string.Concat(_questionController,
            $"/{ControllersActions.QuestionController.GetQuestions}");
        public static string GetQuestion => string.Concat(_questionController,
            $"/{ControllersActions.QuestionController.GetQuestionDetails}", "?id={id}");
        public static string ActionQuestion => string.Concat(_questionController,
            $"/{ControllersActions.QuestionController.ActionQuestion}");
        public static string ActionAnswer => string.Concat(_questionController,
            $"/{ControllersActions.QuestionController.ActionAnswer}");
        public static string RemoveQuestion => string.Concat(_questionController,
            $"/{ControllersActions.QuestionController.RemoveQuestion}", "?id={id}");
        public static string RemoveAnswer => string.Concat(_questionController,
            $"/{ControllersActions.QuestionController.RemoveAnswer}", "?AnswerId={id}");
    }
    public static class Disease
    {
        private static string _DiseasesController => string.Concat(_baseUrl,
            ControllersActions.DiseasesController.Diseases);
        public static string GetAllDiseases => string.Concat(_DiseasesController
            , $"/{ControllersActions.DiseasesController.GetAllDiseases}");
        public static string GetDisease => string.Concat(_DiseasesController
            , $"/{ControllersActions.DiseasesController.GetDisease}");
        public static string Action => string.Concat(_DiseasesController,
            $"/{ControllersActions.DiseasesController.Action}");
        public static string ActionPatientDiseases => string.Concat(_DiseasesController,
            $"/{ControllersActions.DiseasesController.ActionPatientDiseases}");
        public static string GetDiseasesAutoComplete => string.Concat(_DiseasesController
            , $"/{ControllersActions.DiseasesController.GetDiseasesAutoComplete}");
        public static string RemoveDisease => string.Concat(_DiseasesController,
            $"/{ControllersActions.DiseasesController.RemoveDisease}");
    }
    public static class Midicine
    {
        private static string _MidicineController => string.Concat(_baseUrl,
            ControllersActions.MedicineController.Medicine);
        public static string GetAllMidicines => string.Concat(_MidicineController
            , $"/{ControllersActions.MedicineController.GetAllMedicines}");
        public static string GetMidicine => string.Concat(_MidicineController
            , $"/{ControllersActions.MedicineController.GetMedicine}");
        public static string Action => string.Concat(_MidicineController,
            $"/{ControllersActions.MedicineController.Action}");
        public static string ActionPatientMedicine => string.Concat(_MidicineController,
            $"/{ControllersActions.MedicineController.ActionPatientMedicine}");
        public static string GetMedicineAutoComplete => string.Concat(_MidicineController
            , $"/{ControllersActions.MedicineController.GetMedicineAutoComplete}");
        public static string RemoveMedicine => string.Concat(_MidicineController,
            $"/{ControllersActions.MedicineController.RemoveMedicine}");
    }
    public static class Surgery
    {
        private static string _SurgeryController => string.Concat(_baseUrl,
            ControllersActions.SurgeryController.Surgery);
        public static string GetAllSurgeries => string.Concat(_SurgeryController
            , $"/{ControllersActions.SurgeryController.GetAllSurgeries}");
        public static string GetSurgery => string.Concat(_SurgeryController
            , $"/{ControllersActions.SurgeryController.GetSurgery}");
        public static string Action => string.Concat(_SurgeryController,
            $"/{ControllersActions.SurgeryController.Action}");
        public static string ActionPatientSurgery => string.Concat(_SurgeryController,
            $"/{ControllersActions.SurgeryController.ActionPatientSurgery}");
        public static string GetSurgeryAutoComplete => string.Concat(_SurgeryController
            , $"/{ControllersActions.SurgeryController.GetSurgeriesAutoComplete}");
        public static string RemoveSurgery => string.Concat(_SurgeryController,
            $"/{ControllersActions.SurgeryController.RemoveSurgery}");
    }
    public static class Note
    {
        private static string _NoteController => string.Concat(_baseUrl,
            ControllersActions.NoteController.Note);
        public static string GetAllNotes => string.Concat(_NoteController
            , $"/{ControllersActions.NoteController.GetAllNotes}");
        public static string GetNote => string.Concat(_NoteController
            , $"/{ControllersActions.NoteController.GetNote}");
        public static string Action => string.Concat(_NoteController,
            $"/{ControllersActions.NoteController.Action}");
        public static string RemoveNote => string.Concat(_NoteController,
            $"/{ControllersActions.NoteController.RemoveNote}");
    }
    public static class Brand
    {
        private static string _brandController => string.Concat(_baseUrl,
            $"{ControllersActions.BrandController.Brand}");
        public static string GetAllBrands => string.Concat(_brandController,
            $"/{ControllersActions.BrandController.GetAllBrands}");
        public static string ActionBrand => string.Concat(_brandController,
            $"/{ControllersActions.BrandController.ActionBrand}");
        public static string RemoveBrand => string.Concat(_brandController,
            $"/{ControllersActions.BrandController.RemoveBrand}");
    }
    public static class Category
    {
        private static string _categoryController => string.Concat(_baseUrl,
            $"{ControllersActions.MultiLevelCategoryController.MultiLevelCategory}");
        public static string GetAll => string.Concat(_categoryController,
            $"/{ControllersActions.MultiLevelCategoryController.GetMultiLevelCategories}");
        public static string Action => string.Concat(_categoryController,
            $"/{ControllersActions.MultiLevelCategoryController.ActionMultiLevelCategory}");
        public static string Remove => string.Concat(_categoryController,
            $"/{ControllersActions.MultiLevelCategoryController.RemoveMultiLevelCategory}");
        public static string Get => string.Concat(_categoryController,
            $"/{ControllersActions.MultiLevelCategoryController.GetMultiLevelCategories}");
    }
    public static class Account
    {
        private static string _AccountController => string.Concat(_baseUrl,
            $"{ControllersActions.AuthController.Account}");
        public static string CPUserLogin => string.Concat(_AccountController,
            $"/{ControllersActions.AuthController.CPUserLogin}");
        public static string NormalUserLogin => string.Concat(_AccountController,
            $"/{ControllersActions.AuthController.NormalUserLogin}");
    }
    public static class Partners
    {
        private static string _partnerController => string.Concat(_baseUrl,
            $"{ControllersActions.PartnersController.Partners}");
        public static string GetPartners => string.Concat(_partnerController,
            $"/{ControllersActions.PartnersController.GetPartners}?pageSize={pageSize}&pageNumber={pageNumber}");
        public static string GetPartner => string.Concat(_partnerController,
            $"/{ControllersActions.PartnersController.GetPartner}");
        public static string ActionPartner => string.Concat(_partnerController,
            $"/{ControllersActions.PartnersController.ActionPartner}");
        public static string RemovePartner => string.Concat(_partnerController,
            $"/{ControllersActions.PartnersController.RemovePartner}", "?PartnerId={partnerId}");
    }
    public static class Service
    {
        private static string _serviceController => string.Concat(_baseUrl,
            ControllersActions.ServiceController.Service);

        public static string GetServices => string.Concat(_serviceController,
            $"/{ControllersActions.ServiceController.GetServices}", "?query={query}&&WithData={with_data}");

        public static string RemoveServiceById => string.Concat(_serviceController,
            $"/{ControllersActions.ServiceController.RemoveServiceById}", "?serviceId={serviceId}");

        public static string GetServiceDetails => string.Concat(_serviceController,
            $"/{ControllersActions.ServiceController.GetServiceDetails}", "?serviceId={serviceId}");

        public static string ServiceAction => string.Concat(_serviceController,
            $"/{ControllersActions.ServiceController.ServiceAction}");
    }
    public static class Project
    {
        private static string _projectController => string.Concat(_baseUrl,
            ControllersActions.ProjectController.Project);

        public static string GetProjects => string.Concat(_projectController,
            $"/{ControllersActions.ProjectController.GetProjects}",
            "?pageNumber={pageNumber}&&pageSize={pageSize}");

        public static string GetProjectDetails => string.Concat(_projectController,
            $"/{ControllersActions.ProjectController.GetProjectDetails}", "?id={id}");

        public static string RemoveProject => string.Concat(_projectController,
            $"/{ControllersActions.ProjectController.RemoveProject}", "?id={id}");

        public static string AddProject => string.Concat(_projectController,
            $"/{ControllersActions.ProjectController.AddProject}");

        public static string UpdateProject => string.Concat(_projectController,
            $"/{ControllersActions.ProjectController.UpdateProject}");
    }
    public static class GamificationManagments
    {
        private static string _gamificationController => string.Concat(_baseUrl,
            ControllersActions.GamificationManagmentsController.GamificationManagments);

        public static string GetAllGamificationSections => string.Concat(_gamificationController,
            $"/{ControllersActions.GamificationManagmentsController.GetAllGamificationSections}");

        public static string UpdatePointsForGamificactionSections => string.Concat(_gamificationController,
            $"/{ControllersActions.GamificationManagmentsController.UpdatePointsForGamificactionSections}");

        public static string AddNewGamificationSection => string.Concat(_gamificationController,
            $"/{ControllersActions.GamificationManagmentsController.AddNewGamificationSection}");
        //public static string GetUsersWithTheirPoints => string.Concat(_gamificationController,
        //    $"/{ControllersActions.GamificationManagmentsController.GetUsersWithTheirPoints}",
        //    "?enablePagination={enablePagination}&usersHavePointsOnly={usersHavePointsOnly}" +
        //    "&isAscendingOrder={isAscendingOrder}&query={query}&pageSize={pageSize}&pageNumber={pageNumber}");
        public static string GetUsersWithTheirPoints => string.Concat(_gamificationController,
           $"/{ControllersActions.GamificationManagmentsController.GetUsersWithTheirPoints}",
           "?enablePagination={enablePagination}&usersHavePointsOnly={usersHavePointsOnly}" +
            "&isAscendingOrder={isAscendingOrder}&query={query}&pageSize={pageSize}&pageNumber={pageNumber}");
    }
    public static class GamificationMovements
    {
        private static string _gamificationMovementsController => string.Concat(_baseUrl,
            ControllersActions.GamificationMovementsController.GamificationMovements);

        public static string SaveSocialMediaPoints => string.Concat(_gamificationMovementsController,
            $"/{ControllersActions.GamificationMovementsController.SaveSocialMediaPoints}",
            "?socialMediaApps={socialMediaApps}&sectionsForPublishOnSocialMedia={sectionsForPublishOnSocialMedia}");
    }
    public static class CouponManagement
    {
        private static string _couponController => string.Concat(_baseUrl,
            ControllersActions.CouponManagementController.CouponManagement);

        public static string GetCoupons => string.Concat(_couponController,
            $"/{ControllersActions.CouponManagementController.GetCoupons}",
            "?enablePagination={enablePagination}&" +
            "pageNumber={pageNumber}&pageSize={pageSize}&query={query}");

        public static string GetCouponDetails => string.Concat(_couponController,
            $"/{ControllersActions.CouponManagementController.GetCouponDetails}",
            "?id={id}");
        public static string RemoveCoupon => string.Concat(_couponController,
             $"/{ControllersActions.CouponManagementController.RemoveCoupon}",
             "?id={id}");
        public static string AddCoupon => string.Concat(_couponController,
            $"/{ControllersActions.CouponManagementController.AddCoupon}");
        public static string UpdateCoupon => string.Concat(_couponController,
           $"/{ControllersActions.CouponManagementController.UpdateCoupon}");
    }
    public static class ApplyingCouponsSystem
    {
        private static string _applyingCouponsSystemController => string.Concat(_baseUrl,
           ControllersActions.ApplyingCouponsSystemController.ApplyingCouponsSystem);
        public static string GetAvailableCoupons => string.Concat(_applyingCouponsSystemController,
            $"/{ControllersActions.ApplyingCouponsSystemController.GetAvailableCoupons}");
        public static string CouponBooking => string.Concat(_applyingCouponsSystemController,
           $"/{ControllersActions.ApplyingCouponsSystemController.CouponBooking}", "?couponId={couponId}");
        public static string ActivateCouponBooking => string.Concat(_applyingCouponsSystemController,
           $"/{ControllersActions.ApplyingCouponsSystemController.ActivateCouponBooking}", "?code={code}");
        public static string DeActivateCouponBooking => string.Concat(_applyingCouponsSystemController,
          $"/{ControllersActions.ApplyingCouponsSystemController.DeActivateCouponBooking}");
        public static string GetUserCoupons => string.Concat(_applyingCouponsSystemController,
            $"/{ControllersActions.ApplyingCouponsSystemController.GetUserCoupons}",
            "?isBeneficiary={isBeneficiary}&isActivate={isActivate}");
        public static string PointsDeductionFromUser => string.Concat(_applyingCouponsSystemController,
            $"/{ControllersActions.ApplyingCouponsSystemController.PointsDeductionFromUser}",
            "?userCouponId={userCouponId}");
    }
    public static class ExperienceManagement
    {
        private static string _experienceManagementController => string.Concat(_baseUrl,
            ControllersActions.ExperienceManagementController.ExperienceManagement);
        public static string GetExperiencesForUser => string.Concat(_experienceManagementController,
            $"/{ControllersActions.ExperienceManagementController.GetExperiencesForUser}");
        public static string GetOneExperience => string.Concat(_experienceManagementController,
           $"/{ControllersActions.ExperienceManagementController.GetOneExperience}", "?id={id}");
        public static string RemoveExperience => string.Concat(_experienceManagementController,
           $"/{ControllersActions.ExperienceManagementController.RemoveExperience}", "?id={id}");
        public static string AddExperience => string.Concat(_experienceManagementController,
           $"/{ControllersActions.ExperienceManagementController.AddExperience}");
        public static string UpdateExperience => string.Concat(_experienceManagementController,
           $"/{ControllersActions.ExperienceManagementController.UpdateExperience}");
    }
    public static class EducationManagement
    {
        private static string _educationManagementController => string.Concat(_baseUrl,
            ControllersActions.EducationManagementController.EducationManagement);
        public static string GetEducationsForUser => string.Concat(_educationManagementController,
            $"/{ControllersActions.EducationManagementController.GetEducationsForUser}");
        public static string GetOneEducation => string.Concat(_educationManagementController,
            $"/{ControllersActions.EducationManagementController.GetOneEducation}", "?id={id}");
        public static string RemoveEducation => string.Concat(_educationManagementController,
            $"/{ControllersActions.EducationManagementController.RemoveEducation}", "?id={id}");
        public static string AddEducation => string.Concat(_educationManagementController,
            $"/{ControllersActions.EducationManagementController.AddEducation}");
        public static string UpdateEducation => string.Concat(_educationManagementController,
            $"/{ControllersActions.EducationManagementController.UpdateEducation}");
    }
    public static class LicenseManagement
    {
        private static string _licenseManagementController => string.Concat(_baseUrl,
            ControllersActions.LicenseManagementController.LicenseManagement);
        public static string GetLicensesForUser => string.Concat(_licenseManagementController,
            $"/{ControllersActions.LicenseManagementController.GetLicensesForUser}");
        public static string GetOneLicense => string.Concat(_licenseManagementController,
            $"/{ControllersActions.LicenseManagementController.GetOneLicense}", "?id={id}");
        public static string RemoveLicense => string.Concat(_licenseManagementController,
            $"/{ControllersActions.LicenseManagementController.RemoveLicense}", "?id={id}");
        public static string AddLicense => string.Concat(_licenseManagementController,
            $"/{ControllersActions.LicenseManagementController.AddLicense}");
        public static string UpdateLicense => string.Concat(_licenseManagementController,
            $"/{ControllersActions.LicenseManagementController.UpdateLicense}");
    }
    public static class CertificateManagement
    {
        private static string _certificateManagementController => string.Concat(_baseUrl,
            ControllersActions.CertificateManagementController.CertificateManagement);
        public static string GetCertificateForUser => string.Concat(_certificateManagementController,
            $"/{ControllersActions.CertificateManagementController.GetCertificatesForUser}");
        public static string GetOneLCertificate => string.Concat(_certificateManagementController,
            $"/{ControllersActions.CertificateManagementController.GetCertificate}", "?id={id}");
        public static string RemoveCertificate => string.Concat(_certificateManagementController,
            $"/{ControllersActions.CertificateManagementController.RemoveCertificate}", "?id={id}");
        public static string AddCertificate => string.Concat(_certificateManagementController,
            $"/{ControllersActions.CertificateManagementController.AddCertificate}");
        public static string UpdateCertificate => string.Concat(_certificateManagementController,
            $"/{ControllersActions.CertificateManagementController.UpdateCertificate}");
    }

    public static class LectureManagement
    {
        private static string _lectureManagementController => string.Concat(_baseUrl,
            ControllersActions.LectureManagementController.LectureManagement);
        public static string GetLecturesForUser => string.Concat(_lectureManagementController,
            $"/{ControllersActions.LectureManagementController.GetLecturesForUser}");
        public static string GetOneLecture => string.Concat(_lectureManagementController,
            $"/{ControllersActions.LectureManagementController.GetOneLecture}", "?id={id}");
        public static string RemoveLecture => string.Concat(_lectureManagementController,
            $"/{ControllersActions.LectureManagementController.RemoveLecture}", "?id={id}");
        public static string AddLecture => string.Concat(_lectureManagementController,
            $"/{ControllersActions.LectureManagementController.AddLecture}");
        public static string UpdateLecture => string.Concat(_lectureManagementController,
            $"/{ControllersActions.LectureManagementController.UpdateLecture}");
    }
    public static class BuildResume
    {
        private static string _buildResumeController => string.Concat(_baseUrl,
            ControllersActions.BuildResumeController.BuildResume);
        public static string GetResumeForHcpUser => string.Concat(_buildResumeController,
            $"/{ControllersActions.BuildResumeController.GetResumeForHcpUser}", "?hcpId={hcpId}");
    }
}

﻿using System.Text.Json;

using Microsoft.Extensions.Caching.Distributed;
using Microsoft.AspNetCore.Builder;

namespace IGenericControlPanel.WebApplication.Util
{
    public static class DistributedCacheExtensions
    {
        public static bool CachingIsAllowed { get; set; }
        public static async Task SetRecordAsync<T>(this IDistributedCache cache,
            string recordId,
            T data,
            bool dataIsValid,
            TimeSpan? absoluteExpireTime = null,
            TimeSpan? unusedExpireTime = null)
        {
            if (dataIsValid && CachingIsAllowed)
            {
                var options = new DistributedCacheEntryOptions();

                options.AbsoluteExpirationRelativeToNow = absoluteExpireTime ?? TimeSpan.FromSeconds(60);
                options.SlidingExpiration = unusedExpireTime;

                var jsonData = JsonSerializer.Serialize(data);
                await cache.SetStringAsync(recordId, jsonData, options);
            }
        }

        public static async Task<T> GetRecordAsync<T>(this IDistributedCache cache,
            string recordId)
        {
            if (!CachingIsAllowed) return default(T);

            try
            {
                var jsonData = await cache.GetStringAsync(recordId);
                if (jsonData is null)
                {
                    return default(T);
                }

                return JsonSerializer.Deserialize<T>(jsonData);
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        public async static Task RemoveRecordAsync(this IDistributedCache cache,
            string recordId)
        {
            if (CachingIsAllowed)
            {
                var jsonData = await cache.GetStringAsync(recordId);
                if (jsonData is not null)
                {
                    await cache.RemoveAsync(recordId);
                }
            }
        }
    }
}

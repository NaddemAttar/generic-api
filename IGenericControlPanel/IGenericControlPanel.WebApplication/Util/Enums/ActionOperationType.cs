﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.WebApplication.Util.Enums
{
    public enum ActionOperationType
    {
        Create = 0,
        Update,
        Delete
    }
}

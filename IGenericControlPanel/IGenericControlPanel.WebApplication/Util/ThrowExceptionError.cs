﻿using CraftLab.Core.OperationResults;

using IGenericControlPanel.WebApplication.Middleware;

namespace IGenericControlPanel.WebApplication.Util
{
    public static class ThrowExceptionError
    {
        public static void GenericThrowExceptionError(
           this GenericOperationResult enumResult, string errorMessage)
        {
            switch (enumResult)
            {
                case GenericOperationResult.Failed:
                    throw new InternalServerException(errorMessage);
                case GenericOperationResult.NotFound:
                    throw new NotFoundException(errorMessage);
                case GenericOperationResult.ValidationError:
                    throw new BadRequestException(errorMessage);
                default:
                    throw new InternalServerException(errorMessage);
            }
        }
    }
}

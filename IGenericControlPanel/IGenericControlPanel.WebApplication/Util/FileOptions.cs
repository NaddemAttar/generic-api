﻿using System.Collections.Generic;

using AutoMapper;

using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.SharedKernal.Enums.File;

using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.WebApplication.Util
{
    public class FileOptions
    {
        public FileOptions()
        {
            Files = new List<IFormFile>();
        }
        public IList<IFormFile> Files { get; set; }
        public IFormFile File { get; set; }
        public string FilePath { get; set; }
        public IHttpFileService HttpFileService { get; set; }
        public IMapper Mapper { get; set; }
        public FileContentType FileContentType { get; set; }
        public FileSourceType FileSourceType { get; set; }
        public int SourceId { get; set; }
        public string ControllerName { get; set; }
    }
}

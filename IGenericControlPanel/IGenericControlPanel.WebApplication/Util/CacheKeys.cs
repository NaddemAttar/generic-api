﻿namespace IGenericControlPanel.WebApplication.Util;
public static class CacheKeys
{
    #region Brand Keys
    public static string GetOneBrand = nameof(GetOneBrand);
    public static string GetBrands = nameof(GetBrands);
    #endregion

    #region Blogs Keys
    public static string GetOneBlog = nameof(GetOneBlog);
    public static string GetBlogs = nameof(GetBlogs);

    #endregion

    #region Services Keys
    public static string GetServicesWithData = nameof(GetServicesWithData);
    public static string GetServicesWithoutData = nameof(GetServicesWithoutData);
    public static string GetOneService = nameof(GetOneService);

    #endregion

    #region Courses Keys
    public static string GetCourses = nameof(GetCourses);
    public static string GetOneCourse = nameof(GetOneCourse);
    #endregion

    #region Course Types Keys
    public static string GetCourseTypes = nameof(GetCourseTypes);
    public static string GetOneCourseType = nameof(GetOneCourseType);
    #endregion

    #region TeamMembers Keys
    public static string GetTeamMembers = nameof(GetTeamMembers);
    public static string GetOneTeamMember = nameof(GetOneTeamMember);
    #endregion

    #region TealHealth Keys
    public static string GetSessionTypes = nameof(GetSessionTypes);
    public static string GetBookingSettings = nameof(GetBookingSettings);
    public static string GetBookings = nameof(GetBookings);

    #endregion

    #region TimeManagments Keys
    public static string GetOpeningHours = nameof(GetOpeningHours);
    #endregion

    #region Groups Keys
    public static string GetGroups = nameof(GetGroups);
    public static string GetOneGroup = nameof(GetOneGroup);
    #endregion

    #region Products Keys
    public static string GetOneProduct = nameof(GetOneProduct);
    #endregion
}

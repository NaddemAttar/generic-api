﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.WebApplication.Util
{
    public static class PartialViewPaths
    {

        #region Shared

        #region Layouts

        public const string _Layout = "~/Views/Shared/Layouts/_Layout.cshtml";
        public const string _LoginLayout = "~/Views/Shared/Layouts/_LoginLayout.cshtml";

        public const string _SharedStyles = "~/Views/Shared/Layouts/_SharedStyles.cshtml";

        public const string _SharedScripts = "~/Views/Shared/Layouts/_SharedScripts.cshtml";

        public const string _LanguageSelection = "~/Views/Shared/Components/Header/_LanguageSelection.cshtml";

        public const string _Footer = "~/Views/Shared/_Footer.cshtml";

        public const string _Tabs = "~/Views/Shared/_Tabs.cshtml";

        #endregion

        #region Components

        public const string Sidebar = "~/Views/Shared/Components/Sidebar/Default.cshtml";

        public const string Header = "~/Views/Shared/Components/Header/Default.cshtml";

        public const string User = "~/Views/Shared/Components/User/Default.cshtml";
        #endregion

        #endregion
        #region Apartments
        public const string ApartmentsTableResults = "~/Views/Apartment/Partials/TableResults/_ApartmentTableResults.cshtml";
        public const string ApartmentsTableResultsHeader = "~/Views/Apartment/Partials/TableResults/_ApartmentTableResultsHeader.cshtml";
        public const string ApartmentsTableResultsBodyRow = "~/Views/Apartment/Partials/TableResults/_ApartmentTableResultsBodyRow.cshtml";
        public const string ApartmentModelForm = "~/Views/Construction/Modals/_ApartmentModelForm.cshtml";
        public const string FloorModelForm = "~/Views/Construction/Modals/_FloorModelForm.cshtml";


        #endregion
        #region Constructions

        public const string ConstructionsTableResults = "~/Views/Construction/Partials/_TableResults.cshtml";
        public const string ConstructionsTableResultsHeader = "~/Views/Construction/Partials/_TableResultsHeader.cshtml";
        public const string ConstructionsTableResultsBodyRow = "~/Views/Construction/Partials/_TableResultsBodyRow.cshtml"; 
        
        public const string ConstructionFloorResults = "~/Views/Construction/Partials/_ConstructionFloorResults.cshtml";
        public const string ConstructionFloorActionModal = "~/Views/Construction/Modals/_FloorAction.cshtml";
        public const string ConstructionApartmentActionModal = "~/Views/Construction/Modals/_ApartmentAction.cshtml";
        public const string FloorApartmentsCards = "~/Views/Construction/Partials/_FloorApartmentsCards.cshtml";


        #endregion
        #region Products

        public const string ProductsTableResultsViewModel = "~/Views/Product/Partials/TableResults/_ProductsTableResults.cshtml";
        public const string ProductsTableResultsHeaderViewModel = "~/Views/Product/Partials/TableResults/_ProductsTableResultsHeader.cshtml";
        public const string ProductsTableResultsBodyViewModel = "~/Views/Product/Partials/TableResults/_ProductsTableResultsBody.cshtml";

        public const string ProductsTableResultsBodyRow = "~/Views/Product/Partials/TableResults/_ProductsTableResultsBodyRow.cshtml";
        #endregion

        #region Categories

        public const string CategoriesTableResultsHeader = "~/Views/Category/Partials/TableResults/_CategoryTableResultsHeader.cshtml";
        public const string CategoriesTableResultsBody = "~/Views/Category/Partials/TableResults/_CategoryTableResultsBody.cshtml";
        public const string CategoryTableResultsBodyRow = "~/Views/Category/Partials/TableResults/_CategoryTableResultsBodyRow.cshtml";
        public const string CategoriesTableResults = "~/Views/Category/Partials/TableResults/_CategoriesTableResults.cshtml";
        public const string CategoryActionModal = "~/Views/Category/Partials/_ActionModal.cshtml";
        public const string CategoryActionModalBody = "~/Views/Category/Partials/_ActionModalBody.cshtml";


        #endregion


        #region Projects

        public const string ProjectsTableResultsHeader = "~/Views/Project/Partials/TableResults/_ProjectsTableResultsHeader.cshtml";
        public const string ProjectsTableResultsBody = "~/Views/Project/Partials/TableResults/_ProjectsTableResultsBody.cshtml";
        public const string ProjectsTableResultsBodyRow = "~/Views/Project/Partials/TableResults/_ProjectsTableResultsBodyRow.cshtml";
        public const string ProjectsTableResults = "~/Views/Project/Partials/TableResults/_ProjectsTableResults.cshtml";

        #endregion

        #region Information

        public const string InfoTableResultsHeader = "~/Views/Info/Partials/TableResults/_InfoTableResultsHeader.cshtml";
        public const string InfoTableResultsBody = "~/Views/Info/Partials/TableResults/_InfoTableResultsBody.cshtml";
        public const string InfoTableResultsBodyRow = "~/Views/Info/Partials/TableResults/_InfoTableResultsBodyRow.cshtml";
        public const string InfoTableResults = "~/Views/Info/Partials/TableResults/_InfoTableResults.cshtml";

        #endregion

        #region Blog
        public const string BlogTableResultsHeader = "~/Views/Blog/Partials/TableResults/_BlogTableResultsHeader.cshtml";
        public const string BlogTableResultsBody = "~/Views/Blog/Partials/TableResults/_BlogTableResultsBody.cshtml";
        public const string BlogTableResultsBodyRow = "~/Views/Blog/Partials/TableResults/_BlogTableResultsBodyRow.cshtml";
        public const string BlogTableResults = "~/Views/Blog/Partials/TableResults/_BlogTableResults.cshtml";

        #endregion

        #region Services

        public const string ServicesTableResultsHeader = "~/Views/Service/Partials/TableResults/_ServicesTableResultsHeader.cshtml";
        public const string ServicesTableResultsBody = "~/Views/Service/Partials/TableResults/_ServicesTableResultsBody.cshtml";
        public const string ServicesTableResultsBodyRow = "~/Views/Service/Partials/TableResults/_ServicesTableResultsBodyRow.cshtml";
        public const string ServicesTableResults = "~/Views/Service/Partials/TableResults/_ServicesTableResults.cshtml";

        #endregion

        #region TeamMember

        public const string TeamMemberTableResultsHeader = "~/Views/TeamMember/Partials/TableResults/_TeamMemberTableResultsHeader.cshtml";
        public const string TeamMemberTableResultsBody = "~/Views/TeamMember/Partials/TableResults/_TeamMemberTableResultsBody.cshtml";
        public const string TeamMemberTableResultsBodyRow = "~/Views/TeamMember/Partials/TableResults/_TeamMemberTableResultsBodyRow.cshtml";
        public const string TeamMemberTableResults = "~/Views/TeamMember/Partials/TableResults/_TeamMemberTableResults.cshtml";

        #endregion

        #region Client

        public const string ClientTableResultsHeader = "~/Views/Client/Partials/TableResults/_ClientTableResultsHeader.cshtml";
        public const string ClientTableResultsBody = "~/Views/Client/Partials/TableResults/_ClientTableResultsBody.cshtml";
        public const string ClientTableResultsBodyRow = "~/Views/Client/Partials/TableResults/_ClientTableResultsBodyRow.cshtml";
        public const string ClientTableResults = "~/Views/Client/Partials/TableResults/_ClientTableResults.cshtml";

        #endregion


        #region Files

        public const string ImageControl = "~/Views/Shared/Files/Images/_ImageControl.cshtml";
        public const string VideoControl = "~/Views/Shared/Files/Videos/_VideoControl.cshtml";

        public const string AddImageModal = "~/Views/Shared/Files/Images/_AddImageModal.cshtml";
        public const string AddVideoModal = "~/Views/Shared/Files/Videos/_AddVideoModal.cshtml";


        public const string EnableImagesAndVideos = "~/Views/Shared/Files/_EnableImagesAndVideos.cshtml";
        public const string EnableImagesOnly = "~/Views/Shared/Files/_EnableImagesOnly.cshtml";
        public const string EnableVideosOnly = "~/Views/Shared/Files/_EnableVideosOnly.cshtml";

        #endregion
    }

}

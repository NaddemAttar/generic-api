﻿namespace IGenericControlPanel.WebApplication.Util;

public static class ControllersActions
{
    public static class AllergyController
    {
        public static string Allergy => nameof(Allergy);
        public static string GetAllAllergies => nameof(GetAllAllergies);
        public static string GetAllergiesAutoComplete => nameof(GetAllergiesAutoComplete);
        public static string GetPatientAllergies => nameof(GetPatientAllergies);
        public static string GetPersonAllergyDetails => nameof(GetPersonAllergyDetails);
        public static string GetAllergy => nameof(GetAllergy);
        public static string Action => nameof(Action);
        public static string ActionPatientAllegries => nameof(ActionPatientAllegries);
        public static string RemoveAllegry => nameof(RemoveAllegry);
        public static string RemovePatientAllegry => nameof(RemovePatientAllegry);
    }
    public static class OfferController
    {
        public static string Offer => nameof(Offer);
        public static string Action => nameof(Action);
        public static string GetAllOffers => nameof(GetAllOffers);
        public static string GetOffer => nameof(GetOffer);
        public static string Remove => nameof(Remove);
        public static string GetMinMaxPrice => nameof(GetMinMaxPrice);
    }
    public static class TeamMemberController
    {
        public static string TeamMember => nameof(TeamMember);
        public static string ActionTeamMember => nameof(ActionTeamMember);                
        public static string RemoveTeamMember => nameof(RemoveTeamMember);        
    }
    public static class CourseController
    {
        public static string Course => nameof(Course);
        public static string ActionCourse => nameof(ActionCourse);
        public static string RemoveCourses => nameof(RemoveCourses);
    }
    public static class BlogController
    {
        public static string Blog => nameof(Blog);
        public static string GetBlogs => nameof(GetBlogs);
        public static string GetBlogDetails => nameof(GetBlogDetails);
        public static string Action => nameof(Action);
        public static string RemoveBlog => nameof(RemoveBlog);
    }
    public static class ProductController
    {
        public static string Product => nameof(Product);        
        public static string AddProduct => nameof(AddProduct);
        public static string RemoveProducts => nameof(RemoveProducts);

    }
    public static class QuestionController
    {
        public static string Question => nameof(Question);
        public static string GetQuestions => nameof(GetQuestions);
        public static string GetQuestionDetails => nameof(GetQuestionDetails);
        public static string ActionQuestion => nameof(ActionQuestion);
        public static string ActionAnswer => nameof(ActionAnswer);
        public static string RemoveAnswer => nameof(RemoveAnswer);
        public static string RemoveQuestion => nameof(RemoveQuestion);

    }
    public static class DiseasesController
    {
        public static string Diseases => nameof(Diseases);
        public static string GetAllDiseases => nameof(GetAllDiseases);
        public static string GetDisease => nameof(GetDisease);
        public static string GetDiseasesAutoComplete => nameof(GetDiseasesAutoComplete);
        public static string Action => nameof(Action);
        public static string ActionPatientDiseases => nameof(ActionPatientDiseases);
        public static string RemoveDisease => nameof(RemoveDisease);
    }
    public static class MedicineController
    {
        public static string Medicine => nameof(Medicine);
        public static string GetAllMedicines => nameof(GetAllMedicines);
        public static string GetMedicine => nameof(GetMedicine);
        public static string GetMedicineAutoComplete => nameof(GetMedicineAutoComplete);
        public static string Action => nameof(Action);
        public static string ActionPatientMedicine => nameof(ActionPatientMedicine);
        public static string RemoveMedicine => nameof(RemoveMedicine);
    }
    public static class SurgeryController
    {
        public static string Surgery => nameof(Surgery);
        public static string GetAllSurgeries => nameof(GetAllSurgeries);
        public static string GetSurgery => nameof(GetSurgery);
        public static string GetSurgeriesAutoComplete => nameof(GetSurgeriesAutoComplete);
        public static string Action => nameof(Action);
        public static string ActionPatientSurgery => nameof(ActionPatientSurgery);
        public static string RemoveSurgery => nameof(RemoveSurgery);
    }
    public static class NoteController
    {
        public static string Note => nameof(Note);
        public static string GetAllNotes => nameof(GetAllNotes);
        public static string GetNote => nameof(GetNote);
        public static string Action => nameof(Action);
        public static string RemoveNote => nameof(RemoveNote);
    }
    public static class BrandController
    {
        public static string Brand => nameof(Brand);
        public static string GetAllBrands => nameof(GetAllBrands);
        public static string ActionBrand => nameof(ActionBrand);
        public static string RemoveBrand => nameof(RemoveBrand);
    }
    public static class MultiLevelCategoryController
    {
        public static string MultiLevelCategory => nameof(MultiLevelCategory);        
        public static string ActionMultiLevelCategory => nameof(ActionMultiLevelCategory);
        public static string RemoveMultiLevelCategory => nameof(RemoveMultiLevelCategory);
        public static string GetMultiLevelCategories => nameof(GetMultiLevelCategories);
    }
    public static class AuthController
    {
        public static string Account => nameof(Account);
        public static string CPUserLogin => nameof(CPUserLogin);
        public static string NormalUserLogin => nameof(NormalUserLogin);
    }
    public static class PartnersController
    {
        public static string Partners => nameof(Partners);
        public static string GetPartners => nameof(GetPartners);
        public static string GetPartner => nameof(GetPartner);
        public static string ActionPartner => nameof(ActionPartner);
        public static string RemovePartner => nameof(RemovePartner);
    }
    public static class ServiceController
    {
        public static string Service => nameof(Service);
        public static string GetServices => nameof(GetServices);
        public static string RemoveServiceById => nameof(RemoveServiceById);
        public static string GetServiceDetails => nameof(GetServiceDetails);
        public static string ServiceAction => nameof(ServiceAction);
    }
    public static class ProjectController
    {
        public static string Project => nameof(Project);
        public static string GetProjects => nameof(GetProjects);
        public static string GetProjectDetails => nameof(GetProjectDetails);
        public static string RemoveProject => nameof(RemoveProject);
        public static string AddProject => nameof(AddProject);
        public static string UpdateProject => nameof(UpdateProject);
    }
    public static class GamificationManagmentsController
    {
        public static string GamificationManagments => nameof(GamificationManagments);
        public static string GetAllGamificationSections => nameof(GetAllGamificationSections);
        public static string UpdatePointsForGamificactionSections => nameof(UpdatePointsForGamificactionSections);
        public static string AddNewGamificationSection => nameof(AddNewGamificationSection);
        public static string GetUsersWithTheirPoints => nameof(GetUsersWithTheirPoints);
    }
    public static class GamificationMovementsController
    {
        public static string GamificationMovements => nameof(GamificationMovements);
        public static string SaveSocialMediaPoints => nameof(SaveSocialMediaPoints);
    }
    public static class CouponManagementController
    {
        public static string CouponManagement => nameof(CouponManagement);
        public static string GetCoupons => nameof(GetCoupons);
        public static string GetCouponDetails => nameof(GetCouponDetails);
        public static string RemoveCoupon => nameof(RemoveCoupon);
        public static string AddCoupon => nameof(AddCoupon);
        public static string UpdateCoupon => nameof(UpdateCoupon);
    }
    public static class ApplyingCouponsSystemController
    {
        public static string ApplyingCouponsSystem => nameof(ApplyingCouponsSystem);
        public static string GetAvailableCoupons => nameof(GetAvailableCoupons);
        public static string CouponBooking => nameof(CouponBooking);
        public static string ActivateCouponBooking => nameof(ActivateCouponBooking);
        public static string DeActivateCouponBooking => nameof(DeActivateCouponBooking);
        public static string GetUserCoupons => nameof(GetUserCoupons);
        public static string PointsDeductionFromUser => nameof(PointsDeductionFromUser);
    }
    public static class ExperienceManagementController
    {
        public static string ExperienceManagement => nameof(ExperienceManagement);
        public static string GetExperiencesForUser => nameof(GetExperiencesForUser);
        public static string GetOneExperience => nameof(GetOneExperience);
        public static string RemoveExperience => nameof(RemoveExperience);
        public static string AddExperience => nameof(AddExperience);
        public static string UpdateExperience => nameof(UpdateExperience);
    }
    public static class EducationManagementController
    {
        public static string EducationManagement => nameof(EducationManagement);
        public static string GetEducationsForUser => nameof(GetEducationsForUser);
        public static string GetOneEducation => nameof(GetOneEducation);
        public static string RemoveEducation => nameof(RemoveEducation);
        public static string AddEducation => nameof(AddEducation);
        public static string UpdateEducation => nameof(UpdateEducation);
    }

    public static class LicenseManagementController
    {
        public static string LicenseManagement => nameof(LicenseManagement);
        public static string GetLicensesForUser => nameof(GetLicensesForUser);
        public static string GetOneLicense => nameof(GetOneLicense);
        public static string RemoveLicense => nameof(RemoveLicense);
        public static string AddLicense => nameof(AddLicense);
        public static string UpdateLicense => nameof(UpdateLicense);
    }

    public static class CertificateManagementController
    {
        public static string CertificateManagement => nameof(CertificateManagement);
        public static string GetCertificatesForUser => nameof(GetCertificatesForUser);
        public static string GetCertificate => nameof(GetCertificate);
        public static string RemoveCertificate => nameof(RemoveCertificate);
        public static string AddCertificate => nameof(AddCertificate);
        public static string UpdateCertificate => nameof(UpdateCertificate);
    }
    public static class LectureManagementController
    {
        public static string LectureManagement => nameof(LectureManagement);
        public static string GetLecturesForUser => nameof(GetLecturesForUser);
        public static string GetOneLecture => nameof(GetOneLecture);
        public static string RemoveLecture => nameof(RemoveLecture);
        public static string AddLecture => nameof(AddLecture);
        public static string UpdateLecture => nameof(UpdateLecture);
    }

    public static class BuildResumeController
    {
        public static string BuildResume => nameof(BuildResume);
        public static string GetResumeForHcpUser => nameof(GetResumeForHcpUser);
    }

}

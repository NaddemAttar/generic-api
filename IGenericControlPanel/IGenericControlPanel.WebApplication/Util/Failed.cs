﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

namespace IGenericControlPanel.WebApplication.Util
{
    public static class Failed
    {
        public static JsonResult FaildOperation()
        {
            return new JsonResult(new { ErrorMessage = "" }); 
        }
    }
}

﻿
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.SharedKernal.Constants;

namespace IGenericControlPanel.WebApplication.Util
{
    public static class FilesExtensionMethods
    {
        //private static readonly string wwwRootBath = "wwwroot/";
        public static async Task<List<FileDetailsDto>> SaveFiles(this FileOptions fileOptions)
        {
            List<FileDetailsDto> fileToAdd = new();
            if (fileOptions.Files.Count != 0)
            {
                foreach (IFormFile image in fileOptions.Files)
                {
                    var result = await SavingFileInWWWRootFolder(image,
                                      fileOptions.FilePath, fileOptions.HttpFileService);

                    try
                    {
                        fileToAdd.Add(fileOptions.Mapper.Map<FileDetailsDto>(result.Result,
                            moo => moo.Items.Add(ItemNames.FileItems,
                             (ContentType: fileOptions.FileContentType,
                              SourceType: fileOptions.FileSourceType,
                              SourceName: fileOptions.ControllerName,
                              SourceId: fileOptions.SourceId,
                              EntityName: "",
                              EntityDescription: ""))));
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
                return fileToAdd;
            }

            return null;
        }
        public static async Task<FileDetailsDto> SaveFile(FileOptions fileOptions)
        {
            FileDetailsDto fileToAdd = new();
            if (fileOptions.File != null)
            {
                var result = await SavingFileInWWWRootFolder(fileOptions.File, fileOptions.FilePath, fileOptions.HttpFileService);
                try
                {
                    fileToAdd = fileOptions.Mapper.Map<FileDetailsDto>(result.Result,
                        moo => moo.Items.Add(ItemNames.FileItems,
                         (ContentType: fileOptions.FileContentType,
                          SourceType: fileOptions.FileSourceType,
                          SourceName: fileOptions.ControllerName,
                          SourceId: fileOptions?.SourceId ?? 0,
                          EntityName: "",
                          EntityDescription: "")));
                }
                catch (Exception)
                {
                    return null;
                }

                return fileToAdd;
            }

            return null;
        }
        public static async Task<OperationResult<GenericOperationResult, IFileDto>> SavingFileInWWWRootFolder(
            IFormFile imageFile,
            string imagePath, IHttpFileService httpFileService)
        {
            return await httpFileService.SaveFile(imagePath, imageFile).ConfigureAwait(false);

            //var removePath = result.Result.Url;
            // result.Result.Url = result.Result.Url;

            // httpFileService.RemoveFile(removePath);

            //using (var imageStream = new FileStream(wwwRootBath + result.Result.Url, FileMode.Create))
            //{
            //    imageFile.CopyTo(imageStream);
            //}

            //return result;
        }
        public static async Task<OperationResult<GenericOperationResult, bool>> RemoveFileFromWWWRoot(
            string path, IHttpFileService httpFileService)
        {
            httpFileService.RemoveFile(path);
            return new OperationResult<GenericOperationResult, bool>
            {
                EnumResult = GenericOperationResult.Success,
                Result = true,
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Configuration.Dto;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.SharedKernal.Enums.Core;
using IGenericControlPanel.WebApplication.ViewModels.Shared;

using Microsoft.AspNetCore.Mvc.Rendering;

using Newtonsoft.Json;

namespace IGenericControlPanel.WebApplication.Util
{
    public static class ExtensionMethods
    {

        public static bool DataIsValid<T>(this OperationResult<GenericOperationResult, T> result)
        {
            return result.IsSuccess && result.Result != null;
        }

        public static bool IEnumerableDataIsValid<T>(this OperationResult<GenericOperationResult, IEnumerable<T>> result)
        {
            return result.IsSuccess && result.Result.Count() != 0;
        }
        public static string GetKey(
           this string subKey, bool isHcpUser, int userId)
        {
            if (isHcpUser)
                return $"{subKey}_{userId}";

            return $"{subKey}";
        }
        public static string GetKeyWithPagesAsync(
           this string subKey, bool isHcpUser,
           int userId, int? pageNum, int? pageSize)
        {
            if (isHcpUser)
                return $"{subKey}_{userId}_{pageNum}_{pageSize}";

            return $"{subKey}_{pageNum}_{pageSize}";
        }
        public static int GetStatusCodeType(GenericOperationResult genericOperationResult)
        {
            switch (genericOperationResult)
            {
                case GenericOperationResult.Success:
                    return 200;
                case GenericOperationResult.ValidationError:
                    return 400;
                case GenericOperationResult.Failed:
                    return 500;
                default:
                    return 400;
            }
        }

        public static List<T> ConvertJsonStringToObjectModal<T>(this string jsonString)
        {
            if (string.IsNullOrEmpty(jsonString)) return new List<T>();
            return JsonConvert.DeserializeObject<List<T>>(jsonString);
        }
        public static string NameOnly(this string controllerName)
        {
            return controllerName.Replace("Controller", string.Empty);
        }
        public static TabViewModel GetTabViewModel(this IEnumerable<UserLanguageDto> languages, string primaryTabId = "ar-SY", string primaryLanguage = "ar-SY")
        {
            return new TabViewModel(primaryTabId, primaryLanguage)
            {
                TranslationTabs = languages
                .Select(lang => new TabViewModel(lang.CultureCode, lang.CultureCode))
            };
        }



        public static string FormatString(this DateTime date)
        {
            return date.ToString("dd/MM/yyyy");
        }

        public static ICollection<SelectListItem> ToSelectListItems(this IEnumerable<CarouselDetialsDto> carousels)
        {
            if (carousels is null)
            {
                throw new ArgumentNullException("carousels cannot be null");
            }

            return carousels.Select(entity => new SelectListItem()
            {
                Text = entity.Name?.ToString(),
                Value = entity.Id.ToString()
            }).ToList();
        }
        public static ICollection<SelectListItem> ToSelectListItems(this IEnumerable<string> values, Type enumType = null)
        {
            if (values is null)
            {
                throw new ArgumentNullException("carousels cannot be null");
            }

            if (enumType != null)
            {
                if (enumType == typeof(AdditionalFeatures))
                {
                    return values.Select(entity => new SelectListItem()
                    {
                        Text = entity,
                        Value = ((AdditionalFeatures)Enum.Parse(typeof(AdditionalFeatures), entity, true)).ToString(),
                    }).ToList();
                }
                else if (enumType == typeof(HomeDirection))
                {
                    return values.Select(entity => new SelectListItem()
                    {
                        Text = entity,
                        Value = ((HomeDirection)Enum.Parse(typeof(HomeDirection), entity, true)).ToString(),
                    }).ToList();
                }
            }

            return values.Select(entity => new SelectListItem()
            {
                Text = "About us",
                Value = "Home/AboutUs"
            }).ToList();
        }

        public static string GetExtension(string ext)
        {
            switch (ext)
            {
                case "plain":
                    return "txt";

                case "pdf":
                    return "pdf";

                case "vnd.openxmlformats-officedocument.presentationml.presentation":
                    return "pptx";

                case "vnd.openxmlformats-officedocument.wordprocessingml.document":
                    return "docx";

                case "jpeg":
                    return "jpg";

                case "png":
                    return "png";
            }
            return "";
        }


    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.WebApplication.Util
{
    public static class SectionNames
    {
        public const string Styles = nameof(Styles);
        public const string Scripts = nameof(Scripts);
        public const string Modals = nameof(Modals);
    }
}

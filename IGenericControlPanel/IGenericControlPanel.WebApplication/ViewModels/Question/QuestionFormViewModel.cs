﻿using IGenericControlPanel.Content.Dto;

namespace IGenericControlPanel.WebApplication.ViewModels.Question
{
    public class QuestionFormViewModel
    {
        public QuestionFormViewModel()
        {
            Attachments = new List<FileDetailsDto>();
            RemovedFilesIds = new List<int>();
            MultiLevelCategoryIds = new List<int>();
        }
        public int Id { get; set; }
        public string Content { get; set; }
        public string CultureCode { get; set; }
        public List<FileDetailsDto> Attachments { get; set; }
        public IList<IFormFile> AttachedFiles { get; set; }
        public List<int> MultiLevelCategoryIds { get; set; }
        public bool SendEmail { get; set; } = false;
        public List<int> RemovedFilesIds { get; set; }
        public int? ServiceId { get; set; }
        public bool FAQ { get; set; }
        public int? PostTypeId { get; set; }
        public int? GroupId { get; set; }
        public bool ShowFirst { get; set; }
        public bool IsHidden { get; set; }
    }
}
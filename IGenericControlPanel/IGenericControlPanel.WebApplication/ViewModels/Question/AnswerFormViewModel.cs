﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IGenericControlPanel.Content.Dto;

using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.WebApplication.ViewModels.Question
{
    public class AnswerFormViewModel
    {
        public AnswerFormViewModel()
        {
            Attachments = new List<FileDetailsDto>();
            RemovedFilesIds = new List<int>();
        }
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string Content { get; set; }
        public List<int> RemovedFilesIds { get; set; }
        public List<FileDetailsDto> Attachments { get; set; }
        public IList<IFormFile> AttachedFiles { get; set; }

    }
}

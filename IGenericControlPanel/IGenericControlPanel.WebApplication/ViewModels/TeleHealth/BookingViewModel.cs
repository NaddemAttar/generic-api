﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.WebApplication.ViewModels.TeleHealth
{
    public class BookingSettingViewModel
    {
        public int Id { get; set; }
        public int StartOfPatientReception { get; set; }
        public int EndOfPatientReception { get; set; }
    }
}

﻿namespace IGenericControlPanel.WebApplication.ViewModels.TeleHealth
{
    public class SessionTypeViewModel
    {
        public int Id { get; set; }
        public int Duration { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        //public int ServiceId { get; set; }
    }
}
﻿using IGenericControlPanel.SharedUI.ViewModels;

namespace IGenericControlPanel.WebApplication.ViewModels.Category
{
    public class IndexViewModel : BaseViewModel
    {
        public IndexViewModel(string PageTitle):base(PageTitle)
        {

        }
        public CategoryTableResultViewModel TableResultViewModel { get; set; }

        public FormViewModel FormViewModel { get; set; }

    }
}

﻿using IGenericControlPanel.Configuration.Dto;
using IGenericControlPanel.SharedUI.ViewModels;
using System.Collections.Generic;

namespace IGenericControlPanel.WebApplication.ViewModels.Category
{
    public class CategoryTableHeaderViewModel : BaseViewModel
    {
        public CategoryTableHeaderViewModel(string PageTitle):base(PageTitle)
        {

        }
        public string PrimaryLanguageCultureCode { get; set; }
        public IEnumerable<UserLanguageDto> UserSpecifiedCultures { get; set; }
    }
}

﻿using System.Collections.Generic;

using IGenericControlPanel.Configuration.Dto;

namespace IGenericControlPanel.WebApplication.ViewModels.Category
{
    public class CategoryTableBodyRowViewModel
    {
        public int CategoryId { get; set; }
        public int Order { get; set; }
        public int ProductCount { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<UserLanguageDto> UserSpecifiedCultures { get; set; }


        public string CultureCode { get; set; }

    }
}

﻿using System.Collections.Generic;
using IGenericControlPanel.Configuration.Dto;
using IGenericControlPanel.SharedUI.ViewModels;

namespace IGenericControlPanel.WebApplication.ViewModels.Category
{
    public class CategoryTableResultViewModel : BaseViewModel
    {
        public CategoryTableResultViewModel(string PageTitle):base(PageTitle)
        {
        }
        public string PrimaryLanguageCultureCode { get; set; }
        public IEnumerable<UserLanguageDto> UserSpecifiedCultures { get; set; }
        public IEnumerable<CategoryTableBodyRowViewModel> CategoriesTableBodyRowsViewModel { get; set; }

    }
}

﻿using System.Collections.Generic;

using IGenericControlPanel.Configuration.Dto;
using IGenericControlPanel.SharedUI.ViewModels;

namespace IGenericControlPanel.WebApplication.ViewModels.Category
{
    public class CategoryModalFormViewModel : BaseViewModel
    {
        public CategoryModalFormViewModel(string PageTitle) : base(PageTitle)
        {

        }
        public int Id { get; set; }
        public string PrimaryCultureCode { get; set; }

        public string PrimaryLanguageName { get; set; }
        public string PrimaryLanguageDescription { get; set; }

        public int Order { get; set; }
        public List<UserLanguageDto> UserSpecifiedCultures { get; set; }


    }
}

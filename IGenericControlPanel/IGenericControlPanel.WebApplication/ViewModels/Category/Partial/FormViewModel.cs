﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using IGenericControlPanel.WebApplication.ViewModels.Shared;

using Microsoft.AspNetCore.Mvc.Rendering;

namespace IGenericControlPanel.WebApplication.ViewModels.Category
{
    public class FormViewModel
    {
        public FormViewModel()
        {
            TranslationForms = new List<TranslationFormViewModel>();
        }

        public int CategoryId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public int? Order { get; set; }
        [Required]
        public string CultureCode { get; set; }


        public IEnumerable<int> SelectedDepartments { get; set; }
        public IEnumerable<SelectListItem> Departments { get; set; }

        public IList<TranslationFormViewModel> TranslationForms { get; set; }

        public TabViewModel TabViewModel { get; set; }






    }
}

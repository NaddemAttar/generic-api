﻿
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.WebApplication.ViewModels.Blog
{
    public class BlogFormViewModel : BlogPrimaryLanguageFormDto
    {
        public BlogFormViewModel()
        {
            RemovedFilesIds = new List<int>();
            MultiLevelCategoryIds = new List<int>();
            AttachedFiles = new List<IFormFile>();
        }
        /// <summary>
        /// //////////////
        /// </summary>
        /// 
        //public bool HasImage { get; set; }
        //public IFormFile Image { get; set; }
        public IList<IFormFile> AttachedFiles { get; set; }
        //public List<string> CurrentImages { get; set; }
        //public IEnumerable<int> ImagesId { get; set; }

        public BlogType BlogType { get; set; }
        public List<int>? RemovedFilesIds { get; set; }
        public List<int> MultiLevelCategoryIds { get; set; }
        public bool SendEmail { get; set; } = false;
        public bool IsHidden { get; set; }
        public bool ShowFirst { get; set; }

        //public BlogType BlogType { get; set; }
    }
}

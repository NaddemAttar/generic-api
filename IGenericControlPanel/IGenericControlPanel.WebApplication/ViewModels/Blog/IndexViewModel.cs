﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IGenericControlPanel.SharedUI.ViewModels;
using IGenericControlPanel.WebApplication.ViewModels.Blog.TableResults;

namespace IGenericControlPanel.WebApplication.ViewModels.Blog
{
    public class IndexViewModel: BaseViewModel
    {
        public IndexViewModel(string PageTitle) : base(PageTitle)
        { }
        public BlogTableResultsViewModel BlogTableResultsViewModel { get; set; }
        public BlogSearchViewModel BlogSearchViewModel { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CraftLab.Core.Infrastructure;

using IGenericControlPanel.Core.Dto;

using X.PagedList;

namespace IGenericControlPanel.WebApplication.ViewModels.Blog
{
    public class BlogSearchViewModel
    {
        #region Properties
        public int? PageNumber { get; set; }
        public IEnumerable<BlogDto> Blogs { get; set; }
        public IPagedList InfoPageList { get; set; }

        #region Filtering Properties
        public string Query { get; set; }
        public int? SelectedCategoryId { get; set; }

        #endregion
        #endregion
    }
}

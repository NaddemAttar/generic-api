﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.WebApplication.ViewModels.Blog.TableResults
{
    public class BlogTableResultsViewModel
    {
        public BlogTableResultsHeaderViewModel BlogTableResultsHeaderViewModel { get; set; }
        public BlogTableResultsBodyViewModel BlogTableResultsBodyViewModel { get; set; }
    }
}

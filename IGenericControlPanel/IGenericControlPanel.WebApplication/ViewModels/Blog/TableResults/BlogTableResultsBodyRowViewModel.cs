﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IGenericControlPanel.Configuration.Dto;
using IGenericControlPanel.Core.Dto;

namespace IGenericControlPanel.WebApplication.ViewModels.Blog.TableResults
{
    public class BlogTableResultsBodyRowViewModel
    {
        public BlogDetailsDto BlogDetailsDto { get; set; }

        public IEnumerable<UserLanguageDto> UserSpecifiedCultures { get; set; }

        public string PrimaryLanguageCultureCode { get; set; }
    }
}

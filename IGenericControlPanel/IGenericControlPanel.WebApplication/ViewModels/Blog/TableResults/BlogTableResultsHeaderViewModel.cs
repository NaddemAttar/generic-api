﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IGenericControlPanel.Configuration.Dto;

namespace IGenericControlPanel.WebApplication.ViewModels.Blog.TableResults
{
    public class BlogTableResultsHeaderViewModel
    {
        public string PrimaryLanguageCultureCode { get; set; }
        public IEnumerable<UserLanguageDto> UserSpecifiedCultures { get; set; }
    }
}

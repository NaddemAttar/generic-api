﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.WebApplication.ViewModels.Blog.TableResults
{
    public class BlogTableResultsBodyViewModel
    {
        public IEnumerable<BlogTableResultsBodyRowViewModel> BlogTableResultsBodyRowViewModel { get; set; }

    }
}

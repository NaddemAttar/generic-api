﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IGenericControlPanel.Security.Dto.Permissions;

namespace IGenericControlPanel.WebApplication.ViewModels.Permissions
{
    public class PermissionViewModel
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public IEnumerable<WebContentDto> WebContents { get; set; }
        public IEnumerable<int> WebContentsId { get; set; }
    }
}

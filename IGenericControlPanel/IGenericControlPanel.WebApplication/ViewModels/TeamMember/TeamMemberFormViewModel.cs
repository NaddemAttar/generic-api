﻿using IGenericControlPanel.HR.Dto;
using IGenericControlPanel.SharedUI.ViewModels;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace IGenericControlPanel.WebApplication.ViewModels.TeamMember
{
    public class TeamMemberFormViewModel : BaseViewModel
    {
        public TeamMemberFormViewModel(string PageTitle):base(PageTitle)
        {
        }
        public TeamMemberFormViewModel() : base("TeamMember Page")
        {
        }

        public TeamMemberPrimaryLanguageFormDto PrimaryLanguageFormDto { get; set; }

        public IList<TeamMemberTranslationFormDto> TranslationsFormDtos { get; set; }

        public ICollection<IFormFile> Images { get; set; }
        public bool HasImage { get; set; }
        public ContentControlViewModel ImageControlViewModel { get; set; }
    }
}
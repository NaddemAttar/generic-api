﻿using IGenericControlPanel.Configuration.Dto;
using IGenericControlPanel.HR.Dto;
using System.Collections.Generic;

namespace IGenericControlPanel.WebApplication.ViewModels.TeamMember
{
    public class TeamMemberTableResultsBodyRowViewModel
    {
        public TeamMemberDetailsDto TeamMemberDetails { get; set; }

        public string PrimaryLanguageCultureCode { get; set; }

        public IEnumerable<UserLanguageDto> UserSpecifiedCultures { get; set; }

    }
}
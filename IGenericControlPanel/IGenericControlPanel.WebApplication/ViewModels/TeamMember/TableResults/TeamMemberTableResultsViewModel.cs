﻿namespace IGenericControlPanel.WebApplication.ViewModels.TeamMember
{
    public class TeamMemberTableResultsViewModel
    {
        public TeamMemberTableResultsHeaderViewModel TeamMemberTableResultsHeaderViewModel { get; set; }
        public TeamMemberTableResultsBodyViewModel   TeamMemberTableResultsBodyViewModel { get; set; }
    }
}
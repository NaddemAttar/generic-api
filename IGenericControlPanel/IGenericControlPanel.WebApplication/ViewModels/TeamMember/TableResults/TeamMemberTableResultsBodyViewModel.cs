﻿using System.Collections.Generic;

namespace IGenericControlPanel.WebApplication.ViewModels.TeamMember
{
    public class TeamMemberTableResultsBodyViewModel
    {

        public IEnumerable<TeamMemberTableResultsBodyRowViewModel> TeamMembersTableResultsBodyRowViewModels { get; set; }
    }
}
﻿using IGenericControlPanel.Configuration.Dto;
using System.Collections.Generic;

namespace IGenericControlPanel.WebApplication.ViewModels.TeamMember
{
    public class TeamMemberTableResultsHeaderViewModel
    {
        public string PrimaryLanguageCultureCode { get; set; }
        public IEnumerable<UserLanguageDto> UserSpecifiedCultures { get; set; }
    }
}
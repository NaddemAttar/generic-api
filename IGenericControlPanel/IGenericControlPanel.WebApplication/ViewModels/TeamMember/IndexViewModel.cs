﻿using IGenericControlPanel.HR.Dto;
using IGenericControlPanel.SharedUI.ViewModels;
using System.Collections.Generic;

namespace IGenericControlPanel.WebApplication.ViewModels.TeamMember
{
    public class IndexViewModel : BaseViewModel
    {
        public IndexViewModel(string PageTitle):base(PageTitle)
        {

        }
        #region Properties

        public IEnumerable<TeamMemberDto> TeamMembers { get; set; }
        public TeamMemberTableResultsViewModel TeamMemberTableResultsViewModel { get; set; }
        public TeamMemberSearchViewModel TeamMemberSearchViewModel { get; set; }

        #endregion

    }
}
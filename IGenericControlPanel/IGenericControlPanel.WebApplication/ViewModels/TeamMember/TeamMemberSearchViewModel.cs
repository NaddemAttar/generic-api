﻿using IGenericControlPanel.HR.Dto;
using System.Collections.Generic;
using X.PagedList;

namespace IGenericControlPanel.WebApplication.ViewModels.TeamMember
{
    public class TeamMemberSearchViewModel
    {
        #region Properties

        public int? PageNumber { get; set; }
        public IEnumerable<TeamMemberDto> TeamMembers { get; set; }
        public IPagedList TeamMembersPageList { get; set; }

        #region Filtering Properties
        public string Query { get; set; }
        #endregion

        #endregion
    }
}
﻿using System.ComponentModel.DataAnnotations;
using IGenericControlPanel.SharedUI.ViewModels;

namespace IGenericControlPanel.WebApplication.ViewModels.Account
{
    public class LoginViewModel : BaseViewModel
    {

        public LoginViewModel(string PageTitle):base(PageTitle){}

        public LoginViewModel():base("Login")
        {

        }
        #region Properties
        public string Username { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public bool RememberMe { get; set; }

        #endregion

    }
}
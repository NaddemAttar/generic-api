﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

using IGenericControlPanel.Security.Dto.User;

namespace IGenericControlPanel.WebApplication.ViewModels.Account
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string InTouchPointName { get; set; }
        [Required(ErrorMessage = "Email is Required")]
        [EmailAddress]
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public RoleDto Role { get; set; }
        public string PhoneNumber { get; set; }
    }
}

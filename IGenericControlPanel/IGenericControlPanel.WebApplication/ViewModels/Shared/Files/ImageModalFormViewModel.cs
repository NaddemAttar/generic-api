﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.WebApplication.ViewModels
{
    public class ContentModalFormViewModel
    {
        public string ControllerName { get; private set; }
        public string ModalTitle { get; private set; }

        public ContentModalFormViewModel(string controllerName, string modalTitle)
        {
            ControllerName = controllerName;
            ModalTitle = modalTitle;
        }
        public ContentModalFormViewModel()
        {

        }
        public int ContentId { get; set; }
        public int SourceEntityId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public IFormFile Content { get; set; }
    }


    public class ImageModalFormViewModel
    {
        public string ControllerName { get; set; }
        public string ModalTitle { get; set; }


        public int ImageId { get; set; }
        public int SourceEntityId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public IFormFile Image { get; set; }
    }


    public class VideoModalFormViewModel
    {
        public string ControllerName { get; set; }
        public string ModalTitle { get; set; }


        public int VideoId { get; set; }
        public int SourceEntityId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public IFormFile Video { get; set; }
    }

    public class AttachmentModalFormViewModel
    {
        public string ControllerName { get; set; }
        public string ModalTitle { get; set; }


        public int AttachmentId { get; set; }
        public int SourceEntityId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public IFormFile Attachement { get; set; }
    }
}

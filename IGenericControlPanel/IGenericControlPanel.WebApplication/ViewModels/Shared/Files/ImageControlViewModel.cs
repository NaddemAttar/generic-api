﻿using IGenericControlPanel.Content.Dto;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace IGenericControlPanel.WebApplication.ViewModels
{

    public class ContentControlViewModel
    {
        public string ActionName { get; set; }
        public string ControllerName { get; set; }

        public ContentControlViewModel(string actionName, string controllerName, string modalTitle)
        {
            ActionName = actionName;
            ControllerName = controllerName;
            ContentModalFormViewModel = new ContentModalFormViewModel(controllerName, modalTitle);
        }
        public ContentControlViewModel()
        {

        }
        public int EntityId { get; set; }

        public ICollection<FileDetailsDto> ContentEntities { get; set; }
        public ContentModalFormViewModel ContentModalFormViewModel { get; set; }
    }




    public class ImageControlViewModel
    {
        public ImageControlViewModel()
        {

        }

        public ImageControlViewModel(string actionName, string controllerName, string modalTitle)
        {
            ActionName = actionName;
            ControllerName = controllerName;
            ImageModalFormViewModel = new ImageModalFormViewModel()
            {
                ControllerName = controllerName,
                ModalTitle = modalTitle
            };
        }

        public string ActionName { get; set; }
        public string ControllerName { get; set; }

        public ICollection<FileDetailsDto> Images { get; set; }
        public ImageModalFormViewModel ImageModalFormViewModel { get; set; }
        public int EntityId { get; set; }
    }

    public class VideoControlViewModel
    {

        public string ActionName { get; set; }
        public string ControllerName { get; set; }

        public ICollection<FileDetailsDto> Videos { get; set; }
        public VideoModalFormViewModel VideoModalFormViewModel { get; set; }
        public int EntityId { get; set; }
    }

    public class AttachmentControlViewModel
    {
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public ICollection<FileDetailsDto> Attachments { get; set; }
        public AttachmentModalFormViewModel AttachmentModalFormViewModel { get; set; }
        public int EntityId { get; set; }
    }
}

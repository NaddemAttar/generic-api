﻿using IGenericControlPanel.Security.Dto;
using IGenericControlPanel.SharedUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.WebApplication.ViewModels
{
    public class HeaderViewModel : BaseViewModel
    {
        public HeaderViewModel(string PageTitle):base(PageTitle)
        {

        }
        public UserDto UserDto { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.WebApplication.ViewModels.Shared
{
    public class TabViewModel
    {
        public TabViewModel(string primaryTabId, string primaryLanguage)
        {
            TabId = primaryTabId;
            //if (primaryLanguage.Contains("en"))
            //{
            //    Language = Resources.SharedResource.English;
            //}
            //else if (primaryLanguage.Contains("ar"))
            //{
            //   Language = Resources.SharedResource.Arabic;
            //}
        }
        public string TabId { get;  }
        public string Language { get;  }
        public IEnumerable<TabViewModel> TranslationTabs { get; set; }
    }
}

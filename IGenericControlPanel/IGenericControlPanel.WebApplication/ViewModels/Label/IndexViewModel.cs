﻿using System.Collections.Generic;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.SharedUI.ViewModels;

namespace IGenericControlPanel.WebApplication.ViewModels.Label
{
    public class IndexViewModel
    {
        public IEnumerable<LabelDto> Labels { get; set; }
    }
}
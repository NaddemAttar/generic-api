﻿using System.Collections.Generic;

namespace IGenericControlPanel.WebApplication.ViewModels.Label
{
    public class LabelTableResultsBodyViewModel
    {
        public IEnumerable<LabelTableResultsBodyRowViewModel> LabelsTableResultsBodyRowViewModels { get; set; }
    }
}
﻿using IGenericControlPanel.Core.Dto;

namespace IGenericControlPanel.WebApplication.ViewModels.Label
{
    public class LabelTableResultsBodyRowViewModel
    {
        public LabelDto LabelDto { get; set; }

        public string PrimaryLanguageCultureCode { get; set; }

    }
}
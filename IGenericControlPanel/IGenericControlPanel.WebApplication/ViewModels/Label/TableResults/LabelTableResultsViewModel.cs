﻿namespace IGenericControlPanel.WebApplication.ViewModels.Label
{
    public class LabelTableResultsViewModel
    {
        public LabelTableResultsHeaderViewModel LabelTableResultsHeaderViewModel { get; set; }
        public LabelTableResultsBodyViewModel LabelTableResultsBodyViewModel { get; set; }
    }
}
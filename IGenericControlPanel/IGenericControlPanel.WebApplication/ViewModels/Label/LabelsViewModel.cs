﻿using System.Collections.Generic;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.SharedUI.ViewModels;
using IGenericControlPanel.WebApplication.ViewModels.Shared;

using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.WebApplication.ViewModels.Label
{
    public class LabelsViewModel
    {
        public LabelFormDto LabelFormDto { get; set; }
    }
}
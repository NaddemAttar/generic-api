﻿using CraftLab.Core.BoundedContext;

using IGenericControlPanel.Content.Dto;

namespace IGenericControlPanel.WebApplication.ViewModels.Service
{
    public class ServiceViewModel : IFormDto
    {
        public ServiceViewModel()
        {
            Images = new List<IFormFile>();
            ImagesDetails = new List<FileDetailsDto>();
            RemovedFilesUrls = new List<string>();
            RemovedFilesIdss = new List<int>();
            HealthCareSpecialtyIds = new List<int?>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        //public double Price { get; set; }
        public string CultureCode { get; set; }
        public bool ShowFirst { get; set; }
        public bool IsHidden { get; set; }
        public int Order { get; set; }
        public ICollection<FileDetailsDto> ImagesDetails { get; set; }
        public List<int?> HealthCareSpecialtyIds { get; set; }
        public IEnumerable<string> RemovedFilesUrls { get; set; }
        public IEnumerable<int> RemovedFilesIdss { get; set; }
        public ActionOperationType OperationType { get; }
        public List<IFormFile> Images { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IGenericControlPanel.SharedUI.ViewModels;

using Newtonsoft.Json;

namespace IGenericControlPanel.WebApplication.ViewModels.Payment
{
    public class PaymentViewModel
        
    {
        public string eventId { get; set; }
        public string eventName { get; set; }
        public Order order { get; set; }
        public class _3ds
        {
            public string status { get; set; }
            public string eci { get; set; }
            public string eciDescription { get; set; }
            public string summaryText { get; set; }
        }

        public class Amount
        {
            public string currencyCode { get; set; }
            public int value { get; set; }
        }
        public class AuthResponse
        {
            public string authorizationCode { get; set; }
            public bool success { get; set; }
            public string resultCode { get; set; }
            public string resultMessage { get; set; }
            public string mid { get; set; }
            public string rrn { get; set; }
        }
        public class BillingAddress
        {
            public string firstName { get; set; }
            public string lastName { get; set; }
        }
        public class CnpCancel
        {
            public string href { get; set; }
        }
        public class Cury
        {
            public string name { get; set; }
            public string href { get; set; }
            public bool templated { get; set; }
        }
        public class Embedded
        {
            public List<Payment> payment { get; set; }
        }
        public class FormattedOrderSummary
        {
            public string total { get; set; }
            public List<string> items { get; set; }
        }
        public class Item
        {
            public string description { get; set; }
            public TotalPrice totalPrice { get; set; }
            public int quantity { get; set; }
        }
        public class Links
        {
            public Self self { get; set; }

            [JsonProperty("tenant-brand")]
            public TenantBrand TenantBrand { get; set; }

            [JsonProperty("merchant-brand")]
            public MerchantBrand MerchantBrand { get; set; }

            [JsonProperty("cnp:cancel")]
            public CnpCancel CnpCancel { get; set; }
            public List<Cury> curies { get; set; }
        }
        public class MerchantAttributes
        {
            public string paymentAttempts { get; set; }
            public string redirectUrl { get; set; }
        }
        public class MerchantBrand
        {
            public string href { get; set; }
        }
        public class MerchantDefinedData
        {
        }
        public class Order
        {
            public string _id { get; set; }
            public Links _links { get; set; }
            public string type { get; set; }
            public MerchantDefinedData merchantDefinedData { get; set; }
            public string action { get; set; }
            public Amount amount { get; set; }
            public string language { get; set; }
            public MerchantAttributes merchantAttributes { get; set; }
            public string emailAddress { get; set; }
            public string reference { get; set; }
            public string outletId { get; set; }
            public DateTime createDateTime { get; set; }
            public PaymentMethods paymentMethods { get; set; }
            public OrderSummary orderSummary { get; set; }
            public BillingAddress billingAddress { get; set; }
            public string referrer { get; set; }
            public string formattedAmount { get; set; }
            public FormattedOrderSummary formattedOrderSummary { get; set; }
            public Embedded _embedded { get; set; }
        }
        public class OrderSummary
        {
            public List<Item> items { get; set; }
            public Total total { get; set; }
        }
        public class Payment
        {
            public string _id { get; set; }
            public Links _links { get; set; }
            public string reference { get; set; }
            public PaymentMethod paymentMethod { get; set; }
            public string state { get; set; }
            public Amount amount { get; set; }
            public DateTime updateDateTime { get; set; }
            public string outletId { get; set; }
            public string orderReference { get; set; }
            public AuthResponse authResponse { get; set; }
            public _3ds _3ds { get; set; }
        }
        public class PaymentMethod
        {
            public string expiry { get; set; }
            public string cardholderName { get; set; }
            public string name { get; set; }
            public string pan { get; set; }
        }
        public class PaymentMethods
        {
            public List<string> card { get; set; }
        }
        public class Self
        {
            public string href { get; set; }
        }
        public class TenantBrand
        {
            public string href { get; set; }
        }
        public class Total
        {
            public string currencyCode { get; set; }
            public int value { get; set; }
        }
        public class TotalPrice
        {
            public string currencyCode { get; set; }
            public int value { get; set; }
        }
    }
}

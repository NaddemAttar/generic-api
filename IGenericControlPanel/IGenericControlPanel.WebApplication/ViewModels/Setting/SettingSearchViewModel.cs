﻿using IGenericControlPanel.Configuration.Dto;
using System.Collections.Generic;
using X.PagedList;

namespace IGenericControlPanel.WebApplication.ViewModels.Setting
{
    public class SettingSearchViewModel
    {
        #region Properties

        public int? PageNumber { get; set; }
        public IEnumerable<SettingDto> Settings { get; set; }
        public IPagedList SettingsPageList { get; set; }

        #region Filtering Properties

        public string Query { get; set; }
        #endregion
        #endregion
    }
}
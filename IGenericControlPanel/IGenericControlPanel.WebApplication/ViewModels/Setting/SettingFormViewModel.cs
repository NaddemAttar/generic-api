﻿using IGenericControlPanel.Configuration.Dto;
using IGenericControlPanel.SharedUI.ViewModels;

namespace IGenericControlPanel.WebApplication.ViewModels.Setting
{
    public class SettingsFormViewModel : BaseViewModel
    {

        public SettingsFormViewModel(string PageTitle):base(PageTitle)
        {
        }
        public SettingsFormViewModel() : base("Setting Page")
        {
        }
        public SettingFormDto PrimaryLanguageFormDto { get; set; }

        public string LocationLongitude { get; set; }
        public string LocationLatitude { get; set; }

    }
}
﻿using IGenericControlPanel.Configuration.Dto;
using IGenericControlPanel.SharedUI.ViewModels;
using System.Collections.Generic;

namespace IGenericControlPanel.WebApplication.ViewModels.Setting
{
    public class IndexViewModel : BaseViewModel
    {
        public IndexViewModel(string PageTitle):base(PageTitle)
        {

        }
        #region Properties

        public IEnumerable<SettingDto> Settings { get; set; }
        public SettingTableResultsViewModel SettingTableResultsViewModel { get; set; }
        public SettingSearchViewModel SettingSearchViewModel { get; set; }

        #endregion

    }
}
﻿namespace IGenericControlPanel.WebApplication.ViewModels.Setting
{
    public class SettingTableResultsViewModel
    {
        public SettingTableResultsHeaderViewModel SettingTableResultsHeaderViewModel { get; set; }
        public SettingTableResultsBodyViewModel SettingTableResultsBodyViewModel { get; set; }
    }
}
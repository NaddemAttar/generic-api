﻿using System.Collections.Generic;

namespace IGenericControlPanel.WebApplication.ViewModels.Setting
{
    public class SettingTableResultsBodyViewModel
    {
        public IEnumerable<SettingTableResultsBodyRowViewModel> SettingsTableResultsBodyRowViewModels { get; set; }
    }
}
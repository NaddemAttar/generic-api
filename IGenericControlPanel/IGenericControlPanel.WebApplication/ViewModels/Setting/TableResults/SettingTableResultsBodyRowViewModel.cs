﻿using IGenericControlPanel.Configuration.Dto;

namespace IGenericControlPanel.WebApplication.ViewModels.Setting
{
    public class SettingTableResultsBodyRowViewModel
    {
        public SettingDto Setting { get; set; }

        public string PrimaryLanguageCultureCode { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.WebApplication.ViewModels.TimeManagement
{
    public class Time
    {
        public int Minutes { get; set; }
        public int Houres { get; set; }
    }
}

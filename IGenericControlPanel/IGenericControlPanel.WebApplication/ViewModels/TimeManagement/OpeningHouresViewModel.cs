﻿using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.WebApplication.ViewModels.TimeManagement
{
    public class OpeningHouresViewModel
    {
        public int Id { get; set; }
        public Time From { get; set; }
        public Time To { get; set; }
        public bool IsWorkingDay { get; set; }        
    }
}
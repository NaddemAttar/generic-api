﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.Dto.Information;
using IGenericControlPanel.SharedUI.ViewModels;

using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.WebApplication.ViewModels.Info
{
    public class InfoFormViewModel: BaseViewModel
    {
        public InfoFormViewModel(string PageTitle):base(PageTitle)
        {
        }

        public InfoFormViewModel() : base("Info Page")
        {
            Images = new List<FileDetailsDto>();
        }
       
        public int Id { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string CurrentDate { get; set; }
        public List<string> CurrentImgs { get; set; }
        public string CultureCode { get; set; }
        public ICollection<FileDetailsDto> Images { get; set; }
        public IList<InfoTranslationFormDto> TranslationForms { get; set; }

       
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IGenericControlPanel.SharedUI.ViewModels;
using IGenericControlPanel.WebApplication.ViewModels.Info.Partials.TableResults;

namespace IGenericControlPanel.WebApplication.ViewModels.Info
{
    public class IndexViewModel: BaseViewModel
    {
        public IndexViewModel(string PageTitle):base(PageTitle)
        {}
        public InfoTableResultsViewModel InfoTableResultsViewModel { get; set; }
        public InfoSearchViewModel InfoSearchViewModel { get; set; }
    }
}

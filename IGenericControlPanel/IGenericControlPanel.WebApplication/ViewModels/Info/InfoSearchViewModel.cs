﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IGenericControlPanel.Core.Dto.Information;

using X.PagedList;

namespace IGenericControlPanel.WebApplication.ViewModels.Info
{
   
    public class InfoSearchViewModel
    {
        #region Properties
        public int? PageNumber { get; set; }
        public IEnumerable<InfoDto> Informations { get; set; }
        public IPagedList InfoPageList { get; set; }

        #region Filtering Properties

        public string Query { get; set; }
        public int? SelectedCategoryId { get; set; }

        #endregion

        #endregion
    }
}

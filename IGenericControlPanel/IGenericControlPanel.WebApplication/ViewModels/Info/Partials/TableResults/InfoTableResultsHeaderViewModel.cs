﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IGenericControlPanel.Configuration.Dto;

namespace IGenericControlPanel.WebApplication.ViewModels.Info.Partials.TableResults
{
    public class InfoTableResultsHeaderViewModel
    {
        public string PrimaryLanguageCultureCode { get; set; }
        public IEnumerable<UserLanguageDto> UserSpecifiedCultures { get; set; }
    }
}

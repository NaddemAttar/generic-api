﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.WebApplication.ViewModels.Info.Partials.TableResults
{
    public class InfoTableResultsBodyViewModel
    {
        public IEnumerable<InfoTableResultsBodyRowViewModel> InfoTableResultsBodyRowViewModel { get; set; }
    }
}

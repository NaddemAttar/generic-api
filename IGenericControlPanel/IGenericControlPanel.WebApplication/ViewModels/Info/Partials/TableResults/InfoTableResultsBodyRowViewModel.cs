﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IGenericControlPanel.Configuration.Dto;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.Dto.Information;

namespace IGenericControlPanel.WebApplication.ViewModels.Info.Partials.TableResults
{
    public class InfoTableResultsBodyRowViewModel
    {
        public InfoDetailsDto InfoDetailsDto { get; set; }

        public IEnumerable<UserLanguageDto> UserSpecifiedCultures { get; set; }

        public string PrimaryLanguageCultureCode { get; set; }
    }
}

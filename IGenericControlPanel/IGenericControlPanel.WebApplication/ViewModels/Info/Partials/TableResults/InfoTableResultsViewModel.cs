﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.WebApplication.ViewModels.Info.Partials.TableResults
{
    public class InfoTableResultsViewModel
    {
        public InfoTableResultsHeaderViewModel InfoTableResultsHeaderViewModel { get; set; }
        public InfoTableResultsBodyViewModel InfoTableResultsBodyViewModel { get; set; }
    }
}

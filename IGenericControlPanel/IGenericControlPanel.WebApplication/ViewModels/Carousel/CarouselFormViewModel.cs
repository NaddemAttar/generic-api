﻿using CraftLab.Core.BoundedContext;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.SharedUI.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace IGenericControlPanel.WebApplication.ViewModels.Carousel
{
    public class CarouselFormViewModel :IFormDto
    {

        public int Id { get; set; }
        public string Title { get; set; }
        public string TopSubTitle { get; set; }
        public string BottomSubTitle { get; set; }
        public int Order { get; set; }
        public string ButtonActionUrl { get; set; }
        public string ButtonText { get; set; }
        public string CultureCode { get; set; }
        public int CarouselId { get; set; }
        public string ImageUrl { get; set; }
        public string TextColor { get; set; }
        public FileDetailsDto ImageDetails { get; set; }
        public ActionOperationType OperationType { get; }
        public IFormFile Image { get; set; }
        public ICollection<SelectListItem> ActionsUrls { get; set; }
        public ICollection<SelectListItem> Carousels { get; set; }
        public IList<CarouselItemTranslationFormDto> TranslationsFormDtos { get; set; }

    }
}
﻿using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.SharedUI.ViewModels;
using System.Collections.Generic;

namespace IGenericControlPanel.WebApplication.ViewModels.Carousel
{
    public class IndexViewModel : BaseViewModel
    {
        public IndexViewModel(string pageTitle):base(pageTitle){}

        public IndexViewModel() : base("Carousel Page") { }

        #region Properties
        public IEnumerable<CarouselItemDto> Carousels { get; set; }
        public CarouselTableResultsViewModel TableResultsViewModel { get; set; }

        #endregion

    }
}
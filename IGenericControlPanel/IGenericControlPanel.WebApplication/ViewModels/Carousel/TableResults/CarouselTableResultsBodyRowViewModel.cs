﻿using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Configuration.Dto;
using System.Collections.Generic;

namespace IGenericControlPanel.WebApplication.ViewModels.Carousel
{
    public class CarouselTableResultsBodyRowViewModel
    {
        public CarouselItemDetailsDto CarouselDetails { get; set; }

        public string PrimaryLanguageCultureCode { get; set; }

        public IEnumerable<UserLanguageDto> UserSpecifiedCultures { get; set; }

    }
}
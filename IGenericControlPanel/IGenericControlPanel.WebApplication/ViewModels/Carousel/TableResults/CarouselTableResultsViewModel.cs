﻿namespace IGenericControlPanel.WebApplication.ViewModels.Carousel
{
    public class CarouselTableResultsViewModel
    {
        public CarouselTableResultsHeaderViewModel TableResultsHeaderViewModel { get; set; }
        public CarouselTableResultsBodyViewModel TableResultsBodyViewModel { get; set; }
    }
}
﻿using IGenericControlPanel.Configuration.Dto;
using System.Collections.Generic;

namespace IGenericControlPanel.WebApplication.ViewModels.Carousel
{
    public class CarouselTableResultsHeaderViewModel
    {
        public string PrimaryLanguageCultureCode { get; set; }
        public IEnumerable<UserLanguageDto> UserSpecifiedCultures { get; set; }
    }
}
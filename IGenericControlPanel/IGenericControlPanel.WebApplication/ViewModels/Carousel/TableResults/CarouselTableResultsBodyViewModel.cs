﻿using System.Collections.Generic;

namespace IGenericControlPanel.WebApplication.ViewModels.Carousel
{
    public class CarouselTableResultsBodyViewModel
    {

        public IEnumerable<CarouselTableResultsBodyRowViewModel> TableResultsBodyRowViewModels { get; set; }
    }
}
﻿using System.Collections.Generic;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.Dto.Information;
using IGenericControlPanel.Core.Dto.Product;
using IGenericControlPanel.Core.Dto.Service;
using IGenericControlPanel.HR.Dto;
using IGenericControlPanel.SharedUI.ViewModels;

namespace IGenericControlPanel.WebApplication.ViewModels.Home
{
    public class DashboardViewModel : BaseViewModel
    {
        public DashboardViewModel(string PageTitle) : base(PageTitle)
        {

        }

        public IEnumerable<ProductDto> Products { get; set; }




        public IEnumerable<CarouselItemDto> Carousels { get; set; }

        public IEnumerable<TeamMemberDto> TeamMembers { get; set; }

        public IEnumerable<ServiceDto> Services { get; set; }


        public IEnumerable<BlogDto> Blogs { get; set; }
        public IEnumerable<InfoDto> Informations { get; set; }

        public int ProductCount { get; set; }
        public int ConstructionCount { get; set; }
        public int CategoryCount { get; set; }
        public int CarsouselCount { get; set; }
        public int TeamMemberCount { get; set; }
        public int ServiceCount { get; set; }
        public int ProjectCount { get; set; }
        public int BlogsCount { get; set; }
        public int InformationCount { get; set; }
        public int ClientsCount { get; set; }
    }
}

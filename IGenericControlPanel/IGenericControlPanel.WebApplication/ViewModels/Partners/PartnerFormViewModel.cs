﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.WebApplication.ViewModels.Partners
{
    public class PartnerFormViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public IFormFile Image { get; set; }
    }
}

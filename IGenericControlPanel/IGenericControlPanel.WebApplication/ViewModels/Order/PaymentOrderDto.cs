﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.WebApplication.ViewModels.Order
{
    public class PaymentOrderDto
    {
        public string action { get; set; }
        public amount amount { get; set; }
        public string emailAddress { get; set; }
        public string language { get; set; }
        public merchantAttributes merchantAttributes { get; set; }
        public billingAddress billingAddress { get; set; }
        public shippingAddress shippingAddress { get; set; }
    }
    public class amount
    {
        public string currencyCode { get; set; }
        public int value { get; set; }
    }
    public class merchantAttributes
    {
        public string cancelText { get; set; }
        public string cancelUrl { get; set; }
        public string redirectUrl { get; set; }
        public bool skip3DS { get; set; }
        public bool ConfirmationPage { get; set; }
    }
    public class billingAddress
    {
        public string countryCode { get; set; }
        public string city { get; set; }
        public string address1 { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
    }
    public class shippingAddress
    {
        public string countryCode { get; set; }
        public string address1 { get; set; }
        public string city { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
    }
}

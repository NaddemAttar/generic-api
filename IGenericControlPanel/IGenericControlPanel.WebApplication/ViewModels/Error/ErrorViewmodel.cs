﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IGenericControlPanel.SharedUI.ViewModels;

namespace IGenericControlPanel.WebApplication.ViewModels
{
    public class ErrorViewmodel : BaseViewModel
    {
        public ErrorViewmodel(string PageTitle):base(PageTitle)
        {

        }
        public string ErrorMessage { get; set; }
        public string ErrorTitle { get; set; }
        public string OriginalPath { get; set; }
        public string OriginalQueryString { get; set; }

        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);

    }
}

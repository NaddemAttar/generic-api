﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Product;

using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.WebApplication.ViewModels.Product
{
    public class ProductUpdateFormViewModel
    {
        
        public ProductUpdateFormViewModel()
        {
            AttachedFiles = new List<IFormFile>();
            ProductSpecifications = new List<ProductSpecificationDto>();
            RemovedFilesIds = new List<int>();
            Attachments = new List<FileDetailsDto>();


        }
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }

        public string Currency { get; set; }

        [Required]
        public int CategoryId { get; set; }
        [Required]
        public string CultureCode { get; set; }
        public List<FileDetailsDto> Attachments { get; set; }
        public List<int> RemovedFilesIds { get; set; }
        public IList<IFormFile> AttachedFiles { get; set; }
        public List<ProductSpecificationDto> ProductSpecifications { get; set; }
        public int BrandId { get; set; }


        public bool ShowInStore { get; set; }

    }
}

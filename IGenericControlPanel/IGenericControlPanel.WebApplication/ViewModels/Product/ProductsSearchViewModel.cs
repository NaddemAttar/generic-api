﻿using System.Collections.Generic;

using CraftLab.Core.Infrastructure;

using IGenericControlPanel.Core.Dto.Product;

namespace IGenericControlPanel.WebApplication.ViewModels.Product
{
    public class ProductsSearchViewModel
    {
        #region Properties

        public int? PageNumber { get; set; }
        public IPagedList<ProductDto> Products { get; set; }
        public IPagedList<ProductDto> ProductsPageList { get; set; }

        #region Filtering Properties

        public string Query { get; set; }
        public int? SelectedCategoryId { get; set; }

        #endregion

        #endregion

    }
}
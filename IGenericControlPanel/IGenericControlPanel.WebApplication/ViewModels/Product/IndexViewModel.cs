﻿using IGenericControlPanel.SharedUI.ViewModels;

namespace IGenericControlPanel.WebApplication.ViewModels.Product
{
    public class IndexViewModel : BaseViewModel
    {
        public IndexViewModel(string PageTitle):base(PageTitle)
        {

        }
        public ProductsTableResultsViewModel ProductsTableResultsViewModel { get; set; }
        public ProductsSearchViewModel ProductsSearchViewModel { get; set; }

    }
}
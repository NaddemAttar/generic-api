﻿using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Product;
using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.WebApplication.ViewModels.Product
{
    public class ProductFormViewModel
    {

        #region Properties
        public ProductFormViewModel()
        {
            AttachedFiles = new List<IFormFile>();
            ProductSpecifications = new List<ProductSpecificationDto>();
            productSizeColors = new List<ProductSizeColorFormDto>();
        }
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public string CultureCode { get; set; }
        public List<FileDetailsDto> Attachments { get; set; }
        public IList<IFormFile> AttachedFiles { get; set; }
        public List<ProductSpecificationDto> ProductSpecifications { get; set; }
        public int BrandId { get; set; }
        public bool ShowInStore { get; set; }
        public ICollection<ProductSizeColorFormDto> productSizeColors { get; set; }
        #endregion
    }
}
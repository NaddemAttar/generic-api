﻿using System.Collections.Generic;

using IGenericControlPanel.Configuration.Dto;
using IGenericControlPanel.Core.Dto.Product;

namespace IGenericControlPanel.WebApplication.ViewModels.Product
{
    public class ProductsTableResultsBodyRowViewModel
    {
        public ProductDetailsDto ProductDetails { get; set; }

        public IEnumerable<UserLanguageDto> UserSpecifiedCultures { get; set; }

        public string PrimaryLanguageCultureCode { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace IGenericControlPanel.WebApplication.ViewModels.Product
{
    public class ProductsTableResultsBodyViewModel
    {
        public IEnumerable<ProductsTableResultsBodyRowViewModel> ProductsTableResultsBodyRowViewModel { get; set; }
    }
}

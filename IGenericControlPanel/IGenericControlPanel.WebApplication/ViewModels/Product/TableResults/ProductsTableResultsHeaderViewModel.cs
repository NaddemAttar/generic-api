﻿using IGenericControlPanel.Configuration.Dto;
using System.Collections.Generic;

namespace IGenericControlPanel.WebApplication.ViewModels.Product
{
    public class ProductsTableResultsHeaderViewModel
    {
        public string PrimaryLanguageCultureCode { get; set; }
        public IEnumerable<UserLanguageDto> UserSpecifiedCultures { get; set; }
    }
}

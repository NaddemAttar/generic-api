﻿namespace IGenericControlPanel.WebApplication.ViewModels.Product
{
    public class ProductsTableResultsViewModel
    {
        public ProductsTableResultsHeaderViewModel ProductsTableResultsHeaderViewModel { get; set; }
        public ProductsTableResultsBodyViewModel ProductsTableResultsBodyViewModel { get; set; }

    }
}

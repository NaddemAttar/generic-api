﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IGenericControlPanel.SharedKernal.Enums.Security;

using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.WebApplication.ViewModels.Group
{
    public class GroupViewModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public GroupPrivacyLevel GroupPrivacyLevel { get; set; }
        public int? MultiLevelCategoryId { get; set; }
        public IFormFile GroupImage { get; set; }
        public IFormFile CoverImage { get; set; }
        public IEnumerable<int> RemoveFileIds { get; set; }
    }
}

﻿
using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.WebApplication.ViewModels.Feedback
{
    public class FeedbackViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string YouTubeLink { get; set; }
        public string ImageUrl { get; set; }
        public string ClientName { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public IFormFile Image { get; set; }
    }
}

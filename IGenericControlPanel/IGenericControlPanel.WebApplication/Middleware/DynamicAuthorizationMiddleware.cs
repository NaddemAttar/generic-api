﻿
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json.Linq;

namespace IGenericControlPanel.WebApplication.Middleware
{
    public class DynamicAuthorizationMiddleware : IMiddleware
    {
        private IUserRepository _userRepository;
        public DynamicAuthorizationMiddleware(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        string actionType, controllerName, action;
        int? Id;
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var endpoint = context.GetEndpoint();
            //Check if the api is AllowAnonymous Or  Normal Authorize
            bool? isAllowAnonymous = endpoint?.Metadata.Any(x => x.GetType() == typeof(AllowAnonymousAttribute));
            bool? isAuthorize = endpoint?.Metadata.Any(x => x.GetType() == typeof(AuthorizeAttribute));
            if (isAllowAnonymous == true || isAuthorize == true)
            {
                await next(context);
                return;
            }
            //DynamicAuthorization here :
            string userRole = await _userRepository.GetRoleByCurrentUser();
            if (userRole == null)
                throw new UnAuthorizedException("Unauthorized");
            if (nameof(Role.Admin) == userRole 
                || nameof(Role.NormalUser) == userRole 
                || nameof(Role.HealthCareProvider) == userRole ) 
            {
                await next(context);
                return;
            }
            controllerName = context.Request.RouteValues["controller"].ToString();
            actionType = context.Request.RouteValues["action"].ToString();
            Id = await GetId(context);
            if ((actionType.Contains("Action") || action.Contains("Set") || actionType.Contains("Add")) && Id == null || Id==0)
                action = "Action";
            else if((actionType.Contains("Action") || actionType.Contains("Update")) && Id !=null && Id!=0)
                action="Update";
            else if (actionType.Contains("Remove"))
                action = "Remove";
            else if (actionType.Contains("Get"))
                action = "Get";
            string role = OnAuthorization(context,action,controllerName);
           
            if (role.Contains(userRole))
            {
                await next(context);
                return;
            }
            else
            {
                throw new UnAuthorizedException("Unauthorized");
            }
           

        }
        private async Task<int> GetId(HttpContext context) 
        {
            int id=0;
            if (context.Request.HasFormContentType)
            {
                IFormCollection form;
                form = context.Request.Form; 
                form = await context.Request.ReadFormAsync(); // async
                id = int.Parse(form["Id"]);
            }
            else
            {
                context.Request.EnableBuffering();
                var bodyAsText = await new StreamReader(context.Request.Body).ReadToEndAsync();
                JObject json = JObject.Parse(bodyAsText);
                id = Convert.ToInt32(json["id"]);
                context.Request.Body.Position = 0;
            }
            return id;
        }
        private string OnAuthorization(HttpContext context,string action,string controller)
        {
            var dbContext = context.RequestServices.GetRequiredService<IPermissionRepository>();
            var roles = new List<string>();
            roles = dbContext.GetWebContent(controllerName, action).ToList();
            var Roles = string.Join(",", roles);
            return Roles;
        }
    }
}

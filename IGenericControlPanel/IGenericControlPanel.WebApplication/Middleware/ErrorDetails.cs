﻿using Newtonsoft.Json;

namespace IGenericControlPanel.WebApplication.Middleware
{
    public class ErrorDetails
    {
        public int statusCode { get; set; }
        public string errorMessage { get; set; }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}

﻿namespace IGenericControlPanel.WebApplication.Middleware;
public static class DynamicAuthorizationMiddlewareExtension
{
    public static void ConfigureCustomDynamicAuthMiddleware(this IApplicationBuilder app)
    {
        app.UseMiddleware<DynamicAuthorizationMiddleware>();
    }
}

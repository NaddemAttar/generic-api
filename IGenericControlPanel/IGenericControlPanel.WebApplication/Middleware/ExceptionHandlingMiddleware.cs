﻿
using System.Net;
using System.Threading.Tasks;
using IGenericControlPanel.SharedKernal.Logger.Interface;


using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.WebApplication.Middleware
{
    public class ExceptionHandlingMiddleware : IMiddleware
    {
        private ILogService LogService { get; }
        public ExceptionHandlingMiddleware(ILogService LogService)
        {
            this.LogService = LogService;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var x = context.Request.Headers.AcceptLanguage.ToString(); // this return string value ex: "ar" || "en"
            try
            {
                await next(context);
            }
            catch (NotFoundException e)
            {
                LogService.Log(new SharedKernal.Logger.Classes.HITLog() { Exception=e,Message= e.Message });
                await HandleExceptionAsync(context, (int)HttpStatusCode.NotFound, e.Message);
            }

            catch (InternalServerException e)
            {
                LogService.Log(new SharedKernal.Logger.Classes.HITLog() { Exception = e, Message = e.Message });
                await HandleExceptionAsync(context, (int)HttpStatusCode.InternalServerError, e.Message);
            }

            catch (BadRequestException e)
            {
                LogService.Log(new SharedKernal.Logger.Classes.HITLog() { Exception = e, Message = e.Message });
                await HandleExceptionAsync(context, (int)HttpStatusCode.BadRequest, e.Message);
            }

            catch (UnAuthorizedException e)
            {
                LogService.Log(new SharedKernal.Logger.Classes.HITLog() { Exception = e, Message = e.Message });
                await HandleExceptionAsync(context, (int)HttpStatusCode.Unauthorized, e.Message);
            }

            catch (Exception e)
            {
                LogService.Log(new SharedKernal.Logger.Classes.HITLog() { Exception = e, Message = e.Message });
                await HandleExceptionAsync(context, (int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        private Task HandleExceptionAsync(
            HttpContext context, int httpStatusCode, string message)
        {
            context.Response.StatusCode = httpStatusCode;

            return context.Response.WriteAsync(new ErrorDetails
            {
                statusCode = httpStatusCode,
                errorMessage = message
            }.ToString());
        }

    }
}

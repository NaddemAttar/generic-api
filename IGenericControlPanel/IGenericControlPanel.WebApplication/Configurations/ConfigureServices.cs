﻿
using System.Globalization;

using AutoMapper;

using CraftLab.Core.Database.Identity;
using CraftLab.Core.Database.Identity.Interfaces;
using CraftLab.Core.Localization.Extensions;
using CraftLab.Core.Services.HttpFile;
using CraftLab.Core.Services.Renders;

using IGenericControlPanel.AutoMapperConfig.Profiles;
using IGenericControlPanel.Communications.Data.Repositories;
using IGenericControlPanel.Communications.IData.Interfaces;
using IGenericControlPanel.Configuration.Data.Repositories;
using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Consultation.Data.Repositories;
using IGenericControlPanel.Consultation.IData.Interfaces;
using IGenericControlPanel.Content.Data.Repositories;
using IGenericControlPanel.Content.IData;
using IGenericControlPanel.Content.IData.Interfaces;
using IGenericControlPanel.Core.Data;
using IGenericControlPanel.Core.Data.Repositories;
using IGenericControlPanel.Core.IData;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.GamificationSystem.Data.Repositories;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.HR.Data;
using IGenericControlPanel.HR.Data.Repository;
using IGenericControlPanel.HR.IData;
using IGenericControlPanel.HR.IData.Interfaces;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.Data.Repositories;
using IGenericControlPanel.Security.IData;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Logger.Classes;
using IGenericControlPanel.SharedKernal.Logger.Interface;
using IGenericControlPanel.SharedKernal.Logger.Repository;
using IGenericControlPanel.UserExperiences.Data.Repositories;
using IGenericControlPanel.UserExperiences.IData.Interfaces;
using IGenericControlPanel.WebApplication.Seeder;

using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace IGenericControlPanel.WebApplication.Configurations
{
    public static class ConfigureServices
    {
        public static IServiceCollection BeginCustomConfiguration(this IServiceCollection serviceCollection)
        {
            return serviceCollection;
        }

        public static IServiceCollection AddLocalizationConfiguration(this IServiceCollection serviceCollection)
        {
            RequestCulture requestDefaultCulture = new RequestCulture("en");
            CultureInfo[] supportedCultures = new[]
            {
                new CultureInfo("en"),
                new CultureInfo("ar")
            };
            serviceCollection.ConfigureLocalizationService(supportedCultures, supportedCultures, requestDefaultCulture);
            return serviceCollection;

            //var serviceProvider = serviceCollection.BuildServiceProvider();

            //var userLanguageRepository = serviceProvider.GetService<IUserLanguageRepository>();

            //var supportedCultures = userLanguageRepository.GetSupportedCultures();
            //serviceCollection.ConfigureLocalizationService(supportedCultures, supportedCultures, new RequestCulture(userLanguageRepository.GetDefaultCulture()));

            //return serviceCollection;
        }

        public static IServiceCollection AddLoggerConfiguration(this IServiceCollection serviceCollection)
        {

            var serviceProvider = serviceCollection.BuildServiceProvider();
            var logger = serviceProvider.GetService<ILogger<HITLog>>();
            serviceCollection.AddSingleton(typeof(ILogger), logger);
            return serviceCollection;
        }

        public static IServiceCollection AddDatabaseContextConfiguration(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            return serviceCollection;
        }

        public static IServiceCollection AddIdentityConfiguration(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddIdentity<CPUser, CPRole>(opt =>
            {
                opt.Password.RequiredLength = 7;
                opt.Password.RequireDigit = false;
                opt.Password.RequireUppercase = false;
                opt.Password.RequireLowercase = false;
                opt.Password.RequireNonAlphanumeric = false;
                opt.User.RequireUniqueEmail = true;
            })
            .AddEntityFrameworkStores<CPDbContext>()
            .AddDefaultTokenProviders()
            .AddRoles<CPRole>();
            return serviceCollection;
        }

        public static IServiceCollection AddApplicationDependenciesConfiguration(this IServiceCollection serviceCollection)
        {
            #region IGeneric Control Panel dependencies
            #region Consultation Repos
            serviceCollection.AddScoped<IHealthProviderQuestionsRepository, HealthProviderQuestionsRepository>();
            serviceCollection.AddScoped<IConsultationChoicesRepository, ConsultationChoicesRepository>();
            serviceCollection.AddScoped<IUserConsultationRepository, UserConsultationRepository>();
            #endregion

            #region Services Section
            serviceCollection.AddScoped<ISeederService, SeederService>();
            serviceCollection.AddScoped<IUserService, UserService>();
            serviceCollection.AddScoped<ILogService, LogService>();
            serviceCollection.AddScoped<ITokenService, TokenService>();
            #endregion
            serviceCollection.AddScoped<ISubscriberRepository, SubscriperRepository>();
            serviceCollection.AddScoped<IMultiLevelCategoryRepository, MultiLevelCategoryRepository>();
            serviceCollection.AddScoped<IBlogCommentRepository, BlogCommentRepository>();
            #region Products Repos
            serviceCollection.AddScoped<ILabelRepository<string, string>, Core.Data.Repositories.LabelRepository>();
            serviceCollection.AddScoped<IServiceRepository, ServiceRepository>();
            serviceCollection.AddScoped<ITeamMemberRepository, TeamMemberRepository>();
            serviceCollection.AddScoped<IProductRepository, ProductRepository>();
            serviceCollection.AddScoped<ISpecificationRepository, SpecificationRepository>();
            serviceCollection.AddScoped<IBrandRepository, BrandRpository>();

            #endregion

            #region Settings Repos
            serviceCollection.AddScoped<ILabelRepository<string, string>, Core.Data.Repositories.LabelRepository>();
            serviceCollection.AddScoped<ISettingRepository, SettingRepository>();
            serviceCollection.AddScoped<IUserLanguageRepository, UserLanguageRepository>();
            serviceCollection.AddScoped<IContactInfoRepository, ContactInfoRepository>();
            serviceCollection.AddScoped<IContentRepository, ContentRepository>();

            #endregion

            #region Gamification System
            serviceCollection.AddScoped<IGamificationManagmentsRepository, GamificationManagmentsRepository>();
            serviceCollection.AddScoped<IGamificationMovementsRepository, GamificationMovementsRepository>();
            serviceCollection.AddScoped<ICouponManagementRepository, CouponManagementRepository>();
            serviceCollection.AddScoped<IApplyingCouponsSystemRepository, ApplyingCouponsSystemRepository>();
            #endregion

            #region Basic Repos
            serviceCollection.AddScoped<IServiceRepository, ServiceRepository>();
            serviceCollection.AddScoped<ICarouselItemRepository, CarouselItemRepository>();
            serviceCollection.AddScoped<IFileRepository, FileRepository>();
            serviceCollection.AddScoped<IInformationRepository, InformationRepository>();
            serviceCollection.AddScoped<IBlogRepository, BlogRepository>();
            serviceCollection.AddScoped<IQuestionRepository, QuestionRepository>();
            serviceCollection.AddScoped<IGroupRepository, GroupRepository>();
            serviceCollection.AddScoped<ITeleHealthRpository, TeleHealthRepository>();
            serviceCollection.AddScoped<IOrderRepository, OrderRepository>();
            serviceCollection.AddScoped<ILocationRepository, LocationRepository>();
            serviceCollection.AddScoped<ITipsRepository, TipsRepository>();
            serviceCollection.AddScoped<IPartnersRepository, PartnersRepository>();
            serviceCollection.AddScoped<IFeedbackRepository, FeedbackRepository>();
            serviceCollection.AddScoped<ITimeManagementRepository, TimeManagementRepository>();
            serviceCollection.AddScoped<IPostTypeRepository, PostTypeRepository>();
            serviceCollection.AddScoped<IDoctorLocationRepository, DoctorLocationRepository>();
            serviceCollection.AddScoped<IProjectRepository, ProjectRepository>();
            #endregion

            #region Users Repos
            serviceCollection.AddScoped<ITeamMemberRepository, TeamMemberRepository>();
            serviceCollection.AddScoped<IAccountRepository, AccountRepository>();
            serviceCollection.AddTransient<IPermissionRepository, PermissionRepository>();
            serviceCollection.AddScoped<IUserRepository, UserRepository>();
            #endregion

            #region Courses Repos
            serviceCollection.AddScoped<ICourseRepository, CourseRepository>();
            serviceCollection.AddScoped<ICourseTypeRepository, CourseTypeRepository>();
            #endregion

            #region Offers Repos
            serviceCollection.AddScoped<IJobOfferRepository, JobOfferRepository>();
            serviceCollection.AddScoped<IOfferRepository, OfferRepository>();
            #endregion

            #region Health Profile Repos
            serviceCollection.AddScoped<IDiseasesRepository, DiseasesRepository>();
            serviceCollection.AddScoped<IAllergyRepository, AllergyRepository>();
            serviceCollection.AddScoped<IMedicineRepository, MedicineRepository>();
            serviceCollection.AddScoped<ISurgeryRepository, SurgeryRepository>();
            serviceCollection.AddScoped<IMedicalTestsRepository, MedicalTestsRepository>();
            serviceCollection.AddScoped<INoteRepository, NoteRepository>();
            #endregion

            #region HealthCare
            serviceCollection.AddScoped<IHealthCareSpecialtyRepository, HealthCareSpecialtyRepository>();
            #endregion

            #region User_Experiences Repos
            serviceCollection.AddScoped<IExperienceRepository, ExperienceRepository>();
            serviceCollection.AddScoped<IEducationRepository, EducationRepository>();
            serviceCollection.AddScoped<ILicenseRepository, LicenseRepository>();
            serviceCollection.AddScoped<ILectureRepository, LectureRepository>();
            serviceCollection.AddScoped<ICertificateRepository, CertificateRepository>();
            #endregion

            #endregion

            #region Morel dependencies

            // serviceCollection.AddTransient<ILogService, TraceLogService>();
            serviceCollection.AddScoped<IHttpFileService, DriveFileService>();
            serviceCollection.AddScoped<IViewRenderService, ViewRenderService>();

            #endregion

            return serviceCollection;
        }

        public static MapperConfiguration ConfigureMapping(this IServiceCollection serviceCollection)
        {
            if (serviceCollection is null)
            {
                throw new System.ArgumentNullException(nameof(serviceCollection));
            }

            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProductProfile>();
                cfg.AddProfile<UserLanguageProfile>();
                cfg.AddProfile<CategoryProfile>();
                cfg.AddProfile<FileProfile>();
                cfg.AddProfile<CarouselItemProfile>();
                cfg.AddProfile<ServiceProfile>();
                cfg.AddProfile<LabelProfile>();
                cfg.AddProfile<TeamMemberProfile>();
                cfg.AddProfile<InformationProfile>();
                cfg.AddProfile<BlogProfile>();
                cfg.AddProfile<QuestionProfile>();
            });
        }
    }
}

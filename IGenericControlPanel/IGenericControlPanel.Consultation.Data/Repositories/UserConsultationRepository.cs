﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Consultation.Dto.ConsultationDto;
using IGenericControlPanel.Consultation.IData.Interfaces;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using IGenericControlPanel.Model.Consultations;
using IGenericControlPanel.Security.IData.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Consultation.Data.Repositories;
public class UserConsultationRepository : BasicRepository<CPDbContext, ConsultationEntity>, IUserConsultationRepository
{
    #region Constructor & Properties
    private IUserRepository UserRepository { get; }
    public UserConsultationRepository(
        CPDbContext dbContext,
        IUserRepository userRepository) : base(dbContext)
    {
        UserRepository = userRepository;
    }
    #endregion

    #region Consultation Order
    public async Task<OperationResult<GenericOperationResult>> ConsultationOrderAsync(
        IEnumerable<UserConsultationFormDto> userConsultationDtos, int healthProviderId,string caseDescription)
    {
        var result = new OperationResult<GenericOperationResult>(GenericOperationResult.Failed);

        try
        {
            var addEntity = new ConsultationEntity
            {
                HealthCareProviderId = healthProviderId,
                CaseDescription=caseDescription,
                ConsultaionAnswers = userConsultationDtos
                .Select(entity => new ConsultationAnswers
                {
                    HealthProviderQuestionsId = entity.QuestionId,
                    Comment = entity.Comment,
                    Status = entity.Status,
                    MultiChoices = !string.IsNullOrEmpty(entity.MultiChoicesString)
                    ? string.Join(',', Context.ConsultationChoices
                    .Where(ccEntity => ccEntity.IsValid &&
                    entity.MultiChoicesString.Split(',', StringSplitOptions.None)
                    .ToList().Contains(ccEntity.Id.ToString()))
                    .Select(ccEntity => ccEntity.Name)
                    .ToList())
                    : null
                }).ToList()
            };

            await Context.AddAsync(addEntity);
            await Context.SaveChangesAsync();

            return result.UpdateResultStatus(GenericOperationResult.Success);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError);
        }
    }
    #endregion

    #region Get Consultation Answers
    public async Task<OperationResult<GenericOperationResult, IEnumerable<UserConsultationAnswersDependingonConsultation>>> GetConsultationAnswersAsync
        (int healthProviderId)
    {
        var result = new OperationResult<GenericOperationResult,
            IEnumerable<UserConsultationAnswersDependingonConsultation>>(GenericOperationResult.Failed);

        try
        {
            int currentUserId = UserRepository.CurrentUserIdentityId();

            var data = await Context.Consultations
                .Where(entity => entity.IsValid && (healthProviderId == 0 ?
                entity.CreatedBy == currentUserId :
                (entity.HealthCareProviderId == healthProviderId &&
                entity.CreatedBy == currentUserId)))
                .Select(entity => new UserConsultationAnswersDependingonConsultation
                {
                    ConsultationId = entity.Id,
                    HealthProviderAnswer = entity.HealthProviderAnswer,
                    CaseDescription=entity.CaseDescription,
                    PatientName = Context.Users
                    .Where(user => user.Id == entity.CreatedBy)
                    .Select(user => user.UserName)
                    .SingleOrDefault(),
                    UserConsultationDtos = entity.ConsultaionAnswers
                    .Select(u_c_entity => new UserConsultationDto
                    {
                        Comment = u_c_entity.Comment,
                        Status = u_c_entity.Status,
                        QuestionText = u_c_entity.HealthProviderQuestions.QuestionText,
                        ConsultationQuestionType = u_c_entity.HealthProviderQuestions.ConsultationQuestionType,
                        ConsultationChoices = !string.IsNullOrEmpty(u_c_entity.MultiChoices)
                        ? u_c_entity.MultiChoices.Split(',', StringSplitOptions.None)
                        .ToList()
                        : null
                    }).ToList()
                }).ToListAsync();

            return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError);
        }
    }
    public async Task<OperationResult<GenericOperationResult, IEnumerable<TotalConsultationForPatient>>> GetTotalConsultationForPatientAsync()
    {
        var result = new OperationResult<GenericOperationResult, IEnumerable<TotalConsultationForPatient>>(GenericOperationResult.Failed);

        try
        {
            int patientId = UserRepository.CurrentUserIdentityId();
            var totalConsultationForPatient = new List<TotalConsultationForPatient>();

            var consultations = await Context.Consultations
                .Where(c => c.CreatedBy == patientId && c.IsValid)
                .Include(c => c.HealthCareProvider)
                .ToListAsync();

            var groupingConsultations = consultations
                .GroupBy(c => c.HealthCareProviderId)
                .Select(s => new { hcpId = s.Key, consultations = s.ToList() })
                .ToList();

            foreach (var Dr in groupingConsultations)
            {
                totalConsultationForPatient.Add(new TotalConsultationForPatient
                {
                    DrId = Dr.hcpId,
                    DrName = Dr.consultations[0].HealthCareProvider.FirstName + " " + Dr.consultations[0].HealthCareProvider.LastName,
                    DrPhoto = Dr.consultations[0].HealthCareProvider.ProfilePhotoUrl,
                    ConsultationCount = Dr.consultations.Count
                });
            }
            return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(totalConsultationForPatient);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError);
        }
    }
    public async Task<OperationResult<GenericOperationResult, IEnumerable<UserConsultationAnswersDependingonConsultation>>> GetConsultationAnswersByHcpIdAsync
        (int healthProviderId)
    {
        var result = new OperationResult<GenericOperationResult,
            IEnumerable<UserConsultationAnswersDependingonConsultation>>(GenericOperationResult.Failed);

        try
        {
            int patientId = UserRepository.CurrentUserIdentityId();

            var data = await Context.Consultations
                .Where(entity => entity.IsValid && entity.CreatedBy == patientId && entity.HealthCareProviderId == healthProviderId)
                .Select(entity => new UserConsultationAnswersDependingonConsultation
                {
                    ConsultationId = entity.Id,
                    HealthProviderAnswer = entity.HealthProviderAnswer,
                    CaseDescription = entity.CaseDescription,
                    PatientName = Context.Users
                    .Where(user => user.Id == entity.CreatedBy)
                    .Select(user => user.UserName)
                    .SingleOrDefault(),
                    UserConsultationDtos = entity.ConsultaionAnswers
                    .Select(u_c_entity => new UserConsultationDto
                    {
                        Comment = u_c_entity.Comment,
                        Status = u_c_entity.Status,
                        QuestionText = u_c_entity.HealthProviderQuestions.QuestionText,
                        ConsultationQuestionType = u_c_entity.HealthProviderQuestions.ConsultationQuestionType,
                        ConsultationChoices = !string.IsNullOrEmpty(u_c_entity.MultiChoices)
                        ? u_c_entity.MultiChoices.Split(',', StringSplitOptions.None)
                        .ToList()
                        : null
                    }).ToList()
                })
                .ToListAsync();

            return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError);
        }
    }
    #endregion

    #region Get Health Provider Questions By Health Provider Id
    public async Task<OperationResult<GenericOperationResult,IEnumerable<HealthProviderQuestionsDto>>> GetHealthProviderQuestionsByIdAsync(int heathProviderId)
    {
        var result = new OperationResult<GenericOperationResult, IEnumerable<HealthProviderQuestionsDto>>(GenericOperationResult.Failed);

        try
        {
            var data = await Context.HealthProviderQuestions
                .Include(entity => entity.ConsultationQuestionChoices)
                .ThenInclude(entity => entity.ConsultationChoices)
                .Where(entity => entity.IsValid && entity.CreatedBy == heathProviderId)
                .Select(entity => new HealthProviderQuestionsDto
                {
                    Id = entity.Id,
                    QuestionText = entity.QuestionText,
                    ConsultationQuestionType = entity.ConsultationQuestionType,
                    ConsultationChoices = entity.ConsultationQuestionChoices
                        .Where(entity => entity.IsValid)
                        .Select(entity => new ConsultationChoicesDto
                        {
                            Id = entity.ConsultationChoices.Id,
                            Name = entity.ConsultationChoices.Name
                        }).ToList(),
                }).ToListAsync();

            return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError);
        }
    }
    #endregion
}
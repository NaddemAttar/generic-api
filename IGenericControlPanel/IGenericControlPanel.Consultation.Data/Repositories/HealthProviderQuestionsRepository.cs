﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Consultation.Dto.ConsultationDto;
using IGenericControlPanel.Consultation.IData.Interfaces;
using IGenericControlPanel.Model.Consultations;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums.Consultations;
using IGenericControlPanel.SharedKernal.Extension;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Consultation.Data.Repositories;
public class HealthProviderQuestionsRepository : BasicRepository<CPDbContext, HealthProviderQuestions>, IHealthProviderQuestionsRepository
{
    #region Constructor & Properties
    private IUserRepository _userRepository { get; }
    public HealthProviderQuestionsRepository(CPDbContext dbContext,
        IUserRepository userRepository) : base(dbContext)
    {
        _userRepository = userRepository;
    }
    #endregion

    #region Get Section
    public async Task<OperationResult<GenericOperationResult, IEnumerable<HealthProviderQuestionsDto>>> GetHealthProviderQuestionsAsync(bool enablePagination, int pageNumber, int pageSize)
    {
        var userInfo = await _userRepository.GetCurrentUserInfo();
        var builder = WebApplication.CreateBuilder();
        bool multiProviders = bool.Parse(builder.Configuration.GetSection("MultiProviders").Value.ToLower());
        var result = new OperationResult<GenericOperationResult, IEnumerable<HealthProviderQuestionsDto>>(GenericOperationResult.Failed);
        try
        {
            var entity = Context.HealthProviderQuestions
                .Include(entity => entity.ConsultationQuestionChoices)
                .ThenInclude(entity => entity.ConsultationChoices)
                .Where(entity => entity.IsValid && (
                userInfo.CurrentUserId == 0 || userInfo.IsUserAdmin
                || (!multiProviders || userInfo.IsHealthCareProvider ? userInfo.CurrentUserId == entity.CreatedBy : true)));

            if (enablePagination)
                entity = entity.GetDataWithPagination(pageSize, pageNumber);

            var resultData = await entity.Select(entity => new HealthProviderQuestionsDto
            {
                Id = entity.Id,
                QuestionText = entity.QuestionText,
                ConsultationQuestionType = entity.ConsultationQuestionType,
                ConsultationChoices = entity.ConsultationQuestionChoices
                .Where(entity => entity.IsValid)
                .Select(entity => new ConsultationChoicesDto
                {
                    Id = entity.ConsultationChoices.Id,
                    Name = entity.ConsultationChoices.Name
                }).ToList(),
            }).ToListAsync();

            return result.UpdateResultData(resultData).UpdateResultStatus(GenericOperationResult.Success);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError);
        }
    }
    public async Task<OperationResult<GenericOperationResult, HealthProviderQuestionsDto>> GetOneHealthProviderQuestionAsync(int id)
    {
        var result = new OperationResult<GenericOperationResult, HealthProviderQuestionsDto>(GenericOperationResult.Failed);

        try
        {
            var entity = Context.HealthProviderQuestions
                .Include(entity => entity.ConsultationQuestionChoices).ThenInclude(entity => entity.ConsultationChoices)
                .Where(entity => entity.IsValid && entity.Id == id);

            if (entity is null)
                return result.AddError(ErrorMessages.HealthProviderQuestionNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

            var data = await entity
                .Select(entity => new HealthProviderQuestionsDto
                {
                    Id = entity.Id,
                    QuestionText = entity.QuestionText,
                    ConsultationChoices = entity.ConsultationQuestionChoices
                    .Where(entity => entity.IsValid && entity.HealthProviderQuestionsId == id)
                    .Select(entity => new ConsultationChoicesDto
                    {
                        Id = entity.ConsultationChoices.Id,
                        Name = entity.ConsultationChoices.Name
                    }).ToList(),
                    ConsultationQuestionType = entity.ConsultationQuestionType,
                }).SingleOrDefaultAsync();

            return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError);
        }
    }
    #endregion

    #region Action Section
    public async Task<OperationResult<GenericOperationResult, HealthProviderQuestionsFormDto>> HealthProviderQuestionActionAsync(
        HealthProviderQuestionsFormDto healthProviderQuestionsFormDto)
    {
        return healthProviderQuestionsFormDto.Id == 0 ?
               await AddHealthProviderQuestionAsync(healthProviderQuestionsFormDto) :
               await UpdateHealthProviderQuestionAsync(healthProviderQuestionsFormDto);
    }
    private async Task<OperationResult<GenericOperationResult, HealthProviderQuestionsFormDto>> AddHealthProviderQuestionAsync(
        HealthProviderQuestionsFormDto healthProviderQuestionsFormDto)
    {
        var result = new OperationResult<GenericOperationResult, HealthProviderQuestionsFormDto>(GenericOperationResult.Failed);

        try
        {
            var entity = await CreateHealthProviderQuestionAsync(healthProviderQuestionsFormDto);
            await Context.AddAsync(entity);
            await Context.SaveChangesAsync();

            return result.UpdateResultData(healthProviderQuestionsFormDto).UpdateResultStatus(GenericOperationResult.Success);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError);
        }
    }
    private async Task<OperationResult<GenericOperationResult, HealthProviderQuestionsFormDto>> UpdateHealthProviderQuestionAsync(
        HealthProviderQuestionsFormDto healthProviderQuestionsFormDto)
    {
        var result = new OperationResult<GenericOperationResult, HealthProviderQuestionsFormDto>(GenericOperationResult.Failed);

        try
        {
            var entity = await GetHealthProviderQuestionEntityAsync(healthProviderQuestionsFormDto.Id);
            if (entity == null)
                return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.HealthProviderQuestionNotExist);

            if (entity.ConsultationQuestionType == ConsultationQuestionType.MultiChoices
                && healthProviderQuestionsFormDto.ConsultationQuestionType != ConsultationQuestionType.MultiChoices)
            {
                await RemoveConsultationQuestionChoicesByOnlyIdAsync(healthProviderQuestionsFormDto.Id);
            }

            entity.QuestionText = healthProviderQuestionsFormDto.QuestionText;
            entity.ConsultationQuestionType = healthProviderQuestionsFormDto.ConsultationQuestionType;
            await UpdateInConsultationQuestionChoicesAsync(healthProviderQuestionsFormDto);

            Context.Update(entity);
            await Context.SaveChangesAsync();

            return result.UpdateResultStatus(GenericOperationResult.Success);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError);
        }
    }
    private async Task UpdateInConsultationQuestionChoicesAsync(HealthProviderQuestionsFormDto healthProviderQuestionsFormDto)
    {
        var consultationChoiceIds = await GetConsultationChoiceIdsByHealthProviderQuestionIdAsync(healthProviderQuestionsFormDto.Id);

        var removeConsultationChoiceIds = consultationChoiceIds.Except(healthProviderQuestionsFormDto.ConsultationChoiceIds);

        if (removeConsultationChoiceIds.Count() != 0)
            await RemoveConsultationQuestionChoicesAsync(removeConsultationChoiceIds, healthProviderQuestionsFormDto.Id);

        var newConsultationChoiceIds = healthProviderQuestionsFormDto.ConsultationChoiceIds.Except(consultationChoiceIds);

        if (newConsultationChoiceIds.Count() != 0)
            await AddNewConsultationQuestionChoicesByIdAsync(newConsultationChoiceIds, healthProviderQuestionsFormDto.Id);
    }
    private async Task RemoveConsultationQuestionChoicesAsync(
        IEnumerable<int> ids, int healthProviderQuestionId)
    {
        Context.RemoveRange(Context.ConsultationQuestionChoices
            .Where(entity => entity.HealthProviderQuestionsId
            == healthProviderQuestionId &&
            ids.Contains(entity.ConsultationChoicesId))
            .ToList());

        await Context.SaveChangesAsync();
    }
    private async Task RemoveConsultationQuestionChoicesByOnlyIdAsync(int healthProviderQuestionId)
    {
        Context.RemoveRange(Context.ConsultationQuestionChoices
            .Where(entity => entity.HealthProviderQuestionsId == healthProviderQuestionId)
            .ToList());

        await Context.SaveChangesAsync();
    }
    private async Task AddNewConsultationQuestionChoicesByIdAsync(
       IEnumerable<int> ids, int healthProviderQuestionId)
    {
        var C_Q_Choices_List = new List<ConsultationQuestionChoices>();
        foreach (var id in ids)
        {
            var C_Q_Choices_Entity = new ConsultationQuestionChoices
            {
                HealthProviderQuestionsId = healthProviderQuestionId,
                ConsultationChoicesId = id
            };
            C_Q_Choices_List.Add(C_Q_Choices_Entity);
        }
        await Context.ConsultationQuestionChoices.AddRangeAsync(C_Q_Choices_List);
        await Context.SaveChangesAsync();
    }
    private async Task<IEnumerable<int>> GetConsultationChoiceIdsByHealthProviderQuestionIdAsync(int id)
    {
        return await Task.FromResult(Context.ConsultationQuestionChoices
                .Where(entity => entity.IsValid && entity.HealthProviderQuestionsId == id)
                .Select(entity => entity.ConsultationChoicesId)
                .ToList());
    }
    private async Task<HealthProviderQuestions> CreateHealthProviderQuestionAsync(HealthProviderQuestionsFormDto healthProviderQuestionsFormDto)
    {
        var newEntity = new HealthProviderQuestions
        {
            QuestionText = healthProviderQuestionsFormDto.QuestionText,
            ConsultationQuestionType = healthProviderQuestionsFormDto.ConsultationQuestionType,
            ConsultationQuestionChoices = healthProviderQuestionsFormDto.ConsultationChoiceIds
            .Select(consultationQuestionChoiceId => new ConsultationQuestionChoices
            {
                ConsultationChoicesId = consultationQuestionChoiceId
            }).ToList()
        };
        return await Task.FromResult(newEntity);
    }
    private async Task<HealthProviderQuestions> GetHealthProviderQuestionEntityAsync(int id)
    {
        return await Context.HealthProviderQuestions
            .Where(entity => entity.Id == id)
            .SingleOrDefaultAsync();
    }
    #endregion

    #region Remove Section
    public async Task<OperationResult<GenericOperationResult>> RemoveHealthProviderQuestionsAsync(int id)
    {
        var result = new OperationResult<GenericOperationResult>(GenericOperationResult.Failed);

        try
        {
            var removeEntity = await Context.HealthProviderQuestions
                .Where(entity => entity.IsValid && entity.Id == id)
                .SingleOrDefaultAsync();

            if (removeEntity is null)
                return result.AddError(ErrorMessages.HealthProviderQuestionNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

            Context.HealthProviderQuestions.Remove(removeEntity);
            await Context.SaveChangesAsync();

            return result.UpdateResultStatus(GenericOperationResult.Success);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError);
        }
    }

    #endregion

    #region Heath Provider Answer
    public async Task<OperationResult<GenericOperationResult, HealthProviderAnswerDto>> AnswerAsync(HealthProviderAnswerDto dto)
    {
        var result = new OperationResult<GenericOperationResult,HealthProviderAnswerDto>(GenericOperationResult.Failed);

        try
        {
            var consultationEntity = await Context.Consultations
                .Where(entity => entity.IsValid && entity.Id == dto.ConsultationId)
                .SingleOrDefaultAsync();

            if (consultationEntity is null)
                return result.AddError(ErrorMessages.ConsultationNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

            consultationEntity.HealthProviderAnswer = dto.AnswerText;

            Context.Consultations.Update(consultationEntity);
            await Context.SaveChangesAsync();

            return result.UpdateResultStatus(GenericOperationResult.Success);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError);
        }
    }
    #endregion
}
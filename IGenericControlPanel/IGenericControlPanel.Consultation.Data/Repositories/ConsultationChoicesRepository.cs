﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Consultation.Dto.ConsultationDto;
using IGenericControlPanel.Consultation.IData.Interfaces;
using IGenericControlPanel.Model.Consultations;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Extension;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Consultation.Data.Repositories;
public class ConsultationChoicesRepository : BasicRepository<CPDbContext, ConsultationChoices>,
    IConsultationChoicesRepository
{
    #region Constructor
    public ConsultationChoicesRepository(CPDbContext dbContext) : base(dbContext) { }
    #endregion

    #region Action(Add & Update)
    public async Task<OperationResult<GenericOperationResult, ConsultationChoicesDto>> ActionAsync(ConsultationChoicesDto dto)
    {
        return dto.Id == 0 ?
            await AddNewChoiceAsync(dto) :
            await UpdateChoiceAsync(dto);
    }

    private async Task<OperationResult<GenericOperationResult, ConsultationChoicesDto>> AddNewChoiceAsync(ConsultationChoicesDto dto)
    {
        var result = new OperationResult<GenericOperationResult, ConsultationChoicesDto>(GenericOperationResult.Failed);

        try
        {
            var entity = new ConsultationChoices
            {
                Name = dto.Name
            };

            await Context.AddAsync(entity);

            await Context.SaveChangesAsync();

            dto.Id = entity.Id;
            return result.UpdateResultData(dto).UpdateResultStatus(GenericOperationResult.Success);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError);
        }
    }

    private async Task<OperationResult<GenericOperationResult, ConsultationChoicesDto>> UpdateChoiceAsync(ConsultationChoicesDto dto)
    {
        var result = new OperationResult<GenericOperationResult, ConsultationChoicesDto>(GenericOperationResult.Failed);

        try
        {
            var entity = await Context.ConsultationChoices
                .Where(entity => entity.IsValid && entity.Id == dto.Id)
                .SingleOrDefaultAsync();

            if (entity is null)
                return result.AddError(ErrorMessages.ConsultationChoiceNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

            entity.Name = dto.Name;
            await Context.SaveChangesAsync();

            return result.UpdateResultStatus(GenericOperationResult.Success);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError);
        }
    }
    #endregion

    #region Get
    public async Task<OperationResult<GenericOperationResult, IEnumerable<ConsultationChoicesDto>>> GetChoicesAsync(bool enablePagination, int pageNumber, int pageSize, string query)
    {
        var result = new OperationResult<GenericOperationResult, IEnumerable<ConsultationChoicesDto>>(GenericOperationResult.Failed);

        try
        {
            var entity = Context.ConsultationChoices
                .Where(entity => entity.IsValid && (string.IsNullOrEmpty(query) || query.Contains(entity.Name)));

            if (enablePagination)
                entity = entity.GetDataWithPagination(pageSize, pageNumber);

            var resultData = await entity
                .Select(entity => new ConsultationChoicesDto
                {
                    Id = entity.Id,
                    Name = entity.Name
                }).ToListAsync();

            return result.UpdateResultData(resultData).UpdateResultStatus(GenericOperationResult.Success);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError);
        }
    }
    #endregion

    #region Remove
    public async Task<OperationResult<GenericOperationResult>> RemoveAsync(int id)
    {
        var result = new OperationResult<GenericOperationResult>(GenericOperationResult.Failed);

        try
        {
            var entity = await Context.ConsultationChoices
                .Where(entity => entity.IsValid && entity.Id == id)
                .SingleOrDefaultAsync();

            if (entity is null)
                return result.AddError(ErrorMessages.ConsultationChoiceNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

            Context.ConsultationChoices.Remove(entity);

            await Context.SaveChangesAsync();

            return result.UpdateResultStatus(GenericOperationResult.Success);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError);
        }
    }
    #endregion
}
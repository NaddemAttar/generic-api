﻿using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Security.IData.Interfaces;

using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.SharedUI.Controllers
{
    public abstract class LocalizedBaseController<TController> : BaseController where TController : Controller
    {
        protected IDistributedCache _cache { get; }
        protected IConfiguration Configuration { get; }
        protected IStringLocalizer<TController> FromResx { get; }
        protected IUserRepository _userRepository { get; }
        protected string RequestCultureCode
        {
            get
            {
                var requestCutlureFeature = Request.HttpContext.Features.Get<IRequestCultureFeature>();
                return requestCutlureFeature.RequestCulture.UICulture.Name;
            }
        }

        public LocalizedBaseController(IConfiguration configuration, IUserLanguageRepository userLanguageRepository,
                              ISettingRepository settingRepository,
                              IStringLocalizer<TController> stringLocalizer,
                              IDistributedCache cache = null,
                              IUserRepository userRepository = null)
            : base(userLanguageRepository, settingRepository)
        {
            Configuration = configuration;
            FromResx = stringLocalizer;
            _cache = cache;
            _userRepository = userRepository;
        }

        public LocalizedBaseController() { }

        public LocalizedBaseController(
            IConfiguration configuration,
            IDistributedCache cache = null,
            IUserRepository userRepository = null)
        {
            Configuration = configuration;
            _cache = cache;
            _userRepository = userRepository;
        }

        public LocalizedBaseController(
            IStringLocalizer<TController> stringLocalizer,
            IDistributedCache cache = null)
        {
            FromResx = stringLocalizer;
            _cache = cache;
        }

        public LocalizedBaseController(
            IDistributedCache cache,
            IUserRepository userRepository = null)
        {
            _cache = cache;
            _userRepository = userRepository;
        }

        public LocalizedBaseController(
            IStringLocalizer<TController> stringLocalizer,
            IConfiguration configuration)
        {
            FromResx = stringLocalizer;
            Configuration = configuration;
        }
    }
}

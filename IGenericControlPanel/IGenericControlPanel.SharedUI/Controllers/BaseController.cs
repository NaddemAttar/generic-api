﻿using CraftLab.Core.MVC.Controllers;

using IGenericControlPanel.Configuration.Dto;
using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.SharedKernal.Constants;

using Microsoft.AspNetCore.Localization;

using System.Collections.Generic;

namespace IGenericControlPanel.SharedUI.Controllers
{
    public abstract class BaseController : CraftLabController
    {

        protected IUserLanguageRepository UserLanguageRepository { get; }
        protected ISettingRepository SettingRepository { get; }



        protected IEnumerable<UserLanguageDto> UserLanguages => UserLanguageRepository.GetAll().Result;
        protected IEnumerable<UserLanguageDto> NonDefaultUserLanguages => UserLanguageRepository.GetNotDefaultUserLangauges().Result;


        protected string CurrentUICultureCode => Request.HttpContext.Features.Get<IRequestCultureFeature>().RequestCulture.UICulture.Name;
        protected string CurrentCultureCode => Request.HttpContext.Features.Get<IRequestCultureFeature>().RequestCulture.Culture.Name;

        protected string DefaultCultureCode => SettingRepository.Get(SettingName.DefaultCultureCode) ?? "ar-SY";


        public BaseController(IUserLanguageRepository userLanguageRepository, ISettingRepository settingRepository)
        {
            UserLanguageRepository = userLanguageRepository;
            SettingRepository = settingRepository;
        }

        public BaseController() { }


    }

}

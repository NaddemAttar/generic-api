﻿using CraftLab.Core.MVC.ViewModel;


namespace IGenericControlPanel.SharedUI.ViewModels
{
    public abstract class BaseViewModel : CraftLabViewModel
    {
        public BaseViewModel(string PageTitle):base(PageTitle){}
        public string Errors { get; set; } = string.Empty;
        public bool HasErrors => !string.IsNullOrEmpty(Errors);
        public string WebsiteDirection => IsRTL ? "rtl" : "ltr";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;

using CraftLab.Core.Localization.Extensions;
using CraftLab.Core.Services.HttpFile;
using CraftLab.Core.Services.Renders;

using IGenericControlPanel.AutoMapperConfig.Profiles;
using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Constants;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IGenericControlPanel.SharedUI.Configuration
{
    public static class ConfigServices
    {
        public static IServiceCollection PleaseConfigure(this IServiceCollection serviceCollection, string connectionString, string migrationAssembly)
        {
            return serviceCollection
                .AddDatabaseContextConfiguration(connectionString, migrationAssembly)
                .AddIdentityConfiguration()
                .AddMorelDependenciesConfiguration()
                .AddLocalizationConfiguration();
        }

        public static IServiceCollection AddLocalizationConfiguration(this IServiceCollection serviceCollection)
        {
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var userLanguageRepository = serviceProvider.GetService<IUserLanguageRepository>();
            var supportedCultures = userLanguageRepository.GetSupportedCultures();
            var defaultCultreuRequest = new RequestCulture(userLanguageRepository.GetDefaultCulture());
            serviceCollection.ConfigureLocalizationService(supportedCultures, supportedCultures, defaultCultreuRequest);

            return serviceCollection;
        }

        public static IServiceCollection AddDatabaseContextConfiguration(this IServiceCollection serviceCollection, string connectionString, string migrationAssembly)
        {
            //serviceCollection.AddDbContext<CPDbContext>(options =>
            // options.UseMySql(connectionString, sqlDbOptions => sqlDbOptions.MigrationsAssembly(migrationAssembly)));

            return serviceCollection;
        }
        public static IServiceCollection AddIdentityConfiguration(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddIdentity<CPUser, CPRole>()
                     .AddEntityFrameworkStores<CPDbContext>()
                     .AddDefaultTokenProviders();
            return serviceCollection;
        }

        public static IServiceCollection AddMorelDependenciesConfiguration(this IServiceCollection serviceCollection)
        {

           // serviceCollection.AddTransient<ILogService, TraceLogService>();
            serviceCollection.AddScoped<IHttpFileService, DriveFileService>();
            serviceCollection.AddScoped<IViewRenderService, ViewRenderService>();

            return serviceCollection;
        }

        public static MapperConfiguration ConfigureMapping(this IServiceCollection serviceCollection)
        {
            if (serviceCollection is null)
            {
                throw new ArgumentNullException(nameof(serviceCollection));
            }

            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProductProfile>();
                cfg.AddProfile<UserLanguageProfile>();
                cfg.AddProfile<CategoryProfile>();
                cfg.AddProfile<FileProfile>();
                cfg.AddProfile<CarouselItemProfile>();
                cfg.AddProfile<ServiceProfile>();
                cfg.AddProfile<LabelProfile>();
                cfg.AddProfile<TeamMemberProfile>();
                cfg.AddProfile<InformationProfile>();
            });
        }
    }
}

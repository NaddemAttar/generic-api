﻿using System.Collections.Generic;

namespace IGenericControlPanel.Core.Dto.Allegry
{
    public class AllergyInfoDto : AllergyFormDto
    {
        public AllergyInfoDto()
        {
            PatientsAllegry = new List<PatientAllergyDto>();
            // MedicineAllergies = new List<MedicineAllergyDto>();
        }
        public int CreatedBy { get; set; }
        public string UserName { get; set; }
        public IEnumerable<PatientAllergyDto> PatientsAllegry { get; set; }


    }
}

﻿using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Medicine;
using Newtonsoft.Json;

namespace IGenericControlPanel.Core.Dto.Allegry
{
    public class PatientAllergyDto
    {
        public PatientAllergyDto()
        {
            Medicines = new List<PatientMedicineDto>();
            AllergyCausingMedicins = new List<AllergyCausingMedicinDto>();
            Images = new List<FileDetailsDto>();
        }
        public int Id { get; set; }
        public int AllergyId { get; set; }
        public string AllergyName { get; set; }
        public string AllergyDescription { get; set; }
        public int UserId { get; set; }
        public ICollection<PatientMedicineDto> Medicines { get; set; }
        public ICollection<AllergyCausingMedicinDto> AllergyCausingMedicins { get; set; }
        public ICollection<FileDetailsDto> Images { get; set; }
        [JsonIgnore]
        public ICollection<string> RemovedFilesUrls { get; set; }

    }
}
﻿using System.Collections.Generic;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.SharedKernal.Enums.Core;

using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.Core.Dto.Allegry
{
    public class AllergyFormDto
    {
        public AllergyFormDto()
        {
            Images = new List<FileDetailsDto>();
            RemovedFilesUrls = new List<string>();
            RemovedFilesIds = new List<int>();
            ImagesForm = new List<IFormFile>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public AllergyType AllergyType { get; set; }
        public string Description { get; set; } = string.Empty;
        public List<FileDetailsDto> Images { get; set; }
        public List<IFormFile> ImagesForm { get; set; }
        public IEnumerable<string> RemovedFilesUrls { get; set; }
        public IEnumerable<int> RemovedFilesIds { get; set; }

    }
}

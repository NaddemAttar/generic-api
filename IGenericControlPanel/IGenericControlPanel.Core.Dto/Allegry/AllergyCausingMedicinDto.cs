﻿namespace IGenericControlPanel.Core.Dto.Allegry
{
    public class AllergyCausingMedicinDto
    {
        public int Id { get; set; }
        public int MedicineId { get; set; }
        public string MedicineName { get; set; }
    }
}

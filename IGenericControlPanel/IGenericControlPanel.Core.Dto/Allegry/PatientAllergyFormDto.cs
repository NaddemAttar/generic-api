﻿using System.Collections.Generic;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Medicine;

using Microsoft.AspNetCore.Http;

using Newtonsoft.Json;

namespace IGenericControlPanel.Core.Dto.Allegry
{
    public class PatientAllergyFormDto
    {
        public PatientAllergyFormDto()
        {
            MedicineIds = new List<int>();
            AllergyCausingMedicinIds = new List<int>();
            Images = new List<FileDetailsDto>();
            RemovedFilesIds = new List<int>();
            ImagesForm = new List<IFormFile>();
        }
        public int Id { get; set; }
        public int AllergyId { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public ICollection<int> MedicineIds { get; set; }
        public ICollection<int> AllergyCausingMedicinIds { get; set; }
        public List<IFormFile> ImagesForm { get; set; }
        [JsonIgnore]
        public List<string> ImagesUrls { get; set; }
        public List<FileDetailsDto> Images { get; set; }
        public IEnumerable<int> RemovedFilesIds { get; set; }

    }
}

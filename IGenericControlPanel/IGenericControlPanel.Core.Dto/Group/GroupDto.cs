﻿
using CraftLab.Core.BoundedContext;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.SharedKernal.Enums.Security;

namespace IGenericControlPanel.Core.Dto.Group
{
    public class GroupDto : IFormDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public GroupPrivacyLevel GroupPrivacyLevel { get; set; }
        public ActionOperationType OperationType { get; }
        public string CultureCode { get; }
        public GroupDto Group { get; set; }
        public FileDetailsDto GroupImage { get; set; }
        public FileDetailsDto CoverImage { get; set; }
        public int BlogCount { get; set; }
        public int MemberCount { get; set; }
        public int QuestionCount { get; set; }
    }
}

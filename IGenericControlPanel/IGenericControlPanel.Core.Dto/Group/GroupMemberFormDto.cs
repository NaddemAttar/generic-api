﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Group
{
    public class GroupMemberFormDto:GroupMemberDto
    {
        public int GroupId { get; set; }
        public int UserId { get; set; }
    }
}

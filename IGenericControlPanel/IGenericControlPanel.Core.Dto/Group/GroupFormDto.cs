﻿using System.Collections.Generic;

using IGenericControlPanel.Content.Dto;

namespace IGenericControlPanel.Core.Dto.Group
{
    public class GroupFormDto : GroupDto
    {
        public GroupFormDto()
        {
            RemoveFileUrls = new List<string>();
        }
        public IEnumerable<int> RemoveFileIds { get; set; }
        public FileDetailsDto GroupImage { get; set; }
        public FileDetailsDto CoverImage { get; set; }
        public IEnumerable<string> RemoveFileUrls { get; set; }
        public int? MultiLevelCategoryId { get; set; }


    }
}

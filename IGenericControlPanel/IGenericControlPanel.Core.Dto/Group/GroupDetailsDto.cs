﻿
using System.Collections.Generic;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Category;
using IGenericControlPanel.Core.Dto.Service;

namespace IGenericControlPanel.Core.Dto.Group
{
    public class GroupDetailsDto : GroupDto
    {
        public GroupTranslationDto GroupTranslationDto { get; set; }
        public FileDetailsDto GroupImage { get; set; }
        public FileDetailsDto CoverImage { get; set; }
        public MultiLevelCategoryDto Category { get; set; }
        public ServiceDto Service { get; set; }

    }
}

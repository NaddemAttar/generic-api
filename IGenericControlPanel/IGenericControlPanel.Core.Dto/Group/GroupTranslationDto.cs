﻿
namespace IGenericControlPanel.Core.Dto.Group
{
    public class GroupTranslationDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}

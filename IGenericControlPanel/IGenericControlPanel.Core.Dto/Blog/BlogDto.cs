﻿
using CraftLab.Core.BoundedContext;

using IGenericControlPanel.Core.Dto.Service;
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Core.Dto
{
    public class BlogDto : IFormDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTimeOffset Date { get; set; }
        public string ImageUrl { get; set; }
        public string CultureCode { get; set; }
        public int BlogCommentsCount { get; set; } = 0;
        public ActionOperationType OperationType { get; }
        public Guid ArachnoitCategoryId { get; set; }
        public Guid ArachnoitSubCategoryId { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public BlogType BlogType { get; set; }
        public int? UrlNumbering { get; set; }
        public string MetaDescription { get; set; }
        public string YouTubeLink { get; set; }
        public ServiceDto Service { get; set; }
        public int? PostTypeId { get; set; }
    }
}

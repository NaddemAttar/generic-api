﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.BoundedContext;

namespace IGenericControlPanel.Core.Dto
{
   public class BlogTranslationFormDto: IFormDto
    {
        public int Id { get; set; }
        public bool IsValid { get; set; }
        public int BlogId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string CultureCode { get; set; }
        public ActionOperationType OperationType { get; }
    }
}

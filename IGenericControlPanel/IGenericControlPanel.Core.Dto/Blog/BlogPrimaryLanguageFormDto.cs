﻿
using IGenericControlPanel.Content.Dto;

namespace IGenericControlPanel.Core.Dto
{
    public class BlogPrimaryLanguageFormDto
    {
        public BlogPrimaryLanguageFormDto()
        {
            Images = new List<FileDetailsDto>();
            Attachments = new List<FileDetailsDto>();
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public List<string>? CurrentImgs { get; set; }
        public string CultureCode { get; set; }
        public ICollection<FileDetailsDto> Images { get; set; }
        public List<FileDetailsDto> Attachments { get; set; }
        public string? YouTubeLink { get; set; }
        public int? ServiceId { get; set; }
        public int? PostTypeId { get; set; }
        public int? GroupId { get; set; }
    }
}

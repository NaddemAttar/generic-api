﻿
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Core.Dto
{
    public class BlogFormDto : BlogPrimaryLanguageFormDto
    {
        public BlogFormDto()
        {
            RemovedFilesIds = new List<int>();

            MultiLevelCategoryIds = new List<int>();
        }
        public bool SendEmail { get; set; } = false;
        public bool ShowFirst { get; set; }
        public bool IsHidden { get; set; }
        public List<int> MultiLevelCategoryIds { get; set; }
        public BlogType BlogType { get; set; }
        public List<int> RemovedFilesIds { get; set; }
        public IEnumerable<string> RemovedFilesUrls { get; set; }
    }
}

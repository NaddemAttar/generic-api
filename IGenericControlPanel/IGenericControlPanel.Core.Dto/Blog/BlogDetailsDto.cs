﻿using System.Collections.Generic;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Content.Dto.Tips;
using IGenericControlPanel.Core.Dto.BlogComment;
using IGenericControlPanel.Core.Dto.Category;
using IGenericControlPanel.Core.Dto.Group;

namespace IGenericControlPanel.Core.Dto
{
    public class BlogDetailsDto : BlogDto
    {
        public BlogDetailsDto()
        {
            MultiLevelCategoryIds = new List<int>();
            Attachments = new List<FileDetailsDto>();
            BlogComments = new List<BlogCommentDto>();
        }
        public ICollection<BlogTranslationDto> Translations { get; set; }
        public IEnumerable<int> MultiLevelCategoryIds { get; set; }
        public IEnumerable<MultiLevelCategoryDto> MultiLevelCategories { get; set; }
        public IEnumerable<BlogCommentDto> BlogComments { get; set; }
        public List<FileDetailsDto> Images { get; set; }
        public List<FileDetailsDto> Attachments { get; set; }
        public GroupDto Group { get; set; }
        public TipsDto Tip { get; set; }
        public bool ShowFirst { get; set; }
        public bool IsHidden { get; set; }

    }
}

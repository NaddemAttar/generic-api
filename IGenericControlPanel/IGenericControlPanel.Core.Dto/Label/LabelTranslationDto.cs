﻿using System;
using System.Collections.Generic;
using System.Text;

using CraftLab.Core.BoundedContext;

namespace IGenericControlPanel.Core.Dto
{
    public class LabelTranslationDto : IFormDto
    {
        public string LabelKey { get; set; }
        public string Text { get; set; }
        public string CultureCode { get; set; }

        public ActionOperationType OperationType { get; }
    }
}

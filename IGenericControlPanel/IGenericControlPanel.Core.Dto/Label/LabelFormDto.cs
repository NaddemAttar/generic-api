﻿using System;
using System.Collections.Generic;
using System.Text;

using IGenericControlPanel.Content.Dto;

using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.Core.Dto
{
    public class LabelFormDto
    {
        public LabelDto LabelDto { get; set; }
    }
}

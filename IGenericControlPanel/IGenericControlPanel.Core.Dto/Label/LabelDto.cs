﻿using System;
using System.Collections.Generic;
using System.Text;

using IGenericControlPanel.Content.Dto;

namespace IGenericControlPanel.Core.Dto
{
    public class LabelDto
    {
        public string Key { get; set; }
        public string Text { get; set; }
        public string CultureCode { get; set; }
        public IEnumerable<LabelTranslationDto> Translations { get; set; }
        public string RenderedHtmlText { get; set; }
    }
}

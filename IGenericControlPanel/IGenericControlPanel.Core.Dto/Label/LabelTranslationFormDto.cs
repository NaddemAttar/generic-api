﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.Core.Dto
{
    public class LabelTranslationFormDto
    {

        public IList<LabelTranslationDto> TranslationLabels { get; set; }

        public string CultureCode { get; set; }
    }
}

﻿
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.SharedKernal.Enums.Core;

using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.Core.Dto.JobOffer
{
    public class JobApplicationsDto
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public int JobOfferId { get; set; }
        public int? UserId { get; set; }
        public JobStatus JobStatus { get; set; }
        public FileDetailsDto CvFile { get; set; }
        public string RemovedFilesUrls { get; set; }
        public int RemovedFilesIds { get; set; }
        public IFormFile AttachedCv { get; set; }
    }
}

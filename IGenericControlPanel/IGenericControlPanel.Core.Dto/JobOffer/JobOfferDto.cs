﻿using System.Collections.Generic;

namespace IGenericControlPanel.Core.Dto.JobOffer
{
    public class JobOfferDto : JobOfferFormDto
    {
        public string CityName { get; set; }
        public string CountryName { get; set; }
        public ICollection<JobApplicationsDto> Applicants { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Core.Dto.Pagination;
using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Core.Dto.JobOffer
{
    public class JobOfferOptions
    {
        public string Query { get; set; }
        public int? CityId { get; set; }
        public int? CountryId { get; set; }
        public ContractType? ContractType { get; set; }
        public PlaceOfWork? PlaceOfWork { get; set; }
        public double StartSalary { get; set; }
        public double EndSalary { get; set; }
        public PaginationOptions PaginationOptions { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.SharedKernal.Enums.Core;

using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.Core.Dto.JobOffer
{
    public class JobOfferFormDto
    {
        public JobOfferFormDto()
        {
            Attachments = new List<FileDetailsDto>();
            RemovedFilesIds = new List<int>();
            RemovedFilesUrls = new List<string>();
            AttachedFiles = new List<IFormFile>();
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Responsibilty { get; set; }
        public string Qualification { get; set; }
        public ContractType? ContractType { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int? CityId { get; set; }
        public PlaceOfWork? PlaceOfWork { get; set; }
        public SalaryType? SalaryType { get; set; }
        public double? Salary { get; set; }

        public double? MaxSalary { get; set; }
        public List<FileDetailsDto> Attachments { get; set; }
        public IEnumerable<string> RemovedFilesUrls { get; set; }
        public List<int> RemovedFilesIds { get; set; }
        public IList<IFormFile> AttachedFiles { get; set; }

    }
}

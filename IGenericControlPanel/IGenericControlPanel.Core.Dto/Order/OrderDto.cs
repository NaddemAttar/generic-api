﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Order
{
    public class OrderDto
    {
        public int Id { get; set; }
        public IEnumerable<OrderProductDetailsDto> Products { get; set; }
        public int UserId { get; set; }
        public string PersonName { get; set; }
        public DateTimeOffset OrderDate { get; set; }
        public int ProductsCount { get; set; }
        public double? TotalPrice { get; set; }
    }
}

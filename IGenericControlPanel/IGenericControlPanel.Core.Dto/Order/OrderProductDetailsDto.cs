﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Core.Dto.Product;

namespace IGenericControlPanel.Core.Dto.Order
{
    public class OrderProductDetailsDto:ProductDto
    {
        public double? TotalPrice { get; set; }
        public double? Price { get; set; }
        public string BrandName { get; set; }
        public int BrandId { get; set; }
        public int Quantity { get; set; }
    }
}

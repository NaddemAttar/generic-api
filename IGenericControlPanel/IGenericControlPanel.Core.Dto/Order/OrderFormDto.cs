﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Order
{
    public class OrderFormDto
    {
        public int Id { get; set; }
        public IEnumerable<Tuple<int,int>> ProductsIdsAndQuantities { get; set; }
        //public int PersonId { get; set; }

    }
}

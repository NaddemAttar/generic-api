﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Pagination
{
    public class PaginationOptions
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public bool EnablePagination { get; set; }
    }
}

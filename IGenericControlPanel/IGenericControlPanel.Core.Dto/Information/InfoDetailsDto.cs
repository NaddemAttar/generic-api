﻿using System;
using System.Collections.Generic;
using System.Text;

using IGenericControlPanel.Content.Dto;

namespace IGenericControlPanel.Core.Dto.Information
{
    public class InfoDetailsDto : InfoDto
    {
        public ICollection<InfoTranslationDto> Translations { get; set; }
        public ICollection<FileDetailsDto> Images { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

using CraftLab.Core.BoundedContext;

namespace IGenericControlPanel.Core.Dto.Information
{
    public class InfoTranslationFormDto : IFormDto
    {
        public int Id { get; set; }
        public bool IsValid { get; set; }
        public int InfoId { get; set; }
        public int MyProperty { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string CultureCode { get; set; }
        public ActionOperationType OperationType { get; }
    }
}

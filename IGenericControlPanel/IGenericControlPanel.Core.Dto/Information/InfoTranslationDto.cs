﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.Core.Dto.Information
{
    public class InfoTranslationDto
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string CultureCode { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

using CraftLab.Core.BoundedContext;

namespace IGenericControlPanel.Core.Dto.Information
{
    public class InfoDto : IFormDto
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public DateTime CurrentDate { get; set; }
        public string ImageUrl { get; set; }
        public string CultureCode { get; set; }

        public ActionOperationType OperationType { get; }
    }
}

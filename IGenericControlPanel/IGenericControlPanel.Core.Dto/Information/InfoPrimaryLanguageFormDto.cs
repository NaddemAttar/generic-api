﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

using IGenericControlPanel.Content.Dto;

namespace IGenericControlPanel.Core.Dto
{
    public class InfoPrimaryLanguageFormDto
    {
        public InfoPrimaryLanguageFormDto()
        {
            Images = new List<FileDetailsDto>();
        }
        public int Id { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string CurrentDate { get; set; }
        public List<string> CurrentImgs { get; set; }
        public string CultureCode { get; set; }
        public ICollection<FileDetailsDto> Images { get; set; }
    }
}

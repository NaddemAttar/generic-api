﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Note
{
    public class UserNoteDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int NoteId { get; set; }

    }
}

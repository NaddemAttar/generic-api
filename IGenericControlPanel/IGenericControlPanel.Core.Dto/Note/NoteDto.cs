﻿namespace IGenericControlPanel.Core.Dto.Note
{
    public class NoteDto
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public int UserId { get; set; }
    }
}

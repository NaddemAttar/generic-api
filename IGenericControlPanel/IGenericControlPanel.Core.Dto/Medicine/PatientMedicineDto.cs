﻿namespace IGenericControlPanel.Core.Dto.Medicine
{
    public class PatientMedicineDto
    {
        public int Id { get; set; }
        public int MedicineId { get; set; }
        public string MedicineName { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }
    }
}

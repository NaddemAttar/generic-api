﻿using System.Collections.Generic;

namespace IGenericControlPanel.Core.Dto.Medicine
{
    public class MedicineInfoDto : MedicineFormDto
    {
        public MedicineInfoDto()
        {

            PatientMedicines = new List<PatientMedicineDto>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int CreatedBy { get; set; }
        public string UserName { get; set; }

        public IEnumerable<PatientMedicineDto> PatientMedicines { get; set; }
    }
}

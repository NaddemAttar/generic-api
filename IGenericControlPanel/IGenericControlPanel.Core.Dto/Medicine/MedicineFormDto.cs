﻿using System.Collections.Generic;

using IGenericControlPanel.Content.Dto;

using Microsoft.AspNetCore.Http;

using Newtonsoft.Json;

namespace IGenericControlPanel.Core.Dto.Medicine
{
    public class MedicineFormDto
    {
        public MedicineFormDto()
        {
            Images = new List<FileDetailsDto>();
            ImagesForm = new List<IFormFile>();
            RemovedFilesIds = new List<int>();
            RemovedFilesUrls = new List<string>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int PersonId { get; set; }
        public ICollection<FileDetailsDto> Images { get; set; }
        [JsonIgnore]
        public List<IFormFile> ImagesForm { get; set; }
        [JsonIgnore]
        public IEnumerable<string> RemovedFilesUrls { get; set; }
        [JsonIgnore]
        public IEnumerable<int> RemovedFilesIds { get; set; }
    }
}

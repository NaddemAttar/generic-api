﻿using IGenericControlPanel.Content.Dto;

namespace IGenericControlPanel.Core.Dto.Question
{
    public class QuestionFormDto:QuestionDetailsDto
    {
        public QuestionFormDto()
        {
            Attachments = new List<FileDetailsDto>();
            RemovedFilesIds = new List<int>();
            RemovedFilesUrls = new List<string>();
        }
        public List<int> RemovedFilesIds { get; set; }
        public bool SendEmail { get; set; } = false;
        public IEnumerable<string> RemovedFilesUrls { get; set; }
        public int? GroupId { get; set; }
    }
}

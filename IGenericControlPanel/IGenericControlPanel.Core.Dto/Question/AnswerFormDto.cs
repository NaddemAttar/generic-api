﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Content.Dto;

namespace IGenericControlPanel.Core.Dto.Question
{
    public class AnswerFormDto:AnswerDto
    {
        public AnswerFormDto()
        {
            Files = new List<FileDetailsDto>();
            RemovedFilesIds = new List<int>();
            RemovedFilesData = new List<FileDetailsDto>();
        }
        public List<int> RemovedFilesIds { get; set; }
        public IEnumerable<FileDetailsDto> RemovedFilesData { get; set; }
        public List<FileDetailsDto> Files { get; set; }

    }
}

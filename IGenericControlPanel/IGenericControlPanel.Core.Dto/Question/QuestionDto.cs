﻿
using CraftLab.Core.BoundedContext;

namespace IGenericControlPanel.Core.Dto.Question
{
    public class QuestionDto : IFormDto
    {
        public int Id { get; set; }
        public int? PostTypeId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        //public DateTime BlogDate { get; set; }
        //public string ImageUrl { get; set; }
        public string? CultureCode { get; set; }
        public ActionOperationType OperationType { get; }
        public Guid? ArachnoitCategoryId { get; set; }
        public Guid? ArachnoitSubCategoryId { get; set; }

        public IEnumerable<int> MultiLevelCategoryIds { get; set; }

        //public BlogType BlogType { get; set; }
        public int? UrlNumbering { get; set; }
        public string? MetaDescription { get; set; }
        public DateTimeOffset? Date { get; set; }
        public int? ServiceId { get; set; }
        public bool? FAQ { get; set; }

    }
}

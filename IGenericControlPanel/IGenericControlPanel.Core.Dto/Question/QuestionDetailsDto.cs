﻿
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Group;
using IGenericControlPanel.Core.Dto.Service;

namespace IGenericControlPanel.Core.Dto.Question
{
    public class QuestionDetailsDto : QuestionDto
    {
        public QuestionDetailsDto()
        {
            Attachments = new List<FileDetailsDto>();
            Answers = new List<AnswerFormDto>();
        }
        public List<FileDetailsDto> Attachments { get; set; }

        public IEnumerable<AnswerFormDto> Answers { get; set; }
        public int AnswersCount { get; set; }
        public ServiceDto Service { get; set; }
        public GroupDto Group { get; set; }
        public bool CanDelete { get; set; }
        public bool ShowFirst { get; set; }
        public bool IsHidden { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.HealthCareSpecialty
{
    public class HealthCareSpecialtyServiceDto
    {
        public HealthCareSpecialtyServiceDto()
        {
            HealthCareProviders = new List<HealthCareProviderSpecialtyDto>();
        }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string ServiceDescription { get; set; }
        public IEnumerable<HealthCareProviderSpecialtyDto>  HealthCareProviders { get; set; }
    }
}

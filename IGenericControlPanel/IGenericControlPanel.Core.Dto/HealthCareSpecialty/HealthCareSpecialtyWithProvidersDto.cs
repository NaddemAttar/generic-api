﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.HealthCareSpecialty
{
    public class HealthCareSpecialtyWithProvidersDto
    {
        public HealthCareSpecialtyWithProvidersDto()
        {
            HealthCareProviderSpecialties = new List<HealthCareProviderSpecialtyDto>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<HealthCareProviderSpecialtyDto>  HealthCareProviderSpecialties { get; set; }
    }
}

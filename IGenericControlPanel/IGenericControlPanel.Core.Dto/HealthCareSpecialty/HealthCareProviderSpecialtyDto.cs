﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.HealthCareSpecialty
{
    public class HealthCareProviderSpecialtyDto
    {
        public HealthCareProviderSpecialtyDto()
        {
            HealthCareSpecialties = new List<HealthCareSpecialtyDto>();
        }
        public int HealthCareProviderId { get; set; }
        public string HealthCareProviderName { get; set; }
        public ICollection<HealthCareSpecialtyDto>  HealthCareSpecialties { get; set; }

    }
}

﻿
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Service;

using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.Core.Dto.HealthCareSpecialty
{
    public class HealthCareSpecialtyDto
    {
        public HealthCareSpecialtyDto()
        {
            Images = new List<FileDetailsDto>();
            ImagesForm = new List<IFormFile>();
            RemovedFilesIds = new List<int>();
            RemovedFilesUrls = new List<string>();
            ServicesIds = new List<int>();
        }
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public List<int> ServicesIds { get; set; }
        public ICollection<FileDetailsDto> Images { get; set; }
        public List<IFormFile> ImagesForm { get; set; }
        public IEnumerable<string> RemovedFilesUrls { get; set; }
        public IEnumerable<int> RemovedFilesIds { get; set; }
    }
}

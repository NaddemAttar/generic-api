﻿using IGenericControlPanel.Core.Dto.Service;

namespace IGenericControlPanel.Core.Dto.HealthCareSpecialty
{
    public class HealthCareSpecialtyDetailsDto:HealthCareSpecialtyDto
    {
        public IEnumerable<HealthCareSpecialtyDetailsDto> Children { get; set; }
        public IEnumerable<ServiceDetailsDto> Services { get; set; }
    }
}
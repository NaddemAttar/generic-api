﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Content.Dto;

namespace IGenericControlPanel.Core.Dto.Project
{
    public class ProjectDetailsDto : ProjectDto
    {
        public ProjectDetailsDto()
        {
            Images = new List<FileDetailsDto>();
            Components = new List<ComponentDto>();
            Editions = new List<EditionDto>();
        }
        public List<string> Keys { get; set; }
        public IEnumerable<FileDetailsDto> Images { get; set; }
        public ICollection<ComponentDto> Components { get; set; }
        public ICollection<EditionDto> Editions { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Content.Dto;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IGenericControlPanel.Core.Dto.Project
{
    public class ProjectFormDto
    {
        public ProjectFormDto()
        {
            Images = new List<FileDetailsDto>();
            Components = new List<ComponentDto>();
            Editions = new List<EditionDto>();
            Keys = new List<string>();
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string YoutubeLink { get; set; }
        public IList<IFormFile> ImagesForm { get; set; }
        public List<FileDetailsDto> Images { get; set; }
        public IEnumerable<string> Keys { get; set; }
        public IEnumerable<ComponentDto> Components { get; set; }
        public IEnumerable<EditionDto> Editions { get; set; }
        public IEnumerable<int> RemoveEditionIds { get; set; }
        public IEnumerable<int> RemoveComponentIds { get; set; }
        public IEnumerable<int> RemovedFileIds { get; set; }
        public IEnumerable<string> FileUrlsMustRemoveItFromServer { get; set; }
    }
}

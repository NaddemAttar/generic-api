﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Offer
{
    public class BrandMaxMinPrice
    {
        public double MaxProductPrice { get; set; }
        public double MinProductPrice { get; set; }
    }
}

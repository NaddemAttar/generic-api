﻿using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Core.Dto.Offer
{
    public class AllOfferDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public double? Value { get; set; }
        public double? Percentage { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public OfferFor OfferFor { get; set; }
        public OfferType OfferType { get; set; }
        public bool IsExpired { get; set; }
        public bool IsStarted { get; set; }
    }
}
﻿using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Core.Dto.Offer
{
    public class OfferDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public double? Value { get; set; }
        public double? Percentage { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<int>? ItemIds { get; set; }// Can Be Empty in Case Choosing from each Product some of it's details (he did't choose the entire product).
        public List<int>? DetailsIds { get; set; }// Can Be Empty in Case Courses and Services.
        public OfferFor? OfferFor { get; set; }
        public OfferType? OfferType { get; set; }
    }
}
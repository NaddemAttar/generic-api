﻿namespace IGenericControlPanel.Core.Dto.Offer
{
    public class OfferItemDetailsDto
    {
        public OfferItemDetailsDto()
        {
            Details = new List<ItemDetailsDto>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public double? Price { get; set; }
        public double? NewPrice { get; set; }
        public string Description { get; set; }
        public bool IsOffer { get; set; }
        public List<ItemDetailsDto> Details { get; set; } // can be empty in case courses, srvices, categories and specific case of product(Brand).

    }
}
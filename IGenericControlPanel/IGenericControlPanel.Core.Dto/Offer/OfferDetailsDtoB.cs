﻿namespace IGenericControlPanel.Core.Dto.Offer
{
    public class OfferDetailsDto : AllOfferDto
    {
        public OfferDetailsDto()
        {
            Item = new List<OfferItemDetailsDto>();
        }
        public List<OfferItemDetailsDto> Item { get; set; }
    }
}
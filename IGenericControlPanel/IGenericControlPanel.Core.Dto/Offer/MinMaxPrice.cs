﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Offer
{
    public class MinMaxPrice
    {
      
        public double MaxProductPrice { get; set; }
        public double MinProductPrice { get; set; }
        public double MaxServicePrice { get; set; }
        public double MinServicePrice { get; set; }
        public double MaxCoursePrice { get; set; }
        public double MinCoursePrice { get; set; }
        public BrandMaxMinPrice  BrandMaxMinPrice { get; set; }
        public CategoryMaxMinPrice CategoryMaxMinPrice { get; set; }
    }
}

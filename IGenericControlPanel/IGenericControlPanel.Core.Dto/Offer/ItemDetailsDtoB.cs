﻿namespace IGenericControlPanel.Core.Dto.Offer
{
    public class ItemDetailsDto
    {
        public int Id { get; set; }        
        public string SizeName { get; set; }
        public string ColorHex { get; set; }
        public double Price { get; set; }
        public double? NewPrice { get; set; }
        public int Qunatity { get; set; }
    }
}
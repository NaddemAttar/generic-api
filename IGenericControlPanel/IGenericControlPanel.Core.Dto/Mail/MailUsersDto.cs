﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Mail
{
    public class MailUsersDto
    {
        public MailUsersDto()
        {
            UsersIds = new List<int>();
        }
        public string Subject { get; set; }
        public string Body { get; set; }
        public ICollection<int> UsersIds { get; set; }
    }
}

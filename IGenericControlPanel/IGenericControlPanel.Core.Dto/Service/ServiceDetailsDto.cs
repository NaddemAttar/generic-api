﻿using IGenericControlPanel.Communications.Dto.Feedback;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.HealthCareProvider;
using IGenericControlPanel.Core.Dto.Question;

namespace IGenericControlPanel.Core.Dto.Service
{
    public class ServiceDetailsDto : ServiceDto
    {
        public ServiceDetailsDto()
        {
            Images = new List<FileDetailsDto>();
            HealthCareProvidersServices = new();
            EnterpriseServices = new();
        }
        public ICollection<FileDetailsDto> Images { get; set; }
        public List<QuestionDetailsDto> Questions { get; set; }
        public List<BlogDetailsDto> Blogs { get; set; }
        public List<FeedbackDto> Feedback { get; set; }
        public ICollection<HealthCareSpecialtyDto> ServiceSpecialties { get; set; }
        public List<HealthCareProviderServicesDto> HealthCareProvidersServices { get; set; }
        public List<EnterpriseDetailsDto> EnterpriseServices { get; set; }
        //public List<SessionTypeDto> SessionsType { get; set; }
        public bool WithData { get; set; }
        public int QuestionCount { get; set; }
        public bool ShowFirst { get; set; }
        public bool IsHidden { get; set; }
    }
}
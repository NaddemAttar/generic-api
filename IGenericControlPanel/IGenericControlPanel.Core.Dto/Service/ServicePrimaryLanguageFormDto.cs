﻿using System.Collections.Generic;

using CraftLab.Core.BoundedContext;

using IGenericControlPanel.Content.Dto;

namespace IGenericControlPanel.Core.Dto
{
    public class ServicePrimaryLanguageFormDto : IFormDto
    {
        public ServicePrimaryLanguageFormDto()
        {
            Images = new List<FileDetailsDto>();
            RemovedFilesUrls = new List<string>();
            RemovedFilesIdss = new List<int>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double? Price { get; set; }
        public string CultureCode { get; set; }

        public int Order { get; set; }

        public ICollection<FileDetailsDto> Images { get; set; }
        public IEnumerable<string> RemovedFilesUrls { get; set; }
        public IEnumerable<int> RemovedFilesIdss { get; set; }
        public ActionOperationType OperationType { get; }
    }
}

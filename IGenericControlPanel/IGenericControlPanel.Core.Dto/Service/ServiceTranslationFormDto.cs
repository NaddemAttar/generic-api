﻿using CraftLab.Core.BoundedContext;

using IGenericControlPanel.SharedKernal.Interfaces;

using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.Core.Dto
{
    public class ServiceTranslationFormDto : IFormDto
    {
        public int Id { get; set; }
        public bool IsValid { get; set; }
        public int ServiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public string CultureCode { get; set; }
        public ActionOperationType OperationType { get; }
    }
}

﻿using CraftLab.Core.BoundedContext;

namespace IGenericControlPanel.Core.Dto.Service
{
    public class ServiceDto : IFormDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? Order { get; set; }
        public string ImageUrl { get; set; }
        public string CultureCode { get; set; }
        public ActionOperationType OperationType { get; }
    }
}
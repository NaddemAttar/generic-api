﻿using CraftLab.Core.BoundedContext;

using IGenericControlPanel.Core.Dto.Brand;

namespace IGenericControlPanel.Core.Dto.Product
{
    public class ProductDto : IFormDto
    {
        public ProductDto()
        {
            ProductReviewsDto = new();
            ProductSizeColors = new();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        //public int BrandId { get; set; }
        public BrandDto Brand { get; set; }
        public double AvarageReview { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        //public string CategoryDescription { get; set; }
        public List<ProductReviewDto> ProductReviewsDto { get; set; }
        public List<ProductSizeColorDto> ProductSizeColors { get; set; }
        public string CultureCode { get; set; }
        public ActionOperationType OperationType { get; }
        public bool ShowInStore { get; set; }
    }
}
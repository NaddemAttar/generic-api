﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Product
{
    public class ProductReviewDto
    {
        public int Id { get; set; }
        public double Rating { get; set; }
        public int ProductId { get; set; }
        public string ReviewerName { get; set; }
        public string Description { get; set; }
        public DateTimeOffset CreationDate { get; set; }

    }
}

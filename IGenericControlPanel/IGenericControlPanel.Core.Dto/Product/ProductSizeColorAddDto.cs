﻿namespace IGenericControlPanel.Core.Dto.Product
{
    public class ProductSizeColorAddDto
    {

        public int ProductId { get; set; }
        public string ColorHex { get; set; }
        public string SizeText { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
    }
}

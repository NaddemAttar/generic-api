﻿using Microsoft.AspNetCore.Mvc;

namespace IGenericControlPanel.Core.Dto.Product
{
    //[ModelBinder(BinderType = typeof(MetadataValueModelBinder))]
    public class ProductSizeColorFormDto
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string? ColorHex { get; set; }
        public string? SizeText { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        
    }
}
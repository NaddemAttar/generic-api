﻿using IGenericControlPanel.Content.Dto;

namespace IGenericControlPanel.Core.Dto.Product
{
    public class ProductDetailsDto : ProductDto
    {
        public ProductDetailsDto()
        {
            Attachments = new();
            ProductSpecifications = new();
        }
        public List<FileDetailsDto> Attachments { get; set; }
        public List<ProductSpecificationDto> ProductSpecifications { get; set; }
        //public IEnumerable<OfferDto> Offers { get; set; }
    }

}

﻿using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Product;
using IGenericControlPanel.SharedKernal.Enums.Core;
using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.Core.Dto
{
    public class ProductFormDto
    {
        public ProductFormDto()
        {
            RemovedFilesIds = new List<int>();
            RemovedFilesUrls = new List<string>();
            Attachments = new List<FileDetailsDto>();
            ProductSizeColors = new List<ProductSizeColorFormDto>();
            ProductSpecifications = new();

        }

        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public bool ShowInStore { get; set; }
        public string Description { get; set; }

        public CurrencyType CurrencyType { get; set; }

        [Required]
        public int CategoryId { get; set; }
        [Required]
        public int BrandId { get; set; }

        [Required]
        public string CultureCode { get; set; }
        public List<int> RemovedFilesIds { get; set; }
        public IEnumerable<string> RemovedFilesUrls { get; set; }
        public List<FileDetailsDto> Attachments { get; set; }
        #region Product specification
        public List<ProductSpecificationDto> ProductSpecifications { get; set; }
        public ICollection<ProductSizeColorFormDto> ProductSizeColors { get; set; }
        #endregion 
    }
}

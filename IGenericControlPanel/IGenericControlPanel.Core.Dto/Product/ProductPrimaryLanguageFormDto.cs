﻿using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.Core.Dto
{
    public class ProductPrimaryLanguageFormDto
    {
        public ProductPrimaryLanguageFormDto()
        {
            Images = new List<FileDetailsDto>();
            Videos = new List<FileDetailsDto>();
            CurrentVideos = new List<string>();
        }

        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public double? Price { get; set; }

        public string Currency { get; set; }

        public int? DepartmentId { get; set; }
        public IEnumerable<string> CurrentVideos { get; set; }

        [Required]
        public int CategoryId { get; set; }

        [Required]
        public string CultureCode { get; set; }


        public IList<FileDetailsDto> Images { get; set; }
        public IList<FileDetailsDto> Videos { get; set; }
    }
}

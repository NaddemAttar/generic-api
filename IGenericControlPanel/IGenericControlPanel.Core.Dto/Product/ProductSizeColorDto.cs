﻿namespace IGenericControlPanel.Core.Dto.Product
{
    public class ProductSizeColorDto
    {

        public int Id { get; set; }
        public int ProductId { get; set; }
        public string ColorHex { get; set; }
        public string SizeText { get; set; }
        public double Price { get; set; }
        public double? NewPrice { get; set; } = null;
        public bool IsOffer => NewPrice != null;
        public DateTime? EndOfferDate { get; set; }
        public int Quantity { get; set; }
    }
}

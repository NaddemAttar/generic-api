﻿using System.Collections.Generic;

namespace IGenericControlPanel.Core.Dto.Product
{
    public class ProductExcelDto
    {
        public string Brand { get; set; }
        public string Category { get; set; }
        public string Product_Description { get; set; }
        public string Product_Name { get; set; }
        public List<SpecificationDto> Specifications { get; set; }
        public List<ProductSizeColorDto> ProductSizeColors { get; set; }
    }

}

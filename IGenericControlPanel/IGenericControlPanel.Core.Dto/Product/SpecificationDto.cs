﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Product
{
    public class SpecificationDto
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}

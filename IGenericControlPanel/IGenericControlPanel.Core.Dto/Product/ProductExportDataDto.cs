﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Product
{
    public class ProductExportDataDto
    {
        public string Product_Name { get; set; }
        public string Product_Description { get; set; }
        public string Brand { get; set; }
        public string Colors { get; set; }
        public string Sizes { get; set; }
        public string Specifications { get; set; }
        public double Price { get; set; }
        public int Inventory { get; set; }
        public string Category { get; set; }
    }
}

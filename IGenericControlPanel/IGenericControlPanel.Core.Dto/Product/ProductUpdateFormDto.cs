﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using IGenericControlPanel.Content.Dto;

namespace IGenericControlPanel.Core.Dto.Product
{
    public class ProductUpdateFormDto
    {
        public ProductUpdateFormDto()
        {
            Attachments = new List<FileDetailsDto>();
            RemovedFilesIds = new List<int>();
            RemovedFilesUrls = new List<string>();
        }

        public int Id { get; set; }
        [Required]
        public string Name { get; set; }    
        public bool ShowInStore { get; set; }
        public string Description { get; set; }
        public string Currency { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public int BrandId { get; set; }

        [Required]
        public string CultureCode { get; set; }
        public List<int> RemovedFilesIds { get; set; }
        public IEnumerable<string> RemovedFilesUrls { get; set; }
        public List<FileDetailsDto> Attachments { get; set; }
        #region Product specification
        public List<ProductSpecificationDto> ProductSpecifications { get; set; }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.Core.Dto
{
    public class ProductTranslationDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CultureCode { get; set; }
    }
}

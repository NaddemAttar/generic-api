﻿using Microsoft.AspNetCore.Mvc;

namespace IGenericControlPanel.Core.Dto.Product
{
    [ModelBinder(BinderType = typeof(MetadataValueModelBinder))]
    public class ProductSpecificationDto
    {
        public int Id { get; set; }
        public int SpecificationId { get; set; }
        public string SpecificationName { get; set; }
        public string Value { get; set; }
        public bool IsValid { get; set; }
    }
}

﻿namespace IGenericControlPanel.Core.Dto.HealthCareProvider
{
    public class AddServicesForHcpDto
    {
        public int ServiceId { get; set; }
        public List<SessionInfoDto> SessionTypes { get; set; }
    }
}
﻿namespace IGenericControlPanel.Core.Dto.HealthCareProvider
{
    public class SessionInfoDto
    {
        public int SessionTypeId { get; set; }
        public double Price { get; set; }
        public int Duration { get; set; }
    }
}
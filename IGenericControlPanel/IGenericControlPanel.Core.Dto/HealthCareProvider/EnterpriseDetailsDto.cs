﻿using IGenericControlPanel.Communications.Dto.TeleHealth;

namespace IGenericControlPanel.Core.Dto.HealthCareProvider
{
    public class EnterpriseDetailsDto
    {
        public EnterpriseDetailsDto()
        {
            SessionTypes = new();
            HealthCareProviders = new();
        }
        public int EnterpriseId { get; set; }
        public string EnterpriseName { get; set; }
        public List<SessionTypeDto> SessionTypes { get; set; }
        public List<HealthCareProviderServicesDto> HealthCareProviders { get; set; } // here we don't look at SessionTypes inside this property cuz the sessions here r for Enterprise not for CPUser.
    }
}
﻿using IGenericControlPanel.Communications.Dto.TeleHealth;

namespace IGenericControlPanel.Core.Dto.HealthCareProvider
{
    public class HealthCareProviderServicesDto
    {
        public HealthCareProviderServicesDto()
        {
            HealthCareProviderSpecialties = new List<HealthCareSpecialtyDto>();
            HealthCareProviderSessions = new List<SessionTypeDto>();
        }
        public int HealthCareProviderId { get; set; }
        public string HealthCareProviderName { get; set; }
        public string ProfilePhotoUrl { get; set; }
        public ICollection<HealthCareSpecialtyDto> HealthCareProviderSpecialties { get; set; }
        public ICollection<SessionTypeDto> HealthCareProviderSessions { get; set; }
    }
}
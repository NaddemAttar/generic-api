﻿namespace IGenericControlPanel.Core.Dto.HealthCareProvider
{
    public class HealthCareSpecialtyDto
    {
        public int SpecialtyId { get; set; }
        public string SpecialtyName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Course
{
    public class UserCourseDto
    {
        public UserCourseDto()
        {
            Courses = new List<PersonCourseDetails>();
        }
        public int PersonId { get; set; }
        public string PersonName { get; set; }
        public DateTimeOffset EnrollmentDate { get; set; }
        public IEnumerable<PersonCourseDetails> Courses { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Course
{
    public class PersonCourseDetails
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTimeOffset EnrollmentDate { get; set; }
        public IEnumerable<InstructorDto> Instructors { get; set; }

    }
}

﻿namespace IGenericControlPanel.Core.Dto.Course
{
    public class CourseDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Place { get; set; }
        public double Price { get; set; }
        public double? NewPrice { get; set; } = null;
        public bool IsOffer => NewPrice != null;
        public DateTime? EndOfferDate { get; set; }
        public string Link { get; set; }
        public string ImageUrl { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string StartDateFormat { get; set; }
        public string EndDateFormat { get; set; }
        public string Description { get; set; }
        public string PreRequirements { get; set; }
        public string YoutubeLink { get; set; }
        public string Goals { get; set; }
        public string Modules { get; set; }
        public string LocationName { get; set; }
        public bool IsHidden { get; set; }
        public int StudentsCount { get; set; }
        public bool IsEnrolled { get; set; } = false;
        public IEnumerable<InstructorDto> Instructors { get; set; }
        public IEnumerable<CourseTypeDto> CourseTypes { get; set; }
        public List<UserCourseDto> Students { get; set; }
        public DateTime EnrollmentDate { get; set; }
    }
}

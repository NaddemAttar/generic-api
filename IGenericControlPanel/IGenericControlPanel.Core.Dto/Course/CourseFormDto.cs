﻿using IGenericControlPanel.Content.Dto;
using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.Core.Dto.Course
{
    public class CourseFormDto : CourseDto
    {
        public CourseFormDto()
        {
            AttachedFiles = new List<IFormFile>();
            CourseTypeIds = new List<int>();
            Attachments = new List<FileDetailsDto>();
            InstructorIds = new List<int>();
        }
        public List<FileDetailsDto> Attachments { get; set; }
        public IList<IFormFile> AttachedFiles { get; set; }
        public IEnumerable<int> RemovedFileIds { get; set; }
        public IEnumerable<string> RemoveFileUrls { get; set; }
        public List<int> CourseTypeIds { get; set; }
        public List<int> InstructorIds { get; set; }
        public int? LocationId { get; set; }
    }
}

﻿using System.Collections.Generic;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.Security.Dto.User;

namespace IGenericControlPanel.Core.Dto.Course
{
    public class CourseDetailsDto : CourseDto
    {
        public CourseDetailsDto()
        {
            Attachments = new List<FileDetailsDto>();
            Location = new LocationDto();
            NormalUsers = new List<NormalUserDto>();
        }
        public List<FileDetailsDto> Attachments { get; set; }
        public LocationDto Location { get; set; }
        public IEnumerable<NormalUserDto> NormalUsers { get; set; }

    }
}

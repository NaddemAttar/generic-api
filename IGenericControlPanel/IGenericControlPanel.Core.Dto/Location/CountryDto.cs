﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Location
{
    public class CountryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

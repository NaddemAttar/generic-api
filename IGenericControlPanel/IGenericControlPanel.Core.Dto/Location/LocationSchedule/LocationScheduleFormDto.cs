﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Location.LocationSchedule
{
    public class LocationScheduleFormDto
    {
        public int Id { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }
        public string ContactNumber { get; set; }
        
    }
}

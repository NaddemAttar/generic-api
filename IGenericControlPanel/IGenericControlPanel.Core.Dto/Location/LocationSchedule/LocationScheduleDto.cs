﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Core.Dto.Course;

namespace IGenericControlPanel.Core.Dto.Location.LocationSchedule
{
    public class LocationScheduleDto
    {
        public int Id { get; set; }
        public GeoLocationDto Location { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public bool IsExist { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }
    }
}

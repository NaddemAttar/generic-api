﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Location
{
    public class DoctorLocationDto
    {
        public int Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string AddressInfo { get; set; } = string.Empty;
    }
}

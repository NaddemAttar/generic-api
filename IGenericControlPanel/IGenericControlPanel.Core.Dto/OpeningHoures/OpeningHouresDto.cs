﻿namespace IGenericControlPanel.Core.Dto.OpeningHoures
{
    public class OpeningHouresDto
    {
        public int Id { get; set; }
        public TimeSpan From { get; set; }
        public TimeSpan To { get; set; }
        public DayOfWeek Day { get; set; }
        public bool IsWorkingDay { get; set; }
    }
}
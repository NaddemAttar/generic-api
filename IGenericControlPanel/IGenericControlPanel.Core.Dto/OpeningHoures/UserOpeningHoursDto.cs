﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.OpeningHoures
{
    public class UserOpeningHoursDto
    {
        public int? UserId { get; set; }
        public string UserName { get; set; }
        public IEnumerable<OpeningHouresDto> OpeningHours { get; set; }
    }
}

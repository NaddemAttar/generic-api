﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.Core.Dto.Brand
{
    public class BrandFormDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

}

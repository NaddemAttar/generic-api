﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.PostType
{
    public class PostTypeDto
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
    }
}

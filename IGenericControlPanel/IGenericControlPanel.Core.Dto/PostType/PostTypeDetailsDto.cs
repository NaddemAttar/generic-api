﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IGenericControlPanel.Content.Dto.Tips;
using IGenericControlPanel.Core.Dto.Question;

namespace IGenericControlPanel.Core.Dto.PostType
{
    public class PostTypeDetailsDto:PostTypeDto
    {
        public PostTypeDetailsDto()
        {
            BlogDtos = new List<BlogDetailsDto>();
            QuestionDtos = new List<QuestionDetailsDto>();
        }
      public IEnumerable<BlogDetailsDto> BlogDtos { get; set; }
      public IEnumerable<QuestionDetailsDto> QuestionDtos { get; set; }
      public TipsDto Tip { get; set; }
    }
}

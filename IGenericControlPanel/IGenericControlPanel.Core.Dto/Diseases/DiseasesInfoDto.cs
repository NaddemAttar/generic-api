﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Diseases
{
    public class DiseasesInfoDto: DiseasesFormDto
    {
        public int CreatedBy { get; set; }
        public string UserName { get; set; }
        public IEnumerable<PatientDiseasesDto> PatientsDisease { get; set; }
    }
}

﻿using System.Collections.Generic;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.SharedKernal.Enums.Core;

using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.Core.Dto.Diseases
{
    public class DiseasesFormDto
    {
        public DiseasesFormDto()
        {
            Images = new List<FileDetailsDto>();
            ImagesForm = new List<IFormFile>();
            RemovedFilesIds = new List<int>();
            RemovedFilesUrls = new List<string>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DiseaseType DiseaseType { get; set; }
        public string Description { get; set; } = string.Empty;
        public ICollection<FileDetailsDto> Images { get; set; }
        public List<IFormFile> ImagesForm { get; set; }
        public IEnumerable<string> RemovedFilesUrls { get; set; }
        public IEnumerable<int> RemovedFilesIds { get; set; }

    }
}

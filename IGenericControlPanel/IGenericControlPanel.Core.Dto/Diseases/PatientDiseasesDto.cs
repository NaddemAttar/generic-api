﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Medicine;
using IGenericControlPanel.Model.Core;

using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.Core.Dto.Diseases
{
    public class PatientDiseasesDto
    {
        public PatientDiseasesDto()
        {
            MedicineIds = new List<int>();
            Images = new List<FileDetailsDto>();
            RemovedFilesIds = new List<int>();
            RemovedFilesUrls = new List<string>();
            ImagesForm = new List<IFormFile>();
            Medicines = new List<PatientMedicineDto>();
        }
        public int Id { get; set; }
        public int DiseaseId { get; set; }
        public string DiseaseName { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public ICollection<int> MedicineIds { get; set; }
        public ICollection<PatientMedicineDto> Medicines { get; set; }
        public ICollection<FileDetailsDto> Images { get; set; }
        [JsonIgnore]
        public ICollection<string> RemovedFilesUrls { get; set; }
        public List<IFormFile> ImagesForm { get; set; }
        public IEnumerable<int> RemovedFilesIds { get; set; }
    }
}

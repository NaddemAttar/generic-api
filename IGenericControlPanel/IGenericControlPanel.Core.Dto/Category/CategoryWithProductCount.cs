﻿namespace IGenericControlPanel.Core.Dto.Category
{
    public class CategoryWithProductCountL : MultiLevelCategoryDto
    {
        public int ProductCount { get; set; }
        public int BlogsCount { get; set; }
        public int QuestionCount { get; set; }
    }
}
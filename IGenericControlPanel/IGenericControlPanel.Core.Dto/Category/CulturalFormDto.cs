﻿using System;
using System.Collections.Generic;
using System.Text;

using CraftLab.Core.BoundedContext;

namespace IGenericControlPanel.Core.Dto
{
    public class CulturalFormDto : IFormDto
    {
        public string CultureCode { get; set; }

        public ActionOperationType OperationType { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.Core.Dto
{
    public class CulturedCategoryDto
    {
        public int Id { get; set; }
        public string CulturedName { get; set; }
        public string CultureCode { get; set; }
        public int ProductsCount { get; set; }

    }
}

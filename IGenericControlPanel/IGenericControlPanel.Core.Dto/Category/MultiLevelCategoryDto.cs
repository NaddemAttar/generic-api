﻿using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.Core.Dto.Category
{
    public class MultiLevelCategoryDto
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public IFormFile Photo { get; set; }
        public string PhotoUrl { get; set; }
    }
}
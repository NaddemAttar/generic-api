﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Category
{
    public class MultiLevelCategoryDetailsDto: MultiLevelCategoryDto
    {
        public IEnumerable<MultiLevelCategoryDetailsDto> Children { get; set; }
    }
}

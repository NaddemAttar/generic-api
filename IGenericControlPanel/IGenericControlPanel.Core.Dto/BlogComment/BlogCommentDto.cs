﻿using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.Core.Dto.BlogComment
{
    public class BlogCommentDto
    {
        public int Id { get; set; }
        public string CommentContent { get; set; } = string.Empty;
        public int BlogId { get; set; }
        public string ImageUrl { get; set; }
        public IFormFile Image { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Surger
{
    public class SurgeryInfoDto:SurgeryDto
    {
        public SurgeryInfoDto()
        {
            PersonSurgers = new List<PersonSurgeryDto>();
        }
        public IEnumerable<PersonSurgeryDto> PersonSurgers { get; set; }
    }
}

﻿using System.Collections.Generic;

using IGenericControlPanel.Content.Dto;

using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.Core.Dto.Surger
{
    public class SurgeryDto
    {
        public SurgeryDto()
        {
            Images = new List<FileDetailsDto>();
            ImagesForm = new List<IFormFile>();
            RemovedFilesIds = new List<int>();
            RemovedFilesUrls = new List<string>();

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<FileDetailsDto> Images { get; set; }
        public List<IFormFile> ImagesForm { get; set; }
        public IEnumerable<string> RemovedFilesUrls { get; set; }
        public IEnumerable<int> RemovedFilesIds { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Content.Dto;

using Microsoft.AspNetCore.Http;

using Newtonsoft.Json;

namespace IGenericControlPanel.Core.Dto.Surgery
{
    public class PatientSurgeryDto
    {
        public PatientSurgeryDto()
        {
            //MedicineIds = new List<int>();
            Images = new List<FileDetailsDto>();
            RemovedFilesIds = new List<int>();
            RemovedFilesUrls = new List<string>();
            ImagesForm = new List<IFormFile>();
        }
        public int Id { get; set; }
        public string Description { get; set; }
        public int SurgeryId { get; set; }
        public string SurgeryName { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        //public ICollection<int> MedicineIds { get; set; }
        public List<IFormFile> ImagesForm { get; set; }
        [JsonIgnore]
        public List<string> ImagesUrls { get; set; }
        public List<FileDetailsDto> Images { get; set; }
        public IEnumerable<int> RemovedFilesIds { get; set; }
        public IEnumerable<string> RemovedFilesUrls { get; set; }
    }
}

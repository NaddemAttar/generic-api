﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Core.Dto.Surger
{
    public class PersonSurgeryDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int SurgerId { get; set; }
    }
}

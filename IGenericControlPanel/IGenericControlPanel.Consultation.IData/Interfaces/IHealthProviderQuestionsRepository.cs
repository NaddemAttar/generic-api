﻿using CraftLab.Core.OperationResults;

using IGenericControlPanel.Consultation.Dto.ConsultationDto;

namespace IGenericControlPanel.Consultation.IData.Interfaces;
public interface IHealthProviderQuestionsRepository
{
    Task<OperationResult<GenericOperationResult, IEnumerable<HealthProviderQuestionsDto>>> GetHealthProviderQuestionsAsync(
        bool enablePagination, int pageNumber, int pageSize);
    Task<OperationResult<GenericOperationResult, HealthProviderQuestionsDto>> GetOneHealthProviderQuestionAsync(int id);
    Task<OperationResult<GenericOperationResult, HealthProviderQuestionsFormDto>> HealthProviderQuestionActionAsync(
        HealthProviderQuestionsFormDto healthProviderQuestionsFormDto);
    Task<OperationResult<GenericOperationResult>> RemoveHealthProviderQuestionsAsync(int id);
    Task<OperationResult<GenericOperationResult, HealthProviderAnswerDto>> AnswerAsync(
        HealthProviderAnswerDto dto);
}

﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Consultation.Dto.ConsultationDto;

namespace IGenericControlPanel.Consultation.IData.Interfaces;
public interface IUserConsultationRepository
{
    Task<OperationResult<GenericOperationResult>> ConsultationOrderAsync(
        IEnumerable<UserConsultationFormDto> userConsultationDtos, int healthProviderId, string caseDescription);

    Task<OperationResult<GenericOperationResult,
        IEnumerable<UserConsultationAnswersDependingonConsultation>>> GetConsultationAnswersAsync(
        int healthProviderId);

    Task<OperationResult<GenericOperationResult, IEnumerable<HealthProviderQuestionsDto>>> GetHealthProviderQuestionsByIdAsync(
        int heathProviderId);
    Task<OperationResult<GenericOperationResult, IEnumerable<TotalConsultationForPatient>>> GetTotalConsultationForPatientAsync();
    Task<OperationResult<GenericOperationResult, IEnumerable<UserConsultationAnswersDependingonConsultation>>> GetConsultationAnswersByHcpIdAsync(int healthProviderId);
}

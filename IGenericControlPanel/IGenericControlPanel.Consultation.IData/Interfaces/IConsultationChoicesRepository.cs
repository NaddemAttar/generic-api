﻿using CraftLab.Core.OperationResults;

using IGenericControlPanel.Consultation.Dto.ConsultationDto;

namespace IGenericControlPanel.Consultation.IData.Interfaces;
public interface IConsultationChoicesRepository
{
    Task<OperationResult<GenericOperationResult, IEnumerable<ConsultationChoicesDto>>> GetChoicesAsync(
        bool enablePagination, int pageNumber, int pageSize, string query);
    Task<OperationResult<GenericOperationResult, ConsultationChoicesDto>> ActionAsync(ConsultationChoicesDto dto);
    Task<OperationResult<GenericOperationResult>> RemoveAsync(int id);
}

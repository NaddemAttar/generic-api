﻿using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.Communications.Dto
{
    public class ContactFormDto
    {
        #region Properties

        [Required]
        [StringLength(maximumLength: 300)]
        public string FullName { get; set; }

        [Required]
        [StringLength(maximumLength: 300)]
        public string Email { get; set; }

        [Required]
        [StringLength(maximumLength: 2000)]
        public string Address { get; set; }

        [StringLength(maximumLength: 300)]
        public string PhoneNumber { get; set; }

        [Required]
        [StringLength(maximumLength: 300)]
        public string Subject { get; set; }

        public bool Seen { get; set; }
        [StringLength(maximumLength: 5000)]
        [Required]
        public string Message { get; set; }

        #endregion
    }
}

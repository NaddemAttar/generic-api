﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Communications.Dto.Feedback
{
    public class FeedbackDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string YouTubeLink { get; set; }
        public string ImageUrl { get; set; }
        public string ClientName { get; set; }
        public int? ServiceId { get; set; }
        public string ServiceName { get; set; }
    }
}

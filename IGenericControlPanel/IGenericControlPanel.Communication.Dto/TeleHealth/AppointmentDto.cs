﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Communications.Dto.TeleHealth
{
    public class AppointmentDto
    {
        public DateTime Date { get; set; }
        public int SessionTypeId { get; set; }
        public int HeathCareProviderId { get; set; }
    }
}

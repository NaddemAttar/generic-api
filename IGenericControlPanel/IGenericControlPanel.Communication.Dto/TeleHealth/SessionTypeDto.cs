﻿namespace IGenericControlPanel.Communications.Dto.TeleHealth
{
    public class SessionTypeDto
    {
        public int UserServiceSessionId { get; set; }
        public int SessionTypeId { get; set; }
        public int Duration { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public double? NewPrice { get; set; }
        public bool IsOffer => NewPrice != null;
        public DateTime? EndOfferDate { get; set; }
        //public int? ServiceId { get; set; }
        //public string ServiceName { get; set; }
    }
}
﻿using IGenericControlPanel.SharedKernal.Enums.Core;

namespace IGenericControlPanel.Communications.Dto.TeleHealth
{
    public class BookingDto
    {
        public int Id { get; set; }
        public int UserServiceSessionId { get; set; }
        //public int PatientId { get; set; }
        //public string PatientName { get; set; }
        public DateTime AppointmentStart { get; set; }
        public DateTime AppointmentEnd { get; set; }
        public string SessionName { get; set; }
        public string ServiceName { get; set; }
        public string HealthCareProviderName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public int? HealthCareProviderId { get; set; }
        public int? EnterpriseId { get; set; }
        public DateTime Birthdate { get; set; }
        public Gender Gender { get; set; }
        public bool IsConfirm { get; set; }
        public string Token { get; set; }
    }
}
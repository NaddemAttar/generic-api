﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Communications.Dto.TeleHealth
{
    public class BookingSettingsDto
    {
        public int Id { get; set; }
        public int StartOfPatientReception { get; set; }
        public int EndOfPatientReception { get; set; }
    }
}

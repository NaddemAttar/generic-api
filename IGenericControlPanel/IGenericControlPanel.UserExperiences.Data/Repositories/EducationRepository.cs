﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Model.UserExperiences;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using IGenericControlPanel.UserExperiences.Dto.Education;
using IGenericControlPanel.UserExperiences.IData.Interfaces;
using IGenericControlPanel.UserExperiences.Responsibilities.Responsibilities;
using Microsoft.EntityFrameworkCore;
namespace IGenericControlPanel.UserExperiences.Data.Repositories;
public class EducationRepository : BasicRepository<CPDbContext, Education>,
    IEducationRepository
{
    #region Constructor & Proeprties
    private readonly IUserRepository _userRepository;
    public EducationRepository(
         CPDbContext context,
        IUserRepository userRepository) : base(context)
    {
        _userRepository = userRepository;
    }
    #endregion

    #region GET
    public async Task<OperationResult<GenericOperationResult, IEnumerable<EducationDetailsDto>>> GetEducationDataForResumeAsync(
       int hcpId)
    {
        OperationResult<GenericOperationResult,
         IEnumerable<EducationDetailsDto>> result = new(GenericOperationResult.Success);

        try
        {
            hcpId = hcpId == 0 ? _userRepository.CurrentUserIdentityId() : hcpId;

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            return result.UpdateResultData(await Context.GetEducationsForResume(hcpId));
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    public async Task<OperationResult<GenericOperationResult, IEnumerable<EducationDto>>> GetMultipleAsync()
    {
        OperationResult<GenericOperationResult,
        IEnumerable<EducationDto>> result = new(GenericOperationResult.Success);

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            return result.UpdateResultData(await Context.GetEducationsAsync(hcpId));
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    public async Task<OperationResult<GenericOperationResult, EducationDetailsDto>> GetDetailsAsync(int id)
    {
        OperationResult<GenericOperationResult,
      EducationDetailsDto> result = new(GenericOperationResult.Success);

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            return result.UpdateResultData(await Context.GetEducationDetailsAsync(id, hcpId));
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                            .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    #endregion

    #region REMOVE
    public async Task<OperationResult<GenericOperationResult>> RemoveAsync(int id)
    {
        OperationResult<GenericOperationResult> result = new(GenericOperationResult.Success);

        using var transactionAsync = await Context.Database.BeginTransactionAsync();

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            var removeEntity = await Context.Educations
                .Where(ed => ed.IsValid && ed.Id == id && ed.HcpId == hcpId)
                .FirstOrDefaultAsync();

            if (removeEntity is null)
            {
                return result.AddError(ErrorMessages.CanNotRemoveThisEducation)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            Context.Remove(removeEntity);
            await Context.SaveChangesAsync();
            await transactionAsync.CommitAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                            .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }

    #endregion

    #region ACTION
    public async Task<OperationResult<GenericOperationResult, EducationBasicDto>> AddAsync(
        EducationBasicDto addDto, IEnumerable<FileDetailsDto> fileDtos)
    {
        OperationResult<GenericOperationResult,
         EducationBasicDto> result = new(GenericOperationResult.Success, addDto);

        using var transactionAsync = await Context.Database.BeginTransactionAsync();

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            var newEducationEntity = addDto.GenerateEducationEntityAsync(hcpId, fileDtos);

            await Context.Educations.AddAsync(newEducationEntity);
            await Context.SaveChangesAsync();
            await transactionAsync.CommitAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                  .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }

    public async Task<OperationResult<GenericOperationResult, EducationBasicDto>> UpdateAsync(
        EducationBasicDto updateDto, IEnumerable<FileDetailsDto> fileDtos, IEnumerable<int> removeFileIds)
    {
        OperationResult<GenericOperationResult,
          EducationBasicDto> result = new(GenericOperationResult.Success, updateDto);

        using var trasnactionAsync = await Context.Database.BeginTransactionAsync();

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            var updateEntity = await Context.Educations
                .Where(ed => ed.IsValid && ed.Id == updateDto.Id)
                .FirstOrDefaultAsync();

            if (updateEntity is null)
            {
                return result.AddError(ErrorMessages.ThisEducationIsNotFound)
                   .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            Context.Entry(updateEntity).State = EntityState.Detached;

            updateEntity = updateDto.GenerateEducationEntityAsync(hcpId, fileDtos);

            if (removeFileIds != null && removeFileIds.Any())
            {
                Context.Files.RemoveRange(Context.Files.Where(file => removeFileIds.Contains(file.Id)).ToList());
            }

            Context.Educations.Update(updateEntity);
            await Context.SaveChangesAsync();
            await trasnactionAsync.CommitAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }

    #endregion
   
}

﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.UserExperiences;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using IGenericControlPanel.UserExperiences.Dto;
using IGenericControlPanel.UserExperiences.Dto.Certificate;
using IGenericControlPanel.UserExperiences.IData.Interfaces;
using IGenericControlPanel.UserExperiences.Responsibilities.Responsibilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.UserExperiences.Data.Repositories;
public class CertificateRepository : BasicRepository<CPDbContext, Certificate>,
    ICertificateRepository
{
    #region CTOR
    private readonly IUserRepository _userRepository;
    public CertificateRepository(
        CPDbContext context,
        IWebHostEnvironment hostingEnvironment,
        IUserRepository userRepository) : base(context, hostingEnvironment)
    {
        _userRepository = userRepository;
    }
    #endregion

    #region ACTION
    public async Task<OperationResult<GenericOperationResult, CertificateBasicDto>> AddAsync(
        CertificateBasicDto addDto, IEnumerable<FileDetailsDto> fileDtos)
    {
        OperationResult<GenericOperationResult,
            CertificateBasicDto> result = new(
                GenericOperationResult.Success, addDto);

        using var transactionAsync = await Context.Database.BeginTransactionAsync();

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }

            var certificateEntity = addDto.GenerateCertificateEntityAsync(hcpId, fileDtos);

            await Context.Certificates.AddAsync(certificateEntity);
            await Context.SaveChangesAsync();
            await transactionAsync.CommitAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }

    public async Task<OperationResult<GenericOperationResult, CertificateBasicDto>> UpdateAsync(
        CertificateBasicDto updateDto, IEnumerable<FileDetailsDto> fileDtos,
        IEnumerable<int> removeFileIds)
    {
        OperationResult<GenericOperationResult,
            CertificateBasicDto> result = new(
                GenericOperationResult.Success, updateDto);

        using var transactionAsync = await Context.Database.BeginTransactionAsync();

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }

            var certificateEntity = await Context.Certificates.FirstOrDefaultAsync(
                entity => entity.IsValid && entity.Id == updateDto.Id && entity.HcpId == hcpId);

            if (certificateEntity is null)
            {
                return result.AddError(ErrorMessages.ThisCertificateIsNotFound)
                    .UpdateResultStatus(GenericOperationResult.NotFound);
            }

            Context.Entry(certificateEntity).State = EntityState.Detached;

            certificateEntity = updateDto.GenerateCertificateEntityAsync(hcpId, fileDtos);

            if (removeFileIds != null && removeFileIds.Any())
            {
                var removeFileUrls = await Context.Files
                    .Where(file => removeFileIds.Contains(file.Id))
                    .Select(file => file.Url)
                    .ToListAsync();

                Context.RemoveRange(Context.Files.Where(file => removeFileIds.Contains(file.Id)).ToList());

                _ = RemoveFiles(removeFileUrls);
            }

            if (fileDtos != null && fileDtos.Any())
            {
                foreach (var fileDto in fileDtos)
                {
                    var certificateFile = new CertificateFile
                    {
                        Checksum = fileDto.Checksum,
                        FileContentType = fileDto.FileContentType,
                        Description = fileDto.Description,
                        Extension = fileDto.Extension,
                        FileName = fileDto.FileName,
                        Name = fileDto.Name,
                        Url = fileDto.Url,
                    };

                    certificateEntity.CertificateFiles.Add(certificateFile);
                }
            }

            Context.Certificates.Update(certificateEntity);
            await Context.SaveChangesAsync();
            await transactionAsync.CommitAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    #endregion

    #region GET
    public async Task<OperationResult<GenericOperationResult, IEnumerable<CertificateDetailsDto>>> GetCertificateDataForResumeAsync(
       int hcpId)
    {
        OperationResult<GenericOperationResult, IEnumerable<CertificateDetailsDto>> result = new(
          GenericOperationResult.Success);

        try
        {
            hcpId = hcpId == 0 ? _userRepository.CurrentUserIdentityId() : hcpId;

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }

            return result.UpdateResultData(await Context.GetCertificatesForResumeAsync(hcpId));
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    public async Task<OperationResult<GenericOperationResult, CertificateDetailsDto>> GetDetailsAsync(int id)
    {
        OperationResult<GenericOperationResult, CertificateDetailsDto> result = new(
            GenericOperationResult.Success);

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }

            if (id == 0)
            {
                return result.AddError(ErrorMessages.ThisCertificateIsNotFound)
                    .UpdateResultStatus(GenericOperationResult.NotFound);
            }

            return result.UpdateResultData(await Context.GetCertificateDetailsAsync(id, hcpId));
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }

    }

    public async Task<OperationResult<GenericOperationResult, IEnumerable<CertificateDto>>> GetMultipleAsync()
    {
        OperationResult<GenericOperationResult, IEnumerable<CertificateDto>> result = new(
          GenericOperationResult.Success);

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }

            return result.UpdateResultData(await Context.GetCertificatesAsync(hcpId));
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    #endregion

    #region REMOVE
    public async Task<OperationResult<GenericOperationResult>> RemoveAsync(int id)
    {
        OperationResult<GenericOperationResult> result = new(GenericOperationResult.Success);

        using var transactionAsync = await Context.Database.BeginTransactionAsync();

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }

            if (id == 0)
            {
                return result.AddError(ErrorMessages.ThisCertificateIsNotFound)
                    .UpdateResultStatus(GenericOperationResult.NotFound);
            }

            Context.Remove(Context.Certificates
                .Where(entity => entity.IsValid && entity.Id == id && entity.HcpId == hcpId)
                .SingleOrDefault());

            await Context.SaveChangesAsync();
            await transactionAsync.CommitAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }

    #endregion
   
}

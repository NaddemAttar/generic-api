﻿
using CraftLab.Core.OperationResults;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.UserExperiences;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using IGenericControlPanel.UserExperiences.Dto;
using IGenericControlPanel.UserExperiences.Dto.Experience;
using IGenericControlPanel.UserExperiences.IData.Interfaces;
using IGenericControlPanel.UserExperiences.Responsibilities.Responsibilities;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.UserExperiences.Data.Repositories;
public class ExperienceRepository : BasicRepository<CPDbContext, Experience>,
    IExperienceRepository
{
    #region Constructor & Proeprties
    private readonly IUserRepository _userRepository;
    public ExperienceRepository(
         CPDbContext context,
        IUserRepository userRepository) : base(context)
    {
        _userRepository = userRepository;
    }
    #endregion

    #region GET
    public async Task<OperationResult<GenericOperationResult, IEnumerable<ExperienceDetailsDto>>> GetExperienceDataForResumeAsync(
       int hcpId)
    {
        OperationResult<GenericOperationResult,
          IEnumerable<ExperienceDetailsDto>> result = new(GenericOperationResult.Success);

        try
        {
            hcpId = hcpId == 0 ? _userRepository.CurrentUserIdentityId() : hcpId;

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            return result.UpdateResultData(await Context.GetExperiencesForResumeAsync(hcpId));
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                   .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    public async Task<OperationResult<GenericOperationResult, IEnumerable<ExperienceDto>>> GetMultipleAsync()
    {
        OperationResult<GenericOperationResult,
            IEnumerable<ExperienceDto>> result = new(GenericOperationResult.Success);

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            var experienceDtos = await Context.GetExperiencesAsync(hcpId);

            return result.UpdateResultData(experienceDtos);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }

    public async Task<OperationResult<GenericOperationResult, ExperienceDetailsDto>> GetDetailsAsync(int id)
    {
        OperationResult<GenericOperationResult,
           ExperienceDetailsDto> result = new(GenericOperationResult.Success);

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            return result.UpdateResultData(await Context.GetExperienceDetailsAsync(id, hcpId));
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                            .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    #endregion

    #region REMOVE
    public async Task<OperationResult<GenericOperationResult>> RemoveAsync(int id)
    {
        OperationResult<GenericOperationResult> result = new(GenericOperationResult.Success);

        using var transactionAsync = await Context.Database.BeginTransactionAsync();

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            var removeEntity = await Context.Experiences
                .Where(exp => exp.IsValid && exp.Id == id && exp.HcpId == hcpId)
                .FirstOrDefaultAsync();

            if (removeEntity is null)
            {
                return result.AddError(ErrorMessages.CanNotRemoveThisExperience)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            Context.Remove(removeEntity);
            await Context.SaveChangesAsync();
            await transactionAsync.CommitAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                            .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }

    #endregion

    #region ACTION
    public async Task<OperationResult<GenericOperationResult, ExperienceBasicDto>> AddAsync(
        ExperienceBasicDto addDto, IEnumerable<FileDetailsDto> fileDtos)
    {
        OperationResult<GenericOperationResult,
           ExperienceBasicDto> result = new(GenericOperationResult.Success, addDto);

        using var transactionAsync = await Context.Database.BeginTransactionAsync();

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            var newExperienceEntity = addDto.GenerateExperienceEntityAsync(hcpId, fileDtos);

            await Context.AddAsync(newExperienceEntity);
            await Context.SaveChangesAsync();
            await transactionAsync.CommitAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                  .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }

    public async Task<OperationResult<GenericOperationResult, ExperienceBasicDto>> UpdateAsync(
        ExperienceBasicDto updateDto, IEnumerable<FileDetailsDto> fileDtos, IEnumerable<int> removeFileIds)
    {
        OperationResult<GenericOperationResult,
            ExperienceBasicDto> result = new(GenericOperationResult.Success, updateDto);

        using var trasnactionAsync = await Context.Database.BeginTransactionAsync();

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }


            var updateEntity = await Context.Experiences
                .Where(exp => exp.IsValid && exp.Id == updateDto.Id)
                .FirstOrDefaultAsync();

            if (updateEntity is null)
            {
                return result.AddError(ErrorMessages.ThisExperienceIsNotFound)
                   .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            Context.Entry(updateEntity).State = EntityState.Detached;

            updateEntity = updateDto.GenerateExperienceEntityAsync(hcpId, fileDtos);

            if (removeFileIds != null && removeFileIds.Any())
            {
                Context.Files.RemoveRange(Context.Files.Where(file => removeFileIds.Contains(file.Id)).ToList());
            }

            Context.Experiences.Update(updateEntity);
            await Context.SaveChangesAsync();
            await trasnactionAsync.CommitAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }

    #endregion
}

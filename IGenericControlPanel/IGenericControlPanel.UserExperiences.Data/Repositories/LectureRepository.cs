﻿
using CraftLab.Core.OperationResults;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.UserExperiences;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using IGenericControlPanel.UserExperiences.Dto;
using IGenericControlPanel.UserExperiences.Dto.Lecture;
using IGenericControlPanel.UserExperiences.IData.Interfaces;
using IGenericControlPanel.UserExperiences.Responsibilities.Responsibilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.UserExperiences.Data.Repositories;
public class LectureRepository : BasicRepository<CPDbContext, Lecture>,
    ILectureRepository
{
    #region CTOR
    private readonly IUserRepository _userRepository;
    public LectureRepository(
        CPDbContext contex,
        IUserRepository userRepository,
        IWebHostEnvironment hostingEnvironment) : base(contex, hostingEnvironment)
    {
        _userRepository = userRepository;
    }
    #endregion

    #region GET
    public async Task<OperationResult<GenericOperationResult, IEnumerable<LectureDetailsDto>>> GetLectureDataForResumeAsync(
    int hcpId)
    {
        OperationResult<GenericOperationResult,
            IEnumerable<LectureDetailsDto>> result = new(GenericOperationResult.Success);

        try
        {
            hcpId = hcpId == 0 ? _userRepository.CurrentUserIdentityId() : hcpId;

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            return result.UpdateResultData(await Context.GetLecturesForResumeAsync(hcpId));
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
               .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    public async Task<OperationResult<GenericOperationResult, IEnumerable<LectureDto>>> GetMultipleAsync()
    {
        OperationResult<GenericOperationResult,
           IEnumerable<LectureDto>> result = new(GenericOperationResult.Success);

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            return result.UpdateResultData(await Context.GetLecturesAsync(hcpId));
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
              .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }

    public async Task<OperationResult<GenericOperationResult, LectureDetailsDto>> GetDetailsAsync(int id)
    {
        OperationResult<GenericOperationResult,
           LectureDetailsDto> result = new(GenericOperationResult.Success);

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            return result.UpdateResultData(await Context.GetLectureDetailsAsync(id, hcpId));
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
             .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    #endregion

    #region REMOVE
    public async Task<OperationResult<GenericOperationResult>> RemoveAsync(int id)
    {
        OperationResult<GenericOperationResult> result = new(GenericOperationResult.Success);

        using var transactionAsync = await Context.Database.BeginTransactionAsync();

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            if (id == 0)
            {
                return result.AddError(ErrorMessages.ThisLectureIsNotFound)
                    .UpdateResultStatus(GenericOperationResult.NotFound);
            }

            Context.Remove(Context.Lectures
                .FirstOrDefault(entity =>
                entity.IsValid &&
                entity.Id == id &&
                entity.HcpId == hcpId));

            await Context.SaveChangesAsync();
            await transactionAsync.CommitAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
             .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }

    #endregion

    #region ACTION
    public async Task<OperationResult<GenericOperationResult, LectureBasicDto>> AddAsync(
        LectureBasicDto addDto, IEnumerable<FileDetailsDto> fileDtos)
    {
        OperationResult<GenericOperationResult,
          LectureBasicDto> result = new(GenericOperationResult.Success, addDto);

        using var transactionAsync = await Context.Database.BeginTransactionAsync();

        try
        {

            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            var lectureEntity = addDto.GenerateLectureEntityAsync(hcpId, fileDtos);

            await Context.Lectures.AddAsync(lectureEntity);
            await Context.SaveChangesAsync();
            await transactionAsync.CommitAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError("")
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }

    public async Task<OperationResult<GenericOperationResult, LectureBasicDto>> UpdateAsync(
        LectureBasicDto updateDto, IEnumerable<FileDetailsDto> fileDtos, IEnumerable<int> removeFileIds)
    {
        OperationResult<GenericOperationResult,
          LectureBasicDto> result = new(GenericOperationResult.Success, updateDto);

        using var transactionAsync = await Context.Database.BeginTransactionAsync();

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            var updateEntity = await Context.Lectures
                .FirstOrDefaultAsync(entity =>
                entity.IsValid &&
                entity.Id == updateDto.Id &&
                entity.HcpId == hcpId);

            if (updateEntity is null)
            {
                return result.AddError(ErrorMessages.ThisLectureIsNotFound)
                  .UpdateResultStatus(GenericOperationResult.NotFound);
            }

            Context.Entry(updateEntity).State = EntityState.Detached;
            updateEntity = updateDto.GenerateLectureEntityAsync(hcpId, fileDtos);

            if (removeFileIds != null && removeFileIds.Any())
            {
                var removeFileUrls = await Context.Files
                    .Where(file => file.IsValid && removeFileIds.Contains(file.Id))
                    .Select(file => file.Url).ToListAsync();

                Context.RemoveRange(Context.Files.Where(file => removeFileIds.Contains(file.Id)).ToList());

                _ = RemoveFiles(removeFileUrls);
            }

            Context.Lectures.Update(updateEntity);
            await Context.SaveChangesAsync();
            await transactionAsync.CommitAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
               .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }


    #endregion
}

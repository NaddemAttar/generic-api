﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.UserExperiences;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using IGenericControlPanel.UserExperiences.Dto;
using IGenericControlPanel.UserExperiences.Dto.License;
using IGenericControlPanel.UserExperiences.IData.Interfaces;
using IGenericControlPanel.UserExperiences.Responsibilities.Responsibilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.UserExperiences.Data.Repositories;
public class LicenseRepository : BasicRepository<CPDbContext, License>,
    ILicenseRepository
{
    #region CTOR
    private readonly IUserRepository _userRepository;
    public LicenseRepository(
        CPDbContext context,
        IUserRepository userRepository,
        IWebHostEnvironment hostingEnvironment) : base(context, hostingEnvironment)
    {
        _userRepository = userRepository;
    }
    #endregion

    #region GET
    public async Task<OperationResult<GenericOperationResult, IEnumerable<LicenseDetailsDto>>> GetLicenseDataForResumeAsync(
        int hcpId)
    {
        OperationResult<GenericOperationResult,
            IEnumerable<LicenseDetailsDto>> result = new(GenericOperationResult.Success);

        try
        {
            hcpId = hcpId == 0 ? _userRepository.CurrentUserIdentityId() : hcpId;

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            return result.UpdateResultData(await Context.GetResumesForAsync(hcpId));
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
              .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    public async Task<OperationResult<GenericOperationResult, IEnumerable<LicenseDto>>> GetMultipleAsync()
    {
        OperationResult<GenericOperationResult,
           IEnumerable<LicenseDto>> result = new(GenericOperationResult.Success);

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            return result.UpdateResultData(await Context.GetLicensesAsync(hcpId));
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    public async Task<OperationResult<GenericOperationResult, LicenseDetailsDto>> GetDetailsAsync(int id)
    {
        OperationResult<GenericOperationResult,
           LicenseDetailsDto> result = new(GenericOperationResult.Success);

        try
        {
            var hcpId = _userRepository.CurrentUserIdentityId();

            if (id == 0)
            {
                return result.AddError(ErrorMessages.ThisLicenseIsNotFound)
                    .UpdateResultStatus(GenericOperationResult.NotFound);
            }

            if (hcpId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            return result.UpdateResultData(await Context.GetLicenseDetailsAsync(id, hcpId));
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                 .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    #endregion

    #region REMOVE
    public async Task<OperationResult<GenericOperationResult>> RemoveAsync(int id)
    {
        OperationResult<GenericOperationResult> result = new(GenericOperationResult.Success);

        var hcpId = _userRepository.CurrentUserIdentityId();

        if (id == 0)
        {
            return result.AddError(ErrorMessages.ThisLicenseIsNotFound)
                .UpdateResultStatus(GenericOperationResult.NotFound);
        }

        if (hcpId == 0)
        {
            return result.AddError(ErrorMessages.UserMustLoginFirst)
                .UpdateResultStatus(GenericOperationResult.ValidationError);
        }

        using var transactionAsync = await Context.Database.BeginTransactionAsync();

        try
        {
            Context.Remove(Context.Licenses
                .FirstOrDefault(entity => entity.Id == id && entity.HcpId == hcpId));
            await Context.SaveChangesAsync();
            await transactionAsync.CommitAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    #endregion

    #region ACTION
    public async Task<OperationResult<GenericOperationResult, LicenseBasicDto>> AddAsync(
        LicenseBasicDto addDto, IEnumerable<FileDetailsDto> fileDtos)
    {
        OperationResult<GenericOperationResult,
          LicenseBasicDto> result = new(GenericOperationResult.Success, addDto);

        var hcpId = _userRepository.CurrentUserIdentityId();

        if (hcpId == 0)
        {
            return result.AddError(ErrorMessages.UserMustLoginFirst)
                .UpdateResultStatus(GenericOperationResult.ValidationError);
        }

        using var transactionAsync = await Context.Database.BeginTransactionAsync();

        try
        {
            var licenseEntity = addDto.GenerateLicenseEntityAsync(hcpId, fileDtos);

            await Context.Licenses.AddAsync(licenseEntity);
            await Context.SaveChangesAsync();
            await transactionAsync.CommitAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }

    public async Task<OperationResult<GenericOperationResult, LicenseBasicDto>> UpdateAsync(
        LicenseBasicDto updateDto, IEnumerable<FileDetailsDto> fileDtos, IEnumerable<int> removeFileIds)
    {
        OperationResult<GenericOperationResult,
         LicenseBasicDto> result = new(GenericOperationResult.Success, updateDto);

        var hcpId = _userRepository.CurrentUserIdentityId();

        if (hcpId == 0)
        {
            return result.AddError(ErrorMessages.UserMustLoginFirst)
                .UpdateResultStatus(GenericOperationResult.ValidationError);
        }

        using var transactionAsync = await Context.Database.BeginTransactionAsync();

        try
        {
            var updateEntity = await Context.Licenses
                .FirstOrDefaultAsync(entity => 
                entity.IsValid && 
                entity.Id == updateDto.Id && 
                entity.HcpId == hcpId);

            if (updateEntity is null)
            {
                return result.AddError(ErrorMessages.ThisLicenseIsNotFound)
                    .UpdateResultStatus(GenericOperationResult.NotFound);
            }

            Context.Entry(updateEntity).State = EntityState.Detached;

            updateEntity = updateDto.GenerateLicenseEntityAsync(hcpId, fileDtos);

            if (removeFileIds != null && removeFileIds.Any())
            {
                var removeFileUrls = await Context.Files
                    .Where(file => removeFileIds.Contains(file.Id))
                    .Select(file => file.Url)
                    .ToListAsync();

                Context.RemoveRange(Context.Files.Where(file => removeFileIds.Contains(file.Id)).ToList());

                _ = RemoveFiles(removeFileUrls);
            }

            Context.Licenses.Update(updateEntity);
            await Context.SaveChangesAsync();
            await transactionAsync.CommitAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
               .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    
    #endregion
}

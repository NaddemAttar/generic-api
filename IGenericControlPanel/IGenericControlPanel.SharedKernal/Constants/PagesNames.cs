﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.SharedKernal.Constants
{
    public static class PagesNames
    {
        public static int HomePage { get; set; }
        public static int ContactPage { get; set; }
        public static int AboutUsPage { get; set; }
        public static int SharedPage { get; set; }
    }
}

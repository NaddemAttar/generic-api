﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.SharedKernal.Constants
{
    public static class SettingName
    {

        public const string DefaultCultureCode = nameof(DefaultCultureCode);

        public const string ContactMapLatitude = nameof(ContactMapLatitude);
        public const string ContactMapLongitude = nameof(ContactMapLongitude);

        public const string LatestProductsCount = nameof(LatestProductsCount);
        public const string EnableSendingMessages = nameof(EnableSendingMessages);

        public const string FacebookLink = nameof(FacebookLink);
        public const string YoutubeLink = nameof(YoutubeLink);
        public const string LinkedInLink = nameof(LinkedInLink);
        public const string PhoneNumber = nameof(PhoneNumber);
        public const string TwitterLink = nameof(TwitterLink);
        public const string Email = nameof(Email);
        public const string Telegram = nameof(Telegram);
        public const string Whatsapp = nameof(Whatsapp);
        public const string InstagramLink = nameof(InstagramLink);
        public const string WebsiteFont = nameof(WebsiteFont);
        public const string longitude = nameof(longitude);
        public const string latitude = nameof(latitude);
        public const string WorkTime = nameof(WorkTime);
        public const string Location = nameof(Location);

        public const string CurrentLanguage = nameof(CurrentLanguage);
    }
}

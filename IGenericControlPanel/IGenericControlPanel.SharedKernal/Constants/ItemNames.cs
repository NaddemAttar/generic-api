﻿namespace IGenericControlPanel.SharedKernal.Constants
{
    public static class ItemNames
    {
        public const string CultureCode = nameof(CultureCode);
        public const string FileItems = nameof(FileItems);
        public const string SOME_ERRORS_IN_EXCEL_FILE = "Error";
    }
}

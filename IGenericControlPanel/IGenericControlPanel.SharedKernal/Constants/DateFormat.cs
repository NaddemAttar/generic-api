﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.SharedKernal.Constants
{
    public static class DateFormat
    {
        public const string Year_Month_Day = "yyyy-MM-dd";
        public const string Day_Month_Year = "dd-MM-yyyy";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGenericControlPanel.SharedKernal.Constants
{
    public static class ConfigurationNames
    {
        #region DataPaths
        public static readonly string ProductPath = $"{nameof(Database)}:{nameof(DataPath)}:Product";
        public static readonly string ConstructionPath = $"{nameof(Database)}:{nameof(DataPath)}:Construction";
        public static readonly string QuestionPath = $"{nameof(Database)}:{nameof(DataPath)}:Question";
        public static readonly string ApartmentPath = $"{nameof(Database)}:{nameof(DataPath)}:Apartment";
        public static readonly string FloorPath = $"{nameof(Database)}:{nameof(DataPath)}:Floor";
        public static readonly string ProjectPath = $"{nameof(Database)}:{nameof(DataPath)}:Project";
        public static readonly string ServicePath = $"{nameof(Database)}:{nameof(DataPath)}:Service";
        public static readonly string CarouselPath = $"{nameof(Database)}:{nameof(DataPath)}:Carousel";
        public static readonly string TeamMemberPath = $"{nameof(Database)}:{nameof(DataPath)}:TeamMember";
        public static readonly string InformationPath = $"{nameof(Database)}:{nameof(DataPath)}:Information";
        public static readonly string BlogPath = $"{nameof(Database)}:{nameof(DataPath)}:Blog";
        public static readonly string ClientPath = $"{nameof(Database)}:{nameof(DataPath)}:Client";
        public static readonly string AttachmentsPath = $"{nameof(Database)}:{nameof(DataPath)}:Attachments";
        public static readonly string LabelPath = $"{nameof(Database)}:{nameof(DataPath)}:Label";
        #endregion

        #region ConnectionStrings name in appsettings.json

        public static readonly string DevelopmentConnection = $"{nameof(Database)}:{nameof(ConnectionStrings)}:{nameof(DevelopmentConnection)}";
        public static readonly string ProductionConnection = $"{nameof(Database)}:{nameof(ConnectionStrings)}:{nameof(ProductionConnection)}";
        public static readonly string MigrationAssembly = $"{nameof(Database)}:{nameof(MigrationAssembly)}";
        public static readonly string ShowDevelopmentError = $"{nameof(Database)}:{nameof(ShowDevelopmentError)}";


        #endregion


        private static readonly string DataPath = nameof(DataPath);
        private static readonly string ConnectionStrings = nameof(ConnectionStrings);
        private static readonly string Database = nameof(Database);

    }
}

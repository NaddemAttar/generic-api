﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.SharedKernal.Constants
{
    public static class LabelsKeys
    {
        public const string ServiceTitle = nameof(ServiceTitle);

        public const string ServiceDescription = nameof(ServiceDescription);

        public const string LearnTitle = nameof(LearnTitle);

        public const string LearnDescription = nameof(LearnDescription);

        public const string FacultyTitle = nameof(FacultyTitle);

        public const string FacultyDescription = nameof(FacultyDescription);

        public const string HassleTitle = nameof(HassleTitle);

        public const string HassleDescription = nameof(HassleDescription);

        public const string HealthHubTitle = nameof(HealthHubTitle);

        public const string HealthHubDescription = nameof(HealthHubDescription);
    }
}

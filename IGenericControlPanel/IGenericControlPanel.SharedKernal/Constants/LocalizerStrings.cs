﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.SharedKernal.Constants
{
    public static class LocalizerStrings
    {
        #region Titles
        public const string PageTitle_Index = nameof(PageTitle_Index);

        public const string PageTitle_Action_Add = nameof(PageTitle_Action_Add);
        public const string PageTitle_Action_Edit = nameof(PageTitle_Action_Edit);

        public const string ModalTitle_Image = nameof(ModalTitle_Image);
        public const string ModalTitle_Video = nameof(ModalTitle_Video);
        public const string ModalTitle_Attachment = nameof(ModalTitle_Attachment);
        #endregion

        public const string Image_Default_Name = nameof(Image_Default_Name);
        public const string Image_Default_Description = nameof(Image_Default_Description);       
        public const string Video_Default_Name = nameof(Video_Default_Name);
        public const string Video_Default_Description = nameof(Video_Default_Description);

        public const string OperationName_Delete = nameof(OperationName_Delete);
        public const string OperationName_Save = nameof(OperationName_Save);
        public const string OperationName_Retrieve_Data = nameof(OperationName_Retrieve_Data);


        public const string OperationMessage_Success_Add_Update = nameof(OperationMessage_Success_Add_Update);
        public const string OperationMessage_Success_Remove = nameof(OperationMessage_Success_Remove);

        public const string OperationMessage_Failure_Entity_Get_Operation = nameof(OperationMessage_Failure_Entity_Get_Operation);
        public const string OperationMessage_Failure_Null_View_Model = nameof(OperationMessage_Failure_Null_View_Model);
        public const string OperationMessage_Failure_Image_Is_Required = nameof(OperationMessage_Failure_Image_Is_Required);
        public const string OperationMessage_Failure_Video_Is_Required = nameof(OperationMessage_Failure_Video_Is_Required);
        public const string OperationMessage_Failure_Image_Could_Not_Be_Saved = nameof(OperationMessage_Failure_Image_Could_Not_Be_Saved);
        public const string OperationMessage_Failure_Video_Could_Not_Be_Saved = nameof(OperationMessage_Failure_Video_Could_Not_Be_Saved);
        public const string OperationMessage_Failure_Only_One_Image = nameof(OperationMessage_Failure_Only_One_Image);
        public const string OperationMessage_Failure_Only_One_Video = nameof(OperationMessage_Failure_Only_One_Video);

        public const string OperationMessage_Failure_Logic_Remove = nameof(OperationMessage_Failure_Logic_Remove);
        public const string OperationMessage_Failure_Logic_Add_Update = nameof(OperationMessage_Failure_Logic_Add_Update);
        public const string OperationMessage_Failure_Exception_Remove = nameof(OperationMessage_Failure_Exception_Remove);
        public const string OperationMessage_Failure_Exception_Add_Update = nameof(OperationMessage_Failure_Exception_Add_Update);
        public const string OperationMessage_Failure_Try_Again = nameof(OperationMessage_Failure_Try_Again);
        public const string OperationTitle_Failure_Try_Again = nameof(OperationTitle_Failure_Try_Again);
        public const string OperationMessage_Failure_Retrieve_Data = nameof(OperationMessage_Failure_Retrieve_Data);

        public const string OperationMessage_Failure_Image_Save = nameof(OperationMessage_Failure_Image_Save);
        public const string OperationMessage_Failure_One_Image_Must_Exist = nameof(OperationMessage_Failure_One_Image_Must_Exist);
        public const string OperationTitle_Failure_Image_Save = nameof(OperationTitle_Failure_Image_Save);
        public const string OperationTitle_Failure_One_Image_Must_Exist = nameof(OperationTitle_Failure_One_Image_Must_Exist);
        public const string OperationMessage_Failure_Image_Exist = nameof(OperationMessage_Failure_Image_Exist);
        public const string OperationTitle_Failure_Image_Exist = nameof(OperationTitle_Failure_Image_Exist);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

using CraftLab.Core.Infrastructure;
using CraftLab.Core.OperationResults;

namespace IGenericControlPanel.SharedKernal.Interfaces.RepositoryNew
{
    public interface IReadRepository<TEntityIdentity, TDto>
    {
        OperationResult<GenericOperationResult, TDto> Get(TEntityIdentity id, string cultureCode = "ar");
        OperationResult<GenericOperationResult, IEnumerable<TDto>> GetAll(string cultureCode = "ar");
    }

    public interface IReadPagedRepository<TEntityIdentity, TDto>
    {
        OperationResult<GenericOperationResult, IPagedList<TDto>> GetAllPaged(int? pageNumber = 0, int? pageSize = null, string cultureCode = "ar");
        OperationResult<GenericOperationResult, IPagedList<TDto>> GetSomeLatestData(int? pageNumber = 0, int? pageSize = null, string cultureCode = "ar");
    }

    public interface IReadPagedDetailedRepository<TEntityIdentity, TDetailsDto>
    {
        OperationResult<GenericOperationResult, IPagedList<TDetailsDto>> GetAllDetailedPaged(int? pageNumber = 0, int? pageSize = null, string cultureCode = "ar");
    }

    public interface IReadDetailedRepository<TEntityIdentity, TDetailsDto>
    {
        OperationResult<GenericOperationResult, TDetailsDto> GetDetailed(TEntityIdentity id, string cultureCode = "ar");
        OperationResult<GenericOperationResult, IEnumerable<TDetailsDto>> GetAllDetailed(string cultureCode = "ar");

    }

    public interface IActionRepository<TFormDto>
    {
        OperationResult<GenericOperationResult, TFormDto> Action(TFormDto formDto);
    }
    public interface IRemoveRepository<TEntityIdentity>
    {
        OperationResult<GenericOperationResult> Remove(TEntityIdentity id);

    }
}

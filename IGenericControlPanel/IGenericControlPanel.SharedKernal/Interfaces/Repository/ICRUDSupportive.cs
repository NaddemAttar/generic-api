﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.SharedKernal.Interfaces
{
    public interface ICRUDSupportive<TEntityIdentity, TDto, TDetailsDto, TFormDto> : 
        IRetrievable<TEntityIdentity, TDetailsDto, TDto>,
        IRemovable<TEntityIdentity>,
        IUpdatable<TFormDto>
    {

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.SharedKernal.Interfaces
{
    public interface IBasicSettingRepository<TItem, TKey>
    {
        TItem Get(TKey key);
        TCustomReturn Get<TCustomReturn>(TKey key) where TCustomReturn : struct, IConvertible;
        KeyValuePair<TItem, TKey> GetPair(TKey key);

        IEnumerable<TItem> GetAll();
        IDictionary<TKey, TItem> GetAllAsDictionary();

        IEnumerable<TItem> GetSet(IEnumerable<TKey> keys);
        IDictionary<TKey, TItem> GetSetAsDictionary(IEnumerable<TKey> keys);
    }
}

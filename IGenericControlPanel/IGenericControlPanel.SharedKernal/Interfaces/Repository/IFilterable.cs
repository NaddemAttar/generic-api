﻿using System;
using System.Collections.Generic;
using System.Text;

using CraftLab.Core.Infrastructure;

namespace IGenericControlPanel.SharedKernal.Interfaces
{
    public interface IFilterable<TDetailedDto, TDto>
    {
        IEnumerable<TDto> SearchFor(IFilterCriteria filter);
        IEnumerable<TDetailedDto> SearchForDetails(IFilterCriteria filter);
    }

    public interface IFilterable<TDetailedDto, TDto, TFilterData> : IFilterable<TDetailedDto, TDto>
    {
        IPagedList<TDto> SearchFor(IFilterCriteria<TFilterData> filter, int pageNumber, int? pageSize);
        IPagedList<TDetailedDto> SearchForDetails(IFilterCriteria<TFilterData> filter, int pageNumber, int? pageSize);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

using CraftLab.Core.OperationResults;

namespace IGenericControlPanel.SharedKernal.Interfaces
{
    public interface IRemovable<TEntityIdentity>
    {
        /// <summary>
        /// Soft-delete a record with a specific id.
        /// </summary>
        /// <param name="id">The id of the record you want to delete.</param>
        /// <returns></returns>
        OperationResult<GenericOperationResult> Remove(TEntityIdentity id);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

using CraftLab.Core.Infrastructure;
using CraftLab.Core.OperationResults;

namespace IGenericControlPanel.SharedKernal.Interfaces
{
    /// <summary>
    /// Add retrieve operation to the repository with Get and GetAll methods.
    /// </summary>
    /// <typeparam name="TEntityIdentity">The type of Id column of the entity which this repository represents</typeparam>
    /// <typeparam name="TDto">The details dto type that represents this entity.</typeparam>
    public interface IRetrievable<TEntityIdentity, TDto>
    {
        /// <summary>
        /// Get a single record with the correspondence id. The language will be determined by the culture code that is passed in.
        /// </summary>
        /// <param name="id">The record id you want to retreived.</param>
        /// <returns></returns>
        OperationResult<GenericOperationResult, TDto> Get(TEntityIdentity id);

        /// <summary>
        /// Get all the record of the specified entity, used for pagenation.
        /// </summary>
        /// <param name="pageNumber">The page number you want to view.</param>
        /// <param name="pageSize">The number of record you want to view in each page.</param>
        /// <returns></returns>
        OperationResult<GenericOperationResult, IPagedList<TDto>> GetAllPaged(int? pageNumber = 0, 
                                                                              int? pageSize = null);


        /// <summary>
        /// Get all the record of the specified entity, without pagenation.
        /// </summary>
        /// <returns></returns>
        OperationResult<GenericOperationResult, IEnumerable<TDto>> GetAll();

    }


    /// <summary>
    /// Add retrieve operation to the repository with Get and GetAll methods.
    /// </summary>
    /// <typeparam name="TEntityIdentity"></typeparam>
    /// <typeparam name="TDetailedDto"></typeparam>
    /// <typeparam name="TDto"></typeparam>
    public interface IRetrievable<TEntityIdentity, TDetailedDto, TDto> : IRetrievable<TEntityIdentity, TDto>
    {
        /// <summary>
        /// Get a single record with the correspondence id. The language will be determined by the culture code that is passed in.
        /// </summary>
        /// <param name="id">The record id you want to retreived.</param>
        /// <param name="cultureCode">The language of the translated version you want the record to be. If absence, the default language will be used.</param>
        /// <returns></returns>
        OperationResult<GenericOperationResult, TDetailedDto> GetDetailed(TEntityIdentity id);


        /// <summary>
        /// Get all the record of the specified entity, used for pagenation.
        /// </summary>
        /// <param name="pageNumber">The page number you want to view.</param>
        /// <param name="pageSize">The number of record you want to view in each page.</param>
        /// <returns></returns>
        OperationResult<GenericOperationResult, IPagedList<TDetailedDto>> GetDetailedAllPaged(int? pageNumber = 0, int? pageSize = null);

        /// <summary>
        /// Get all the record of the specified entity, without pagenation.
        /// </summary>
        /// <returns></returns>
        OperationResult<GenericOperationResult, IEnumerable<TDetailedDto>> GetDetailedAll();
    }

}

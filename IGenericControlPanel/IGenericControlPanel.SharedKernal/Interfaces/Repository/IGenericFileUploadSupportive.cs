﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

namespace IGenericControlPanel.SharedKernal.Interfaces
{
    public interface IGenericFileUploadSupportive<TDetailedDto> : 
        IImageFileUploadSupportive<TDetailedDto>,
        IVideoFileUploadSupportive<TDetailedDto>,
        IAttachmentUploadSupportive<TDetailedDto>
    {
    }

    public interface IFileUploadSupportive<TDetailedDto>
    {
        Task<OperationResult<GenericOperationResult, TDetailedDto>> FileAction(TDetailedDto detailsDto);

    }

    public interface IImageFileUploadSupportive<TDetailedDto>
    {
        Task<OperationResult<GenericOperationResult, TDetailedDto>> ImageAction(TDetailedDto detailsDto);

    }

    public interface IVideoFileUploadSupportive<TDetailedDto>
    {
        Task<OperationResult<GenericOperationResult, TDetailedDto>> VideoAction(TDetailedDto detailsDto);

    }

    public interface IAttachmentUploadSupportive<TDetailedDto>
    {
        Task<OperationResult<GenericOperationResult, TDetailedDto>> AttachementAction(TDetailedDto detailsDto);

    }
}

﻿using CraftLab.Core.OperationResults;

using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.SharedKernal.Interfaces
{
    /// <summary>
    /// Represents the actions that can be done on a repository.
    /// </summary>
    /// <typeparam name="TFormDto">Object that hold data to do action based on it.</typeparam>
    public interface IUpdatable<TFormDto>
    {
        /// <summary>
        /// Represents an update or add operation for an entity of a repository.
        /// </summary>
        /// <param name="formDto">The form dto that hold the data to add or delete. This can come from a view model.</param>
        /// <returns></returns>
        OperationResult<GenericOperationResult, TFormDto> Action(TFormDto formDto);
    }
}

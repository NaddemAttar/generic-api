﻿using System;
using System.Collections.Generic;
using System.Text;

using CraftLab.Core.Infrastructure;
using CraftLab.Core.OperationResults;

namespace IGenericControlPanel.SharedKernal.Interfaces.Repository
{
    public interface IBasicFileRepository<TEntityIdentity, TDto, TDetailsDto>
    {
        IPagedList<TDto> GetAll();
        IPagedList<TDetailsDto> GetDetailedAll();
        TDetailsDto Get(TEntityIdentity id);

        OperationResult<GenericOperationResult> Remove(TEntityIdentity id);
    }
}

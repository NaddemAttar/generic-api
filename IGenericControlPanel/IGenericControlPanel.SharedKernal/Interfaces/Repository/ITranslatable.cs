﻿using System;
using System.Collections.Generic;
using System.Text;

using CraftLab.Core.Infrastructure;
using CraftLab.Core.OperationResults;

namespace IGenericControlPanel.SharedKernal.Interfaces
{
    /// <summary>
    /// Adds a retrieve operations for the translated version of the repository, with Get and GetAll methods
    /// </summary>
    /// <typeparam name="TEntityIdentity"></typeparam>
    /// <typeparam name="TTranslatedTDto"></typeparam>
    public interface ITranslatable<TEntityIdentity, TTranslatedTDto>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cultureCode">The language of the translated version you want the record to be. If absence, the default language will be used.</param>
        /// <returns></returns>
        OperationResult<GenericOperationResult, TTranslatedTDto> GetTranslated(TEntityIdentity id, string cultureCode = null);

        OperationResult<GenericOperationResult, IPagedList<TTranslatedTDto>> GetAllTranslatedPaged(string cultureCode = null, int? pageNumber = 0, int? pageSize = null);

        OperationResult<GenericOperationResult, IEnumerable<TTranslatedTDto>> GetTranslatedAll(string cultureCode = null);

    }

    /// <summary>
    /// Adds a retrieve operations for the translated version of the repository, with Get and GetAll methods
    /// </summary>
    /// <typeparam name="TTranslatedDto"></typeparam>
    /// <typeparam name="TTranslatedDetailsDto"></typeparam>
    public interface ITranslatable<TEntityIdentity, TTranslatedDetailsDto, TTranslatedDto> : ITranslatable<TEntityIdentity, TTranslatedDto>
    {
        OperationResult<GenericOperationResult, TTranslatedDetailsDto> GetTranslatedDetailed(TEntityIdentity id, string cultureCode = null);

         OperationResult<GenericOperationResult, IPagedList<TTranslatedDetailsDto>> GetTranslatedDetailedAllPaged(string cultureCode = null, int? pageNumber = 0, int? pageSize = null);

        OperationResult<GenericOperationResult, IEnumerable<TTranslatedDetailsDto>> GetTranslatedDetailedAll(string cultureCode = null);

    }
}

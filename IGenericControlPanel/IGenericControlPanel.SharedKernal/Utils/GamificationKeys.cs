﻿
using IGenericControlPanel.SharedKernal.Enums.GamificationSystem;

namespace IGenericControlPanel.SharedKernal.Utils;
public static class GamificationKeys
{
    public static IEnumerable<string> GetGamificationKeys(
        SocialMediaApps socialMediaApps,
        SectionsForPublishOnSocialMedia sectionsForPublishOnSocialMedia)
    {
        if (SocialMediaApps.Facebook == socialMediaApps)
        {
            return (sectionsForPublishOnSocialMedia) switch
            {
                SectionsForPublishOnSocialMedia.Blogs => new List<string> { GamificationNames.PublishBlogOnFB },
                SectionsForPublishOnSocialMedia.Tips => new List<string> { GamificationNames.PublisTipOnFB },
                SectionsForPublishOnSocialMedia.Questions => new List<string> { GamificationNames.PublishQuestionOnFB },
                SectionsForPublishOnSocialMedia.Courses => new List<string> { GamificationNames.PublishCourseOnFB },
                SectionsForPublishOnSocialMedia.Products => new List<string> { GamificationNames.PublishProductOnFB },
                SectionsForPublishOnSocialMedia.LocationsSchedules => new List<string> { GamificationNames.PublishLocationScheduleOnFB },
                SectionsForPublishOnSocialMedia.TeamMembers => new List<string> { GamificationNames.PublishTeamMemberOnFB },
                SectionsForPublishOnSocialMedia.Feedback => new List<string> { GamificationNames.PublishFeedBackOnFB },
                _ => new List<string>()
            };
        }

        else
        {
            return (sectionsForPublishOnSocialMedia) switch
            {
                SectionsForPublishOnSocialMedia.Blogs => new List<string> { GamificationNames.PublishBlogOnINSTA },
                SectionsForPublishOnSocialMedia.Questions => new List<string> { GamificationNames.PublishQuestionOnINSTA },
                SectionsForPublishOnSocialMedia.Courses => new List<string> { GamificationNames.PublishCourseOnINSTA },
                SectionsForPublishOnSocialMedia.Products => new List<string> { GamificationNames.PublishProductOnINSTA },
                SectionsForPublishOnSocialMedia.TeamMembers => new List<string> { GamificationNames.PublishTeamMemberOnINSTA },
                SectionsForPublishOnSocialMedia.Feedback => new List<string> { GamificationNames.PublishFeedBackOnINSTA },
                _ => new List<string>()
            };
        }
    }
}

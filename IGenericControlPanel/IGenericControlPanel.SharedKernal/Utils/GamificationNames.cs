﻿
namespace IGenericControlPanel.SharedKernal.Utils;
public static class GamificationNames
{
    public const string AddService = nameof(AddService);
    public static string AddBlog = nameof(AddBlog);
    public static string AddTip = nameof(AddTip);
    public static string AddQuestion = nameof(AddQuestion);
    public static string AddGroup = nameof(AddGroup);
    public static string JoinInGroup = nameof(JoinInGroup);
    public static string AddProject = nameof(AddProject);
    public static string AddCourse = nameof(AddCourse);
    public static string AddBrand = nameof(AddBrand);
    public static string AddProduct = nameof(AddProduct);
    public static string AddOrder = nameof(AddOrder);
    public static string BuyCourse = nameof(BuyCourse);
    public static string AddLocationSchedule = nameof(AddLocationSchedule);
    public static string AddSession = nameof(AddSession);
    public static string AddAppointment = nameof(AddAppointment);
    public static string AddTeamMember = nameof(AddTeamMember);
    public static string AddPartner = nameof(AddPartner);
    public static string AddFeedback = nameof(AddFeedback);
    public static string AddDisease = nameof(AddDisease);
    public static string AddMedicine = nameof(AddMedicine);
    public static string AddSurgery = nameof(AddSurgery);
    public static string AddAllergy = nameof(AddAllergy);
    public static string AddCategory = nameof(AddCategory);
    public static string AddSpiciality = nameof(AddSpiciality);
    public static string AddPostType = nameof(AddPostType);
    public static string PublishBlogOnFB = nameof(PublishBlogOnFB);
    public static string PublishBlogOnINSTA = nameof(PublishBlogOnINSTA);
    public static string PublishCourseOnFB = nameof(PublishCourseOnFB);
    public static string PublishCourseOnINSTA = nameof(PublishCourseOnINSTA);
    public static string PublishFeedBackOnFB = nameof(PublishFeedBackOnFB);
    public static string PublishFeedBackOnINSTA = nameof(PublishFeedBackOnINSTA);
    public static string PublishLocationScheduleOnFB = nameof(PublishLocationScheduleOnFB);
    public static string PublishProductOnFB = nameof(PublishProductOnFB);
    public static string PublishProductOnINSTA = nameof(PublishProductOnINSTA);
    public static string PublishQuestionOnFB = nameof(PublishQuestionOnFB);
    public static string PublishQuestionOnINSTA = nameof(PublishQuestionOnINSTA);
    public static string PublishTeamMemberOnFB = nameof(PublishTeamMemberOnFB);
    public static string PublishTeamMemberOnINSTA = nameof(PublishTeamMemberOnINSTA);
    public static string PublisTipOnFB = nameof(PublisTipOnFB);
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.SharedKernal.Utils
{
    public static class GenericErrors
    {
        public const string EntityDoesNotExist = "EntityDoesNotExist";
        public const string AdminCannotAcceptOffer = "AdminCannotAcceptOffer";
    }
}

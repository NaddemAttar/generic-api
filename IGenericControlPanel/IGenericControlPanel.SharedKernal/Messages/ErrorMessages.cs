﻿namespace IGenericControlPanel.SharedKernal.Messages
{
    public static class ErrorMessages
    {
        #region Shared Messages
        public static string InternalServerError => nameof(InternalServerError);
        public static string NullObject => nameof(NullObject);
        public static string FilesError => nameof(FilesError);
        public static string LogInFirstBeforeDoingAnyAction => nameof(LogInFirstBeforeDoingAnyAction);
        public static string UserMustLoginFirst => nameof(UserMustLoginFirst);
        public static string FailedSavingPhoto => nameof(FailedSavingPhoto);
        public static string PaymentFailed => nameof(PaymentFailed);
        #endregion

        #region Account
        public static string UsernameCanNotBeNull => nameof(UsernameCanNotBeNull);
        public static string PasswordCanNotBeNull => nameof(PasswordCanNotBeNull);
        public static string UsernameMustNotBeLessThan3Characters => nameof(UsernameMustNotBeLessThan3Characters);
        public static string PasswordMustNotBeLessThan6Characters => nameof(PasswordMustNotBeLessThan6Characters);
        public static string ItMustBeEmailAddress => nameof(ItMustBeEmailAddress);
        public static string UsernameExist => nameof(UsernameExist);
        public static string EmailExists = nameof(EmailExists);
        public static string UserNotExist => nameof(UserNotExist);
        public static string OnlyAdminCanCreateAccount => nameof(OnlyAdminCanCreateAccount);
        public static string UserNotFoundInEnterprise => nameof(UserNotFoundInEnterprise);
        public static string ConfilctBetweenEnterprisesOpenningHoures => nameof(ConfilctBetweenEnterprisesOpenningHoures);
        public static string OutOfEnterpriseOpenningHoures => nameof(OutOfEnterpriseOpenningHoures);
        public static string ConfilctWithIndividualOpenningHoures => nameof(ConfilctWithIndividualOpenningHoures);
        public static string InvalidFacebookToken => nameof(InvalidFacebookToken);
        public static string WrongPasswordOrUsername => nameof(WrongPasswordOrUsername);
        public static string InvalidRefreshToken => nameof(InvalidRefreshToken);
        public static string InvalidGoogleToken => nameof(InvalidGoogleToken);        
        #endregion

        #region Offer
        public static string OfferErrorEnteringDate => nameof(OfferErrorEnteringDate);
        public static string OfferNoProductsFound => nameof(OfferNoProductsFound);
        public static string OfferNoServicesFound => nameof(OfferNoProductsFound);
        public static string OfferNoCoursesFound => nameof(OfferNoProductsFound);
        public static string OfferCategoryNotFound => nameof(OfferCategoryNotFound);
        public static string OfferBrandNotFound => nameof(OfferBrandNotFound);
        public static string OfferNoItemsFound => nameof(OfferNoItemsFound);
        public static string OfferNotFound => nameof(OfferNotFound);
        public static string OfferTypeRequired => nameof(OfferTypeRequired);
        public static string OfferTitleRequired => nameof(OfferTitleRequired);
        public static string OfferDescriptionRequired => nameof(OfferDescriptionRequired);
        public static string OfferPercentageError = nameof(OfferPercentageError);

        public static string YouCanOnlyEnterPercentageError = nameof(YouCanOnlyEnterPercentageError);
        public static string OfferValueCanNotBeBigerThenPrice => nameof(OfferValueCanNotBeBigerThenPrice);
        public static string CanNotAddOfferInTheSameTypeAndDate => nameof(CanNotAddOfferInTheSameTypeAndDate);
        public static string OneOfThoisItemHasOfferBefore => nameof(OneOfThoisItemHasOfferBefore);
        #endregion

        #region Blog
        public static string BlogNotFound => nameof(BlogNotFound);
        public static string BlogCommentNotFound => nameof(BlogCommentNotFound);
        public static string BlogContentRequired => nameof(BlogContentRequired);
        public static string BlogTitleRequired => nameof(BlogTitleRequired);
        public static string BlogCategoryRequired => nameof(BlogCategoryRequired);
        public static string BlogPostTypeRequired => nameof(BlogPostTypeRequired);
        public static string YouDoNotHavePermissionToEdit => nameof(YouDoNotHavePermissionToEdit);
        #endregion

        #region Question
        public static string QuestionContentRequired => nameof(QuestionContentRequired);
        public static string QuestionTitleRequired => nameof(QuestionTitleRequired);
        public static string QuestionCategoryRequired => nameof(QuestionCategoryRequired);
        public static string YouDoNotHavePermissionToDeleteAnswer => nameof(YouDoNotHavePermissionToDeleteAnswer);
        public static string QuestionNotFound => nameof(QuestionNotFound);
        #endregion

        #region Product
        public static string ProductNotFound => nameof(ProductNotFound);
        public static string ProductSizeColorNotFound => nameof(ProductSizeColorNotFound);
        public static string ProductNameRequired => nameof(ProductNameRequired);
        public static string ProductSizeColorRequired => nameof(ProductSizeColorRequired);
        public static string ProductBrandIdRequired => nameof(ProductBrandIdRequired);
        public static string ProductBrandIdNotEquleZero => nameof(ProductBrandIdNotEquleZero);
        public static string ProductCategoryIdRequired => nameof(ProductCategoryIdRequired);
        public static string ProductCategoryIdNotEquleZero => nameof(ProductCategoryIdNotEquleZero);
        public static string ProductIdRequired => nameof(ProductIdRequired);
        public static string ProductPriceRequired => nameof(ProductPriceRequired);
        public static string ProductQuantityRequired => nameof(ProductQuantityRequired);
        public static string ProductReviewNotFound => nameof(ProductReviewNotFound);

        #endregion

        #region Brand Messages
        public static string BrandNotExist => nameof(BrandNotExist);
        public static string BrandNameRequired => nameof(BrandNameRequired);
        #endregion

        #region Allergy Messages
        public static string AllergyNameRequired => nameof(AllergyNameRequired);
        public static string AllergyTypeRequired => nameof(AllergyTypeRequired);
        public static string AllergyDescriptionRequired => nameof(AllergyDescriptionRequired);
        public static string MustChooseAllergy => nameof(MustChooseAllergy);
        public static string AllergyNotExist => nameof(AllergyNotExist);
        public static string PatientAllergyNotExist => nameof(PatientAllergyNotExist);
        #endregion

        #region Course
        public static string CourseNotFound => nameof(CourseNotFound);
        public static string CourseGolesRequired => nameof(CourseGolesRequired);
        public static string CourseNameRequired => nameof(CourseNameRequired);
        public static string CourseTypeRequired => nameof(CourseTypeRequired);
        public static string CoursePreRequirementsRequired => nameof(CoursePreRequirementsRequired);
        public static string CourseModulesRequired => nameof(CourseModulesRequired);
        public static string CourseStudentsCountRequired => nameof(CourseStudentsCountRequired);
        public static string CourseDescriptionRequired => nameof(CourseDescriptionRequired);
        public static string CourseTypeIdsRequired => nameof(CourseTypeIdsRequired);
        public static string CourseInstructorIdsRequired => nameof(CourseInstructorIdsRequired);
        public static string CourseTypeNotFound => nameof(CourseTypeNotFound);
        public static string CourseTypeNameRequired => nameof(CourseTypeNameRequired);
        #endregion
        #region TeleHealth
        public static string SessionTypeNotFound => nameof(SessionTypeNotFound);
        public static string BookingNotFound => nameof(BookingNotFound);
        #endregion


        #region Content
        public static string ContentNotFound => nameof(ContentNotFound);
        #endregion

        #region Tips
        public static string TipNotFound => nameof(TipNotFound);
        public static string TipTitleRequired => nameof(TipTitleRequired);
        public static string TipServiceIdRequired => nameof(TipServiceIdRequired);
        public static string TipDescriptionRequired => nameof(TipDescriptionRequired);
        #endregion

        #region Disease Messages
        public static string DiseaseNameRequired => nameof(DiseaseNameRequired);
        public static string DiseaseDescriptionRequired => nameof(DiseaseDescriptionRequired);
        public static string DiseaseNotExist => nameof(DiseaseNotExist);
        public static string DiseaseTypeRequired => nameof(DiseaseTypeRequired);
        public static string MustChooseDisease => nameof(MustChooseDisease);
        public static string PatientDiseaseNotExist => nameof(PatientDiseaseNotExist);

        #endregion

        #region FeedBack
        public static string FeedBackNotFound => nameof(FeedBackNotFound);
        public static string FeedbackTitleRequired => nameof(FeedbackTitleRequired);
        public static string FeedbackServcieIdRequired => nameof(FeedbackServcieIdRequired);
        public static string FeedbackServiceIdNotEquleZero => nameof(FeedbackServiceIdNotEquleZero);
        #endregion

        #region Group
        public static string GroubNameRequired => nameof(GroubNameRequired);
        public static string GroubCategoryRequired => nameof(GroubCategoryRequired);
        public static string GroubDescriptionRequired => nameof(GroubDescriptionRequired);
        #endregion

        #region Answer
        public static string AnswerNotFound => nameof(AnswerNotFound);
        #endregion

        #region Medical Test Messages
        public static string MedicalTestNameRequired => nameof(MedicalTestNameRequired);
        public static string MedicalTestNotExist => nameof(MedicalTestNotExist);
        #endregion

        #region Medicine Messages
        public static string MedicineNameRequired => nameof(MedicineNameRequired);
        public static string MedicineNotExist => nameof(MedicineNotExist);
        public static string PatientMedicineNotExist => nameof(PatientMedicineNotExist);

        #endregion

        #region Note Messages
        public static string NoteBodyRequired => nameof(NoteBodyRequired);
        public static string MustChooseUserToAddNote => nameof(MustChooseUserToAddNote);
        public static string NoteNotExist => nameof(NoteNotExist);

        #endregion

        #region Surgery Messages
        public static string SurgeryNameRequired => nameof(SurgeryNameRequired);
        public static string SurgeryDescriptionRequired => nameof(SurgeryDescriptionRequired);
        public static string SurgeryNotExist => nameof(SurgeryNotExist);
        public static string PatientSurgeryNotExist => nameof(PatientSurgeryNotExist);

        #endregion

        #region Health Care Specialty Messages
        public static string SpecialtyNotExist => nameof(SpecialtyNotExist);
        #endregion

        #region MultiLevelCategories
        public static string MultiLevelCategoriesNotFound => nameof(MultiLevelCategoriesNotFound);
        #endregion

        #region Subscriber
        public static string SubscriberNotFound => nameof(SubscriberNotFound);
        #endregion

        #region PostType
        public static string PostTypeNotFound => nameof(PostTypeNotFound);
        #endregion


        #region JobOffer
        public static string JobOfferNotFound => nameof(JobOfferNotFound);
        public static string JobOfferTitleRequired => nameof(JobOfferTitleRequired);
        public static string JobOfferResponsibiltyRequired => nameof(JobOfferResponsibiltyRequired);
        public static string JobOfferContractTypeRequired => nameof(JobOfferContractTypeRequired);
        public static string JobOfferExpirationDateRequired => nameof(JobOfferExpirationDateRequired);
        public static string JobOfferQualificationRequired => nameof(JobOfferQualificationRequired);

        #endregion

        #region CarouselItemRepository
        public static string CarouselItemNotExist => nameof(CarouselItemNotExist);
        public static string CarouselNotExist => nameof(CarouselNotExist);
        #endregion

        #region Location Messages
        public static string DoctorLocationNotExist => nameof(DoctorLocationNotExist);
        public static string LocationNotExist => nameof(LocationNotExist);
        public static string OverlapWithAnotherTime => nameof(OverlapWithAnotherTime);
        public static string LocationScheduleNotExist => nameof(LocationScheduleNotExist);
        public static string MustChooseLongitude => nameof(MustChooseLongitude);
        public static string MustChooseLatidude => nameof(MustChooseLatidude);
        public static string MustAddingAddressInfo => nameof(MustAddingAddressInfo);
        public static string CityIdMustNotBeZero => nameof(CityIdMustNotBeZero);
        public static string FromDateRequired => nameof(FromDateRequired);
        public static string ToDateRequired => nameof(ToDateRequired);
        public static string ToDateMustAfterFromDate => nameof(ToDateMustAfterFromDate);
        public static string LocationNameRequired => nameof(LocationNameRequired);
        #endregion

        #region Project Messages
        public static string ProjectTitleRequired => nameof(ProjectTitleRequired);
        public static string ProjectDescriptionRequired => nameof(ProjectDescriptionRequired);
        public static string ProjectDescriptionLengthMustBeAtLeast25Char => nameof(ProjectDescriptionLengthMustBeAtLeast25Char);
        public static string ProjectNotExist => nameof(ProjectNotExist);
        public static string YoutubeLinkNotValid => nameof(YoutubeLinkNotValid);

        #endregion

        #region Service Messages
        public static string ServiceNotExist => nameof(ServiceNotExist);
        public static string ServiceProviderDoesNotExist => nameof(ServiceProviderDoesNotExist);
        public static string ServiceNameRequired => nameof(ServiceNameRequired);
        public static string ServiceDescriptionRequired => nameof(ServiceDescriptionRequired);
        public static string ServiceImageRequired => nameof(ServiceImageRequired);
        #endregion

        #region TeamMember Messages
        public static string TeamMemberNotExist => nameof(TeamMemberNotExist);
        public static string FirstNameIsRequired => nameof(FirstNameIsRequired);
        public static string LastNameIsRequired => nameof(LastNameIsRequired);
        public static string FirstNameMustNotEqualLastName => nameof(FirstNameMustNotEqualLastName);
        public static string AboutIsRequired => nameof(AboutIsRequired);
        public static string BiographyIsRequired => nameof(BiographyIsRequired);
        public static string PhoneNumberRequired => nameof(PhoneNumberRequired);
        public static string PhoneNumberNotValid => nameof(PhoneNumberNotValid);
        public static string EmailIsRequired => nameof(EmailIsRequired);
        public static string EmailNotValid => nameof(EmailNotValid);
        public static string DescriptionIsRequired => nameof(DescriptionIsRequired);
        public static string PositionIsRequired => nameof(PositionIsRequired);
        #endregion

        #region Consultation Messages
        public static string HealthProviderQuestionNotExist => nameof(HealthProviderQuestionNotExist);
        public static string ConsultationChoiceNotExist => nameof(ConsultationChoiceNotExist);
        public static string ConsultationChoiceNameRequired => nameof(ConsultationChoiceNameRequired);
        public static string MustChooseConsultationChoices => nameof(MustChooseConsultationChoices);
        public static string MustNotChooseConsultationChoices => nameof(MustNotChooseConsultationChoices);
        public static string QuestionTextRequired => nameof(QuestionTextRequired);
        public static string ConsultationNotExist => nameof(ConsultationNotExist);
        #endregion

        #region Gamification Messages
        public static string ValueCanNotBeZero => nameof(ValueCanNotBeZero);
        public static string KeyIsExist => nameof(KeyIsExist);
        public static string KeyIsRequired => nameof(KeyIsRequired);
        #endregion

        #region Coupons Management Messages
        public static string ThisCouponNotFound => nameof(ThisCouponNotFound);
        public static string ThisPointsValueIsExistBefore => nameof(ThisPointsValueIsExistBefore);
        public static string CanNotAddThisPercentage => nameof(CanNotAddThisPercentage);
        public static string PointsValueIsRequired => nameof(PointsValueIsRequired);
        public static string PercentageIsRequired => nameof(PercentageIsRequired);
        public static string CouponPeriodIsRequired => nameof(CouponPeriodIsRequired);
        public static string PercentageMustBeBetweenZer0And100 => nameof(PercentageMustBeBetweenZer0And100);
        #endregion

        #region Coupons Operations Messages
        public static string ThisUserHasCouponNotUsingItYet => nameof(ThisUserHasCouponNotUsingItYet);
        public static string ThisCouponIsNotAvailable => nameof(ThisCouponIsNotAvailable);
        public static string ThisCouponIsActivatedAlready => nameof(ThisCouponIsActivatedAlready);
        public static string ThisCouponIsDeActivatedAlready => nameof(ThisCouponIsDeActivatedAlready);
        public static string ThisStatusIsNotAvailableInTheSystem => nameof(ThisStatusIsNotAvailableInTheSystem);
        public static string ThisUserHasNotCoupon => nameof(ThisUserHasNotCoupon);
        public static string ThisUserHasNotActivateCoupon => nameof(ThisUserHasNotActivateCoupon);
        public static string ThisUserHasNotEnoughPointsToUseThisCoupon => nameof(ThisUserHasNotEnoughPointsToUseThisCoupon);
        public static string ThisCouponExpired => nameof(ThisCouponExpired);
        public static string UserCouponIsNotValid => nameof(UserCouponIsNotValid);
        #endregion

        #region Experiences Operations Messages
        public static string CanNotRemoveThisExperience => nameof(CanNotRemoveThisExperience);
        public static string ThisExperienceIsNotFound => nameof(ThisExperienceIsNotFound);
        #endregion

        #region Educations Operations Messages
        public static string CanNotRemoveThisEducation => nameof(CanNotRemoveThisEducation);
        public static string ThisEducationIsNotFound => nameof(ThisEducationIsNotFound);
        #endregion

        #region Licenses Operations Messages
        public static string ThisLicenseIsNotFound => nameof(ThisLicenseIsNotFound);
        #endregion

        #region Lectures Operations Messages
        public static string ThisLectureIsNotFound => nameof(ThisLectureIsNotFound);
        #endregion
        #region Certificates Operations Messages
        public static string ThisCertificateIsNotFound => nameof(ThisCertificateIsNotFound);
        #endregion
        #region SSessionType
        public static string SessionNotFound => nameof(SessionNotFound);
        #endregion
    }
}

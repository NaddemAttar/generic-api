﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace IGenericControlPanel.SharedKernal.Logger.Classes
{
    public static class ExtensionMethods
    {
        public static string ToJsonLogLine(this object obj) =>
                    JsonConvert.SerializeObject(obj, Formatting.None, new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.SharedKernal.Logger.Classes
{
    public class HITLog
    {
        #region Properties
        public string Message { get; set; }
        public Exception Exception { get; set; }
        #endregion

        #region Constructors
        public HITLog()
        {

        }
        public HITLog(string message)
        {
            Message = message;
        }
        public HITLog(Exception exception)
        {
            Exception = exception;
        }

        public HITLog(string message, Exception exception)
        {
            Message = message;
            Exception = exception;
        }
        #endregion

        #region Methods

        #endregion
    }
}

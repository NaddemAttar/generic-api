﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.SharedKernal.Logger.Classes;
using IGenericControlPanel.SharedKernal.Logger.Enums;
using IGenericControlPanel.SharedKernal.Logger.Interface;
using System.Diagnostics;
using Microsoft.Extensions.Logging;

namespace IGenericControlPanel.SharedKernal.Logger.Repository
{
    public class LogService : ILogService
    {
        #region Properties

        protected ILogger Logger { get; private set; }

        #endregion

        #region Constructors

        public LogService(ILogger logger)
        {
            Logger = logger;
        }

        #endregion

        #region Methods

        public void Configure()
        {
            log4net.Config.XmlConfigurator.Configure();
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }


        public void Log(HITLog hitLog, HITLogType logType = HITLogType.Info)
        {
            var hasException = false;
            var message = "";
            if (hitLog.Exception != null)
            {
                hasException = true;
                hitLog.Exception = hitLog.Exception;
                message = hitLog.Exception.Message;
            }
            else
            {
                message = hitLog.Message;
            }
            switch (logType)
            {
                case HITLogType.Info:
                    {
                        if (hasException)
                        {
                            Logger.LogInformation(message, hitLog.Exception);
                            return;
                        }
                        Logger.LogInformation(message);
                        break;
                    }
                case HITLogType.Debug:
                    {
                        if (hasException)
                        {
                            Logger.LogDebug(message, hitLog.Exception);
                            return;
                        }
                        Logger.LogDebug(message);
                        break;
                    }
                case HITLogType.Error:
                    {
                        if (hasException)
                        {
                            Logger.LogError(message, hitLog.Exception);
                            return;
                        }
                        Logger.LogError(message);
                        break;
                    }
                case HITLogType.Fatal:
                    {
                        if (hasException)
                        {
                            Logger.LogCritical(message, hitLog.Exception);
                            return;
                        }
                        Logger.LogCritical(message);
                        break;
                    }
                case HITLogType.Warn:
                    {
                        if (hasException)
                        {
                            Logger.LogWarning(message, hitLog.Exception);
                            return;
                        }
                        Logger.LogWarning(message);
                        break;
                    }
            }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.SharedKernal.Logger.Enums
{
    public enum HITLogType
    {
        Info,
        Debug,
        Warn,
        Error,
        Fatal
    }
}

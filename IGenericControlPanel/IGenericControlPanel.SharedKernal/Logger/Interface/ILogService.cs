﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.SharedKernal.Logger.Classes;
using IGenericControlPanel.SharedKernal.Logger.Enums;

namespace IGenericControlPanel.SharedKernal.Logger.Interface
{

        public interface ILogService
        {
            void Configure();
            void Log(HITLog hitLog, HITLogType logType = HITLogType.Info);
    }
}

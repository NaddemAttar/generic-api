﻿using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using HtmlAgilityPack;

namespace IGenericControlPanel.SharedKernal.Extension
{
    public static class ExtensionMethods
    {
        internal static readonly char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".ToCharArray();
        private static readonly Random _random = new Random();
        public static IQueryable<TEntity> GetDataWithPagination<TEntity>(this IQueryable<TEntity> data,
            int pageSize = 8, int pageNumber = 0)
        {
            return data.Skip(pageSize * pageNumber).Take(pageSize);
        }

        // Generates a random string with a given size.    
        public static string RandomString(int size, bool lowerCase = false)
        {
            var builder = new StringBuilder(size);

            char offset = lowerCase ? 'a' : 'A';
            const int lettersOffset = 26; // A...Z or a..z: length=26  

            for (var i = 0; i < size; i++)
            {
                var @char = (char)_random.Next(offset, offset + lettersOffset);
                builder.Append(@char);
            }

            return lowerCase ? builder.ToString().ToLower() : builder.ToString();
        }

        public static string GetExtension(string ext)
        {
            switch (ext)
            {
                case "plain":
                    return "txt";

                case "pdf":
                    return "pdf";

                case "vnd.openxmlformats-officedocument.presentationml.presentation":
                    return "pptx";

                case "vnd.openxmlformats-officedocument.wordprocessingml.document":
                    return "docx";

                case "jpeg":
                    return "jpg";

                case "png":
                    return "png";
            }
            return "";
        }

        public static string ConvertDateToString(this DateTime date, string format)
        {
            return date.ToString(format);
        }
        public static string GenerateUniqueCode(this int size)
        {
            byte[] data = new byte[4 * size];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetBytes(data);
            }
            StringBuilder result = new StringBuilder(size);
            for (int i = 0; i < size; i++)
            {
                var rnd = BitConverter.ToUInt32(data, i * 4);
                var idx = rnd % chars.Length;

                result.Append(chars[idx]);
            }

            return result.ToString();
        }
        public static string GenerateRandomCode(int length)
        {
            var random = new Random();
            var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            string code = new string(Enumerable.Repeat(0, length).Select(x => characters[random.Next(characters.Length)]).ToArray());
            return code;
        }
        public static string TruncateHtmlAtWord(this string value, int length, HtmlDocument htmlDocument = null)
        {
            if (htmlDocument == null)
            {
                htmlDocument = new HtmlDocument();
            }

            string finalResult = string.Empty;
            if (!string.IsNullOrEmpty(value))
            {
                var decodedText = string.Empty;
                htmlDocument.LoadHtml(value);
                finalResult = htmlDocument.DocumentNode.InnerText.TruncateAtWord(length);
                if (!value.IsHtmlNotEncoded(out decodedText))
                {
                    finalResult = decodedText;
                }
            }
            return finalResult;
        }
        public static bool IsHtmlNotEncoded(this string text, out string decodedText)
        {
            decodedText = HttpUtility.HtmlDecode(text);
            return text.Equals(decodedText, StringComparison.OrdinalIgnoreCase);
        }


        public static string TruncateAtWord(this string value, int length)
        {
            string emptySpace = " ";
            if (value == null || value.Length < length
                              || value.IndexOf(emptySpace, length) == -1)
            {
                return value;
            }
            return value.Substring(0, value.IndexOf(emptySpace, length));
        }

        public static string HTMLToText(string HTMLCode)
        {
            // Remove new lines since they are not visible in HTML
            HTMLCode = HTMLCode.Replace("\n", " ");

            // Remove tab spaces
            HTMLCode = HTMLCode.Replace("\t", " ");

            // Remove multiple white spaces from HTML
            HTMLCode = Regex.Replace(HTMLCode, "\\s+", " ");

            // Remove HEAD tag
            HTMLCode = Regex.Replace(HTMLCode, "<head.*?</head>", ""
                                , RegexOptions.IgnoreCase | RegexOptions.Singleline);

            // Remove any JavaScript
            HTMLCode = Regex.Replace(HTMLCode, "<script.*?</script>", ""
              , RegexOptions.IgnoreCase | RegexOptions.Singleline);

            // Replace special characters like &, <, >, " etc.
            StringBuilder sbHTML = new StringBuilder(HTMLCode);
            // Note: There are many more special characters, these are just
            // most common. You can add new characters in this arrays if needed
            string[] OldWords = {"&nbsp;", "&amp;", "&quot;", "&lt;",
   "&gt;", "&reg;", "&copy;", "&bull;", "&trade;","&#39;"};
            string[] NewWords = { " ", "&", "\"", "<", ">", "Â®", "Â©", "â€¢", "â„¢", "\'" };
            for (int i = 0; i < OldWords.Length; i++)
            {
                sbHTML.Replace(OldWords[i], NewWords[i]);
            }

            // Check if there are line breaks (<br>) or paragraph (<p>)
            sbHTML.Replace("<br>", "\n<br>");
            sbHTML.Replace("<br ", "\n<br ");
            sbHTML.Replace("<p ", "\n<p ");

            // Finally, remove all HTML tags and return plain text
            return System.Text.RegularExpressions.Regex.Replace(
              sbHTML.ToString(), "<[^>]*>", "");
        }


    }
}

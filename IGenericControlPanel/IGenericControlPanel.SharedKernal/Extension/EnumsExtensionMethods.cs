﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Enums.Consultations;
using IGenericControlPanel.SharedKernal.Enums.Content;
using IGenericControlPanel.SharedKernal.Enums.Core;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Enums.GamificationSystem;
using IGenericControlPanel.SharedKernal.Enums.Security;

namespace IGenericControlPanel.SharedKernal.Extension
{
    public static class EnumsExtensionMethods
    {
        public const string ERROR = nameof(ERROR);
        public static string PrivacyMaterialsTypesToString(this PrivacyMaterialsTypes status)
        {
            return status switch
            {
                PrivacyMaterialsTypes.EULA => nameof(PrivacyMaterialsTypes.EULA),
                PrivacyMaterialsTypes.PrivacyPolicy => nameof(PrivacyMaterialsTypes.PrivacyPolicy),
                PrivacyMaterialsTypes.TermsAndConditions => nameof(PrivacyMaterialsTypes.TermsAndConditions),
                _ => ERROR
            };
        }
        public static string AllergyTypeToString(this AllergyType status)
        {
            return status switch
            {
                AllergyType.DrugAllergy => nameof(AllergyType.DrugAllergy),
                AllergyType.NonDrugAllergy => nameof(AllergyType.NonDrugAllergy),
                _ => ERROR
            };
        }
        public static string ContractTypeToString(ContractType status)
        {
            return status switch
            {
                ContractType.Part_Time => nameof(ContractType.Part_Time),
                ContractType.Full_Time => nameof(ContractType.Full_Time),
                ContractType.Remote => nameof(ContractType.Remote),
                _ => ERROR
            };
        }
        public static string DiseaseTypeToString(DiseaseType status)
        {
            return status switch
            {
                DiseaseType.GeneticDisease => nameof(DiseaseType.GeneticDisease),
                DiseaseType.NonChronicDisease => nameof(DiseaseType.NonChronicDisease),
                DiseaseType.ChronicDisease => nameof(DiseaseType.ChronicDisease),
                _ => ERROR
            };
        }
        public static string GenderToString(Gender status)
        {
            return status switch
            {
                Gender.Male => nameof(Gender.Male),
                Gender.Female => nameof(Gender.Female),
                _ => ERROR
            };
        }
        public static string JobStatusToString(JobStatus status)
        {
            return status switch
            {
                JobStatus.Acceptable => nameof(JobStatus.Acceptable),
                JobStatus.Rejected => nameof(JobStatus.Rejected),
                _ => ERROR
            };
        }
        public static string OfferTypeToString(OfferType status)
        {
            return status switch
            {
                OfferType.All => nameof(OfferType.All),
                OfferType.Brand => nameof(OfferType.Brand),
                OfferType.Category => nameof(OfferType.Category),
                OfferType.Multi => nameof(OfferType.Multi),

                _ => ERROR
            };
        }
        public static string RoleToString(Role status)
        {
            return status switch
            {
                Role.Admin => nameof(Role.Admin),
                Role.NormalUser => nameof(Role.NormalUser),
                Role.HealthCareProvider => nameof(Role.HealthCareProvider),
                Role.Modifier => nameof(Role.Modifier),
                _ => ERROR
            };
        }
        public static string PaymentStatusToString(PaymentStatus status)
        {
            return status switch
            {
                PaymentStatus.SUCCESS => nameof(PaymentStatus.SUCCESS),
                PaymentStatus.PENDING => nameof(PaymentStatus.PENDING),
                PaymentStatus.FAILED => nameof(PaymentStatus.FAILED),
                _ => ERROR
            };
        }
        public static string PlaceOfWorkToString(PlaceOfWork status)
        {
            return status switch
            {
                PlaceOfWork.Hospital => nameof(PlaceOfWork.Hospital),
                PlaceOfWork.Home => nameof(PlaceOfWork.Home),
                PlaceOfWork.Clinic => nameof(PlaceOfWork.Clinic),
                _ => ERROR
            };
        }
        public static string SalaryTypeToString(SalaryType status)
        {
            return status switch
            {
                SalaryType.Salary => nameof(SalaryType.Salary),
                SalaryType.Range => nameof(SalaryType.Range),
                SalaryType.AfterInterview => nameof(SalaryType.AfterInterview),
                _ => ERROR
            };
        }
        public static string VoteTypeToString(VoteType status)
        {
            return status switch
            {
                VoteType.Emphasis => nameof(VoteType.Emphasis),
                VoteType.Useful => nameof(VoteType.Useful),
                _ => ERROR
            };
        }
        public static string FileContentTypeToString(FileContentTypee status)
        {
            return status switch
            {
                FileContentTypee.Attachement => nameof(FileContentTypee.Attachement),
                FileContentTypee.Document => nameof(FileContentTypee.Document),
                FileContentTypee.Excel => nameof(FileContentTypee.Excel),
                FileContentTypee.Video => nameof(FileContentTypee.Video),
                FileContentTypee.PDF => nameof(FileContentTypee.PDF),
                FileContentTypee.Word => nameof(FileContentTypee.Word),
                FileContentTypee.Image => nameof(FileContentTypee.Image),
                _ => ERROR
            };
        }
        public static string FileSourceTypeToString(FileSourceType status)
        {
            return status switch
            {
                FileSourceType.AboutUs => nameof(FileSourceType.AboutUs),
                FileSourceType.Group => nameof(FileSourceType.Group),
                FileSourceType.General => nameof(FileSourceType.General),
                FileSourceType.Surgery => nameof(FileSourceType.Surgery),
                FileSourceType.Carousel => nameof(FileSourceType.Carousel),
                FileSourceType.Product => nameof(FileSourceType.Product),
                FileSourceType.Blog => nameof(FileSourceType.Blog),
                FileSourceType.Medicine => nameof(FileSourceType.Medicine),
                FileSourceType.All => nameof(FileSourceType.All),
                FileSourceType.Allergy => nameof(FileSourceType.Allergy),
                FileSourceType.Answer => nameof(FileSourceType.Answer),
                FileSourceType.Course => nameof(FileSourceType.Course),
                FileSourceType.Disease => nameof(FileSourceType.Disease),
                FileSourceType.HealthCareSpecialty => nameof(FileSourceType.HealthCareSpecialty),
                FileSourceType.MedicalTests => nameof(FileSourceType.MedicalTests),
                FileSourceType.Project => nameof(FileSourceType.Project),
                FileSourceType.Question => nameof(FileSourceType.Question),
                FileSourceType.Service => nameof(FileSourceType.Service),
                FileSourceType.TeamMember => nameof(FileSourceType.TeamMember),
                _ => ERROR
            };
        }
        public static string GroupPrivacyLevelToString(GroupPrivacyLevel status)
        {
            return status switch
            {
                GroupPrivacyLevel.Public => nameof(GroupPrivacyLevel.Public),
                GroupPrivacyLevel.Encoded => nameof(GroupPrivacyLevel.Encoded),
                _ => ERROR
            };
        }
        public static string RemoveAccountTypeToString(RemoveAccountType status)
        {
            return status switch
            {
                RemoveAccountType.SoftRemove => nameof(RemoveAccountType.SoftRemove),
                RemoveAccountType.HardRemove => nameof(RemoveAccountType.HardRemove),
                _ => ERROR
            };
        }
        public static string ActionTypeToString(ActionType status)
        {
            return status switch
            {
                ActionType.Action => nameof(ActionType.Action),
                ActionType.Remove => nameof(ActionType.Remove),
                ActionType.Get => nameof(ActionType.Get),
                _ => ERROR
            };
        }
        public static string ConsultationQuestionTypeToString(this ConsultationQuestionType status)
        {
            return status switch
            {
                ConsultationQuestionType.MultiChoices => nameof(ConsultationQuestionType.MultiChoices),
                ConsultationQuestionType.Text => nameof(ConsultationQuestionType.Text),
                ConsultationQuestionType.YesOrNo => nameof(ConsultationQuestionType.YesOrNo),
                _ => ERROR
            };
        }

        public static string SocialMediaAppsTypeToString(this SocialMediaApps status)
        {
            return status switch
            {
                SocialMediaApps.Facebook => nameof(SocialMediaApps.Facebook),
                SocialMediaApps.Instagram => nameof(SocialMediaApps.Instagram),
                _ => ERROR
            };
        }
    }
}

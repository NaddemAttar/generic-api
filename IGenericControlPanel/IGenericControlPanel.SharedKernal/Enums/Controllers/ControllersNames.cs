﻿using System.ComponentModel.DataAnnotations;

namespace IGenericControlPanel.SharedKernal.Enums.Controllers
{
    public enum ControllersNames
    {
        [Display(Description = "Account", ResourceType = typeof(ControllersNames))]
        Account = 1,
        [Display(Description = "Allergy", ResourceType = typeof(ControllersNames))]
        Allergy = 2,
        [Display(Description = "Blog", ResourceType = typeof(ControllersNames))]
        Blog = 3,
        [Display(Description = "Brand", ResourceType = typeof(ControllersNames))]
        Brand = 4,
        [Display(Description = "Carousel", ResourceType = typeof(ControllersNames))]
        Carousel = 5,
        [Display(Description = "ContactInfo", ResourceType = typeof(ControllersNames))]
        ContactInfo = 6,
        [Display(Description = "Content", ResourceType = typeof(ControllersNames))]
        Content = 7,
        [Display(Description = "Course", ResourceType = typeof(ControllersNames))]
        Course = 8,
        [Display(Description = "CourseType", ResourceType = typeof(ControllersNames))]
        CourseType = 9,
        [Display(Description = "Diseases", ResourceType = typeof(ControllersNames))]
        Diseases = 10,
        [Display(Description = "Feedback", ResourceType = typeof(ControllersNames))]
        Feedback = 11,
        [Display(Description = "Group", ResourceType = typeof(ControllersNames))]
        Group = 12,
        [Display(Description = "HealthCareSpecialty", ResourceType = typeof(ControllersNames))]
        HealthCareSpecialty = 13,
        [Display(Description = "Info", ResourceType = typeof(ControllersNames))]
        Info = 14,
        [Display(Description = "JobOffer", ResourceType = typeof(ControllersNames))]
        JobOffer = 15,
        [Display(Description = "Location", ResourceType = typeof(ControllersNames))]
        Location = 16,
        [Display(Description = "MedicalTests", ResourceType = typeof(ControllersNames))]
        MedicalTests = 17,
        [Display(Description = "Medicine", ResourceType = typeof(ControllersNames))]
        Medicine = 18,
        [Display(Description = "MultiLevelCategory", ResourceType = typeof(ControllersNames))]
        MultiLevelCategory = 19,
        [Display(Description = "Note", ResourceType = typeof(ControllersNames))]
        Note = 20,
        [Display(Description = "Offer", ResourceType = typeof(ControllersNames))]
        Offer = 21,
        [Display(Description = "Order", ResourceType = typeof(ControllersNames))]
        Order = 22,
        [Display(Description = "Partners", ResourceType = typeof(ControllersNames))]
        Partners = 23,
        [Display(Description = "PostType", ResourceType = typeof(ControllersNames))]
        PostType = 24,
        [Display(Description = "Product", ResourceType = typeof(ControllersNames))]
        Product = 25,
        [Display(Description = "Project", ResourceType = typeof(ControllersNames))]
        Project = 26,
        [Display(Description = "Question", ResourceType = typeof(ControllersNames))]
        Question = 27,
        [Display(Description = "Service", ResourceType = typeof(ControllersNames))]
        Service = 28,
        [Display(Description = "Setting", ResourceType = typeof(ControllersNames))]
        Setting = 29,
        [Display(Description = "Specification", ResourceType = typeof(ControllersNames))]
        Specification = 30,
        [Display(Description = "Specification", ResourceType = typeof(ControllersNames))]
        Subscriber = 31,
        [Display(Description = "TeamMember", ResourceType = typeof(ControllersNames))]
        TeamMember = 32,
        [Display(Description = "TeleHealth", ResourceType = typeof(ControllersNames))]
        TeleHealth = 33,
        [Display(Description = "TimeManagement", ResourceType = typeof(ControllersNames))]
        TimeManagement = 34,
        [Display(Description = "Permission", ResourceType = typeof(ControllersNames))]
        Permission = 35,


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.SharedKernal.Enums.Content
{
    public enum PrivacyMaterialsTypes
    {
        TermsAndConditions,
        PrivacyPolicy,
        EULA
    }
}

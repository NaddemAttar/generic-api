﻿namespace IGenericControlPanel.SharedKernal.Enums.Consultations;
public enum ConsultationQuestionType
{
    SingleChoice,
    MultiChoices,
    YesOrNo,
    Text
}

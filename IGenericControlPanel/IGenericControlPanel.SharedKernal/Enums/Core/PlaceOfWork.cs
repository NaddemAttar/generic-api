﻿using System.Text.Json.Serialization;

namespace IGenericControlPanel.SharedKernal.Enums.Core
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum PlaceOfWork
    {
        Home,
        Clinic,
        Hospital
    }
}

﻿namespace IGenericControlPanel.SharedKernal.Enums.Core
{
    public enum OfferFor
    {
        Products,
        Courses,
        Services,
    }
}
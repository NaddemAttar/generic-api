﻿namespace IGenericControlPanel.SharedKernal.Enums.Core
{
    public enum HcpType
    {
        Individual,
        Enterprise
    }
}
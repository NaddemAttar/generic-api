﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IGenericControlPanel.SharedKernal.Enums.Core
{
    [Flags]
    public enum AdditionalFeatures
    {
        [Display(Name = "Select_Option_SuperDeluxe")]
        SuperDeluxe = 0,
        [Display(Name = "Select_Option_Covered")]
        Covered = 1 << 0,
        [Display(Name = "Select_Option_Furniture_Polished")]
        FurniturePolished = 1 << 1,
    }
}

﻿namespace IGenericControlPanel.SharedKernal.Enums.Core
{
    public enum OfferType
    {
        All,
        Brand,
        Category,
        Multi,
    }
}

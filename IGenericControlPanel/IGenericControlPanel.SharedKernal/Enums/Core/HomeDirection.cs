﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IGenericControlPanel.SharedKernal.Enums.Core
{
    [Flags]
    public enum HomeDirection
    {
        [Display(Name ="Select_Option_North")]
        North = 0,
        [Display(Name ="Select_Option_West")]
        West = 1 << 0,
        [Display(Name ="Select_Option_East")]
        East = 1 << 1,
        [Display(Name ="Select_Option_South")]
        South = 1 << 2
    }
}

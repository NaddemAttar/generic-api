﻿using System.Text.Json.Serialization;

namespace IGenericControlPanel.SharedKernal.Enums.Core
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum ContractType
    {
        Part_Time,
        Full_Time,
        Remote
    }
}

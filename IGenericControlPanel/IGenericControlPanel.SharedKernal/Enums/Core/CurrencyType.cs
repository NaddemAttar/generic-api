﻿namespace IGenericControlPanel.SharedKernal.Enums.Core
{
    public enum CurrencyType
    {
        USD,
        SP,
        UAE,
        EUR,
    }
}
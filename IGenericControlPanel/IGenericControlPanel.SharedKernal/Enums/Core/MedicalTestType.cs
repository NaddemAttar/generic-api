﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace IGenericControlPanel.SharedKernal.Enums.Core
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum MedicalTestType
    {
        XRay,
        NormalTest
    }
}

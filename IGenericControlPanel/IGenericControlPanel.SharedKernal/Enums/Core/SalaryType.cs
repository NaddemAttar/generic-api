﻿using System.Text.Json.Serialization;

namespace IGenericControlPanel.SharedKernal.Enums.Core
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum SalaryType
    {
        Salary,
        Range,
        AfterInterview
    }
}

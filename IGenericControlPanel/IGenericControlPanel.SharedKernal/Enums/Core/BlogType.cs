﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace IGenericControlPanel.SharedKernal.Enums.Core
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum BlogType
    {
        Text = 0,
        Gallery = 1,
        Video = 2,
        Audio = 3,
        Documents = 4,
        Article = 5
    }
}

﻿namespace IGenericControlPanel.SharedKernal.Enums.GamificationSystem;
public enum GamificationType
{
    Hcp,
    NormalUser,
}

public enum SocialMediaApps
{
    Facebook,
    Instagram
}

public enum SectionsForPublishOnSocialMedia
{
    Blogs,
    Tips,
    Questions,
    Courses,
    Products,
    LocationsSchedules,
    TeamMembers,
    Feedback,
}

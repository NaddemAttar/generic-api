﻿using System;

namespace IGenericControlPanel.SharedKernal.Enums
{
    [Flags]
    public enum FileContentTypee : ulong
    {
        Attachement = 0,

        Image = 1 << 0,
        Video = 1 << 1,

        PDF = 1 << 2,
        Word = 1 << 3,
        Excel = 1 << 4,

        Document = PDF | Word | Excel,
    }
}

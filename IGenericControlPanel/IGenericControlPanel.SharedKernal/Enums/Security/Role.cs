﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.AccessControl;
using System.Text;

using IGenericControlPanel.SharedKernal.Resources;

namespace IGenericControlPanel.SharedKernal.Enums
{
    public enum Role
    {
        [Display(Description = "Admin")]
        Admin = 0,

        [Display(Description = "Modifier")]
        Modifier = 1,
        [Display(Description = "NormalUser")]
        NormalUser = 2,

        [Display(Description = "HealthCareProvider")]
        HealthCareProvider = 3,
    }
}

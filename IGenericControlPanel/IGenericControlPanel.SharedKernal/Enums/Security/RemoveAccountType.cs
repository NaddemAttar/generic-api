﻿namespace IGenericControlPanel.SharedKernal.Enums.Security
{
    public enum RemoveAccountType
    {
        SoftRemove,
        HardRemove
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace IGenericControlPanel.SharedKernal.TagHelpers
{
    /// <summary>
    /// Creates a select input from a custom enum.
    /// </summary>
    [HtmlTargetElement("select-enum")]
    public class SelectEnumTagHelper : TagHelper
    {
        [HtmlAttributeName("asp-for")]
        public ModelExpression AspFor { get; set; }

        public override int Order { get; } = int.MaxValue;

        /// <summary>
        /// The current selected value.
        /// Example: <br />
        /// <code>(int) Enum.ValueName</code>
        /// </summary>
        public int SelectedValue { get; set; } = -1;

        /// <summary>
        /// The type of the enum you want to make it selectable.
        /// <br />
        /// <code>typeof(enum)</code>
        /// </summary>
        public Type EnumType { get; set; }

        /// <summary>
        /// A way to localize the values in the select input.
        /// <br />
        /// Example:
        /// <code>(value) => _localizer["value"] </code>
        /// </summary>
        public Func<string, string> LocalizerDelegate { get; set; }


        /// <summary>
        /// Creating the select-enum tag helper
        /// </summary>
        /// <param name="context"></param>
        /// <param name="output"></param>
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            #region Check for null values
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            if (output is null)
            {
                throw new ArgumentNullException(nameof(output));
            }

            if (EnumType is null)
            {
                throw new ArgumentNullException("The attribute enum-type cannot be empty, please provide the type of the enum you want to create a select list of using typeof(enumName)");
            }
            #endregion

            // The output of processing this tag helper will be a 'select' HTML element.
            output.TagName = "select";

            string name = AspFor.Name;
            if (!string.IsNullOrEmpty(name))
            {
                output.Attributes.Add("name", name);
            }

            //#region Default option element

            //// The 'select' HTML element will have, by default, one 'option' element by default
            //var defaultOptionElement = new TagBuilder("option");
            //// that will have a value of '-1' as nothing is selected yet.
            //defaultOptionElement.Attributes.Add("value", "-1");
            //// specify 'Text' for the default option
            //defaultOptionElement.InnerHtml.Append(LocalizerDelegate is null ?
            //    GetEnumDisplayName(-1) :
            //    LocalizerDelegate(GetEnumDisplayName(-1)));
            //// make it the selected value if nothing is set by the SelectedValue property.
            //if (SelectedValue == -1)
            //{
            //    defaultOptionElement.Attributes.Add("selected", "selected");
            //}
            //// adding the default option to the output element
            //output.Content.AppendHtml(defaultOptionElement);

            //#endregion

            #region Options from the enum specified by EnumType

            // foreach value for the provided enum by 'EnumType' I will create an 'option' element
            foreach (int @enum in Enum.GetValues(EnumType))
            {
               
                if (@enum == -1)
                    continue;

                var optionElement = new TagBuilder("option");

                optionElement.Attributes.Add("value", $"{@enum}");
                optionElement.InnerHtml.Append(LocalizerDelegate is null ?
                    GetEnumDisplayName(@enum) :
                    LocalizerDelegate(GetEnumDisplayName(@enum)));

                if (@enum == SelectedValue)
                {
                    optionElement.Attributes.Add("selected", "selected");
                }

                output.Content.AppendHtml(optionElement);
            }

            #endregion
        }

        private string GetEnumDisplayName(int value)
        {
            var fieldName = Enum.GetName(EnumType, value);
            if (fieldName is null)
            {
                throw new ArgumentNullException($"Couldn't convert the value '{value}' to the enum '{EnumType.Name}', please make sure there is a value that corresponde to that enum.");
            }
            var displayName = EnumType.GetField(fieldName).GetCustomAttributes(false).OfType<DisplayAttribute>().SingleOrDefault()?.Name;
            return displayName ?? fieldName;
        }
    }
}

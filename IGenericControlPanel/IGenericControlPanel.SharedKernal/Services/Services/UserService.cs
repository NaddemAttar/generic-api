﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

using CraftLab.Core.ExtensionMethods;

using IGenericControlPanel.SharedKernal.Dto;
using IGenericControlPanel.SharedKernal.Services.IServices;

using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.SharedKernal.Services.Services
{
    public class UserService:IUserService
    {

        #region Properties

        #region Constants


        #endregion

        private UserDto HITWebUser { get; set; }

        //private HttpContextBase HttpContext { get; }
        private readonly IHttpContextAccessor _httpContextAccessor;

        //private ILogService LogService { get; }

        //private IAuthenticationManager AuthenticationManager { get; }

        public int? UserId
        {
            get
            {
                if (HITWebUser != null)
                {
                    return HITWebUser.Id;
                }
                return GetClaimValue<int?>(ClaimTypes.NameIdentifier);
            }
        }

        //public ClientType ClientType
        //{
        //    get
        //    {
        //        var userClient = _httpContextAccessor.HttpContext.Request.Headers[nameof(ClientType)];
        //        if (Enum.TryParse(userClient, out ClientType clientType))
        //        {
        //            return clientType;
        //        }
        //        return ClientType.Others;
        //    }
        //}

        //public string FcmId
        //{
        //    get
        //    {
        //        return _httpContextAccessor.HttpContext.Request.Headers[HeaderConstants.FcmTokenHeaderKey];
        //    }
        //}

        #endregion

        #region Constructors
        public UserService(
             IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Methods

        public UserDto GetUserInfo()
        {
            if (HITWebUser != null)
            {
                return HITWebUser;
            }
            if (_httpContextAccessor.HttpContext.User == null
                ||
                _httpContextAccessor.HttpContext.User.Identity == null
                ||
                !_httpContextAccessor.HttpContext.User.Identity.IsAuthenticated)
            {
                return new UserDto();
            }

            HITWebUser = new UserDto()
            {
                IsAuthenticated = _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated,
                Id = GetClaimValue<int?>(ClaimTypes.NameIdentifier),
                Email = GetClaimValue<string>(ClaimTypes.Name),
                FirstName = GetClaimValue<string>(ClaimTypes.GivenName),
                LastName = GetClaimValue<string>(ClaimTypes.Surname),
                //InTouchPointName = GetClaimValue<string>(HITClaimTypes.InTouchPointName),
                //PhotoUrl = GetClaimValue<string>(HITClaimTypes.PhotoUrl),
                RoleName = GetClaimValue<string>(ClaimTypes.Role),
                //ProfileType = GetClaimValue<AccountTemplate>(HITClaimTypes.ProfileType),
                //AccountPermissions = GetClaimValues<AccountPermissionType>(HITClaimTypes.AccountPermissions),
                //FcmToken = HttpContext.Request.Headers.Get(HeaderConstants.FcmTokenHeaderKey)
            };
            return HITWebUser;
        }

        public UserDto GetUserInfo(IPrincipal principal, bool setCurrentUser = false)
        {
            if (principal == null
                ||
                principal.Identity == null
                ||
                !(principal is ClaimsPrincipal claimsPrincipal)
                ||
                !principal.Identity.IsAuthenticated)
            {
                return new UserDto();
            }


            var userInfoDto = new UserDto()
            {
                IsAuthenticated = true,
                Id = GetClaimValue<int?>(ClaimTypes.NameIdentifier, claimsPrincipal),
                Email = GetClaimValue<string>(ClaimTypes.Name, claimsPrincipal),
                FirstName = GetClaimValue<string>(ClaimTypes.GivenName, claimsPrincipal),
                LastName = GetClaimValue<string>(ClaimTypes.Surname, claimsPrincipal),
                //InTouchPointName = GetClaimValue<string>(HITClaimTypes.InTouchPointName, claimsPrincipal),
                //PhotoUrl = GetClaimValue<string>(HITClaimTypes.PhotoUrl, claimsPrincipal),
                RoleName = GetClaimValue<string>(ClaimTypes.Role, claimsPrincipal),
                //ProfileType = GetClaimValue<AccountTemplate>(HITClaimTypes.ProfileType, claimsPrincipal),
                //AccountPermissions = GetClaimValues<AccountPermissionType>(HITClaimTypes.AccountPermissions, claimsPrincipal),
                //FcmToken = HttpContext.Request.Headers.Get(HeaderConstants.FcmTokenHeaderKey)
            };
            if (setCurrentUser)
            {
                HITWebUser = userInfoDto;
            }
            return userInfoDto;
        }

        //public IEnumerable<AccountPermissionType> GetAccountPermissions()
        //{
        //    return GetClaimValues<AccountPermissionType>(HITClaimTypes.AccountPermissions);
        //}


        //public bool CheckAccountPermission(AccountPermissionType accountPermissionType)
        //{
        //    try
        //    {
        //        if (HttpContext.User == null
        //            || HttpContext.User.Identity == null
        //            || !HttpContext.User.Identity.IsAuthenticated
        //            || !(HttpContext.User is ClaimsPrincipal userClaims))
        //        {
        //            return false;
        //        }
        //        return userClaims
        //            .Claims
        //            .Any(
        //                    cl => cl.Type == HITClaimTypes.AccountPermissions
        //                          &&
        //                          cl.Value == ((int)accountPermissionType).ToString()
        //                );
        //    }
        //    catch (Exception ex)
        //    {
        //        LogService.Log
        //            (new Logging.Dto.HITLog($"Could not check the user permission {accountPermissionType} due to the following error:", ex)
        //            , Logging.Dto.HITLogType.Warn);
        //        return false;
        //    }
        //}


        //public void IdentitySignin(UserDto user, string providerKey = null, bool isPersistent = false)
        //{
        //    var claims = new List<Claim>
        //    {
        //        new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
        //        new Claim(ClaimTypes.Name, user.Email),
        //        new Claim(ClaimTypes.GivenName, user.FirstName),
        //        new Claim(ClaimTypes.Surname, user.LastName),
        //        new Claim(ClaimTypes.Role, user.RoleName),
        //        //new Claim(HITClaimTypes.InTouchPointName, user.InTouchPointName),
        //        //new Claim(HITClaimTypes.PhotoUrl, user.PhotoUrl),
        //        //new Claim(HITClaimTypes.ProfileType, user.ProfileType.ToString()),
        //    };

        //    #region Fill account permission

        //    //if (user.AccountPermissions != null && user.AccountPermissions.Any())
        //    //{
        //    //    foreach (var accountPermission in user.AccountPermissions)
        //    //    {
        //    //        claims.Add(new Claim(HITClaimTypes.AccountPermissions, ((int)accountPermission).ToString()));
        //    //    }
        //    //}

        //    #endregion

        //    var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
        //    AuthenticationManager.SignIn(new AuthenticationProperties()
        //    {
        //        AllowRefresh = true,
        //        IsPersistent = isPersistent,
        //        ExpiresUtc = DateTime.UtcNow.AddDays(90)
        //    }, identity);
        //}


        //public void IdentitySignout()
        //{
        //    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie,
        //                                  DefaultAuthenticationTypes.ExternalCookie);
        //}

        //public void IdentityUpdateUserInformation(IEnumerable<Claim> claims)
        //{
        //    var userIdentityClaims = new ClaimsIdentity(AuthenticationManager.User.Identity);

        //    foreach (var claim in claims)
        //    {
        //        userIdentityClaims.RemoveClaim(userIdentityClaims.FindFirst(claim.Type));
        //        userIdentityClaims.AddClaim(claim);
        //    }

        //    AuthenticationManager.AuthenticationResponseGrant =
        //        new AuthenticationResponseGrant(
        //            new ClaimsPrincipal(userIdentityClaims),
        //            new AuthenticationProperties { IsPersistent = true }
        //        );
        //}

        private TValue GetClaimValue<TValue>(string claimType)
        {
            try
            {
                if (_httpContextAccessor.HttpContext.User == null
                    || _httpContextAccessor.HttpContext.User.Identity == null
                    || !_httpContextAccessor.HttpContext.User.Identity.IsAuthenticated
                    || !(_httpContextAccessor.HttpContext.User is ClaimsPrincipal userClaims))
                {
                    return default;
                }
                return GetClaimValue<TValue>(claimType, userClaims);
            }
            catch (Exception ex)
            {
                //LogService.Log
                //    (new Logging.Dto.HITLog($"Could not get Claim value for type {claimType}", ex)
                //    , Logging.Dto.HITLogType.Warn);
                return default;
            }
        }

        private TValue GetClaimValue<TValue>(string claimType, ClaimsPrincipal userClaims)
        {
            try
            {
                var claim = userClaims.Claims.FirstOrDefault(cl => cl.Type == claimType);

                if (claim == null)
                {
                    return default(TValue);
                }
                claim.Value.TryConvert(out TValue value);
                return value;
            }
            catch (Exception ex)
            {
                //LogService.Log
                //    (new Logging.Dto.HITLog($"Could not get Claim value for type {claimType}", ex)
                //    , Logging.Dto.HITLogType.Warn);
                return default;
            }
        }

        private IEnumerable<TValue> GetClaimValues<TValue>(string claimType)
        {
            try
            {
                if (_httpContextAccessor.HttpContext.User == null
                    || _httpContextAccessor.HttpContext.User.Identity == null
                    || !_httpContextAccessor.HttpContext.User.Identity.IsAuthenticated
                    || !(_httpContextAccessor.HttpContext.User is ClaimsPrincipal userClaims))
                {
                    return default;
                }
                return GetClaimValues<TValue>(claimType, userClaims);
            }
            catch (Exception ex)
            {
                //LogService.Log
                //    (new Logging.Dto.HITLog($"Could not get Claim value for type {claimType}", ex)
                //    , Logging.Dto.HITLogType.Warn);
                return new TValue[0];
            }
        }

        private IEnumerable<TValue> GetClaimValues<TValue>(string claimType, ClaimsPrincipal userClaims)
        {
            try
            {
                var claims = userClaims.FindAll(cl => cl.Type == claimType);
                if (claims == null || !claims.Any())
                {
                    return new TValue[0];
                }

                var claimsValues = new List<TValue>();

                foreach (var claim in claims)
                {

                    if (claim.Value.TryConvert(out TValue currentValue))
                    {
                        claimsValues.Add(currentValue);
                    }
                }
                return claimsValues;
            }
            catch (Exception ex)
            {
                //LogService.Log
                //    (new Logging.Dto.HITLog($"Could not get Claim value for type {claimType}", ex)
                //    , Logging.Dto.HITLogType.Warn);
                return new TValue[0];
            }
        }

        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.SharedKernal.Dto;

namespace IGenericControlPanel.SharedKernal.Services.IServices
{
    public interface IUserService
    {
        int? UserId { get; }
        //string FcmId { get; }
        UserDto GetUserInfo();
        UserDto GetUserInfo(IPrincipal principal, bool setCurrentUser = false);


        #region Identity

        //void IdentitySignin(UserDto user, string providerKey = null, bool isPersistent = false);

        //void IdentitySignout();

        //void IdentityUpdateUserInformation(IEnumerable<Claim> claims);



        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace IGenericControlPanel.SharedKernal.HelperMethods
{
    public static class HelperMethods
    {
        public static string EnsureCulture(string cultureCode, CultureInfo defaultCulture)
        {
            if (cultureCode is null)
            {
                cultureCode = defaultCulture.Name;
            }

            return !defaultCulture.Name.ToLower().Contains(cultureCode.Trim().ToLower()) ? cultureCode : null;
        }

    }
}

﻿using System.Threading;

using AutoMapper;

using CraftLab.Core.BoundedContext.Repository;
using CraftLab.Core.Model.Interface;
using CraftLab.Core.OperationResults;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace IGenericControlPanel.SharedKernal.Repository
{
    public class BasicRepository<TDbContext, TEntity> : CraftLabRepository<TDbContext, TEntity> where TDbContext : DbContext
                                                                                             where TEntity : class, IDeletable
    {
        protected IWebHostEnvironment _hostingEnvironment;
        protected IMapper Mapper { get; private set; }
        public const string DefaultLanguage = "en-US";
        protected string CurrentCultureCode => Thread.CurrentThread.CurrentCulture.Name;

        protected bool IsDefaultLanguage => DefaultLanguage.Contains(CurrentCultureCode);
        public BasicRepository(TDbContext context, 
            IWebHostEnvironment hostingEnvironment = null) : base(context)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        public BasicRepository(TDbContext context, 
            IMapper mapper, 
            IWebHostEnvironment hostingEnvironment = null) : base(context)
        {
            Mapper = mapper;
            _hostingEnvironment = hostingEnvironment;
        }
        protected bool RemoveFiles(IEnumerable<string> paths)
        {
            foreach (var path in paths)
            {
                string fullPath = Path.Combine(_hostingEnvironment.WebRootPath, path);
                File.Delete(fullPath);
            }

            return true;
        }
    }
}

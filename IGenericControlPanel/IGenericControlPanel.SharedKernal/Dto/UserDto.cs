﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.SharedKernal.Dto
{
    public class UserDto
    {
        #region Properties

        public int? Id { get; set; }
        public string RoleName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public string Email { get; set; }
        public string InTouchPointName { get; set; }
        public string PhotoUrl { get; set; }

        public string FcmToken { get; set; }
        public bool IsAuthenticated { get; set; }

        #endregion

        #region Methods

        public bool IsTokenNotExists()
        {
            return string.IsNullOrEmpty(FcmToken);
        }

        #endregion

    }
}

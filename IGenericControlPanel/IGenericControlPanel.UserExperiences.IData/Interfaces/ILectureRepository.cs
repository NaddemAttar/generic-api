﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Configuration.IData.Interfaces;
using IGenericControlPanel.UserExperiences.Dto.Lecture;

namespace IGenericControlPanel.UserExperiences.IData.Interfaces;
public interface ILectureRepository :
    IGetMultipleGeneric<LectureDto>,
    IGetDetailsGeneric<LectureDetailsDto, int>,
    IRemoveGeneric<int>,
    IAddGeneric<LectureBasicDto>,
    IUpdateGeneric<LectureBasicDto, int>
{
    Task<OperationResult<GenericOperationResult, IEnumerable<LectureDetailsDto>>> GetLectureDataForResumeAsync(int hcpId);
}

﻿
using CraftLab.Core.OperationResults;
using IGenericControlPanel.Configuration.IData.Interfaces;
using IGenericControlPanel.UserExperiences.Dto.Experience;

namespace IGenericControlPanel.UserExperiences.IData.Interfaces;
public interface IExperienceRepository :
    IGetMultipleGeneric<ExperienceDto>,
    IGetDetailsGeneric<ExperienceDetailsDto, int>,
    IRemoveGeneric<int>,
    IAddGeneric<ExperienceBasicDto>,
    IUpdateGeneric<ExperienceBasicDto, int>
{
    Task<OperationResult<GenericOperationResult, IEnumerable<ExperienceDetailsDto>>> GetExperienceDataForResumeAsync(int hcpId);
}

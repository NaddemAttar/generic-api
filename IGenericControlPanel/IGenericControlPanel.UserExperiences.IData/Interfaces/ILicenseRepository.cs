﻿
using CraftLab.Core.OperationResults;
using IGenericControlPanel.Configuration.IData.Interfaces;
using IGenericControlPanel.UserExperiences.Dto.License;

namespace IGenericControlPanel.UserExperiences.IData.Interfaces;
public interface ILicenseRepository :
    IGetMultipleGeneric<LicenseDto>,
    IGetDetailsGeneric<LicenseDetailsDto, int>,
    IRemoveGeneric<int>,
    IAddGeneric<LicenseBasicDto>,
    IUpdateGeneric<LicenseBasicDto, int>
{
    Task<OperationResult<GenericOperationResult, IEnumerable<LicenseDetailsDto>>> GetLicenseDataForResumeAsync(int hcpId);
}

﻿
using CraftLab.Core.OperationResults;
using IGenericControlPanel.Configuration.IData.Interfaces;
using IGenericControlPanel.UserExperiences.Dto.Certificate;

namespace IGenericControlPanel.UserExperiences.IData.Interfaces;
public interface ICertificateRepository:
    IGetMultipleGeneric<CertificateDto>,
    IGetDetailsGeneric<CertificateDetailsDto, int>,
    IRemoveGeneric<int>,
    IAddGeneric<CertificateBasicDto>,
    IUpdateGeneric<CertificateBasicDto, int>
{
    Task<OperationResult<GenericOperationResult, IEnumerable<CertificateDetailsDto>>> GetCertificateDataForResumeAsync(int hcpId);
}

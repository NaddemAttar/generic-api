﻿
using CraftLab.Core.OperationResults;
using IGenericControlPanel.Configuration.IData.Interfaces;
using IGenericControlPanel.UserExperiences.Dto.Education;

namespace IGenericControlPanel.UserExperiences.IData.Interfaces;
public interface IEducationRepository :
    IGetMultipleGeneric<EducationDto>,
    IGetDetailsGeneric<EducationDetailsDto, int>,
    IRemoveGeneric<int>,
    IAddGeneric<EducationBasicDto>,
    IUpdateGeneric<EducationBasicDto, int>
{
    Task<OperationResult<GenericOperationResult, IEnumerable<EducationDetailsDto>>> GetEducationDataForResumeAsync(int hcpId);
}

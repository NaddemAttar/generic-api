﻿
using CraftLab.Core.OperationResults;
using IGenericControlPanel.Configuration.Dto.Pagination;
using IGenericControlPanel.Configuration.IData.Interfaces;
using IGenericControlPanel.GamificationSystem.Dto.Coupon;

namespace IGenericControlPanel.GamificationSystem.IData.Interfaces;
public interface ICouponManagementRepository: ISharedIntegerRepository
{
    Task<OperationResult<GenericOperationResult, IEnumerable<CouponDto>>> GetCouponsAsync(
        PaginationDto dto);

    Task<OperationResult<GenericOperationResult, CouponDto>> GetCouponDetailsAsync(
        int id);
    Task<OperationResult<GenericOperationResult>> RemoveCouponAsync(int id);

    Task<OperationResult<GenericOperationResult, CouponDto>> AddCouponAsync(CouponDto createNewCouponDto);
    Task<OperationResult<GenericOperationResult, CouponDto>> UpdateCouponAsync(CouponDto updateCouponDto);
}

﻿
using CraftLab.Core.OperationResults;

namespace IGenericControlPanel.GamificationSystem.IData.Interfaces;
public interface IGamificationMovementsRepository
{
    Task<OperationResult<GenericOperationResult>> SavePoints(IEnumerable<string> keys);
}

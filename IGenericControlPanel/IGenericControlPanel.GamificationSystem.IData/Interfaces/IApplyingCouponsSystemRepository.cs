﻿
using CraftLab.Core.OperationResults;
using IGenericControlPanel.Configuration.IData.Interfaces;
using IGenericControlPanel.GamificationSystem.Dto.Coupon;

namespace IGenericControlPanel.GamificationSystem.IData.Interfaces;
public interface IApplyingCouponsSystemRepository
{
    Task<OperationResult<GenericOperationResult, IEnumerable<CouponDto>>> GetAvailableCouponsAsync();
    Task<OperationResult<GenericOperationResult, string>> CouponBookingAsync(int couponId);
    Task<OperationResult<GenericOperationResult>> ActivateCouponBookingAsync(string code);
    Task<OperationResult<GenericOperationResult>> DeActivateCouponBookingAsync();
    Task<OperationResult<GenericOperationResult>> PointsDeductionFromUserAsync(int userCouponId);
    Task<OperationResult<GenericOperationResult, IEnumerable<UserCouponDto>>> GetUserCouponsAsync(
        bool isBeneficiary, bool isActivate);
}

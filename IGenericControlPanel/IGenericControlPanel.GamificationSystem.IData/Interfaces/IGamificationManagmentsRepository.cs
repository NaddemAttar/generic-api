﻿

using CraftLab.Core.OperationResults;
using IGenericControlPanel.GamificationSystem.Dto.Gamification;

namespace IGenericControlPanel.GamificationSystem.IData.Interfaces;
public interface IGamificationManagmentsRepository
{
    Task<OperationResult<GenericOperationResult, IEnumerable<GamificationDto>>> GetAllGamificationSectionsAsync();
    Task<OperationResult<GenericOperationResult, IEnumerable<GamificationDto>>> UpdatePointsForGamificactionSectionsAsync(
        IEnumerable<GamificationDto> gamifications);
    Task<OperationResult<GenericOperationResult, GamificationDto>> AddNewGamificationSectionAsync(
        GamificationDto dto);
    Task<OperationResult<GenericOperationResult, IEnumerable<UserWithPointsDto>>> GetUsersWithTheirPointsAsync(
        FiltersUsersDto filtersDto);
}

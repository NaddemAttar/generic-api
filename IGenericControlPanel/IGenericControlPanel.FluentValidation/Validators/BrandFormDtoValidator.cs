﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FluentValidation;

using IGenericControlPanel.Core.Dto.Brand;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.WebApplication.Controllers;

using Microsoft.Extensions.Localization;

namespace IGenericControlPanel.FluentValidation.Validators
{
    public class BrandFormDtoValidator : AbstractValidator<BrandFormDto>
    {
        public BrandFormDtoValidator(IStringLocalizer<BrandController> FromResx)
        {
            RuleFor(b => b.Id).NotNull().WithMessage(FromResx[ErrorMessages.BrandIdMustBeNotNull]);
            RuleFor(b => b.Name).NotEmpty().WithMessage(FromResx[ErrorMessages.BrandNameRequired]);
        }
    }
}

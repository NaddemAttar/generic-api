﻿using CraftLab.Core.OperationResults;
using Google.Protobuf.WellKnownTypes;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Course;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.Model.HR;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Enums.Core;
using IGenericControlPanel.SharedKernal.Extension;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using IGenericControlPanel.SharedKernal.Utils;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class CourseRepository : BasicRepository<CPDbContext, Course>, ICourseRepository
    {
        #region Constructor
        private IUserRepository UserRepository { get; }
        private IGamificationMovementsRepository _gamificationMovementRepo { get; }

        public CourseRepository(
            CPDbContext context,
            IUserRepository UserRepository,
            IGamificationMovementsRepository gamificationMovementRepo) : base(context)
        {
            this.UserRepository = UserRepository;
            _gamificationMovementRepo = gamificationMovementRepo;
        }
        #endregion

        #region Action Course
        public async Task<OperationResult<GenericOperationResult>> CourseEnrollmentAsync(int CourseId)
        {
            var result = new OperationResult<GenericOperationResult>(GenericOperationResult.Failed);
            try
            {
                int userId = UserRepository.CurrentUserIdentityId();

                var course = await ValidEntities
                    .Where(entity => entity.IsValid == true && entity.Id == CourseId)
                    .SingleOrDefaultAsync();

                if (course == null)
                    return result.AddError(ErrorMessages.CourseNotFound).UpdateResultStatus(GenericOperationResult.NotFound);

                var userCourse = new UserCourse()
                {
                    UserId = userId,
                    CourseId = CourseId
                };

                await Context.UserCourses.AddAsync(userCourse);
                await Context.SaveChangesAsync();

                var savePointsResult = await _gamificationMovementRepo.SavePoints(new List<string>
                {
                        GamificationNames.BuyCourse
                });

                if (!savePointsResult.IsSuccess)
                {
                    return result.AddError(savePointsResult.ErrorMessages).UpdateResultStatus(GenericOperationResult.Failed);
                }

                result = new OperationResult<GenericOperationResult>(GenericOperationResult.Success);
            }
            catch
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
            return result;
        }
        public async Task<OperationResult<GenericOperationResult, CourseFormDto>> ActionAsync(CourseFormDto courseFormDto)
        {
            var result = courseFormDto.Id == 0
                ? await AddCourseAsync(courseFormDto)
                : await UpdateCourseAsync(courseFormDto);

            return result;
        }
        public async Task<OperationResult<GenericOperationResult, CourseFormDto>> AddCourseAsync(CourseFormDto courseFormDto)
        {
            OperationResult<GenericOperationResult, CourseFormDto> result = new(GenericOperationResult.Failed);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();
                Course courseEntity = new()
                {
                    UserId = userInfo.CurrentUserId,
                    Name = courseFormDto.Name,
                    Place = courseFormDto.Place,
                    Link = courseFormDto.Link,
                    StartDate = courseFormDto.StartDate,
                    Price = courseFormDto.Price,
                    EndDate = courseFormDto.EndDate,
                    Description = courseFormDto.Description,
                    PreRequirements = courseFormDto.PreRequirements,
                    Goals = courseFormDto.Goals,
                    Modules = courseFormDto.Modules,
                    YoutubeLink = courseFormDto.YoutubeLink,
                    IsHidden = courseFormDto.IsHidden,
                    LocationId = courseFormDto.LocationId,
                    Instructors = courseFormDto.InstructorIds
                    .Select(instructorId => new Instructor
                    {
                        TeamMemberId = instructorId
                    }).ToList(),
                    CourseTypeCourses = courseFormDto.CourseTypeIds
                    .Select(courseTypeId => new CourseTypeCourses
                    {
                        CourseTypeId = courseTypeId
                    }).ToList()
                };

                foreach (var attachment in courseFormDto.Attachments)
                {
                    var courseFile = new CourseFile()
                    {
                        Checksum = attachment.Checksum,
                        FileContentType = attachment.FileContentType,
                        Description = attachment.Description,
                        Extension = attachment.Extension,
                        FileName = attachment.FileName,
                        Name = attachment.Name,
                        Url = attachment.Url,
                    };
                    courseEntity.Files.Add(courseFile);
                }

                await Context.AddAsync(courseEntity);
                await Context.SaveChangesAsync();

                var savePointsResult = await _gamificationMovementRepo.SavePoints(new List<string>
                {
                    GamificationNames.AddCourse
                });

                if (!savePointsResult.IsSuccess)
                {
                    return result.AddError(savePointsResult.ErrorMessages).UpdateResultStatus(GenericOperationResult.Failed);
                }
                courseFormDto.Id = courseEntity.Id;
                return result.UpdateResultData(courseFormDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, CourseFormDto>> UpdateCourseAsync(CourseFormDto courseFormDto)
        {
            OperationResult<GenericOperationResult, CourseFormDto> result = new(GenericOperationResult.Failed);
            try
            {
                var courseModel = await Context.Courses.FirstOrDefaultAsync(c => c.Id == courseFormDto.Id);
                if (courseModel == null)
                    return result.AddError(ErrorMessages.CourseNotFound).UpdateResultStatus(GenericOperationResult.NotFound);

                if (courseFormDto.RemovedFileIds is not null)
                {
                    courseFormDto.RemoveFileUrls = Context.Files
                        .Where(fileEntity => courseFormDto.RemovedFileIds.Contains(fileEntity.Id))
                        .Select(fileEntity => fileEntity.Url)
                        .ToList();

                    Context.Files.RemoveRange(Context.Files
                        .Where(fileEntity => courseFormDto.RemovedFileIds.Contains(fileEntity.Id))
                        .ToList());
                }

                Context.CourseTypeCourses.RemoveRange(Context.CourseTypeCourses
                    .Where(data => data.IsValid && data.CourseId == courseFormDto.Id)
                    .ToList());

                Context.Instructors.RemoveRange(Context.Instructors
                    .Where(data => data.IsValid && data.CourseId == courseFormDto.Id)
                    .ToList());

                var createdBy = Context.Courses
                    .Where(entity => entity.IsValid && entity.Id == courseFormDto.Id)
                    .Select(entity => entity.CreatedBy)
                    .SingleOrDefault();

                Course updateCourseEntity = new()
                {
                    Id = courseFormDto.Id,
                    IsValid = true,
                    Name = courseFormDto.Name,
                    Link = courseFormDto.Link,
                    StartDate = courseFormDto.StartDate,
                    EndDate = courseFormDto.EndDate,
                    Price = courseFormDto.Price,
                    Description = courseFormDto.Description,
                    PreRequirements = courseFormDto.PreRequirements,
                    Goals = courseFormDto.Goals,
                    Modules = courseFormDto.Modules,
                    YoutubeLink = courseFormDto.YoutubeLink,
                    IsHidden = courseFormDto.IsHidden,
                    LocationId = courseFormDto.LocationId,
                    CreatedBy = createdBy,
                    Instructors = courseFormDto.InstructorIds
                    .Select(instructorId => new Instructor
                    {
                        TeamMemberId = instructorId
                    }).ToList(),
                    CourseTypeCourses = courseFormDto.CourseTypeIds
                   .Select(courseTypeId => new CourseTypeCourses
                   {
                       CourseTypeId = courseTypeId
                   }).ToList()
                };

                foreach (var attachment in courseFormDto.Attachments)
                {
                    CourseFile courseFile = new()
                    {
                        Checksum = attachment.Checksum,
                        FileContentType = attachment.FileContentType,
                        Description = attachment.Description,
                        Extension = attachment.Extension,
                        FileName = attachment.FileName,
                        Name = attachment.Name,
                        Url = attachment.Url,
                    };
                    updateCourseEntity.Files.Add(courseFile);
                }

                _ = Context.Update(updateCourseEntity);
                _ = await Context.SaveChangesAsync();

                return result.UpdateResultData(courseFormDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        #endregion

        #region Get Courses With Pagination
        public async Task<OperationResult<GenericOperationResult, IEnumerable<CourseDto>>> GetCoursesWithPaginationAsync(
            int locationId, IEnumerable<int> courseTypeIds, bool MyCourses, IEnumerable<int> instructorIds,
            string courseName, int? pageNumber, int? pageSize, int userId, bool? hiddenCourse = null)
        {
            OperationResult<GenericOperationResult, IEnumerable<CourseDto>> result = new(GenericOperationResult.Failed);
            try
            {
                var currentUserId = UserRepository.CurrentUserIdentityId();
                var userInfo = await UserRepository.GetCurrentUserInfo();
                var builder = WebApplication.CreateBuilder();
                bool multiProviders = bool.Parse(builder.Configuration.GetSection("MultiProviders").Value.ToLower());
                var coursesModel = await ValidEntities
                    .Include(entity => entity.CourseTypeCourses)
                    .Include(entity => entity.Offer)
                    .Include(entity => entity.Files)
                    .Include(entity => entity.Location)
                    .Include(entity => entity.UserCourses.Where(uc => uc.IsValid)).ThenInclude(uc => uc.User)
                    .Where(entity =>
                    (userInfo.CurrentUserId == 0 || userInfo.IsUserAdmin || (multiProviders == false ? true : userInfo.IsHealthCareProvider ? userInfo.CurrentUserId == entity.CreatedBy : true))
                    && entity.IsValid
                    && (hiddenCourse != null ? entity.IsHidden == hiddenCourse.Value : true)
                    && (userId != 0 ? entity.UserCourses.Any(pc => pc.IsValid && pc.UserId == userId) : true)
                    && (MyCourses == true ? entity.UserCourses.Any(pc => pc.IsValid && pc.UserId == currentUserId) : true)
                    && (instructorIds.Count() != 0 ?
                     entity.Instructors.Any(instructor => instructor.IsValid && instructorIds.Contains(instructor.TeamMemberId)) : true)
                    && (courseTypeIds.Count() != 0 ?
                     entity.CourseTypeCourses.Any(courseType => courseType.IsValid && courseTypeIds.Contains(courseType.CourseTypeId)) : true)
                    && (locationId != 0 ?
                     entity.LocationId == locationId : true)
                    && (!string.IsNullOrEmpty(courseName) ? entity.Name.Contains(courseName) : true))
                    .OrderByDescending(courseEntity => courseEntity.CreationDate)
                    .Skip(pageNumber.Value * pageSize.Value)
                    .Take(pageSize.Value)
                    .ToListAsync();
                var coursesDto = new List<CourseDto>();
                foreach (var course in coursesModel)
                {
                    var courseDto = new CourseDto();
                    courseDto.Id = course.Id;
                    courseDto.Name = course.Name;
                    courseDto.Place = course.Place;
                    courseDto.Link = course.Link;
                    courseDto.StartDate = course.StartDate;
                    courseDto.EndDate = course.EndDate;
                    courseDto.EndDateFormat = course.StartDate.ConvertDateToString(DateFormat.Year_Month_Day);
                    courseDto.EndDateFormat = course.EndDate.ConvertDateToString(DateFormat.Year_Month_Day);
                    courseDto.Description = course.Description;
                    courseDto.PreRequirements = course.PreRequirements;
                    courseDto.Price = course.Price;
                    if (course.OfferId != null && courseDto.NewPrice == null && DateTime.Now >= course.Offer.StartDate && course.Offer.EndDate >= DateTime.Now && course.Offer.IsValid)
                    {
                        //see it
                        courseDto.NewPrice =
                            (course.Offer.Value != null) ?
                            (course.Price - course.Offer.Value) :
                            (course.Price - (course.Price * (course.Offer.Percentage / 100.0)));
                        courseDto.EndOfferDate = courseDto.NewPrice != null ? course.Offer.EndDate : null;
                    }
                    else if (courseDto.NewPrice == null)
                    {
                        //esle see the all
                        var offer = await Context.Offers.FirstOrDefaultAsync(o => o.CreatedBy == course.CreatedBy && o.OfferType == 0 && (int)o.OfferFor == 1 && DateTime.Now >= o.StartDate && o.EndDate >= DateTime.Now && o.IsValid);
                        if (offer != null)
                        {
                            courseDto.NewPrice =
                            (offer.Value != null) ?
                            (course.Price - offer.Value) :
                            (course.Price - (course.Price * (offer.Percentage / 100.0)));
                            courseDto.EndOfferDate = courseDto.NewPrice != null ? offer.EndDate : null;
                        }
                    }
                    courseDto.Goals = course.Goals;
                    courseDto.Modules = course.Modules;
                    courseDto.YoutubeLink = course.YoutubeLink;
                    courseDto.LocationName = course.Location != null ? course.Location.Name : "";
                    courseDto.IsHidden = course.IsHidden;
                    courseDto.StudentsCount = course.UserCourses.Where(pc => pc.Course.IsValid).Count();
                    courseDto.IsEnrolled = course.UserCourses.Any(pc => currentUserId != 0 && pc.IsValid && pc.UserId == currentUserId);
                    courseDto.ImageUrl = course.Files
                    .Where(courseEntity => courseEntity.IsValid)
                    .Select(courseEntity => courseEntity.Url).FirstOrDefault();
                    courseDto.CourseTypes = Context.CourseTypeCourses
                        .Where(data => data.IsValid && data.CourseId == course.Id)
                        .Select(data => new CourseTypeDto
                        {
                            Id = data.CourseType.Id,
                            Name = data.CourseType.Name
                        }).ToList();
                    courseDto.Students = course.UserCourses.Select(uc => new UserCourseDto
                    {
                        PersonId = uc.UserId.Value,
                        PersonName = uc.User.FirstName + " " + uc.User.LastName,
                    }).ToList();
                    coursesDto.Add(courseDto);
                }
                //.Select(courseEntity => new CourseDto()
                //{
                //    Id = courseEntity.Id,
                //    Name = courseEntity.Name,
                //    Place = courseEntity.Place,
                //    Link = courseEntity.Link,
                //    StartDate = courseEntity.StartDate,
                //    StartDateFormat = courseEntity.StartDate.ConvertDateToString(DateFormat.Year_Month_Day),
                //    EndDate = courseEntity.EndDate,
                //    EndDateFormat = courseEntity.EndDate.ConvertDateToString(DateFormat.Year_Month_Day),
                //    Description = courseEntity.Description,
                //    PreRequirements = courseEntity.PreRequirements,
                //    Price = courseEntity.Price,
                //    NewPrice = (courseEntity.OfferId != null) ?
                //                (courseEntity.Offer.Value != null) ?
                //                (courseEntity.Price - courseEntity.Offer.Value) :
                //                (courseEntity.Price - (courseEntity.Price * (courseEntity.Offer.Percentage / 100.0))) : null,
                //    IsOffer = (courseEntity.OfferId != null) ? true : false,
                //    EndOfferDate = courseEntity.Offer.EndDate,
                //    Goals = courseEntity.Goals,
                //    Modules = courseEntity.Modules,
                //    YoutubeLink = courseEntity.YoutubeLink,
                //    LocationName = courseEntity.Location != null ? courseEntity.Location.Name : "",
                //    IsHidden = courseEntity.IsHidden,
                //    StudentsCount = courseEntity.UserCourses.Where(pc => pc.Course.IsValid).Count(),
                //    IsEnrolled = courseEntity.UserCourses.Any(pc => currentUserId != 0 && pc.IsValid && pc.UserId == currentUserId),
                //    ImageUrl = courseEntity.Files
                //    .Where(courseEntity => courseEntity.IsValid)
                //    .Select(courseEntity => courseEntity.Url).FirstOrDefault(),
                //    CourseTypes = Context.CourseTypeCourses
                //        .Where(data => data.IsValid && data.CourseId == courseEntity.Id)
                //        .Select(data => new CourseTypeDto
                //        {
                //            Id = data.CourseType.Id,
                //            Name = data.CourseType.Name
                //        }).ToList(),
                //}).ToList();

                result.UpdateResultData(coursesDto).UpdateResultStatus(GenericOperationResult.Success);
                return result;
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }

        }

        #endregion

        #region Get Course Details
        public async Task<OperationResult<GenericOperationResult, CourseDetailsDto>> GetDetailedAsync(
            int id, string cultureCode = "ar")
        {
            var result = new OperationResult<GenericOperationResult, CourseDetailsDto>(GenericOperationResult.Failed);

            var currentUserId = UserRepository.CurrentUserIdentityId();
            try
            {
                var courseModel = await ValidEntities
                    .Include(c => c.Offer)
                    .Include(entity => entity.Files)
                    .Include(entity => entity.Location)
                    .Include(entity => entity.Instructors).ThenInclude(inst => inst.TeamMember).ThenInclude(tm => tm.Image)
                    .Include(entity => entity.UserCourses).ThenInclude(entity => entity.User)
                    .FirstOrDefaultAsync(c => c.Id == id);

                if (courseModel == null)
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.CourseNotFound);

                var courseDto = new CourseDetailsDto();
                courseDto.Id = courseModel.Id;
                courseDto.Name = courseModel.Name;
                courseDto.Place = courseModel.Place;
                courseDto.Price = courseModel.Price;
                if (courseModel.OfferId != null && courseDto.NewPrice == null && DateTime.Now >= courseModel.Offer.StartDate && courseModel.Offer.EndDate >= DateTime.Now && courseModel.Offer.IsValid)
                {
                    //see it
                    courseDto.NewPrice =
                        (courseModel.Offer.Value != null) ?
                        (courseModel.Price - courseModel.Offer.Value) :
                        (courseModel.Price - (courseModel.Price * (courseModel.Offer.Percentage / 100.0)));
                    courseDto.EndOfferDate = courseDto.NewPrice != null ? courseModel.Offer.EndDate : null;
                }
                else if (courseDto.NewPrice == null)
                {
                    //esle see the all
                    var offer = await Context.Offers.FirstOrDefaultAsync(o => o.CreatedBy == courseModel.CreatedBy && o.OfferType == 0 && (int)o.OfferFor == 1 && DateTime.Now >= o.StartDate && o.EndDate >= DateTime.Now && o.IsValid);
                    if (offer != null)
                    {
                        courseDto.NewPrice =
                        (offer.Value != null) ?
                        (courseModel.Price - offer.Value) :
                        (courseModel.Price - (courseModel.Price * (offer.Percentage / 100.0)));
                        courseDto.EndOfferDate = courseDto.NewPrice != null ? offer.EndDate : null;
                    }
                }
                courseDto.Goals = courseModel.Goals;
                courseDto.StartDate = courseModel.StartDate;
                courseDto.EndDate = courseModel.EndDate;
                courseDto.Modules = courseModel.Modules;
                courseDto.Description = courseModel.Description;
                courseDto.YoutubeLink = courseModel.YoutubeLink;
                courseDto.PreRequirements = courseModel.PreRequirements;
                courseDto.LocationName = courseModel.Location != null ? courseModel.Location.Name : "";
                courseDto.IsHidden = courseModel.IsHidden;
                courseDto.StudentsCount = courseModel.UserCourses.Where(pc => pc.Course.IsValid).Count();
                courseDto.Instructors = courseModel.Instructors
                   .Where(data => data.IsValid)
                   .Select(data => new InstructorDto
                   {
                       Id = data.TeamMember.Id,
                       Name = data.TeamMember.FullName,
                       About = data.TeamMember.About,
                       Biography = data.TeamMember.Biography,
                       PhoneNumber = data.TeamMember.PhoneNumber,
                       Email = data.TeamMember.Email,
                       Description = data.TeamMember.Description,
                       Position = data.TeamMember.Position,
                       ImageUrl = data.TeamMember.Image.Url
                   }).ToList();
                courseDto.IsEnrolled = courseModel.UserCourses.Any(pc => currentUserId != 0 && pc.IsValid && pc.UserId == currentUserId);
                courseDto.Attachments = courseModel.Files.Where(fileEntity => fileEntity.IsValid)
                .Select(fileEntity => new FileDetailsDto()
                {
                    Id = fileEntity.Id,
                    Extension = fileEntity.Extension,
                    FileName = fileEntity.FileName,
                    FileContentType = fileEntity.FileContentType,
                    Url = fileEntity.Url,
                    SourceName = fileEntity.SourceName,
                    Checksum = fileEntity.Checksum,
                    Description = fileEntity.Description,
                    Name = fileEntity.Name,
                }).ToList();
                courseDto.ImageUrl = courseModel.Files
                .Where(courseEntity => courseEntity.IsValid)
                .Select(courseEntity => courseEntity.Url).FirstOrDefault();
                courseDto.CourseTypes = Context.CourseTypeCourses
                    .Where(data => data.IsValid && data.CourseId == courseModel.Id)
                    .Select(data => new CourseTypeDto
                    {
                        Id = data.CourseType.Id,
                        Name = data.CourseType.Name
                    }).ToList();
                //.Select(courseEntity => new CourseDetailsDto()
                //{
                //    Id = courseEntity.Id,
                //    Name = courseEntity.Name,
                //    Place = courseEntity.Place,
                //    Price = courseEntity.Price,
                //    NewPrice = (courseEntity.OfferId != null) ?
                //             (courseEntity.Offer.Value != null) ?
                //             (courseEntity.Price - courseEntity.Offer.Value) :
                //             (courseEntity.Price - (courseEntity.Price * (courseEntity.Offer.Percentage / 100.0))) : null,
                //    //IsOffer = (courseEntity.OfferId != null) ? true : false,
                //    EndOfferDate = courseEntity.Offer.EndDate,
                //    Link = courseEntity.Link,
                //    StartDate = courseEntity.StartDate,
                //    EndDate = courseEntity.EndDate,
                //    Description = courseEntity.Description,
                //    PreRequirements = courseEntity.PreRequirements,
                //    IsEnrolled = currentUserId == 0 ? false : courseEntity.UserCourses.Any(c => c.UserId == currentUserId && c.IsValid),
                //    Goals = courseEntity.Goals,
                //    Modules = courseEntity.Modules,
                //    YoutubeLink = courseEntity.YoutubeLink,
                //    IsHidden = courseEntity.IsHidden,
                //    Instructors = Context.Instructors
                //    .Where(data => data.IsValid && data.CourseId == id)
                //    .Select(data => new InstructorDto
                //    {
                //        Id = data.TeamMember.Id,
                //        Name = data.TeamMember.FullName,
                //        About = data.TeamMember.About,
                //        Biography = data.TeamMember.Biography,
                //        PhoneNumber = data.TeamMember.PhoneNumber,
                //        Email = data.TeamMember.Email,
                //        Description = data.TeamMember.Description,
                //        Position = data.TeamMember.Position,
                //        ImageUrl = data.TeamMember.Image.Url
                //    }).ToList(),
                //    Location = courseEntity.Location != null ? new LocationDto
                //    {
                //        Id = courseEntity.Location.Id,
                //        Name = courseEntity.Location.Name
                //    } : null,
                //    CourseTypes = Context.CourseTypeCourses
                //    .Where(data => data.IsValid && data.CourseId == id)
                //    .Select(data => new CourseTypeDto
                //    {
                //        Id = data.CourseType.Id,
                //        Name = data.CourseType.Name
                //    }).ToList(),
                //    NormalUsers = courseEntity.UserCourses.Select(personCourse => new NormalUserForBladesDto
                //    {
                //        UserName = personCourse.User.UserName,
                //        Email = personCourse.User.Email,
                //        PhoneNumber = personCourse.User.PhoneNumber,
                //        PaymentStatus = personCourse.PaymentStatus
                //    }).ToList(),
                //    Attachments = courseEntity.Files.Where(fileEntity => fileEntity.IsValid)
                // .Select(fileEntity => new FileDetailsDto()
                // {
                //     Id = fileEntity.Id,
                //     Extension = fileEntity.Extension,
                //     FileName = fileEntity.FileName,
                //     FileContentType = fileEntity.FileContentType,
                //     Url = fileEntity.Url,
                //     SourceName = fileEntity.SourceName,
                //     Checksum = fileEntity.Checksum,
                //     Description = fileEntity.Description,
                //     Name = fileEntity.Name,
                // }).ToList()
                //}).SingleOrDefault();
                return result.UpdateResultData(courseDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Remove & Hide & Show Course By Id
        public async Task<OperationResult<GenericOperationResult>> RemoveAsync(IEnumerable<int> ids)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.Failed);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();
                var data = await Context.Courses
                    .Where(entity => entity.IsValid && ids.Contains(entity.Id)
                    && (userInfo.IsUserAdmin || userInfo.CurrentUserId == entity.CreatedBy))
                    .ToListAsync();

                if (data.Count != ids.Count())
                    return result.AddError(ErrorMessages.CourseNotFound).UpdateResultStatus(GenericOperationResult.NotFound);

                Context.RemoveRange(data);
                _ = await Context.SaveChangesAsync();
                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult>> HideCourseByIdAsync(int courseId, bool isHidden)
        {
            var result = new OperationResult<GenericOperationResult>(GenericOperationResult.Failed);

            try
            {
                var courseEntity = await Context.Courses
                    .Where(entity => entity.IsValid && entity.Id == courseId)
                    .SingleOrDefaultAsync();

                if (courseEntity == null)
                    return result.AddError(ErrorMessages.CourseNotFound).UpdateResultStatus(GenericOperationResult.NotFound);

                courseEntity.IsHidden = isHidden;
                _ = await Context.SaveChangesAsync();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, IEnumerable<CourseDto>>> GetCoursesSelectAsync()
        {
            var result = new OperationResult<GenericOperationResult, IEnumerable<CourseDto>>(GenericOperationResult.Failed);

            try
            {
                var courseList = await ValidEntities.Select(entity => new CourseDto()
                {
                    Id = entity.Id,
                    Name = entity.Name
                }).ToListAsync();

                return result.UpdateResultData(courseList).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult>> ActionCoursePaymentAsync(
            int userId, int courseId, PaymentStatus paymentStatus)
        {
            var result = new OperationResult<GenericOperationResult>(
                GenericOperationResult.ValidationError);

            try
            {
                var personCourse = new UserCourse()
                {
                    CourseId = courseId,
                    UserId = userId,
                    PaymentStatus = paymentStatus
                };
                _ = await Context.AddAsync(personCourse);
                _ = await Context.SaveChangesAsync();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        #endregion
    }
}

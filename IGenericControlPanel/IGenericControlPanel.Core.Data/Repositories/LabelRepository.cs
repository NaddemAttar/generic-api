﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.IData;
using IGenericControlPanel.Model.Content;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Repository;

using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class LabelRepository : BasicRepository<CPDbContext, Label>, ILabelRepository<string, string>
    {
        #region Properties

        private IUserLanguageRepository UserLanguageRepository { get; }

        #endregion

        #region Constructors

        public LabelRepository(CPDbContext dbContext, IMapper mapper, IUserLanguageRepository userLanguageRepository) : base(dbContext, mapper)
        {
            UserLanguageRepository = userLanguageRepository;
        }
        #endregion

        public IList<string> GetLabelsKeys { get; set; } = new List<string>() // For CCS Project
        {
               LabelsKeys.ServiceTitle,
               LabelsKeys.ServiceDescription,
               LabelsKeys.LearnTitle,
               LabelsKeys.LearnDescription,
               LabelsKeys.FacultyTitle,
               LabelsKeys.FacultyDescription,
               LabelsKeys.HassleTitle,
               LabelsKeys.HassleDescription,
               LabelsKeys.HealthHubTitle,
               LabelsKeys.HealthHubDescription
        };

        public string Get(string key)
        {
            string preproccessedKey = key.Trim();
            return ValidEntities
                .SingleOrDefault(entity => entity.Key == preproccessedKey)?.Text;

        }
        public TCustomReturn Get<TCustomReturn>(string key) where TCustomReturn : struct, IConvertible
        {
            if (key is null)
            {
                throw new ArgumentNullException("key", "The parameter key cannot be null");
            }

            string preproccessedKey = key?.Trim();
            string result = ValidEntities
                .SingleOrDefault(entity => entity.Key == preproccessedKey)?.Text;

            if (result == null)
            {
                return default;
            }

            try
            {
                return (TCustomReturn)Convert.ChangeType(result, typeof(TCustomReturn));
            }
            catch
            {
                return default;
            }
        }
        public KeyValuePair<string, string> GetPair(string key)
        {
            if (key is null)
            {
                throw new ArgumentNullException("key", "The parameter key cannot be null");
            }
            string preproccessedKey = key.Trim();
            return ValidEntities
                .Where(entity => entity.Key == preproccessedKey)
                .ToDictionary(name => name.Key, value => value.Text)
                .SingleOrDefault();
        }

        public IEnumerable<string> GetAll()
        {
            return ValidEntities
                .Select(entity => entity.Key).ToList();
        }

        public IEnumerable<string> GetSet(IEnumerable<string> keys)
        {
            if (keys is null)
            {
                throw new ArgumentNullException("keys", "The parameter keys cannot be null");
            }

            IEnumerable<string> preproccessedKey = keys.Select(key => key.Trim());
            return ValidEntities
                .Where(entity => preproccessedKey.Any(name => name == entity.Key))
                .Select(entity => entity.Text);
        }

        public IDictionary<string, string> GetAllAsDictionary()
        {
            return ValidEntities
                .ToDictionary(name => name.Key, value => value.Text);
        }

        public IDictionary<string, string> GetSetAsDictionary(IEnumerable<string> keys)
        {
            if (keys is null)
            {
                throw new ArgumentNullException("keys", "The parameter keys cannot be null");
            }

            IEnumerable<string> preproccessedKey = keys.Select(key => key.Trim());
            return ValidEntities
                .Where(entity => preproccessedKey.Any(name => name == entity.Key))
                .ToDictionary(name => name.Key, value => value.Text);
        }
        public IDictionary<string, string> GetTranslationsSetAsDictionary(IEnumerable<string> keys)
        {
            if (keys is null)
            {
                throw new ArgumentNullException("keys", "The parameter keys cannot be null");
            }

            IEnumerable<string> preproccessedKey = keys.Select(key => key.Trim());
            return GetValidEntities<LabelTranslation>()
                .Where(entity => preproccessedKey.Any(name => name == entity.LabelKey))
                .ToDictionary(name => name.LabelKey, value => value.Text);
        }

        public string GetBaseDirectoryUrl()
        {
            return "data";
        }

        public OperationResult<GenericOperationResult, LabelDto> UpdateOneLabel(LabelDto formDto)
        {
            try
            {
                Label labelSet = ValidEntities
                         .Where(label => formDto.Key == label.Key).SingleOrDefault();

                labelSet.Text = formDto.Text;

                EntityState entityState = EntityState.Modified;

                Context.Entry(labelSet).State = entityState;

                _ = Context.SaveChanges();

                return new OperationResult<GenericOperationResult, LabelDto>(GenericOperationResult.Success, formDto, null);
            }
            catch (Exception ex)
            {
                return new OperationResult<GenericOperationResult, LabelDto>(GenericOperationResult.Failed, null, ex);
            }
        }

        public IEnumerable<FileDetailsDto> GetFiles()
        {
            Label data = ValidEntities
                 .Include(entity => entity.Files)
                 .Where(entity => entity.Files.SingleOrDefault().IsValid && entity.Key == "Images")
                 .SingleOrDefault();

            data ??= new Label();

            IEnumerable<int> fileIds = data.Files.Select(file => file.Id);

            List<FileDetailsDto> result = Context.Files.Where(img => fileIds.Contains(img.Id))
                 .Select(e => new FileDetailsDto()
                 {
                     Url = e.Url,
                     FileContentType = e.FileContentType
                 }).ToList();

            return result;
        }

        public OperationResult<GenericOperationResult, LabelDto> SetLabel(LabelDto formDto)
        {
            OperationResult<GenericOperationResult, LabelDto> result = new(GenericOperationResult.Failed);

            Label labelEntity = ValidEntities.Where(entity => entity.Key == formDto.Key).SingleOrDefault();

            //int websitePageId = Context.WebsitePages.Where(label => label.IsValid).FirstOrDefault().Id;

            if (labelEntity != null)
            {
                return result;
            }

            LabelDto data = null;

            try
            {
                Label newLabelSet = new()
                {
                    Key = formDto.Key,
                    Text = formDto.Text,
                    CultureCode = "ar-SY",
                    IsValid = true
                };

                _ = Context.Add(newLabelSet);
                _ = Context.SaveChanges();

                data = new LabelDto()
                {
                    Key = formDto.Key,
                    Text = formDto.Text
                };

            }
            catch (Exception)
            {
                return result;
            }
            return result = new OperationResult<GenericOperationResult, LabelDto>(GenericOperationResult.Success, data);
        }

        public IEnumerable<LabelDto> GetAllLabels()
        {
            return ValidEntities
                .Where(entity => entity.IsValid)
                .Select(entity => new LabelDto
                {
                    Key = entity.Key,
                    Text = entity.Text
                }).ToList();
        }
    }

}


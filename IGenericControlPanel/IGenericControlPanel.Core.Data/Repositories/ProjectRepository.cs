﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;

using CraftLab.Core.Infrastructure;
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Project;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Projects;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using IGenericControlPanel.SharedKernal.Utils;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class ProjectRepository : BasicRepository<CPDbContext, Project>, IProjectRepository
    {
        #region Properties
        private IUserRepository UserRepository { get; }
        private IGamificationMovementsRepository _gamificationMovementRepo { get; }
        public ProjectRepository(
            CPDbContext context,
            IMapper mapper,
            IUserRepository userRepository,
            IGamificationMovementsRepository gamificationMovementRepo
            ) : base(context, mapper)
        {
            UserRepository = userRepository;
            _gamificationMovementRepo = gamificationMovementRepo;
        }
        #endregion

        #region Add New Project
        public async Task<OperationResult<GenericOperationResult, ProjectFormDto>> AddProject(
            ProjectFormDto projectFormDto)
        {
            var result = new OperationResult<GenericOperationResult,
                ProjectFormDto>(GenericOperationResult.Success);

            try
            {
                var entity = CreateProject(projectFormDto);

                if (entity is null)
                {
                    return result.AddError(ErrorMessages.InternalServerError)
                        .UpdateResultStatus(GenericOperationResult.Failed);
                }

                await Context.AddAsync(entity);
                await Context.SaveChangesAsync();

                var savePointsResult = await _gamificationMovementRepo.SavePoints(new List<string>
                {
                    GamificationNames.AddProject
                });

                if (!savePointsResult.IsSuccess)
                {
                    return result.AddError(savePointsResult.ErrorMessages)
                        .UpdateResultStatus(GenericOperationResult.Failed);
                }

                return result.UpdateResultData(projectFormDto);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                        .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public Project CreateProject(ProjectFormDto projectFormDto)
        {
            try
            {
                var entity = new Project()
                {
                    Title = projectFormDto.Title,
                    Description = projectFormDto.Description,
                    YoutubeLink = projectFormDto.YoutubeLink,
                    ProjectKeys = projectFormDto.Keys.Select(projectKey => new ProjectKey
                    {
                        KeyName = projectKey
                    }).ToList(),
                    ProjectComponents = projectFormDto.Components.Select(projectComponent => new ProjectComponent
                    {
                        Title = projectComponent.Title,
                        Description = projectComponent.Description,
                        ProjectComponentImageUrl = projectComponent.ComponentImageUrl
                    }).ToList(),
                    ProjectEditions = projectFormDto.Editions.Select(edition => new ProjectEdition
                    {
                        Title = edition.Title,
                        Description = edition.Description,
                        Link = edition.Link
                    }).ToList()
                };

                entity = AddingNewImagesToProject(entity, projectFormDto.Images);

                return entity;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region Update Project
        public OperationResult<GenericOperationResult, ProjectFormDto> UpdateProject(ProjectFormDto projectFormDto)
        {
            var result = new OperationResult<GenericOperationResult,
                ProjectFormDto>(GenericOperationResult.Success);

            try
            {
                if (projectFormDto.RemovedFileIds is not null)
                {
                    projectFormDto.FileUrlsMustRemoveItFromServer =
                        RemoveProjectFiles(projectFormDto.RemovedFileIds);
                }

                var entity = GetProjectEntity(projectFormDto.Id);

                if (entity is null)
                {
                    return result.AddError(ErrorMessages.ProjectNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                RemoveOldProjectKeys(entity);
                RemoveProjectComponents(entity, projectFormDto.RemoveComponentIds);
                RemoveProjectEditions(entity, projectFormDto.RemoveEditionIds);
                entity.Title = projectFormDto.Title;
                entity.Description = projectFormDto.Description;
                entity.YoutubeLink = projectFormDto.YoutubeLink;
                var ProjectKeys = projectFormDto.Keys
                    .Select(projectKey => new ProjectKey
                    {
                        ProjectId = projectFormDto.Id,
                        KeyName = projectKey,
                        IsValid = true
                    }).ToList();

                entity = AddingNewImagesToProject(entity, projectFormDto.Images);

                foreach (var projectKey in ProjectKeys)
                {
                    entity.ProjectKeys.Add(projectKey);
                }

                if (projectFormDto.Editions != null)
                {
                    entity = UpdateProjectEditions(entity, projectFormDto.Editions);
                }
                if (projectFormDto.Components != null)
                {
                    entity = UpdateProjectComponents(entity, projectFormDto.Components);
                }
                Context.Update(entity);
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
            result.Result = projectFormDto;
            return result;
        }
        public Project UpdateProjectEditions(Project entity,
            IEnumerable<EditionDto> editions)
        {
            Parallel.ForEach(editions, edition =>
            {
                if (edition.Id == 0)
                {
                    var projectEdition = new ProjectEdition()
                    {
                        Title = edition.Title,
                        Description = edition.Description,
                        Link = edition.Link
                    };
                    entity.ProjectEditions.Add(projectEdition);
                }
                else
                {
                    var existingProjectEdition = entity.ProjectEditions
                            .FirstOrDefault(pe => pe.Id == edition.Id);
                    existingProjectEdition.Title = edition.Title;
                    existingProjectEdition.Description = edition.Description;
                    existingProjectEdition.Link = edition.Link;
                    Context.Update(existingProjectEdition);
                }
            });
            //foreach (var edition in editions)
            //{
            //    if (edition.Id == 0)
            //    {
            //        var projectEdition = new ProjectEdition()
            //        {
            //            Title = edition.Title,
            //            Description = edition.Description,
            //            Link = edition.Link
            //        };
            //        entity.ProjectEditions.Add(projectEdition);
            //    }
            //    else
            //    {
            //        var existingProjectEdition = entity.ProjectEditions
            //                .FirstOrDefault(pe => pe.Id == edition.Id);
            //        existingProjectEdition.Title = edition.Title;
            //        existingProjectEdition.Description = edition.Description;
            //        existingProjectEdition.Link = edition.Link;
            //        Context.Update(existingProjectEdition);
            //    }
            //}
            return entity;
        }
        public Project UpdateProjectComponents(Project entity,
            IEnumerable<ComponentDto> components)
        {
            Parallel.ForEach(components, component =>
            {
                if (component.Id == 0)
                {
                    var projectComponent = new ProjectComponent()
                    {
                        Title = component.Title,
                        Description = component.Description,
                        ProjectComponentImageUrl = component.ComponentImageUrl
                    };
                    entity.ProjectComponents.Add(projectComponent);
                }
                else
                {
                    var existingProjectComponent = entity.ProjectComponents
                            .FirstOrDefault(pc => pc.Id == component.Id);
                    existingProjectComponent.Title = component.Title;
                    existingProjectComponent.Description = component.Description;
                    if (component.ComponentImageUrl != null)
                    {
                        existingProjectComponent.ProjectComponentImageUrl = component.ComponentImageUrl;
                    }
                    Context.Update(existingProjectComponent);
                }
            });

            //foreach (var component in components)
            //{
            //    if (component.Id == 0)
            //    {
            //        var projectComponent = new ProjectComponent()
            //        {
            //            Title = component.Title,
            //            Description = component.Description,
            //            ProjectComponentImageUrl = component.ComponentImageUrl
            //        };
            //        entity.ProjectComponents.Add(projectComponent);
            //    }
            //    else
            //    {
            //        var existingProjectComponent = entity.ProjectComponents
            //                .FirstOrDefault(pc => pc.Id == component.Id);
            //        existingProjectComponent.Title = component.Title;
            //        existingProjectComponent.Description = component.Description;
            //        if (component.ComponentImageUrl != null)
            //        {
            //            existingProjectComponent.ProjectComponentImageUrl = component.ComponentImageUrl;
            //        }
            //        Context.Update(existingProjectComponent);
            //    }
            //}
            return entity;
        }
        public void RemoveProjectComponents(Project entity, IEnumerable<int> projectComponentIds)
        {
            if (projectComponentIds is not null)
            {
                Context.RemoveRange(entity.ProjectComponents
                  .Where(pc => projectComponentIds.Contains(pc.Id))
                  .ToList());
            }
        }

        public void RemoveProjectEditions(Project entity, IEnumerable<int> projectEditionIds)
        {
            if (projectEditionIds is not null)
            {
                Context.RemoveRange(entity.ProjectEditions
                       .Where(pe => projectEditionIds.Contains(pe.Id))
                       .ToList());
            }
        }
        public IEnumerable<string> RemoveProjectFiles(IEnumerable<int> removedFileIds)
        {
            var fileUrlsMustRemoveItFromServer = Context.Files
                        .Where(fileEntity => removedFileIds.Contains(fileEntity.Id))
                        .Select(fileEntity => fileEntity.Url)
                        .ToList();

            Context.Files.RemoveRange(Context.Files
                        .Where(fileEntity => removedFileIds.Contains(fileEntity.Id))
                        .ToList());

            return fileUrlsMustRemoveItFromServer;
        }
        public void RemoveOldProjectKeys(Project entity)
        {
            Context.RemoveRange(entity.ProjectKeys
               .Where(pk => pk.IsValid && pk.ProjectId == entity.Id)
               .ToList());
        }
        public Project GetProjectEntity(int projectId)
        {
            return Context.Projects
                   .Include(entity => entity.ProjectKeys)
                   .Include(entity => entity.ProjectEditions)
                   .Include(entity => entity.ProjectComponents)
                   .Include(entity => entity.ProjectFiles)
                   .FirstOrDefault(p => p.Id == projectId);
        }

        #endregion

        #region Adding New Images To Project
        public Project AddingNewImagesToProject(Project entity, IEnumerable<FileDetailsDto> images)
        {
            Parallel.ForEach(images, image =>
            {
                var file = new ProjectFile
                {
                    Checksum = image.Checksum,
                    FileContentType = image.FileContentType,
                    Description = image.Description,
                    Extension = image.Extension,
                    FileName = image.FileName,
                    Name = image.Name,
                    Url = image.Url,
                };
                entity.ProjectFiles.Add(Mapper.Map<ProjectFile>(file));
            });
            return entity;
        }
        #endregion

        #region Get All Projects With Pagination
        public async Task<OperationResult<GenericOperationResult, IPagedList<ProjectDto>>> GetAllProjectsWithPagination(
            int? pageNumber = 0, int? pageSize = 6)
        {
            var result = new OperationResult<GenericOperationResult,
                IPagedList<ProjectDto>>(GenericOperationResult.Success);

            try
            {
                var data = await GetAllProjectsWithPaginationFromServer(pageNumber, pageSize);

                var pageData = new PagedList<ProjectDto>(data, pageNumber.Value, pageSize.Value);

                return result.UpdateResultData(pageData);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public async Task<IEnumerable<ProjectDto>> GetAllProjectsWithPaginationFromServer(int? pageNumber = 0, int? pageSize = 6)
        {
            var userInfo = await UserRepository.GetCurrentUserInfo();

            return ValidEntities
                .Skip(pageNumber.Value * pageSize.Value)
                .Take(pageSize.Value)
                .Where(entity => userInfo.CurrentUserId == 0 || userInfo.IsUserAdmin
                || (userInfo.IsHealthCareProvider ? userInfo.CurrentUserId == entity.CreatedBy : true))
                .Select(entity => new ProjectDetailsDto
                {
                    Id = entity.Id,
                    Title = entity.Title,
                    Description = entity.Description,
                    YoutubeLink = entity.YoutubeLink,
                    Images = entity.ProjectFiles.Where(file => file.IsValid)
                                 .Select(file => new FileDetailsDto()
                                 {
                                     Id = file.Id,
                                     Checksum = file.Checksum,
                                     FileContentType = file.FileContentType,
                                     Description = file.Description,
                                     FileName = file.FileName,
                                     Name = file.Name,
                                     Url = file.Url,
                                     Extension = file.Extension
                                 }).ToList(),

                }).ToList();
        }
        #endregion

        #region Get Project Details
        public OperationResult<GenericOperationResult, ProjectDetailsDto> GetProjectDetails(int id)
        {
            var result = new OperationResult<GenericOperationResult,
                ProjectDetailsDto>(GenericOperationResult.Success);
            try
            {
                var data = GetProjectDetailsFromServer(id);
                return result.UpdateResultData(data);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public ProjectDetailsDto GetProjectDetailsFromServer(int projectId)
        {
            try
            {
                var result = Context.Projects
                 .Where(entity => entity.Id == projectId)
                 .Include(entity => entity.ProjectFiles)
                 .Include(entity => entity.ProjectKeys)
                 .Include(entity => entity.ProjectComponents)
                 .Include(entity => entity.ProjectEditions)
                 .Select(entity => new ProjectDetailsDto
                 {
                     Id = entity.Id,
                     Title = entity.Title,
                     Description = entity.Description,
                     YoutubeLink = entity.YoutubeLink,
                     Keys = entity.ProjectKeys.Where(pk => pk.IsValid).Select(pk => pk.KeyName).ToList(),
                     Editions = entity.ProjectEditions.Where(pe => pe.IsValid)
                     .Select(pe => new EditionDto
                     {
                         Id = pe.Id,
                         Description = pe.Description,
                         Link = pe.Link,
                         Title = pe.Title,
                     }).ToList(),
                     Components = entity.ProjectComponents.Where(pc => pc.IsValid)
                     .Select(pc => new ComponentDto
                     {
                         Id = pc.Id,
                         Title = pc.Title,
                         Description = pc.Description,
                         ComponentImageUrl = pc.ProjectComponentImageUrl
                     }).ToList(),
                     Images = entity.ProjectFiles.Where(file => file.IsValid)
                                 .Select(file => new FileDetailsDto()
                                 {
                                     Id = file.Id,
                                     Checksum = file.Checksum,
                                     FileContentType = file.FileContentType,
                                     Description = file.Description,
                                     FileName = file.FileName,
                                     Name = file.Name,
                                     Url = file.Url,
                                     Extension = file.Extension
                                 }),
                 }).SingleOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        #endregion

        #region Remove Project By Id
        public async Task<OperationResult<GenericOperationResult>> RemoveProject(int id)
        {
            var result = new OperationResult<GenericOperationResult>(GenericOperationResult.Success);

            var resultAfterRemovedProjectFromServer = await RemoveProjectFromServer(id);
            if (resultAfterRemovedProjectFromServer)
            {
                return result; // Removed Project from server successful //
            }

            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed); // Error //
        }

        public async Task<bool> RemoveProjectFromServer(int projectId)
        {
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                Context.Projects
                    .Remove(Context.Projects
                    .Where(entity => entity.Id == projectId
                    && (userInfo.IsUserAdmin || entity.CreatedBy == userInfo.CurrentUserId))
                    .SingleOrDefault());

                Context.SaveChanges();

                return true; // Removed Project from server successful //
            }
            catch (Exception)
            {
                return false; // Error //
            }
        }
        #endregion
    }
}

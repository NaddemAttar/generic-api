﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Core.Dto.OpeningHoures;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Enums.Core;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.AspNetCore.Identity;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class TimeManagementRepository : BasicRepository<CPDbContext, OpenningHouresSet>, ITimeManagementRepository
    {
        #region Properties & Constructor
        private IUserRepository UserRepository { get; }
        private UserManager<CPUser> UserManager { get; }
        public TimeManagementRepository(
            CPDbContext context,
            IUserRepository userRepository,
            UserManager<CPUser> userManager) : base(context)
        {
            UserRepository = userRepository;
            UserManager = userManager;
        }
        #endregion

        #region Get Opening Hours For Providers In WebSites
        public async Task<OperationResult<GenericOperationResult, IEnumerable<OpeningHouresDto>>> GetOpeningHouresAsync(int? userId, int? enterpriseId)
        {
            OperationResult<GenericOperationResult, IEnumerable<OpeningHouresDto>> result = new(GenericOperationResult.Failed);

            try
            {
                var healthCareProviderModel = new HealthCareProviderEnterprise();

                if (userId == null && enterpriseId == null)
                {
                    userId = (await UserManager.GetUsersInRoleAsync(Role.Admin.ToString())).FirstOrDefault().Id;
                }
                else if (userId != null && enterpriseId != null)
                {
                    healthCareProviderModel = await Context.HealthCareProviderEnterprise
                        .FirstOrDefaultAsync(hcpe => hcpe.UserId == userId && hcpe.EnterpriseId == enterpriseId);

                    if (healthCareProviderModel == null)
                        return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.UserNotFoundInEnterprise);
                }
                var openningHouresDto = await Context.OpenningHoures
                    .Where(op => (userId != null && enterpriseId != null && healthCareProviderModel != null) ?
                    op.HealthCareProviderEnterpriseId == healthCareProviderModel.Id
                    : (userId != null) ? op.UserId == userId : op.UserId == enterpriseId)
                    .Select(oh => new OpeningHouresDto()
                    {
                        Day = oh.Day,
                        From = oh.From,
                        Id = oh.Id,
                        IsWorkingDay = oh.IsWorkingDay,
                        To = oh.To
                    }).ToListAsync();

                //var data = Context.OpenningHoures
                //    .Where(entity => userId == null || entity.CreatedBy == userId)
                //    .Select(oh => new OpeningHouresDto()
                //    {
                //        Day = oh.Day,
                //        From = oh.From,
                //        Id = oh.Id,
                //        IsWorkingDay = oh.IsWorkingDay,
                //        To = oh.To
                //    }).ToList();

                return result.UpdateResultData(openningHouresDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Get Opening Hours For Providers In Dashboard
        public async Task<OperationResult<GenericOperationResult, IEnumerable<OpeningHouresDto>>> GetOpeningHouresForHealthProviderAsync(int? userId)
        {
            OperationResult<GenericOperationResult, IEnumerable<OpeningHouresDto>> result = new(GenericOperationResult.Failed);

            var userInfo = await UserRepository.GetCurrentUserInfo();
            if (userInfo.CurrentUserId == 0)
                return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.UserNotExist);

            await SeedOpeningHouresAsync(userInfo.CurrentUserId);

            try
            {
                var data = await Context.OpenningHoures
                     .Where(entity => (userId == null && entity.CreatedBy == userInfo.CurrentUserId) || entity.UserId == userId)
                     .Select(oh => new OpeningHouresDto()
                     {
                         Day = oh.Day,
                         From = oh.From,
                         Id = oh.Id,
                         IsWorkingDay = oh.IsWorkingDay,
                         To = oh.To
                     }).ToListAsync();

                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        private async Task SeedOpeningHouresAsync(int currentUserId)
        {
            var check = Context.OpenningHoures
                .Where(openingHour => openingHour.CreatedBy == currentUserId)
                .Any();

            if (!check)
            {
                foreach (var day in Enum.GetValues(typeof(DayOfWeek)).Cast<DayOfWeek>())
                {
                    Context.OpenningHoures.Add(new OpenningHouresSet
                    {
                        Day = day,
                        From = new TimeSpan(0, 0, 0),
                        To = new TimeSpan(12, 0, 0),
                        IsWorkingDay = true,
                    });
                }
                await Context.SaveChangesAsync();
            }
        }
        #endregion

        #region Changing the Opening Hours According to health Provider
        public async Task<OperationResult<GenericOperationResult, bool>> EditOpeningHouresAsync(
            IEnumerable<OpeningHouresDto> openingHouresDtoList)

        {
            OperationResult<GenericOperationResult,
                bool> result = new(GenericOperationResult.ValidationError);
            try
            {
                var user = await UserRepository.GetCurrentUserInfo();

                var openningHoursIds = new List<int>();
                foreach (var openningHoursDto in openingHouresDtoList)
                    openningHoursIds.Add(openningHoursDto.Id);

                var openningHoursModel = await Context.OpenningHoures
                    .Include(op => op.User)
                    .Include(op => op.HealthCareProviderEnterprise)
                    .Where(op => openningHoursIds.Contains(op.Id) && op.IsValid)
                    .OrderBy(op => op.Id)
                    .ToListAsync();

                //here update OpenningHours for Enterprise.
                if (openningHoursModel[0].UserId != null && openningHoursModel[0].HealthCareProviderEnterpriseId == null && openningHoursModel[0].User.HcpType == HcpType.Enterprise && openningHoursModel.Count() == openingHouresDtoList.Count())
                {
                    openingHouresDtoList = openingHouresDtoList.OrderBy(op => op.Id);
                    for (int i = 0; i < 7; i++)
                    {
                        openningHoursModel[i].From = openingHouresDtoList.ElementAt(i).From;
                        openningHoursModel[i].To = openingHouresDtoList.ElementAt(i).To;
                        openningHoursModel[i].IsWorkingDay = openingHouresDtoList.ElementAt(i).IsWorkingDay;
                        openningHoursModel[i].LastUpdatedBy = user.CurrentUserId;
                        openningHoursModel[i].LastUpdateDate = DateTime.Now;
                    }
                }

                bool[] isUpdated = new bool[7];
                //here update OpenningHours for CpUser.
                if (openningHoursModel[0].UserId != null && openningHoursModel[0].HealthCareProviderEnterpriseId == null && openningHoursModel[0].User.HcpType == HcpType.Individual && openningHoursModel.Count() == openingHouresDtoList.Count())
                {
                    //openingHouresDtoList = openingHouresDtoList.OrderBy(op => op.Day);

                    var enterprisesUserWorkOnItIds = await Context.HealthCareProviderEnterprise // Get HealthCareProviderEnterpriseIds.
                        .Where(hcpe => hcpe.IsValid && hcpe.UserId == user.CurrentUserId)
                        .Select(hcpe => hcpe.Id)
                        .ToListAsync();

                    var openningHoursOfUserInEnterprises = await Context.OpenningHoures
                        .Where(op => enterprisesUserWorkOnItIds.Contains(op.HealthCareProviderEnterpriseId.Value))
                        .ToListAsync();

                    _ = openningHoursOfUserInEnterprises.Count % 7 != 0 ? throw new Exception() : true;

                    //grouping them which each user in specific enterprise has 7 days.
                    var groupingOpenningHoursOfUserInEnterprises = openningHoursOfUserInEnterprises
                        .GroupBy(g => g.HealthCareProviderEnterpriseId)
                        .Select(s => new { HealthCareProviderEnterpriseId = s.Key, Data = s.ToList() });

                    foreach (var openningHoursOfUserInEnterprise in groupingOpenningHoursOfUserInEnterprises)
                    {
                        //openningHoursOfUserInEnterprise.Data
                        //    .OrderBy(op => op.Day)
                        //    .ToList();

                        for (int i = 0; i < 7; i++)
                        {
                            if (
                                   (openningHoursOfUserInEnterprise.Data[i].IsWorkingDay)
                                   && (openingHouresDtoList.ElementAt(i).IsWorkingDay)

                                   && ((openingHouresDtoList.ElementAt(i).From >= openningHoursOfUserInEnterprise.Data[i].From && openingHouresDtoList.ElementAt(i).From <= openningHoursOfUserInEnterprise.Data[i].To)
                                      && (openingHouresDtoList.ElementAt(i).To >= openningHoursOfUserInEnterprise.Data[i].From && openingHouresDtoList.ElementAt(i).To <= openningHoursOfUserInEnterprise.Data[i].To)

                                      || ((openingHouresDtoList.ElementAt(i).From <= openningHoursOfUserInEnterprise.Data[i].From && openingHouresDtoList.ElementAt(i).From <= openningHoursOfUserInEnterprise.Data[i].To)
                                         && (openingHouresDtoList.ElementAt(i).To >= openningHoursOfUserInEnterprise.Data[i].From && openingHouresDtoList.ElementAt(i).To <= openningHoursOfUserInEnterprise.Data[i].To))

                                      || ((openingHouresDtoList.ElementAt(i).From >= openningHoursOfUserInEnterprise.Data[i].From && openingHouresDtoList.ElementAt(i).From <= openningHoursOfUserInEnterprise.Data[i].To)
                                         && (openingHouresDtoList.ElementAt(i).To >= openningHoursOfUserInEnterprise.Data[i].From && openingHouresDtoList.ElementAt(i).To >= openningHoursOfUserInEnterprise.Data[i].To)))
                                   )
                            {
                                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.ConfilctBetweenEnterprisesOpenningHoures);
                            }
                            else
                            {
                                if (!isUpdated[i])
                                {
                                    openningHoursModel[i].From = openingHouresDtoList.ElementAt(i).From;
                                    openningHoursModel[i].To = openingHouresDtoList.ElementAt(i).To;
                                    openningHoursModel[i].IsWorkingDay = openingHouresDtoList.ElementAt(i).IsWorkingDay;
                                    openningHoursModel[i].LastUpdatedBy = user.CurrentUserId;
                                    openningHoursModel[i].LastUpdateDate = DateTime.Now;
                                    isUpdated[i] = true;
                                }
                                else
                                    continue;
                            }
                        }
                    }
                    if (groupingOpenningHoursOfUserInEnterprises.Count() == 0)
                    {
                        for (int i = 0; i < 7; i++)
                        {
                            openningHoursModel[i].From = openingHouresDtoList.ElementAt(i).From;
                            openningHoursModel[i].To = openingHouresDtoList.ElementAt(i).To;
                            openningHoursModel[i].IsWorkingDay = openingHouresDtoList.ElementAt(i).IsWorkingDay;
                            openningHoursModel[i].LastUpdatedBy = user.CurrentUserId;
                            openningHoursModel[i].LastUpdateDate = DateTime.Now;
                        }
                    }
                }

                //here update OpenningHours for CpUser in sepcific Enterprise.
                if (openningHoursModel[0].HealthCareProviderEnterpriseId != null && openningHoursModel[0].UserId == null && openningHoursModel.Count() == openingHouresDtoList.Count())
                {
                    //openingHouresDtoList = openingHouresDtoList.OrderBy(op => op.Day);
                    var enterprisesUserWorkOnItIds = await Context.HealthCareProviderEnterprise
                        .Where(hcpe => hcpe.IsValid && hcpe.UserId == user.CurrentUserId)
                        .Select(hcpe => hcpe.Id)
                        .ToListAsync();

                    var openningHoursOfUserInEnterprises = await Context.OpenningHoures
                        .Where(op => enterprisesUserWorkOnItIds.Contains(op.HealthCareProviderEnterpriseId.Value))
                        .ToListAsync();

                    _ = openningHoursOfUserInEnterprises.Count % 7 != 0 ? throw new Exception() : true;

                    //grouping them which each user in specific enterprise has 7 days.
                    var groupingOpenningHoursOfUserInEnterprises = openningHoursOfUserInEnterprises
                        .GroupBy(g => g.HealthCareProviderEnterpriseId)
                        .Select(s => new { HealthCareProviderEnterpriseId = s.Key, Data = s.ToList() });

                    var individualOpenningHours = await Context.OpenningHoures
                        .Where(op => op.UserId == user.CurrentUserId)
                        .OrderBy(op => op.Day)
                        .ToListAsync();

                    //here get OpenningHours for Enterprise which r already updated.
                    var enterpriseOpenningHours = await Context.OpenningHoures
                        .Where(op => op.UserId == openningHoursModel[0].HealthCareProviderEnterprise.EnterpriseId)
                        .OrderBy(op => op.Day)
                        .ToListAsync();

                    foreach (var openningHoursOfUserInEnterprise in groupingOpenningHoursOfUserInEnterprises)
                    {
                        //openningHoursOfUserInEnterprise.Data
                        //    .OrderBy(op => op.Day)
                        //    .ToList();                        

                        for (int i = 0; i < 7; i++)
                        {
                            //this condition for conflicting openningHours between Enterprises.
                            if (
                                   openningHoursOfUserInEnterprise.Data[i].IsWorkingDay
                                   && openingHouresDtoList.ElementAt(i).IsWorkingDay
                                   && (openningHoursModel[0].HealthCareProviderEnterpriseId != openningHoursOfUserInEnterprise.HealthCareProviderEnterpriseId)

                                   && (openingHouresDtoList.ElementAt(i).From >= openningHoursOfUserInEnterprise.Data[i].From && openingHouresDtoList.ElementAt(i).From <= openningHoursOfUserInEnterprise.Data[i].To
                                      && openingHouresDtoList.ElementAt(i).To >= openningHoursOfUserInEnterprise.Data[i].From && openingHouresDtoList.ElementAt(i).To <= openningHoursOfUserInEnterprise.Data[i].To

                                      || (openingHouresDtoList.ElementAt(i).From <= openningHoursOfUserInEnterprise.Data[i].From && openingHouresDtoList.ElementAt(i).From <= openningHoursOfUserInEnterprise.Data[i].To
                                         && openingHouresDtoList.ElementAt(i).To >= openningHoursOfUserInEnterprise.Data[i].From && openingHouresDtoList.ElementAt(i).To <= openningHoursOfUserInEnterprise.Data[i].To)

                                      || (openingHouresDtoList.ElementAt(i).From >= openningHoursOfUserInEnterprise.Data[i].From && openingHouresDtoList.ElementAt(i).From <= openningHoursOfUserInEnterprise.Data[i].To
                                         && openingHouresDtoList.ElementAt(i).To >= openningHoursOfUserInEnterprise.Data[i].From && openingHouresDtoList.ElementAt(i).To >= openningHoursOfUserInEnterprise.Data[i].To))
                                   )
                            {
                                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.ConfilctBetweenEnterprisesOpenningHoures);
                            }

                            //this mean From and To should be between From and To of Enterprise.
                            else if (
                                   (openningHoursModel[0].HealthCareProviderEnterpriseId == openningHoursOfUserInEnterprise.HealthCareProviderEnterpriseId)
                                   && openingHouresDtoList.ElementAt(i).IsWorkingDay
                                   && (((openingHouresDtoList.ElementAt(i).From < enterpriseOpenningHours[i].From && openingHouresDtoList.ElementAt(i).From > enterpriseOpenningHours[i].To
                                         && openingHouresDtoList.ElementAt(i).To < enterpriseOpenningHours[i].From && openingHouresDtoList.ElementAt(i).To > enterpriseOpenningHours[i].To))
                                      || (enterpriseOpenningHours[i].IsWorkingDay == false))
                                   )
                            {
                                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.OutOfEnterpriseOpenningHoures);
                            }

                            if (!isUpdated[i])
                            {
                                openningHoursModel[i].From = openingHouresDtoList.ElementAt(i).From;
                                openningHoursModel[i].To = openingHouresDtoList.ElementAt(i).To;
                                openningHoursModel[i].IsWorkingDay = openingHouresDtoList.ElementAt(i).IsWorkingDay;
                                openningHoursModel[i].LastUpdatedBy = user.CurrentUserId;
                                openningHoursModel[i].LastUpdateDate = DateTime.Now;
                                isUpdated[i] = true;
                            }
                            else
                                continue;
                        }
                    }
                    for (int i = 0; i < 7; i++)
                    {
                        //here to don't conflict with individual OpeningHours.
                        if (
                              (individualOpenningHours[i].IsWorkingDay) && (openingHouresDtoList.ElementAt(i).IsWorkingDay)

                              && ((openingHouresDtoList.ElementAt(i).From >= individualOpenningHours[i].From && openingHouresDtoList.ElementAt(i).From <= individualOpenningHours[i].To)
                              && (openingHouresDtoList.ElementAt(i).To >= individualOpenningHours[i].From && openingHouresDtoList.ElementAt(i).To <= individualOpenningHours[i].To)

                              || ((openingHouresDtoList.ElementAt(i).From <= individualOpenningHours[i].From && openingHouresDtoList.ElementAt(i).From <= individualOpenningHours[i].To)
                                 && (openingHouresDtoList.ElementAt(i).To >= individualOpenningHours[i].From && openingHouresDtoList.ElementAt(i).To <= individualOpenningHours[i].To))

                              || ((openingHouresDtoList.ElementAt(i).From >= individualOpenningHours[i].From && openingHouresDtoList.ElementAt(i).From <= individualOpenningHours[i].To)
                                 && (openingHouresDtoList.ElementAt(i).To >= individualOpenningHours[i].From && openingHouresDtoList.ElementAt(i).To >= individualOpenningHours[i].To)))
                           )
                        {
                            return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.ConfilctWithIndividualOpenningHoures);
                        }
                        if (!isUpdated[i])
                        {
                            openningHoursModel[i].From = openingHouresDtoList.ElementAt(i).From;
                            openningHoursModel[i].To = openingHouresDtoList.ElementAt(i).To;
                            openningHoursModel[i].IsWorkingDay = openingHouresDtoList.ElementAt(i).IsWorkingDay;
                            openningHoursModel[i].LastUpdatedBy = user.CurrentUserId;
                            openningHoursModel[i].LastUpdateDate = DateTime.Now;
                            isUpdated[i] = true;
                        }
                        else
                            continue;
                    }
                }
                //foreach (OpeningHouresDto openingHouresDto in openingHouresDtoList)
                //{
                //    OpenningHouresSet Entity = Context.OpenningHoures
                //        .Where(op => op.Id == openingHouresDto.Id)
                //        .SingleOrDefault();

                //    Entity.From = openingHouresDto.From;
                //    Entity.To = openingHouresDto.To;
                //    Entity.IsWorkingDay = openingHouresDto.IsWorkingDay;
                //    Entity.UserId = UserRepository.CurrentUserIdentityId();

                //    await Context.SaveChangesAsync();

                //    openingHouresDto.Id = Entity.Id;
                //}

                //var reultdata = openingHouresDtoList.Select(op => new OpeningHouresDto()
                //{
                //    Id = op.Id,
                //    IsWorkingDay = op.IsWorkingDay,
                //    To = op.To,
                //    Day = op.Day,
                //    From = op.From
                //}).ToList();

                await Context.SaveChangesAsync();
                return result.UpdateResultData(true)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed)
                    .AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion
    }
}
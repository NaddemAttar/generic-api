﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;
using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Group;
using IGenericControlPanel.Core.Dto.Question;
using IGenericControlPanel.Core.Dto.Service;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Extension;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using IGenericControlPanel.SharedKernal.Utils;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class QuestionRepository : BasicRepository<CPDbContext, Question>, IQuestionRepository
    {
        #region Constructs && Properties
        private IGamificationMovementsRepository _gamificationMovementRepo { get; }
        private IUserLanguageRepository UserLanguageRepository { get; }
        private IMailRepository mailService { get; }
        private IUserRepository UserRepository { get; }
        private IHttpContextAccessor httpContextAccessor { get; }
        public QuestionRepository(CPDbContext dbContext,
            IMapper mapper, IMailRepository mailService, IHttpContextAccessor httpContextAccessor,
            IUserLanguageRepository userLanguageRepository,
            IUserRepository userRepository,
            IGamificationMovementsRepository gamificationMovementRepo) : base(dbContext, mapper)
        {
            UserLanguageRepository = userLanguageRepository;
            this.mailService = mailService;
            UserRepository = userRepository;
            this.httpContextAccessor = httpContextAccessor;
            _gamificationMovementRepo = gamificationMovementRepo;
        }

        #endregion

        #region Action Section
        public async Task<OperationResult<GenericOperationResult, QuestionFormDto>> Action(QuestionFormDto formDto)
        {
            var currentUserId = UserRepository.CurrentUserIdentityId();
            OperationResult<GenericOperationResult, QuestionFormDto> result = new(GenericOperationResult.Failed);

            //var isCreate = formDto != null && formDto.Id == 0;

            result = formDto.Id == 0 ? await Create(formDto, currentUserId) : await Update(formDto, currentUserId);

            return result.IsSuccess ? new OperationResult<GenericOperationResult,
                    QuestionFormDto>(GenericOperationResult.Success, entity: result.Result) : result;

        }
        #endregion

        #region Create New Question
        private async Task<OperationResult<GenericOperationResult, QuestionFormDto>> Create(QuestionFormDto formDto, int currentUserId)
        {
            var result = new OperationResult<GenericOperationResult, QuestionFormDto>(GenericOperationResult.Failed);

            try
            {
                var entity = new Question()
                {
                    Title = formDto.Title,
                    Content = formDto.Content,
                    CultureCode = formDto.CultureCode,
                    ServiceId = formDto.ServiceId,
                    FAQ = formDto.FAQ.Value,
                    PostTypeId = formDto.PostTypeId,
                    GroupId = formDto.GroupId,
                    IsHidden = false,
                    ShowFirst = false
                };
                foreach (var MultiLevelCategoryId in formDto.MultiLevelCategoryIds)
                {
                    await Context.MultiLevelCategoryEntities.AddAsync(new MultiLevelCategoryRelatedEntity()
                    {
                        Question = entity,
                        MultiLevelCategoryId = MultiLevelCategoryId
                    });
                }

                foreach (var attachment in formDto.Attachments)
                {
                    var file = new QuestionFile()
                    {
                        Checksum = attachment.Checksum,
                        FileContentType = attachment.FileContentType,
                        Description = attachment.Description,
                        Extension = attachment.Extension,
                        FileName = attachment.FileName,
                        Name = attachment.Name,
                        Url = attachment.Url,

                    };
                    entity.Files.Add(Mapper.Map<QuestionFile>(file));
                }

                await Context.AddAsync(entity);
                await Context.SaveChangesAsync();

                if (formDto.SendEmail)
                {
                    var body = "";
                    var host = "https://" + httpContextAccessor.HttpContext.Request.Host.Value + "/questions/" + entity.Id.ToString();

                    List<string> emails = Context.Subscribers.Where(sub => sub.IsValid).Select(sub => sub.Email).ToList();
                    if (formDto.Content.Length >= 600)
                    {
                        body = $"{formDto.Content[..600]} <a href=`{host}`>Read More ...</a> ";
                    }

                    await mailService.SendEmailAsync(new MailRequest()
                    {
                        ToEmails = emails,
                        Subject = formDto.Title,
                        Body = body != "" ? body : formDto.Content,

                    });
                }
                formDto.Attachments = entity.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                {
                    Id = file.Id,
                    Checksum = file.Checksum,
                    FileContentType = file.FileContentType,
                    Description = file.Description,
                    FileName = file.FileName,
                    Name = file.Name,
                    Url = file.Url
                }).ToList();
                formDto.Id = entity.Id;

                foreach (var file in formDto.Attachments)
                    file.SourceId = entity.Id;

                var savePointsResult = await _gamificationMovementRepo.SavePoints(new List<string>
                {
                    GamificationNames.AddQuestion
                });

                if (!savePointsResult.IsSuccess)
                {
                    return result.AddError(savePointsResult.ErrorMessages).UpdateResultStatus(savePointsResult.EnumResult);
                }

                return new OperationResult<GenericOperationResult, QuestionFormDto>(GenericOperationResult.Success, formDto);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Update Question
        private async Task<OperationResult<GenericOperationResult, QuestionFormDto>> Update(QuestionFormDto formDto, int currentUserId)
        {
            if (formDto != null)
            {

                var entityToEdit = await ValidEntities
                    .Where(entit => entit.Id == formDto.Id)
                    .Include(entity => entity.Files)
                    .Include(entity => entity.MultiLevelCategoryRelatedEntities)
                    .SingleOrDefaultAsync();

                if (entityToEdit == null)
                    return new OperationResult<GenericOperationResult, QuestionFormDto>(GenericOperationResult.NotFound).AddError(ErrorMessages.QuestionNotFound);

                var removedFilesdata = await Context.Question
                    .Where(question => question.Id == formDto.Id)
                    .Include(entity => entity.Files)
                    .Select(question => question.Files.Where(file => formDto.RemovedFilesIds.Contains(file.Id)).Select(file => new FileDetailsDto()
                    {
                        Id = file.Id,
                        Checksum = file.Checksum,
                        FileContentType = file.FileContentType,
                        Description = file.Description,
                        FileName = file.FileName,
                        Name = file.Name,
                        Url = file.Url
                    }))
                    .FirstOrDefaultAsync();

                Context.RemoveRange(Context.Files.Where(file => formDto.RemovedFilesIds.Contains(file.Id)));
                formDto.RemovedFilesUrls = removedFilesdata.Select(files => files.Url);

                if (entityToEdit != null)
                {
                    entityToEdit.Id = formDto.Id;
                    entityToEdit.Title = formDto.Title;
                    entityToEdit.IsHidden = formDto.IsHidden;
                    entityToEdit.ShowFirst = formDto.ShowFirst;
                    entityToEdit.Content = formDto.Content;
                    entityToEdit.IsValid = true;
                    entityToEdit.ServiceId = formDto.ServiceId;
                    entityToEdit.FAQ = formDto.FAQ.Value;
                    entityToEdit.PostTypeId = formDto.PostTypeId;
                    entityToEdit.GroupId = formDto.GroupId;
                    Context.MultiLevelCategoryEntities.RemoveRange(Context.MultiLevelCategoryEntities
                        .Where(mc => mc.IsValid && mc.QuestionId == entityToEdit.Id && !formDto.MultiLevelCategoryIds.Contains(mc.MultiLevelCategoryId)).ToList());
                    foreach (var MultiLevelCategoryId in formDto.MultiLevelCategoryIds)
                    {
                        var Category = Context.MultiLevelCategories.Where(cat => cat.IsValid && cat.Id == MultiLevelCategoryId).SingleOrDefault();
                        if (Category != null)
                        {
                            Context.MultiLevelCategoryEntities.Add(new MultiLevelCategoryRelatedEntity()
                            {
                                MultiLevelCategoryId = Category.Id,
                                QuestionId = entityToEdit.Id
                            });
                        }
                    }

                    foreach (var attachment in formDto.Attachments)
                    {
                        var extension = $".{attachment.Url.Split('.')[1]}";
                        var file = new QuestionFile()
                        {
                            Checksum = attachment.Checksum,
                            FileContentType = attachment.FileContentType,
                            Description = attachment.Description,
                            Extension = extension,
                            FileName = attachment.FileName,
                            Name = attachment.Name,
                            Url = attachment.Url,
                        };
                        entityToEdit.Files.Add(file);
                    }

                    Context.Update(entityToEdit);
                }

                await Context.SaveChangesAsync();

                formDto.Attachments = entityToEdit.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                {
                    Id = file.Id,
                    Checksum = file.Checksum,
                    FileContentType = file.FileContentType,
                    Description = file.Description,
                    FileName = file.FileName,
                    Name = file.Name,
                    Url = file.Url
                }).ToList();

                return new OperationResult<GenericOperationResult, QuestionFormDto>(GenericOperationResult.Success, formDto);
            }
            else
            {
                return new OperationResult<GenericOperationResult, QuestionFormDto>(GenericOperationResult.Failed, formDto).AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Get Section
        private IEnumerable<int> GetCategoriesInCategory(int categoryId)
        {
            var categories = Context.MultiLevelCategories.Include(cat => cat.Children).ToList();
            var categoriesIds = new List<int>();
            var currentParent = categories.Where(cat => cat.Id == categoryId).SingleOrDefault();
            addToListOfCategories(currentParent, categoriesIds);
            return categoriesIds;
        }
        private void addToListOfCategories(MultiLevelCategory cat, List<int> catList)
        {
            catList.Add(cat.Id);
            foreach (var item in cat.Children)
            {
                addToListOfCategories(item, catList);
            }
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<QuestionDetailsDto>>> GetAllDetailedPaged(
            bool? showFirst, bool? isHidden, int? PostTypeId, int? MultiLevelCategoryId, int? UserId,
            List<int> serviceIds, int? GroupId, bool WithAnswersOnly, bool FAQ, string query, int pageNumber,
            int pageSize, string cultureCode = "ar", bool enablePagination = true)
        {
            OperationResult<GenericOperationResult, IEnumerable<QuestionDetailsDto>> result = new(GenericOperationResult.Failed);

            var categoriesIds = new List<int>();

            if (MultiLevelCategoryId != null)
                categoriesIds = GetCategoriesInCategory(MultiLevelCategoryId.Value).ToList();

            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();
                var builder = WebApplication.CreateBuilder();
                bool multiProviders = bool.Parse(builder.Configuration.GetSection("MultiProviders").Value.ToLower());
                var Rawdata = ValidEntities
                    .Where(entity => (PostTypeId != null ? entity.PostTypeId == PostTypeId : true)
                    && (MultiLevelCategoryId == null ? true :
                    (entity.MultiLevelCategoryRelatedEntities.Where(cat => cat.MultiLevelCategoryId == MultiLevelCategoryId).Any(x => categoriesIds.Contains(x.MultiLevelCategoryId))))
                    && (FAQ ? entity.FAQ == true : true)
                    && (serviceIds.Count() > 0 ? serviceIds.Contains(entity.ServiceId ?? 0) : true)
                    && (GroupId != null ? entity.GroupId == GroupId : true)
                    && (UserId != null ? entity.UserId == UserId : true)
                    && (isHidden != null ? entity.IsHidden == isHidden : entity.IsHidden == false)
                    && (showFirst != null ? entity.ShowFirst == showFirst : true)
                    && (!string.IsNullOrEmpty(query) ? entity.Title.Replace(" ", "").Contains(query.Replace
                    (" ", "")) || entity.Content.Replace(" ", "").Contains(query.Replace
                    (" ", "")) : true)
                    && (WithAnswersOnly ? entity.Answers.Any(Answer => Answer.IsValid) : true))
                    .OrderByDescending(entity => entity.ShowFirst)
                    .AsQueryable();

                if (enablePagination)
                    Rawdata = Rawdata.GetDataWithPagination(pageSize, pageNumber);

                var data = Rawdata
                    .Include(entity => entity.Files)
                    .Include(entity => entity.Service)
                    .Include(entity => entity.Answers)
                    .Include(entity => entity.Group)
                    .Include(entity => entity.PostType)
                    .Include(entity => entity.MultiLevelCategoryRelatedEntities)
                    .ToList()
                    .Select(entity => new QuestionDetailsDto
                    {
                        Id = entity.Id,
                        MultiLevelCategoryIds = entity.MultiLevelCategoryRelatedEntities.Select(ml => ml.MultiLevelCategoryId).ToList(),
                        Title = entity.Title,
                        ShowFirst = entity.ShowFirst,
                        IsHidden = entity.IsHidden,
                        Group = entity.GroupId == null ? null : new GroupDto()
                        {
                            Name = entity.Group.Name,
                            Id = entity.GroupId.Value
                        },
                        CanDelete = entity.CreatedBy == 0 ? userInfo.IsUserAdmin : (entity.CreatedBy == userInfo.CurrentUserId || userInfo.IsUserAdmin),
                        Answers = entity.Answers
                        .Where(answer => answer.IsValid)
                        .OrderByDescending(answer => answer.CreationDate)
                        .Take(1)
                        .Select(entity => new AnswerFormDto()
                        {
                            Id = entity.Id,
                            Content = entity.Content
                        }).ToList(),
                        Content = entity.Content,
                        Attachments = entity.Files != null || entity.Files.Count > 0 ?
                        entity.Files.Where(entity => entity.IsValid
                        && entity.FileContentType == FileContentType.Attachement).Select(entity => new FileDetailsDto
                        {
                            Id = entity.Id,
                            Checksum = entity.Checksum,
                            FileContentType = entity.FileContentType,
                            Description = entity.Description,
                            FileName = entity.FileName,
                            Name = entity.Name,
                            Url = entity.Url,
                            Extension = entity.Extension
                        }).ToList() : null,
                        Date = entity.CreationDate,
                        AnswersCount = entity.Answers.Where(a => a.IsValid).Count()
                    });
                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public OperationResult<GenericOperationResult, QuestionDetailsDto> GetDetailed(int id, string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, QuestionDetailsDto> result = new(GenericOperationResult.Failed);
            try
            {
                var data = ValidEntities
                    .Include(entity => entity.Files)
                    .Include(entity => entity.Group)
                    .Include(entity => entity.Answers).ThenInclude(entity => entity.Files)
                    .Include(entity => entity.Service).Include(entity => entity.MultiLevelCategoryRelatedEntities)
                    .Where(entity => entity.Id == id)
                    .ToList()
                    .Select(entity => new QuestionDetailsDto()
                    {
                        PostTypeId = entity.PostTypeId,
                        Answers = entity.Answers.Where(answer => answer.IsValid).Select(answer => new AnswerFormDto()
                        {
                            Content = answer.Content,
                            Files = answer.Files.Select(file => new FileDetailsDto()
                            {
                                Id = file.Id,
                                Checksum = file.Checksum,
                                FileContentType = file.FileContentType,
                                Description = file.Description,
                                FileName = file.FileName,
                                Name = file.Name,
                                Url = file.Url,
                                Extension = file.Extension
                            }).ToList(),
                            Id = answer.Id,
                            QuestionId = entity.Id
                        }),
                        Id = entity.Id,
                        ServiceId = entity.ServiceId,
                        FAQ = entity.FAQ,
                        Group = entity.GroupId == null ? null : new GroupDto()
                        {
                            Name = entity.Group.Name,
                            Id = entity.GroupId.Value
                        },
                        Service = entity.Service != null ? new ServiceDto()
                        {
                            Id = entity.ServiceId.Value,
                            Name = entity.Service.Name
                        } : null,
                        Attachments = entity.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                        {
                            Id = file.Id,
                            Checksum = file.Checksum,
                            FileContentType = file.FileContentType,
                            Description = file.Description,
                            FileName = file.FileName,
                            Extension = file.Extension,
                            Name = file.Name,
                            Url = file.Url
                        }).ToList(),
                        Content = entity.Content,
                        MultiLevelCategoryIds = entity.MultiLevelCategoryRelatedEntities.Select(ml => ml.MultiLevelCategoryId).ToList(),
                        CultureCode = entity.CultureCode,
                        MetaDescription = entity.MetaDescription,
                        Title = entity.Title,
                        Date = entity.CreationDate,
                        IsHidden = entity.IsHidden,
                        ShowFirst = entity.ShowFirst,
                        AnswersCount = entity.Answers.Where(a => a.IsValid).Count()
                    }).ToList()
                    .FirstOrDefault();
                return data is null
                    ? result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.QuestionNotFound)
                    : result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }

        }

        public async Task<OperationResult<GenericOperationResult, IEnumerable<QuestionDetailsDto>>> GetQuestionAutoComplete(string query)
        {
            OperationResult<GenericOperationResult, IEnumerable<QuestionDetailsDto>> result = new(GenericOperationResult.Failed);
            try
            {
                var data = await ValidEntities
                    .Where(question => question.Content.ToUpper().Contains(query.ToUpper()))
                    .Select(x => new QuestionDetailsDto()
                    {
                        Content = x.Content,
                        Id = x.Id
                    }).ToListAsync();

                return data is null
                   ? result.UpdateResultStatus(GenericOperationResult.NotFound)
                   : result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Remove Section
        public async Task<OperationResult<GenericOperationResult>> Remove(int id)
        {
            var result = new OperationResult<GenericOperationResult>(GenericOperationResult.Failed);

            try
            {
                var questionEntity = await ValidEntities.FirstOrDefaultAsync(question => question.Id == id);

                if (questionEntity != null)
                {
                    Context.Remove(questionEntity);
                    await Context.SaveChangesAsync();
                }
                else
                    result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.QuestionNotFound);

                return new OperationResult<GenericOperationResult>(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult>> RemoveAnswer(int id)
        {
            var result = new OperationResult<GenericOperationResult>(GenericOperationResult.Failed);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var answer = await Context.Answer
                    .Where(entity => entity.IsValid && entity.Id == id)
                    .SingleOrDefaultAsync();

                if (answer == null)
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.AnswerNotFound);

                if (userInfo.IsUserAdmin || userInfo.CurrentUserId == answer.CreatedBy)
                {
                    Context.Answer.Remove(answer);
                    await Context.SaveChangesAsync();
                }
                else
                {
                    return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.YouDoNotHavePermissionToEdit);
                }
                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        public FileDetailsDto GetFile(string Url)
        {
            return Context.Files.Where(file => file.IsValid && file.Url == Url)
                .Select(file => new FileDetailsDto
                {
                    Url = file.Url,
                    Extension = file.Extension,
                    Name = file.Name
                }).SingleOrDefault();
        }
        public async Task<OperationResult<GenericOperationResult, AnswerFormDto>> ActionAnswerQuestion(AnswerFormDto formDto)
        {
            var result = new OperationResult<GenericOperationResult, AnswerFormDto>(GenericOperationResult.Failed);
            try
            {
                result = formDto.Id == 0 ? await AddAnswer(formDto) : await UpdateAnswer(formDto);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
            return result;
        }
        private async Task<OperationResult<GenericOperationResult, AnswerFormDto>> AddAnswer(AnswerFormDto formDto)
        {
            OperationResult<GenericOperationResult, AnswerFormDto> result = new(GenericOperationResult.Failed);
            try
            {
                //check if the question Exists.
                var isExists = await Context.Question.AnyAsync(q => q.Id == formDto.QuestionId);
                if (!isExists)
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.QuestionNotFound);

                Answer entity = new()
                {
                    Content = formDto.Content,
                    QuestionId = formDto.QuestionId,
                };

                if (formDto.Files != null)
                {
                    entity = AddingAnswerFiles(formDto.Files, entity);
                }
                await Context.Answer.AddAsync(entity);
                if (formDto.Files is not null)
                {
                    foreach (FileDetailsDto image in formDto.Files)
                    {
                        image.SourceId = entity.Id;
                    }
                }
                await Context.SaveChangesAsync();
                return result.UpdateResultData(formDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError).UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        private async Task<OperationResult<GenericOperationResult, AnswerFormDto>> UpdateAnswer(AnswerFormDto formDto)
        {
            var result = new OperationResult<GenericOperationResult, AnswerFormDto>(GenericOperationResult.Failed);
            try
            {
                var entity = await Context.Answer
                    .Where(entity => entity.IsValid && entity.Id == formDto.Id)
                    .SingleOrDefaultAsync();

                if (entity is null)
                    return result.AddError(ErrorMessages.AnswerNotFound).UpdateResultStatus(GenericOperationResult.NotFound);

                //entity.Id = formDto.Id;
                entity.Content = formDto.Content;
                entity.QuestionId = formDto.QuestionId;
                if (formDto.RemovedFilesIds.Count() > 0)
                    formDto = RemoveAnswerFiles(formDto);

                Context.Entry(entity).State = EntityState.Detached;

                if (formDto.Files.Count != 0)
                    entity = AddingAnswerFiles(formDto.Files, entity);

                Context.Answer.Update(entity);
                await Context.SaveChangesAsync();
                return result.UpdateResultData(formDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        private Answer AddingAnswerFiles(IEnumerable<FileDetailsDto> images, Answer entity)
        {
            foreach (FileDetailsDto image in images)
            {
                entity.Files.Add(new AnswerFile()
                {
                    Checksum = image.Checksum,
                    FileContentType = image.FileContentType,
                    Description = image.Description,
                    Extension = image.Extension,
                    FileName = image.FileName,
                    Name = image.Name,
                    Url = image.Url,
                });
            }

            return entity;
        }
        private AnswerFormDto RemoveAnswerFiles(AnswerFormDto formDto)
        {
            var removedFiles = Context.Files
                    .Where(file => formDto.RemovedFilesIds.Contains(file.Id))
                    .ToList();

            Context.RemoveRange(removedFiles);
            formDto.RemovedFilesData = removedFiles.Select(entity => new FileDetailsDto()
            {
                Checksum = entity.Checksum,
                FileContentType = entity.FileContentType,
                Description = entity.Description,
                Extension = entity.Extension,
                FileName = entity.FileName,
                Name = entity.Name,
                Url = entity.Url,
            });
            return formDto;
        }
    }
}
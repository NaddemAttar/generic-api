﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.Dto.Product;
using IGenericControlPanel.Core.IData;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.Model;
using IGenericControlPanel.Model.Content;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using IGenericControlPanel.SharedKernal.Utils;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IGenericControlPanel.Core.Data.Repositories
{

    public class ProductRepository : BasicRepository<CPDbContext, Product>, IProductRepository
    {
        #region Properties
        private IUserLanguageRepository UserLanguageRepository { get; }
        private IUserRepository UserRepository { get; }
        private IGamificationMovementsRepository _gamificationMovementRepo { get; }

        #endregion

        #region Constructors

        public ProductRepository(CPDbContext context,
                                IUserLanguageRepository userLanguageRepository,
                                IUserRepository userRepository,
                                IMapper mapper,
                                IGamificationMovementsRepository gamificationMovementRepo) : base(context, mapper)
        {
            UserRepository = userRepository;
            UserLanguageRepository = userLanguageRepository;
            _gamificationMovementRepo = gamificationMovementRepo;
        }

        #endregion


        #region GET
        public async Task<OperationResult<GenericOperationResult, ProductDetailsDto>> GetDetailedAsync(
            int id, string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, ProductDetailsDto> result = new(GenericOperationResult.Failed);

            try
            {
                var product = await Context.Products
                             .Include(p => p.Category).ThenInclude(cate => cate.Products)
                             .Include(p => p.Category).ThenInclude(cate => cate.Offer)
                             .Include(p => p.Files)
                             .Include(p => p.Offer)
                             .Include(entity => entity.ProductSpecifications.Where(ps => ps.IsValid)).ThenInclude(ps => ps.Specification)
                             .Include(p => p.ProductSizeColors).ThenInclude(psc => psc.Offer)
                             .Include(p => p.Brand).ThenInclude(psc => psc.Offer)
                             .Include(p => p.ProductReviews).ThenInclude(pr => pr.User)
                             .OrderBy(p => p.Order)
                             .FirstOrDefaultAsync(p => p.Id == id);

                if (product == null)
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.ProductNotFound);

                var x = await CustomTheResult(new List<Product> { product });

                ProductDetailsDto productDetailsDto = new();
                productDetailsDto = x.Result[0];

                return result.UpdateResultData(productDetailsDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Get All Paged
        public async Task<OperationResult<GenericOperationResult, IEnumerable<ProductDetailsDto>>> GetAllDetailedPagedAsync(
            bool? TopRated,
            int? MinPrice, int? MaxPrice,
            int? CategoryId, int? BrandId, IEnumerable<int> SpecificationsIds, string query = "",
            int? pageNumber = 0, int? pageSize = null, string cultureCode = "ar", bool? showInStore = null, int? OfferIdToOrderBy = null)
        {
            OperationResult<GenericOperationResult, IEnumerable<ProductDetailsDto>> result = new(GenericOperationResult.Failed);
            try
            {
                pageSize ??= 10;
                pageNumber ??= 0;
                List<int> categoriesIds = new();
                var builder = WebApplication.CreateBuilder();
                bool multiProviders = bool.Parse(builder.Configuration.GetSection("MultiProviders").Value.ToLower());
                if (CategoryId != null)
                {
                    var ids = await GetCategoriesInCategoryAsync(CategoryId.Value);
                    categoriesIds = ids.ToList();
                }

                var userInfo = await UserRepository.GetCurrentUserInfo();
                var products = await ValidEntities
                            .Where(entity => (userInfo.CurrentUserId == 0 || userInfo.IsUserAdmin
                            || (multiProviders == false ? true : userInfo.IsHealthCareProvider ? userInfo.CurrentUserId == entity.CreatedBy : true))
                            && (showInStore == null || entity.ShowInStore == showInStore)
                            && (query == null || entity.Name.Contains(query) || entity.Description.Contains(query))
                            && (CategoryId == null || categoriesIds.Contains(entity.CategoryId))
                            && (BrandId == null || entity.BrandId == BrandId))
                            .OrderByDescending(entity => entity.CreationDate)
                            .Skip(pageNumber.Value * pageSize.Value)
                            .Take(pageSize.Value)
                            .Include(p => p.Offer)
                            .Include(entity => entity.Category).ThenInclude(psc => psc.Offer)
                            .Include(entity => entity.Brand).ThenInclude(psc => psc.Offer)
                            .Include(entity => entity.Files)
                            .Include(entity => entity.ProductSpecifications.Where(ps => ps.IsValid)).ThenInclude(ps => ps.Specification)
                            .Include(entity => entity.OrderProducts)
                            .Include(entity => entity.ProductSizeColors).ThenInclude(psc => psc.Offer)
                            .Include(entity => entity.User)
                            .Include(entity => entity.ProductReviews).ThenInclude(entity => entity.User)
                            .ToListAsync();
                List<ProductDetailsDto> productsDetailsDto = (await CustomTheResult(products)).Result;

                return result.UpdateResultData(productsDetailsDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        private async Task<OperationResult<GenericOperationResult, List<ProductDetailsDto>>> CustomTheResult(List<Product> products)
        {
            var result = new OperationResult<GenericOperationResult, List<ProductDetailsDto>>(GenericOperationResult.Failed);
            try
            {
                List<ProductDetailsDto> productsDetailsDto = new();
                foreach (var product in products)
                {
                    ProductDetailsDto productDetailsDto = new();
                    productDetailsDto.Id = product.Id;
                    productDetailsDto.CategoryId = product.CategoryId;
                    productDetailsDto.CategoryName = product.Category.Name;
                    productDetailsDto.Description = product.Description;
                    productDetailsDto.CultureCode = product.CultureCode;
                    productDetailsDto.Name = product.Name;
                    productDetailsDto.ShowInStore = product.ShowInStore;
                    productDetailsDto.AvarageReview = product.ProductReviews.Count() != 0 ? product.ProductReviews.Sum(pr => pr.Rating) / product.ProductReviews.Count() : 0;
                    productDetailsDto.Brand = new();
                    productDetailsDto.Brand.Id = product.BrandId;
                    productDetailsDto.Brand.Name = product.Brand.Name;

                    productDetailsDto.ProductReviewsDto = new List<ProductReviewDto>();
                    foreach (var productReview in product.ProductReviews)
                    {
                        ProductReviewDto productReviewDto = new();
                        productReviewDto.Id = productReview.Id;
                        productReviewDto.Rating = productReview.Rating;
                        productReviewDto.Description = productReview.Description;
                        productReviewDto.CreationDate = productReview.CreationDate;
                        productReviewDto.ReviewerName = productReview.User.FirstName + " " + productReview.User.LastName;
                        productDetailsDto.ProductReviewsDto.Add(productReviewDto);
                    }

                    foreach (var file in product.Files)
                    {
                        FileDetailsDto fileDetailsDto = new();
                        fileDetailsDto.Id = file.Id;
                        fileDetailsDto.Checksum = file.Checksum;
                        fileDetailsDto.FileContentType = file.FileContentType;
                        fileDetailsDto.Description = file.Description;
                        fileDetailsDto.FileName = file.FileName;
                        fileDetailsDto.Name = file.Name;
                        fileDetailsDto.Url = file.Url;
                        fileDetailsDto.Extension = file.Extension;
                        productDetailsDto.Attachments.Add(fileDetailsDto);
                    }

                    foreach (var spec in product.ProductSpecifications)
                    {
                        ProductSpecificationDto productSpecificationDto = new();
                        //productSpecificationDto.Id = spec.Id;
                        productSpecificationDto.SpecificationId = spec.SpecificationId;
                        productSpecificationDto.SpecificationName = spec.Specification.Name;
                        productSpecificationDto.Value = spec.Value;
                        productSpecificationDto.IsValid = spec.IsValid;
                        productDetailsDto.ProductSpecifications.Add(productSpecificationDto);
                    }

                    foreach (var detail in product.ProductSizeColors)
                    {
                        ProductSizeColorDto productSizeColorDto = new();
                        productSizeColorDto.Id = detail.Id;
                        productSizeColorDto.ProductId = detail.ProductId;
                        productSizeColorDto.ColorHex = detail.ColorHex;
                        productSizeColorDto.Price = detail.Price;
                        if (detail.OfferId != null && productSizeColorDto.NewPrice == null && DateTime.Now >= detail.Offer.StartDate && detail.Offer.EndDate >= DateTime.Now && detail.Offer.IsValid)
                        {
                            //else see the psc
                            productSizeColorDto.NewPrice =
                                (detail.Offer.Value != null) ?
                                (detail.Price - detail.Offer.Value) :
                                (detail.Price - (detail.Price * (detail.Offer.Percentage / 100.0)));
                            productSizeColorDto.EndOfferDate = productSizeColorDto.NewPrice != null ? detail.Offer.EndDate : null;
                        }
                        else if (product.OfferId != null && productSizeColorDto.NewPrice == null && DateTime.Now >= product.Offer.StartDate && product.Offer.EndDate >= DateTime.Now && product.Offer.IsValid)
                        {
                            //See the father of product
                            productSizeColorDto.NewPrice =
                                (product.Offer.Value != null) ?
                                (detail.Price - product.Offer.Value) :
                                (detail.Price - (detail.Price * (product.Offer.Percentage / 100.0)));
                            productSizeColorDto.EndOfferDate = productSizeColorDto.NewPrice != null ? product.Offer.EndDate : null;
                        }

                        else if (productSizeColorDto.NewPrice == null)
                        {
                            //esle see the all
                            var offer = await Context.Offers.FirstOrDefaultAsync(o => o.CreatedBy == product.UserId && o.OfferType == 0 && o.OfferFor == 0 && DateTime.Now >= o.StartDate && o.EndDate >= DateTime.Now && o.IsValid);
                            if (offer != null)
                            {
                                productSizeColorDto.NewPrice =
                                (offer.Value != null) ?
                                (detail.Price - offer.Value) :
                                (detail.Price - (detail.Price * (offer.Percentage / 100.0)));
                                productSizeColorDto.EndOfferDate = productSizeColorDto.NewPrice != null ? offer.EndDate : null;
                            }
                        }
                        else if (product.Category.OfferId != null && productSizeColorDto.NewPrice == null && DateTime.Now >= product.Category.Offer.StartDate && product.Category.Offer.EndDate >= DateTime.Now && detail.Offer.IsValid)
                        {
                            //else see category
                            productSizeColorDto.NewPrice =
                                (product.Category.Offer.Value != null) ?
                                (detail.Price - product.Category.Offer.Value) :
                                (detail.Price - (detail.Price * (product.Category.Offer.Percentage / 100.0)));
                            productSizeColorDto.EndOfferDate = productSizeColorDto.NewPrice != null ? product.Category.Offer.EndDate : null;
                        }
                        else if (product.Brand.OfferId != null && productSizeColorDto.NewPrice == null && DateTime.Now >= product.Brand.Offer.StartDate && product.Brand.Offer.EndDate >= DateTime.Now && detail.Offer.IsValid)
                        {
                            //else see brand
                            productSizeColorDto.NewPrice =
                                (product.Brand.Offer.Value != null) ?
                                (detail.Price - product.Brand.Offer.Value) :
                                (detail.Price - (detail.Price * (product.Brand.Offer.Percentage / 100.0)));
                            productSizeColorDto.EndOfferDate = productSizeColorDto.NewPrice != null ? product.Brand.Offer.EndDate : null;
                        }
                        productSizeColorDto.Quantity = detail.Quantity;
                        productSizeColorDto.SizeText = detail.SizeText;

                        productDetailsDto.ProductSizeColors.Add(productSizeColorDto);
                    }
                    productsDetailsDto.Add(productDetailsDto);
                }
                return result.UpdateResultData(productsDetailsDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Remove

        public async Task<OperationResult<GenericOperationResult>> RemoveAsync(List<int> ids)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.Failed);

            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var data = await Context.Products
                    .Where(entity => entity.IsValid && ids.Contains(entity.Id))
                    .ToListAsync();

                if (data.Count != ids.Count)
                    return result.AddError(ErrorMessages.ProductNotFound).UpdateResultStatus(GenericOperationResult.NotFound);

                else
                {
                    Context.RemoveRange(data);
                    _ = await Context.SaveChangesAsync();
                }

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult>> RemoveProductReviewAsync(int Id)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.Failed);
            try
            {
                ProductReview reviewEntity = await Context.ProductReviews
                    .Where(review => review.Id == Id)
                    .SingleOrDefaultAsync();

                if (reviewEntity is not null)
                {
                    _ = Context.ProductReviews.Remove(reviewEntity);
                    _ = await Context.SaveChangesAsync();

                    return result.UpdateResultStatus(GenericOperationResult.Success);
                }
                else
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.ProductReviewNotFound);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult>> RemoveProductSizeColorAsync(int Id)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.Failed);
            try
            {
                var productSizeColor = await Context.ProductSizesColors
                    .Where(entity => entity.IsValid && entity.Id == Id)
                    .SingleOrDefaultAsync();

                if (productSizeColor is not null)
                {
                    _ = Context.ProductSizesColors.Remove(productSizeColor);
                    _ = await Context.SaveChangesAsync();
                    return result.UpdateResultStatus(GenericOperationResult.Success);
                }
                else
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.ProductSizeColorNotFound);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Action
        public async Task<OperationResult<GenericOperationResult, ProductFormDto>> AddProductAsync(ProductFormDto formDto)
        {
            OperationResult<GenericOperationResult, ProductFormDto> result = new(GenericOperationResult.Failed);
            try
            {
                var user = await UserRepository.GetCurrentUserInfo();
                Product entity = new()
                {
                    CategoryId = formDto.CategoryId,
                    CultureCode = formDto.CultureCode,
                    Description = formDto.Description,
                    Name = formDto.Name,
                    BrandId = formDto.BrandId,
                    UserId = user.CurrentUserId,
                    ShowInStore = formDto.ShowInStore,
                    ProductSpecifications = formDto.ProductSpecifications.Select(ps => new ProductSpecification()
                    {
                        SpecificationId = ps.SpecificationId,
                        Value = ps.Value,
                        IsValid = ps.IsValid
                    }).ToList(),
                };
                entity.ProductSizeColors.AddRange(formDto.ProductSizeColors.Select(psc => new ProductSizeColor()
                {
                    ProductId = entity.Id,
                    Price = psc.Price,
                    Quantity = psc.Quantity,
                    ColorHex = psc.ColorHex,
                    SizeText = psc.SizeText,
                }).ToList());

                if (formDto.Attachments != null)
                {
                    foreach (FileDetailsDto attachment in formDto.Attachments)
                    {
                        ProductFile file = new()
                        {
                            Checksum = attachment.Checksum,
                            FileContentType = attachment.FileContentType,
                            Description = attachment.Description,
                            Extension = attachment.Extension,
                            FileName = attachment.FileName,
                            Name = attachment.Name,
                            Url = attachment.Url,
                        };
                        entity.Files.Add(Mapper.Map<ProductFile>(file));
                    }
                }
                _ = await Context.Products.AddAsync(entity);
                _ = await Context.SaveChangesAsync();

                formDto.Attachments = entity.Files
                    .Where(file => file.IsValid)
                    .Select(file => new FileDetailsDto()
                    {
                        Id = file.Id,
                        Checksum = file.Checksum,
                        FileContentType = file.FileContentType,
                        Description = file.Description,
                        FileName = file.FileName,
                        Name = file.Name,
                        Url = file.Url
                    }).ToList();

                foreach (FileDetailsDto file in formDto.Attachments)
                    file.SourceId = entity.Id;

                formDto.Id = entity.Id;
                formDto.ProductSpecifications = entity.ProductSpecifications.Select(ps => new ProductSpecificationDto()
                {
                    Id = ps.Id,
                    SpecificationId = ps.SpecificationId,
                    Value = ps.Value,
                    IsValid = ps.IsValid

                }).ToList();
                formDto.ProductSizeColors = entity.ProductSizeColors.Select(psc => new ProductSizeColorFormDto()
                {
                    Id = psc.Id,
                    ColorHex = psc.ColorHex,
                    Price = psc.Price,
                    Quantity = psc.Quantity,
                    SizeText = psc.SizeText
                }).ToList();

                var savePointsResult = await _gamificationMovementRepo.SavePoints(new List<string>
                {
                    GamificationNames.AddProduct
                });

                if (!savePointsResult.IsSuccess)
                {
                    return result.AddError(savePointsResult.ErrorMessages).UpdateResultStatus(GenericOperationResult.Failed);
                }

                return result.UpdateResultData(formDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, ProductUpdateFormDto>> UpdateProductAsync(ProductUpdateFormDto formDto)
        {
            OperationResult<GenericOperationResult, ProductUpdateFormDto> result = new(GenericOperationResult.Failed);

            try
            {
                int entityId = formDto.Id;

                Product entity = await ValidEntities
                        .Include(entity => entity.Files)
                        .Include(entity => entity.ProductSpecifications)
                        .Where(entity => entity.Id == entityId)
                        .SingleOrDefaultAsync();

                if (entity == null)
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.ProductNotFound);

                if (formDto.RemovedFilesIds.Count > 0)
                {
                    var removedFilesdata = Context.Products
                        .Where(Product => Product.Id == formDto.Id)
                        .Include(entity => entity.Files)
                        .FirstOrDefault()
                        .Files.Where(file => formDto.RemovedFilesIds.Contains(file.Id))
                        .Select(file => new FileDetailsDto()
                        {
                            Checksum = file.Checksum,
                            FileContentType = file.FileContentType,
                            Description = file.Description,
                            FileName = file.FileName,
                            Name = file.Name,
                            Url = file.Url
                        });
                    Context.RemoveRange(Context.Files.Where(file => formDto.RemovedFilesIds.Contains(file.Id)));
                    formDto.RemovedFilesUrls = removedFilesdata.Select(files => files.Url);
                }
                //entity.Id = formDto.Id;
                entity.CategoryId = formDto.CategoryId;
                entity.BrandId = formDto.BrandId;
                entity.CultureCode = formDto.CultureCode;
                entity.Description = formDto.Description;
                entity.Name = formDto.Name;
                entity.IsValid = true;
                entity.ShowInStore = formDto.ShowInStore;

                foreach (ProductSpecificationDto item in formDto.ProductSpecifications.Where(ps => ps.Id != 0))
                {
                    ProductSpecification productSpecs = entity.ProductSpecifications.Where(pd => pd.Id == item.Id).SingleOrDefault();
                    productSpecs.IsValid = item.IsValid;
                    productSpecs.Value = item.Value;
                }
                foreach (ProductSpecification item in formDto.ProductSpecifications.Where(ps => ps.Id == 0).Select(ps => new ProductSpecification()
                {
                    Id = ps.Id,
                    SpecificationId = ps.SpecificationId,
                    Value = ps.Value,
                    IsValid = ps.IsValid

                }).ToList())
                {
                    entity.ProductSpecifications.Add(item);
                }
                foreach (FileDetailsDto attachment in formDto.Attachments)
                {
                    string extension = $".{attachment.Url.Split('.')[1]}";
                    ProductFile file = new()
                    {
                        Checksum = attachment.Checksum,
                        FileContentType = attachment.FileContentType,
                        Description = attachment.Description,
                        Extension = extension,
                        FileName = attachment.FileName,
                        Name = attachment.Name,
                        Url = attachment.Url
                    };
                    entity.Files.Add(file);
                }

                _ = Context.Update(entity);
                _ = await Context.SaveChangesAsync();

                formDto.Attachments = entity.Files
                    .Where(file => file.IsValid)
                    .Select(file => new FileDetailsDto()
                    {
                        Id = file.Id,
                        Checksum = file.Checksum,
                        FileContentType = file.FileContentType,
                        Description = file.Description,
                        FileName = file.FileName,
                        Name = file.Name,
                        Url = file.Url
                    }).ToList();
                formDto.ProductSpecifications = entity.ProductSpecifications.Select(ps => new ProductSpecificationDto()
                {
                    Id = ps.Id,
                    SpecificationId = ps.SpecificationId,
                    Value = ps.Value
                }).ToList();

                return result.UpdateResultData(formDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, ProductSizeColorDto>> AddProductSizeColorAsync(ProductSizeColorDto formDto)
        {
            OperationResult<GenericOperationResult, ProductSizeColorDto> result = new(GenericOperationResult.Failed);
            try
            {
                var product = await Context.Products
                    .Where(entity => entity.IsValid && entity.Id == formDto.ProductId)
                    .SingleOrDefaultAsync();

                if (product is not null)
                {
                    ProductSizeColor entity = new()
                    {
                        ProductId = formDto.ProductId,
                        ColorHex = formDto.ColorHex,
                        SizeText = formDto.SizeText,
                        Quantity = formDto.Quantity,
                        Price = formDto.Price,
                    };
                    _ = await Context.ProductSizesColors.AddAsync(entity);
                    _ = await Context.SaveChangesAsync();

                    return result.UpdateResultData(formDto).UpdateResultStatus(GenericOperationResult.Success);
                }
                else
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.ProductNotFound);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, ProductSizeColorDto>> UpdateProductSizeColorAsync(ProductSizeColorDto formDto)
        {
            OperationResult<GenericOperationResult, ProductSizeColorDto> result = new(GenericOperationResult.Failed);

            try
            {
                var product = await Context.ProductSizesColors
                    .Where(entity => entity.IsValid && entity.Id == formDto.Id)
                    .SingleOrDefaultAsync();

                if (product is not null)
                {
                    //product.Id = formDto.Id;
                    product.ProductId = formDto.ProductId;
                    product.ColorHex = formDto.ColorHex;
                    product.SizeText = formDto.SizeText;
                    product.Quantity = formDto.Quantity;
                    product.Price = formDto.Price;
                    _ = Context.ProductSizesColors.Update(product);

                    _ = await Context.SaveChangesAsync();
                    return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(formDto);
                }
                else
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.ProductNotFound);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, ProductReviewDto>> ActionProductReviewAsync(ProductReviewDto formDto)
        {
            OperationResult<GenericOperationResult, ProductReviewDto> result = new(GenericOperationResult.Failed);

            int userId = UserRepository.CurrentUserIdentityId();

            Product productEntity = await Context.Products
                .Where(product => product.IsValid && product.Id == formDto.ProductId)
                .SingleOrDefaultAsync();

            if (productEntity is null)
                result = new OperationResult<GenericOperationResult, ProductReviewDto>(GenericOperationResult.NotFound).AddError(ErrorMessages.ProductNotFound);
            else
            {
                _ = formDto.Id != 0
                   ? await Context.ProductReviews.AddAsync(new ProductReview() { Id = formDto.Id, Description = formDto.Description, ProductId = formDto.ProductId, Rating = formDto.Rating, UserId = UserRepository.CurrentUserIdentityId() })
                   : await Context.ProductReviews.AddAsync(new ProductReview() { ProductId = formDto.ProductId, Description = formDto.Description, Rating = formDto.Rating, UserId = userId });
            }
            _ = await Context.SaveChangesAsync();
            return result;
        }

        #endregion

        private async Task<IEnumerable<int>> GetCategoriesInCategoryAsync(int categoryId)
        {
            List<MultiLevelCategory> categories = Context.MultiLevelCategories.Include(cat => cat.Children).ToList();
            List<int> categoriesIds = new();
            MultiLevelCategory currentParent = categories.Where(cat => cat.Id == categoryId).SingleOrDefault();
            await addToListOfCategoriesAsync(currentParent, categoriesIds);
            return categoriesIds;
        }
        private async Task addToListOfCategoriesAsync(MultiLevelCategory cat, List<int> catList)
        {
            catList.Add(cat.Id);
            foreach (MultiLevelCategory item in cat.Children)
            {
                await addToListOfCategoriesAsync(item, catList);
            }
        }

        public async Task<OperationResult<GenericOperationResult>> ShowInStoreAsync(int productId, bool showInStore)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.Failed);
            try
            {
                Product productEntity = await Context.Products
                    .Where(product => product.IsValid && product.Id == productId)
                    .SingleOrDefaultAsync();

                if (productEntity is null)
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.ProductNotFound);

                productEntity.ShowInStore = showInStore;

                _ = await Context.SaveChangesAsync();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, string>> AddingProductsAfterReadingExcelFileAsync(
            IEnumerable<ProductExcelDto> products)
        {
            OperationResult<GenericOperationResult, string> result = new(GenericOperationResult.Failed);

            using IDbContextTransaction transaction = await Context.Database.BeginTransactionAsync();

            try
            {
                foreach (ProductExcelDto product in products)
                {
                    bool resultAfterValidateProperties = await ValidateRequiredPropertiesAsync(product);

                    if (!resultAfterValidateProperties)
                        return result.AddError(ErrorMessages.InternalServerError);

                    if (resultAfterValidateProperties)
                    {
                        int brandId = Context.Brand.Where(brand => brand.Name.Contains(product.Brand.Trim())).FirstOrDefault().Id;
                        int categoryId = Context.MultiLevelCategories.Where(category => category.Name.Contains(product.Category.Trim())).FirstOrDefault().Id;
                        Product productEntity = new()
                        {
                            BrandId = brandId,
                            CategoryId = categoryId,
                            Name = product.Product_Name,
                            Description = product.Product_Description,
                            ProductSpecifications = product.Specifications.Select(ps => new ProductSpecification()
                            {
                                SpecificationId = Context.Specifications.Where(specification => specification.Name.Contains(ps.Name.Trim())).FirstOrDefault().Id,
                                Value = ps.Value,
                                IsValid = true

                            }).ToList(),
                        };
                        _ = await Context.AddAsync(productEntity);
                        _ = await Context.SaveChangesAsync();
                    }
                }
                transaction.Commit();
            }
            catch (Exception)
            {
                await transaction.RollbackAsync();
                return new OperationResult<GenericOperationResult, string>(GenericOperationResult.Failed);
            }

            return new OperationResult<GenericOperationResult, string>(GenericOperationResult.Success, result.Result);
        }

        public async Task<bool> ValidateRequiredPropertiesAsync(ProductExcelDto product)
        {
            try
            {
                if (string.IsNullOrEmpty(product.Product_Name))
                {
                    return false;
                }

                if (string.IsNullOrEmpty(product.Product_Description))
                {
                    return false;
                }

                if (string.IsNullOrEmpty(product.Brand))
                {
                    return false;
                }

                if (string.IsNullOrEmpty(product.Category))
                {
                    return false;
                }

                _ = Context.Brand.Where(brand => brand.Name.Contains(product.Brand.Trim())).FirstOrDefault();

            }
            catch (Exception)
            {
                return false;
            }

            return await Task.FromResult(true);
        }

        public async Task<OperationResult<GenericOperationResult, IEnumerable<ProductExportDataDto>>> GetProductsByIdToExportToExcelFileAsync(
            IEnumerable<int> productIds)
        {
            OperationResult<GenericOperationResult, IEnumerable<ProductExportDataDto>> result = new(GenericOperationResult.Failed);

            try
            {
                List<ProductExcelDto> productList = await Context.Products
                    .Include(product => product.Brand)
                    .Include(product => product.Category)
                    .Include(produc => produc.ProductSizeColors)
                    .Include(product => product.ProductSpecifications)
                    .Where(product => productIds.Contains(product.Id))
                    .Select(product => new ProductExcelDto
                    {
                        Product_Name = product.Name,
                        Product_Description = product.Description,
                        Brand = product.Brand.Name,
                        Category = product.Category.Name,
                        ProductSizeColors = product.ProductSizeColors.Select(psc => new ProductSizeColorDto()
                        {
                            Id = psc.Id,
                            ProductId = psc.ProductId,
                            ColorHex = psc.ColorHex,
                            Price = psc.Price,
                            Quantity = psc.Quantity,
                            SizeText = psc.SizeText,
                        }).ToList(),
                        Specifications = product.ProductSpecifications.Select(ps => new SpecificationDto
                        {
                            Name = ps.Specification.Name,
                            Value = ps.Value
                        }).ToList()
                    }).ToListAsync();

                List<ProductExportDataDto> products = await ConvertFromExcelDataToExportDataAsync(productList);

                result.Result = products;

                return result;
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<List<ProductExportDataDto>> ConvertFromExcelDataToExportDataAsync(
            List<ProductExcelDto> productList)
        {
            List<ProductExportDataDto> products = new();

            foreach (ProductExcelDto product in productList)
            {
                ProductExportDataDto productExportDataDto = new()
                {
                    Product_Name = product.Product_Name,
                    Product_Description = product.Product_Description,
                    Brand = product.Brand,
                    Category = product.Category,

                };
                var stringBuilder = new StringBuilder();
                foreach (SpecificationDto specification in product.Specifications)
                {
                    _ = stringBuilder.Append(specification.Name + ":" + specification.Value + ",");
                }


                products.Add(productExportDataDto);
            }

            return await Task.FromResult(products);
        }

        public async Task<OperationResult<GenericOperationResult, IEnumerable<ProductSizeColorDto>>> GetProductSizesColorsAsync(int productId)
        {
            OperationResult<GenericOperationResult, IEnumerable<ProductSizeColorDto>> result = new(GenericOperationResult.Failed);

            try
            {
                var product = await Context.Products
                    .Where(entity => entity.IsValid && entity.Id == productId)
                    .SingleOrDefaultAsync();

                if (product is not null)
                {
                    var productSizesColors = Context.ProductSizesColors
                        .Where(entity => entity.IsValid && entity.ProductId == productId)
                        .Select(entity => new ProductSizeColorDto()
                        {
                            Id = entity.Id,
                            ProductId = entity.ProductId,
                            ColorHex = entity.ColorHex,
                            SizeText = entity.SizeText,
                            Quantity = entity.Quantity,
                            Price = entity.Price,
                            NewPrice = (entity.OfferId != null) ?
                                    (entity.Offer.Value != null) ?
                                    (entity.Price - entity.Offer.Value) :
                                    (entity.Price - (entity.Price * (entity.Offer.Percentage / 100.0))) : null,
                            EndOfferDate = entity.Offer.EndDate
                        });

                    return result.UpdateResultData(productSizesColors).UpdateResultStatus(GenericOperationResult.Success);
                }
                else
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.ProductNotFound);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
    }
}
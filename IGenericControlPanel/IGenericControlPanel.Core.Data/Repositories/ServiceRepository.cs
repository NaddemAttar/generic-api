﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using IGenericControlPanel.Communications.Dto.Feedback;
using IGenericControlPanel.Communications.Dto.TeleHealth;
using IGenericControlPanel.Communications.IData.Interfaces;
using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.Dto.HealthCareProvider;
using IGenericControlPanel.Core.Dto.Question;
using IGenericControlPanel.Core.Dto.Service;
using IGenericControlPanel.Core.IData;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.Model;
using IGenericControlPanel.Model.Content;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums.Core;
using IGenericControlPanel.SharedKernal.Extension;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using IGenericControlPanel.SharedKernal.Utils;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class ServiceRepository : BasicRepository<CPDbContext, Service>, IServiceRepository
    {
        #region Properties
        private readonly IGamificationMovementsRepository _gamificationMovementRepo;
        private readonly ITeleHealthRpository _teleHealthRepo;
        private readonly IUserLanguageRepository UserLanguageRepository;
        public readonly IUserRepository UserRepository;
        private readonly IHttpContextAccessor _context;
        #endregion

        #region Constructors

        public ServiceRepository(
            CPDbContext dbContext,
            IMapper mapper,
            IUserLanguageRepository userLanguageRepository,
            IUserRepository userRepository,
            IHttpContextAccessor context,
            IGamificationMovementsRepository gamificationMovementRepo
,
            ITeleHealthRpository teleHealthRepo) : base(dbContext, mapper)
        {
            UserLanguageRepository = userLanguageRepository;
            UserRepository = userRepository;
            _context = context;
            _gamificationMovementRepo = gamificationMovementRepo;
            _teleHealthRepo = teleHealthRepo;
        }

        #endregion

        #region Get Service Details
        public async Task<OperationResult<GenericOperationResult, ServiceDetailsDto>> GetDetailedAsync(int id, int? EnterpriseId, int? UserId)
        {
            OperationResult<GenericOperationResult, ServiceDetailsDto> result = new(GenericOperationResult.Failed);

            try
            {
                var serviceModel = await Context.Services
                     .Include(s => s.HealthCareProvidersServices.Where(hcps => hcps.IsValid)).ThenInclude(hcps => hcps.CPUser).ThenInclude(u => u.HealthCareProviderSpecialties).ThenInclude(hcpspec => hcpspec.HealthCareSpecialty)
                     .Include(s => s.HealthCareProvidersServices.Where(hcps => hcps.IsValid)).ThenInclude(hcps => hcps.UserServiceSession).ThenInclude(uss => uss.SessionType)
                     .Include(s => s.HealthCareProvidersServices.Where(hcps => hcps.IsValid)).ThenInclude(hcps => hcps.Offer)
                     .Include(entity => entity.Files)
                     .Include(entity => entity.Translations)
                     .Include(entity => entity.UserLanguage)
                     .Include(s => s.HealthCareSpecialtiesServices.Where(hcpp => hcpp.IsValid)).ThenInclude(hcpp => hcpp.HealthCareSpecialty)
                     .OrderBy(entity => entity.Order)
                     .Where(s => (s.Id == id)
                     && ((UserId != null && EnterpriseId != null) ? s.HealthCareProvidersServices.Any(hcps => hcps.EnterpriseId == EnterpriseId && hcps.CPUserId == UserId)
                     : (UserId != null) ? s.HealthCareProvidersServices.Any(hcp => hcp.CPUserId == UserId)
                     : (EnterpriseId != null) ? s.HealthCareProvidersServices.Any(hcps => hcps.CPUserId == EnterpriseId) : true))
                     .FirstOrDefaultAsync();

                if (serviceModel == null)
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.ServiceNotExist);

                var serviceDetailsDto = new ServiceDetailsDto();
                serviceDetailsDto.Id = serviceModel.Id;
                serviceDetailsDto.Name = serviceModel.Name;
                serviceDetailsDto.Description = serviceModel.Description;
                serviceDetailsDto.Order = serviceModel.Order;
                serviceDetailsDto.IsHidden = serviceModel.IsHidden;
                serviceDetailsDto.ShowFirst = serviceModel.ShowFirst;

                serviceDetailsDto.ServiceSpecialties = serviceModel.HealthCareSpecialtiesServices
                    .Select(s => new HealthCareSpecialtyDto
                    {
                        SpecialtyId = s.HealthCareSpecialtyId,
                        SpecialtyName = s.HealthCareSpecialty.Name
                    }).ToList();

                if (serviceModel.HealthCareProvidersServices.Count != 0)
                {
                    // here we grouping the Hcps under one Enterprise. to show this Enterprise like it gives this service if it has hcp produce this service.
                    var groupingHcpsForEnterprise = serviceModel.HealthCareProvidersServices
                        .Where(hcps => hcps.EnterpriseId != null)
                        .GroupBy(hcps => hcps.EnterpriseId)
                        .Select(s => new { EnterpriseId = s.Key, HCPs = s.ToList() })
                        .ToList();

                    // here we take the Individual HCPs where they produce this service not inside the Enterprise
                    var IndividualHcps = serviceModel.HealthCareProvidersServices
                        .Where(hcps => hcps.CPUser.HcpType == HcpType.Individual && hcps.EnterpriseId == null)
                        .ToList();

                    foreach (var hcps in IndividualHcps)
                    {
                        var healthCareProviderServicesDto = new HealthCareProviderServicesDto();

                        healthCareProviderServicesDto.HealthCareProviderId = hcps.CPUserId.Value;
                        healthCareProviderServicesDto.HealthCareProviderName = hcps.CPUser.FirstName + " " + hcps.CPUser.LastName;
                        healthCareProviderServicesDto.ProfilePhotoUrl = hcps.CPUser.ProfilePhotoUrl;
                        serviceDetailsDto.HealthCareProvidersServices.Add(healthCareProviderServicesDto);
                        foreach (var spec in hcps.CPUser.HealthCareProviderSpecialties)
                        {
                            healthCareProviderServicesDto.HealthCareProviderSpecialties.Add(new HealthCareSpecialtyDto
                            {
                                SpecialtyId = spec.HealthCareSpecialtyId,
                                SpecialtyName = spec.HealthCareSpecialty.Name
                            });
                        }

                        foreach (var userServiceSession in hcps.UserServiceSession)
                        {
                            var sessionTypeDto = new SessionTypeDto();
                            sessionTypeDto.UserServiceSessionId = userServiceSession.Id;
                            sessionTypeDto.SessionTypeId = userServiceSession.SessionTypeId;
                            sessionTypeDto.Name = userServiceSession.SessionType.Name;
                            sessionTypeDto.Price = userServiceSession.Price;
                            sessionTypeDto.Duration = userServiceSession.Duration;
                            if (hcps.OfferId != null && sessionTypeDto.NewPrice == null && DateTime.Now >= hcps.Offer.StartDate && hcps.Offer.EndDate >= DateTime.Now && hcps.Offer.IsValid)
                            {
                                //see it
                                sessionTypeDto.NewPrice = sessionTypeDto.Price - (sessionTypeDto.Price * (hcps.Offer.Percentage / 100.0));
                                sessionTypeDto.EndOfferDate = sessionTypeDto.NewPrice != null ? hcps.Offer.EndDate : null;
                            }
                            else if (sessionTypeDto.NewPrice == null)
                            {
                                //else see the all
                                var offer = await Context.Offers.FirstOrDefaultAsync(o => o.CreatedBy == serviceModel.CreatedBy && o.OfferType == 0 && (int)o.OfferFor == 2 && DateTime.Now >= o.StartDate && o.EndDate >= DateTime.Now && o.IsValid);
                                if (offer != null)
                                {
                                    sessionTypeDto.NewPrice = sessionTypeDto.Price - (sessionTypeDto.Price * (offer.Percentage / 100.0));
                                    sessionTypeDto.EndOfferDate = sessionTypeDto.NewPrice != null ? offer.EndDate : null;
                                }
                            }
                            healthCareProviderServicesDto.HealthCareProviderSessions.Add(sessionTypeDto);
                        }
                        serviceDetailsDto.HealthCareProvidersServices.Add(healthCareProviderServicesDto);
                    }
                    foreach (var enterpriseWithHCPs in groupingHcpsForEnterprise)
                    {
                        var enterpriseDetails = new EnterpriseDetailsDto();

                        //var enterprise = serviceModel.HealthCareProvidersServices
                        //    .Where(hcps => hcps.CPUserId == enterpriseWithHCPs.EnterpriseId.Value)
                        //    .Select(s => s.CPUser)
                        //    .FirstOrDefault();

                        var healthCareProviderService = serviceModel.HealthCareProvidersServices
                            .Where(hcps => hcps.CPUserId == enterpriseWithHCPs.EnterpriseId.Value)
                            .FirstOrDefault();

                        enterpriseDetails.EnterpriseId = enterpriseWithHCPs.EnterpriseId.Value;
                        enterpriseDetails.EnterpriseName = serviceModel.HealthCareProvidersServices
                            .Where(hcps => hcps.CPUserId == enterpriseWithHCPs.EnterpriseId.Value)
                            .Select(s => s.CPUser.FirstName + " " + s.CPUser.FirstName)
                            .FirstOrDefault();

                        var enterpriseSessions = serviceModel.HealthCareProvidersServices
                            .Where(hcps => hcps.CPUserId == enterpriseWithHCPs.EnterpriseId.Value)
                            .Select(s => s.UserServiceSession)
                            .FirstOrDefault();

                        foreach (var userServiceSession in enterpriseSessions)
                        {
                            var sessionTypeDto = new SessionTypeDto();
                            sessionTypeDto.UserServiceSessionId = userServiceSession.Id;
                            sessionTypeDto.SessionTypeId = userServiceSession.SessionTypeId;
                            sessionTypeDto.Name = userServiceSession.SessionType.Name;
                            sessionTypeDto.Price = userServiceSession.Price;
                            sessionTypeDto.Duration = userServiceSession.Duration;
                            if (healthCareProviderService.OfferId != null && sessionTypeDto.NewPrice == null && DateTime.Now >= healthCareProviderService.Offer.StartDate && healthCareProviderService.Offer.EndDate >= DateTime.Now && healthCareProviderService.Offer.IsValid)
                            {
                                //see it
                                sessionTypeDto.NewPrice = sessionTypeDto.Price - (sessionTypeDto.Price * (healthCareProviderService.Offer.Percentage / 100.0));
                                sessionTypeDto.EndOfferDate = sessionTypeDto.NewPrice != null ? healthCareProviderService.Offer.EndDate : null;
                            }
                            else if (sessionTypeDto.NewPrice == null)
                            {
                                //else see the all
                                var offer = await Context.Offers.FirstOrDefaultAsync(o => o.CreatedBy == serviceModel.CreatedBy && o.OfferType == 0 && (int)o.OfferFor == 2 && DateTime.Now >= o.StartDate && o.EndDate >= DateTime.Now && o.IsValid);
                                if (offer != null)
                                {
                                    sessionTypeDto.NewPrice = sessionTypeDto.Price - (sessionTypeDto.Price * (offer.Percentage / 100.0));
                                    sessionTypeDto.EndOfferDate = sessionTypeDto.NewPrice != null ? offer.EndDate : null;
                                }
                            }
                            enterpriseDetails.SessionTypes.Add(sessionTypeDto);
                        }
                        foreach (var hcps in enterpriseWithHCPs.HCPs)
                        {
                            var healthCareProviderServicesDto = new HealthCareProviderServicesDto();

                            healthCareProviderServicesDto.HealthCareProviderId = hcps.CPUserId.Value;
                            healthCareProviderServicesDto.HealthCareProviderName = hcps.CPUser.FirstName + " " + hcps.CPUser.LastName;
                            healthCareProviderServicesDto.ProfilePhotoUrl = hcps.CPUser.ProfilePhotoUrl;
                            serviceDetailsDto.HealthCareProvidersServices.Add(healthCareProviderServicesDto);
                            foreach (var spec in hcps.CPUser.HealthCareProviderSpecialties)
                            {
                                healthCareProviderServicesDto.HealthCareProviderSpecialties.Add(new HealthCareSpecialtyDto
                                {
                                    SpecialtyId = spec.HealthCareSpecialtyId,
                                    SpecialtyName = spec.HealthCareSpecialty.Name
                                });
                            }
                            enterpriseDetails.HealthCareProviders.Add(healthCareProviderServicesDto);
                        }
                        serviceDetailsDto.EnterpriseServices.Add(enterpriseDetails);
                    }
                }
                serviceDetailsDto.Images = serviceModel.Files.Where(file => file.IsValid).Select(image => new FileDetailsDto()
                {
                    Id = image.Id,
                    Checksum = image.Checksum,
                    FileContentType = image.FileContentType,
                    Description = image.Description,
                    Extension = image.Extension,
                    FileName = image.FileName,
                    Name = image.Name,
                    Url = image.Url,
                }).ToList();
                //foreach (var sessionType in service.SessionTypes)
                //{
                //    var sessionTypeDto = new SessionTypeDto();
                //    sessionTypeDto.Id = sessionType.Id;
                //    sessionTypeDto.Name = sessionType.Name;
                //    if (service.OfferId != null && sessionTypeDto.NewPrice == null && DateTime.Now >= service.Offer.StartDate && service.Offer.EndDate >= DateTime.Now && service.Offer.IsValid)
                //    {
                //        //see it
                //        sessionTypeDto.NewPrice = sessionType.Price - (sessionType.Price * (service.Offer.Percentage / 100.0));
                //        sessionTypeDto.EndOfferDate = sessionTypeDto.NewPrice != null ? service.Offer.EndDate : null;
                //    }
                //    else if (sessionTypeDto.NewPrice == null)
                //    {
                //        //else see the all
                //        var offer = await Context.Offers.FirstOrDefaultAsync(o => o.CreatedBy == service.CreatedBy && o.OfferType == 0 && (int)o.OfferFor == 2 && DateTime.Now >= o.StartDate && o.EndDate >= DateTime.Now && o.IsValid);
                //        if (offer != null)
                //        {
                //            sessionTypeDto.NewPrice = sessionType.Price - (sessionType.Price * (offer.Percentage / 100.0));
                //            sessionTypeDto.EndOfferDate = sessionTypeDto.NewPrice != null ? offer.EndDate : null;
                //        }
                //    }
                //    serviceDetailsDto.SessionsType.Add(sessionTypeDto);
                //}

                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(serviceDetailsDto);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<HealthCareProviderServicesDto>>> GetHealthCareProvidersByServiceId(int ServiceId)
        {
            OperationResult<GenericOperationResult, IEnumerable<HealthCareProviderServicesDto>> result = new(GenericOperationResult.Failed);
            try
            {
                var entity = await Context.HealthCareProviderServices.Where(entity => entity.IsValid &&
                entity.ServiceId == ServiceId).Select(entity => new HealthCareProviderServicesDto()
                {
                    HealthCareProviderId = entity.CPUser.Id,
                    HealthCareProviderName = entity.CPUser.FirstName + " " + entity.CPUser.LastName,
                    HealthCareProviderSpecialties = entity.CPUser.HealthCareProviderSpecialties.Where(sp => sp.IsValid)
                      .Select(sp => new Dto.HealthCareProvider.HealthCareSpecialtyDto()
                      {
                          SpecialtyId = sp.HealthCareSpecialty.Id,
                          SpecialtyName = sp.HealthCareSpecialty.Name
                      }).ToList()
                }).ToListAsync();
                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(entity);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Get All Services
        public async Task<OperationResult<GenericOperationResult, IEnumerable<ServiceDto>>> GetAllAsync(
              bool? showFirst, bool? isHidden, int? SpecialtyId, string query, int pageSize, int pageNumber, bool enablePagination, int? UserId,
              int? EnterpriseId, string cultureCode = "ar")
        {
            var result = new OperationResult<GenericOperationResult, IEnumerable<ServiceDto>>(GenericOperationResult.Failed);

            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();
                var builder = WebApplication.CreateBuilder();
                bool multiProviders = bool.Parse(builder.Configuration.GetSection("MultiProviders").Value.ToLower());
                var data = ValidEntities
                    .Include(s => s.Files)
                    .Include(s => s.Questions)
                    .Include(s => s.Translations)
                    .Include(s => s.UserLanguage)
                    .Include(s => s.HealthCareSpecialtiesServices.Where(hcpp => hcpp.IsValid)).ThenInclude(hcpp => hcpp.HealthCareSpecialty)
                    .OrderByDescending(s => s.ShowFirst)
                    .Where(s => (userInfo.CurrentUserId == 0 || userInfo.IsUserAdmin || (multiProviders == false ? true : userInfo.IsHealthCareProvider ? userInfo.CurrentUserId == s.CreatedBy : true))
                            && (query == null || s.Name.ToUpper().Contains(query.ToUpper()))
                            && (isHidden != null ? s.IsHidden == isHidden : s.IsHidden == false)
                            && ((showFirst != null && showFirst.Value == true) ? s.ShowFirst == true : true)
                            && (
                            (UserId != null && EnterpriseId != null) ? s.HealthCareProvidersServices.Any(hcps => hcps.EnterpriseId == EnterpriseId && hcps.CPUserId == UserId)
                            : (UserId != null) ? s.HealthCareProvidersServices.Any(hcp => hcp.CPUserId == UserId)
                            : (EnterpriseId != null) ? s.HealthCareProvidersServices.Any(hcps => hcps.CPUserId == EnterpriseId) : true)
                            && (SpecialtyId != null ? s.HealthCareSpecialtiesServices.Where(hcp => hcp.IsValid).Any(hcp => hcp.HealthCareSpecialtyId == SpecialtyId && hcp.HealthCareSpecialty.IsValid) : true));

                if (enablePagination)
                    data = data.GetDataWithPagination(pageSize, pageNumber);

                var servicesModel = await data.ToListAsync();

                var servicesDetailsDto = new List<ServiceDetailsDto>();
                foreach (var service in servicesModel)
                {
                    var serviceDetailsDto = new ServiceDetailsDto();
                    serviceDetailsDto.Id = service.Id;
                    serviceDetailsDto.Name = service.Name;
                    serviceDetailsDto.Description = service.Description;
                    serviceDetailsDto.Order = service.Order;
                    serviceDetailsDto.ServiceSpecialties = service.HealthCareSpecialtiesServices
                        .Select(s => new HealthCareSpecialtyDto
                        {
                            SpecialtyId = s.HealthCareSpecialtyId,
                            SpecialtyName = s.HealthCareSpecialty.Name
                        }).ToList();

                    serviceDetailsDto.Images = service.Files
                        .Where(file => file.IsValid)
                        .Select(image => new FileDetailsDto()
                        {
                            Id = image.Id,
                            Checksum = image.Checksum,
                            FileContentType = image.FileContentType,
                            Description = image.Description,
                            Extension = image.Extension,
                            FileName = image.FileName,
                            Name = image.Name,
                            Url = image.Url,
                        }).ToList();
                    serviceDetailsDto.QuestionCount = service.Questions != null ? service.Questions.Where(q => q.IsValid).Count() : 0;

                    servicesDetailsDto.Add(serviceDetailsDto);
                }

                return result.UpdateResultData(servicesDetailsDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<ServiceDetailsDto>>> GetAllDetailedAsync(int? EnterpriseId,
            bool? showFirst, bool? isHidden, int? SpecialtyId, string query, int pageSize, int pageNumber, int? UserId, bool enablePagination, string cultureCode = "ar")
        {
            var result = new OperationResult<GenericOperationResult, IEnumerable<ServiceDetailsDto>>(GenericOperationResult.Failed);

            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();
                var builder = WebApplication.CreateBuilder();
                bool multiProviders = bool.Parse(builder.Configuration.GetSection("MultiProviders").Value.ToLower());
                var data = ValidEntities
                    .Include(entity => entity.Files)
                    .Include(entity => entity.Translations)
                    .Include(s => s.HealthCareSpecialtiesServices.Where(hcpp => hcpp.IsValid)).ThenInclude(hcpp => hcpp.HealthCareSpecialty)
                    .Include(entity => entity.UserLanguage)
                    .Include(entity => entity.Blogs).ThenInclude(entity => entity.Files)
                    .Include(entity => entity.Questions).ThenInclude(entity => entity.Files)
                    .Include(entity => entity.FeedBack)
                    .Where(s => (userInfo.CurrentUserId == 0 || userInfo.IsUserAdmin
                    || (multiProviders == false ? true : userInfo.IsHealthCareProvider ? userInfo.CurrentUserId == s.CreatedBy : true))
                    && (query == null || s.Name.ToUpper().Contains(query.ToUpper()))
                    && s.IsValid
                    && (isHidden != null ? s.IsHidden == isHidden : s.IsHidden == false)
                    && (showFirst != null ? s.ShowFirst == showFirst : true)
                    && (
                    (UserId != null && EnterpriseId != null) ? s.HealthCareProvidersServices.Any(hcps => hcps.EnterpriseId == EnterpriseId && hcps.CPUserId == UserId)
                    : (UserId != null) ? s.HealthCareProvidersServices.Any(hcp => hcp.CPUserId == UserId)
                    : (EnterpriseId != null) ? s.HealthCareProvidersServices.Any(hcps => hcps.CPUserId == EnterpriseId) : true)
                    && (SpecialtyId != null ? s.HealthCareSpecialtiesServices.Where(hcp => hcp.IsValid).Any(hcp => hcp.HealthCareSpecialtyId == SpecialtyId && hcp.HealthCareSpecialty.IsValid) : true))
                    .OrderByDescending(s => s.ShowFirst)
                    .AsQueryable();

                if (enablePagination)
                    data = data.GetDataWithPagination(pageSize, pageNumber);

                var servicesModel = await data.ToListAsync();

                var servicesDetailsDto = new List<ServiceDetailsDto>();
                foreach (var service in servicesModel)
                {
                    var serviceDetailsDto = new ServiceDetailsDto();
                    serviceDetailsDto.Id = service.Id;
                    serviceDetailsDto.Name = service.Name;
                    serviceDetailsDto.IsHidden = service.IsHidden;
                    serviceDetailsDto.ShowFirst = service.ShowFirst;
                    serviceDetailsDto.Description = service.Description;
                    serviceDetailsDto.Order = service.Order;
                    serviceDetailsDto.ServiceSpecialties = service.HealthCareSpecialtiesServices
                        .Select(s => new HealthCareSpecialtyDto
                        {
                            SpecialtyId = s.HealthCareSpecialtyId,
                            SpecialtyName = s.HealthCareSpecialty.Name
                        }).ToList();
                    //if (service.HealthCareProvidersServices.Count != 0 && service.HealthCareProvidersServices != null)//this condition is bad cuz there is no service without a HCP.
                    //{
                    //    foreach (var hcp in service.HealthCareProvidersServices)
                    //    {
                    //        var cpUserSessionTypes = _teleHealthRepo.GetHealtCareProviderSessionTypesAsync(service.Id, hcp.CPUserId);
                    //        if (cpUserSessionTypes.Result.IsSuccess && cpUserSessionTypes.Result.Result.Count() == 0)
                    //            continue;
                    //        else
                    //        {
                    //            var healthCareProvidersDto = new HealthCareProviderServicesDto();
                    //            healthCareProvidersDto.HealthCareProviderId = hcp.CPUserId;
                    //            healthCareProvidersDto.HealthCareProviderName = hcp.CPUser.FirstName + " " + hcp.CPUser.LastName;
                    //            healthCareProvidersDto.ProfilePhotoUrl = hcp.CPUser.ProfilePhotoUrl;
                    //            serviceDetailsDto.HealthCareProvidersServices.Add(healthCareProvidersDto);
                    //        }
                    //    }
                    //}
                    serviceDetailsDto.Images = service.Files
                        .Where(file => file.IsValid)
                        .Select(image => new FileDetailsDto()
                        {
                            Id = image.Id,
                            Checksum = image.Checksum,
                            FileContentType = image.FileContentType,
                            Description = image.Description,
                            Extension = image.Extension,
                            FileName = image.FileName,
                            Name = image.Name,
                            Url = image.Url,
                        }).ToList();
                    serviceDetailsDto.Blogs = service.Blogs.Select(blog => new BlogDetailsDto()
                    {
                        Title = blog.Title,
                        Content = blog.Content,
                        Date = blog.BlogDate,
                        CultureCode = blog.CultureCode,
                        BlogType = blog.BlogType,

                        YouTubeLink = blog.YouTubeLink,

                        Attachments = blog.Files.Select(file => new FileDetailsDto()
                        {
                            Id = file.Id,
                            Checksum = file.Checksum,
                            FileContentType = file.FileContentType,
                            Description = file.Description,
                            FileName = file.FileName,
                            Name = file.Name,
                            Url = file.Url
                        }).ToList()
                    }).ToList();
                    serviceDetailsDto.Questions = service.Questions
                        .Select(question => new QuestionDetailsDto()
                        {
                            Id = question.Id,
                            FAQ = question.FAQ,
                            Attachments = question.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                            {
                                Id = file.Id,
                                Checksum = file.Checksum,
                                FileContentType = file.FileContentType,
                                Description = file.Description,
                                FileName = file.FileName,
                                Name = file.Name,
                                Url = file.Url
                            }).ToList(),
                            Content = question.Content,
                            CultureCode = question.CultureCode,
                            MetaDescription = question.MetaDescription,
                            Title = question.Title,
                            Date = question.CreationDate
                        }).ToList();
                    serviceDetailsDto.Feedback = service.FeedBack
                        .Select(feedback => new FeedbackDto()
                        {
                            Id = feedback.Id,
                            ClientName = feedback.ClientName,
                            Description = feedback.Description,
                            ImageUrl = feedback.ImageUrl,
                            ServiceId = feedback.ServiceId,
                            Title = feedback.Title,
                            YouTubeLink = feedback.YouTubeLink,
                            ServiceName = feedback.Service.Name
                        }).ToList();
                    servicesDetailsDto.Add(serviceDetailsDto);
                }
                return result.UpdateResultData(servicesDetailsDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Private methods for Service Action 
        public async Task<OperationResult<GenericOperationResult, ServiceFormDto>> ActionAsync(
            ServiceFormDto dto)
        {
            var result = new OperationResult<GenericOperationResult, ServiceFormDto>(GenericOperationResult.Failed);

            try
            {
                result = dto.Id == 0
                    ? await CreateAsync(dto)
                    : await UpdateAsync(dto);
                return result;
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        private async Task<OperationResult<GenericOperationResult, ServiceFormDto>> CreateAsync(
            ServiceFormDto formDto)
        {
            OperationResult<GenericOperationResult, ServiceFormDto> result = new(GenericOperationResult.Failed);
            try
            {
                var images = formDto.ImagesDetails;

                Service entityToAdd = new()
                {
                    Name = formDto.Name,
                    Description = formDto.Description,
                    IsHidden = false,
                    ShowFirst = false,
                    //Price = formDto.Price,
                    CultureCode = formDto.CultureCode,
                    Order = formDto.Order,
                    CreationDate = new DateTimeOffset()
                };

                if (images != null)
                {
                    foreach (FileDetailsDto image in images)
                    {
                        entityToAdd.Files.Add(new ServiceFile()
                        {
                            Checksum = image.Checksum,
                            FileContentType = image.FileContentType,
                            Description = image.Description,
                            Extension = image.Extension,
                            FileName = image.FileName,
                            Name = image.Name,
                            Url = image.Url,
                        });
                    }

                }

                _ = await Context.AddAsync(entityToAdd);
                foreach (var hcsId in formDto.HealthCareSpecialtyIds)
                {
                    await Context.HealthCareSpecialtyServices.AddAsync(new HealthCareSpecialtyService()
                    {
                        HealthCareSpecialtyId = hcsId.Value,
                        Service = entityToAdd
                    });
                }
                foreach (var hcpId in formDto.HealthCareProvidersIds)
                {
                    await Context.HealthCareProviderServices.AddAsync(new HealthCareProviderService()
                    {
                        CPUserId = hcpId.Value,
                        Service = entityToAdd
                    });
                }
                _ = await Context.SaveChangesAsync();

                formDto.Id = entityToAdd.Id;

                if (images is not null)
                {
                    foreach (FileDetailsDto image in images)
                    {
                        image.SourceId = entityToAdd.Id;
                    }
                }

                var savePointsResult = await _gamificationMovementRepo.SavePoints(new List<string>
                {
                    GamificationNames.AddService
                });

                if (!savePointsResult.IsSuccess)
                    return result.AddError(savePointsResult.ErrorMessages).UpdateResultStatus(GenericOperationResult.Failed);

                return result.UpdateResultData(formDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        private async Task<OperationResult<GenericOperationResult, ServiceFormDto>> UpdateAsync(ServiceFormDto formDto)
        {
            OperationResult<GenericOperationResult, ServiceFormDto> result = new(GenericOperationResult.Failed);
            try
            {
                var images = formDto.ImagesDetails;

                int entityId = formDto?.Id ??
                    formDto.TranslationsFomrsDto.FirstOrDefault()?.ServiceId ?? 0;

                Service data = await ValidEntities
                    .Include(entity => entity.Files)
                    .Where(entity => entity.Id == entityId)
                    .SingleOrDefaultAsync();

                if (formDto != null)
                {
                    if (formDto.RemovedFilesIdss.Count() > 0)
                    {
                        List<FileBase> removedFiles = Context.Files.Where(file => formDto.RemovedFilesIdss.Contains(file.Id)).ToList();
                        Context.RemoveRange(removedFiles);
                        formDto.RemovedFilesUrls = removedFiles.Select(file => file.Url).ToList();
                    }
                    Context.Entry(data).State = EntityState.Detached;

                    Service entityToEdit = new()
                    {
                        Id = formDto.Id,
                        Name = formDto.Name,
                        Description = formDto.Description,
                        //Price = formDto.Price,
                        CultureCode = formDto.CultureCode,
                        Order = formDto.Order,
                        IsValid = true,
                        CreatedBy = data.CreatedBy,
                        IsHidden = formDto.IsHidden,
                        ShowFirst = formDto.ShowFirst,
                    };


                    if (images.Count != 0)
                    {
                        foreach (FileDetailsDto image in images)
                        {
                            entityToEdit.Files.Add(new ServiceFile()
                            {
                                Checksum = image.Checksum,
                                FileContentType = image.FileContentType,
                                Description = image.Description,
                                Extension = image.Extension,
                                FileName = image.FileName,
                                Name = image.Name,
                                Url = image.Url,
                            });
                        }

                    }

                    Context.HealthCareSpecialtyServices.RemoveRange
                        (await Context.HealthCareSpecialtyServices
                        .Where(entity => entity.ServiceId == entityToEdit.Id
                       ).ToListAsync());

                    Context.HealthCareProviderServices.RemoveRange
                        (await Context.HealthCareProviderServices
                        .Where(entity => entity.ServiceId == entityToEdit.Id
                       ).ToListAsync());
                    _ = Context.Update(entityToEdit);
                    _ = await Context.SaveChangesAsync();
                    foreach (var hcsId in formDto.HealthCareSpecialtyIds)
                    {
                        await Context.HealthCareSpecialtyServices.AddAsync(new HealthCareSpecialtyService()
                        {
                            HealthCareSpecialtyId = hcsId.Value,
                            Service = entityToEdit
                        });
                    }
                    foreach (var hcpId in formDto.HealthCareProvidersIds)
                    {
                        await Context.HealthCareProviderServices.AddAsync(new HealthCareProviderService()
                        {
                            CPUserId = hcpId.Value,
                            Service = entityToEdit
                        });
                    }

                    formDto.ImagesDetails = entityToEdit.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                    {
                        Checksum = file.Checksum,
                        FileContentType = file.FileContentType,
                        Description = file.Description,
                        Extension = file.Extension,
                        FileName = file.FileName,
                        Name = file.Name,
                        Url = file.Url,
                        Id = file.Id
                    }).ToList();
                    await Context.SaveChangesAsync();

                    return result.UpdateResultData(formDto).UpdateResultStatus(GenericOperationResult.Success);
                }
                else
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.ServiceNotExist);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Remove Service
        public async Task<OperationResult<GenericOperationResult>> RemoveAsync(int id)
        {
            var result = new OperationResult<GenericOperationResult>(GenericOperationResult.Failed);

            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                Service serviceEntity = await ValidEntities.SingleOrDefaultAsync(service => service.Id == id);

                if (serviceEntity is null)
                    return result.AddError(ErrorMessages.ServiceNotExist).UpdateResultStatus(GenericOperationResult.NotFound);


                _ = Context.Remove(serviceEntity);
                _ = await Context.SaveChangesAsync();

                return result;
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }


        public async Task<OperationResult<GenericOperationResult>> AddToMyServices(int ServiceId)
        {
            var result = new OperationResult<GenericOperationResult>(GenericOperationResult.Failed);

            try
            {
                var userId = UserRepository.CurrentUserIdentityId();

                Service serviceEntity = await ValidEntities.SingleOrDefaultAsync(service => service.Id == ServiceId);

                if (serviceEntity is null)
                    return result.AddError(ErrorMessages.ServiceNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                var ServiceProvider = new HealthCareProviderService()
                {
                    CPUserId = userId,
                    ServiceId = serviceEntity.Id
                };

                _ = await Context.AddAsync(ServiceProvider);
                _ = await Context.SaveChangesAsync();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult>> RemoveFromMyServices(int ServiceId)
        {
            var result = new OperationResult<GenericOperationResult>(GenericOperationResult.Failed);

            try
            {
                var userId = UserRepository.CurrentUserIdentityId();

                var serviceEntity = await ValidEntities.AnyAsync(service => service.Id == ServiceId);

                if (!serviceEntity)
                    return result.AddError(ErrorMessages.ServiceNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                var ServiceProviderEntity = await Context.HealthCareProviderServices
                    .Where(sp => sp.IsValid && sp.CPUserId == userId && sp.ServiceId == ServiceId)
                    .SingleOrDefaultAsync();

                if (ServiceProviderEntity is null)
                    return result.AddError(ErrorMessages.ServiceProviderDoesNotExist).UpdateResultStatus(GenericOperationResult.NotFound);


                _ = Context.Remove(ServiceProviderEntity);
                _ = await Context.SaveChangesAsync();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        #endregion
    }
}

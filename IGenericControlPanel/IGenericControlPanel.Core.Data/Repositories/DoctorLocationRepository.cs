﻿using System;
using System.Collections.Generic;
using System.Linq;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Location;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class DoctorLocationRepository : BasicRepository<CPDbContext, DoctorLocation>, IDoctorLocationRepository
    {
        public DoctorLocationRepository(CPDbContext context) : base(context)
        {
        }

        public OperationResult<GenericOperationResult, DoctorLocationDto> ActionDoctorLocation(
            DoctorLocationDto formDto)
        {
            OperationResult<GenericOperationResult,
                DoctorLocationDto> result = new(GenericOperationResult.Success);

            try
            {
                var LocationEntity = Context.DoctorLocations
                    .Where(entity => entity.Id == formDto.Id)
                    .SingleOrDefault();

                if (LocationEntity != null)
                {
                    LocationEntity.Latitude = formDto.Latitude;
                    LocationEntity.Longitude = formDto.Longitude;
                    LocationEntity.AddressInfo = formDto.AddressInfo;
                }
                else
                {
                    LocationEntity = new DoctorLocation()
                    {
                        Latitude = formDto.Latitude,
                        Longitude = formDto.Longitude,
                        AddressInfo = formDto.AddressInfo,
                    };
                    Context.DoctorLocations.Add(LocationEntity);
                }
                Context.SaveChanges();

                return result.UpdateResultData(formDto);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        public OperationResult<GenericOperationResult, IEnumerable<DoctorLocationDto>> GetAllDoctorLocations()
        {
            OperationResult<GenericOperationResult,
                IEnumerable<DoctorLocationDto>> result = new(GenericOperationResult.ValidationError);

            try
            {
                var DoctorLocations = ValidEntities
                .Select(entity => new DoctorLocationDto()
                {
                    Id = entity.Id,
                    AddressInfo = entity.AddressInfo,
                    Latitude = entity.Latitude,
                    Longitude = entity.Longitude
                }).ToList();

                return result.UpdateResultData(DoctorLocations)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        public OperationResult<GenericOperationResult, DoctorLocationDto> GetDoctorLocation(int Id)
        {
            OperationResult<GenericOperationResult,
                DoctorLocationDto> result = new(GenericOperationResult.ValidationError);

            try
            {
                var doctorLocationDto = Context.DoctorLocations
                    .Where(entity => entity.Id == Id)
                    .Select(entity => new DoctorLocationDto()
                    {
                        Id = entity.Id,
                        AddressInfo = entity.AddressInfo,
                        Latitude = entity.Latitude,
                        Longitude = entity.Longitude,
                    }).SingleOrDefault();

                if (doctorLocationDto is null)
                {
                    return result.AddError(ErrorMessages.DoctorLocationNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                return result.UpdateResultData(doctorLocationDto)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public OperationResult<GenericOperationResult> RemoveDoctorLocation(int Id)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.ValidationError);

            try
            {
                var entity = ValidEntities
                    .Where(entity => entity.Id == Id)
                    .SingleOrDefault();

                if (entity is null)
                {
                    return result.AddError(ErrorMessages.DoctorLocationNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                Context.Remove(entity);
                Context.SaveChanges();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
    }
}

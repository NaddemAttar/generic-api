﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Surger;
using IGenericControlPanel.Core.Dto.Surgery;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Extension;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class SurgeryRepository : BasicRepository<CPDbContext, Surgery>, ISurgeryRepository
    {
        #region Properties & Constructor
        private IUserRepository UserRepository { get; }
        public SurgeryRepository(
            CPDbContext context,
            IMapper mapper,
            IUserRepository userRepository) : base(context, mapper)
        {
            UserRepository = userRepository;
        }
        #endregion

        #region Surgery Action
        public OperationResult<GenericOperationResult, SurgeryDto> Action(SurgeryDto surgeryDto)
        {
            OperationResult<GenericOperationResult,
                SurgeryDto> result = new(GenericOperationResult.ValidationError);

            if (string.IsNullOrEmpty(surgeryDto.Name))
                return result.AddError(ErrorMessages.SurgeryNameRequired);

            if (string.IsNullOrEmpty(surgeryDto.Description))
                return result.AddError(ErrorMessages.SurgeryDescriptionRequired);

            try
            {
                surgeryDto = surgeryDto.Id == 0 ?
                    AddSurgery(surgeryDto) :
                    UpdateSurgery(surgeryDto);

                return result.UpdateResultData(surgeryDto)
                .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        #region Add Surgery
        private SurgeryDto AddSurgery(SurgeryDto surgeryDto)
        {
            Surgery entity = new()
            {
                Name = surgeryDto.Name,
                Description = surgeryDto.Description,
            };
            if (surgeryDto.Images != null)
            {
                entity = AddingSurgeryFiles(surgeryDto.Images, entity);
            }
            Context.Surgeries.Add(entity);
            if (surgeryDto.Images is not null)
            {
                foreach (FileDetailsDto image in surgeryDto.Images)
                {
                    image.SourceId = entity.Id;
                }

            }
            Context.SaveChanges();
            surgeryDto.Id = entity.Id;
            return surgeryDto;
        }
        private Surgery AddingSurgeryFiles(
            IEnumerable<FileDetailsDto> images, Surgery entity)
        {
            foreach (FileDetailsDto image in images)
            {
                entity.Files.Add(new SurgeryFile()
                {
                    Checksum = image.Checksum,
                    FileContentType = image.FileContentType,
                    Description = image.Description,
                    Extension = image.Extension,
                    FileName = image.FileName,
                    Name = image.Name,
                    Url = image.Url,
                });
            }

            return entity;
        }
        #endregion

        #region Update Surgery
        private SurgeryDto UpdateSurgery(SurgeryDto surgeryDto)
        {
            Surgery entity = new()
            {
                Id = surgeryDto.Id,
                Name = surgeryDto.Name,
                Description = surgeryDto.Description,
                IsValid = true
            };
            if (surgeryDto.RemovedFilesIds.Count() > 0)
            {
                surgeryDto = RemoveSurgeryFiles(surgeryDto);
            }
            Context.Entry(entity).State = EntityState.Detached;

            if (surgeryDto.Images.Count != 0)
            {
                entity = AddingSurgeryFiles(surgeryDto.Images, entity);
            }
            Context.Surgeries.Update(entity);
            Context.SaveChanges();
            return surgeryDto;
        }
        private SurgeryDto RemoveSurgeryFiles(SurgeryDto surgeryDto)
        {
            var removedFiles = Context.Files
                  .Where(file => surgeryDto.RemovedFilesIds.Contains(file.Id))
                  .ToList();

            Context.RemoveRange(removedFiles);
            surgeryDto.RemovedFilesUrls = removedFiles
                .Select(file => file.Url)
                .ToList();

            return surgeryDto;
        }
        #endregion

        #endregion

        #region Patient Surgery Action
        public OperationResult<GenericOperationResult, PatientSurgeryDto> ActionPatientSurgery(PatientSurgeryDto patientSurgeryDto)
        {
            OperationResult<GenericOperationResult,
                PatientSurgeryDto> result = new(GenericOperationResult.ValidationError);
            try
            {
                patientSurgeryDto.UserId = patientSurgeryDto.UserId != 0 ?
                  patientSurgeryDto.UserId :
                  UserRepository.CurrentUserIdentityId();

                patientSurgeryDto = patientSurgeryDto.Id == 0 ?
                 AddPatientSurgery(patientSurgeryDto) :
                 UpdatePatientSurgery(patientSurgeryDto);

                if (patientSurgeryDto is null)
                {
                    return result.AddError(ErrorMessages.SurgeryNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                return result.UpdateResultData(patientSurgeryDto)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        #region Add Patient Surgery
        private PatientSurgeryDto AddPatientSurgery(PatientSurgeryDto patientSurgeryDto)
        {
            var patientSurgeryEntity = new PatientSurgery()
            {
                UserId = patientSurgeryDto.UserId,
                SurgeryId = patientSurgeryDto.SurgeryId,
                Description = patientSurgeryDto.Description
            };

            if (patientSurgeryDto.Images.Count > 0)
                    patientSurgeryEntity = AddingPatientSurgeryFiles(patientSurgeryDto.Images, patientSurgeryEntity);
            
            Context.Add(patientSurgeryEntity);
            Context.SaveChanges();

            return patientSurgeryDto;
        }
        private PatientSurgery AddingPatientSurgeryFiles(
             IEnumerable<FileDetailsDto> fileDetailsDto, PatientSurgery entity)
        {
            foreach (FileDetailsDto image in fileDetailsDto)
            {
                entity.Files.Add(new PatientSurgeryFile()
                {
                    Checksum = image.Checksum,
                    FileContentType = image.FileContentType,
                    Description = image.Description,
                    Extension = image.Extension,
                    FileName = image.FileName,
                    Name = image.Name,
                    Url = image.Url,
                });
            }
            return entity;
        }
        #endregion

        #region Update Patient Surgery
        private PatientSurgeryDto UpdatePatientSurgery(PatientSurgeryDto patientSurgeryDto)
        {
            var patientSurgeryEntity = Context.PatientSurgeries
                .Where(entity => entity.IsValid && entity.Id == patientSurgeryDto.Id)
                .Include(entity => entity.Files)
                .SingleOrDefault();

            if (patientSurgeryEntity is null) return null;

            if (patientSurgeryDto.RemovedFilesIds.Count() > 0)
            {
                patientSurgeryDto = RemovePatientSurgeryFiles(patientSurgeryDto, patientSurgeryEntity);
            }

            if (patientSurgeryDto.Images.Count > 0)
            {
                patientSurgeryEntity = AddingPatientSurgeryFiles(patientSurgeryDto.Images, patientSurgeryEntity);
            }

            patientSurgeryEntity.Description = patientSurgeryDto.Description;
            Context.PatientSurgeries.Update(patientSurgeryEntity);
            Context.SaveChanges();

            return patientSurgeryDto;
        }
        private PatientSurgeryDto RemovePatientSurgeryFiles(
            PatientSurgeryDto patientSurgeryDto, PatientSurgery patientSurgeryEntity)
        {
            var removedFiles = patientSurgeryEntity.Files
                   .Where(file => patientSurgeryDto.RemovedFilesIds.Contains(file.Id))
                   .ToList();

            Context.RemoveRange(removedFiles);
            patientSurgeryDto.RemovedFilesUrls = removedFiles.Select(file => file.Url).ToList();

            return patientSurgeryDto;
        }
        #endregion

        #endregion

        #region Get
        public OperationResult<GenericOperationResult, IEnumerable<SurgeryDto>> GetAllSurgeries(
            int? userId, bool enablePagination, int pageSize, int pageNumber)
        {
            OperationResult<GenericOperationResult,
                IEnumerable<SurgeryDto>> result = new(GenericOperationResult.ValidationError);

            try
            {
                var Surgeries = Context.Surgeries
                        .Where(entity => entity.IsValid
                        && (!userId.HasValue
                        || entity.PersonSurgers
                        .Any(ps => ps.UserId == userId)));

                if (enablePagination)
                {
                    Surgeries = Surgeries.GetDataWithPagination(pageSize, pageNumber);
                }

                var resultData = Surgeries.Select(entity =>
                       new SurgeryDto()
                       {
                           Id = entity.Id,
                           Name = entity.Name,
                           Description = entity.Description,
                           Images = entity.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                           {
                               Id = file.Id,
                               Checksum = file.Checksum,
                               FileContentType = file.FileContentType,
                               Description = file.Description,
                               FileName = file.FileName,
                               Name = file.Name,
                               Url = file.Url,
                               Extension = file.Extension
                           }).ToList()
                       }).ToList();

                return result.UpdateResultData(resultData)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        public async Task<OperationResult<GenericOperationResult, SurgeryInfoDto>> GetSurgery(int Id)
        {
            OperationResult<GenericOperationResult,
                SurgeryInfoDto> result = new(GenericOperationResult.ValidationError);
            try
            {
                var Surger = await Context.Surgeries
                    .Where(surger => surger.IsValid && surger.Id == Id)
                    .Select(entity => new SurgeryInfoDto()
                    {
                        Id = entity.Id,
                        Name = entity.Name,
                        Description = entity.Description,
                        PersonSurgers = Context.PatientSurgeries
                        .Where(p => p.IsValid && p.SurgeryId == Id)
                        .Select(surger => new PersonSurgeryDto()
                        {
                            Id = surger.Id,
                            UserId = surger.UserId ?? 0,
                            SurgerId = surger.SurgeryId
                        }).ToList(),
                        Images = entity.Files.Where(file => file.IsValid)
                        .Select(file => new FileDetailsDto()
                        {
                            Id = file.Id,
                            Checksum = file.Checksum,
                            FileContentType = file.FileContentType,
                            Description = file.Description,
                            FileName = file.FileName,
                            Name = file.Name,
                            Url = file.Url,
                            Extension = file.Extension
                        }).ToList()
                    }).SingleOrDefaultAsync();

                if (Surger == null)
                {
                    return result.AddError(ErrorMessages.SurgeryNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                return result.UpdateResultData(Surger)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<PatientSurgeryDto>>> GetAllPatientSurgeries(
            int? userId, bool enablePagination, int pageSize, int pageNumber)
        {
            OperationResult<GenericOperationResult,
                IEnumerable<PatientSurgeryDto>> result = new(GenericOperationResult.ValidationError);

            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var data = Context.PatientSurgeries
                        .Where(ps => ps.IsValid && (!userId.HasValue && userInfo.CurrentUserId == ps.UserId
                       || ((userInfo.IsUserAdmin || userInfo.IsNormalUser) && ps.UserId == userId)
                       || (userInfo.IsHealthCareProvider && (userId == ps.CreatedBy && userId == ps.UserId)
                       || (userInfo.CurrentUserId == ps.CreatedBy && userId == ps.UserId))));

                if (enablePagination)
                {
                    data = data.GetDataWithPagination(pageSize, pageNumber);
                }

                var resultData = data.Select(ps => new PatientSurgeryDto()
                {
                    Id = ps.Id,
                    UserId = ps.UserId ?? 0,
                    SurgeryId = ps.SurgeryId,
                    SurgeryName = ps.Surgery.Name,
                    Description = ps.Description,
                    Images = ps.Files.Where(file => file.IsValid)
                    .Select(file => new FileDetailsDto()
                    {
                        Id = file.Id,
                        Checksum = file.Checksum,
                        FileContentType = file.FileContentType,
                        Description = file.Description,
                        FileName = file.FileName,
                        Name = file.Name,
                        Url = file.Url,
                        Extension = file.Extension
                    }).ToList(),
                }).ToList();

                return result.UpdateResultData(resultData)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public OperationResult<GenericOperationResult, PatientSurgeryDto> GetPatientSurgery(int id)
        {
            OperationResult<GenericOperationResult,
                PatientSurgeryDto> result = new(GenericOperationResult.ValidationError);

            try
            {
                var data = Context.PatientSurgeries
                    .Where(ps => ps.IsValid && ps.Id == id)
                    .Select(ps => new PatientSurgeryDto()
                    {
                        Id = ps.Id,
                        UserId = ps.UserId ?? 0,
                        SurgeryId = ps.SurgeryId,
                        Description = ps.Description,
                        Images = ps.Files.Where(file => file.IsValid)
                        .Select(file => new FileDetailsDto()
                        {
                            Id = file.Id,
                            Checksum = file.Checksum,
                            FileContentType = file.FileContentType,
                            Description = file.Description,
                            FileName = file.FileName,
                            Name = file.Name,
                            Url = file.Url,
                            Extension = file.Extension
                        }).ToList(),
                    }).FirstOrDefault();

                if (data is null)
                {
                    return result.AddError(ErrorMessages.PatientSurgeryNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                return result.UpdateResultData(data)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public OperationResult<GenericOperationResult, IEnumerable<SurgerySelectDto>> GetSurgeriesAutoComplete(string query)
        {
            OperationResult<GenericOperationResult,
                IEnumerable<SurgerySelectDto>> result = new(GenericOperationResult.ValidationError);

            try
            {
                var data = ValidEntities
                    .Where(all => all.Name.ToUpper().Contains(query)
                    || all.Description.ToUpper().Contains(query))
                    .Select(all => new SurgerySelectDto()
                    {
                        Id = all.Id,
                        Name = all.Name
                    });

                return result.UpdateResultData(data)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion

        #region Remove
        public OperationResult<GenericOperationResult> RemoveSurgery(int Id)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.ValidationError);

            try
            {
                var PatientSurgeries = Context.PatientSurgeries
              .Where(surger => surger.IsValid && surger.SurgeryId == Id)
              .ToList();

                Context.RemoveRange(PatientSurgeries);

                var surger = Context.Surgeries
                    .Where(surger => surger.IsValid && surger.Id == Id)
                    .SingleOrDefault();

                if (surger is null)
                {
                    return result.AddError(ErrorMessages.SurgeryNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                Context.Surgeries.Remove(surger);
                Context.SaveChanges();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public async Task<OperationResult<GenericOperationResult>> RemovePatientSurgery(int patientSurgeryId)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.ValidationError);

            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var patientSurgery = Context.PatientSurgeries
                    .Where(ps => ps.IsValid
                    && (userInfo.IsUserAdmin || userInfo.CurrentUserId == ps.CreatedBy)
                    && ps.Id == patientSurgeryId)
                    .SingleOrDefault();

                if (patientSurgery is null)
                {
                    return result.AddError(ErrorMessages.PatientSurgeryNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                Context.PatientSurgeries.Remove(patientSurgery);
                Context.SaveChanges();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion
    }
}

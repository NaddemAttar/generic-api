﻿
using AutoMapper;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Content.Dto.Tips;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.Dto.PostType;
using IGenericControlPanel.Core.Dto.Question;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;

using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class PostTypeRepository : BasicRepository<CPDbContext, PostType>, IPostTypeRepository
    {
        public PostTypeRepository(CPDbContext context, IMapper mapper) : base(context, mapper)
        {
        }

        #region Action
        public OperationResult<GenericOperationResult, PostTypeDto> ActionPostType(PostTypeDto formDto)
        {

            OperationResult<GenericOperationResult, PostTypeDto> result = new(GenericOperationResult.ValidationError);
            try
            {
                PostType PostTypeEntity = ValidEntities.Where(entity => entity.Id == formDto.Id).SingleOrDefault();
                if (PostTypeEntity != null)//Update
                {
                    PostTypeEntity.TypeName = formDto.TypeName;
                }
                else //Add
                {
                    PostTypeEntity = new PostType()
                    {
                        TypeName = formDto.TypeName
                    };
                    _ = Context.PostTypes.Add(PostTypeEntity);
                }
                _ = Context.SaveChanges();
                formDto.Id = PostTypeEntity.Id;
                return result.UpdateResultData(formDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion

        #region GET
        public OperationResult<GenericOperationResult, IEnumerable<PostTypeDetailsDto>> GetPostTypes(bool WithBlogs, bool WithQustions, int pageNumber = 0, int pageSize = 5)
        {
            OperationResult<GenericOperationResult, IEnumerable<PostTypeDetailsDto>> result = new(GenericOperationResult.ValidationError);
            try
            {
                List<PostTypeDetailsDto> PostTypeList = ValidEntities.Where(pt => pt.Blogs.Any(blog => blog.IsValid)).Skip(pageNumber * pageSize).Take(pageSize).Select(entity => new PostTypeDetailsDto()
                {
                    Id = entity.Id,
                    TypeName = entity.TypeName,
                    Tip = Context.Tips.Where(tip => tip.IsValid).Select(tip => new TipsDto()
                    {
                        Title = tip.Title,
                        Description = tip.Description,
                    }).FirstOrDefault(),
                    BlogDtos = WithBlogs == true ? entity.Blogs.Where(blog => blog.IsValid).OrderByDescending(entity => entity.CreationDate).Take(5).Select(blog => new BlogDetailsDto()
                    {
                        BlogType = blog.BlogType,
                        Attachments = blog.Files.Select(entity => new FileDetailsDto
                        {
                            Id = entity.Id,
                            Checksum = entity.Checksum,
                            FileContentType = entity.FileContentType,
                            Description = entity.Description,
                            FileName = entity.FileName,
                            Name = entity.Name,
                            Url = entity.Url,
                            Extension = entity.Extension
                        }).ToList(),
                        Content = blog.Content,
                        CultureCode = blog.CultureCode,
                        Id = blog.Id,
                        MetaDescription = blog.MetaDescription,
                        PostTypeId = blog.PostTypeId.Value,
                        Title = blog.Title,
                        UrlNumbering = blog.UrlNumbering,
                        YouTubeLink = blog.YouTubeLink,
                    }) : null,
                    QuestionDtos = WithQustions == true ? entity.Questions.Where(question => question.IsValid)
                    .OrderByDescending(entity => entity.CreationDate).Take(5)
                    .Select(qustion => new QuestionDetailsDto()
                    {
                        Id = qustion.Id,
                        MetaDescription = qustion.MetaDescription,
                        Content = qustion.Content,
                        CultureCode = qustion.CultureCode,
                        FAQ = qustion.FAQ,
                        Title = qustion.Title,
                        UrlNumbering = qustion.UrlNumbering,
                        Attachments = qustion.Files.Select(entity => new FileDetailsDto
                        {
                            Id = entity.Id,
                            Checksum = entity.Checksum,
                            FileContentType = entity.FileContentType,
                            Description = entity.Description,
                            FileName = entity.FileName,
                            Name = entity.Name,
                            Url = entity.Url,
                            Extension = entity.Extension
                        }).ToList(),

                    }) : null
                }).ToList();

                return result.UpdateResultData(PostTypeList).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }

        }
        public OperationResult<GenericOperationResult, IEnumerable<PostTypeDto>> GetAllPostType()
        {

            OperationResult<GenericOperationResult, IEnumerable<PostTypeDto>> result = new(GenericOperationResult.ValidationError);
            try
            {
                List<PostTypeDto> PostTypes = ValidEntities
               .Select(entity => new PostTypeDto
               {
                   Id = entity.Id,
                   TypeName = entity.TypeName,

               }).ToList();

                return result.UpdateResultData(PostTypes)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                     .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public OperationResult<GenericOperationResult, PostTypeDto> GetPostType(int Id)
        {
            OperationResult<GenericOperationResult, PostTypeDto> result = new(GenericOperationResult.ValidationError);
            PostType post = Context.PostTypes.Where(post => post.IsValid && post.Id == Id).SingleOrDefault();

            if (post != null)
            {
                PostTypeDto formDto = new() { Id = post.Id, TypeName = post.TypeName };
                return result.UpdateResultData(formDto)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            else
            {
                return result.AddError(ErrorMessages.PostTypeNotFound)
                     .UpdateResultStatus(GenericOperationResult.NotFound);
            }
        }
        #endregion

        #region Remove
        public OperationResult<GenericOperationResult> RemovePostType(int Id)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.ValidationError);
            PostType post = Context.PostTypes.Include(post => post.Questions).Include(post => post.Blogs).SingleOrDefault(post => post.Id == Id);

            if (post != null)
            {
                _ = Context.PostTypes.Remove(post);
                _ = Context.SaveChanges();
                return result
                   .UpdateResultStatus(GenericOperationResult.Success);
            }
            else
            {
                return result.AddError(ErrorMessages.PostTypeNotFound)
                    .UpdateResultStatus(GenericOperationResult.NotFound);
            }
        }
        #endregion
    }
}

﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Diseases;
using IGenericControlPanel.Core.Dto.Medicine;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Extension;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class DiseasesRepository : BasicRepository<CPDbContext, Diseases>, IDiseasesRepository
    {
        #region Properties & Constructor
        private IUserRepository UserRepository { get; }
        public DiseasesRepository(
            CPDbContext context,
            IUserRepository userRepository) : base(context)
        {
            UserRepository = userRepository;
        }

        #endregion

        #region Disease Action
        public async Task<OperationResult<GenericOperationResult, DiseasesFormDto>> Action(DiseasesFormDto diseasesFormDto)
        {
            OperationResult<GenericOperationResult, DiseasesFormDto> result = new(GenericOperationResult.Failed);

            try
            {
                diseasesFormDto = diseasesFormDto.Id == 0 ?
                    await AddDisease(diseasesFormDto) :
                    await UpdateDisease(diseasesFormDto);

                if (diseasesFormDto is null)
                    return result.AddError(ErrorMessages.DiseaseNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                return result.UpdateResultData(diseasesFormDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        #region Add Disease
        private async Task<DiseasesFormDto> AddDisease(DiseasesFormDto diseaseFormDto)
        {
            Diseases entity = new()
            {
                Name = diseaseFormDto.Name,
                Description = diseaseFormDto.Description,
                DiseaseType = diseaseFormDto.DiseaseType,
            };

            if (diseaseFormDto.Images != null)
                entity = AddingDiseaseFiles(diseaseFormDto.Images, entity);

            await Context.Diseases.AddAsync(entity);

            if (diseaseFormDto.Images is not null)
                foreach (FileDetailsDto image in diseaseFormDto.Images)
                    image.SourceId = entity.Id;

            await Context.SaveChangesAsync();
            diseaseFormDto.Id = entity.Id;
            return diseaseFormDto;
        }

        private Diseases AddingDiseaseFiles(
            IEnumerable<FileDetailsDto> images, Diseases entity)
        {
            foreach (FileDetailsDto image in images)
            {
                entity.Files.Add(new DiseaseFile()
                {
                    Checksum = image.Checksum,
                    FileContentType = image.FileContentType,
                    Description = image.Description,
                    Extension = image.Extension,
                    FileName = image.FileName,
                    Name = image.Name,
                    Url = image.Url,
                });
            }

            return entity;
        }
        #endregion

        #region Update Disease
        private async Task<DiseasesFormDto> UpdateDisease(DiseasesFormDto diseasesFormDto)
        {
            var entity = await Context.Diseases
                   .Where(d => d.IsValid && d.Id == diseasesFormDto.Id)
                   .SingleOrDefaultAsync();

            if (entity is null)
                return null;

            //entity.Id = diseasesFormDto.Id;
            entity.Name = diseasesFormDto.Name;
            entity.Description = diseasesFormDto.Description;
            entity.DiseaseType = diseasesFormDto.DiseaseType;

            if (diseasesFormDto.RemovedFilesIds.Count() > 0)
                diseasesFormDto = RemoveDiseaseFiles(diseasesFormDto);

            if (diseasesFormDto.Images.Count != 0)
                entity = AddingDiseaseFiles(diseasesFormDto.Images, entity);

            Context.Diseases.Update(entity);
            await Context.SaveChangesAsync();

            return diseasesFormDto;
        }
        private DiseasesFormDto RemoveDiseaseFiles(DiseasesFormDto diseasesFormDto)
        {
            var removedFiles = Context.Files
                    .Where(file => diseasesFormDto.RemovedFilesIds.Contains(file.Id))
                    .ToList();

            Context.RemoveRange(removedFiles);
            diseasesFormDto.RemovedFilesUrls = removedFiles
                .Select(file => file.Url)
                .ToList();

            return diseasesFormDto;
        }
        #endregion

        #endregion

        #region Patient Disease Action 
        public async Task<OperationResult<GenericOperationResult, PatientDiseasesDto>> ActionPatientDiseases(PatientDiseasesDto patientDiseasesDto)
        {
            OperationResult<GenericOperationResult, PatientDiseasesDto> result = new(GenericOperationResult.Failed);

            if (patientDiseasesDto.DiseaseId == 0)
                return result.AddError(ErrorMessages.MustChooseDisease).UpdateResultStatus(GenericOperationResult.ValidationError);

            patientDiseasesDto.UserId = patientDiseasesDto.UserId != 0 ?
                patientDiseasesDto.UserId :
                UserRepository.CurrentUserIdentityId();
            try
            {
                var diseaseExist = await Context.Diseases.AnyAsync(d => d.Id == patientDiseasesDto.DiseaseId && d.IsValid);
                if (!diseaseExist)
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.DiseaseNotExist);

                var resultData = patientDiseasesDto.Id == 0 ?
                await AddPatientDiseases(patientDiseasesDto) :
                await UpdatePatientDiseases(patientDiseasesDto);

                if (resultData == null)
                    return result.AddError(ErrorMessages.PatientDiseaseNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                return result.UpdateResultData(resultData).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        #region Add patient Disease
        private async Task<PatientDiseasesDto> AddPatientDiseases(PatientDiseasesDto patientDiseasesDto)
        {
            PatientDiseasesDto data = new();

            PatientsDiseases entity = new()
            {
                DiseasesId = patientDiseasesDto.DiseaseId,
                UserId = patientDiseasesDto.UserId
            };

            await Context.PatientsDiseases.AddAsync(entity);

            if (patientDiseasesDto.Images.Count != 0)
                entity = AddingPatientDiseaseFiles(patientDiseasesDto.Images, entity);

            if (patientDiseasesDto.MedicineIds.Count != 0)
                AddingPatientMedicines(patientDiseasesDto, entity);

            await Context.SaveChangesAsync();
            data.Id = entity.Id;

            return data;
        }
        private PatientsDiseases AddingPatientDiseaseFiles(
            IEnumerable<FileDetailsDto> fileDetailsDto, PatientsDiseases entity)
        {
            foreach (FileDetailsDto image in fileDetailsDto)
            {
                entity.Files.Add(new PatientsDiseasesFile()
                {
                    Checksum = image.Checksum,
                    FileContentType = image.FileContentType,
                    Description = image.Description,
                    Extension = image.Extension,
                    FileName = image.FileName,
                    Name = image.Name,
                    Url = image.Url,
                });
            }
            return entity;
        }
        private void AddingPatientMedicines(
            PatientDiseasesDto patientDiseasesDto, PatientsDiseases entity)
        {
            foreach (int medicineId in patientDiseasesDto.MedicineIds)
            {
                Context.PatientMedicines.Add(new PatientMedicine()
                {
                    MedicineId = medicineId,
                    PatientsDiseases = entity,
                    UserId = patientDiseasesDto.UserId,
                });
            }
        }
        #endregion

        #region Update Patient Diseases
        private async Task<PatientDiseasesDto> UpdatePatientDiseases(
            PatientDiseasesDto patientDiseasesDto)
        {
            PatientDiseasesDto data = new();

            var entity = await Context.PatientsDiseases
                .Where(entity => entity.IsValid
                && entity.Id == patientDiseasesDto.Id)
                .Include(entity => entity.Files)
                .Include(entity => entity.Medicines)
                .SingleOrDefaultAsync();

            if (entity == null || entity.Id != patientDiseasesDto.Id)
                return null;

            if (patientDiseasesDto.RemovedFilesIds.Count() > 0)
                data = RemovePatientDiseaseFiles(data, patientDiseasesDto.RemovedFilesIds);

            if (patientDiseasesDto.Images.Count != 0)
                entity = AddingPatientDiseaseFiles(patientDiseasesDto.Images, entity);

            RemovePatientMedicines(patientDiseasesDto.MedicineIds, entity);
            entity = AddingPatientMedicinesAtUpdateState(patientDiseasesDto, entity);

            entity.DiseasesId = patientDiseasesDto.DiseaseId;
            Context.PatientsDiseases.Update(entity);
            await Context.SaveChangesAsync();
            return data;
        }
        private void RemovePatientMedicines(
            IEnumerable<int> ids, PatientsDiseases entity)
        {
            var MedsToRemove = entity.Medicines
               .Where(med => !ids.Contains(med.MedicineId))
               .ToList();

            Context.RemoveRange(MedsToRemove);
        }
        private PatientsDiseases AddingPatientMedicinesAtUpdateState(
            PatientDiseasesDto patientDiseasesDto, PatientsDiseases entity)
        {
            var DbMedicineIds = entity.Medicines
               .Where(med => med.IsValid)
               .Select(med => med.MedicineId)
               .ToList();

            patientDiseasesDto.MedicineIds = patientDiseasesDto.MedicineIds
                .Where(md => !DbMedicineIds.Contains(md))
                .ToList();

            foreach (int medicineId in patientDiseasesDto.MedicineIds)
            {
                _ = Context.PatientMedicines.Add(new PatientMedicine()
                {
                    MedicineId = medicineId,
                    PatientsDiseases = entity,
                    UserId = patientDiseasesDto.UserId,

                });
            }
            return entity;
        }
        private PatientDiseasesDto RemovePatientDiseaseFiles(
            PatientDiseasesDto data, IEnumerable<int> removeFileIds)
        {
            var removedFiles = Context.Files
                  .Where(file => removeFileIds.Contains(file.Id))
                  .ToList();

            data.RemovedFilesUrls = removedFiles.Select(file => file.Url).ToList();
            Context.RemoveRange(removedFiles);

            return data;
        }
        #endregion

        #endregion

        #region Get
        public async Task<OperationResult<GenericOperationResult, IEnumerable<DiseasesFormDto>>> GetAllDiseases(int? userId, int pageSize, int pageNumber)
        {
            var result = new OperationResult<GenericOperationResult, IEnumerable<DiseasesFormDto>>(GenericOperationResult.Failed);
            try
            {
                var data = await Context.Diseases
                    .Include(entity => entity.Files)
                    .Where(entity => entity.IsValid && (userId == null || entity.PatientsDiseases.Any(entity => entity.UserId == userId)))
                    .Skip(pageSize * pageNumber)
                    .Take(pageSize)
                    .Select(entity => new DiseasesFormDto()
                    {
                        Id = entity.Id,
                        Name = entity.Name,
                        Description = entity.Description,
                        DiseaseType = entity.DiseaseType,
                        Images = entity.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                        {
                            Id = file.Id,
                            Checksum = file.Checksum,
                            FileContentType = file.FileContentType,
                            Description = file.Description,
                            FileName = file.FileName,
                            Name = file.Name,
                            Url = file.Url,
                            Extension = file.Extension
                        }).ToList()
                    }).ToListAsync();

                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public OperationResult<GenericOperationResult, IEnumerable<DiseaseSelectDto>> GetDiseasesAutoComplete(string query)
        {
            OperationResult<GenericOperationResult, IEnumerable<DiseaseSelectDto>> result = new(GenericOperationResult.Failed);

            try
            {
                var data = ValidEntities
                    .Where(all => all.Name.ToUpper().Contains(query) || all.Description.ToUpper().Contains(query))
                    .Select(all => new DiseaseSelectDto()
                    {
                        Id = all.Id,
                        Name = all.Name
                    });

                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<PatientDiseasesDto>>> GetAllPatientDisease(int? userId, bool enablePagination, int pageSize, int pageNumber)
        {
            OperationResult<GenericOperationResult, IEnumerable<PatientDiseasesDto>> result = new(GenericOperationResult.Failed);

            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();
                var data = Context.PatientsDiseases
                      .Where(pd => pd.IsValid && (!userId.HasValue && userInfo.CurrentUserId == pd.UserId
                       || ((userInfo.IsUserAdmin || userInfo.IsNormalUser) && pd.UserId == userId)
                       || (userInfo.IsHealthCareProvider && (userId == pd.CreatedBy && userId == pd.UserId)
                       || (userInfo.CurrentUserId == pd.CreatedBy && userId == pd.UserId))));

                if (enablePagination)
                    data = data.GetDataWithPagination(pageSize, pageNumber);

                var resultData = data.Select(pa => new PatientDiseasesDto()
                {
                    Id = pa.Id,
                    DiseaseId = pa.DiseasesId,
                    DiseaseName = pa.Diseases.Name,
                    Medicines = pa.Medicines
                    .Where(med => med.IsValid)
                    .Select(med => new PatientMedicineDto()
                    {
                        Id = med.Id,
                        MedicineId = med.MedicineId,
                        MedicineName = med.Medicine.Name
                    }).ToList(),
                    UserId = pa.UserId ?? 0,
                    UserName = pa.User.UserName,
                    Images = pa.Files.Where(file => file.IsValid)
                    .Select(file => new FileDetailsDto()
                    {
                        Id = file.Id,
                        Checksum = file.Checksum,
                        FileContentType = file.FileContentType,
                        Description = file.Description,
                        FileName = file.FileName,
                        Name = file.Name,
                        Url = file.Url,
                        Extension = file.Extension
                    }).ToList(),
                }).ToList();

                return result.UpdateResultData(resultData).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, DiseasesInfoDto>> GetDisease(int Id)
        {
            OperationResult<GenericOperationResult, DiseasesInfoDto> result = new(GenericOperationResult.Failed);

            var entity = await Context.Diseases
                .Include(entity => entity.Files)
                .Where(entity => entity.IsValid && entity.Id == Id)
                .SingleOrDefaultAsync();

            if (entity is null)
                return result.AddError(ErrorMessages.DiseaseNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

            try
            {
                DiseasesInfoDto formDto = new()
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Description = entity.Description,
                    DiseaseType = entity.DiseaseType,
                    CreatedBy = entity.CreatedBy,
                    PatientsDisease = await Context.PatientsDiseases
                    .Where(p => p.Diseases == entity)
                    .Select(d => new PatientDiseasesDto()
                    {
                        Id = d.Id,
                        UserName = d.User.UserName,
                        UserId = d.UserId ?? 0,
                        MedicineIds = d.Medicines.Select(med => med.MedicineId).ToList(),
                        Images = d.Files.Select(file => new FileDetailsDto()
                        {
                            Id = file.Id,
                            Checksum = file.Checksum,
                            FileContentType = file.FileContentType,
                            Description = file.Description,
                            FileName = file.FileName,
                            Name = file.Name,
                            Url = file.Url,
                            Extension = file.Extension
                        }).ToList()
                    }).ToListAsync(),
                    Images = await entity.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                    {
                        Id = file.Id,
                        Checksum = file.Checksum,
                        FileContentType = file.FileContentType,
                        Description = file.Description,
                        FileName = file.FileName,
                        Name = file.Name,
                        Url = file.Url,
                        Extension = file.Extension
                    }).ToListAsync()
                };

                return result.UpdateResultData(formDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, PatientDiseasesDto>> GetPatientDisease(int patientDiseaseId)
        {
            OperationResult<GenericOperationResult, PatientDiseasesDto> result = new(GenericOperationResult.Failed);

            try
            {
                PatientDiseasesDto data = await Context.PatientsDiseases
                    .Where(pa => pa.IsValid && pa.Id == patientDiseaseId)
                    .Select(pa => new PatientDiseasesDto()
                    {
                        Id = pa.Id,
                        DiseaseId = pa.DiseasesId,
                        UserId = pa.UserId ?? 0,
                        UserName = pa.User.UserName,
                        Medicines = pa.Medicines.Where(med => med.IsValid)
                        .Select(med => new PatientMedicineDto()
                        {
                            Id = med.Id,
                            MedicineId = med.MedicineId,
                            MedicineName = med.Medicine.Name

                        }).ToList(),
                        Images = pa.Files.Where(file => file.IsValid)
                        .Select(file => new FileDetailsDto()
                        {
                            Id = file.Id,
                            Checksum = file.Checksum,
                            FileContentType = file.FileContentType,
                            Description = file.Description,
                            FileName = file.FileName,
                            Name = file.Name,
                            Url = file.Url,
                            Extension = file.Extension
                        }).ToList()
                    }).SingleOrDefaultAsync();

                if (data is null)
                    return result.AddError(ErrorMessages.DiseaseNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                return
                    result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Remove
        public async Task<OperationResult<GenericOperationResult, IEnumerable<string>>> RemoveDiseases(int Id)
        {
            OperationResult<GenericOperationResult, IEnumerable<string>> result = new(GenericOperationResult.Failed);
            try
            {
                Diseases entity = await Context.Diseases
                    .Where(entity => entity.IsValid && entity.Id == Id)
                    .SingleOrDefaultAsync();

                if (entity is null)
                    return result.AddError(ErrorMessages.DiseaseNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                var data = (await Context.Diseases
                    .Where(entity => entity.IsValid && entity.Id == Id)
                    .Include(entity => entity.Files)
                    .SingleOrDefaultAsync()).Files;

                Context.RemoveRange(data);
                Context.Diseases.Remove(entity);
                await Context.SaveChangesAsync();

                return result.UpdateResultData(data.Select(file => file.Url)).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<string>>> RemovePatientDiseases(int patientDiseaseId)
        {
            OperationResult<GenericOperationResult, IEnumerable<string>> result = new(GenericOperationResult.Failed);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var entity = await Context.PatientsDiseases
                    .Where(entity => entity.IsValid
                    && (userInfo.IsUserAdmin || userInfo.CurrentUserId == entity.CreatedBy)
                    && entity.Id == patientDiseaseId)
                    .SingleOrDefaultAsync();

                if (entity is null)
                    return result.AddError(ErrorMessages.PatientDiseaseNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                var data = (await Context.PatientsDiseases
                    .Where(entity => entity.IsValid && entity.Id == patientDiseaseId)
                    .Include(entity => entity.Files)
                    .SingleOrDefaultAsync()).Files;

                Context.RemoveRange(data);
                Context.PatientsDiseases.Remove(entity);
                await Context.SaveChangesAsync();

                return result.UpdateResultData(data.Select(file => file.Url)).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion
    }
}

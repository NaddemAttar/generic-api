﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Course;
using IGenericControlPanel.Core.Dto.Location;
using IGenericControlPanel.Core.Dto.Location.LocationSchedule;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using IGenericControlPanel.SharedKernal.Utils;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class LocationRepository : BasicRepository<CPDbContext, Location>, ILocationRepository
    {
        #region Properties & Constructor
        private IUserRepository UserRepository { get; }
        private IGamificationMovementsRepository _gamificationMovementRepo { get; }
        public LocationRepository(
            CPDbContext context,
            IUserRepository userRepository,
            IGamificationMovementsRepository gamificationMovementRepo) : base(context)
        {
            UserRepository = userRepository;
            _gamificationMovementRepo = gamificationMovementRepo;
        }

        #endregion

        #region Action For Location
        public OperationResult<GenericOperationResult, LocationDto> Action(LocationDto formDto)
        {
            OperationResult<GenericOperationResult,
                LocationDto> result = new(GenericOperationResult.ValidationError);

            if (string.IsNullOrEmpty(formDto.Name))
            {
                return result.AddError(ErrorMessages.LocationNameRequired);
            }

            try
            {
                var data = formDto.Id == 0 ?
                    AddLocation(formDto) :
                    UpdateLocation(formDto);

                return data is null
                    ? result.AddError(ErrorMessages.LocationNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound)
                    : result.UpdateResultData(data)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return new OperationResult<GenericOperationResult,
                    LocationDto>(GenericOperationResult.Failed);
            }

        }

        public LocationDto AddLocation(LocationDto locationDto)
        {
            Location newLocation = new()
            {
                Name = locationDto.Name
            };

            Context.Add(newLocation);

            Context.SaveChanges();
            return locationDto;
        }

        public LocationDto UpdateLocation(LocationDto locationDto)
        {
            var updateLocation = Context.Location
                .Where(entity => entity.Id == locationDto.Id)
                .SingleOrDefault();

            if (updateLocation is null)
            {
                return null;
            }

            updateLocation.Name = locationDto.Name;

            Context.SaveChanges();

            return locationDto;
        }
        #endregion

        #region Get All Locations
        public OperationResult<GenericOperationResult, IEnumerable<LocationDto>> GetAll(
            string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult,
                IEnumerable<LocationDto>> result = new(GenericOperationResult.ValidationError);

            try
            {
                var locations = ValidEntities
                 .Select(entity => new LocationDto
                 {
                     Id = entity.Id,
                     Name = entity.Name
                 }).ToList();

                return result.UpdateResultData(locations)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                     .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion

        #region Remove Location
        public OperationResult<GenericOperationResult> Remove(int id)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.ValidationError);

            try
            {
                var entity = Context.Location
              .Where(entity => entity.Id == id)
              .SingleOrDefault();

                if (entity is null)
                {
                    return result.AddError(ErrorMessages.LocationNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                entity.IsValid = false;
                Context.SaveChanges();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        #endregion

        #region Get Locations Schedule
        public async Task<OperationResult<GenericOperationResult, IEnumerable<LocationScheduleDto>>> GetLocationsSchedule(
            DateTime? fromDate, DateTime? toDate, int cityId,
            bool ignoreOldDates = false, bool orderFromNewestToOldest = false)
        {
            OperationResult<GenericOperationResult,
                IEnumerable<LocationScheduleDto>> result = new(GenericOperationResult.ValidationError);

            try
            {
                var data = await GetLocationScheduleEntityWithInclude(ignoreOldDates, fromDate, toDate, cityId);

                data = GetLocationScheduleEntityWithCorrectOrder(orderFromNewestToOldest, data);

                var resultData = data
                    .Select(loc => new LocationScheduleDto()
                    {
                        Id = loc.Id,
                        FromDate = loc.FromDate,
                        ToDate = loc.ToDate,
                        IsExist = DateTime.Now >= loc.FromDate && DateTime.Now <= loc.ToDate,
                        Address = loc.Address,
                        ContactNumber = loc.ContactNumber,
                        Location = new GeoLocationDto()
                        {
                            CityId = loc.CityId,
                            CityName = IsDefaultLanguage ? loc.City.Name :
                                loc.City.Translations
                                .Where(trns => trns.Culture == CurrentCultureCode)
                                .SingleOrDefault().Name,
                            CountryId = loc.City.CountryId,
                            CountryName = IsDefaultLanguage ? loc.City.Country.Name :
                                loc.City.Country.Translations
                                .Where(trns => trns.Culture == CurrentCultureCode)
                                .SingleOrDefault().Name
                        }
                    }).ToList();

                return result.UpdateResultData(resultData)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        public async Task<IQueryable<LocationSchedule>> GetLocationScheduleEntityWithInclude(
          bool ignoreOldDates, DateTime? fromDate, DateTime? toDate, int cityId)
        {
            var userInfo = await UserRepository.GetCurrentUserInfo();
            var builder = WebApplication.CreateBuilder();
            bool multiProviders = bool.Parse(builder.Configuration.GetSection("MultiProviders").Value.ToLower());
            var data = Context.LocationSchedule
                        .Where(slocation => (slocation.IsValid && (userInfo.CurrentUserId == 0
                       || userInfo.IsUserAdmin
                       || (multiProviders == false ? true : userInfo.IsHealthCareProvider
                       ? userInfo.CurrentUserId == slocation.CreatedBy : true))
                        && (ignoreOldDates ? slocation.ToDate > DateTime.Now : !ignoreOldDates)
                        && (fromDate == null || slocation.FromDate > fromDate)
                        && (toDate == null || slocation.ToDate < toDate)
                        && (cityId == 0 || slocation.CityId == cityId)));

            return IsDefaultLanguage
                ? data.Include(entity => entity.City.Country)
                : data
                     .Include(entity => entity.City.Country)
                     .ThenInclude(Country => Country.Translations)
                     .Include(entity => entity.City)
                     .ThenInclude(city => city.Translations);
        }

        public IQueryable<LocationSchedule> GetLocationScheduleEntityWithCorrectOrder(
            bool orderFromNewestToOldest, IQueryable<LocationSchedule> data)
        {
            return orderFromNewestToOldest ? data.OrderByDescending(entity => entity.FromDate) : (data = data.OrderBy(entity => entity.FromDate));
        }
        #endregion

        #region Action For Location Schedule
        public async Task<OperationResult<GenericOperationResult, LocationScheduleDto>> ActionLocationSchedule(
            LocationScheduleFormDto locationSchedule)
        {
            OperationResult<GenericOperationResult,
                LocationScheduleDto> result = new(GenericOperationResult.Success);

            try
            {
                var Entity = Context.LocationSchedule
                    .Where(slocation => slocation.IsValid && slocation.Id == locationSchedule.Id)
                    .Include(entity => entity.City)
                    .SingleOrDefault();

                bool OverLapEntity = Context.LocationSchedule
                    .Where(slocation => slocation.IsValid
                    && slocation.Id != locationSchedule.Id
                    && !(locationSchedule.FromDate > slocation.ToDate
                    || locationSchedule.ToDate < slocation.FromDate)).Any();

                if (OverLapEntity)
                {
                    return result.AddError(ErrorMessages.OverlapWithAnotherTime)
                        .UpdateResultStatus(GenericOperationResult.ValidationError);
                }

                if (Entity == null)
                {
                    Entity = new LocationSchedule()
                    {
                        FromDate = locationSchedule.FromDate,
                        ToDate = locationSchedule.ToDate,
                        CityId = locationSchedule.CityId,
                        Address = locationSchedule.Address,
                        ContactNumber = locationSchedule.ContactNumber
                    };
                    await Context.AddAsync(Entity);
                }
                else
                {
                    Entity.FromDate = locationSchedule.FromDate;
                    Entity.ToDate = locationSchedule.ToDate;
                    Entity.CityId = locationSchedule.CityId;
                    Entity.Address = locationSchedule.Address;
                    Entity.ContactNumber = locationSchedule.ContactNumber;
                }

                await Context.SaveChangesAsync();

                if (locationSchedule.Id == 0)
                {
                    var savePointsResult = await _gamificationMovementRepo
                        .SavePoints(new List<string>
                    {
                        GamificationNames.AddLocationSchedule
                    });

                    if (!savePointsResult.IsSuccess)
                    {
                        return result.AddError(savePointsResult.ErrorMessages)
                            .UpdateResultStatus(GenericOperationResult.Failed);
                    }
                }

                GeoLocationDto location = new();

                location = IsDefaultLanguage
                    ? Context.Cities
                    .Where(city => city.Id == locationSchedule.CityId)
                    .Include(entity => entity.Country)
                   .Select(entity => new GeoLocationDto()
                   {
                       CityId = entity.Id,
                       CityName = entity.Name,
                       CountryId = entity.CountryId,
                       CountryName = entity.Country.Name
                   }).SingleOrDefault()
                    : Context.Cities.Where(city => city.Id == locationSchedule.CityId).Include(entity => entity.Country)
                  .ThenInclude(Country => Country.Translations)
                  .Include(city => city.Translations).Select(entity => new GeoLocationDto()
                  {
                      CityId = entity.Id,
                      CityName = entity.Translations.Where(trns => trns.Culture == CurrentCultureCode).SingleOrDefault().Name,
                      CountryId = entity.CountryId,
                      CountryName = entity.Country.Translations.Where(trns => trns.Culture == CurrentCultureCode).SingleOrDefault().Name
                  }).SingleOrDefault();

                LocationScheduleDto locationScheduleDto = new()
                {
                    Id = Entity.Id,
                    FromDate = Entity.FromDate,
                    ToDate = Entity.ToDate,
                    Location = location,
                    ContactNumber = Entity.ContactNumber
                };
                return result.UpdateResultData(locationScheduleDto);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion

        #region Get Countries & Cities
        public OperationResult<GenericOperationResult, IEnumerable<CountryDto>> GetCountries()
        {
            OperationResult<GenericOperationResult,
                IEnumerable<CountryDto>> result = new(GenericOperationResult.ValidationError);

            try
            {
                if (IsDefaultLanguage)
                {
                    var data = Context.Countries
                        .Select(Country => new CountryDto()
                        {
                            Id = Country.Id,
                            Name = Country.Name
                        }).ToList();

                    result.UpdateResultData(data);
                }

                else
                {
                    var data = Context.CountryTranslations
                        .Where(countrans => countrans.Culture == CurrentCultureCode)
                       .Select(Country => new CountryDto()
                       {
                           Id = Country.CountryId,
                           Name = Country.Name
                       }).ToList();

                    result.UpdateResultData(data);
                }

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public OperationResult<GenericOperationResult, IEnumerable<CityDto>> GetCities(int countryId)
        {
            OperationResult<GenericOperationResult,
                IEnumerable<CityDto>> result = new(GenericOperationResult.ValidationError);

            try
            {
                if (IsDefaultLanguage)
                {
                    var data = Context.Cities.Where(city => city.CountryId == countryId)
                            .Select(city => new CityDto()
                            {
                                Id = city.Id,
                                Name = city.Name
                            }).ToList();
                    result.UpdateResultData(data);
                }
                else
                {
                    var data = Context.CityTranslations.Where(city => city.Culture == CurrentCultureCode && city.City.CountryId == countryId)
                       .Select(city => new CityDto()
                       {
                           Id = city.CityId,
                           Name = city.Name
                       }).ToList();
                    result.UpdateResultData(data);
                }

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        #endregion

        #region Remove Location Schedule
        public async Task<OperationResult<GenericOperationResult>> RemoveLocationSchedule(int LocationScheduleId)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.ValidationError);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var Entity = Context.LocationSchedule
                    .Where(locationSc => locationSc.Id == LocationScheduleId
                    && (userInfo.IsUserAdmin
                    || userInfo.CurrentUserId == locationSc.CreatedBy))
                    .SingleOrDefault();

                if (Entity == null)
                {
                    return result.AddError(ErrorMessages.LocationScheduleNotExist)
                        .UpdateResultStatus(GenericOperationResult.ValidationError);
                }

                Context.SaveChanges();
                Context.Remove(Entity);
                await Context.SaveChangesAsync();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        #endregion

        public OperationResult<GenericOperationResult, LocationScheduleDto> GetLocationScheduleForDate(DateTime date)
        {
            OperationResult<GenericOperationResult, LocationScheduleDto> result = new(GenericOperationResult.Failed);
            try
            {
                if (IsDefaultLanguage)
                {

                    LocationScheduleDto data = Context.LocationSchedule.Where(slocation => slocation.IsValid
                      && slocation.IsValid
                && slocation.FromDate < date && slocation.ToDate > date).Include(entity => entity.City.Country)
                        .Select(loc => new LocationScheduleDto()
                        {
                            Id = loc.Id,
                            FromDate = loc.FromDate,
                            ToDate = loc.ToDate,
                            Address = loc.Address,
                            Location = new GeoLocationDto()
                            {
                                CityId = loc.CityId,
                                CityName = loc.City.Name,
                                CountryId = loc.City.CountryId,
                                CountryName = loc.City.Country.Name
                            }
                        }).SingleOrDefault();
                    return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
                }
                else
                {
                    LocationScheduleDto data = Context.LocationSchedule.Where(slocation => slocation.IsValid
                      && slocation.IsValid
                && slocation.FromDate < date && slocation.ToDate > date).Include(entity => entity.City.Country)
                  .ThenInclude(Country => Country.Translations)
                  .Include(entity => entity.City).ThenInclude(city => city.Translations)
                    .Select(loc => new LocationScheduleDto()
                    {
                        Id = loc.Id,
                        FromDate = loc.FromDate,
                        ToDate = loc.ToDate,
                        Address = loc.Address,
                        Location = new GeoLocationDto()
                        {
                            CityId = loc.CityId,
                            CityName = loc.City.Translations.Where(trns => trns.Culture == CurrentCultureCode).SingleOrDefault().Name,
                            CountryId = loc.City.CountryId,
                            CountryName = loc.City.Country.Translations.Where(trns => trns.Culture == CurrentCultureCode).SingleOrDefault().Name
                        }
                    }).SingleOrDefault();
                    return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
                }

            }
            catch (Exception ex)
            {
                return result.AddError(ex.Message);
            }
        }
    }
}

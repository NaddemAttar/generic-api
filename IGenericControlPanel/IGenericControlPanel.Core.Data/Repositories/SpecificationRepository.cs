﻿using System;
using System.Collections.Generic;
using System.Linq;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Specifiation;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class SpecificationRepository : BasicRepository<CPDbContext, Specification>, ISpecificationRepository
    {
        public SpecificationRepository(CPDbContext dbContext) : base(dbContext) { }

        public OperationResult<GenericOperationResult, SpecificationDto> Action(SpecificationDto specificationDto)
        {
            OperationResult<GenericOperationResult, SpecificationDto> result = new(GenericOperationResult.ValidationError);
            result = specificationDto.Id == 0 ? AddEntity(specificationDto) : UpdateEntity(specificationDto);
            return result;
        }
        private OperationResult<GenericOperationResult, SpecificationDto> AddEntity(SpecificationDto specificationDto)
        {
            OperationResult<GenericOperationResult, SpecificationDto> result = new(GenericOperationResult.ValidationError);
            try
            {
                Specification entity = new()
                {
                    Name = specificationDto.Name
                };

                _ = Context.Add(entity);

                _ = Context.SaveChanges();

                specificationDto.Id = entity.Id;
                return result.UpdateResultData(specificationDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }

        }

        private OperationResult<GenericOperationResult, SpecificationDto> UpdateEntity(SpecificationDto specificationDto)
        {
            OperationResult<GenericOperationResult, SpecificationDto> result = new(GenericOperationResult.ValidationError);
            try
            {
                Specification entity = Context.Specifications
                    .Where(specification => specification.IsValid &&
                    specification.Id == specificationDto.Id).SingleOrDefault();
                if (entity is null)
                {
                    return result.UpdateResultStatus(GenericOperationResult.NotFound);
                }

                entity.Name = specificationDto.Name;

                _ = Context.Update(entity);
                _ = Context.SaveChanges();
                return result.UpdateResultData(specificationDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }


        }

        public OperationResult<GenericOperationResult, SpecificationDto> Get(int id, string cultureCode = "ar")
        {
            throw new NotImplementedException();
        }

        public OperationResult<GenericOperationResult, IEnumerable<SpecificationDto>> GetAll(string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, IEnumerable<SpecificationDto>> result = new(GenericOperationResult.ValidationError);

            try
            {

                List<SpecificationDto> specificationDtos = Context.Specifications
                    .Where(specification => specification.IsValid)
                    .Select(specification => new SpecificationDto()
                    {
                        Id = specification.Id,
                        Name = specification.Name
                    }).ToList();

                return result.UpdateResultData(specificationDtos).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }

        public OperationResult<GenericOperationResult, IEnumerable<SpecificationDto>> GetAllDetailed(string cultureCode = "ar")
        {
            throw new NotImplementedException();
        }

        public OperationResult<GenericOperationResult, SpecificationDto> GetDetailed(int id, string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, SpecificationDto> result = new(GenericOperationResult.ValidationError);

            try
            {
                SpecificationDto specificationDto = Context.Specifications
                    .Where(specification => specification.IsValid && specification.Id == id)
                    .Select(specification => new SpecificationDto()
                    {
                        Id = specification.Id,
                        Name = specification.Name
                    }).SingleOrDefault();
                return specificationDto is null
                    ? result.UpdateResultStatus(GenericOperationResult.NotFound)
                    : result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(specificationDto);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }

        }

        public OperationResult<GenericOperationResult> Remove(int id)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.ValidationError);

            try
            {
                Specification specificationEntity = Context.Specifications
                    .Where(specification => specification.IsValid && specification.Id == id)
                    .SingleOrDefault();

                if (specificationEntity is not null)
                {
                    Context.Remove(specificationEntity);
                    _ = Context.SaveChanges();
                    return result.UpdateResultStatus(GenericOperationResult.Success);
                }
                else
                {
                    return result.UpdateResultStatus(GenericOperationResult.NotFound);
                }
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }


        }
    }
}

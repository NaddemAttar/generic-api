﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Core.Dto.Offer;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums.Core;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class OfferRepository : BasicRepository<CPDbContext, Offer>, IOfferRepository
    {
        private readonly IUserRepository userRepository;
        public OfferRepository(CPDbContext context, IUserRepository userRepository) : base(context)
        {
            this.userRepository = userRepository;
        }
        #region Action

        public async Task<OperationResult<GenericOperationResult, int>> ActionCreate(OfferDto offerDto)
        {
            OperationResult<GenericOperationResult, int> result = new(GenericOperationResult.ValidationError);
            var transaction = Context.Database.BeginTransaction();
            try
            {
                if (offerDto.Percentage != 0 && offerDto.Percentage >= 100 && offerDto.Percentage <= 1)
                    return result.UpdateResultStatus(GenericOperationResult.ValidationError).AddError(ErrorMessages.OfferPercentageError);

                if (offerDto.OfferFor == OfferFor.Services && offerDto.Value != null && offerDto.Percentage == null)
                    return result.UpdateResultStatus(GenericOperationResult.ValidationError).AddError(ErrorMessages.YouCanOnlyEnterPercentageError);

                var user = userRepository.GetCurrentUserInformation();

                var offerId = 0;
                if (offerDto.OfferFor == OfferFor.Products)
                {
                    if (offerDto.OfferType == OfferType.All)
                    {
                        //There should only one offer of type "All" for each type(Product, Service, Courses).
                        var offerModel = await Context.Offers
                            .Where(o => o.OfferType == OfferType.All && o.OfferFor == OfferFor.Products && o.IsValid)
                            .FirstOrDefaultAsync();

                        //Delete the old one.
                        if (offerModel != null)
                            Context.Offers.Remove(offerModel);

                        //Add new One.
                        var addOfferModel = await CreateOffer(offerDto, user.Id);

                        offerId = addOfferModel.Id;
                    }
                    else if (offerDto.OfferType == OfferType.Multi)
                    {
                        var addOfferModel = await CreateOffer(offerDto, user.Id);

                        offerId = addOfferModel.Id;
                        if (offerDto.ItemIds.Count != 0) //this for Products Ids.
                        {
                            foreach (var productId in offerDto.ItemIds)
                            {
                                var productModel = await Context.Products.FirstOrDefaultAsync(p => p.Id == productId && p.IsValid);
                                productModel.OfferId = addOfferModel.Id;
                            }
                        }
                        if (offerDto.DetailsIds.Count != 0) //this for Products Details Ids.
                        {
                            foreach (var productSizeColorId in offerDto.DetailsIds)
                            {
                                var productSizeColorModel = await Context.ProductSizesColors.FirstOrDefaultAsync(psc => psc.Id == productSizeColorId && psc.IsValid);
                                productSizeColorModel.OfferId = addOfferModel.Id;
                            }
                        }
                    }
                    else if (offerDto.OfferType == OfferType.Brand)
                    {
                        var addOfferModel = await CreateOffer(offerDto, user.Id);

                        offerId = addOfferModel.Id;
                        if (offerDto.ItemIds.Count != 0)
                        {
                            foreach (var brandId in offerDto.ItemIds)
                            {
                                var brandModel = await Context.Brand.FirstOrDefaultAsync(b => b.Id == brandId && b.IsValid);
                                brandModel.OfferId = addOfferModel.Id;
                            }
                        }
                    }
                }
                else if (offerDto.OfferFor == OfferFor.Courses)
                {
                    if (offerDto.OfferType == OfferType.All)
                    {
                        var offerModel = await Context.Offers
                            .Where(o => o.OfferType == OfferType.All && o.OfferFor == OfferFor.Courses && o.IsValid)
                            .FirstOrDefaultAsync();

                        if (offerModel != null)
                            Context.Offers.Remove(offerModel);

                        var addOfferModel = await CreateOffer(offerDto, user.Id);

                        offerId = addOfferModel.Id;
                    }
                    else if (offerDto.OfferType == OfferType.Multi)
                    {
                        var addOfferModel = await CreateOffer(offerDto, user.Id);

                        offerId = addOfferModel.Id;
                        if (offerDto.ItemIds.Count != 0)
                        {
                            foreach (var courseId in offerDto.ItemIds)
                            {
                                var courseModel = await Context.Courses.FirstOrDefaultAsync(c => c.Id == courseId && c.IsValid);
                                courseModel.OfferId = addOfferModel.Id;
                            }
                        }
                    }
                }
                else if (offerDto.OfferFor == OfferFor.Services)
                {
                    if (offerDto.OfferType == OfferType.All)
                    {
                        var offerModel = await Context.Offers
                            .Where(o => o.OfferType == OfferType.All && o.OfferFor == OfferFor.Services && o.IsValid)
                            .FirstOrDefaultAsync();

                        if (offerModel != null)
                            Context.Offers.Remove(offerModel);

                        var addOfferModel = await CreateOffer(offerDto, user.Id);

                        offerId = addOfferModel.Id;
                    }
                    else if (offerDto.OfferType == OfferType.Multi)
                    {
                        var addOfferModel = await CreateOffer(offerDto, user.Id);

                        offerId = addOfferModel.Id;
                        if (offerDto.ItemIds.Count != 0)
                        {
                            foreach (var serviceId in offerDto.ItemIds)
                            {
                                if (user.userType == HcpType.Enterprise)
                                {
                                    // here we make offer for specific service for all hcp in this Enterprise cuz the owner of the enterprise who's make the offer and then it will be reflect to all hcps in this enterprise.
                                    var userEnterpriseService = await Context.HealthCareProviderServices
                                        .Where(ues => ues.IsValid && ues.ServiceId == serviceId && (ues.CPUserId == user.Id || ues.EnterpriseId == user.Id))
                                        .ToListAsync();
                                    userEnterpriseService.ForEach(ues => ues.OfferId = offerId);
                                }
                                else
                                {
                                    var userEnterpriseService = await Context.HealthCareProviderServices
                                        .Where(ues => ues.IsValid && ues.ServiceId == serviceId && ues.CPUserId == user.Id)
                                        .FirstOrDefaultAsync();
                                    if (userEnterpriseService == null)
                                        return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.ServiceNotExist);

                                    userEnterpriseService.OfferId = offerId;
                                }
                                //var serviceModel = await Context.Services.FirstOrDefaultAsync(s => s.Id == serviceId && s.IsValid);
                                //serviceModel.OfferId = addOfferModel.Id;
                            }
                        }
                    }
                }
                if (offerDto.OfferType == OfferType.Category)
                {
                    var addOfferModel = await CreateOffer(offerDto, user.Id);

                    offerId = addOfferModel.Id;
                    if (offerDto.ItemIds.Count != 0)
                    {
                        foreach (var categoryId in offerDto.ItemIds)
                        {
                            var categoryModel = await Context.MultiLevelCategories.FirstOrDefaultAsync(c => c.IsValid && c.Id == categoryId);
                            categoryModel.OfferId = addOfferModel.Id;
                        }
                    }
                }
                await Context.SaveChangesAsync();
                await transaction.CommitAsync();
                return result.UpdateResultData(offerId).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                await transaction.RollbackAsync();
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, int>> ActionUpdate(OfferDto offerDto)
        {
            OperationResult<GenericOperationResult, int> result = new(GenericOperationResult.Failed);
            try
            {
                var offerModel = await Context.Offers
                    .Include(o => o.Products)
                    .Include(o => o.ProductsDetails)
                    .Include(o => o.Categories)
                    .Include(o => o.Brands)
                    .Include(o => o.Courses)
                    .Include(o => o.HealthCareProviderService)
                    .FirstOrDefaultAsync(o => o.Id == offerDto.Id);

                if (offerModel == null)
                    return result.UpdateResultStatus(GenericOperationResult.ValidationError).AddError(ErrorMessages.OfferNotFound);
                var user = userRepository.GetCurrentUserInformation();
                offerModel.Title = offerDto.Title;
                offerModel.Description = offerDto.Description;
                offerModel.StartDate = offerDto.StartDate;
                offerModel.EndDate = offerDto.EndDate;
                offerModel.LastUpdateDate = DateTime.Now;
                offerModel.LastUpdatedBy = user.Id;

                if (offerModel.OfferFor == OfferFor.Products && offerModel.OfferType != OfferType.All)
                {
                    if (offerModel.OfferType == OfferType.Multi)
                    {
                        offerModel.Products.ToList().ForEach(p => p.OfferId = null);
                        offerModel.ProductsDetails.ToList().ForEach(psc => psc.OfferId = null);
                        foreach (var productId in offerDto.ItemIds)
                        {
                            var productModel = await Context.Products
                                //.Include(p => p.ProductSizeColors)
                                .FirstOrDefaultAsync(p => p.Id == productId);

                            if (productModel == null)
                                return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.ProductNotFound);

                            //foreach (var psc in productModel.ProductSizeColors)
                            //    if (psc.OfferId == offerDto.Id)
                            //        psc.OfferId = null;

                            productModel.OfferId = offerDto.Id;
                        }
                        foreach (var detailId in offerDto.DetailsIds)
                        {
                            var pscModel = await Context.ProductSizesColors.FirstOrDefaultAsync(psc => psc.Id == detailId);
                            if (pscModel == null)
                                return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.ProductSizeColorNotFound);

                            pscModel.OfferId = offerDto.Id;
                        }
                    }
                    else if (offerModel.OfferType == OfferType.Category)
                    {
                        offerModel.Categories.ToList().ForEach(psc => psc.OfferId = null);
                        foreach (var categoryId in offerDto.ItemIds)
                        {
                            var categoryModel = await Context.MultiLevelCategories.FirstOrDefaultAsync(c => c.Id == categoryId);
                            if (categoryModel == null)
                                return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.MultiLevelCategoriesNotFound);

                            categoryModel.OfferId = offerDto.Id;
                        }
                    }
                    else if (offerModel.OfferType == OfferType.Brand)
                    {
                        offerModel.Brands.ToList().ForEach(psc => psc.OfferId = null);
                        foreach (var brandId in offerDto.ItemIds)
                        {
                            var brandModel = await Context.Brand.FirstOrDefaultAsync(c => c.Id == brandId);
                            if (brandModel == null)
                                return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.BrandNotExist);

                            brandModel.OfferId = offerDto.Id;
                        }
                    }
                }
                else if (offerModel.OfferFor == OfferFor.Services)
                {
                    if (offerModel.OfferType == OfferType.Multi)
                    {
                        offerModel.HealthCareProviderService.ToList().ForEach(s => s.OfferId = null);
                        foreach (var serviceId in offerDto.ItemIds)
                        {
                            if (user.userType == HcpType.Enterprise)
                            {
                                var userEnterpriseService = await Context.HealthCareProviderServices
                                    .Where(ues => ues.IsValid && ues.ServiceId == serviceId && (ues.CPUserId == user.Id || ues.EnterpriseId == user.Id))
                                    .ToListAsync();
                                userEnterpriseService.ForEach(ues => ues.OfferId = offerModel.Id);
                            }
                            else
                            {
                                var userEnterpriseService = await Context.HealthCareProviderServices
                                    .Where(ues => ues.IsValid && ues.ServiceId == serviceId && ues.CPUserId == user.Id)
                                    .FirstOrDefaultAsync();
                                if (userEnterpriseService == null)
                                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.ServiceNotExist);

                                userEnterpriseService.OfferId = offerModel.Id;
                            }
                            //var serviceModel = await Context.Services.FirstOrDefaultAsync(psc => psc.Id == serviceId);
                            //if (serviceModel == null)
                            //    return result.UpdateResultStatus(GenericOperationResult.ValidationError).AddError(ErrorMessages.ProductSizeColorNotFound);

                            //serviceModel.OfferId = offerDto.Id;
                        }
                    }
                }
                else if (offerModel.OfferFor == OfferFor.Courses)
                {
                    if (offerModel.OfferType == OfferType.Multi)
                    {
                        offerModel.Courses.ToList().ForEach(c => c.OfferId = null);
                        foreach (var courseId in offerDto.ItemIds)
                        {
                            var courseModel = await Context.Courses.FirstOrDefaultAsync(c => c.Id == courseId);
                            if (courseModel == null)
                                return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.CourseNotFound);

                            courseModel.OfferId = offerDto.Id;
                        }
                    }
                }
                await Context.SaveChangesAsync();
                return result.UpdateResultData(offerModel.Id).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion
        public async Task<OperationResult<GenericOperationResult, IEnumerable<AllOfferDto>>> GetAllAsync(int pageNumber = 0, int pageSize = 5)
        {
            OperationResult<GenericOperationResult, IEnumerable<AllOfferDto>> result = new(GenericOperationResult.Failed);
            try
            {
                var user = await userRepository.GetCurrentUserInfo();
                var offersModel = await Context.Offers
                    .Where(o => o.IsValid && (o.CreatedBy == user.CurrentUserId || user.IsUserAdmin))
                    .Skip(pageNumber * pageSize)
                    .Take(pageSize)
                    .Select(o => new AllOfferDto
                    {
                        Id = o.Id,
                        Description = o.Description,
                        EndDate = o.EndDate,
                        StartDate = o.StartDate,
                        OfferFor = o.OfferFor,
                        OfferType = o.OfferType,
                        Percentage = o.Percentage,
                        Title = o.Title,
                        Value = o.Value,
                        IsExpired = DateTime.Now > o.EndDate ? true : false,
                        IsStarted = DateTime.Now < o.StartDate ? false : true,
                    }).ToListAsync();
                return result.UpdateResultData(offersModel).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, OfferDetailsDto>> GetAsync(int Id)
        {
            OperationResult<GenericOperationResult, OfferDetailsDto> result = new(GenericOperationResult.Failed);
            try
            {
                var user = await userRepository.GetCurrentUserInfo();
                var offerModel = await Context.Offers
                    .Where(o => o.IsValid && o.Id == Id && (o.CreatedBy == user.CurrentUserId || user.IsUserAdmin))
                    .Include(o => o.Products).ThenInclude(p => p.ProductSizeColors)
                    .Include(o => o.ProductsDetails).ThenInclude(psc => psc.Product)
                    .Include(o => o.Categories)
                    .Include(o => o.Brands)
                    .Include(o => o.Courses)
                    .Include(o => o.HealthCareProviderService).ThenInclude(hcps => hcps.Service)
                    .FirstOrDefaultAsync();

                if (offerModel == null)
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.OfferNotFound);

                var offerDetailsDto = new OfferDetailsDto()
                {
                    Id = offerModel.Id,
                    Description = offerModel.Description,
                    EndDate = offerModel.EndDate,
                    StartDate = offerModel.StartDate,
                    OfferFor = offerModel.OfferFor,
                    OfferType = offerModel.OfferType,
                    Percentage = offerModel.Percentage,
                    Title = offerModel.Title,
                    Value = offerModel.Value,
                    IsExpired = DateTime.Now > offerModel.EndDate ? true : false,
                    IsStarted = DateTime.Now < offerModel.StartDate ? false : true,
                };
                if (offerModel.OfferFor == OfferFor.Products)
                {
                    if (offerModel.OfferType == OfferType.Multi)
                    {
                        foreach (var product in offerModel.Products)
                        {
                            var offerItemDetails = new OfferItemDetailsDto();
                            offerItemDetails.Id = product.Id;
                            offerItemDetails.Description = product.Description;
                            offerItemDetails.Name = product.Name;
                            offerItemDetails.IsOffer = true;
                            foreach (var productDetails in product.ProductSizeColors)
                            {
                                var details = new ItemDetailsDto();
                                details.Id = productDetails.Id;
                                details.SizeName = productDetails.SizeText;
                                details.Price = productDetails.Price;
                                details.ColorHex = productDetails.ColorHex;
                                details.Qunatity = productDetails.Quantity;
                                details.NewPrice = offerModel.Percentage != null ?
                                    (details.Price - (details.Price * (offerModel.Percentage.Value / 100.0)))
                                    : (details.Price - offerModel.Value.Value);

                                offerItemDetails.Details.Add(details);
                            }
                            offerDetailsDto.Item.Add(offerItemDetails);
                        }

                        if (offerModel.ProductsDetails.Count != 0)
                        {
                            var groupingProductDetails = offerModel.ProductsDetails
                            .GroupBy(psc => psc.ProductId)
                            .Select(s => new { ProductId = s.Key, Data = s.ToList() })
                            .ToList();

                            foreach (var product in groupingProductDetails)
                            {
                                var offerItemDetails = new OfferItemDetailsDto();
                                offerItemDetails.Id = product.ProductId;
                                offerItemDetails.Description = product.Data[0].Product.Description;
                                offerItemDetails.Name = product.Data[0].Product.Name;
                                offerItemDetails.IsOffer = false;

                                var productModel = await Context.Products
                                    .Include(p => p.ProductSizeColors)
                                    .FirstOrDefaultAsync(p => p.IsValid && p.Id == product.ProductId);

                                foreach (var productDetails in productModel.ProductSizeColors)
                                {
                                    var details = new ItemDetailsDto();
                                    details.Id = productDetails.Id;
                                    details.SizeName = productDetails.SizeText;
                                    details.Price = productDetails.Price;
                                    details.ColorHex = productDetails.ColorHex;
                                    details.Qunatity = productDetails.Quantity;
                                    if (productDetails.OfferId == offerModel.Id)
                                    {
                                        details.NewPrice = offerModel.Percentage != null ?
                                            (details.Price - (details.Price * (offerModel.Percentage.Value / 100.0)))
                                            : (details.Price - offerModel.Value.Value);
                                    }
                                    offerItemDetails.Details.Add(details);
                                }
                                offerDetailsDto.Item.Add(offerItemDetails);
                            }
                        }
                    }
                    if (offerModel.OfferType == OfferType.Brand)
                    {
                        foreach (var brand in offerModel.Brands)
                        {
                            var offerItemDetails = new OfferItemDetailsDto();
                            offerItemDetails.Id = brand.Id;
                            offerItemDetails.Name = brand.Name;
                            offerItemDetails.IsOffer = true;
                            offerDetailsDto.Item.Add(offerItemDetails);
                        }
                    }
                }
                if (offerModel.OfferFor == OfferFor.Services)
                {
                    if (offerModel.OfferType == OfferType.Multi)
                    {
                        var services = offerModel.HealthCareProviderService
                            .GroupBy(hcps => hcps.ServiceId)
                            .Select(s => new
                            {
                                serviceId = s.Key,
                                Data = s.ToList()
                            });
                        foreach (var service in services)
                        {
                            var offerItemDetails = new OfferItemDetailsDto();
                            offerItemDetails.Id = service.serviceId;
                            offerItemDetails.Description = service.Data[0].Service.Description;
                            offerItemDetails.Name = service.Data[0].Service.Name;
                            offerItemDetails.IsOffer = true;
                            offerDetailsDto.Item.Add(offerItemDetails);
                        }
                    }
                }
                if (offerModel.OfferFor == OfferFor.Courses)
                {
                    if (offerModel.OfferType == OfferType.Multi)
                    {
                        foreach (var course in offerModel.Courses)
                        {
                            var offerItemDetails = new OfferItemDetailsDto();
                            offerItemDetails.Id = course.Id;
                            offerItemDetails.Description = course.Description;
                            offerItemDetails.Name = course.Name;
                            offerItemDetails.IsOffer = true;
                            offerItemDetails.Price = course.Price;
                            offerItemDetails.NewPrice = offerModel.Percentage != null ?
                                            (course.Price - (course.Price * (offerModel.Percentage.Value / 100.0)))
                                            : (course.Price - offerModel.Value.Value);
                            offerDetailsDto.Item.Add(offerItemDetails);
                        }
                    }
                }
                if (offerModel.OfferType == OfferType.Category)
                {
                    foreach (var category in offerModel.Categories)
                    {
                        var offerItemDetails = new OfferItemDetailsDto();
                        offerItemDetails.Id = category.Id;
                        offerItemDetails.Name = category.Name;
                        offerItemDetails.IsOffer = true;
                        offerDetailsDto.Item.Add(offerItemDetails);
                    }
                }
                return result.UpdateResultData(offerDetailsDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, bool>> Remove(int Id)
        {
            OperationResult<GenericOperationResult, bool> result = new(GenericOperationResult.Failed);
            try
            {
                var user = await userRepository.GetCurrentUserInfo();
                var offerModel = await Context.Offers
                    .Include(o => o.Products)
                    .Include(o => o.ProductsDetails)
                    .Include(o => o.Brands)
                    .Include(o => o.Categories)
                    .Include(o => o.HealthCareProviderService)
                    .Include(o => o.Courses)
                    .FirstOrDefaultAsync(o => o.Id == Id);

                if (offerModel == null)
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.OfferNotFound);

                offerModel.IsValid = false;
                offerModel.LastUpdatedBy = user.CurrentUserId;
                offerModel.LastUpdateDate = DateTime.Now;
                offerModel.Products.ToList().ForEach(p => p.OfferId = null);
                offerModel.ProductsDetails.ToList().ForEach(psc => psc.OfferId = null);
                offerModel.Brands.ToList().ForEach(b => b.OfferId = null);
                offerModel.Categories.ToList().ForEach(c => c.OfferId = null);
                offerModel.HealthCareProviderService.ToList().ForEach(s => s.OfferId = null);
                offerModel.Courses.ToList().ForEach(c => c.OfferId = null);
                await Context.SaveChangesAsync();

                return result.UpdateResultData(true).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        private async Task<Offer> CreateOffer(OfferDto offerDto, int CreatedBy)
        {
            var addOfferModel = new Offer()
            {
                IsValid = true,
                CreatedBy = CreatedBy,
                CreationDate = DateTime.Now,
                LastUpdatedBy = CreatedBy,
                LastUpdateDate = DateTime.Now,
                Title = offerDto.Title,
                Value = offerDto.Value,
                Percentage = offerDto.Percentage,
                Description = offerDto.Description,
                OfferType = offerDto.OfferType.Value,
                OfferFor = offerDto.OfferFor.Value,
                StartDate = offerDto.StartDate,
                EndDate = offerDto.EndDate,
            };
            await Context.Offers.AddAsync(addOfferModel);
            await Context.SaveChangesAsync();
            return addOfferModel;
        }
        #region Get        
        public async Task<OperationResult<GenericOperationResult, MinMaxPrice>> GetMinMaxPrice(List<int> BrandsIds, List<int> CategoriesIds)
        {
            OperationResult<GenericOperationResult, MinMaxPrice> result = new(GenericOperationResult.Failed);
            try
            {
                var data = new MinMaxPrice()
                {
                    MaxProductPrice = await Context.ProductSizesColors.Where(entity => entity.IsValid).Select(entity => entity.Price).DefaultIfEmpty().MaxAsync(),
                    MinProductPrice = await Context.ProductSizesColors.Where(entity => entity.IsValid).Select(entity => entity.Price).DefaultIfEmpty().MinAsync(),
                    MaxCoursePrice = await Context.Courses.Where(entity => entity.IsValid).Select(entity => entity.Price).DefaultIfEmpty().MaxAsync(),
                    MinCoursePrice = await Context.Courses.Where(entity => entity.IsValid).Select(entity => entity.Price).DefaultIfEmpty().MinAsync(),
                    MaxServicePrice = await Context.Services.Where(entity => entity.IsValid).Select(entity => entity.Price).DefaultIfEmpty().MaxAsync(),
                    MinServicePrice = await Context.Services.Where(entity => entity.IsValid).Select(entity => entity.Price).DefaultIfEmpty().MinAsync(),
                    BrandMaxMinPrice = BrandsIds != null ? new BrandMaxMinPrice()
                    {
                        MaxProductPrice = Context.ProductSizesColors.Where(pcs => pcs.IsValid
                        && BrandsIds.Contains(pcs.Product.BrandId)).Select(pr => pr.Price).DefaultIfEmpty().Max(),
                        MinProductPrice = Context.ProductSizesColors.Where(pcs => pcs.IsValid
                        && BrandsIds.Contains(pcs.Product.BrandId)).Select(pr => pr.Price).DefaultIfEmpty().Min()
                    } : null,
                    CategoryMaxMinPrice = CategoriesIds != null ? new CategoryMaxMinPrice()
                    {
                        MaxProductPrice = Context.ProductSizesColors.Where(pcs => pcs.IsValid
                       && CategoriesIds.Contains(pcs.Product.CategoryId)).Select(pr => pr.Price).DefaultIfEmpty().Max(),
                        MinProductPrice = Context.ProductSizesColors.Where(pcs => pcs.IsValid
                        && CategoriesIds.Contains(pcs.Product.CategoryId)).Select(pr => pr.Price).DefaultIfEmpty().Min()
                    } : null,
                };
                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(data);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion
    }
}
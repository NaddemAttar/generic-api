﻿
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.HealthCareSpecialty;
using IGenericControlPanel.Core.Dto.Service;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Extension;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;

using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class HealthCareSpecialtyRepository : BasicRepository<CPDbContext, HealthCareSpecialty>,
        IHealthCareSpecialtyRepository
    {
        public HealthCareSpecialtyRepository(CPDbContext context) : base(context)
        {
        }
        #region Actions
        public OperationResult<GenericOperationResult, HealthCareSpecialtyDto> ActionHealthCareSpecialty(
            HealthCareSpecialtyDto formDto)
        {
            OperationResult<GenericOperationResult,
                HealthCareSpecialtyDto> result = new(GenericOperationResult.ValidationError);

            ICollection<FileDetailsDto> images = formDto.Images;
            try
            {
                HealthCareSpecialty entity = formDto.Id != 0 ? ValidEntities
                    .Where(cat => cat.Id == formDto.Id && cat.IsValid)
                    .SingleOrDefault() : new HealthCareSpecialty();
                entity!.IsValid = true;
                entity.Name = formDto.Name;
                entity.ParentSpecialtyId = formDto.ParentId;

                if (formDto.Id == 0)
                {
                    if (images != null)
                    {
                        foreach (FileDetailsDto image in images)
                        {
                            entity.Files.Add(new HealthCareSpecialtyImage()
                            {
                                Checksum = image.Checksum,
                                FileContentType = image.FileContentType,
                                Description = image.Description,
                                Extension = image.Extension,
                                FileName = image.FileName,
                                Name = image.Name,
                                Url = image.Url,
                            });
                        }
                    }

                    _ = Context.HealthCareSpecialties.Add(entity);
                    if (images is not null)
                    {
                        foreach (FileDetailsDto image in images)
                        {
                            image.SourceId = entity.Id;
                        }

                    }
                }
                else
                {
                    if (formDto.RemovedFilesIds.Count() > 0)
                    {
                        List<Model.FileBase> removedFiles = Context.Files.Where(file => formDto.RemovedFilesIds.Contains(file.Id)).ToList();
                        Context.RemoveRange(removedFiles);
                        formDto.RemovedFilesUrls = removedFiles.Select(file => file.Url).ToList();
                    }

                    if (images.Count != 0)
                    {
                        foreach (FileDetailsDto image in images)
                        {
                            entity.Files.Add(new HealthCareSpecialtyImage()
                            {
                                Checksum = image.Checksum,
                                FileContentType = image.FileContentType,
                                Description = image.Description,
                                Extension = image.Extension,
                                FileName = image.FileName,
                                Name = image.Name,
                                Url = image.Url,
                            });
                        }

                    }
                    _ = Context.HealthCareSpecialties.Update(entity);
                }
                _ = Context.SaveChanges();
                formDto.Id = entity.Id;
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                   .UpdateResultStatus(GenericOperationResult.Failed);
            }
            return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(formDto);
        }


        #endregion
        #region Remove
        public OperationResult<GenericOperationResult> RemoveSpecialty(int Id)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.ValidationError);
            try
            {
                var entity = Context.HealthCareSpecialties
              .Where(entity => entity.IsValid && entity.Id == Id)
              .SingleOrDefault();

                if (entity is null)
                {
                    return result.AddError(ErrorMessages.SpecialtyNotExist)
                       .UpdateResultStatus(GenericOperationResult.Failed);
                }

                _ = Context.HealthCareSpecialties.Remove(entity);
                _ = Context.SaveChanges();
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError).UpdateResultStatus(GenericOperationResult.Failed);
            }
            return result.UpdateResultStatus(GenericOperationResult.Success);
        }
        #endregion
        #region Get
        public OperationResult<GenericOperationResult, IEnumerable<HealthCareSpecialtyDetailsDto>> GetAllSpecialty(string query, int pageNumber, int pageSize, bool enablePagination)
        {
            OperationResult<GenericOperationResult, IEnumerable<HealthCareSpecialtyDetailsDto>> result = new(GenericOperationResult.ValidationError);
            try
            {
                var RawData = Context.HealthCareSpecialties
                    .Where(mlc => mlc.ParentSpecialtyId == null && mlc.IsValid && (query == null || query == "" || mlc.Name.ToUpper().Contains(query.ToUpper())));
                if (enablePagination)
                    RawData = RawData.GetDataWithPagination(pageSize, pageNumber);

                var data = RawData.Include(entity => entity.Files).Include(ser => ser.SpecialtiesServices)
                    .Select(mlc => new HealthCareSpecialtyDetailsDto()
                    {
                        Id = mlc.Id,
                        Name = mlc.Name,
                        ParentId = mlc.ParentSpecialtyId,
                        Services = mlc.SpecialtiesServices.Where(ser => ser.IsValid && ser.Service.IsValid).Select(ser => new ServiceDetailsDto()
                        {
                            Id = ser.Service.Id,
                            Name = ser.Service.Name,
                            Description = ser.Service.Description,
                            Images = ser.Service.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                            {
                                Id = file.Id,
                                Checksum = file.Checksum,
                                FileContentType = file.FileContentType,
                                Description = file.Description,
                                FileName = file.FileName,
                                Name = file.Name,
                                Url = file.Url,
                                Extension = file.Extension
                            }).ToList(),
                        }).ToList(),
                        Images = mlc.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                        {
                            Id = file.Id,
                            Checksum = file.Checksum,
                            FileContentType = file.FileContentType,
                            Description = file.Description,
                            FileName = file.FileName,
                            Name = file.Name,
                            Url = file.Url,
                            Extension = file.Extension
                        }).ToList(),
                        Children = mlc.Children.Where(ch => ch.IsValid).Select(mlc1 => new HealthCareSpecialtyDetailsDto()
                        {
                            Id = mlc1.Id,
                            Name = mlc1.Name,
                            ParentId = mlc1.ParentSpecialtyId,
                            Services = mlc.SpecialtiesServices.Where(ser => ser.IsValid && ser.Service.IsValid).Select(ser => new ServiceDetailsDto()
                            {
                                Id = ser.Service.Id,
                                Name = ser.Service.Name,
                                Description = ser.Service.Description,
                                Images = ser.Service.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                                {
                                    Id = file.Id,
                                    Checksum = file.Checksum,
                                    FileContentType = file.FileContentType,
                                    Description = file.Description,
                                    FileName = file.FileName,
                                    Name = file.Name,
                                    Url = file.Url,
                                    Extension = file.Extension
                                }).ToList(),
                            }).ToList(),
                            Images = mlc1.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                            {
                                Id = file.Id,
                                Checksum = file.Checksum,
                                FileContentType = file.FileContentType,
                                Description = file.Description,
                                FileName = file.FileName,
                                Name = file.Name,
                                Url = file.Url,
                                Extension = file.Extension
                            }).ToList(),
                            Children = mlc1.Children.Where(ch => ch.IsValid).Select(mlc2 => new HealthCareSpecialtyDetailsDto()
                            {
                                Id = mlc2.Id,
                                Name = mlc2.Name,
                                ParentId = mlc2.ParentSpecialtyId,
                                Services = mlc.SpecialtiesServices.Where(ser => ser.IsValid && ser.Service.IsValid).Select(ser => new ServiceDetailsDto()
                                {
                                    Id = ser.Service.Id,
                                    Name = ser.Service.Name,
                                    Description = ser.Service.Description,
                                    Images = ser.Service.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                                    {
                                        Id = file.Id,
                                        Checksum = file.Checksum,
                                        FileContentType = file.FileContentType,
                                        Description = file.Description,
                                        FileName = file.FileName,
                                        Name = file.Name,
                                        Url = file.Url,
                                        Extension = file.Extension
                                    }).ToList(),
                                }).ToList(),
                                Images = mlc2.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                                {
                                    Id = file.Id,
                                    Checksum = file.Checksum,
                                    FileContentType = file.FileContentType,
                                    Description = file.Description,
                                    FileName = file.FileName,
                                    Name = file.Name,
                                    Url = file.Url,
                                    Extension = file.Extension
                                }).ToList(),
                                Children = mlc2.Children.Where(ch => ch.IsValid).Select(mlc3 => new HealthCareSpecialtyDetailsDto()
                                {
                                    Id = mlc3.Id,
                                    Name = mlc3.Name,
                                    ParentId = mlc3.ParentSpecialtyId,
                                    Services = mlc.SpecialtiesServices.Where(ser => ser.IsValid && ser.Service.IsValid).Select(ser => new ServiceDetailsDto()
                                    {
                                        Id = ser.Service.Id,
                                        Name = ser.Service.Name,
                                        Description = ser.Service.Description,
                                        Images = ser.Service.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                                        {
                                            Id = file.Id,
                                            Checksum = file.Checksum,
                                            FileContentType = file.FileContentType,
                                            Description = file.Description,
                                            FileName = file.FileName,
                                            Name = file.Name,
                                            Url = file.Url,
                                            Extension = file.Extension
                                        }).ToList(),
                                    }).ToList(),
                                    Images = mlc3.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                                    {
                                        Id = file.Id,
                                        Checksum = file.Checksum,
                                        FileContentType = file.FileContentType,
                                        Description = file.Description,
                                        FileName = file.FileName,
                                        Name = file.Name,
                                        Url = file.Url,
                                        Extension = file.Extension
                                    }).ToList(),
                                    Children = mlc3.Children.Where(ch => ch.IsValid).Select(mlc4 => new HealthCareSpecialtyDetailsDto()
                                    {
                                        Id = mlc4.Id,
                                        Name = mlc4.Name,
                                        ParentId = mlc4.ParentSpecialtyId,
                                        Services = mlc.SpecialtiesServices.Where(ser => ser.IsValid && ser.Service.IsValid).Select(ser => new ServiceDetailsDto()
                                        {
                                            Id = ser.Service.Id,
                                            Name = ser.Service.Name,
                                            Description = ser.Service.Description,
                                            Images = ser.Service.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                                            {
                                                Id = file.Id,
                                                Checksum = file.Checksum,
                                                FileContentType = file.FileContentType,
                                                Description = file.Description,
                                                FileName = file.FileName,
                                                Name = file.Name,
                                                Url = file.Url,
                                                Extension = file.Extension
                                            }).ToList(),
                                        }).ToList(),
                                        Images = mlc4.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                                        {
                                            Id = file.Id,
                                            Checksum = file.Checksum,
                                            FileContentType = file.FileContentType,
                                            Description = file.Description,
                                            FileName = file.FileName,
                                            Name = file.Name,
                                            Url = file.Url,
                                            Extension = file.Extension
                                        }).ToList(),
                                    })
                                })
                            })
                        })
                    })
                    .ToList();
                _ = result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                   .UpdateResultStatus(GenericOperationResult.Failed);
            }
            return result;

        }

        public OperationResult<GenericOperationResult, HealthCareSpecialtyDetailsDto> GetSpecialty(int Id)
        {
            OperationResult<GenericOperationResult, HealthCareSpecialtyDetailsDto> result = new(GenericOperationResult.ValidationError);
            try
            {

                var data = Context.HealthCareSpecialties
                    .Where(mlc => mlc.Id == Id && mlc.IsValid).Include(entity => entity.Files).Include(ser => ser.SpecialtiesServices)
                    .Select(mlc => new HealthCareSpecialtyDetailsDto()
                    {
                        Id = mlc.Id,
                        Name = mlc.Name,
                        ParentId = mlc.ParentSpecialtyId,
                        Services = mlc.SpecialtiesServices.Where(ser => ser.IsValid && ser.Service.IsValid).Select(ser => new ServiceDetailsDto()
                        {
                            Id = ser.Service.Id,
                            Name = ser.Service.Name,
                            Description = ser.Service.Description,
                            Images = ser.Service.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                            {
                                Id = file.Id,
                                Checksum = file.Checksum,
                                FileContentType = file.FileContentType,
                                Description = file.Description,
                                FileName = file.FileName,
                                Name = file.Name,
                                Url = file.Url,
                                Extension = file.Extension
                            }).ToList(),
                        }).ToList(),
                        Images = mlc.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                        {
                            Id = file.Id,
                            Checksum = file.Checksum,
                            FileContentType = file.FileContentType,
                            Description = file.Description,
                            FileName = file.FileName,
                            Name = file.Name,
                            Url = file.Url,
                            Extension = file.Extension
                        }).ToList(),
                        Children = mlc.Children.Where(ch => ch.IsValid).Select(mlc1 => new HealthCareSpecialtyDetailsDto()
                        {
                            Id = mlc1.Id,
                            Name = mlc1.Name,
                            ParentId = mlc1.ParentSpecialtyId,
                            Services = mlc.SpecialtiesServices.Where(ser => ser.IsValid && ser.Service.IsValid).Select(ser => new ServiceDetailsDto()
                            {
                                Id = ser.Service.Id,
                                Name = ser.Service.Name,
                                Description = ser.Service.Description,
                                Images = ser.Service.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                                {
                                    Id = file.Id,
                                    Checksum = file.Checksum,
                                    FileContentType = file.FileContentType,
                                    Description = file.Description,
                                    FileName = file.FileName,
                                    Name = file.Name,
                                    Url = file.Url,
                                    Extension = file.Extension
                                }).ToList(),
                            }).ToList(),
                            Images = mlc1.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                            {
                                Id = file.Id,
                                Checksum = file.Checksum,
                                FileContentType = file.FileContentType,
                                Description = file.Description,
                                FileName = file.FileName,
                                Name = file.Name,
                                Url = file.Url,
                                Extension = file.Extension
                            }).ToList(),
                            Children = mlc1.Children.Where(ch => ch.IsValid).Select(mlc2 => new HealthCareSpecialtyDetailsDto()
                            {
                                Id = mlc2.Id,
                                Name = mlc2.Name,
                                ParentId = mlc2.ParentSpecialtyId,
                                Services = mlc.SpecialtiesServices.Where(ser => ser.IsValid && ser.Service.IsValid).Select(ser => new ServiceDetailsDto()
                                {
                                    Id = ser.Service.Id,
                                    Name = ser.Service.Name,
                                    Description = ser.Service.Description,
                                    Images = ser.Service.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                                    {
                                        Id = file.Id,
                                        Checksum = file.Checksum,
                                        FileContentType = file.FileContentType,
                                        Description = file.Description,
                                        FileName = file.FileName,
                                        Name = file.Name,
                                        Url = file.Url,
                                        Extension = file.Extension
                                    }).ToList(),
                                }).ToList(),
                                Images = mlc2.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                                {
                                    Id = file.Id,
                                    Checksum = file.Checksum,
                                    FileContentType = file.FileContentType,
                                    Description = file.Description,
                                    FileName = file.FileName,
                                    Name = file.Name,
                                    Url = file.Url,
                                    Extension = file.Extension
                                }).ToList(),
                                Children = mlc2.Children.Where(ch => ch.IsValid).Select(mlc3 => new HealthCareSpecialtyDetailsDto()
                                {
                                    Id = mlc3.Id,
                                    Name = mlc3.Name,
                                    ParentId = mlc3.ParentSpecialtyId,
                                    Services = mlc.SpecialtiesServices.Where(ser => ser.IsValid && ser.Service.IsValid).Select(ser => new ServiceDetailsDto()
                                    {
                                        Id = ser.Service.Id,
                                        Name = ser.Service.Name,
                                        Description = ser.Service.Description,
                                        Images = ser.Service.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                                        {
                                            Id = file.Id,
                                            Checksum = file.Checksum,
                                            FileContentType = file.FileContentType,
                                            Description = file.Description,
                                            FileName = file.FileName,
                                            Name = file.Name,
                                            Url = file.Url,
                                            Extension = file.Extension
                                        }).ToList(),
                                    }).ToList(),
                                    Images = mlc3.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                                    {
                                        Id = file.Id,
                                        Checksum = file.Checksum,
                                        FileContentType = file.FileContentType,
                                        Description = file.Description,
                                        FileName = file.FileName,
                                        Name = file.Name,
                                        Url = file.Url,
                                        Extension = file.Extension
                                    }).ToList(),
                                    Children = mlc3.Children.Where(ch => ch.ParentSpecialtyId == Id && ch.IsValid).Select(mlc4 => new HealthCareSpecialtyDetailsDto()
                                    {
                                        Id = mlc4.Id,
                                        Name = mlc4.Name,
                                        ParentId = mlc4.ParentSpecialtyId,
                                        Services = mlc.SpecialtiesServices.Where(ser => ser.IsValid && ser.Service.IsValid).Select(ser => new ServiceDetailsDto()
                                        {
                                            Id = ser.Service.Id,
                                            Name = ser.Service.Name,
                                            Description = ser.Service.Description,
                                            Images = ser.Service.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                                            {
                                                Id = file.Id,
                                                Checksum = file.Checksum,
                                                FileContentType = file.FileContentType,
                                                Description = file.Description,
                                                FileName = file.FileName,
                                                Name = file.Name,
                                                Url = file.Url,
                                                Extension = file.Extension
                                            }).ToList(),
                                        }).ToList(),
                                        Images = mlc4.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                                        {
                                            Id = file.Id,
                                            Checksum = file.Checksum,
                                            FileContentType = file.FileContentType,
                                            Description = file.Description,
                                            FileName = file.FileName,
                                            Name = file.Name,
                                            Url = file.Url,
                                            Extension = file.Extension
                                        }).ToList(),
                                    })
                                })
                            })
                        })
                    }).AsEnumerable()
                    .SingleOrDefault();
                if (data is null)
                {
                    return result.AddError(ErrorMessages.SpecialtyNotExist)
                      .UpdateResultStatus(GenericOperationResult.NotFound);
                }
                _ = result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                   .UpdateResultStatus(GenericOperationResult.Failed);
            }
            return result;
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<HealthCareSpecialtyServiceDto>>> GetSpecialtyServicesByHealthCareSpecialtyId(int HealthCareSpecialtyId)
        {
            OperationResult<GenericOperationResult, IEnumerable<HealthCareSpecialtyServiceDto>> result = new(GenericOperationResult.ValidationError);
            try
            {
                var entity = await Context.HealthCareSpecialtyServices.Where(entity => entity.IsValid &&
                entity.HealthCareSpecialtyId == HealthCareSpecialtyId).Select(entity => new HealthCareSpecialtyServiceDto()
                {
                    ServiceId = entity.ServiceId,
                    ServiceName = entity.Service.Name,
                    ServiceDescription = entity.Service.Description,
                    HealthCareProviders = entity.HealthCareSpecialty.HealthCareProviderSpecialties.Where(hsc => hsc.IsValid)
                   .Select(hsc => new HealthCareProviderSpecialtyDto()
                   {
                       HealthCareProviderId = hsc.CPUser.Id,
                       HealthCareProviderName = hsc.CPUser.FirstName + " " + hsc.CPUser.LastName
                   }).ToList()
                }).ToListAsync();
                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(entity);
            }
            catch (Exception ex)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<HealthCareSpecialtyWithProvidersDto>>> GetSpecialtyByServiceId(int ServiceId)
        {
            OperationResult<GenericOperationResult, IEnumerable<HealthCareSpecialtyWithProvidersDto>> result = new(GenericOperationResult.ValidationError);
            try
            {
                var entity = await Context.HealthCareSpecialtyServices.Where(entity => entity.IsValid &&
                entity.ServiceId == ServiceId).Select(entity => new HealthCareSpecialtyWithProvidersDto()
                {
                    Id = entity.HealthCareSpecialty.Id,
                    Name = entity.HealthCareSpecialty.Name,
                    HealthCareProviderSpecialties = entity.HealthCareSpecialty.HealthCareProviderSpecialties.Where(hsc => hsc.IsValid)
                   .Select(hsc => new HealthCareProviderSpecialtyDto()
                   {
                       HealthCareProviderId = hsc.CPUser.Id,
                       HealthCareProviderName = hsc.CPUser.FirstName + " " + hsc.CPUser.LastName
                   }).ToList()
                }).ToListAsync();
                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(entity);
            }
            catch (Exception ex)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        #endregion



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.ContactInfo;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class ContactInfoRepository : BasicRepository<CPDbContext, ContactInfo>, IContactInfoRepository
    {
        public ContactInfoRepository(CPDbContext context) : base(context)
        {

        }

        public OperationResult<GenericOperationResult, ContactInfoDto> ActionContactInfo(ContactInfoDto contactInfoDto)
        {
            OperationResult<GenericOperationResult, ContactInfoDto> result = new(GenericOperationResult.ValidationError);
            try
            {
                ContactInfo entity = contactInfoDto.Id != 0 ? Context.ContactInfo.Where(ci => ci.Id == contactInfoDto.Id).SingleOrDefault() : new ContactInfo();
                entity.key = contactInfoDto.Key;
                entity.Value = contactInfoDto.Value;
                if (contactInfoDto.Id == 0)
                {
                    _ = Context.ContactInfo.Add(entity);
                }
                _ = Context.SaveChanges();
                contactInfoDto.Id = entity.Id;
                return result.UpdateResultData(contactInfoDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
            }


        }

        public OperationResult<GenericOperationResult, IEnumerable<ContactInfoDto>> GetContactInfo()
        {
            OperationResult<GenericOperationResult, IEnumerable<ContactInfoDto>> result = new(GenericOperationResult.ValidationError);
            try
            {
                IQueryable<ContactInfoDto> data = Context.ContactInfo.Where(ci => ci.IsValid).Select(ci => new ContactInfoDto()
                {
                    Id = ci.Id,
                    Key = ci.key,
                    Value = ci.Value
                });
                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
    }
}

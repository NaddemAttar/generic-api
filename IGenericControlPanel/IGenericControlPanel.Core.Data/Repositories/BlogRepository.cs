﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Content.Dto.Tips;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.Dto.Blog;
using IGenericControlPanel.Core.Dto.Category;
using IGenericControlPanel.Core.Dto.Group;
using IGenericControlPanel.Core.Dto.Service;
using IGenericControlPanel.Core.IData;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using IGenericControlPanel.SharedKernal.Utils;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data
{
    public class BlogRepository : BasicRepository<CPDbContext, Blog>, IBlogRepository
    {
        #region Constructs && Properties
        private IGamificationMovementsRepository _gamificationMovementRepo { get; }
        private IUserLanguageRepository UserLanguageRepository { get; }
        private IMailRepository mailService { get; }
        public IUserRepository UserRepository { get; }
        private IHttpContextAccessor httpContextAccessor { get; }
        public BlogRepository(CPDbContext dbContext,
            IMapper mapper, IMailRepository mailService, IHttpContextAccessor httpContextAccessor,
            IUserLanguageRepository userLanguageRepository,
            IUserRepository userRepository,
            IGamificationMovementsRepository gamificationMovementRepo
            ) : base(dbContext, mapper)
        {
            UserLanguageRepository = userLanguageRepository;
            this.mailService = mailService;
            UserRepository = userRepository;
            this.httpContextAccessor = httpContextAccessor;
            _gamificationMovementRepo = gamificationMovementRepo;
        }

        #endregion

        #region Action Section
        public async Task<OperationResult<GenericOperationResult, BlogFormDto>> ActionAsync(
            BlogFormDto formDto)
        {
            OperationResult<GenericOperationResult,
                BlogFormDto> result = new(GenericOperationResult.ValidationError);

            var isCreate = formDto != null &&
                   formDto.Id == 0 &&
                   formDto.Title != null;

            result = isCreate ? await CreateAsync(formDto) : await UpdateAsync(formDto);

            return result.IsSuccess ? new OperationResult<GenericOperationResult,
                    BlogFormDto>(GenericOperationResult.Success, entity: result.Result) : result;

        }
        #endregion

        #region Create New Blog
        private async Task<OperationResult<GenericOperationResult, BlogFormDto>> CreateAsync(
            BlogFormDto formDto)
        {
            var result = new OperationResult<GenericOperationResult, BlogFormDto>(GenericOperationResult.Failed);
            try
            {
                var entity = new Blog()
                {
                    Title = formDto.Title,
                    Content = formDto.Content,
                    BlogDate = DateTime.Now,
                    CultureCode = CurrentCultureCode,
                    BlogType = formDto.BlogType,
                    YouTubeLink = formDto.YouTubeLink,
                    ServiceId = formDto.ServiceId,
                    PostTypeId = formDto.PostTypeId,
                    GroupId = formDto.GroupId,
                    ShowFirst = false,
                    IsHidden = false
                };
                foreach (var MultiLevelCategoryId in formDto.MultiLevelCategoryIds)
                {
                    Context.MultiLevelCategoryEntities.Add(new MultiLevelCategoryRelatedEntity()
                    {
                        Blog = entity,
                        MultiLevelCategoryId = MultiLevelCategoryId
                    });
                }

                foreach (var attachment in formDto.Attachments)
                {
                    var file = new BlogFile()
                    {
                        Checksum = attachment.Checksum,
                        FileContentType = attachment.FileContentType,
                        Description = attachment.Description,
                        Extension = attachment.Extension,
                        FileName = attachment.FileName,
                        Name = attachment.Name,
                        Url = attachment.Url
                    };
                    entity.Files.Add(Mapper.Map<BlogFile>(file));
                }
                await Context.AddAsync(entity);
                await Context.SaveChangesAsync();

                if (formDto.SendEmail)
                {
                    var body = "";
                    var host = "https://" + httpContextAccessor.HttpContext.Request.Host.Value + "/blogs/" + entity.Id.ToString();

                    List<string> emails = Context.Subscribers.Where(sub => sub.IsValid).Select(sub => sub.Email).ToList();
                    if (formDto.Content.Length >= 600)
                    {
                        body = $"{formDto.Content[..600]} <a href=`{host}`>Read More ...</a> ";
                    }

                    await mailService.SendEmailAsync(new MailRequest()
                    {
                        ToEmails = emails,
                        Subject = formDto.Title,
                        Body = body != "" ? body : formDto.Content,
                    });
                }

                formDto.Attachments = entity.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                {
                    Id = file.Id,
                    Checksum = file.Checksum,
                    FileContentType = file.FileContentType,
                    Description = file.Description,
                    FileName = file.FileName,
                    Name = file.Name,
                    Url = file.Url
                }).ToList();
                formDto.Id = entity.Id;

                foreach (var file in formDto.Attachments)
                {
                    file.SourceId = entity.Id;
                }

                var savePointsResult = await _gamificationMovementRepo.SavePoints(new List<string>
                {
                    GamificationNames.AddBlog
                });

                if (!savePointsResult.IsSuccess)
                {
                    return result.AddError(savePointsResult.ErrorMessages)
                        .UpdateResultStatus(GenericOperationResult.Failed);
                }

                return result.UpdateResultData(formDto)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed)
                    .AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Update Blog
        private async Task<OperationResult<GenericOperationResult, BlogFormDto>> UpdateAsync(BlogFormDto formDto)
        {
            OperationResult<GenericOperationResult,
                BlogFormDto> result = new(GenericOperationResult.Failed);
            try
            {
                var entityId = formDto?.Id ?? 0;

                var data = ValidEntities
                    .Include(entity => entity.Files)
                    .Where(entity => entity.Id == entityId)
                    .SingleOrDefault();

                if (formDto != null)
                {
                    var removedFilesdata = Context.Blogs
                        .Where(blog => blog.Id == formDto.Id)
                        .Include(entity => entity.Files)
                        .FirstOrDefault().Files.Where(file => formDto.RemovedFilesIds.Contains(file.Id))
                        .Select(file => new FileDetailsDto()
                        {
                            Checksum = file.Checksum,
                            FileContentType = file.FileContentType,
                            Description = file.Description,
                            FileName = file.FileName,
                            Name = file.Name,
                            Url = file.Url
                        });

                    Context.RemoveRange(Context.Files.Where(file => formDto.RemovedFilesIds.Contains(file.Id)));
                    formDto.RemovedFilesUrls = removedFilesdata.Select(files => files.Url).ToList();

                    var entityToEdit = ValidEntities.Where(entit => entit.Id == formDto.Id).Include(entity => entity.Files).SingleOrDefault();
                    if (entityToEdit != null)
                    {
                        entityToEdit.Id = formDto.Id;
                        entityToEdit.Title = formDto.Title;
                        entityToEdit.Content = formDto.Content;
                        //entityToEdit.BlogDate = DateTime.Parse(formDto.Date);
                        entityToEdit.IsValid = true;
                        entityToEdit.BlogType = formDto.BlogType;
                        entityToEdit.YouTubeLink = formDto.YouTubeLink;
                        entityToEdit.ServiceId = formDto.ServiceId;
                        entityToEdit.PostTypeId = formDto.PostTypeId;
                        entityToEdit.GroupId = formDto.GroupId;
                        entityToEdit.CreatedBy = data.CreatedBy;
                        entityToEdit.ShowFirst = formDto.ShowFirst;
                        entityToEdit.IsHidden = formDto.IsHidden;
                        Context.MultiLevelCategoryEntities.RemoveRange(Context.MultiLevelCategoryEntities
                            .Where(mc => mc.IsValid && mc.BlogId == entityToEdit.Id && !formDto.MultiLevelCategoryIds.Contains(mc.MultiLevelCategoryId)).ToList());
                        await Context.SaveChangesAsync();
                        foreach (var MultiLevelCategoryId in formDto.MultiLevelCategoryIds)
                        {
                            var Category = Context.MultiLevelCategories.Where(cat => cat.IsValid && cat.Id == MultiLevelCategoryId).Include(cat => cat.MultiLevelCategoryRelatedEntities)
                            .SingleOrDefault();
                            if (Category != null)
                            {
                                if (!Category.MultiLevelCategoryRelatedEntities.Any(mlc => mlc.BlogId == entityToEdit.Id))
                                {

                                    Context.MultiLevelCategoryEntities.Add(new MultiLevelCategoryRelatedEntity()
                                    {
                                        MultiLevelCategoryId = Category.Id,
                                        BlogId = entityToEdit.Id
                                    });
                                }
                            }
                        }
                        foreach (var attachment in formDto.Attachments)
                        {
                            var extension = $".{attachment.Url.Split('.')[1]}";
                            var file = new BlogFile()
                            {
                                Checksum = attachment.Checksum,
                                FileContentType = attachment.FileContentType,
                                Description = attachment.Description,
                                Extension = extension,
                                FileName = attachment.FileName,
                                Name = attachment.Name,
                                Url = attachment.Url
                            };
                            entityToEdit.Files.Add(file);
                        }

                        Context.Update(entityToEdit);
                    }

                    await Context.SaveChangesAsync();

                    result.UpdateResultData(formDto)
                        .UpdateResultStatus(GenericOperationResult.Success);

                    formDto.Attachments = entityToEdit.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                    {
                        Id = file.Id,
                        Checksum = file.Checksum,
                        FileContentType = file.FileContentType,
                        Description = file.Description,
                        FileName = file.FileName,
                        Name = file.Name,
                        Url = file.Url
                    }).ToList();
                }
                else
                {
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.BlogNotFound);
                }
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
            }

            return result;
        }
        #endregion

        #region Get Section
        public async Task<OperationResult<GenericOperationResult, BlogDto>> GetAsync(
            int id, string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult,
                BlogDto> result = new(GenericOperationResult.Failed);

            try
            {
                var data = ValidEntities
                    .Include(entity => entity.Translations)
                    .Include(entity => entity.Files)
                    .Include(entity => entity.Service)
                    .Include(entity => entity.PostType)
                    .Include(entity => entity.BlogComments)
                    .Include(entity => entity.MultiLevelCategoryRelatedEntities).ThenInclude(mll => mll.MultiLevelCategory)
                    .Include(entity => entity.Group)
                    .Where(entity => entity.Id == id)
                    .ToList()
                    .Select(entity => new BlogDetailsDto()
                    {
                        MultiLevelCategoryIds = entity.MultiLevelCategoryRelatedEntities.Where(ml => ml.IsValid).Select(ml => ml.MultiLevelCategoryId).ToList(),
                        Id = entity.Id,
                        Title = entity.Title,
                        PostTypeId = entity.PostTypeId,
                        Content = entity.Content,
                        BlogCommentsCount = entity.BlogComments.Count,
                        IsHidden = entity.IsHidden,
                        ShowFirst = entity.ShowFirst,
                        BlogComments = entity.BlogComments.Where(entity => entity.IsValid).Select(entity => new Dto.BlogComment.BlogCommentDto()
                        {
                            Id = entity.Id,
                            BlogId = entity.BlogId,
                            CommentContent = entity.CommentContent,
                            ImageUrl = entity.ImageUrl,
                        }).ToList(),
                        MetaDescription = entity.MetaDescription,
                        MultiLevelCategories = entity.MultiLevelCategoryRelatedEntities.Where(ml => ml.IsValid).Select(ml => new MultiLevelCategoryDto()
                        {
                            Id = ml.MultiLevelCategoryId,
                            Name = ml.MultiLevelCategory.Name
                        }).ToList(),
                        BlogType = entity.BlogType,
                        Date = new DateTimeOffset(entity.BlogDate,
                                       TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time").GetUtcOffset(entity.BlogDate)),
                        UrlNumbering = entity.UrlNumbering,
                        YouTubeLink = entity.YouTubeLink,

                        CultureCode = entity.CultureCode,

                        Group = entity.GroupId == null ? null : new GroupDto()
                        {
                            Name = entity.Group.Name,
                            Id = entity.GroupId.Value
                        },
                        Service = entity.Service == null ? null : new ServiceDto()
                        {
                            Id = entity.ServiceId.Value,
                            Name = entity.Service.Name
                        },
                        Attachments = entity.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                        {
                            Id = file.Id,
                            Checksum = file.Checksum,
                            FileContentType = file.FileContentType,
                            Description = file.Description,
                            FileName = file.FileName,
                            Name = file.Name,
                            Url = file.Url
                        }).ToList()

                    }).FirstOrDefault();

                return await Task.FromResult(data is null
                    ? result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.BlogNotFound)
                    : result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success));
            }
            catch (Exception)
            {
                return await Task.FromResult(result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed));
            }

        }
        private async Task<IEnumerable<int>> GetCategoriesInCategoryAsync(int categoryId)
        {
            var categories = Context.MultiLevelCategories.Include(cat => cat.Children).ToList();
            var categoriesIds = new List<int>();
            var currentParent = categories.Where(cat => cat.Id == categoryId).SingleOrDefault();
            await addToListOfCategoriesAsync(currentParent, categoriesIds);
            return categoriesIds;
        }
        private async Task addToListOfCategoriesAsync(MultiLevelCategory cat, List<int> catList)
        {
            catList.Add(cat.Id);
            foreach (var item in cat.Children)
            {
                await addToListOfCategoriesAsync(item, catList);
            }
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<BlogDetailsDto>>> GetAllDetailedPagedAsync
            (bool? showFirst, bool? isHidden,
            int? PostTypeId, int? MultiLevelCategoryId, int? ServiceId, int? GroupId,
            string query, int? pageNumber = 0, int? pageSize = null, string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, IEnumerable<BlogDetailsDto>> result = new(GenericOperationResult.Failed);
            try
            {
                pageSize = pageSize ?? 10;
                pageNumber = pageNumber ?? 0;
                var userInfo = await UserRepository.GetCurrentUserInfo();
                var categoriesIds = new List<int>();
                if (MultiLevelCategoryId != null)
                {
                    var ids = await GetCategoriesInCategoryAsync(MultiLevelCategoryId.Value);
                    categoriesIds = ids.ToList();
                }
                var builder = WebApplication.CreateBuilder();
                bool multiProviders = bool.Parse(builder.Configuration.GetSection("MultiProviders").Value.ToLower());
                var data = ValidEntities
                                .Where(entity => (userInfo.CurrentUserId == 0 || userInfo.IsUserAdmin
                                || (multiProviders == false ? true : userInfo.IsHealthCareProvider ? userInfo.CurrentUserId == entity.CreatedBy : true))
                                && (PostTypeId != null ? entity.PostTypeId == PostTypeId : true)
                                && (GroupId != null ? entity.GroupId == GroupId : true)
                                && (isHidden == null || entity.IsHidden == isHidden)
                                // && (CurrentCultureCode==entity.CultureCode)
                                && (MultiLevelCategoryId == null ? true :
                                (entity.MultiLevelCategoryRelatedEntities.Where(cat => cat.MultiLevelCategoryId == MultiLevelCategoryId && cat.IsValid).Any(x => categoriesIds.Contains(x.MultiLevelCategoryId))))
                                && (!string.IsNullOrEmpty(query) ? entity.Title.Replace(" ", "").Contains(query.Replace
                                (" ", "")) || entity.Content.Replace(" ", "").Contains(query.Replace
                                (" ", "")) : true))
                                 .OrderByDescending(entity => entity.ShowFirst)
                                 .Skip(pageNumber.Value * pageSize.Value)
                                 .Take(pageSize.Value)
                                 .Include(entity => entity.Translations)
                                 .Include(entity => entity.Files)
                                 .Include(entity => entity.Service)
                                 .Include(entity => entity.Group)
                                 .Include(entity => entity.PostType)
                                 .Include(entity => entity.BlogComments)
                                 .Include(entity => entity.MultiLevelCategoryRelatedEntities).ThenInclude(x => x.MultiLevelCategory)
                                 .ToList()
                                 .Select(entity => new BlogDetailsDto()
                                 {
                                     MultiLevelCategories = entity.MultiLevelCategoryRelatedEntities.Where(ml => ml.IsValid).Select(ml => new MultiLevelCategoryDto()
                                     {
                                         Id = ml.MultiLevelCategoryId,
                                         Name = ml.MultiLevelCategory.Name
                                     }).ToList(),
                                     Id = entity.Id,
                                     Title = entity.Title,
                                     PostTypeId = entity.PostTypeId,
                                     Content = entity.Content,
                                     MetaDescription = entity.MetaDescription,
                                     BlogType = entity.BlogType,
                                     BlogCommentsCount = entity.BlogComments.Count,
                                     BlogComments = entity.BlogComments.Where(entity => entity.IsValid).Select(entity => new Dto.BlogComment.BlogCommentDto()
                                     {
                                         Id = entity.Id,
                                         BlogId = entity.BlogId,
                                         CommentContent = entity.CommentContent,
                                         ImageUrl = entity.ImageUrl,
                                     }).ToList(),
                                     Date = new DateTimeOffset(entity.BlogDate,
                                           TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time").GetUtcOffset(entity.BlogDate)),
                                     UrlNumbering = entity.UrlNumbering,
                                     YouTubeLink = entity.YouTubeLink,
                                     Tip = Context.Tips.Where(tip => tip.IsValid).Select(tip => new TipsDto()
                                     {
                                         Title = tip.Title,
                                         Description = tip.Description,
                                     }).FirstOrDefault(),
                                     CultureCode = entity.CultureCode,

                                     Group = entity.GroupId == null ? null : new GroupDto()
                                     {
                                         Name = entity.Group.Name,
                                         Id = entity.GroupId.Value
                                     },
                                     Attachments = entity.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                                     {
                                         Id = file.Id,
                                         Checksum = file.Checksum,
                                         FileContentType = file.FileContentType,
                                         Description = file.Description,
                                         FileName = file.FileName,
                                         Name = file.Name,
                                         Url = file.Url,
                                         Extension = file.Extension
                                     }).ToList()
                                 });
                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<MultiLevelCategoryEntityDto>>> GetBlogCategoriesAsync(int BlogId)
        {
            var result = new OperationResult<GenericOperationResult,
                IEnumerable<MultiLevelCategoryEntityDto>>(GenericOperationResult.Failed);

            var blogFound = Context.Blogs.Any(b => b.Id == BlogId);
            if (!blogFound)
                return result.AddError(ErrorMessages.BlogNotFound).UpdateResultStatus(GenericOperationResult.NotFound);

            var entity = await Context.MultiLevelCategoryEntities
                .Where(entity => entity.IsValid && entity.BlogId == BlogId)
                .Select(entity => new MultiLevelCategoryEntityDto()
                {
                    MultiLevelCategoryId = entity.MultiLevelCategoryId
                }).ToListAsync();

            return result.UpdateResultData(entity).UpdateResultStatus(GenericOperationResult.Success);
        }
        #endregion

        #region Remove Blog
        public async Task<OperationResult<GenericOperationResult>> RemoveAsync(int id)
        {
            var result = new OperationResult<GenericOperationResult>(GenericOperationResult.ValidationError);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var blogEntity = await ValidEntities
                             .SingleOrDefaultAsync(blog => blog.Id == id);

                if (blogEntity is null)
                {
                    return result.AddError(ErrorMessages.BlogNotFound)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                Context.Remove(blogEntity);

                await Context.SaveChangesAsync();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        #endregion

        #region Action File
        public async Task<OperationResult<GenericOperationResult, FileDetailsDto>> ActionFileAsync(FileDetailsDto file)
        {
            var result = new OperationResult<GenericOperationResult,
                FileDetailsDto>(GenericOperationResult.Failed);
            try
            {
                var BlogFile = new UnAttachedFilesSet()
                {
                    Checksum = file.Checksum,
                    FileContentType = file.FileContentType,
                    Description = file.Description,
                    Extension = file.Extension,
                    FileName = file.FileName,
                    Name = file.Name,
                    Url = file.Url,
                };
                //Context.Files.Add(BlogFile);
                //Context.SaveChanges();
                //file.Id = BlogFile.Id;
                return await Task.FromResult(
                    result.UpdateResultData(file)
                    .UpdateResultStatus(GenericOperationResult.Success));
            }
            catch (Exception)
            {
                return await Task.FromResult(result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed));
            }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Medicine;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Extension;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;

using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class MedicineRepository : BasicRepository<CPDbContext, Medicine>, IMedicineRepository
    {
        #region Properties & Constructor
        private IUserRepository UserRepository { get; }
        public MedicineRepository(
            CPDbContext context,
            IUserRepository userRepository) : base(context)
        {
            UserRepository = userRepository;
        }
        #endregion

        #region Medicine Action
        public OperationResult<GenericOperationResult, MedicineFormDto> Action(MedicineFormDto medicineFormDto)
        {
            OperationResult<GenericOperationResult,
                MedicineFormDto> result = new(GenericOperationResult.ValidationError);

            if (string.IsNullOrEmpty(medicineFormDto.Name))
                return result.AddError(ErrorMessages.MedicineNameRequired);

            try
            {
                medicineFormDto = medicineFormDto.Id == 0 ?
                    AddMedicine(medicineFormDto) :
                    UpdateMedicine(medicineFormDto);

                if (medicineFormDto is null)
                {
                    return result.AddError(ErrorMessages.MedicineNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                Context.SaveChanges();
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }

            return result.UpdateResultData(medicineFormDto)
                 .UpdateResultStatus(GenericOperationResult.Success);
        }

        #region Add Medicine
        private MedicineFormDto AddMedicine(MedicineFormDto medicineFormDto)
        {
            Medicine entity = new()
            {
                Name = medicineFormDto.Name,
            };

            if (medicineFormDto.Images != null)
            {
                entity = AddingMedicineFiles(medicineFormDto.Images, entity);
            }
            Context.Medicines.Add(entity);
            if (medicineFormDto.Images is not null)
            {
                foreach (FileDetailsDto image in medicineFormDto.Images)
                {
                    image.SourceId = entity.Id;
                }
            }
            Context.SaveChanges();
            medicineFormDto.Id=entity.Id;

            return medicineFormDto;
        }
        private Medicine AddingMedicineFiles(IEnumerable<FileDetailsDto> images, Medicine entity)
        {
            foreach (FileDetailsDto image in images)
            {
                entity.Files.Add(new MedicineFile()
                {
                    Checksum = image.Checksum,
                    FileContentType = image.FileContentType,
                    Description = image.Description,
                    Extension = image.Extension,
                    FileName = image.FileName,
                    Name = image.Name,
                    Url = image.Url,
                });
            }

            return entity;
        }
        #endregion

        #region Update Medicine
        private MedicineFormDto UpdateMedicine(MedicineFormDto medicineFormDto)
        {
            var entity = Context.Medicines
                    .Where(entity => entity.IsValid && entity.Id == medicineFormDto.Id)
                    .SingleOrDefault();

            if (entity is null) return null;

            entity.Id = medicineFormDto.Id;
            entity.Name = medicineFormDto.Name;
            if (medicineFormDto.RemovedFilesIds.Count() > 0)
            {
                medicineFormDto = RemoveMedicineFiles(medicineFormDto);
            }
            Context.Entry(entity).State = EntityState.Detached;
            if (medicineFormDto.Images.Count != 0)
            {
                entity = AddingMedicineFiles(medicineFormDto.Images, entity);
            }
            Context.Medicines.Update(entity);

            return medicineFormDto;
        }
        private MedicineFormDto RemoveMedicineFiles(MedicineFormDto medicineFormDto)
        {
            var removedFiles = Context.Files
                    .Where(file => medicineFormDto.RemovedFilesIds.Contains(file.Id))
                    .ToList();

            Context.RemoveRange(removedFiles);
            medicineFormDto.RemovedFilesUrls = removedFiles
                .Select(file => file.Url)
                .ToList();

            return medicineFormDto;
        }
        #endregion

        #endregion

        #region Patient Medicine Action
        public OperationResult<GenericOperationResult> AddPatientMedicine(int medicineId, int userId)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.ValidationError);

            try
            {
                userId = userId != 0 ? userId : UserRepository.CurrentUserIdentityId();

                var addNewPatientMedicine = new PatientMedicine()
                {
                    UserId = userId,
                    MedicineId = medicineId
                };

                Context.PatientMedicines.Add(addNewPatientMedicine);
                Context.SaveChanges();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                     .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion

        #region Get 
        public OperationResult<GenericOperationResult, IEnumerable<MedicineFormDto>> GetAllMedicines(
            int? userId, bool enablePagination, int pageSize, int pageNumber)
        {
            OperationResult<GenericOperationResult,
                IEnumerable<MedicineFormDto>> result = new(GenericOperationResult.ValidationError);
            try
            {
                var medicines = Context.Medicines
                     .Include(m => m.Files)
                     .Where(entity => entity.IsValid
                     && (userId == null
                     || entity.PatientMedicine
                     .Any(entity => entity.UserId == userId)));

                if (enablePagination)
                {
                    medicines = medicines.GetDataWithPagination(pageSize, pageNumber);
                }
                var resultData = medicines.Select(entity =>
                         new MedicineFormDto()
                         {
                             Id = entity.Id,
                             Name = entity.Name,
                             Images = entity.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                             {
                                 Id = file.Id,
                                 Checksum = file.Checksum,
                                 FileContentType = file.FileContentType,
                                 Description = file.Description,
                                 FileName = file.FileName,
                                 Name = file.Name,
                                 Url = file.Url,
                                 Extension = file.Extension
                             }).ToList()
                         }).ToList();

                return result.UpdateResultData(resultData)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        public async Task<OperationResult<GenericOperationResult, IEnumerable<PatientMedicineDto>>> GetAllPatientMedicines(
            int? userId, bool enablePagination, int pageSize, int pageNumber)
        {
            OperationResult<GenericOperationResult,
                IEnumerable<PatientMedicineDto>> result = new(GenericOperationResult.ValidationError);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var medicines = Context.PatientMedicines
                        .Where(pm => pm.IsValid && (!userId.HasValue && userInfo.CurrentUserId == pm.UserId
                       || ((userInfo.IsUserAdmin || userInfo.IsNormalUser) && pm.UserId == userId)
                       || (userInfo.IsHealthCareProvider && (userId == pm.CreatedBy && userId == pm.UserId)
                       || (userInfo.CurrentUserId == pm.CreatedBy && userId == pm.UserId))));

                if (enablePagination)
                {
                    medicines = medicines.GetDataWithPagination(pageSize, pageNumber);
                }

                var resultData = medicines.Select(entity =>
                            new PatientMedicineDto()
                            {
                                Id = entity.Id,
                                MedicineName = entity.Medicine.Name,
                                MedicineId = entity.MedicineId,
                                UserId = entity.UserId,
                                UserName = $"{entity.User.FirstName} ${entity.User.LastName}"
                            }).ToList();

                return result.UpdateResultData(resultData)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public OperationResult<GenericOperationResult, MedicineInfoDto> GetMedicine(int Id)
        {
            OperationResult<GenericOperationResult,
                MedicineInfoDto> result = new(GenericOperationResult.ValidationError);

            try
            {
                var entity = Context.Medicines
               .Where(entity => entity.IsValid && entity.Id == Id)
               .Include(entity => entity.Files)
               .SingleOrDefault();

                if (entity is null)
                {
                    return result.AddError(ErrorMessages.MedicineNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                MedicineInfoDto medicine = new()
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    CreatedBy = entity.CreatedBy,
                    UserName = Context.Users
                    .Where(userEntity => userEntity.Id == entity.CreatedBy)
                    .Select(userEntity => userEntity.UserName)
                    .SingleOrDefault(),
                    PatientMedicines = Context.PatientMedicines
                    .Where(medicine => medicine.Medicine == entity)
                    .Select(entity => new PatientMedicineDto()
                    {
                        Id = entity.Id,
                        UserId = entity.UserId,
                        UserName = entity.User.UserName
                    }),
                    Images = entity.Files.Where(file => file.IsValid).Select(file => new FileDetailsDto()
                    {
                        Id = file.Id,
                        Checksum = file.Checksum,
                        FileContentType = file.FileContentType,
                        Description = file.Description,
                        FileName = file.FileName,
                        Name = file.Name,
                        Url = file.Url,
                        Extension = file.Extension,
                    }).ToList()
                };

                return result.UpdateResultData(medicine)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        public OperationResult<GenericOperationResult, IEnumerable<MedicinSelectDto>> GetMedicineAutoComplete(string query)
        {
            OperationResult<GenericOperationResult,
                IEnumerable<MedicinSelectDto>> result = new(GenericOperationResult.ValidationError);

            try
            {
                var data = ValidEntities
                    .Where(med => med.Name.ToUpper().Contains(query))
                    .Select(med => new MedicinSelectDto()
                    {
                        Id = med.Id,
                        Name = med.Name
                    });

                return result.UpdateResultData(data)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion

        #region Remove
        public OperationResult<GenericOperationResult> RemoveMedicine(int id)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.ValidationError);

            try
            {
                var entity = Context.Medicines
               .Where(entity => entity.IsValid && entity.Id == id)
               .SingleOrDefault();

                if (entity is null)
                {
                    return result.AddError(ErrorMessages.MedicineNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                Context.Medicines.Remove(entity);
                Context.SaveChanges();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public async Task<OperationResult<GenericOperationResult>> RemovePatientMedicine(int patientMedicineId)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.ValidationError);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var entity = Context.PatientMedicines
                    .Where(entity => entity.IsValid
                    && (userInfo.IsUserAdmin || userInfo.CurrentUserId == entity.CreatedBy)
                    && entity.Id == patientMedicineId)
                    .SingleOrDefault();

                if (entity is null)
                {
                    return result.AddError(ErrorMessages.PatientMedicineNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                Context.PatientMedicines.Remove(entity);
                Context.SaveChanges();
                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion
    }
}

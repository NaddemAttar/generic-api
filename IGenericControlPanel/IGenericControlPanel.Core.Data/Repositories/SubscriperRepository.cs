﻿using System;
using System.Collections.Generic;
using System.Linq;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Subscription;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class SubscriperRepository : BasicRepository<CPDbContext, Subscriber>, ISubscriberRepository
    {
        public SubscriperRepository(CPDbContext context) : base(context)
        {
        }
        #region Actions
        public OperationResult<GenericOperationResult, SubscriberDto> AddSubscriber(SubscriberDto formDto)
        {
            OperationResult<GenericOperationResult, SubscriberDto> result = new(GenericOperationResult.ValidationError);
            try
            {
                var entity = new Subscriber()
                {
                    FirstName = formDto.FirstName,
                    LastName = formDto.LastName,
                    Email = formDto.Email,
                };
                Context.Subscribers.Add(entity);
                Context.SaveChanges();
                result.UpdateResultData(formDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                  .UpdateResultStatus(GenericOperationResult.Failed);
            }
            return result;
        }
        public OperationResult<GenericOperationResult, SubscriberDto> UpdateSubscriber(SubscriberDto formDto)
        {
            OperationResult<GenericOperationResult, SubscriberDto> result = new(GenericOperationResult.ValidationError);
            try
            {
                var entity = Context.Subscribers.Where(entity => entity.IsValid && entity.Id == formDto.Id).SingleOrDefault();
                if (entity is not null)
                {
                    entity.FirstName = formDto.FirstName;
                    entity.LastName = formDto.LastName;
                    entity.Email = formDto.Email;
                    Context.Subscribers.Update(entity);
                    Context.SaveChanges();
                    result.UpdateResultData(formDto).UpdateResultStatus(GenericOperationResult.Success);
                }
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                  .UpdateResultStatus(GenericOperationResult.Failed);
            }

            return result;
        }
        #endregion
        #region Get
        public OperationResult<GenericOperationResult, IEnumerable<SubscriberDto>> GetAllSubscribers(string query)
        {
            OperationResult<GenericOperationResult, IEnumerable<SubscriberDto>> result = new(GenericOperationResult.ValidationError);
            try
            {
                var entity = Context.Subscribers.Where(entity => entity.IsValid
                            && (!string.IsNullOrEmpty(query) ? entity.FirstName.Replace(" ", "").Contains(query.Replace
                                            (" ", "")) || entity.LastName.Replace(" ", "").Contains(query.Replace
                                            (" ", "")) || entity.Email.Replace(" ", "").Contains(query.Replace
                                            (" ", "")) : true))
                                .Select(entity => new SubscriberDto()
                                {
                                    Id = entity.Id,
                                    FirstName = entity.FirstName,
                                    LastName = entity.LastName,
                                    Email = entity.Email,
                                }).ToList();
                result.UpdateResultData(entity).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                  .UpdateResultStatus(GenericOperationResult.Failed);
            }

            return result;
        }

        public OperationResult<GenericOperationResult, SubscriberDto> GetSubscriber(int id)
        {
            OperationResult<GenericOperationResult, SubscriberDto> result = new(GenericOperationResult.ValidationError);
            try
            {
                var entity = Context.Subscribers.Where(entity => entity.IsValid && entity.Id == id)
                .Select(entity => new SubscriberDto()
                {
                    Id = entity.Id,
                    FirstName = entity.FirstName,
                    LastName = entity.LastName,
                    Email = entity.Email,
                }).SingleOrDefault();
                if (entity is null)
                {
                    return result.AddError(ErrorMessages.SubscriberNotFound)
                  .UpdateResultStatus(GenericOperationResult.NotFound);
                }
                result.UpdateResultData(entity).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                  .UpdateResultStatus(GenericOperationResult.Failed);
            }

            return result;
        }
        #endregion
        #region Remove
        public OperationResult<GenericOperationResult> RemoveSubscriber(int id)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.ValidationError);
            try
            {
                var entity = Context.Subscribers.Where(entity => entity.IsValid && entity.Id == id).SingleOrDefault();
                if (entity is not null)
                {
                    Context.Subscribers.Remove(entity);
                    Context.SaveChanges();
                    result.UpdateResultStatus(GenericOperationResult.Success);
                }
                else
                {
                    return result.AddError(ErrorMessages.SubscriberNotFound)
                 .UpdateResultStatus(GenericOperationResult.NotFound);
                }

            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                  .UpdateResultStatus(GenericOperationResult.Failed);
            }
            return result;
        }
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Note;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class NoteRepository : BasicRepository<CPDbContext, Note>, INoteRepository
    {
        #region Properties & Constructor
        public IUserRepository UserRepository { get; set; }
        public NoteRepository(CPDbContext context, IUserRepository UserRepository) : base(context)
        {
            this.UserRepository = UserRepository;
        }
        #endregion

        #region Action Section (Create & Update)
        public OperationResult<GenericOperationResult, NoteDto> Action(NoteDto noteDto)
        {
            OperationResult<GenericOperationResult,
                NoteDto> result = new(GenericOperationResult.ValidationError);

            if (string.IsNullOrEmpty(noteDto.Body))
                return result.AddError(ErrorMessages.NoteBodyRequired);

            //if (noteDto.UserId == 0)
            //    return result.AddError(ErrorMessages.MustChooseUserToAddNote);

            try
            {

                noteDto.Id = (noteDto.Id == 0 ? AddNote(noteDto) : UpdateNote(noteDto)).Id;

            }
            catch (Exception ex)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }

            return result.UpdateResultData(noteDto)
                .UpdateResultStatus(GenericOperationResult.Success);
        }
        private Note AddNote(NoteDto noteDto)
        {
            var noteEntity = new Note()
            {
                Body = noteDto.Body,
            };
            Context.Notes.Add(noteEntity);
            Context.SaveChanges();
            return noteEntity;
        }
        private Note UpdateNote(NoteDto noteDto)
        {
            var noteEntity = Context.Notes.Where(note => note.IsValid && note.Id == noteDto.Id).SingleOrDefault();
            noteEntity.Body = noteDto.Body;
            Context.SaveChanges();
            return noteEntity;
        }
        #endregion

        #region Get Section
        public OperationResult<GenericOperationResult, IEnumerable<NoteDto>> GetAllNotes()
        {
            OperationResult<GenericOperationResult,
                IEnumerable<NoteDto>> result = new(GenericOperationResult.ValidationError);

            try
            {
                var notes = Context.Notes
            .Where(note => note.IsValid)
            .Select(note =>
                new NoteDto()
                {
                    Id = note.Id,
                    Body = note.Body,
                }).ToList();

                return result.UpdateResultData(notes)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        public async Task<OperationResult<GenericOperationResult, IEnumerable<NoteDto>>> GetAllUserNotes(int? userId)
        {
            OperationResult<GenericOperationResult,
                IEnumerable<NoteDto>> result = new(GenericOperationResult.ValidationError);

            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var notes = Context.UserNotes
                        .Where(note => note.IsValid && (!userId.HasValue && userInfo.CurrentUserId == note.UserId
                               || ((userInfo.IsUserAdmin || userInfo.IsNormalUser) && note.UserId == userId)
                               || (userInfo.IsHealthCareProvider && (userId == note.CreatedBy && userId == note.UserId)
                               || (userInfo.CurrentUserId == note.CreatedBy && userId == note.UserId))))
                            .Select(note =>
                                new NoteDto()
                                {
                                    Id = note.Id,
                                    Body = note.Note.Body,
                                    UserId = note.UserId
                                }).ToList();

                return result.UpdateResultData(notes)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }


        }

        public OperationResult<GenericOperationResult, NoteDto> GetNote(int Id)
        {
            OperationResult<GenericOperationResult,
                NoteDto> result = new(GenericOperationResult.ValidationError);
            try
            {
                var note = Context.Notes
            .Where(note => note.IsValid && note.Id == Id)
            .Select(note => new NoteDto()
            {
                Id = note.Id,
                Body = note.Body,
            }).SingleOrDefault();

                if (note is null)
                {
                    return result.AddError(ErrorMessages.NoteNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                return result.UpdateResultData(note)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion

        #region Remove Section
        public OperationResult<GenericOperationResult> RemoveNote(int Id)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.ValidationError);
            try
            {
                var note = Context.Notes
                    .Where(note => note.IsValid && note.Id == Id)
                    .SingleOrDefault();

                if (note is null)
                {
                    return result.AddError(ErrorMessages.NoteNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                Context.SaveChanges();
                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public OperationResult<GenericOperationResult> RemoveUserNote(int Id)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.Failed);

            var note = Context.UserNotes
                .Where(note => note.IsValid && note.Id == Id)
                .SingleOrDefault();

            if (note != null)
            {
                Context.UserNotes.Remove(note);
            }

            Context.SaveChanges();

            return result.UpdateResultStatus(GenericOperationResult.Success);
        }
        #endregion

        public OperationResult<GenericOperationResult> ActionUserNote(IEnumerable<UserNoteDto> formDto, int UserId)
        {
            OperationResult<GenericOperationResult, UserNoteDto> result = new(GenericOperationResult.Failed);
            try
            {
                AddUserNote(formDto, UserId);

            }
            catch (Exception)
            {
                return result;
            }

            return result
                .UpdateResultStatus(GenericOperationResult.Success);
        }
        private void AddUserNote(IEnumerable<UserNoteDto> noteDto, int UserId)
        {
            var EntitiesToAdd = noteDto.Select(note => new UserNotes()
            {
                NoteId = note.NoteId,
                UserId = UserId
            }).ToList();
            var UserNotes = Context.UserNotes.Where(pnote => pnote.UserId == UserId && pnote.IsValid).ToList();
            var NotesToRemove = UserNotes.Where(pn => !noteDto.Any(nd => nd.NoteId == pn.NoteId)).ToList();
            var ExistingEntites = UserNotes.Where(pn => noteDto.Any(nd => nd.NoteId == pn.NoteId)).Select(ent => ent.NoteId).ToList();
            Context.RemoveRange(NotesToRemove);
            _ = Context.SaveChanges();
            EntitiesToAdd = EntitiesToAdd.Where(ead => !ExistingEntites.Contains(ead.NoteId)).ToList();
            Context.AddRange(EntitiesToAdd);
            _ = Context.SaveChanges();
        }

    }
}

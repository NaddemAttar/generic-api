﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CraftLab.Core.Infrastructure;
using CraftLab.Core.OperationResults;
using CraftLab.Core.Services.HttpFile;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Category;
using IGenericControlPanel.Core.Dto.Group;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.Dto;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums.File;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;

using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class GroupRepository : BasicRepository<CPDbContext, Group>, IGroupRepository
    {
        #region Properties & Constructor
        private IUserRepository UserRepository { get; }
        public GroupRepository(
            CPDbContext context,
            IUserRepository userRepository) : base(context)
        {
            UserRepository = userRepository;
        }
        #endregion

        #region Action Section
        public async Task<OperationResult<GenericOperationResult, GroupFormDto>> ActionAsync(GroupFormDto groupFormDto)
        {
            var result = groupFormDto.Id == 0
                ? await AddEntityAsync(groupFormDto)
                : await UpdateEntityAsync(groupFormDto);

            return result;
        }
        public async Task<OperationResult<GenericOperationResult, GroupMemberFormDto>> ActionGroupMemberAsync(
            GroupMemberFormDto groupMemberFormDto)
        {
            var result = await AddGroupMemberEntityAsync(groupMemberFormDto);

            return result;
        }
        public async Task<OperationResult<GenericOperationResult, GroupFormDto>> AddEntityAsync(GroupFormDto groupFormDto)
        {
            OperationResult<GenericOperationResult, GroupFormDto> result = new(GenericOperationResult.ValidationError);
            try
            {
                Group newGroup = new()
                {
                    Name = groupFormDto.Name,
                    Description = groupFormDto.Description,
                    GroupPrivacyLevel = groupFormDto.GroupPrivacyLevel,
                    MultiLevelCategoryId = groupFormDto.MultiLevelCategoryId
                };

                if (groupFormDto.GroupImage != null)
                {
                    GroupFile groupImg = new()
                    {
                        Checksum = groupFormDto.GroupImage.Checksum,
                        FileContentType = groupFormDto.GroupImage.FileContentType,
                        Description = groupFormDto.GroupImage.Description,
                        Extension = groupFormDto.GroupImage.Extension,
                        FileName = groupFormDto.GroupImage.FileName,
                        Name = groupFormDto.GroupImage.Name,
                        Url = groupFormDto.GroupImage.Url,
                        ImageType = ImageType.MainImage
                    };
                    newGroup.Images.Add(groupImg);
                }

                if (groupFormDto.CoverImage != null)
                {
                    GroupFile coverImg = new()
                    {
                        Checksum = groupFormDto.CoverImage.Checksum,
                        FileContentType = groupFormDto.CoverImage.FileContentType,
                        Description = groupFormDto.CoverImage.Description,
                        Extension = groupFormDto.CoverImage.Extension,
                        FileName = groupFormDto.CoverImage.FileName,
                        Name = groupFormDto.CoverImage.Name,
                        Url = groupFormDto.CoverImage.Url,
                        ImageType = ImageType.CoverImage
                    };
                    newGroup.Images.Add(coverImg);
                }

                _ = Context.Add(newGroup);
                _ = Context.SaveChanges();

                return result.UpdateResultData(groupFormDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError).UpdateResultStatus(GenericOperationResult.Failed);
            }


        }
        public async Task<OperationResult<GenericOperationResult, GroupFormDto>> UpdateEntityAsync(
            GroupFormDto groupFormDto)
        {
            OperationResult<GenericOperationResult,
                GroupFormDto> result = new(GenericOperationResult.ValidationError);
            try
            {
                if (groupFormDto.RemoveFileIds is not null)
                {
                    groupFormDto.RemoveFileUrls = Context.Files
                        .Where(file => groupFormDto
                        .RemoveFileIds
                        .Contains(file.Id))
                        .Select(file => file.Url).ToList();

                    Context.Files.RemoveRange(Context.Files
                        .Where(file => groupFormDto.RemoveFileIds.Contains(file.Id))
                        .ToList());
                }

                Group editGroup = Context.Groups
                    .Where(group => group.Id == groupFormDto.Id)
                    .Include(fileEntity => fileEntity.Images)
                    .SingleOrDefault();

                if (editGroup is not null)
                {
                    editGroup.Name = groupFormDto.Name;
                    editGroup.Description = groupFormDto.Description;
                    editGroup.GroupPrivacyLevel = groupFormDto.GroupPrivacyLevel;
                    editGroup.MultiLevelCategoryId = groupFormDto.MultiLevelCategoryId;
                    editGroup.CreatedBy = editGroup.CreatedBy;
                    var UrlsToDelete = new List<string>();
                    UrlsToDelete.AddRange(groupFormDto.RemoveFileUrls);
                    if (groupFormDto.GroupImage != null)
                    {
                        GroupFile groupImg = new()
                        {
                            Checksum = groupFormDto.GroupImage.Checksum,
                            FileContentType = groupFormDto.GroupImage.FileContentType,
                            Description = groupFormDto.GroupImage.Description,
                            Extension = groupFormDto.GroupImage.Extension,
                            FileName = groupFormDto.GroupImage.FileName,
                            Name = groupFormDto.GroupImage.Name,
                            Url = groupFormDto.GroupImage.Url,
                            ImageType = ImageType.MainImage
                        };
                        var OldGroupImage = editGroup.Images
                            .Where(image => image.IsValid &&
                            image.ImageType == ImageType.MainImage)
                            .SingleOrDefault();
                        if (OldGroupImage != null)
                        {
                            Context.Remove(OldGroupImage);
                            UrlsToDelete.Add(OldGroupImage.Url);
                        }
                        editGroup.Images.Add(groupImg);
                    }

                    if (groupFormDto.CoverImage != null)
                    {
                        GroupFile coverImg = new()
                        {
                            Checksum = groupFormDto.CoverImage.Checksum,
                            FileContentType = groupFormDto.CoverImage.FileContentType,
                            Description = groupFormDto.CoverImage.Description,
                            Extension = groupFormDto.CoverImage.Extension,
                            FileName = groupFormDto.CoverImage.FileName,
                            Name = groupFormDto.CoverImage.Name,
                            Url = groupFormDto.CoverImage.Url,
                            ImageType = ImageType.CoverImage
                        };
                        var OldGroupCoverImage = editGroup.Images.Where(image => image.IsValid && image.ImageType == ImageType.CoverImage).SingleOrDefault();
                        if (OldGroupCoverImage != null)
                        {
                            Context.Remove(OldGroupCoverImage);
                            UrlsToDelete.Add(OldGroupCoverImage.Url);
                        }
                        editGroup.Images.Add(coverImg);
                    }
                    groupFormDto.RemoveFileUrls = UrlsToDelete;
                    Context.Update(editGroup);
                    await Context.SaveChangesAsync();

                    return result.UpdateResultStatus(GenericOperationResult.Success)
                        .UpdateResultData(groupFormDto);
                }
                else
                {
                    return result.UpdateResultStatus(GenericOperationResult.NotFound);
                }
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed)
                    .AddError(ErrorMessages.InternalServerError);
            }



        }

        public async Task<OperationResult<GenericOperationResult, GroupMemberFormDto>> AddGroupMemberEntityAsync(
            GroupMemberFormDto groupMemberFormDto)
        {
            OperationResult<GenericOperationResult,
                GroupMemberFormDto> result = new(GenericOperationResult.ValidationError);
            try
            {
                GroupMember newGroupMember = new()
                {
                    GroupId = groupMemberFormDto.GroupId,
                    UserId = groupMemberFormDto.UserId
                };


                await Context.AddAsync(newGroupMember);
                await Context.SaveChangesAsync();

                groupMemberFormDto.Id = newGroupMember.Id;

                return result.UpdateResultData(groupMemberFormDto)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed)
                    .AddError(ErrorMessages.InternalServerError);
            }

        }
        #endregion

        #region Get Section
        public async Task<OperationResult<GenericOperationResult, GroupDetailsDto>> GetDetailedAsync(
            int id, string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult,
                GroupDetailsDto> result = new(GenericOperationResult.Success);

            try
            {
                var data = ValidEntities
                     .Where(groupEntity => groupEntity.Id == id)
                     .Include(groupEntity => groupEntity.Images)
                     .Include(groupEntity => groupEntity.MultiLevelCategory)
                     .ToList()
                     .Select(groupEntity => new GroupDetailsDto()
                     {
                         Id = groupEntity.Id,
                         Name = groupEntity.Name,
                         Description = groupEntity.Description,
                         GroupPrivacyLevel = groupEntity.GroupPrivacyLevel,
                         GroupImage = groupEntity.Images.Where(groupImage => groupImage.IsValid && groupImage.FileContentType == FileContentType.Image)
                         .Select(groupImage => new FileDetailsDto()
                         {
                             Id = groupImage.Id,
                             Extension = groupImage.Extension,
                             FileName = groupImage.FileName,
                             FileContentType = groupImage.FileContentType,
                             Url = groupImage.Url,

                             SourceName = groupImage.SourceName,
                             Checksum = groupImage.Checksum,
                             Description = groupImage.Description,
                             Name = groupImage.Name,
                         }).SingleOrDefault(),
                         Category = groupEntity.MultiLevelCategory != null ? new MultiLevelCategoryDto()
                         {
                             Id = groupEntity.MultiLevelCategoryId.Value,
                             Name = groupEntity.MultiLevelCategory.Name
                         } : null,
                         CoverImage = groupEntity.Images
                         .Where(coverImage => coverImage.IsValid
                             && coverImage.FileContentType == FileContentType.Attachement)
                         .Select(coverImage => new FileDetailsDto()
                         {
                             Id = coverImage.Id,
                             Extension = coverImage.Extension,
                             FileName = coverImage.FileName,
                             FileContentType = coverImage.FileContentType,
                             Url = coverImage.Url,
                             SourceName = coverImage.SourceName,
                             Checksum = coverImage.Checksum,
                             Description = coverImage.Description,
                             Name = coverImage.Name,
                         }).SingleOrDefault()
                     }).SingleOrDefault();
                if (data is null)
                    return await Task.FromResult(result.UpdateResultStatus(GenericOperationResult.NotFound));
                else return await Task.FromResult(result.UpdateResultStatus(GenericOperationResult.Success)
                    .UpdateResultData(data));
            }
            catch (Exception)
            {
                return await Task.FromResult(result.UpdateResultStatus(GenericOperationResult.Failed)
                    .AddError(ErrorMessages.InternalServerError));
            }

        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<GroupDto>>> GetAllPagedAsync(int? pageNumber = 0, int? pageSize = null,
            string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult,
                IEnumerable<GroupDto>> result = new(GenericOperationResult.ValidationError);

            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var data = ValidEntities
                   .OrderByDescending(group => group.CreationDate)

                   .Include(groupEntity => groupEntity.Images)
                   .Include(groupEntity => groupEntity.GroupMembers)
                   .Include(groupEntity => groupEntity.Blogs)
                   .Include(groupEntity => groupEntity.Question)
                   .ToList()
                   .Where(entity => userInfo.CurrentUserId == 0
                   || userInfo.IsUserAdmin
                   || (userInfo.IsHealthCareProvider
                   ? userInfo.CurrentUserId == entity.CreatedBy
                   : true))
                   .Skip(pageNumber.Value * pageSize.Value)
                   .Take(pageSize.Value)
                   .Select(groupEntity => new GroupDto()
                   {
                       Id = groupEntity.Id,
                       Name = groupEntity.Name,
                       Description = groupEntity.Description,
                       GroupPrivacyLevel = groupEntity.GroupPrivacyLevel,
                       MemberCount = groupEntity.GroupMembers.Where(gm => gm.IsValid).Count(),
                       BlogCount = groupEntity.Blogs.Where(gm => gm.IsValid).Count(),
                       QuestionCount = groupEntity.Question.Where(gm => gm.IsValid).Count(),
                       GroupImage = groupEntity.Images.Where(groupImage => groupImage.IsValid &&
                       groupEntity.Id == groupImage.GroupId &&
                       groupImage.FileContentType == FileContentType.Image)
                       .Where(groupImage => groupImage.IsValid && groupImage.ImageType == ImageType.MainImage)
                           .Select(groupImage => new FileDetailsDto()
                           {
                               Id = groupImage.Id,
                               Extension = groupImage.Extension,
                               FileName = groupImage.FileName,
                               FileContentType = groupImage.FileContentType,
                               Url = groupImage.Url,
                               SourceName = groupImage.SourceName,
                               Checksum = groupImage.Checksum,
                               Description = groupImage.Description,
                               Name = groupImage.Name,

                           }).SingleOrDefault(),
                       CoverImage = groupEntity.Images.Where(coverImage => coverImage.IsValid &&
                       groupEntity.Id == coverImage.GroupId &&
                       coverImage.FileContentType == FileContentType.Attachement
                       && coverImage.IsValid
                       && coverImage.ImageType == ImageType.CoverImage)
                           .Select(coverImage => new FileDetailsDto()
                           {
                               Id = coverImage.Id,
                               Extension = coverImage.Extension,
                               FileName = coverImage.FileName,
                               FileContentType = coverImage.FileContentType,
                               Url = coverImage.Url,
                               SourceName = coverImage.SourceName,
                               Checksum = coverImage.Checksum,
                               Description = coverImage.Description,
                               Name = coverImage.Name,
                           }).SingleOrDefault()
                   }).ToList();
                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(data);

            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Remove Section
        public async Task<OperationResult<GenericOperationResult>> RemoveAsync(int id)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.Success);

            try
            {
                Group data = Context.Groups.Where(group => group.Id == id).SingleOrDefault();

                if (result is null)
                {
                    return result.UpdateResultStatus(GenericOperationResult.NotFound);
                }

                data.IsValid = false;
                await Context.SaveChangesAsync();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed)
                    .AddError(ErrorMessages.InternalServerError);
            }

        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<GroupMemberDto>>> GetAllGroupMembersAsync(
            int GroupId, int? pageNumber = 0, int? pageSize = null, bool enablePagination = true)
        {
            //TODO Nadim - add new pagination method here 
            var result = new OperationResult<GenericOperationResult,
                IEnumerable<GroupMemberDto>>(GenericOperationResult.ValidationError);
            try
            {
                var data = Context.GroupMembers
                    .Where(gm => gm.GroupId == GroupId && gm.IsValid)
                    .Select(gm => new GroupMemberDto()
                    {
                        Id = gm.Id,
                        Group = new GroupDto()
                        {
                            Name = gm.Group.Name,
                            Id = gm.GroupId
                        },
                        User = new UserDto()
                        {
                            Id = gm.UserId
                        }
                    });
                return await Task.FromResult(result.UpdateResultStatus(GenericOperationResult.Failed)
                    .AddError(ErrorMessages.InternalServerError));
            }
            catch (Exception ex)
            {
                return await Task.FromResult(result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed));
            }
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<GroupMemberDto>>> GetUserGroupsAsync(
            int UserId, int? pageNumber = 0, int? pageSize = null, bool enablePagination = true)
        {
            //TODO Nadim - add new pagination method here 
            var result = new OperationResult<GenericOperationResult,
                IEnumerable<GroupMemberDto>>(GenericOperationResult.ValidationError);
            try
            {
                var data = Context.GroupMembers
                    .Where(gm => gm.UserId == UserId && gm.IsValid)
                    .Select(gm => new GroupMemberDto()
                    {
                        Id = gm.Id,
                        Group = new GroupDto()
                        {
                            Name = gm.Group.Name,
                            Id = gm.GroupId
                        },
                        User = new UserDto()
                        {
                            Id = gm.UserId
                        }
                    });
                return await Task.FromResult(result.UpdateResultData(data)
                    .UpdateResultStatus(GenericOperationResult.Success));
            }
            catch (Exception ex)
            {
                return await Task.FromResult(result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed));
            }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.Core.Dto.Mail;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Repository;

using MailKit.Security;

using Microsoft.Extensions.Options;


using MimeKit;


namespace IGenericControlPanel.Core.Data.Repositories
{
    public class MailRepository : BasicRepository<CPDbContext, Question>, IMailRepository
    {
        private readonly MailSettings _mailSettings;

        public MailRepository(CPDbContext context, IOptions<MailSettings> mailSettings) : base(context)
        {
            _mailSettings = mailSettings.Value;
        }

        public async Task SendEmailAsync(MailRequest mailRequest)
        {
            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(_mailSettings.Username);
          
            foreach (string emailTo in mailRequest.ToEmails)
            {
                email.To.Add(MailboxAddress.Parse(emailTo));
            }
            email.Subject = mailRequest.Subject;
            var builder = new BodyBuilder();
            if (mailRequest.Attachments != null)
            {
                byte[] fileBytes;
                foreach (var file in mailRequest.Attachments)
                {
                    if (file.Length > 0)
                    {
                        using (var ms = new MemoryStream())
                        {
                            file.CopyTo(ms);
                            fileBytes = ms.ToArray();
                        }
                        builder.Attachments.Add(file.FileName, fileBytes, ContentType.Parse(file.ContentType));
                    }
                }
            }
            builder.HtmlBody = mailRequest.Body;
            email.Body = builder.ToMessageBody();
            using var smtp = new MailKit.Net.Smtp.SmtpClient();
            
            smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls);
            
            smtp.Authenticate(_mailSettings.Username, _mailSettings.Password);
            await smtp.SendAsync(email);
            smtp.Disconnect(true);
        }

        public async Task SendEmailSubscriptions(MailSubscriptionsDto formDto)
        {
            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(_mailSettings.Username);
            List<string> emails = Context.Subscribers.Where(entity => entity.IsValid).Select(entity => entity.Email).ToList();
            foreach (string emailTo in emails)
            {
                email.To.Add(MailboxAddress.Parse(emailTo));
            }
            email.Subject = formDto.Subject;
            var builder = new BodyBuilder();
   
            builder.HtmlBody = formDto.Body;
            email.Body = builder.ToMessageBody();
            using var smtp = new MailKit.Net.Smtp.SmtpClient();

            smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls);

            smtp.Authenticate(_mailSettings.Username, _mailSettings.Password);
            await smtp.SendAsync(email);
            smtp.Disconnect(true);
        }
        public async Task SendEmailSubscriptionsByIds(MailUsersDto formDto)
        {
            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(_mailSettings.Username);
            
            foreach (int id in formDto.UsersIds)
            {
                string userEmail = Context.Users.Where(entity => entity.IsValid && entity.Id==id).Select(entity => entity.Email).SingleOrDefault();
                email.To.Add(MailboxAddress.Parse(userEmail));
            }
            email.Subject = formDto.Subject;
            var builder = new BodyBuilder();

            builder.HtmlBody = formDto.Body;
            email.Body = builder.ToMessageBody();
            using var smtp = new MailKit.Net.Smtp.SmtpClient();

            smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls);

            smtp.Authenticate(_mailSettings.Username, _mailSettings.Password);
            await smtp.SendAsync(email);
            smtp.Disconnect(true);
        }
    }
}

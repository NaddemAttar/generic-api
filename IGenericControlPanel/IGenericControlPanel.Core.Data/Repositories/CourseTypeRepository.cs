﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Core.Dto.CourseType;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class CourseTypeRepository : BasicRepository<CPDbContext, CourseType>, ICourseTypeRepository
    {
        public CourseTypeRepository(CPDbContext context) : base(context)
        { }

        public async Task<OperationResult<GenericOperationResult, CourseTypeDto>> ActionAsync(CourseTypeDto courseTypeDto)
        {
            OperationResult<GenericOperationResult, CourseTypeDto> result = new(GenericOperationResult.Failed);

            result = courseTypeDto.Id == 0
                ? await CreateNewCourseTypeAsync(courseTypeDto)
                : await UpdateCourseTypeAsync(courseTypeDto);

            return result;
        }

        private async Task<OperationResult<GenericOperationResult, CourseTypeDto>> CreateNewCourseTypeAsync(CourseTypeDto courseTypeDto)
        {
            OperationResult<GenericOperationResult, CourseTypeDto> result = new(GenericOperationResult.Failed);
            try
            {
                CourseType courseTypeEntity = new()
                {
                    Name = courseTypeDto.Name
                };

                await Context.AddAsync(courseTypeEntity);
                await Context.SaveChangesAsync();

                return result.UpdateResultData(courseTypeDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        private async Task<OperationResult<GenericOperationResult, CourseTypeDto>> UpdateCourseTypeAsync(
            CourseTypeDto courseTypeDto)
        {
            OperationResult<GenericOperationResult, CourseTypeDto> result = new(GenericOperationResult.Failed);
            try
            {
                CourseType courseTypeEntity = await Context.CourseTypes
                    .Where(courseType => courseType.Id == courseTypeDto.Id)
                    .SingleOrDefaultAsync();

                if (courseTypeEntity is not null)
                {
                    courseTypeEntity.Name = courseTypeDto.Name;
                    Context.Update(courseTypeEntity);
                    await Context.SaveChangesAsync();

                    return result.UpdateResultData(courseTypeDto).UpdateResultStatus(GenericOperationResult.Success);
                }
                else
                    return result.AddError(ErrorMessages.CourseTypeNotFound).UpdateResultStatus(GenericOperationResult.NotFound);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, IEnumerable<CourseTypeDto>>> GetAllAsync(string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, IEnumerable<CourseTypeDto>> result = new(GenericOperationResult.Failed);
            try
            {
                var courseTypes = await ValidEntities
                    .Select(courseType => new CourseTypeDto
                    {
                        Id = courseType.Id,
                        Name = courseType.Name
                    })
                    .ToListAsync();

                return result.UpdateResultData(courseTypes).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult>> RemoveAsync(int id)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.Failed);
            try
            {
                var entity = await Context.CourseTypes
                    .Where(entity => entity.IsValid && entity.Id == id)
                    .SingleOrDefaultAsync();

                if (entity is null)
                    return result.AddError(ErrorMessages.CourseTypeNotFound).UpdateResultStatus(GenericOperationResult.NotFound);

                Context.CourseTypes.Remove(entity);

                await Context.SaveChangesAsync();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
    }
}
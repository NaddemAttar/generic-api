﻿using System;
using System.Collections.Generic;
using System.Linq;

using CraftLab.Core.Database.Identity.Interfaces;
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Core.Dto.Order;
using IGenericControlPanel.Core.IData;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Enums.Core;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using IGenericControlPanel.SharedKernal.Utils;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class OrderRepository : BasicRepository<CPDbContext, Order>, IOrderRepository
    {
        private readonly IUserService UserService;
        private IGamificationMovementsRepository _gamificationMovementRepo { get; }
        public OrderRepository(
            CPDbContext Context,
            IUserService _userService,
            IGamificationMovementsRepository gamificationMovementRepo) : base(Context)
        {
            UserService = _userService;
            _gamificationMovementRepo = gamificationMovementRepo;
        }
        public async Task<OperationResult<GenericOperationResult, OrderFormDto>> ActionOrder(OrderFormDto formDto)
        {
            var result = new OperationResult<GenericOperationResult, OrderFormDto>(GenericOperationResult.ValidationError);
            try
            {
                var userId = UserService.GetCurrentUserId<int>();
                var OrderEntity = ValidEntities.Where(order => order.Id == formDto.Id).SingleOrDefault();
                var ProductsIds = formDto.ProductsIdsAndQuantities.Select(pq => pq.Item1).ToList();
                var productsToAdd = Context.ProductSizesColors.Where(prod => ProductsIds.Contains(prod.Id)).ToList();
                if (OrderEntity != null)
                {
                    var OrderProductsToRemove = Context.OrderProducts.Where(prod => prod.IsValid && !ProductsIds.Contains(prod.Id) && prod.OrderId == formDto.Id).ToList();
                    foreach (var item in OrderProductsToRemove)
                    {
                        item.ForceDelete = true;
                        item.Item.Quantity += item.Quantity;
                    }
                    Context.RemoveRange(OrderProductsToRemove);
                    var orderProducts = Context.OrderProducts.Where(orderpro => orderpro.IsValid && orderpro.OrderId == formDto.Id).ToList();
                    foreach (var item in formDto.ProductsIdsAndQuantities)
                    {
                        if (orderProducts.Any(order => order.ItemId == item.Item1))
                        {
                            continue;
                        }
                        orderProducts.Add(new OrderProducts()
                        {
                            OrderId = formDto.Id,
                            ItemId = item.Item1,
                            Quantity = item.Item2
                        });
                    }
                    OrderEntity.OrderProducts = orderProducts;
                    await Context.SaveChangesAsync();

                    formDto.Id = OrderEntity.Id;
                }
                else
                {
                    OrderEntity = new Order()
                    {
                        UserId = userId,
                        OrderProducts = formDto.ProductsIdsAndQuantities.Select(prqnt => new OrderProducts()
                        {
                            ItemId = prqnt.Item1,
                            Quantity = prqnt.Item2
                        }).ToList()
                    };
                    Context.Add(OrderEntity);
                    foreach (var orderProduct in OrderEntity.OrderProducts)
                    {
                        orderProduct.Item.Quantity -= orderProduct.Quantity;
                    }

                    await Context.SaveChangesAsync();

                    var savePointsResult = await _gamificationMovementRepo.SavePoints(new List<string>
                    {
                        GamificationNames.AddOrder
                    });

                    if (!savePointsResult.IsSuccess)
                    {
                        return result.AddError(savePointsResult.ErrorMessages)
                            .UpdateResultStatus(GenericOperationResult.Failed);
                    }

                    formDto.Id = OrderEntity.Id;

                }
                return result.UpdateResultData(formDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed)
                    .AddError(ErrorMessages.InternalServerError);
            }
        }

        public OperationResult<GenericOperationResult> ChangeOrderStatus(int orderId, PaymentStatus paymentStatus)
        {
            var result = new OperationResult<GenericOperationResult>(GenericOperationResult.ValidationError);
            try
            {
                var data = ValidEntities.Where(order => order.Id == orderId).SingleOrDefault();
                if (data == null)
                {
                    return result.UpdateResultStatus(GenericOperationResult.NotFound);
                }
                data.PaymentStatus = paymentStatus;
                Context.SaveChanges();
                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }

        public OperationResult<GenericOperationResult, OrderDto> GetOrder(int id)
        {
            var result = new OperationResult<GenericOperationResult, OrderDto>(GenericOperationResult.ValidationError);
            try
            {
                var data = ValidEntities.Where(order => order.Id == id).Select(order => new OrderDto()
                {
                    Id = order.Id,
                    Products = order.OrderProducts.Select(orderPr => new OrderProductDetailsDto()
                    {
                        Id = orderPr.Item.Id,
                        Name = orderPr.Item.Product.Name,
                        CategoryId = orderPr.Item.Product.CategoryId,
                        CategoryName = orderPr.Item.Product.Category.Name,
                    }).ToList(),
                    UserId = order.UserId ?? 0
                }).SingleOrDefault();
                return data == null
                    ? result.UpdateResultStatus(GenericOperationResult.NotFound)
                    : result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }

        public OperationResult<GenericOperationResult, IEnumerable<OrderDto>> GetOrders(int pageNumber = 0,
            int pageSize = 8, int userId = 0)
        {
            var result = new OperationResult<GenericOperationResult, IEnumerable<OrderDto>>(GenericOperationResult.ValidationError);
            try
            {
                var data = ValidEntities
                    .Where(entity => entity.IsValid && (userId == 0 || entity.UserId == userId))
                    .Skip(pageNumber * pageSize)
                    .Take(pageSize)
                    .Select(order => new OrderDto()
                    {
                        Id = order.Id,
                        UserId = order.UserId ?? 0,
                        PersonName = order.User.UserName,
                        OrderDate = order.CreationDate,
                        ProductsCount = order.OrderProducts.Count,
                        TotalPrice = order.OrderProducts
                         .Sum(orderPr => orderPr.Item.Price * orderPr.Quantity),
                        Products = order.OrderProducts.Select(orderPr => new OrderProductDetailsDto()
                        {
                            Id = orderPr.Item.Id,
                            Name = orderPr.Item.Product.Name,
                            CategoryId = orderPr.Item.Product.CategoryId,
                            CategoryName = orderPr.Item.Product.Category.Name,
                            TotalPrice = orderPr.Item.Price * orderPr.Quantity
                        }).ToList(),
                    }).ToList();
                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }
        public OperationResult<GenericOperationResult> RemoveOrder(int id)
        {
            var result = new OperationResult<GenericOperationResult>(GenericOperationResult.ValidationError);
            try
            {
                var data = ValidEntities.Where(order => order.Id == id).SingleOrDefault();
                if (data == null)
                {
                    return result.UpdateResultStatus(GenericOperationResult.NotFound);
                }
                else
                {
                    foreach (var orderProduct in data.OrderProducts)
                    {
                        orderProduct.Item.Quantity += orderProduct.Quantity;
                    }
                    Context.Remove(data);
                    Context.SaveChangesAsync();
                }
                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
    }
}

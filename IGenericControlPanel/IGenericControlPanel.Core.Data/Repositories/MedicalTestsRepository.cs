﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Extension;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;

using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class MedicalTestsRepository : BasicRepository<CPDbContext, MedicalTests>, IMedicalTestsRepository
    {
        #region Properties & Constructor
        private IUserRepository UserRepository { get; }
        public MedicalTestsRepository(
            CPDbContext context,
            IUserRepository userRepository) : base(context)
        {
            UserRepository = userRepository;
        }
        #endregion

        #region Medical Test Action 
        public OperationResult<GenericOperationResult, MedicalTestsDto> Action(MedicalTestsDto medicalTestsDto)
        {
            OperationResult<GenericOperationResult,
                MedicalTestsDto> result = new(GenericOperationResult.ValidationError);

            if (string.IsNullOrEmpty(medicalTestsDto.Name))
                return result.AddError(ErrorMessages.MedicalTestNameRequired);

            try
            {
                medicalTestsDto.UserId = medicalTestsDto.UserId != 0 ?
                   medicalTestsDto.UserId :
                   UserRepository.CurrentUserIdentityId();

                medicalTestsDto = medicalTestsDto.Id == 0 ?
                    AddMedicalTest(medicalTestsDto) :
                    UpdateMedicalTest(medicalTestsDto);

                if (medicalTestsDto is null)
                {
                    return result.AddError(ErrorMessages.MedicalTestNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                return result.UpdateResultData(medicalTestsDto)
                 .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        #region Add Medical Test
        public MedicalTestsDto AddMedicalTest(MedicalTestsDto medicalTestsDto)
        {
            MedicalTests entity = new()
            {
                Name = medicalTestsDto.Name,
                Description = medicalTestsDto.Description,
                UserId = medicalTestsDto.UserId,
                MedicalTestType = medicalTestsDto.MedicalTestType
            };
            if (medicalTestsDto.Images != null)
            {
                entity = AddingMedicalTestFiles(medicalTestsDto.Images, entity);
            }

            Context.MedicalTests.Add(entity);
            Context.SaveChanges();
            if (medicalTestsDto.Images is not null)
            {
                foreach (FileDetailsDto image in medicalTestsDto.Images)
                {
                    image.SourceId = entity.Id;
                }
            }

            return medicalTestsDto;
        }
        public MedicalTests AddingMedicalTestFiles(
            IEnumerable<FileDetailsDto> images, MedicalTests entity)
        {
            foreach (FileDetailsDto image in images)
            {
                entity.Files.Add(new MedicalTestFiles()
                {
                    Checksum = image.Checksum,
                    FileContentType = image.FileContentType,
                    Description = image.Description,
                    Extension = image.Extension,
                    FileName = image.FileName,
                    Name = image.Name,
                    Url = image.Url,
                });
            }

            return entity;
        }
        #endregion

        #region Update Medical Test
        public MedicalTestsDto UpdateMedicalTest(MedicalTestsDto medicalTestsDto)
        {
            var entity = Context.MedicalTests
                .Where(entity => entity.IsValid && entity.Id == medicalTestsDto.Id)
                .SingleOrDefault();

            if (entity is null) return null;

            entity.Id = medicalTestsDto.Id;
            entity.Name = medicalTestsDto.Name;
            entity.Description = medicalTestsDto.Description;
            entity.MedicalTestType = medicalTestsDto.MedicalTestType;
            if (medicalTestsDto.RemovedFilesIds.Count() > 0)
            {
                medicalTestsDto = RemoveMedicalTestFiles(medicalTestsDto);
            }

            Context.Entry(entity).State = EntityState.Detached;

            if (medicalTestsDto.Images.Count != 0)
            {
                entity = AddingMedicalTestFiles(medicalTestsDto.Images, entity);

            }
            Context.MedicalTests.Update(entity);
            Context.SaveChanges();

            return medicalTestsDto;
        }
        public MedicalTestsDto RemoveMedicalTestFiles(MedicalTestsDto medicalTestsDto)
        {
            var removedFiles = Context.Files
                .Where(file => medicalTestsDto.RemovedFilesIds.Contains(file.Id))
                .ToList();

            Context.RemoveRange(removedFiles);
            medicalTestsDto.RemovedFilesUrls = removedFiles
                .Select(file => file.Url)
                .ToList();

            return medicalTestsDto;
        }
        #endregion

        #endregion

        #region Get
        public async Task<OperationResult<GenericOperationResult, IEnumerable<MedicalTestsDto>>> GetAllUserMedicalTests(
            int? userId, bool enablePagination, int pageSize, int pageNumber)
        {
            OperationResult<GenericOperationResult,
                IEnumerable<MedicalTestsDto>> result = new(GenericOperationResult.ValidationError);

            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var entity = Context.MedicalTests
                    .Include(entity => entity.Files)
                    .Where(entity => entity.IsValid && (!userId.HasValue && userInfo.CurrentUserId == entity.UserId
                           || ((userInfo.IsUserAdmin || userInfo.IsNormalUser) && entity.UserId == userId)
                           || (userInfo.IsHealthCareProvider && (userId == entity.CreatedBy && userId == entity.UserId)
                           || (userInfo.CurrentUserId == entity.CreatedBy && userId == entity.UserId))));

                if (enablePagination)
                {
                    entity = entity.GetDataWithPagination(pageSize, pageNumber);
                }

                var resultData = entity.Select(entity => new MedicalTestsDto()
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Description = entity.Description,
                    MedicalTestType = entity.MedicalTestType,
                    Images = entity.Files
                     .Where(file => file.IsValid)
                     .Select(file => new FileDetailsDto()
                     {
                         Id = file.Id,
                         Checksum = file.Checksum,
                         FileContentType = file.FileContentType,
                         Description = file.Description,
                         FileName = file.FileName,
                         Name = file.Name,
                         Url = file.Url
                     }).ToList()
                }).ToList();

                return result.UpdateResultData(resultData)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public OperationResult<GenericOperationResult, IEnumerable<MedicalTestsDto>> GetAllMedicalTests()
        {
            OperationResult<GenericOperationResult,
                IEnumerable<MedicalTestsDto>> result = new(GenericOperationResult.ValidationError);

            try
            {
                var resultData = Context.MedicalTests
             .Include(entity => entity.Files)
             .Where(entity => entity.IsValid)
             .Select(entity => new MedicalTestsDto()
             {
                 Id = entity.Id,
                 Name = entity.Name,
                 Description = entity.Description,
                 MedicalTestType = entity.MedicalTestType,
                 Images = entity.Files
                 .Where(file => file.IsValid)
                 .Select(file => new FileDetailsDto()
                 {
                     Id = file.Id,
                     Checksum = file.Checksum,
                     FileContentType = file.FileContentType,
                     Description = file.Description,
                     FileName = file.FileName,
                     Name = file.Name,
                     Url = file.Url
                 }).ToList()
             }).ToList();

                return result.UpdateResultData(resultData)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public OperationResult<GenericOperationResult, MedicalTestsDto> GetMedicalTest(int Id)
        {
            OperationResult<GenericOperationResult,
                MedicalTestsDto> result = new(GenericOperationResult.ValidationError);

            try
            {
                var resultData = Context.MedicalTests
               .Include(entity => entity.Files)
               .Where(entity => entity.IsValid && entity.Id == Id)
               .Select(entity => new MedicalTestsDto()
               {
                   Id = entity.Id,
                   Name = entity.Name,
                   Description = entity.Description,
                   MedicalTestType = entity.MedicalTestType,
                   Images = entity.Files
                   .Where(file => file.IsValid)
                   .Select(file => new FileDetailsDto()
                   {
                       Id = file.Id,
                       Checksum = file.Checksum,
                       FileContentType = file.FileContentType,
                       Description = file.Description,
                       FileName = file.FileName,
                       Name = file.Name,
                       Url = file.Url
                   }).ToList()
               }).SingleOrDefault();

                if (resultData is null)
                {
                    return result.AddError(ErrorMessages.MedicalTestNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                return result.UpdateResultData(resultData)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion

        #region Remove
        public async Task<OperationResult<GenericOperationResult>> RemoveMedicalTest(int Id)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.ValidationError);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var entity = Context.MedicalTests
                    .Where(entity => entity.IsValid
                    && (userInfo.IsUserAdmin || userInfo.CurrentUserId == entity.CreatedBy)
                    && entity.Id == Id)
                    .SingleOrDefault();

                if (entity is null)
                {
                    return result.AddError(ErrorMessages.MedicalTestNotExist)
                        .UpdateResultStatus(GenericOperationResult.NotFound);
                }

                Context.MedicalTests.Remove(entity);
                Context.SaveChanges();
                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }

        }
        #endregion
    }
}

﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Allegry;
using IGenericControlPanel.Core.Dto.Medicine;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Extension;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class AllergyRepository : BasicRepository<CPDbContext, Allergy>, IAllergyRepository
    {
        #region Properties & Constructor
        private readonly IUserRepository UserRepository;
        public AllergyRepository(
            CPDbContext context,
            IMapper mapper,
            IUserRepository UserRepository) : base(context, mapper)
        {
            this.UserRepository = UserRepository;
        }
        #endregion

        #region Allergy Action
        public async Task<OperationResult<GenericOperationResult, AllergyFormDto>> Action(AllergyFormDto allergyFormDto)
        {
            OperationResult<GenericOperationResult,
                AllergyFormDto> result = new(GenericOperationResult.ValidationError);

            if (string.IsNullOrEmpty(allergyFormDto.Name))
                return result.AddError(ErrorMessages.AllergyNameRequired);

            if (string.IsNullOrEmpty(allergyFormDto.Description))
                return result.AddError(ErrorMessages.AllergyDescriptionRequired);

            try
            {
                allergyFormDto = allergyFormDto.Id == 0 ?
                   await AddAllergy(allergyFormDto) :
                   await UpdateAllergy(allergyFormDto);

                if (allergyFormDto is null)
                    return result.AddError(ErrorMessages.AllergyNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                result.UpdateResultData(allergyFormDto).UpdateResultStatus(GenericOperationResult.Success);
                return result;
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError).UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        #region Add Allergy
        public async Task<AllergyFormDto> AddAllergy(AllergyFormDto allergyFormDto)
        {
            Allergy entity = new()
            {
                Name = allergyFormDto.Name,
                AllergyType = allergyFormDto.AllergyType,
                Description = allergyFormDto.Description,
            };

            if (allergyFormDto.Images != null)
                entity = AddingAllergyFiles(allergyFormDto.Images, entity);

            await Context.Allegries.AddAsync(entity);

            if (allergyFormDto.Images is not null)
                foreach (FileDetailsDto image in allergyFormDto.Images)
                    image.SourceId = entity.Id;

            await Context.SaveChangesAsync();
            allergyFormDto.Id = entity.Id;
            return allergyFormDto;
        }
        public Allergy AddingAllergyFiles(IEnumerable<FileDetailsDto> images, Allergy entity)
        {
            foreach (FileDetailsDto image in images)
            {
                entity.Files.Add(new AllergyFile()
                {
                    Checksum = image.Checksum,
                    FileContentType = image.FileContentType,
                    Description = image.Description,
                    Extension = image.Extension,
                    FileName = image.FileName,
                    Name = image.Name,
                    Url = image.Url,
                });
            }
            return entity;
        }
        #endregion

        #region Update Allergy
        public async Task<AllergyFormDto> UpdateAllergy(AllergyFormDto allergyFormDto)
        {
            var entity = await Context.Allegries
                .Where(entity => entity.IsValid
                && entity.Id == allergyFormDto.Id)
                .SingleOrDefaultAsync();

            if (entity is null)
                return null;

            entity.Name = allergyFormDto.Name;
            entity.AllergyType = allergyFormDto.AllergyType;
            entity.Description = allergyFormDto.Description;

            if (allergyFormDto.RemovedFilesIds.Count() > 0)
                allergyFormDto = await RemoveAllergyFiles(allergyFormDto);

            Context.Entry(entity).State = EntityState.Detached;

            if (allergyFormDto.Images.Count != 0)
                entity = AddingAllergyFiles(allergyFormDto.Images, entity);

            Context.Allegries.Update(entity);
            await Context.SaveChangesAsync();
            return allergyFormDto;
        }

        public async Task<AllergyFormDto> RemoveAllergyFiles(AllergyFormDto allergyFormDto)
        {
            var removedFiles = await Context.Files
                .Where(file => allergyFormDto.RemovedFilesIds.Contains(file.Id))
                .ToListAsync();

            Context.RemoveRange(removedFiles);

            allergyFormDto.RemovedFilesUrls = removedFiles
                .Select(file => file.Url)
                .ToList();

            return allergyFormDto;
        }
        #endregion

        #endregion

        #region Patient Allergy Action
        public async Task<OperationResult<GenericOperationResult, PatientAllergyDto>> ActionPatientAllergy(PatientAllergyFormDto patientAllergyFormDto)
        {
            OperationResult<GenericOperationResult,
                PatientAllergyDto> result = new(GenericOperationResult.ValidationError);

            if (patientAllergyFormDto.AllergyId == 0)
                return result.AddError(ErrorMessages.MustChooseAllergy);

            patientAllergyFormDto.UserId = patientAllergyFormDto.UserId != 0 ?
                patientAllergyFormDto.UserId :
                UserRepository.CurrentUserIdentityId();

            try
            {
                var data = patientAllergyFormDto.Id == 0 ?
                    await AddPatientAllergy(patientAllergyFormDto) :
                    await UpdatePatientAlergy(patientAllergyFormDto);

                if (data is null)
                    return result.AddError(ErrorMessages.AllergyNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError).UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        #region Add Patient Allergy
        private async Task<PatientAllergyDto> AddPatientAllergy(PatientAllergyFormDto patientAllergyFormDto)
        {
            PatientAllergyDto data = new();

            var entity = new PatientAllergy
            {
                AllegryId = patientAllergyFormDto.AllergyId,
                UserId = patientAllergyFormDto.UserId
            };

            await Context.PatientAllergies.AddAsync(entity);

            if (patientAllergyFormDto.Images.Count != 0)
                entity = AddingPatientAllergyFiles(patientAllergyFormDto.Images, entity);

            entity = await AddingAllergycausingMedicines(entity, patientAllergyFormDto.AllergyCausingMedicinIds);

            entity = await AddingAllergyMedicines(entity, patientAllergyFormDto.MedicineIds);

            data.Id = entity.Id;

            return data;
        }
        private PatientAllergy AddingPatientAllergyFiles(
        IEnumerable<FileDetailsDto> fileDetailsDto, PatientAllergy entity)
        {
            foreach (FileDetailsDto image in fileDetailsDto)
            {
                entity.Files.Add(new PatientAllergyFile()
                {
                    Checksum = image.Checksum,
                    FileContentType = image.FileContentType,
                    Description = image.Description,
                    Extension = image.Extension,
                    FileName = image.FileName,
                    Name = image.Name,
                    Url = image.Url,
                });
            }
            return entity;
        }
        private async Task<PatientAllergy> AddingAllergycausingMedicines(PatientAllergy entity, IEnumerable<int> allergyCausingMedicinIds)
        {
            var allergycausingMeds = Context.Medicines
                .Where(med => med.IsValid
                && allergyCausingMedicinIds
                .Contains(med.Id))
                .Select(md => new AllergyCausingMedicin()
                {
                    MedicineId = md.Id,
                    PatientAllergy = entity
                }).ToList();

            Context.AddRange(allergycausingMeds);
            await Context.SaveChangesAsync();

            return entity;
        }
        private async Task<PatientAllergy> AddingAllergyMedicines(PatientAllergy entity, IEnumerable<int> medicineIds)
        {
            var AllergyMedicines = Context.Medicines
               .Where(med => med.IsValid
               && medicineIds
               .Contains(med.Id))
               .Select(md => new PatientMedicine()
               {
                   MedicineId = md.Id,
                   PatientAllergy = entity
               }).ToList();

            Context.AddRange(AllergyMedicines);
            await Context.SaveChangesAsync();

            return entity;
        }

        #endregion

        #region Update Patient Allergy
        private async Task<PatientAllergyDto> UpdatePatientAlergy(PatientAllergyFormDto patientAllergyFormDto)
        {
            PatientAllergyDto data = new();

            var entity = await Context.PatientAllergies
                .Where(entity => entity.IsValid
                && entity.Id == patientAllergyFormDto.Id)
                .Include(entity => entity.Files)
                .Include(entity => entity.AllergyCausingMedicins)
                .Include(entity => entity.Medicines)
                .SingleOrDefaultAsync();

            if (entity == null || entity.Id != patientAllergyFormDto.Id)
                return null;

            if (patientAllergyFormDto.RemovedFilesIds.Count() > 0)
                data = RemovePatientAllergyFiles(data, patientAllergyFormDto.RemovedFilesIds);

            if (patientAllergyFormDto.Images.Count != 0)
                entity = AddingPatientAllergyFiles(patientAllergyFormDto.Images, entity);

            Context.PatientAllergies.Update(entity);

            RemovePatientAllergyMedicines(entity,
                patientAllergyFormDto.AllergyCausingMedicinIds,
                patientAllergyFormDto.MedicineIds);

            AddingPatientAllergyMedicinesAtUpdateState(entity,
                patientAllergyFormDto.AllergyCausingMedicinIds,
                patientAllergyFormDto.MedicineIds);

            Context.SaveChanges();
            data.Id = entity.Id;

            return data;
        }
        private void AddingPatientAllergyMedicinesAtUpdateState(
            PatientAllergy entity,
            IEnumerable<int> patientAllergyMedicinedIds,
            IEnumerable<int> patientMedicineIds)
        {
            var AllergyCausinMedsIds = entity.AllergyCausingMedicins
                .Where(entity => entity.IsValid)
                .Select(entity => entity.MedicineId)
                .ToList();

            var AllergyMedsIds = entity.Medicines
                .Where(entity => entity.IsValid)
                .Select(entity => entity.MedicineId)
                .ToList();

            var AllergyCausinMedsToAdd = Context.Medicines
                .Where(med => med.IsValid
                && !AllergyCausinMedsIds
                .Contains(med.Id)
                && patientAllergyMedicinedIds
                .Contains(med.Id))
                .Select(md => new PatientMedicine()
                {
                    MedicineId = md.Id,
                    PatientAllergy = entity
                }).ToList();

            var AllergyMedsToAdd = Context.Medicines
                 .Where(med => med.IsValid
                 && !AllergyMedsIds
                 .Contains(med.Id)
                 && patientMedicineIds
                 .Contains(med.Id))
                 .Select(md => new AllergyCausingMedicin()
                 {
                     MedicineId = md.Id,
                     PatientAllergy = entity
                 }).ToList();

            Context.AddRange(AllergyCausinMedsToAdd);
            Context.AddRange(AllergyMedsToAdd);
        }
        private void RemovePatientAllergyMedicines(
            PatientAllergy entity,
            IEnumerable<int> patientAllergyMedicinedIds,
            IEnumerable<int> patientMedicineIds)
        {
            var AllergyCausinMedsToRemove = entity.AllergyCausingMedicins
                         .Where(entity => entity.IsValid &&
                         !patientAllergyMedicinedIds.Contains(entity.MedicineId))
                         .ToList();

            var MedicinsToRemove = entity.Medicines
                     .Where(med => med.IsValid &&
                     !patientMedicineIds.Contains(med.MedicineId))
                     .ToList();

            Context.RemoveRange(MedicinsToRemove);
            Context.RemoveRange(AllergyCausinMedsToRemove);
        }

        private PatientAllergyDto RemovePatientAllergyFiles(
            PatientAllergyDto data, IEnumerable<int> removeFileIds)
        {
            var removedFiles = Context.Files
                    .Where(file => removeFileIds.Contains(file.Id))
                    .ToList();

            data.RemovedFilesUrls = removedFiles.Select(file => file.Url).ToList();
            Context.RemoveRange(removedFiles);

            return data;
        }
        #endregion

        #endregion

        #region Get
        public async Task<OperationResult<GenericOperationResult, IEnumerable<AllergyFormDto>>> GetAllAllergy(
            int? userId, bool enablePagination, int pageSize, int pageNumber)
        {
            OperationResult<GenericOperationResult, IEnumerable<AllergyFormDto>> result = new(GenericOperationResult.Failed);

            try
            {
                var allergyData = Context.Allegries
                    .Include(entity => entity.Files)
                    .Where(entity => entity.IsValid
                    && (userId == null || entity.PatientsAllegry.Any(entity => entity.UserId == userId)));

                if (enablePagination)
                    allergyData = ExtensionMethods.GetDataWithPagination(allergyData, pageSize, pageNumber);

                var resultData = await allergyData
                    .Select(entity =>
                    new AllergyFormDto()
                    {
                        Id = entity.Id,
                        Name = entity.Name,
                        Description = entity.Description,
                        AllergyType = entity.AllergyType,
                        Images = entity.Files
                        .Where(file => file.IsValid)
                        .Select(file => new FileDetailsDto()
                        {
                            Id = file.Id,
                            Checksum = file.Checksum,
                            FileContentType = file.FileContentType,
                            Description = file.Description,
                            FileName = file.FileName,
                            Name = file.Name,
                            Url = file.Url,
                            Extension = file.Extension
                        }).ToList()
                    }).ToListAsync();

                return result.UpdateResultData(resultData).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, IEnumerable<AllergySelectDto>>> GetAllergiesAutoComplete(string query)
        {
            OperationResult<GenericOperationResult, IEnumerable<AllergySelectDto>> result = new(GenericOperationResult.Failed);
            try
            {
                var data = await ValidEntities
                    .Where(all => all.Name.ToUpper().Contains(query) || all.Description.ToUpper().Contains(query))
                    .Select(all => new AllergySelectDto()
                    {
                        Id = all.Id,
                        Name = all.Name
                    }).ToListAsync();

                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, AllergyInfoDto>> GetAllergy(int Id)
        {
            OperationResult<GenericOperationResult, AllergyInfoDto> result = new(GenericOperationResult.Failed);
            try
            {
                var data = await Context.Allegries
                    .Include(entity => entity.Files)
                    .Where(entity => entity.IsValid && entity.Id == Id)
                    .Select(entity => new AllergyInfoDto()
                    {
                        Id = entity.Id,
                        Name = entity.Name,
                        AllergyType = entity.AllergyType,
                        Description = entity.Description,
                        CreatedBy = entity.CreatedBy,
                        PatientsAllegry = Context.PatientAllergies
                        .Where(a => a.AllegryId == Id)
                        .Select(entity => new PatientAllergyDto()
                        {
                            Id = entity.Id,
                            UserId = entity.UserId ?? 0,
                            Medicines = entity.Medicines.Select(medicine => new PatientMedicineDto()
                            {
                                Id = medicine.Id,
                                MedicineId = medicine.MedicineId,
                                MedicineName = medicine.Medicine.Name,

                            }).ToList(),
                            AllergyCausingMedicins = entity.AllergyCausingMedicins
                            .Select(acm => new AllergyCausingMedicinDto()
                            {
                                Id = acm.Id,
                                MedicineId = acm.MedicineId,
                            }).ToList()
                        }).ToList(),

                        Images = entity.Files
                        .Where(file => file.IsValid)
                        .Select(file => new FileDetailsDto()
                        {
                            Id = file.Id,
                            Checksum = file.Checksum,
                            FileContentType = file.FileContentType,
                            Description = file.Description,
                            FileName = file.FileName,
                            Name = file.Name,
                            Url = file.Url,
                            Extension = file.Extension
                        }).ToList()
                    }).SingleOrDefaultAsync();

                if (data is null)
                    return result.AddError(ErrorMessages.AllergyNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, IEnumerable<PatientAllergyDto>>> GetPatientAllergies(
            int? userId, bool enablePagination, int pageSize, int pageNumber)
        {
            OperationResult<GenericOperationResult, IEnumerable<PatientAllergyDto>> result = new(GenericOperationResult.Failed);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var data = Context.PatientAllergies
                       .Where(pa => pa.IsValid && (!userId.HasValue && userInfo.CurrentUserId == pa.UserId
                       || ((userInfo.IsUserAdmin || userInfo.IsNormalUser) && pa.UserId == userId)
                       || (userInfo.IsHealthCareProvider && (userId == pa.CreatedBy && userId == pa.UserId)
                       || (userInfo.CurrentUserId == pa.CreatedBy && userId == pa.UserId))));

                if (enablePagination)
                    data = data.GetDataWithPagination(pageSize, pageNumber);

                var resultData = data
                    .Select(pa => new PatientAllergyDto()
                    {
                        Id = pa.Id,
                        AllergyId = pa.AllegryId,
                        UserId = pa.UserId ?? 0,
                        AllergyName = pa.Allegry.Name,
                        AllergyDescription = pa.Allegry.Description,
                        Images = pa.Files
                        .Where(file => file.IsValid)
                        .Select(file => new FileDetailsDto()
                        {
                            Id = file.Id,
                            Checksum = file.Checksum,
                            FileContentType = file.FileContentType,
                            Description = file.Description,
                            FileName = file.FileName,
                            Name = file.Name,
                            Url = file.Url,
                            Extension = file.Extension,
                            IsValid = file.IsValid
                        }).ToList(),
                        AllergyCausingMedicins = pa.AllergyCausingMedicins
                           .Where(med => med.IsValid)
                           .Select(alm => new AllergyCausingMedicinDto()
                           {
                               Id = alm.Id,
                               MedicineId = alm.MedicineId,
                               MedicineName = alm.Medicine.Name
                           }).ToList(),
                        Medicines = pa.Medicines
                           .Where(med => med.IsValid)
                           .Select(med => new PatientMedicineDto()
                           {
                               Id = med.Id,
                               MedicineId = med.MedicineId,
                               MedicineName = med.Medicine.Name
                           }).ToList()
                    }).ToList();

                return result.UpdateResultData(resultData).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, PatientAllergyDto>> GetPatientAllergyDetails(int PatientAllergyId)
        {
            OperationResult<GenericOperationResult, PatientAllergyDto> result = new(GenericOperationResult.Failed);
            try
            {
                var data = await Context.PatientAllergies
                    .Where(pa => pa.IsValid && pa.Id == PatientAllergyId)
                    .Select(pa => new PatientAllergyDto()
                    {
                        Id = pa.Id,
                        AllergyId = pa.AllegryId,
                        UserId = pa.UserId ?? 0,
                        AllergyCausingMedicins = pa.AllergyCausingMedicins
                        .Where(entity => entity.IsValid)
                        .Select(alm => new AllergyCausingMedicinDto()
                        {
                            Id = alm.Id,
                            MedicineId = alm.MedicineId,
                            MedicineName = alm.Medicine.Name
                        }).ToList(),
                        Images = pa.Files
                        .Where(file => file.IsValid)
                        .Select(file => new FileDetailsDto()
                        {
                            Id = file.Id,
                            Checksum = file.Checksum,
                            FileContentType = file.FileContentType,
                            Description = file.Description,
                            FileName = file.FileName,
                            Name = file.Name,
                            Url = file.Url,
                            Extension = file.Extension,
                            IsValid = file.IsValid
                        }).ToList(),
                        Medicines = pa.Medicines
                        .Where(med => med.IsValid)
                        .Select(med => new PatientMedicineDto()
                        {
                            Id = med.Id,
                            MedicineId = med.MedicineId,
                            MedicineName = med.Medicine.Name
                        }).ToList()
                    }).SingleOrDefaultAsync();

                if (data is null)
                    return result.AddError(ErrorMessages.PatientAllergyNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }

            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError).UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion

        #region Remove
        public async Task<OperationResult<GenericOperationResult>> RemoveAllergy(int Id)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.Failed);
            try
            {
                var entity = await Context.Allegries
                    .Where(entity => entity.IsValid && entity.Id == Id)
                    .SingleOrDefaultAsync();

                if (entity is null)                
                    return result.AddError(ErrorMessages.AllergyNotExist).UpdateResultStatus(GenericOperationResult.NotFound);
               
                Context.Allegries.Remove(entity);
                await Context.SaveChangesAsync();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult>> RemovePatientAllergy(int PatientAllergyId)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.Failed);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                PatientAllergy entity = await Context.PatientAllergies
                    .Where(entity => entity.IsValid
                    && (userInfo.IsUserAdmin || userInfo.CurrentUserId == entity.CreatedBy)
                    && entity.Id == PatientAllergyId)
                    .SingleOrDefaultAsync();

                if (entity is null)                
                    return result.AddError(ErrorMessages.AllergyNotExist).UpdateResultStatus(GenericOperationResult.NotFound);                

                Context.PatientAllergies.Remove(entity);
                await Context.SaveChangesAsync();

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion
    }
}
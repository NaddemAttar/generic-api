﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AutoMapper;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.JobOffer;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums.Core;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;

using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class JobOfferRepository : BasicRepository<CPDbContext, JobOffer>, IJobOfferRepository
    {
        #region Properties & Constructor
        private IUserRepository UserRepository { get; }
        public JobOfferRepository(
            CPDbContext context,
            IMapper mapper,
            IUserRepository UserRepository) : base(context, mapper)
        {
            this.UserRepository = UserRepository;
        }
        #endregion

        #region Change Job Status
        public OperationResult<GenericOperationResult> ChangeJobStatus(int jobApplicationId, JobStatus status)
        {
            var result = new OperationResult<GenericOperationResult>(GenericOperationResult.ValidationError);
            try
            {
                var entity = Context.JobApplications
                .Where(jobApp => jobApp.Id == jobApplicationId)
                .SingleOrDefault();

                if (entity != null)
                {
                    entity.JobStatus = status;
                    Context.JobApplications.Update(entity);
                    Context.SaveChanges();
                    return result
                   .UpdateResultStatus(GenericOperationResult.Success);
                }
                else
                {
                    result.AddError(ErrorMessages.JobOfferNotFound).UpdateResultStatus(GenericOperationResult.NotFound);
                }
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                   .UpdateResultStatus(GenericOperationResult.Failed);
            }


            return result;
        }
        #endregion

        #region Apply to Job Offer
        public OperationResult<GenericOperationResult, JobApplicationsDto> ApplyToJobOffer(JobApplicationsDto formDto)
        {
            var result = new OperationResult<GenericOperationResult,
                JobApplicationsDto>(GenericOperationResult.ValidationError);
            try
            {
                var entity = new JobApplication()
                {
                    Email = formDto.Email,
                    FirstName = formDto.FirstName,
                    LastName = formDto.LastName,
                    Phone = formDto.Phone,
                    UserId = formDto.UserId,
                    JobOfferId = formDto.JobOfferId,
                    JobStatus = JobStatus.Pending
                };
                entity.CvFile = new UserJobOfferCv()
                {
                    Checksum = formDto.CvFile.Checksum,
                    FileContentType = formDto.CvFile.FileContentType,
                    Description = formDto.CvFile.Description,
                    Extension = formDto.CvFile.Extension,
                    FileName = formDto.CvFile.FileName,
                    Name = formDto.CvFile.Name,
                    Url = formDto.CvFile.Url,
                };
                Context.JobApplications.Add(entity);
                Context.SaveChanges();
                return result.UpdateResultData(formDto)
                  .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                  .UpdateResultStatus(GenericOperationResult.Failed);
            }

        }

        #endregion

        #region Actions
        public OperationResult<GenericOperationResult, JobOfferFormDto> Action(JobOfferFormDto jobOfferFormDto)
        {
            var result = new OperationResult<GenericOperationResult,
                JobOfferFormDto>(GenericOperationResult.ValidationError);
            try
            {
                if (jobOfferFormDto is null)
                {
                    result.AddError("Cannot have null parameter.");
                    return result;
                }

                result = jobOfferFormDto.Id == 0 ? Create(jobOfferFormDto) : Update(jobOfferFormDto);

                if (!result.IsSuccess)
                {
                    result.AddError("Error Happend");
                    return result;
                }

                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                result.AddError("An Exception has been thrown.");
                return result;
            }
        }

        #region Create Job Offer
        private OperationResult<GenericOperationResult, JobOfferFormDto> Create(JobOfferFormDto jobOfferFormDto)
        {
            var result = new OperationResult<GenericOperationResult,
                JobOfferFormDto>(GenericOperationResult.ValidationError);
            try
            {
                var entity = CreateJobOfferEntity(jobOfferFormDto);

                entity = JobOfferSalary(jobOfferFormDto, entity);

                if (jobOfferFormDto.Attachments != null || jobOfferFormDto.Attachments.Count() != 0)
                {
                    entity = AddingJobOfferFiles(jobOfferFormDto.Attachments, entity);
                }

                Context.Add(entity);
                Context.SaveChanges();

                jobOfferFormDto.Attachments = GetEntityFiles(entity);

                foreach (var file in jobOfferFormDto.Attachments)
                {
                    file.SourceId = entity.Id;
                }
                // TODO: think of how to update the id after savechange.
                jobOfferFormDto.Id = entity.Id;

                return result.UpdateResultData(jobOfferFormDto)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                  .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion

        #region Updaet Job Offer
        private OperationResult<GenericOperationResult, JobOfferFormDto> Update(JobOfferFormDto jobOfferFormDto)
        {
            var result = new OperationResult<GenericOperationResult,
                JobOfferFormDto>(GenericOperationResult.ValidationError);
            try
            {
                var entity = GetJobOfferEntityById(jobOfferFormDto.Id);

                if (jobOfferFormDto.RemovedFilesIds.Count > 0)
                {
                    jobOfferFormDto = RemoveJobOfferFiles(jobOfferFormDto);
                }
                entity.Id = jobOfferFormDto.Id;
                entity.Title = jobOfferFormDto.Title;
                entity.Responsibilty = jobOfferFormDto.Responsibilty;
                entity.Qualification = jobOfferFormDto.Qualification;
                entity.ContractType = jobOfferFormDto.ContractType.Value;
                entity.CityId = jobOfferFormDto.CityId;
                entity.PlaceOfWork = jobOfferFormDto.PlaceOfWork;
                entity.SalaryType = jobOfferFormDto.SalaryType;
                entity = JobOfferSalary(jobOfferFormDto, entity);
                foreach (var attachment in jobOfferFormDto.Attachments)
                {
                    entity = AddingJobOfferFiles(jobOfferFormDto.Attachments, entity);
                }
                Context.Update(entity);
                Context.SaveChanges();
                jobOfferFormDto.Attachments = GetEntityFiles(entity);
                return result.UpdateResultData(jobOfferFormDto)
                  .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (AutoMapperMappingException)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                  .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion

        #region Another Private Methods Related to the Action
        private JobOffer GetJobOfferEntityById(int entityId)
        {
            return ValidEntities
                    .Include(entity => entity.Files)
                    .Where(entity => entity.Id == entityId)
                    .SingleOrDefault();
        }
        private JobOfferFormDto RemoveJobOfferFiles(JobOfferFormDto jobOfferFormDto)
        {
            var removedFiles = Context.Files
                 .Where(file => jobOfferFormDto.RemovedFilesIds.Contains(file.Id))
                 .ToList();

            Context.RemoveRange(removedFiles);
            jobOfferFormDto.RemovedFilesUrls = removedFiles
                .Select(file => file.Url)
                .ToList();

            return jobOfferFormDto;
        }
        private JobOffer CreateJobOfferEntity(JobOfferFormDto jobOfferFormDto)
        {
            return new JobOffer()
            {
                Title = jobOfferFormDto.Title,
                Responsibilty = jobOfferFormDto.Responsibilty,
                Qualification = jobOfferFormDto.Qualification,
                ContractType = jobOfferFormDto.ContractType.Value,
                ExpirationDate = jobOfferFormDto.ExpirationDate,
                PlaceOfWork = jobOfferFormDto.PlaceOfWork,
                CityId = jobOfferFormDto.CityId,
                SalaryType = jobOfferFormDto.SalaryType
            };
        }
        private JobOffer JobOfferSalary(
            JobOfferFormDto jobOfferFormDto, JobOffer entity)
        {
            switch (jobOfferFormDto.SalaryType)
            {
                case SalaryType.Salary:
                    entity.Salary = jobOfferFormDto.Salary;
                    break;
                case SalaryType.Range:
                    entity.Salary = jobOfferFormDto.Salary;
                    entity.MaxSalary = jobOfferFormDto.MaxSalary;
                    break;
                case SalaryType.AfterInterview:
                    break;
                default:
                    break;
            }
            return entity;
        }
        private JobOffer AddingJobOfferFiles(
            IEnumerable<FileDetailsDto> attachments, JobOffer entity)
        {
            foreach (var attachment in attachments)
            {
                var file = new JobOfferImage()
                {
                    Checksum = attachment.Checksum,
                    FileContentType = attachment.FileContentType,
                    Description = attachment.Description,
                    Extension = attachment.Extension,
                    FileName = attachment.FileName,
                    Name = attachment.Name,
                    Url = attachment.Url,
                };

                entity.Files.Add(Mapper.Map<JobOfferImage>(file));
            }

            return entity;
        }
        private List<FileDetailsDto> GetEntityFiles(JobOffer entity)
        {
            return entity.Files
                    .Where(file => file.IsValid)
                    .Select(file => new FileDetailsDto()
                    {
                        Id = file.Id,
                        Checksum = file.Checksum,
                        FileContentType = file.FileContentType,
                        Description = file.Description,
                        FileName = file.FileName,
                        Name = file.Name,
                        Url = file.Url
                    })
                    .ToList();
        }

        #endregion

        #endregion

        #region Get
        public async Task<OperationResult<GenericOperationResult, IEnumerable<JobOfferDto>>> GetAllJobOffers(JobOfferOptions jobOfferOptions)
        {
            var result = new OperationResult<GenericOperationResult,
                IEnumerable<JobOfferDto>>(GenericOperationResult.ValidationError);
            var builder = WebApplication.CreateBuilder();
            bool multiProviders = bool.Parse(builder.Configuration.GetSection("MultiProviders").Value.ToLower());
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();
                var JobOffers = ValidEntities
                    .Include(file => file.Files)
                    .Where(entity => (userInfo.CurrentUserId == 0
                    || userInfo.IsUserAdmin
                    || (multiProviders == false ? true : userInfo.IsHealthCareProvider ? userInfo.CurrentUserId == entity.CreatedBy : true))
                    && (jobOfferOptions.Query == null
                    || entity.Responsibilty.ToUpper().Contains(jobOfferOptions.Query.ToUpper())
                    || (entity.Qualification.ToUpper().Contains(jobOfferOptions.Query.ToUpper()))
                    || (entity.Title.ToUpper().Contains(jobOfferOptions.Query.ToUpper())))
                        && (jobOfferOptions.ContractType == null
                        || entity.ContractType == jobOfferOptions.ContractType)
                        && (jobOfferOptions.PlaceOfWork == null
                        || entity.PlaceOfWork == jobOfferOptions.PlaceOfWork)
                        && (jobOfferOptions.CityId == null || entity.CityId == jobOfferOptions.CityId)
                        && (jobOfferOptions.CountryId == null
                        || entity.City.CountryId == jobOfferOptions.CountryId)
                        && (jobOfferOptions.StartSalary == 0
                        && jobOfferOptions.EndSalary == 0
                        || (((entity.Salary >= jobOfferOptions.StartSalary)
                        || jobOfferOptions.StartSalary == 0)
                        && ((entity.Salary <= jobOfferOptions.EndSalary)
                        || jobOfferOptions.EndSalary == 0))))
                    .Skip(jobOfferOptions.PaginationOptions.PageNumber * jobOfferOptions.PaginationOptions.PageSize)
                    .Take(jobOfferOptions.PaginationOptions.PageSize)
                    .Select(entity => new JobOfferDto
                    {
                        Id = entity.Id,
                        Title = entity.Title,
                        Responsibilty = entity.Responsibilty,
                        Qualification = entity.Qualification,
                        ContractType = entity.ContractType,
                        ExpirationDate = entity.ExpirationDate,
                        CityId = entity.CityId,
                        CityName = entity.City.Name,
                        CountryName = entity.City.Country.Name,
                        PlaceOfWork = entity.PlaceOfWork,
                        SalaryType = entity.SalaryType,
                        Salary = entity.Salary,
                        MaxSalary = entity.MaxSalary,
                        Attachments = entity.Files
                        .Where(file => file.IsValid)
                        .Select(file => new FileDetailsDto()
                        {
                            Id = file.Id,
                            Name = file.Name,
                            FileName = file.FileName,
                            CultureCode = file.FileName,
                            Url = file.Url,
                            Extension = file.Extension
                        }).ToList(),
                        Applicants = entity.JobApplications
                           .Where(entity => entity.IsValid)
                           .Select(entity => new JobApplicationsDto()
                           {
                               Id = entity.Id,
                               UserId = entity.UserId,
                               Email = entity.Email,
                               FirstName = entity.FirstName,
                               LastName = entity.LastName,
                               Phone = entity.Phone,
                               CvFile = new FileDetailsDto()
                               {
                                   Id = entity.CvFile.Id,
                                   Name = entity.CvFile.Name,
                                   FileName = entity.CvFile.FileName,
                                   CultureCode = entity.CvFile.FileName,
                                   Url = entity.CvFile.Url,
                                   Extension = entity.CvFile.Extension
                               }
                           }).ToList()
                    })
                    .ToList();

                return result.UpdateResultData(JobOffers)
                    .UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                  .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public OperationResult<GenericOperationResult, JobOfferDto> GetJobOffer(int id)
        {
            var result = new OperationResult<GenericOperationResult,
                JobOfferDto>(GenericOperationResult.Failed);
            try
            {
                var formDto = ValidEntities
                    .Where(entity => entity.Id == id)
                    .Select(entity => new JobOfferDto()
                    {
                        Id = entity.Id,
                        Responsibilty = entity.Responsibilty,
                        Qualification = entity.Qualification,
                        ContractType = entity.ContractType,
                        Title = entity.Title,
                        ExpirationDate = entity.ExpirationDate,
                        CityId = entity.CityId,
                        CityName = entity.City.Name,
                        CountryName = entity.City.Country.Name,
                        PlaceOfWork = entity.PlaceOfWork,
                        SalaryType = entity.SalaryType,
                        MaxSalary = entity.MaxSalary,
                        Salary = entity.Salary,
                        Attachments = entity.Files
                        .Where(file => file.IsValid)
                        .Select(file => new FileDetailsDto()
                        {
                            Id = file.Id,
                            Name = file.Name,
                            FileName = file.FileName,
                            CultureCode = file.FileName,
                            Url = file.Url,
                            Extension = file.Extension
                        }).ToList(),
                        Applicants = entity.JobApplications
                        .Where(e => e.IsValid)
                        .Select(e => new JobApplicationsDto()
                        {
                            Id = e.Id,
                            UserId = e.UserId,
                            Email = e.Email,
                            FirstName = e.FirstName,
                            LastName = e.LastName,
                            Phone = e.Phone,
                            JobStatus = e.JobStatus,
                            CvFile = new FileDetailsDto()
                            {
                                Id = e.CvFile.Id,
                                Name = e.CvFile.Name,
                                FileName = e.CvFile.FileName,
                                CultureCode = e.CvFile.FileName,
                                Url = e.CvFile.Url,
                                Extension = e.CvFile.Extension
                            }
                        }).ToList()
                    }).SingleOrDefault();
                return formDto == null
                    ? result.UpdateResultStatus(GenericOperationResult.NotFound)
                    : result.UpdateResultData(formDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                  .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion

        #region Remove
        public async Task<OperationResult<GenericOperationResult, JobOfferDto>> RemoveJobOffer(int id)
        {
            var result = new OperationResult<GenericOperationResult, JobOfferDto>(GenericOperationResult.ValidationError);
            try
            {
                var userInfo = await UserRepository.GetCurrentUserInfo();

                var JobEntity = ValidEntities
                    .Where(entity => entity.Id == id && (userInfo.IsUserAdmin || userInfo.CurrentUserId == entity.CreatedBy))
                    .SingleOrDefault();

                if (JobEntity != null)
                {
                    Context.JobOffers.Remove(JobEntity);
                    Context.SaveChanges();
                    return result.UpdateResultStatus(GenericOperationResult.Success);
                }
                return result.UpdateResultStatus(GenericOperationResult.NotFound);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError)
                  .UpdateResultStatus(GenericOperationResult.Failed);
            }
        }

        #endregion
    }
}

﻿using AutoMapper;
using CraftLab.Core.OperationResults;
using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Core.Dto.Category;
using IGenericControlPanel.Core.IData;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data
{
    public class MultiLevelCategoryRepository : BasicRepository<CPDbContext, MultiLevelCategory>, IMultiLevelCategoryRepository
    {
        #region Properties

        private IUserLanguageRepository UserLanguageRepository { get; }

        #endregion

        #region Constructors

        public MultiLevelCategoryRepository(CPDbContext context,
            IMapper mapper,
            IUserLanguageRepository userLanguageRepository)
            : base(context, mapper)
        {
            UserLanguageRepository = userLanguageRepository;
        }
        #endregion

        #region Get

        public async Task<OperationResult<GenericOperationResult, IEnumerable<MultiLevelCategoryDetailsDto>>> GetMultiLevelCategoriesNest()
        {
            OperationResult<GenericOperationResult, IEnumerable<MultiLevelCategoryDetailsDto>> result = new(GenericOperationResult.Failed);
            try
            {
                List<MultiLevelCategoryDetailsDto> data = await Context.MultiLevelCategories
                    .Where(mlc => mlc.ParentCategoryId == null && mlc.IsValid)
                    .Select(mlc => new MultiLevelCategoryDetailsDto()
                    {
                        Id = mlc.Id,
                        Name = mlc.Name,
                        ParentId = mlc.ParentCategoryId,
                        Children = mlc.Children.Where(ch => ch.IsValid).Select(mlc1 => new MultiLevelCategoryDetailsDto()
                        {
                            Id = mlc1.Id,
                            Name = mlc1.Name,
                            ParentId = mlc1.ParentCategoryId,
                            Children = mlc1.Children.Where(ch => ch.IsValid).Select(mlc2 => new MultiLevelCategoryDetailsDto()
                            {
                                Id = mlc2.Id,
                                Name = mlc2.Name,
                                ParentId = mlc2.ParentCategoryId,
                                Children = mlc2.Children.Where(ch => ch.IsValid).Select(mlc3 => new MultiLevelCategoryDetailsDto()
                                {
                                    Id = mlc3.Id,
                                    Name = mlc3.Name,
                                    ParentId = mlc3.ParentCategoryId,
                                    Children = mlc3.Children.Where(ch => ch.IsValid).Select(mlc4 => new MultiLevelCategoryDetailsDto()
                                    {
                                        Id = mlc4.Id,
                                        Name = mlc4.Name,
                                        ParentId = mlc4.ParentCategoryId
                                    })
                                })
                            })
                        })
                    })
                    .ToListAsync();
                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<MultiLevelCategoryDto>>> GetMultiLevelCategoriesLeafs()
        {
            OperationResult<GenericOperationResult, IEnumerable<MultiLevelCategoryDto>> result = new(GenericOperationResult.Failed);
            try
            {
                List<MultiLevelCategoryDto> data = await Context.MultiLevelCategories
                    .Where(mlc => mlc.Children.Count == 0 && mlc.IsValid).Select(mlc => new MultiLevelCategoryDto()
                    {
                        Id = mlc.Id,
                        Name = mlc.Name,
                        ParentId = mlc.ParentCategoryId
                    })
                    .ToListAsync();
                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<CategoryWithProductCountL>>> GetCategoriesWithChildrenCount()
        {
            OperationResult<GenericOperationResult, IEnumerable<CategoryWithProductCountL>> result = new(GenericOperationResult.Failed);
            try
            {
                List<CategoryWithProductCountL> data = await Context.MultiLevelCategories
                    .Where(mlc => mlc.Children.Count == 0 && mlc.IsValid)
                    .Select(mlc => new CategoryWithProductCountL()
                    {
                        Id = mlc.Id,
                        Name = mlc.Name,
                        ParentId = mlc.ParentCategoryId,
                        ProductCount = mlc.Products.Where(entity => entity.IsValid).Count(),
                        BlogsCount = mlc.MultiLevelCategoryRelatedEntities.Where(entity => entity.IsValid).Select(entity => entity.Blog).Count(),
                        QuestionCount = mlc.MultiLevelCategoryRelatedEntities.Where(entity => entity.IsValid).Select(entity => entity.Question).Count()
                    })
                    .ToListAsync();
                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<MultiLevelCategoryDetailsDto>>> GetMultiLevelCategories(string query, bool WithProducts)
        {
            OperationResult<GenericOperationResult, IEnumerable<MultiLevelCategoryDetailsDto>> result = new(GenericOperationResult.Failed);
            try
            {
                List<MultiLevelCategoryDetailsDto> data = await Context.MultiLevelCategories
                    .Where(mlc => mlc.IsValid && mlc.Name.Contains(query) && (WithProducts ? mlc.Products.Any(pr => pr.IsValid) : true))
                    .Select(mlc => new MultiLevelCategoryDetailsDto()
                    {
                        Id = mlc.Id,
                        Name = mlc.Name,
                    })
                    .ToListAsync();
                return result.UpdateResultData(data).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, string>> GetMultiLevelCategoryPhotoById(int multiLevelCategoryId)
        {
            OperationResult<GenericOperationResult, string> result = new(GenericOperationResult.Failed);
            try
            {
                var multiLevelCategoryUrl = (await Context.MultiLevelCategories.FirstOrDefaultAsync(mlc => mlc.Id == multiLevelCategoryId)).PhotoUrl;
                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(multiLevelCategoryUrl);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Remove
        public async Task<OperationResult<GenericOperationResult>> RemoveMultiLevelCategory(int id)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.Failed);
            try
            {
                MultiLevelCategory entity = Context.MultiLevelCategories.Where(cat => cat.Id == id & cat.IsValid).SingleOrDefault();
                if (entity == null)
                    return result.AddError(ErrorMessages.MultiLevelCategoriesNotFound).UpdateResultStatus(GenericOperationResult.NotFound);

                _ = Context.MultiLevelCategories.Remove(entity);
                await Context.SaveChangesAsync();
                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Action
        public async Task<OperationResult<GenericOperationResult, MultiLevelCategoryDto>> ActionMultiLevelCategory(MultiLevelCategoryDto formDto)
        {
            OperationResult<GenericOperationResult, MultiLevelCategoryDto> result = new(GenericOperationResult.Failed);
            try
            {
                MultiLevelCategory entity = formDto.Id != 0 ? await Context.MultiLevelCategories.Where(cat => cat.Id == formDto.Id && cat.IsValid).SingleOrDefaultAsync() : new MultiLevelCategory();
                entity.IsValid = true;
                entity.Name = formDto.Name;
                entity.ParentCategoryId = formDto.ParentId;
                entity.PhotoUrl = formDto.PhotoUrl;
                if (formDto.Id == 0)
                    await Context.MultiLevelCategories.AddAsync(entity);
                
                await Context.SaveChangesAsync();
                formDto.Id = entity.Id;
                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(formDto);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion
    }
}
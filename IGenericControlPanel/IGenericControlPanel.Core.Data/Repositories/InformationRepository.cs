﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using CraftLab.Core.Infrastructure;
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto.Information;
using IGenericControlPanel.Core.IData;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.HelperMethods;
using IGenericControlPanel.SharedKernal.Repository;

using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class InformationRepository : BasicRepository<CPDbContext, Information>, IInformationRepository
    {

        #region Constructs && Properties
        private IUserLanguageRepository UserLanguageRepository { get; }
        private string blob { get; set; }
        public InformationRepository(CPDbContext dbContext,
            IMapper mapper,
            IUserLanguageRepository userLanguageRepository) : base(dbContext, mapper)
        {
            UserLanguageRepository = userLanguageRepository;
        }
        #endregion

        #region Action Section
        public OperationResult<GenericOperationResult, InfoFormDto> Action(InfoFormDto formDto)
        {
            try
            {
                OperationResult<GenericOperationResult, InfoFormDto> result = new(GenericOperationResult.Failed);

                if (formDto is null)
                {
                    _ = result.AddError("Cannot have null parameter.");
                    return result;
                }

                bool isCreate = formDto != null &&
                    formDto.Id == 0 &&
                    formDto.Address != null;

                result = isCreate ? Create(formDto) : Update(formDto);

                return result.IsSuccess ? new OperationResult<GenericOperationResult,
                    InfoFormDto>(GenericOperationResult.Success, entity: result.Result) : result;
            }
            catch (Exception ex)
            {
                OperationResult<GenericOperationResult, InfoFormDto> exceptionResult = new(GenericOperationResult.Failed, null, ex);

                _ = exceptionResult.AddError("An Exception has been thrown.");
                return exceptionResult;
            }
        }
        #endregion

        #region Create New Information
        private OperationResult<GenericOperationResult, InfoFormDto> Create(InfoFormDto formDto)
        {
            OperationResult<GenericOperationResult, InfoFormDto> result = new(GenericOperationResult.Failed);
            try
            {
                Information entity = new()
                {
                    CultureCode = formDto.CultureCode,
                    Address = formDto.Address,
                    Description = formDto.Description,
                    CurrentDate = DateTime.Parse(formDto.CurrentDate)
                };

                foreach (FileDetailsDto image in formDto.Images)
                {
                    entity.Files.Add(Mapper.Map<InformationFile>(image));
                }

                _ = Context.Add(entity);
                _ = Context.SaveChanges();

                formDto.Id = entity.Id;
                foreach (FileDetailsDto file in formDto.Images)
                {
                    file.SourceId = entity.Id;
                }

                return new OperationResult<GenericOperationResult, InfoFormDto>(GenericOperationResult.Success, formDto);
            }
            catch (AutoMapperMappingException)
            {
                _ = result.AddError("Could not map ProductPrimaryLangaugeFormDto object with Product object");

                return result;
            }
        }
        #endregion

        #region Update Information
        private OperationResult<GenericOperationResult, InfoFormDto> Update(InfoFormDto formDto)
        {
            int entityId = formDto != null && formDto.Address != null ?
                formDto.Id : formDto.TranslationsFormDto?.FirstOrDefault()?.InfoId ?? 0;

            OperationResult<GenericOperationResult, InfoFormDto> result = new(GenericOperationResult.Failed);

            Information data = ValidEntities
                 .Where(entity => entity.Id == entityId)
                 .SingleOrDefault();

            if (formDto != null && formDto.Address != null)
            {


                Context.Entry(data).State = EntityState.Detached;

                try
                {
                    Information entity = new()
                    {
                        Id = formDto.Id,
                        IsValid = true,
                        CultureCode = formDto.CultureCode,
                        Address = formDto.Address,
                        Description = formDto.Description,
                        CurrentDate = DateTime.ParseExact(formDto.CurrentDate,
                        "dd/MM/yyyy", null)
                    };
                    _ = Context.Update(entity);
                }
                catch (AutoMapperMappingException)
                {
                    _ = result.AddError("Could not map ProductPrimaryLangaugeFormDto object with Product object");

                    return result;
                }
            }

            if (formDto.TranslationsFormDto != null && formDto.TranslationsFormDto.Count() > 0)
            {

                foreach (InfoTranslationFormDto translation in formDto.TranslationsFormDto)
                {
                    InformationTranslation dictionaryEntity = GetValidEntities<InformationTranslation>()
                        .Where(dictionary => dictionary.InformationId == translation.InfoId &&
                        dictionary.CultureCode == translation.CultureCode)
                        .SingleOrDefault();

                    EntityState entityState = EntityState.Modified;
                    if (dictionaryEntity == null)
                    {
                        dictionaryEntity = new InformationTranslation()
                        {

                            Address = translation.Address,
                            Description = translation.Description,
                            CultureCode = translation.CultureCode,
                            InformationId = translation.InfoId
                        };

                        entityState = EntityState.Added;
                    }
                    else
                    {
                        dictionaryEntity.Address = translation.Address;
                        dictionaryEntity.Description = translation.Description;
                        dictionaryEntity.CultureCode = translation.CultureCode;
                    }
                    Context.Entry(dictionaryEntity).State = entityState;
                }

            }

            _ = Context.SaveChanges();

            return new OperationResult<GenericOperationResult, InfoFormDto>(GenericOperationResult.Success, formDto);
        }
        #endregion

        #region Get Section
        public OperationResult<GenericOperationResult, InfoDto> Get(int id, string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, InfoDto> result = new(GenericOperationResult.Failed);


            if (id == 0)
            {
                _ = result.AddError("Id is zero");
                return result;
            }

            InfoDto data = ValidEntities
                .Include(entity => entity.Translations)
                .Where(entity => entity.Id == id)
                .Select(entity => Mapper.Map<Information, InfoDto>(entity, opt => opt.Items.Add(ItemNames.CultureCode, HelperMethods.EnsureCulture(cultureCode, UserLanguageRepository.GetDefaultCulture()))))
                .FirstOrDefault();

            return new OperationResult<GenericOperationResult, InfoDto>(GenericOperationResult.Success, data);
        }

        public OperationResult<GenericOperationResult, IEnumerable<InfoDto>> GetAll(string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, IEnumerable<InfoDto>> result = new();

            try
            {
                IEnumerable<InfoDto> data = ValidEntities
                    .Include(entity => entity.Translations)
                    .ToList()
                    .Select(entity => Mapper
                    .Map<Information, InfoDto>(entity, opt => opt.Items.Add(ItemNames.CultureCode,
                    HelperMethods.EnsureCulture(cultureCode, UserLanguageRepository.GetDefaultCulture()))));

                result = new OperationResult<GenericOperationResult, IEnumerable<InfoDto>>(GenericOperationResult.Success, data);
            }
            catch (AutoMapperMappingException exception)
            {
                result = new OperationResult<GenericOperationResult, IEnumerable<InfoDto>>(GenericOperationResult.Failed, null, exception.InnerException);
            }
            catch (Exception exception)
            {
                result = new OperationResult<GenericOperationResult, IEnumerable<InfoDto>>(GenericOperationResult.Failed, null, exception);
            }
            return result;
        }

        public OperationResult<GenericOperationResult, IEnumerable<InfoDetailsDto>> GetAllDetailed(string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, IEnumerable<InfoDetailsDto>> result = new();
            try
            {
                IEnumerable<InfoDetailsDto> data = ValidEntities
                    .Include(entity => entity.Translations)
                    .ToList()
                    .Select(entity => Mapper
                    .Map<Information, InfoDetailsDto>(entity, opt => opt.Items.Add(ItemNames.CultureCode,
                    HelperMethods.EnsureCulture(cultureCode, UserLanguageRepository.GetDefaultCulture()))));

                result = new OperationResult<GenericOperationResult, IEnumerable<InfoDetailsDto>>(GenericOperationResult.Success, data);
            }
            catch (AutoMapperMappingException exception)
            {
                result = new OperationResult<GenericOperationResult, IEnumerable<InfoDetailsDto>>(GenericOperationResult.Failed, null, exception.InnerException);
            }
            catch (Exception exception)
            {
                result = new OperationResult<GenericOperationResult, IEnumerable<InfoDetailsDto>>(GenericOperationResult.Failed, null, exception);
            }

            return result;
        }

        public OperationResult<GenericOperationResult, IPagedList<InfoDetailsDto>> GetAllDetailedPaged(int? pageNumber = 0, int? pageSize = null, string cultureCode = "ar")
        {
            pageSize ??= 10;
            pageNumber ??= 0;

            IQueryable<InfoDetailsDto> data = ValidEntities
                             .Skip(pageNumber.Value * pageSize.Value)
                             .Take(pageSize.Value)
                             .Include(entity => entity.Translations)
                             .Select(entity => Mapper.Map<Information, InfoDetailsDto>(entity, opt => opt.Items.Add(ItemNames.CultureCode, HelperMethods.EnsureCulture(cultureCode, UserLanguageRepository.GetDefaultCulture()))));

            PagedList<InfoDetailsDto> result = new(data, pageNumber.Value, pageSize.Value);

            return new OperationResult<GenericOperationResult, IPagedList<InfoDetailsDto>>(GenericOperationResult.Success, result);
        }

        public OperationResult<GenericOperationResult, IPagedList<InfoDto>> GetAllPaged(int? pageNumber = 0, int? pageSize = null, string cultureCode = "ar")
        {
            pageSize ??= 10;
            pageNumber ??= 0;

            IEnumerable<InfoDto> data = ValidEntities
                             .Include(entity => entity.Translations)
                             .Include(entity => entity.Files)
                             .ToList()
                             .Skip(pageNumber.Value * pageSize.Value)
                             .TakeLast(pageSize.Value)
                             .Select(entity => Mapper.Map<Information, InfoDto>(entity, opt => opt.Items.Add(ItemNames.CultureCode, HelperMethods.EnsureCulture(cultureCode, UserLanguageRepository.GetDefaultCulture()))));

            PagedList<InfoDto> result = new(data, pageNumber.Value, pageSize.Value);

            return new OperationResult<GenericOperationResult, IPagedList<InfoDto>>(GenericOperationResult.Success, result);
        }

        public OperationResult<GenericOperationResult, InfoDetailsDto> GetDetailed(int id, string cultureCode = "ar")
        {
            OperationResult<GenericOperationResult, InfoDetailsDto> result = new(GenericOperationResult.Failed);

            if (id == 0)
            {
                _ = result.AddError("Id is zero");
                return result;
            }

            InfoDetailsDto data = ValidEntities
                .Include(entity => entity.Translations)
                .Where(entity => entity.Id == id)
                .ToList()
                .Select(entity => Mapper.Map<Information, InfoDetailsDto>(entity, opt => opt.Items.Add(ItemNames.CultureCode, HelperMethods.EnsureCulture(cultureCode, UserLanguageRepository.GetDefaultCulture()))))
                .FirstOrDefault();

            return new OperationResult<GenericOperationResult, InfoDetailsDto>(GenericOperationResult.Success, data);
        }

        #endregion

        #region Remove Information
        public OperationResult<GenericOperationResult> Remove(int id)
        {
            try
            {
                Information infoEntity = ValidEntities.SingleOrDefault(information => information.Id == id);
                if (infoEntity != null)
                {
                    _ = Context.Remove(infoEntity);
                }
                _ = Context.SaveChanges();
                return new OperationResult<GenericOperationResult>(GenericOperationResult.Success);
            }
            catch (Exception ex)
            {
                return new OperationResult<GenericOperationResult>(GenericOperationResult.Failed, exception: ex);
            }
        }

        public OperationResult<GenericOperationResult, IPagedList<InfoDto>> GetSomeLatestData(int? pageNumber = 0, int? pageSize = null, string cultureCode = "ar")
        {
            pageSize ??= 10;
            pageNumber ??= 0;

            IEnumerable<InfoDto> data = ValidEntities
                             .Include(entity => entity.Translations)
                             .Include(entity => entity.Files)
                             .ToList()
                             .Skip(pageNumber.Value * pageSize.Value)
                             .TakeLast(pageSize.Value)
                             .Select(entity => Mapper.Map<Information, InfoDto>(entity, opt => opt.Items.Add(ItemNames.CultureCode, HelperMethods.EnsureCulture(cultureCode, UserLanguageRepository.GetDefaultCulture()))));

            PagedList<InfoDto> result = new(data, pageNumber.Value, pageSize.Value);

            return new OperationResult<GenericOperationResult, IPagedList<InfoDto>>(GenericOperationResult.Success, result);

        }
        #endregion

    }
}

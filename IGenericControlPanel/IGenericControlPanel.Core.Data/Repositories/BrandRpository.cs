﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Core.Dto.Brand;
using IGenericControlPanel.Core.Dto.Product;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class BrandRpository : BasicRepository<CPDbContext, Brand>, IBrandRepository
    {
        public BrandRpository(CPDbContext Context) : base(Context)
        { }

        #region Get
        public async Task<OperationResult<GenericOperationResult, BrandDto>> GetBrandByIdAsync(int BrandId)
        {
            OperationResult<GenericOperationResult, BrandDto> result = new(GenericOperationResult.Failed);
            try
            {
                var BrandDto = await ValidEntities
                    .Where(entity => entity.Id == BrandId)
                    .Select(entity => new BrandDto()
                    {
                        Id = entity.Id,
                        Name = entity.Name,
                        Products = entity.Products.Select(product => new ProductDto()
                        {
                            Id = product.Id,
                            Name = product.Name,
                            CategoryId = product.CategoryId,
                            CategoryName = product.Category.Name,
                        })
                    }).SingleOrDefaultAsync();

                return BrandDto == null
                    ? result.AddError(ErrorMessages.BrandNotExist).UpdateResultStatus(GenericOperationResult.NotFound)
                    : result.UpdateResultData(BrandDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<BrandDto>>> GetAllBrandsAsync()
        {
            OperationResult<GenericOperationResult, IEnumerable<BrandDto>> result = new(GenericOperationResult.Failed);
            try
            {
                List<BrandDto> Brands = await ValidEntities
               .Select(entity => new BrandDto
               {
                   Id = entity.Id,
                   Name = entity.Name,
                   Products = entity.Products.Select(product => new ProductDto()
                   {
                       Id = product.Id,
                       Name = product.Name,
                       CategoryId = product.CategoryId,
                       CategoryName = product.Category.Name,
                   })
               }).ToListAsync();

                return result.UpdateResultData(Brands).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, IEnumerable<BrandDto>>> GetBrandsAsync(int pageNumber = 0, int pageSize = 5)
        {
            OperationResult<GenericOperationResult, IEnumerable<BrandDto>> result = new(GenericOperationResult.Failed);
            try
            {
                var BrandList = await ValidEntities
                    .Skip(pageNumber * pageSize)
                    .Take(pageSize)
                    .Select(entity => new BrandDto()
                    {
                        Id = entity.Id,
                        Name = entity.Name,
                        Products = entity.Products
                        .Select(product => new ProductDto()
                        {
                            Id = product.Id,
                            Name = product.Name,
                            CategoryId = product.CategoryId,
                            CategoryName = product.Category.Name,
                        })
                    }).ToListAsync();

                return result.UpdateResultData(BrandList).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Action
        public async Task<OperationResult<GenericOperationResult, BrandDto>> ActionBrandAsync(
            BrandFormDto formDto)
        {
            OperationResult<GenericOperationResult, BrandDto> result = new(GenericOperationResult.ValidationError);

            if (!string.IsNullOrEmpty(formDto.Name))
                return result.AddError(ErrorMessages.BrandNameRequired);

            try
            {
                Brand BrandEntity = await ValidEntities
                    .Where(entity => entity.Id == formDto.Id)
                    .SingleOrDefaultAsync();

                if (BrandEntity != null)
                    BrandEntity.Name = formDto.Name;
                
                else
                {
                    BrandEntity = new Brand()
                    {
                        Name = formDto.Name
                    };
                    await Context.AddAsync(BrandEntity);
                }

                await Context.SaveChangesAsync();

                var brandDto = new BrandDto
                {
                    Id = BrandEntity.Id,
                    Name = formDto.Name,
                };

                return result.UpdateResultData(brandDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError).UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        #endregion

        #region Remove
        public async Task<OperationResult<GenericOperationResult>> RemoveBrandAsync(int BrandId)
        {
            OperationResult<GenericOperationResult> result = new(GenericOperationResult.Failed);
            try
            {
                var brandEntity = await ValidEntities
                    .Where(entity => entity.Id == BrandId)
                    .SingleOrDefaultAsync();

                if (brandEntity == null)
                    return result.AddError(ErrorMessages.BrandNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                Context.Remove(brandEntity);
                await Context.SaveChangesAsync();
                return result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion
    }
}
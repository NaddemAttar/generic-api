﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Core.Dto.BlogComment;
using IGenericControlPanel.Core.IData.Interfaces;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Core.Data.Repositories
{
    public class BlogCommentRepository : BasicRepository<CPDbContext, BlogComment>, IBlogCommentRepository
    {
        private IUserRepository userRepository { get; }
        public BlogCommentRepository(CPDbContext context, IUserRepository userRepository) : base(context)
        {
            this.userRepository = userRepository;
        }
        #region Action
        public async Task<OperationResult<GenericOperationResult, BlogCommentDto>> AddComment(BlogCommentDto formDto)
        {
            OperationResult<GenericOperationResult, BlogCommentDto> result = new(GenericOperationResult.Failed);
            try
            {
                var blog = await Context.Blogs.Where(entity => entity.IsValid && entity.Id == formDto.BlogId).SingleOrDefaultAsync();
                if (blog is null)
                    return result.AddError(ErrorMessages.BlogNotFound).UpdateResultStatus(GenericOperationResult.NotFound);

                var blogComment = new BlogComment()
                {
                    Blog = blog,
                    CommentContent = formDto.CommentContent,
                    CPUserId = userRepository.CurrentUserIdentityId(),
                    CreatedBy = userRepository.CurrentUserIdentityId(),
                    CreationDate = DateTime.Now,
                };
                await Context.BlogComments.AddAsync(blogComment);
                await Context.SaveChangesAsync();
                formDto.Id = blogComment.Id;
                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(formDto);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, BlogCommentDto>> UpdateComment(BlogCommentDto formDto)
        {
            OperationResult<GenericOperationResult, BlogCommentDto> result = new(GenericOperationResult.Failed);
            try
            {
                var blogComment = await Context.BlogComments
                    .Where(entity => entity.IsValid && entity.Id == formDto.Id)
                    .SingleOrDefaultAsync();

                blogComment.CommentContent = formDto.CommentContent;
                Context.BlogComments.Update(blogComment);
                await Context.SaveChangesAsync();
                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(formDto);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion
        #region Get
        public async Task<OperationResult<GenericOperationResult, IEnumerable<BlogCommentDto>>> GetBlogComments(int BlogId)
        {
            OperationResult<GenericOperationResult, IEnumerable<BlogCommentDto>> result = new(GenericOperationResult.Failed);
            try
            {
                var blogComments = await Context.BlogComments.Where(bc => bc.IsValid && bc.BlogId == BlogId)
                    .Select(bc => new BlogCommentDto()
                    {
                        Id = bc.Id,
                        BlogId = bc.BlogId,
                        CommentContent = bc.CommentContent,
                        ImageUrl = bc.ImageUrl,
                    }).ToListAsync();
                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(blogComments);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, BlogComment>> GetComment(int commentId)
        {
            OperationResult<GenericOperationResult, BlogComment> result = new(GenericOperationResult.Failed);
            try
            {
                var blogComment = await Context.BlogComments
                    .Where(entity => entity.IsValid && entity.Id == commentId)
                    .SingleOrDefaultAsync();

                if (blogComment is null)
                    return result.AddError(ErrorMessages.BlogCommentNotFound).UpdateResultStatus(GenericOperationResult.NotFound);

                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(blogComment);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, IEnumerable<BlogCommentDto>>> GetUserComments(int UserId)
        {
            OperationResult<GenericOperationResult, IEnumerable<BlogCommentDto>> result = new(GenericOperationResult.ValidationError);
            try
            {
                var blogComments = await Context.BlogComments.Where(bc => bc.IsValid && bc.CreatedBy == UserId).
                    Select(bc => new BlogCommentDto()
                    {
                        Id = bc.Id,
                        BlogId = bc.BlogId,
                        CommentContent = bc.CommentContent,
                        ImageUrl = bc.ImageUrl,
                    }).ToListAsync();
                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(blogComments);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion
        #region Remove
        public async Task<OperationResult<GenericOperationResult, BlogComment>> RemoveComment(int CommentId)
        {
            OperationResult<GenericOperationResult, BlogComment> result = new(GenericOperationResult.Failed);
            try
            {
                var userInfo = await userRepository.GetCurrentUserInfo();
                var blogComment = await Context.BlogComments
                    .Where(entity => entity.IsValid && entity.Id == CommentId).SingleOrDefaultAsync();

                if (blogComment is null)
                    return result.AddError(ErrorMessages.BlogCommentNotFound).UpdateResultStatus(GenericOperationResult.NotFound);

                if (blogComment.CreatedBy == userRepository.CurrentUserIdentityId() || userInfo.IsUserAdmin)
                {
                    Context.BlogComments.Remove(blogComment);
                    await Context.SaveChangesAsync();
                    return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(blogComment);
                }
                else
                {
                    return result.UpdateResultStatus(GenericOperationResult.Failed)
                        .AddError(ErrorMessages.YouDoNotHavePermissionToEdit);
                }
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion
    }
}
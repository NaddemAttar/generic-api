﻿using IGenericControlPanel.Communications.Dto;

namespace IGenericControlPanel.Communications.IData
{
    public interface IContactRepository
    {
        bool SubmitContactForm(ContactFormDto contactForm);
    }
}

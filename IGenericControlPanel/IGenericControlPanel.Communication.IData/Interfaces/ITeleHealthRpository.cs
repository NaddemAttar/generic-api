﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Communications.Dto.TeleHealth;
using IGenericControlPanel.Core.Dto.HealthCareProvider;

namespace IGenericControlPanel.Communications.IData.Interfaces
{
    public interface ITeleHealthRpository
    {
        Task<OperationResult<GenericOperationResult, BookingSettingsDto>> SetHealthcareProviderBookingInfoAsync(
            BookingSettingsDto bookingSettingsDto);
        Task<OperationResult<GenericOperationResult, bool>> RemoveSessionAsync(int SessionId);
        Task<OperationResult<GenericOperationResult, bool>> SetHealthCareProviderSessionTypeAsync(
            SessionTypeDto sessionType);
        Task<OperationResult<GenericOperationResult, BookingSettingsDto>> GetbookingSettingsAsync();
        Task<OperationResult<GenericOperationResult, IEnumerable<SessionTypeDto>>> GetHealtCareProviderSessionTypesAsync(
            int? serviceId = null, int? HealthCareProviderId = null);
        Task<OperationResult<GenericOperationResult, BookingDto>> BookAppointmentAsync(
            BookingDto bookingDto);
        Task<OperationResult<GenericOperationResult, List<DateTime>>> GetAvailableAppointmentsAsync(
            DateTime Date, int UserServiceSessionId, int? HealthCareProviderId, int? EnterpriseId);
        Task<OperationResult<GenericOperationResult, IEnumerable<BookingDto>>> GetBookingsAsync(bool MyBookings);
        Task<OperationResult<GenericOperationResult>> RemoveAppointmentAsync(int AppointmentId);
        Task<OperationResult<GenericOperationResult, bool>> ConfirmingBookingAsync(int BookingId, string Token);
        Task<OperationResult<GenericOperationResult, bool>> RemoveBookingAsync(int BookingId, string Token);
        Task<OperationResult<GenericOperationResult>> AddServicesForHcpInEnterpriseAsync(int userId, List<AddServicesForHcpDto> services);
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;

using CraftLab.Core.OperationResults;

using IGenericControlPanel.Communications.Dto.Feedback;

namespace IGenericControlPanel.Communications.IData.Interfaces
{
    public interface IFeedbackRepository
    {
        Task<OperationResult<GenericOperationResult, IEnumerable<FeedbackDto>>> GetFeedbacks(int ServiceId, int pageSize = 6, int pageNumber = 0);
        OperationResult<GenericOperationResult, FeedbackDto> GetFeedback(int FeedbackId);
        Task<OperationResult<GenericOperationResult, FeedbackDto>> ActionFeedback(FeedbackDto formDto);
        Task<OperationResult<GenericOperationResult>> RemoveFeedback(int FeedbackId);
    }
}

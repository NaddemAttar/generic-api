﻿using CraftLab.Core.ExtensionMethods;
using CraftLab.Core.Infrastructure;
using CraftLab.Core.OperationResults;
using IGenericControlPanel.Model.Core;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.Dto;
using IGenericControlPanel.Security.Dto.External.Facebook;
using IGenericControlPanel.Security.Dto.External.Google;
using IGenericControlPanel.Security.Dto.User;
using IGenericControlPanel.Security.IData;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Enums.Security;
using IGenericControlPanel.SharedKernal.Extension;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace IGenericControlPanel.Security.Data.Repositories
{
    public class AccountRepository : BasicRepository<CPDbContext, CPUser>, IAccountRepository
    {
        #region Properties && Constructor
        private UserManager<CPUser> UserManager { get; }
        private readonly RoleManager<CPRole> RoleManager;
        private SignInManager<CPUser> SignInManager { get; }
        private IPasswordHasher<CPUser> PasswordHasher { get; }
        private IConfiguration Configuration { get; }
        private readonly IHttpClientFactory httpClientFactory;
        private IUserRepository UserRepository { get; }

        public AccountRepository(CPDbContext context,
                                 UserManager<CPUser> userManager,
                                 SignInManager<CPUser> signInManager,
                                 RoleManager<CPRole> roleManager,
                                 IPasswordHasher<CPUser> passwordHasher,
                                 IConfiguration configuration,
                                 IHttpClientFactory _httpClientFactory,
                                 IUserRepository userRepository) : base(context)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            RoleManager = roleManager;
            PasswordHasher = passwordHasher;
            Configuration = configuration;
            httpClientFactory = _httpClientFactory;
            UserRepository = userRepository;
        }
        private const string FacebookTokenValidationUrl = "https://graph.facebook.com/v13.0/debug_token?input_token={0}&access_token={1}|{2}";
        private const string FacebookUserInfoUrl = "https://graph.facebook.com/v13.0/me?fields=first_name,email,last_name,picture,id&access_token={0}";
        private const string GoogleUserInfoUrl = "https://www.googleapis.com/oauth2/v3/userinfo";
        private const string GoogleTokenUrl = "https://oauth2.googleapis.com/token";
        #endregion

        #region Login && Logout
        public async Task<OperationResult<GenericLoginResult, UserDto>> Login(UserDto loginDto)
        {
            try
            {
                (bool isValid, ICollection<System.ComponentModel.DataAnnotations.ValidationResult> validationResults) = loginDto.ValidateObject();
                if (!isValid)
                {
                    return new OperationResult<GenericLoginResult, UserDto>(GenericLoginResult.ValidationError)
                    {
                        ValidationResults = validationResults
                    };
                }
                CPUser userEntity = GetUser(loginDto.UserName, loginDto.Email);
                UserDto userDto = await GetUserDto(loginDto.UserName, loginDto.Email);
                if (userEntity == null || !await UserManager.CheckPasswordAsync(userEntity, loginDto.Password))
                {
                    return new OperationResult<GenericLoginResult, UserDto>
                        (GenericLoginResult.WrongUsernameOrPassword)
                        .AddError("Wrong username or password");
                }
                await SignInManager.SignInAsync(userEntity, loginDto.RememberMe);

                UserDto userInfoDto = new()
                {
                    Id = userEntity.Id,
                    UserName = userEntity.UserName,
                    Email = userEntity.Email,
                    IsValid = userEntity.IsValid,
                    RoleName = (await UserManager.GetRolesAsync(userEntity)).SingleOrDefault(),
                    FirstName = userEntity.FirstName,
                    LastName = userEntity.LastName,
                    Birthdate = userEntity.Birthdate,
                    Gender = userEntity.Gender,
                    ProfilePhotoUrl = userEntity.ProfilePhotoUrl,
                };
                return new OperationResult<GenericLoginResult, UserDto>
                    (GenericLoginResult.Success, userInfoDto);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<OperationResult<GenericLoginResult, UserDto>> ExternalLogin(UserDto loginDto)
        {
            try
            {
                (bool isValid, ICollection<System.ComponentModel.DataAnnotations.ValidationResult> validationResults) = loginDto.ValidateObject();
                if (!isValid)
                {
                    return new OperationResult<GenericLoginResult, UserDto>(GenericLoginResult.ValidationError)
                    { ValidationResults = validationResults }
                    .AddError(ErrorMessages.WrongPasswordOrUsername);
                }

                CPUser userEntity = GetUser(loginDto.UserName, loginDto.Email);
                await SignInManager.SignInAsync(userEntity, loginDto.RememberMe);
                //need to check about the user if he login successfully.
                UserDto userInfoDto = new()
                {
                    Id = userEntity.Id,
                    UserName = userEntity.UserName,
                    Email = userEntity.Email,
                    IsValid = userEntity.IsValid,
                    RoleName = (await UserManager.GetRolesAsync(userEntity)).SingleOrDefault(),
                    FirstName = userEntity.FirstName,
                    LastName = userEntity.LastName,
                    Birthdate = userEntity.Birthdate,
                    Gender = userEntity.Gender,
                    PhoneNumber = userEntity.PhoneNumber,
                    ProfilePhotoUrl = userEntity.ProfilePhotoUrl
                };
                return new OperationResult<GenericLoginResult, UserDto>
                    (GenericLoginResult.Success, userInfoDto);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public async Task Logout()
        {
            await SignInManager.SignOutAsync();
        }
        #endregion

        #region Get Users Section
        private CPUser GetUser(string user)
        {
            return GetUser(user, user);
        }

        /// <summary>
        /// Used to get user by email or username
        /// return null in case the user does not exists
        /// </summary>
        /// <param name="user">email or username</param>
        /// <returns></returns>
        private CPUser GetUser(string username, string email)
        {
            return ValidEntities
                       .SingleOrDefault(user =>

                                    (!string.IsNullOrEmpty(username)
                                    &&
                                    user.UserName == username)

                            ||

                                    (!string.IsNullOrEmpty(email)
                                    &&
                                    user.Email == email)

                            );
        }

        public async Task<UserDto> GetUserDto(string Username, string Email)
        {
            UserDto UserDto = ValidEntities.Where(user =>

                                    !string.IsNullOrEmpty(Username)
                                    &&
                                    user.UserName == Username

                            ||

                                    !string.IsNullOrEmpty(Email)
                                    &&
                                    user.Email == Email

                            ).Select(userEntity => new UserDto()
                            {
                                Id = userEntity.Id,
                                UserName = userEntity.UserName,
                                Email = userEntity.Email,
                                IsValid = userEntity.IsValid,
                                FirstName = userEntity.FirstName ?? null,
                                LastName = userEntity.LastName ?? null,
                            }).SingleOrDefault();
            return UserDto;
        }

        public async Task<OperationResult<GenericOperationResult, IEnumerable<UserDto>>> GetAllUsers(int? usresType)
        {
            try
            {   // usersTypes == 0 : to get admins and Modifers.
                // usersTypes == 1 : to get NormalUsers.
                // usersTypes == 2 : to get CpUsers.
                List<UserDto> result = await Context.Users
                    .Where(user => usresType == 0 ? user.UserRoles.Any(ur => ur.RoleId == 1 || ur.RoleId == 2)
                    : usresType == 1 ? user.UserRoles.Any(ur => ur.RoleId == 3)
                    : usresType == 2 ? user.UserRoles.Any(ur => ur.RoleId == 4) : true)
                    .Select(user => new UserDto()
                    {
                        Email = user.Email,
                        Id = user.Id,
                        PhoneNumber = user.PhoneNumber,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        ProfilePhotoUrl = user.ProfilePhotoUrl,
                        UserName = user.UserName,
                        IsValid = user.IsValid,
                        HealthCareProviderSpecialties = user.HealthCareProviderSpecialties.Where(entity => entity.IsValid).Select(
                        hcs => new HealthCareProviderSpecialtyDto()
                        {
                            HealthCareProviderId = hcs.HealthCareSpecialtyId,
                            HealthCareProviderName = hcs.HealthCareSpecialty.Name
                        }).ToList(),
                        Services = user.EnterpriseServices.Where(ser => ser.IsValid).Select(ser => new ServiceDto()
                        {
                            Id = ser.Service.Id,
                            Name = ser.Service.Name,
                            Description = ser.Service.Description
                        })
                    }).ToListAsync();
                foreach (UserDto item in result)
                {
                    string roleName = (await UserManager.GetRolesAsync(Context.Users.Where(user => user.Id == item.Id).SingleOrDefault())).FirstOrDefault();
                    item.isHealthCareProvider = roleName != Role.NormalUser.ToString();
                    int roleId = Context.Roles.Where(rrole => rrole.Name == roleName).SingleOrDefault().Id;
                    item.RoleName = roleName;
                    item.Role = new RoleDto()
                    {
                        Id = roleId,
                        Name = roleName
                    };
                }
                return new OperationResult<GenericOperationResult, IEnumerable<UserDto>>(GenericOperationResult.Success, result);
            }
            catch (Exception)
            {
                return new OperationResult<GenericOperationResult, IEnumerable<UserDto>>(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }

        #endregion

        #region Action Normal User
        public async Task<OperationResult<GenericOperationResult, UserFormDto>> ActionNormalUser(UserFormDto userFormDto)
        {
            OperationResult<GenericOperationResult, UserFormDto> result = userFormDto.Id == 0 ? await CreateNormalUser(userFormDto) : await UpdateNormalUser(userFormDto);
            return result;
        }
        #region Create Normal User
        public async Task<OperationResult<GenericOperationResult, UserFormDto>> CreateNormalUser(UserFormDto userFormDto)
        {
            OperationResult<GenericOperationResult, UserFormDto> result = new(GenericOperationResult.ValidationError);
            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    IdentityResult identityResult = new();

                    CPUser User = new()
                    {
                        UserName = userFormDto.UserName,
                        Email = userFormDto.Email,
                        EmailConfirmed = true,
                        PhoneNumber = userFormDto.PhoneNumber,
                        PhoneNumberConfirmed = true,
                        FirstName = userFormDto.FirstName,
                        LastName = userFormDto.LastName,
                        Birthdate = userFormDto.Birthdate,
                        Gender = userFormDto.Gender,
                        ProfilePhotoUrl = userFormDto.ProfilePhotoUrl == null ? null : userFormDto.ProfilePhotoUrl,
                    };
                    CPUser CheckUser = await UserManager.FindByNameAsync(userFormDto.UserName);
                    CPUser CheckUserByEmail = await UserManager.FindByEmailAsync(userFormDto.Email);

                    if (CheckUser is not null)
                        return result.UpdateResultStatus(GenericOperationResult.ValidationError).AddError(ErrorMessages.UsernameExist);

                    if (CheckUserByEmail is not null)
                        return result.UpdateResultStatus(GenericOperationResult.ValidationError).AddError(ErrorMessages.EmailExists);

                    identityResult = userFormDto.Password == null ? await UserManager.CreateAsync(User) : await UserManager.CreateAsync(User, userFormDto.Password);
                    _ = await Context.SaveChangesAsync();

                    if (identityResult == IdentityResult.Success)
                    {
                        try
                        {
                            CPUser user = await UserManager.FindByNameAsync(userFormDto.UserName);
                            IdentityResult roleForUser = await UserManager.AddToRoleAsync(user, Role.NormalUser.ToString());
                            transaction.Commit();
                        }
                        catch (Exception)
                        {

                            transaction.Rollback();
                            return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
                        }
                    }
                    else
                    {
                        userFormDto.ErrorMessage = identityResult.Errors.ElementAt(0).Code;
                    }
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
                }
            }


            return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(userFormDto);
        }
        #endregion

        #region Update Normal User
        public async Task<OperationResult<GenericOperationResult, UserFormDto>> UpdateNormalUser(UserFormDto userFormDto)
        {
            OperationResult<GenericOperationResult, UserFormDto> result = new(GenericOperationResult.ValidationError);
            var transaction = Context.Database.BeginTransaction();

            try
            {
                var PersonEntity = await Context.Users.Where(entity => entity.IsValid && entity.Id == userFormDto.Id).SingleOrDefaultAsync();
                if (PersonEntity == null)
                    return result.AddError(ErrorMessages.UserNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                CPUser CheckUserByEmail = await UserManager.FindByEmailAsync(userFormDto.Email);
                if (CheckUserByEmail is not null && CheckUserByEmail.Id != userFormDto.Id)
                    return result.UpdateResultStatus(GenericOperationResult.ValidationError).AddError(ErrorMessages.EmailExists);

                PersonEntity.Email = userFormDto.Email;
                PersonEntity.Birthdate = userFormDto.Birthdate;
                PersonEntity.FirstName = userFormDto.FirstName;
                PersonEntity.LastName = userFormDto.LastName;
                PersonEntity.Gender = userFormDto.Gender;
                // PersonEntity.UserName = userFormDto.UserName;
                PersonEntity.ProfilePhotoUrl = userFormDto.ProfilePhotoUrl == null ? PersonEntity.ProfilePhotoUrl : userFormDto.ProfilePhotoUrl;
                transaction.Commit();
                await Context.SaveChangesAsync();
                return result.UpdateResultData(userFormDto).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #endregion

        #region Action CP User
        public async Task<OperationResult<GenericOperationResult, UserFormDto>> ActionCPUser(CPUserRegistrationDto userFormDto)

        {
            var userRole = await UserRepository.CurrentUserRole();

            OperationResult<GenericOperationResult, UserFormDto> result = userFormDto.Id == 0 ? await CreateCPUser(userFormDto, userRole)
                : await UpdateCPUser(userFormDto, userRole);

            return result;
        }

        #region Create CP User
        public async Task<OperationResult<GenericOperationResult, UserFormDto>> CreateCPUser(CPUserRegistrationDto userFormDto, CPRole role)
        {
            OperationResult<GenericOperationResult,
                UserFormDto> result = new(GenericOperationResult.ValidationError);

            if (role.Name != EnumsExtensionMethods.RoleToString(Role.Admin))
            {
                return result.AddError(ErrorMessages.OnlyAdminCanCreateAccount)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }
            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    IdentityResult identityResult = new();

                    CPUser User = new()
                    {
                        UserName = userFormDto.UserName,
                        FirstName = userFormDto.FirstName,
                        LastName = userFormDto.LastName,
                        Email = userFormDto.Email,
                        EmailConfirmed = true,
                        HcpType = userFormDto.HcpType,
                        PhoneNumber = userFormDto.PhoneNumber,
                        ProfilePhotoUrl = userFormDto.ProfilePhotoUrl == null ? null : userFormDto.ProfilePhotoUrl,
                    };
                    CPUser CheckUser = await UserManager.FindByNameAsync(userFormDto.UserName);
                    CPUser CheckUserByEmail = await UserManager.FindByEmailAsync(userFormDto.Email);
                    if (CheckUser is not null)
                        return result.UpdateResultStatus(GenericOperationResult.ValidationError).AddError(ErrorMessages.UsernameExist);

                    if (CheckUser is not null)
                        return result.UpdateResultStatus(GenericOperationResult.ValidationError).AddError(ErrorMessages.EmailExists);

                    identityResult = await UserManager.CreateAsync(User, userFormDto.Password);
                    if (identityResult == IdentityResult.Success)
                    {
                        CPUser user = await UserManager.FindByNameAsync(userFormDto.UserName);
                        IdentityResult roleForUser = await UserManager.AddToRoleAsync(user, userFormDto.Role.Name);
                        transaction.Commit();
                    }
                    else
                        throw new Exception();

                    foreach (var hcsId in userFormDto.HealthCareSpecialtyIds)
                    {
                        await Context.HealthCareProviderSpecialties.AddAsync(new HealthCareProviderSpecialty()
                        {
                            CPUser = User,
                            HealthCareSpecialtyId = hcsId,
                        });
                    }
                    foreach (var serviceId in userFormDto.ServicesIds)
                    {
                        await Context.HealthCareProviderServices.AddAsync(new HealthCareProviderService()
                        {
                            CPUser = User,
                            ServiceId = serviceId
                        });
                    }
                    await Context.SaveChangesAsync();

                    foreach (var day in Enum.GetValues(typeof(DayOfWeek)).Cast<DayOfWeek>())
                    {
                        var openningHoures = new OpenningHouresSet()
                        {
                            CreatedBy = UserRepository.CurrentUserIdentityId(),
                            UserId = User.Id,
                            Day = day,
                            IsValid = true,
                            IsWorkingDay = false,
                            LastUpdateDate = DateTime.Now,
                            CreationDate = DateTime.Now,
                            LastUpdatedBy = UserRepository.CurrentUserIdentityId(),
                            From = TimeSpan.Zero,
                            To = TimeSpan.Zero,
                            HealthCareProviderEnterpriseId = null
                        };
                        await Context.OpenningHoures.AddAsync(openningHoures);
                    }
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
                }
            }
            return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(userFormDto);
        }
        #endregion

        #region Update CP User
        public async Task<OperationResult<GenericOperationResult, UserFormDto>> UpdateCPUser(CPUserRegistrationDto userFormDto, CPRole role)
        {
            OperationResult<GenericOperationResult, UserFormDto> result = new(GenericOperationResult.ValidationError);
            var transaction = Context.Database.BeginTransaction();
            try
            {
                CPUser user = await UserManager.FindByIdAsync(userFormDto.Id.ToString());
                if (user != null)
                {
                    CPUser CheckUserByEmail = await UserManager.FindByEmailAsync(userFormDto.Email);

                    if (CheckUserByEmail is not null && CheckUserByEmail.Id != user.Id)
                        return result.UpdateResultStatus(GenericOperationResult.ValidationError).AddError(ErrorMessages.EmailExists);

                    user.FirstName = userFormDto.FirstName;
                    user.LastName = userFormDto.LastName;
                    user.Email = userFormDto.Email;
                    user.PhoneNumber = userFormDto.PhoneNumber;
                    user.ProfilePhotoUrl = userFormDto.ProfilePhotoUrl == null ? user.ProfilePhotoUrl : userFormDto.ProfilePhotoUrl;

                    Context.HealthCareProviderSpecialties.RemoveRange
                       (await Context.HealthCareProviderSpecialties.Where(entity => entity.IsValid && entity.CPUserId == user.Id).ToListAsync());

                    Context.HealthCareProviderServices.RemoveRange(await Context.HealthCareProviderServices
                        .Where(ser => ser.IsValid && ser.CPUser == user).ToListAsync());

                    await Context.SaveChangesAsync();

                    foreach (var hcsId in userFormDto.HealthCareSpecialtyIds)
                    {
                        await Context.HealthCareProviderSpecialties.AddAsync(new HealthCareProviderSpecialty()
                        {
                            CPUser = user,
                            HealthCareSpecialtyId = hcsId,
                        });
                    }
                    foreach (var serviceId in userFormDto.ServicesIds)
                    {
                        await Context.HealthCareProviderServices.AddAsync(new HealthCareProviderService()
                        {
                            CPUser = user,
                            ServiceId = serviceId
                        });
                    }
                    await Context.SaveChangesAsync();
                    if (role.Name == EnumsExtensionMethods.RoleToString(Role.Admin))
                    {
                        string roleName = GetRoleName(userFormDto);
                        var removeOldRole = await UserManager.RemoveFromRoleAsync(user, roleName);
                        var roleResult = await UserManager.AddToRoleAsync(user, userFormDto.Role.Name);
                    }
                    IdentityResult userResult = await UserManager.UpdateAsync(user);
                    if (!userResult.Succeeded)
                        throw new Exception();

                }
                else
                    return result.AddError(ErrorMessages.UserNotExist).UpdateResultStatus(GenericOperationResult.NotFound);

                await Context.SaveChangesAsync();
                transaction.Commit();
                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(userFormDto);
            }
            catch (Exception)
            {
                await transaction.RollbackAsync();
                return result.AddError(ErrorMessages.InternalServerError).UpdateResultStatus(GenericOperationResult.Failed);
            }
        }
        public string GetRoleName(CPUserRegistrationDto userFormDto)
        {
            var oldRoleId = Context.UserRoles
                    .Where(userRole => userRole.UserId == userFormDto.Id)
                    .SingleOrDefault()
                    .RoleId;

            var oldRoleName = Context.Roles
                .Where(role => role.Id == oldRoleId)
                .SingleOrDefault().Name;

            return oldRoleName;
        }

        #endregion 

        #endregion
        #region Add User To Enterprise
        public async Task<OperationResult<GenericOperationResult, bool>> AddCpUserToEnterprise(List<int> usersId, int enterpriseId)
        {
            var transaction = await Context.Database.BeginTransactionAsync();
            var result = new OperationResult<GenericOperationResult, bool>();
            try
            {
                foreach (var userId in usersId)
                {
                    var userModel = await Context.Users.FirstOrDefaultAsync(u => u.Id == userId);
                    if (userModel == null)
                        return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.UserNotExist);

                    var userEnterpriseModel = new HealthCareProviderEnterprise()
                    {
                        UserId = userId,
                        EnterpriseId = enterpriseId,
                        CreatedBy = UserRepository.CurrentUserIdentityId(),
                        LastUpdatedBy = UserRepository.CurrentUserIdentityId(),
                        CreationDate = DateTime.Now,
                        LastUpdateDate = DateTime.Now,
                        IsValid = true
                    };
                    await Context.HealthCareProviderEnterprise.AddAsync(userEnterpriseModel);
                    await Context.SaveChangesAsync();

                    foreach (var day in Enum.GetValues(typeof(DayOfWeek)).Cast<DayOfWeek>())
                    {
                        var openningHoures = new OpenningHouresSet()
                        {
                            CreatedBy = UserRepository.CurrentUserIdentityId(),
                            HealthCareProviderEnterpriseId = userEnterpriseModel.Id,
                            Day = day,
                            IsValid = true,
                            IsWorkingDay = false,
                            LastUpdateDate = DateTime.Now,
                            CreationDate = DateTime.Now,
                            LastUpdatedBy = UserRepository.CurrentUserIdentityId(),
                            From = TimeSpan.Zero,
                            To = TimeSpan.Zero,
                            UserId = null,
                        };
                        await Context.OpenningHoures.AddAsync(openningHoures);
                    }
                }
                await Context.SaveChangesAsync();
                await transaction.CommitAsync();
                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(true);
            }
            catch (Exception)
            {
                await transaction.RollbackAsync();
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }

        #endregion

        #region Remove Account Soft Or Hard
        public async Task<OperationResult<GenericOperationResult>> RemoveNormalUser(int userId,
            RemoveAccountType removeAccountType)
        {
            using (var transaction = await Context.Database.BeginTransactionAsync())
            {
                try
                {
                    CPUser user = await UserManager.FindByIdAsync(userId.ToString());

                    if (removeAccountType == RemoveAccountType.HardRemove)
                    {
                        Context.ProductReviews.RemoveRange(Context.ProductReviews.Where(entity => entity.IsValid && entity.UserId == user.Id).ToList());
                        Context.UserCourses.RemoveRange(Context.UserCourses.Where(entity => entity.IsValid && entity.UserId == user.Id).ToList());
                        Context.JobApplications.RemoveRange(Context.JobApplications.Where(entity => entity.IsValid && entity.UserId == user.Id).ToList());
                        //Context.PatientAllergies.RemoveRange(Context.PatientAllergies.Where(entity => entity.IsValid && entity.PersonId == person.Id).ToList());
                        Context.Order.RemoveRange(Context.Order.Where(entity => entity.IsValid && entity.UserId == user.Id).ToList());
                        //Context.PatientMedicines.RemoveRange(Context.PatientMedicines.Where(entity => entity.IsValid && entity.PersonId == person.Id).ToList());
                        //Context.PatientSurgeries.RemoveRange(Context.PatientSurgeries.Where(entity => entity.IsValid && entity.PersonId == person.Id).ToList());
                        //Context.PatientsDiseases.RemoveRange(Context.PatientsDiseases.Where(entity => entity.IsValid && entity.PersonId == person.Id).ToList());
                        Context.MedicalTests.RemoveRange(Context.MedicalTests.Where(entity => entity.IsValid && entity.UserId == user.Id).ToList());
                        _ = Context.Users.Remove(user); //TODO -  remove all person related entities before deleting the person
                        user.ForceDelete = true;
                        Context.UserRoles
                       .RemoveRange(Context.UserRoles.Where(userRole => userRole.UserId == userId)
                       .ToList());
                        user.ForceDelete = true;
                        _ = Context.Users.Remove(user);
                    }
                    else
                        user.IsValid = false;

                    await Context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return new OperationResult<GenericOperationResult>(GenericOperationResult.Success);
                }
                catch (Exception)
                {
                    await transaction.RollbackAsync();
                    return new OperationResult<GenericOperationResult>(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
                }
            }
        }

        #endregion

        #region Get All Roles
        public OperationResult<GenericOperationResult, IEnumerable<RoleDto>> GetRoles()
        {
            var roles = Context.Roles
                .Where(roleEntity => roleEntity.IsValid)
                .Select(roleEntity => new RoleDto
                {
                    Id = roleEntity.Id,
                    Name = roleEntity.Name
                }).ToList();
            return new OperationResult<GenericOperationResult, IEnumerable<RoleDto>>(GenericOperationResult.Success, roles);
        }


        #endregion

        #region Get User Details
        public OperationResult<GenericOperationResult, UserDto> GetUserDetails(int userId)
        {
            var userDetails = Context.Users
                .Where(userEntity => userEntity.IsValid && userEntity.Id == userId)
                .Select(userEntity => new UserDto()
                {
                    Id = userEntity.Id,
                    Email = userEntity.Email,
                    FirstName = userEntity.FirstName,
                    LastName = userEntity.LastName,
                    PhoneNumber = userEntity.PhoneNumber,
                }).SingleOrDefault();

            return new OperationResult<GenericOperationResult, UserDto>(GenericOperationResult.Success, userDetails);
        }

        public async Task<OperationResult<GenericOperationResult>> ActionArchive(int userId, bool Activate)
        {
            using (var transaction = await Context.Database.BeginTransactionAsync())
            {
                try
                {
                    CPUser user = await UserManager.FindByIdAsync(userId.ToString());
                    if (user == null)
                        return new OperationResult<GenericOperationResult>(GenericOperationResult.NotFound).AddError(ErrorMessages.NoteNotExist);

                    user.IsValid = Activate;

                    await Context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return new OperationResult<GenericOperationResult>(GenericOperationResult.Success);
                }
                catch (Exception)
                {
                    await transaction.RollbackAsync();
                    return new OperationResult<GenericOperationResult>(GenericOperationResult.Failed);
                }
            }
        }

        #endregion

        #region External login

        public async Task<OperationResult<GenericOperationResult, FacebookTokenValidationResult>> ValidateFacebookAccessToken(string accessToken)
        {
            OperationResult<GenericOperationResult, FacebookTokenValidationResult> result = new(GenericOperationResult.Failed);
            string formattedUrl = string.Format(FacebookTokenValidationUrl, accessToken, Configuration.GetValue<string>("FacebookAppId"), Configuration.GetValue<string>("FacebookAppSecret"));
            try
            {
                HttpResponseMessage httpResult = await httpClientFactory.CreateClient().GetAsync(formattedUrl);
                _ = httpResult.EnsureSuccessStatusCode();
                string responseAsString = await httpResult.Content.ReadAsStringAsync();
                FacebookTokenValidationResult facebookTokenValidationResult = JsonConvert.DeserializeObject<FacebookTokenValidationResult>(responseAsString);
                return result.UpdateResultData(facebookTokenValidationResult).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }

        }
        public async Task<OperationResult<GenericOperationResult, FacebookUserInfoResult>> GetFacebookUserInfo(string accessToken)
        {
            OperationResult<GenericOperationResult, FacebookUserInfoResult> result = new(GenericOperationResult.Failed);
            string formattedUrl = string.Format(FacebookUserInfoUrl, accessToken);
            try
            {
                HttpResponseMessage httpResult = await httpClientFactory.CreateClient().GetAsync(formattedUrl);
                _ = httpResult.EnsureSuccessStatusCode();
                string responseAsString = await httpResult.Content.ReadAsStringAsync();
                FacebookUserInfoResult FacebookUserInfoResult = JsonConvert.DeserializeObject<FacebookUserInfoResult>(responseAsString);
                return result.UpdateResultData(FacebookUserInfoResult).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }

        public async Task<OperationResult<GenericOperationResult, GoogleUserInfo>> GetGoogleUserInfo(string accessToken)
        {
            OperationResult<GenericOperationResult, GoogleUserInfo> result = new(GenericOperationResult.Failed);
            try
            {
                HttpClient httpclient = new();
                var contentDictionary = new Dictionary<string, string>
                {
                     { "code", accessToken },
                     { "client_id", Configuration.GetValue<string>("GoogleAppId") },
                     { "client_secret", Configuration.GetValue<string>("GoogleAppSecret") },
                     { "grant_type", "authorization_code" },
                     { "redirect_uri", Configuration.GetValue<string>("GoogleRedirectURI") },
                };
                HttpRequestMessage request = new()
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri(GoogleTokenUrl),
                    Headers =
                    {
                       { "accept", "*/*" },
                    },
                    Content = new FormUrlEncodedContent(contentDictionary),
                };
                using (HttpResponseMessage response = await httpclient.SendAsync(request))
                {
                    string body = await response.Content.ReadAsStringAsync();
                    try
                    {
                        _ = response.EnsureSuccessStatusCode();
                        accessToken = JsonConvert.DeserializeObject<GoogleTokenInfo>(body).access_token;
                    }
                    catch (Exception)
                    {
                        return result.UpdateResultStatus(GenericOperationResult.ValidationError).AddError(ErrorMessages.InvalidGoogleToken);
                        //_ = result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ex.Message).AddError(body);
                    }
                }
                using HttpClient client = new();
                GoogleUserInfo userinfo = new();
                //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

                HttpResponseMessage Res = await client.GetAsync(GoogleUserInfoUrl + "?access_token=" + accessToken);
                string info = await Res.Content.ReadAsStringAsync();
                if (Res.IsSuccessStatusCode)
                {
                    userinfo = JsonConvert.DeserializeObject<GoogleUserInfo>(info);
                }
                else
                {
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.UserNotExist);
                }
                return result.UpdateResultData(userinfo).UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }
        #endregion

        #region Get Normal Users
        public OperationResult<GenericOperationResult, IPagedList<NormalUserDto>> GetNormalUsers(
          int courseId, string qurey, IEnumerable<int> instructorIds, int locationId,
          IEnumerable<int> courseTypeIds, DateTime? startDate = null,
          DateTime? endDate = null, int? pageNumber = 0, int? pageSize = 4)
        {
            var result = new OperationResult<GenericOperationResult, IPagedList<NormalUserDto>>(GenericOperationResult.Failed);
            try
            {
                List<NormalUserForBladesDto> data = Context.Users
                .Include(entity => entity.Orders)
                .Include(entity => entity.UserRoles)
                .Include(entity => entity.UserCourses)
                .ThenInclude(entity => entity.Course)
                .ThenInclude(entity => entity.Instructors)
                .Where(entity => entity.UserRoles
                .Any(userRole => userRole.Role.Name == EnumsExtensionMethods.RoleToString(Role.NormalUser))
                && entity.IsValid && (courseId == 0
                || entity.UserCourses.Any(course => course.CourseId == courseId))
                && (instructorIds.Count() == 0
                || entity.UserCourses.Any(pc => pc.Course.Instructors
                .Any(instructor => instructorIds.Contains(instructor.TeamMemberId))))
                && (string.IsNullOrEmpty(qurey)
                || entity.FirstName.Contains(qurey)
                || entity.LastName.Contains(qurey))
                && (courseTypeIds.Count() == 0
                || entity.UserCourses.Any(pc => pc.Course.CourseTypeCourses
                .Any(type => courseTypeIds.Contains(type.CourseTypeId))))
                && (startDate == null
                && endDate == null
                || entity.UserCourses
                .Any(pc => pc.Course.StartDate > startDate)
                && entity.UserCourses
                .Any(pc => pc.Course.EndDate < endDate)
                && (locationId == 0
                || entity.UserCourses
                .Any(pc => pc.Course.LocationId == locationId))))
                .Select(entity =>
                new NormalUserForBladesDto()
                {
                    Id = entity.Id,
                    UserName = entity.UserName,
                    Email = entity.Email,
                    PhoneNumber = entity.PhoneNumber,
                    CoursesCount = entity.UserCourses.Where(pc => pc.Course.IsValid).Count(),
                    OrdersCount = entity.Orders
                    .Where(order => order.IsValid && order.UserId == entity.Id)
                    .Count()
                })
                .Skip(pageNumber.Value * pageSize.Value)
                .Take(pageSize.Value)
                .ToList();

                PagedList<NormalUserDto> x = new(data, pageNumber.Value, pageSize.Value);

                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(x);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }
        public async Task<OperationResult<GenericOperationResult, NormalUserDto>> GetprofileInfo(int userId)
        {
            try
            {
                userId = userId == 0 ? UserRepository.CurrentUserIdentityId() : userId;

                NormalUserDto data = await Context.Users
                    .Where(entity => entity.Id == userId)
                    .Select(entity =>
                    new NormalUserDto()
                    {
                        Id = entity.Id,
                        UserName = entity.UserName,
                        Email = entity.Email,
                        PhoneNumber = entity.PhoneNumber,
                        FirstName = entity.FirstName,
                        LastName = entity.LastName,
                        BirthDate = entity.Birthdate,
                        Gender = entity.Gender,
                        ProfilePhotoUrl = entity.ProfilePhotoUrl
                    }).SingleOrDefaultAsync();

                OperationResult<GenericOperationResult, NormalUserDto> result = new(GenericOperationResult.Success, data);
                return result;
            }
            catch (Exception)
            {
                return new OperationResult<GenericOperationResult, NormalUserDto>(GenericOperationResult.NotFound).AddError(ErrorMessages.UserNotExist);
            }
        }

        public OperationResult<GenericOperationResult, IPagedList<NormalUserDto>> GetNormalUsers(string query,
            int? pageNumber = 0, int? pageSize = 4)
        {
            var result = new OperationResult<GenericOperationResult, IPagedList<NormalUserDto>>(GenericOperationResult.Failed);
            try
            {
                List<NormalUserForDrNedalDto> data = Context.Users
                .Include(entity => entity.UserRoles)
                .Include(entity => entity.PatientsDiseases)
                .Include(entity => entity.PatientAllegries)
                .Include(entity => entity.PatientsSurgers)
                .Include(entity => entity.PatientMedicines)
                .Include(entity => entity.MedicalTests)
                .Where(entity => entity.UserRoles
                .Any(userRole => userRole.Role.Name == EnumsExtensionMethods.RoleToString(Role.NormalUser))
                && entity.IsValid &&
                string.IsNullOrEmpty(query)
                || entity.UserName.Contains(query)
                || entity.Email.Contains(query)
                || entity.PhoneNumber.Contains(query))
                .Select(entity =>
                new NormalUserForDrNedalDto()
                {
                    Id = entity.Id,
                    UserName = entity.UserName,
                    Email = entity.Email,
                    PhoneNumber = entity.PhoneNumber,
                    DiseasesCount = entity.PatientsDiseases
                    .Where(patDis => patDis.IsValid).Count(),
                    AllergiesCount = entity.PatientAllegries
                    .Where(patAller => patAller.IsValid).Count(),
                    SurgeriesCount = entity.PatientsSurgers
                    .Where(patSurg => patSurg.IsValid).Count(),
                    MedicinesCount = entity.PatientMedicines
                    .Where(patMed => patMed.IsValid).Count(),
                    MedicalTestsCount = entity
                    .MedicalTests.Where(Mtest => Mtest.IsValid).Count(),
                    Notes = entity.UserNotes.Where(note => note.IsValid)
                    .Select(note => note.Note.Body)
                    .ToList()
                })
                .Skip(pageNumber.Value * pageSize.Value)
                .Take(pageSize.Value)
                .ToList();
                PagedList<NormalUserDto> x = new(data, pageNumber.Value, pageSize.Value);
                return result.UpdateResultStatus(GenericOperationResult.Success).UpdateResultData(x);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        #endregion

        #region Get Health Care Providers Who Have Opening Hours
        public OperationResult<GenericOperationResult, IEnumerable<HealthCareProviderDto>> GetHeathCareProviders()
        {
            var result = new OperationResult<GenericOperationResult,
                IEnumerable<HealthCareProviderDto>>(GenericOperationResult.Failed);

            try
            {
                var data = Context.Users
                .Include(entity => entity.OpeningHours)
                .Where(entity => entity.UserRoles.Any(
                    userRole => userRole.Role.Name ==
                    EnumsExtensionMethods.RoleToString(Role.HealthCareProvider))
                )
                .Select(entity => new HealthCareProviderDto
                {
                    UserId = entity.Id,
                    UserName = entity.UserName,
                    FirstName = entity.FirstName,
                    LastName = entity.LastName,
                    HealthCareSpecialtyIds = entity.HealthCareProviderSpecialties.Where(entity => entity.IsValid).Select(
                        hcs => hcs.HealthCareSpecialtyId).ToList(),

                }).ToList();

                return new OperationResult<GenericOperationResult,
                    IEnumerable<HealthCareProviderDto>>(GenericOperationResult.Success, data);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }
        }

        public OperationResult<GenericOperationResult> AddRefreshTokenToUser(int UserId, string RefreshToken)
        {

            var result = new OperationResult<GenericOperationResult>(GenericOperationResult.Failed);
            try
            {
                var user = Context.Users.Where(user => user.Id == UserId).SingleOrDefault();
                if (user == null)
                    return result.UpdateResultStatus(GenericOperationResult.NotFound).AddError(ErrorMessages.UserNotExist);

                user.RefreshToken = RefreshToken;
                user.RefreshTokenExpiryTime = DateTime.Now.AddYears(5);
                _ = Context.SaveChanges();
                _ = result.UpdateResultStatus(GenericOperationResult.Success);
            }
            catch (Exception)
            {
                return result.AddError(ErrorMessages.InternalServerError);
            }

            return result;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CraftLab.Core.BoundedContext.Repository;
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Model.Security;
using IGenericControlPanel.Model.Security.Permissions;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.Dto.Permissions;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums;

using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Security.Data.Repositories
{
    public class PermissionRepository : CraftLabRepository<CPDbContext, WebContentSet>, IPermissionRepository
    {
        #region Properties And Constructor

        private readonly IUserRepository _userRepository;

        public PermissionRepository(CPDbContext iSchoolDbContext, IUserRepository userRepository) : base(iSchoolDbContext)
        {
            _userRepository = userRepository;
        }
        #endregion

        #region Get

        public IEnumerable<string> GetSideBarContent()
        {
            int userId = _userRepository.CurrentUserIdentityId();
            Dto.UserDto user = _userRepository.GetUser(userId);

            List<string> res = GetValidEntities<WebContentRoleSet>()
                         .Include(entity => entity.Content)
                         .Where(entity => entity.RoleId == user.RoleId && entity.IsValid)
                         .Select(entity => entity.Content.Name)
                         .ToList();
            return res;
        }

        public IEnumerable<RoleDto> GetAllPermission()
        {
            List<RoleDto> res = GetValidEntities<CPRole>()
                         .Include(entity => entity.WebContentRoles)
                         .ThenInclude(entity => entity.Content)
                         .Where(entity => entity.Name != nameof(Role.Admin) &&
                         entity.Name != nameof(Role.NormalUser))
                         .Select(entity => new RoleDto()
                         {
                             Id = entity.Id,
                             RoleName = entity.Name,

                             WebContents = entity.WebContentRoles.Where(webc => webc.IsValid)
                                                  .Select(con => new WebContentDto()
                                                  {
                                                      Id = con.ContentId,
                                                      CanAdd = con.CanAdd,
                                                      CanDelete = con.CanDelete,
                                                      CanEdit = con.CanEdit,
                                                      CanView = con.CanView,
                                                      EnName = con.Content.Name
                                                  })
                                                  .ToList()

                         }).ToList();

            return res;
        }

        public IEnumerable<int> GetWebContents(int roleId)
        {
            List<int> res = GetValidEntities<WebContentRoleSet>()
                      .Include(entity => entity.Content)
                      .Where(entity => entity.RoleId == roleId && entity.IsValid)
                      .Select(entity => entity.ContentId).ToList();

            return res;
        }
        public IEnumerable<string> GetWebContentsNames(int roleId)
        {
            List<string> Contents = GetValidEntities<WebContentRoleSet>()
                      .Include(entity => entity.Content)
                      .Where(entity => entity.RoleId == roleId && entity.IsValid)
                      .Select(entity => entity.Content.Name).ToList();

            return Contents;
        }

        public IEnumerable<WebContentDto> GetAllWebContents()
        {
            List<WebContentDto> res = ValidEntities
                      .Where(entity => entity.IsValid)
                      .Select(entity => new WebContentDto()
                      {
                          Id = entity.Id,
                          EnName = entity.Name,
                          ArName = entity.Name,


                      }).ToList();

            return res;
        }

        public IEnumerable<string> GetWebContent(string controllerName, string actionType)
        {
            if (controllerName.Count() > 0)
            {
                int contentId = GetContentId(controllerName);

                List<string> data = GetValidEntities<WebContentRoleSet>()
                              .Include(entity => entity.Role)
                              .Where(entity => entity.ContentId == contentId && entity.IsValid
                               && (controllerName.Count() > 1)
                               && ((actionType == nameof(ActionType.Get) && entity.CanView)
                                     || (actionType == "Action" || actionType=="Add" && entity.CanAdd)
                                     || (actionType == "Update" && entity.CanEdit)
                                     || (actionType == nameof(ActionType.Remove) && entity.CanDelete)
                                     ))
                              .Select(entity => entity.Role.Name)
                              .ToList();

                return data;
            }
            else
            {
                return new List<string>();
            }
        }

        public IEnumerable<string> GetWebContent(string controllerName)
        {
            int contentId = GetContentId(controllerName);

            List<string> data = GetValidEntities<WebContentRoleSet>()
                          .Include(entity => entity.Role)
                          .Where(entity => entity.ContentId == contentId && entity.IsValid)
                          .Select(entity => entity.Role.Name)
                          .ToList();

            return data;
        }

        private int GetContentId(string controllerName)
        {
            int? id = ValidEntities.SingleOrDefault(entity => entity.Name == controllerName)?.Id;

            return id.HasValue ? id.Value : 0;
        }

        private int GetContentId(string controllerName, string actionName)
        {
            int? id = ValidEntities.SingleOrDefault(entity => entity.Name == controllerName)?.Id;

            return id.HasValue ? id.Value : 0;
        }
        #endregion

        #region Action

        public OperationResult<GenericOperationResult, IEnumerable<WebContentDto>> ActionPermission(IEnumerable<WebContentDto> webContents, int roleId)
        {
            using Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction trans = Context.Database.BeginTransaction();
            try
            {
                OperationResult<GenericOperationResult, IEnumerable<int>> result = new(GenericOperationResult.Failed);

                if (webContents != null && webContents.Count() > 0)
                {
                    List<WebContentRoleSet> webContentRoleEntities = new();
                    bool EntityExist = true;
                    foreach (WebContentDto webContent in webContents)
                    {
                        webContent.CanView = webContent.CanAdd || webContent.CanDelete
                                             || webContent.CanEdit || webContent.CanView;
                        WebContentRoleSet webContentEntity = Context.WebContentRoles.FirstOrDefault(entity => entity.RoleId == roleId
                        && entity.ContentId == webContent.Id);
                        if (webContentEntity is null)
                        {
                            EntityExist = false;
                            webContentEntity = new WebContentRoleSet();
                        }
                        webContentEntity.ContentId = webContent.Id;
                        webContentEntity.RoleId = roleId;
                        webContentEntity.CanAdd = webContent.CanAdd;
                        webContentEntity.CanDelete = webContent.CanDelete;
                        webContentEntity.CanEdit = webContent.CanEdit;
                        webContentEntity.CanView = webContent.CanView;
                        webContentEntity.IsValid = webContentEntity.CanAdd || webContentEntity.CanEdit
                                                     || webContentEntity.CanDelete
                                                     || webContentEntity.CanView;
                        if (EntityExist)
                        {
                            _ = Context.WebContentRoles.Update(webContentEntity);
                        }
                        else
                        {
                            EntityExist = true;
                            _ = Context.WebContentRoles.Add(webContentEntity);
                        }
                        _ = Context.SaveChanges();
                    }
                }
                trans.Commit();

                return new OperationResult<GenericOperationResult, IEnumerable<WebContentDto>>(GenericOperationResult.Success, webContents);
            }
            catch (Exception ex)
            {
                trans.Rollback();
                return new OperationResult<GenericOperationResult, IEnumerable<WebContentDto>>(GenericOperationResult.Failed, default, ex);
            }
        }

        public IEnumerable<WebContentDto> GetAllWebContents(int RoleId)
        {
            List<WebContentDto> res = ValidEntities
                       .Where(entity => entity.IsValid)
                       .Select(entity => new WebContentDto()
                       {
                           Id = entity.Id,
                           EnName = entity.Name,
                           ArName = entity.Name,
                       }).ToList();

            return res;
        }

        public RoleDto GetWebContentsForRole(int RoleId)
        {
            RoleDto res = GetValidEntities<CPRole>()
                       .Where(entity => entity.IsValid && entity.Id == RoleId)
                       .Select(entity => new RoleDto()
                       {
                           Id = entity.Id,
                           RoleName = entity.Name,
                       }).SingleOrDefault();
            if (res != null)
            {
                res.WebContents = Context.WebContents.Select(ent => new WebContentDto()
                {
                    Id = ent.Id,
                    EnName = ent.Name,
                    CanAdd = ent.WebContentRoles.Any(web => web.RoleId == RoleId && web.IsValid) && ent.WebContentRoles.SingleOrDefault(web => web.RoleId == RoleId).CanAdd,
                    CanDelete = ent.WebContentRoles.Any(web => web.RoleId == RoleId && web.IsValid) && ent.WebContentRoles.SingleOrDefault(web => web.RoleId == RoleId).CanDelete,
                    CanEdit = ent.WebContentRoles.Any(web => web.RoleId == RoleId && web.IsValid) && ent.WebContentRoles.SingleOrDefault(web => web.RoleId == RoleId).CanEdit,
                    CanView = ent.WebContentRoles.Any(web => web.RoleId == RoleId && web.IsValid) && ent.WebContentRoles.SingleOrDefault(web => web.RoleId == RoleId).CanView,
                }).ToList();
            }
            return res;
        }

        public async Task<RawPermissionsDto> GetPermissionsForWebContentId(int WebContentId)
        {
            CPRole userRole = await _userRepository.CurrentUserRole();
            RawPermissionsDto res = GetValidEntities<WebContentRoleSet>()
                      .Where(entity => entity.ContentId == WebContentId && entity.RoleId == userRole.Id)
                      .Select(entity => new RawPermissionsDto()
                      {

                          CanAdd = entity.CanAdd,
                          CanDelete = entity.CanDelete,
                          CanEdit = entity.CanEdit,
                          CanView = entity.CanView


                      }).SingleOrDefault();

            return res;
        }
        #endregion

        #region Remove

        public async Task<bool> RemovePermission(int roleId)
        {
            try
            {
                IQueryable<WebContentRoleSet> webContetntEntities = GetValidEntities<WebContentRoleSet>()
                                    .Where(entity => entity.RoleId == roleId);

                await webContetntEntities.ForEachAsync(entitty => entitty.IsValid = false);

                Context.UpdateRange(webContetntEntities);
                _ = await Context.SaveChangesAsync();

                return true;
            }
            catch
            {
                return false;
            }
        }


        #endregion
    }
}

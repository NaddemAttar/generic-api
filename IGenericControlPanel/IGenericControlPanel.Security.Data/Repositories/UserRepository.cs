﻿using CraftLab.Core.BoundedContext.Repository;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.Dto;
using IGenericControlPanel.Security.Dto.User;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Extension;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace IGenericControlPanel.Security.Data.Repositories
{
    public class UserRepository : CraftLabRepository<CPDbContext, CPUser>, IUserRepository
    {
        private IHttpContextAccessor _httpContextAccessor { get; set; }
        private UserManager<CPUser> UserManager { get; }
        private RoleManager<CPRole> RoleManager;
        public UserRepository(
            IHttpContextAccessor httpContextAccessor,
            CPDbContext context,
            UserManager<CPUser> userManager,
            SignInManager<CPUser> signInManager
            , RoleManager<CPRole> roleManager) : base(context)
        {
            _httpContextAccessor = httpContextAccessor;
            UserManager = userManager;
            RoleManager = roleManager;
        }
        public int CurrentUserIdentityId()
        {
            try
            {
                var _httpcontext = _httpContextAccessor.HttpContext;
                if (_httpcontext != null
                    && _httpcontext.User != null
                    && _httpcontext.User.Identity != null
                    && _httpcontext.User.Identity.IsAuthenticated)
                {
                    var CPUserId = int.Parse(_httpcontext.User
                        .FindFirstValue(ClaimTypes.NameIdentifier));
                    return CPUserId;
                }
            }
            catch (Exception ex)
            {
            }
            return 0;
        }
        public async Task<CPRole> CurrentUserRole()
        {
            try
            {
                HttpContext _httpcontext = _httpContextAccessor.HttpContext;
                if (_httpcontext != null
                    && _httpcontext.User != null
                    && _httpcontext.User.Identity != null
                    && _httpcontext.User.Identity.IsAuthenticated)
                {
                    string RoleName = _httpcontext.User
                        .FindFirstValue(ClaimTypes.Role);
                    CPRole Role = await RoleManager.FindByNameAsync(RoleName);
                    return Role;

                }
            }
            catch
            {
            }
            return null;
        }

        public UserDto GetUser(int id)
        {
            UserDto user = ValidEntities
                .Where(iv => iv.IsValid && iv.Id == id)
                .Select(userinfo => new UserDto()
                {
                    Id = userinfo.Id,
                    UserName = userinfo.UserName,
                    Password = userinfo.PasswordHash,
                    Email = userinfo.Email,
                    ProfilePhotoUrl = userinfo.ProfilePhotoUrl,
                }).SingleOrDefault();

            CPUser userIdentity = UserManager.FindByIdAsync(user.Id.ToString()).Result;
            user.RoleName = UserManager.GetRolesAsync(userIdentity).Result.ToList()[0];
            user.RoleId = RoleManager.FindByNameAsync(UserManager.GetRolesAsync(userIdentity).Result.ToList()[0]).Result.Id;

            return user;
        }

        public UserDto GetCurrentUserInformation()
        {
            try
            {
                HttpContext _httpcontext = _httpContextAccessor.HttpContext;
                if (_httpcontext != null
                    && _httpcontext.User != null
                    && _httpcontext.User.Identity != null
                    && _httpcontext.User.Identity.IsAuthenticated)
                {
                    int id = int.Parse(_httpcontext.User
                        .FindFirstValue(ClaimTypes.NameIdentifier));

                    UserDto user = ValidEntities.Where(iv => iv.IsValid && iv.Id == id).Select(userinfo => new UserDto()
                    {
                        Id = userinfo.Id,
                        //FirstName = userinfo.FirstName,
                        //LastName = userinfo.LastName,
                        UserName = userinfo.UserName,
                        Password = userinfo.PasswordHash,
                        Email = userinfo.Email,
                        ProfilePhotoUrl = userinfo.ProfilePhotoUrl,
                        userType = userinfo.HcpType
                    }).SingleOrDefault();

                    CPUser userIdentity = UserManager.FindByIdAsync(user.Id.ToString()).Result;
                    user.RoleName = UserManager.GetRolesAsync(userIdentity).Result.ToList()[0];
                    user.RoleId = RoleManager.FindByNameAsync(UserManager.GetRolesAsync(userIdentity).Result.ToList()[0]).Result.Id;

                    return user;
                }
            }
            catch
            {
                return null;
            }
            return null;

        }

        public string GetRole(int roleId)
        {
            CPRole role = RoleManager.Roles.Where(role => role.Id == roleId).SingleOrDefault();

            return role.Name;
        }
        public async Task<string> GetRoleByCurrentUser()
        {
            try
            {
                HttpContext _httpcontext = _httpContextAccessor.HttpContext;
                if (_httpcontext != null
                    && _httpcontext.User != null
                    && _httpcontext.User.Identity != null
                    && _httpcontext.User.Identity.IsAuthenticated)
                {
                    var RoleName = _httpcontext.User
                        .FindFirstValue(ClaimTypes.Role);
                    CPRole Role = await RoleManager.FindByNameAsync(RoleName);
                    return Role.Name;
                }
            }
            catch
            {
            }
            return null;
        }
        public async Task<UserInfoDto> GetCurrentUserInfo()
        {
            int currentUserId = CurrentUserIdentityId();
            if (currentUserId == 0)
            {
                return new UserInfoDto
                {
                    CurrentUserId = currentUserId,
                    IsUserAdmin = false,
                    IsHealthCareProvider = false,
                    IsNormalUser = false
                };
            }
            var currentUserRole = await CurrentUserRole();
            bool isUserAdmin = currentUserRole.Name == EnumsExtensionMethods.RoleToString(Role.Admin);
            bool isHealthCareProvider = currentUserRole.Name == EnumsExtensionMethods.RoleToString(Role.HealthCareProvider);
            bool isNormalUser = currentUserRole.Name == EnumsExtensionMethods.RoleToString(Role.NormalUser);

            return new UserInfoDto
            {
                CurrentUserId = currentUserId,
                IsUserAdmin = isUserAdmin,
                IsHealthCareProvider = isHealthCareProvider,
                IsNormalUser = isNormalUser
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;

using AutoMapper;

using CraftLab.Core.Infrastructure;
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Configuration.Dto;
using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Model;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Repository;

using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.Configuration.Data.Repositories
{
    public class UserLanguageRepository : BasicRepository<CPDbContext, UserLanguage>, IUserLanguageRepository
    {

        #region Constructors

        public UserLanguageRepository(CPDbContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
        }

        #endregion

        public OperationResult<GenericOperationResult, UserLanguageDto> Get(string id, string cultureCode)
        {
            if (string.IsNullOrEmpty(cultureCode))
            {
                cultureCode = GetDefaultCulture().Name;
            }

            try
            {
                UserLanguageDto data = ValidEntities.Where(entity => entity.CultureCode == id)
                               .OrderBy(entity => entity.Order)
                               .Select(entity => new UserLanguageDto()
                               {
                                   CultureCode = entity.CultureCode,
                                   IsValid = entity.IsValid,
                                   LanguageName = NeutralCultureCode == cultureCode ? entity.LanguageName : entity.Translations.SingleOrDefault(translation => translation.CultureCode == cultureCode.Trim()).LanguageName ?? "",
                                   IsPrimaryLanguage = entity.IsPrimaryLanguage,
                                   Order = entity.Order,
                                   Translations = entity.Translations.Where(entityDic => entityDic.IsValid)
                                                            .Select(entityDic => new UserLanguageTranslationDto()
                                                            {
                                                                Id = entityDic.Id,
                                                                CultureCode = entityDic.CultureCode,
                                                                LanguageName = entityDic.LanguageName,
                                                            }),
                               }).FirstOrDefault();

                return new OperationResult<GenericOperationResult, UserLanguageDto>(GenericOperationResult.Success, data);
            }
            catch (Exception ex)
            {
                return new OperationResult<GenericOperationResult, UserLanguageDto>(GenericOperationResult.Failed, null, ex);
            }

        }
        public OperationResult<GenericOperationResult, UserLanguageDto> Get(string id)
        {
            OperationResult<GenericOperationResult, UserLanguageDto> result = new(GenericOperationResult.Failed);
            try
            {
                UserLanguageDto data = ValidEntities.Where(entity => entity.CultureCode == id)
                               .OrderBy(entity => entity.Order)
                               .Select(entity => new UserLanguageDto()
                               {
                                   CultureCode = entity.CultureCode,
                                   IsValid = entity.IsValid,
                                   LanguageName = entity.LanguageName,
                                   IsPrimaryLanguage = entity.IsPrimaryLanguage,
                                   Order = entity.Order,
                                   Translations = entity.Translations.Where(entityDic => entityDic.IsValid)
                                                            .Select(entityDic => new UserLanguageTranslationDto()
                                                            {
                                                                Id = entityDic.Id,
                                                                CultureCode = entityDic.CultureCode,
                                                                LanguageName = entityDic.LanguageName,
                                                            }),
                               }).FirstOrDefault();

                result = new OperationResult<GenericOperationResult, UserLanguageDto>(GenericOperationResult.Success, data);
            }
            catch (Exception ex)
            {
                return new OperationResult<GenericOperationResult, UserLanguageDto>(GenericOperationResult.Failed, null, ex);
            }

            return result;
        }

        public OperationResult<GenericOperationResult, IPagedList<UserLanguageDto>> GetAllPaged(int? pageNumber = 0, int? pageSize = null)
        {
            try
            {
                pageSize ??= 10;
                pageNumber ??= 0;

                IQueryable<UserLanguageDto> data = ValidEntities.Skip(pageNumber.Value * pageSize.Value)
                                 .Take(pageSize.Value)
                                 .Include(entity => entity.Translations)
                                 .OrderBy(entity => entity.Order)
                                       .Select(entity => new UserLanguageDto()
                                       {
                                           CultureCode = entity.CultureCode,
                                           IsValid = entity.IsValid,
                                           IsPrimaryLanguage = entity.IsPrimaryLanguage,
                                           Order = entity.Order,
                                           LanguageName = entity.LanguageName,
                                           Translations = entity.Translations.Where(entityDic => entityDic.IsValid)
                                                                    .Select(entityDic => new UserLanguageTranslationDto()
                                                                    {
                                                                        Id = entityDic.Id,
                                                                        CultureCode = entityDic.CultureCode,
                                                                        LanguageName = entityDic.LanguageName,
                                                                    }),
                                           CultureInfo = new CultureInfo(entity.CultureCode)
                                       });
                return new OperationResult<GenericOperationResult, IPagedList<UserLanguageDto>>(GenericOperationResult.Success, new PagedList<UserLanguageDto>(data, pageNumber.Value, pageSize.Value));
            }
            catch (Exception ex)
            {
                return new OperationResult<GenericOperationResult, IPagedList<UserLanguageDto>>(GenericOperationResult.Failed, null, ex);
            }

        }
        public IPagedList<UserLanguageDto> GetAll(int? pageNumber = 0, int? pageSize = null, string cultureCode = null)
        {
            if (cultureCode is null)
            {
                cultureCode = GetDefaultCulture().Name;
            }
            pageSize ??= 10;
            pageNumber ??= 0;

            IQueryable<UserLanguageDto> data = ValidEntities.Skip(pageNumber.Value * pageSize.Value)
                             .Take(pageSize.Value)
                             .Include(entity => entity.Translations)
                             .OrderBy(entity => entity.Order)
                                   .Select(entity => new UserLanguageDto()
                                   {
                                       CultureCode = entity.CultureCode,
                                       IsValid = entity.IsValid,
                                       IsPrimaryLanguage = entity.IsPrimaryLanguage,
                                       Order = entity.Order,
                                       LanguageName = NeutralCultureCode == cultureCode ? entity.LanguageName : entity.Translations.SingleOrDefault(translation => translation.CultureCode == cultureCode).LanguageName ?? "",
                                       Translations = entity.Translations.Where(entityDic => entityDic.IsValid)
                                                                .Select(entityDic => new UserLanguageTranslationDto()
                                                                {
                                                                    Id = entityDic.Id,
                                                                    CultureCode = entityDic.CultureCode,
                                                                    LanguageName = entityDic.LanguageName,
                                                                }),
                                       CultureInfo = new CultureInfo(entity.CultureCode)
                                   });
            return new PagedList<UserLanguageDto>(data, pageNumber.Value, pageSize.Value);
        }


        public IList<CultureInfo> GetSupportedCultures()
        {
            try
            {
                return ValidEntities
                                .Select(culture => new CultureInfo(culture.CultureCode))
                                .ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public CultureInfo GetDefaultCulture()
        {
            UserLanguage defaultLanguage = Context.UserLanguages.Where(entity => entity.IsPrimaryLanguage).FirstOrDefault();

            return defaultLanguage is null ? new CultureInfo("ar-SY") : new CultureInfo(defaultLanguage.CultureCode);
        }

        public OperationResult<GenericOperationResult, IEnumerable<UserLanguageDto>> GetNotDefaultUserLangauges()
        {
            OperationResult<GenericOperationResult, IEnumerable<UserLanguageDto>> result = new(GenericOperationResult.Failed);

            try
            {
                IEnumerable<UserLanguageDto> data = ValidEntities
                    .Include(entity => entity.Translations)
                    .Where(entity => !entity.IsPrimaryLanguage)
                    .OrderBy(entity => entity.Order)
                    .ToList()
                    .Select(entity => Mapper.Map<UserLanguage, UserLanguageDto>(entity));

                return new OperationResult<GenericOperationResult, IEnumerable<UserLanguageDto>>(GenericOperationResult.Success, data);

            }
            catch
            {
                _ = result.AddError("An error has occurred while retrieveing data");
                return result;
            }

        }
        public IEnumerable<UserLanguageDto> GetAll(string cultureCode)
        {
            if (cultureCode is null)
            {
                cultureCode = GetDefaultCulture().Name;
            }

            IQueryable<UserLanguageDto> data = ValidEntities
                .Include(entity => entity.Translations)
                .OrderBy(entity => entity.Order)
                .Select(entity => Mapper.Map<UserLanguage, UserLanguageDto>(entity, opts => opts.Items.Add(ItemNames.CultureCode, cultureCode)));

            return data;
        }
        public OperationResult<GenericOperationResult, IEnumerable<UserLanguageDto>> GetAll()
        {
            OperationResult<GenericOperationResult, IEnumerable<UserLanguageDto>> result = new(GenericOperationResult.Failed);
            try
            {
                IQueryable<UserLanguageDto> data = ValidEntities
                                .Include(entity => entity.Translations)
                                .OrderBy(entity => entity.Order)
                                .Select(entity => Mapper.Map<UserLanguage, UserLanguageDto>(entity));

                return new OperationResult<GenericOperationResult, IEnumerable<UserLanguageDto>>(GenericOperationResult.Success, data);
            }
            catch (AutoMapperConfigurationException)
            {
                result.ValidationResults.Add(new ValidationResult("Exception has been thrown from automapper! It's about configuration.", new List<string> { "entity" }));
                return result;
            }
            catch (AutoMapperMappingException)
            {
                result.ValidationResults.Add(new ValidationResult("Exception has been thrown from automapper! It's about mapping.", new List<string> { "entity" }));
                return result;
            }


        }


    }
}

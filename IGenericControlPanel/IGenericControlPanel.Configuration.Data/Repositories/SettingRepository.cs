﻿using System;
using System.Collections.Generic;
using System.Linq;

using CraftLab.Core.BoundedContext.Repository;
using CraftLab.Core.OperationResults;

using IGenericControlPanel.Configuration.IData;
using IGenericControlPanel.Model.Configuration;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Constants;
using IGenericControlPanel.SharedKernal.Messages;

using Microsoft.EntityFrameworkCore;


namespace IGenericControlPanel.Configuration.Data.Repositories
{
    public class SettingRepository : CraftLabRepository<CPDbContext, Setting>, ISettingRepository
    {

        public IList<string> EdibleSettings { get; set; } = new List<string>()
        {
             SettingName.DefaultCultureCode ,
             SettingName.Email,
             SettingName.FacebookLink,
             SettingName.LinkedInLink,
             SettingName.PhoneNumber,
             SettingName.YoutubeLink,
             SettingName.TwitterLink,
             SettingName.Telegram,
             SettingName.Whatsapp,
             SettingName.InstagramLink,
             SettingName.WebsiteFont,
             SettingName.longitude,
             SettingName.latitude,
             SettingName.WorkTime,
             SettingName.Location,
             SettingName.CurrentLanguage
             //SettingName.LatestProductsCount,
        };

        public SettingRepository(CPDbContext dbContext) : base(dbContext)
        {
        }

        public string Get(string key)
        {
            string preproccessedKey = key.Trim();
            return ValidEntities
                .SingleOrDefault(entity => entity.Name == preproccessedKey)?.Value;

        }
        public TCustomReturn Get<TCustomReturn>(string key) where TCustomReturn : struct, IConvertible
        {
            if (key is null)
            {
                throw new ArgumentNullException("key", "The parameter key cannot be null");
            }

            string preproccessedKey = key?.Trim();
            string result = ValidEntities
                .SingleOrDefault(entity => entity.Name == preproccessedKey)?.Value;

            if (result == null)
            {
                return default;
            }

            try
            {
                return (TCustomReturn)Convert.ChangeType(result, typeof(TCustomReturn));
            }
            catch
            {
                return default;
            }
        }
        public KeyValuePair<string, string> GetPair(string key)
        {
            if (key is null)
            {
                throw new ArgumentNullException("key", "The parameter key cannot be null");
            }
            string preproccessedKey = key.Trim();
            return ValidEntities
                .Where(entity => entity.Name == preproccessedKey)
                .ToDictionary(name => name.Name, value => value.Value)
                .SingleOrDefault();
        }

        public IEnumerable<string> GetAll()
        {
            return ValidEntities
                .Select(entity => entity.Value);
        }

        public IEnumerable<string> GetSet(IEnumerable<string> keys)
        {
            if (keys is null)
            {
                throw new ArgumentNullException("keys", "The parameter keys cannot be null");
            }

            IEnumerable<string> preproccessedKey = keys.Select(key => key.Trim());
            return ValidEntities
                .Where(entity => preproccessedKey.Any(name => name == entity.Name))
                .Select(entity => entity.Value);
        }

        public IDictionary<string, string> GetAllAsDictionary()
        {
            return ValidEntities
                .ToDictionary(name => name.Name, value => value.Value);
        }

        public IDictionary<string, string> GetSetAsDictionary(IEnumerable<string> keys)
        {
            if (keys is null)
            {
                throw new ArgumentNullException("keys", "The parameter keys cannot be null");
            }

            IEnumerable<string> preproccessedKey = keys.Select(key => key.Trim());
            return ValidEntities
                .Where(entity => preproccessedKey.Any(name => name == entity.Name))
                .ToDictionary(name => name.Name, value => value.Value);
        }

        public string GetBaseDirectoryUrl()
        {
            return "data";
        }

        public OperationResult<GenericOperationResult, IDictionary<string, string>> Action(IDictionary<string, string> settings)
        {
            OperationResult<GenericOperationResult, IDictionary<string, string>> result = new(GenericOperationResult.ValidationError);
            try
            {
                List<Setting> list = ValidEntities
                    .Where(setting => settings.Keys.Contains(setting.Name)).ToList();
                foreach (KeyValuePair<string, string> setting1 in settings)
                {
                    KeyValuePair<string, string> setting = setting1;
                    Setting settingSet = list.SingleOrDefault(s => s.Name == setting.Key);
                    EntityState entityState;
                    if (settingSet == null)
                    {
                        Setting entity = new()
                        {
                            Name = setting.Key,
                            Value = setting.Value
                        };
                        settingSet = entity;
                        entityState = EntityState.Added;
                    }
                    else
                    {
                        settingSet.Value = setting.Value;
                        entityState = EntityState.Modified;
                    }

                    Context.Entry(settingSet).State = entityState;

                    _ = Context.SaveChanges();

                }
                return new OperationResult<GenericOperationResult, IDictionary<string, string>>(GenericOperationResult.Success, settings, null);
            }
            catch (Exception)
            {
                return result.UpdateResultStatus(GenericOperationResult.Failed).AddError(ErrorMessages.InternalServerError);
            }
        }

    }
}

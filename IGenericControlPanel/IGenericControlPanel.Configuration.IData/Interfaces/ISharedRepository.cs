﻿
namespace IGenericControlPanel.Configuration.IData.Interfaces;
public interface ISharedRepository
{
    Task<bool> IsExistAsync(int id);
}

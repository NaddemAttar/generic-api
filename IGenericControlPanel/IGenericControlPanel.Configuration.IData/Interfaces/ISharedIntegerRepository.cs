﻿
namespace IGenericControlPanel.Configuration.IData.Interfaces;
public interface ISharedIntegerRepository: ISharedRepository
{
    Task<bool> IsSameValueAsync(int value, int id = 0);
}

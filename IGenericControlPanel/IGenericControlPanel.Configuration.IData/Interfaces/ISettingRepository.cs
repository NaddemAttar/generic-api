﻿using CraftLab.Core.OperationResults;

using IGenericControlPanel.SharedKernal.Interfaces;

using System.Collections.Generic;

namespace IGenericControlPanel.Configuration.IData
{
    public interface ISettingRepository : IBasicSettingRepository<string, string>
    {
        string GetBaseDirectoryUrl();
        IList<string> EdibleSettings { get; set; }
        OperationResult<GenericOperationResult, IDictionary<string, string>> Action(IDictionary<string, string> settings);

    }
}

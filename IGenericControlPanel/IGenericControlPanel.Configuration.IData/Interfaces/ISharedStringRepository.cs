﻿
namespace IGenericControlPanel.Configuration.IData.Interfaces;
public interface ISharedStringRepository: ISharedRepository
{
    Task<bool> IsSameNameAsync(string name);
}

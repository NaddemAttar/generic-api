﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.Content.Dto;

namespace IGenericControlPanel.Configuration.IData.Interfaces;
public interface IGetMultipleGeneric<TDto>
{
    Task<OperationResult<GenericOperationResult, IEnumerable<TDto>>> GetMultipleAsync();
}

public interface IGetDetailsGeneric<TDto, Ty>
{
    Task<OperationResult<GenericOperationResult, TDto>> GetDetailsAsync(Ty id);
}

public interface IRemoveGeneric<Ty>
{
    Task<OperationResult<GenericOperationResult>> RemoveAsync(Ty id);
}

public interface IActionGeneric<TDto>
{
    Task<OperationResult<GenericOperationResult, TDto>> ActionAsync(
        TDto dto, IEnumerable<FileDetailsDto> fileDtos);
}

public interface IAddGeneric<TDto>
{
    Task<OperationResult<GenericOperationResult, TDto>> AddAsync(
        TDto addDto, IEnumerable<FileDetailsDto> fileDtos);
}
public interface IUpdateGeneric<TDto, Ty>
{
    Task<OperationResult<GenericOperationResult, TDto>> UpdateAsync(
        TDto updateDto, IEnumerable<FileDetailsDto> fileDtos, IEnumerable<Ty> removeFileIds);
}
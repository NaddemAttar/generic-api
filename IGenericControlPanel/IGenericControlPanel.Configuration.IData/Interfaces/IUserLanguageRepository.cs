﻿using CraftLab.Core.OperationResults;

using IGenericControlPanel.Configuration.Dto;
using IGenericControlPanel.SharedKernal.Interfaces;

using System.Collections.Generic;
using System.Globalization;

namespace IGenericControlPanel.Configuration.IData
{
    public interface IUserLanguageRepository : IRetrievable<string, UserLanguageDto>
    {
        IList<CultureInfo> GetSupportedCultures();
        CultureInfo GetDefaultCulture();
        OperationResult<GenericOperationResult, IEnumerable<UserLanguageDto>> GetNotDefaultUserLangauges();
    }
}

﻿
using IGenericControlPanel.SharedKernal.Enums.Consultations;

namespace IGenericControlPanel.Consultation.Dto.ConsultationDto;
public class HealthProviderQuestionsFormDto
{
    public int Id { get; set; }
    public string QuestionText { get; set; }
    public ConsultationQuestionType ConsultationQuestionType { get; set; }
    public IEnumerable<int> ConsultationChoiceIds { get; set; }
}

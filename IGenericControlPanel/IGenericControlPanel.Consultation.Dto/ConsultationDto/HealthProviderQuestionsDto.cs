﻿
using IGenericControlPanel.SharedKernal.Enums.Consultations;

namespace IGenericControlPanel.Consultation.Dto.ConsultationDto;
public class HealthProviderQuestionsDto
{
    public int Id { get; set; }
    public string QuestionText { get; set; }
    public ConsultationQuestionType ConsultationQuestionType { get; set; }
    public IEnumerable<ConsultationChoicesDto> ConsultationChoices { get; set; }
}

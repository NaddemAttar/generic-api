﻿namespace IGenericControlPanel.Consultation.Dto.ConsultationDto
{
    public class TotalConsultationForPatient
    {
        public int DrId { get; set; }
        public string DrName { get; set; }
        public string DrPhoto { get; set; }
        public int ConsultationCount { get; set; }
    }
}
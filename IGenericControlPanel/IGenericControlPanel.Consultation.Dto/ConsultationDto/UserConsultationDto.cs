﻿using IGenericControlPanel.SharedKernal.Enums.Consultations;

namespace IGenericControlPanel.Consultation.Dto.ConsultationDto;
public class UserConsultationDto
{
    public string QuestionText { get; set; }
    public ConsultationQuestionType ConsultationQuestionType { get; set; }
    public bool? Status { get; set; }
    public IEnumerable<string> ConsultationChoices { get; set; }
    public string Comment { get; set; }
}

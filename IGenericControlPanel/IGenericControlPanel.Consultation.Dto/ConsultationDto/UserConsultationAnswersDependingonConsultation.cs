﻿namespace IGenericControlPanel.Consultation.Dto.ConsultationDto;
public class UserConsultationAnswersDependingonConsultation
{
    public int ConsultationId { get; set; }
    public string HealthProviderAnswer { get; set; }
    public string CaseDescription { get; set; }
    public string PatientName { get; set; }
    public IEnumerable<UserConsultationDto> UserConsultationDtos { get; set; }
}

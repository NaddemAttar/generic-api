﻿namespace IGenericControlPanel.Consultation.Dto.ConsultationDto;
public class ConsultationChoicesDto
{
    public int Id { get; set; }
    public string Name { get; set; }
}

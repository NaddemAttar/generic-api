﻿namespace IGenericControlPanel.Consultation.Dto.ConsultationDto;
public class UserConsultationFormDto
{
    public int QuestionId { get; set; }
    public string Comment { get; set; }
    public bool? Status { get; set; }
    public string MultiChoicesString { get; set; }
}

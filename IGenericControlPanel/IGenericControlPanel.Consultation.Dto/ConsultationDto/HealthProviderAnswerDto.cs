﻿namespace IGenericControlPanel.Consultation.Dto.ConsultationDto;
public class HealthProviderAnswerDto
{
    public int ConsultationId { get; set; }
    public string AnswerText { get; set; }
}

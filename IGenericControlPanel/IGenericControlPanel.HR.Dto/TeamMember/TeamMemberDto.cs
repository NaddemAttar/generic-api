﻿using CraftLab.Core.BoundedContext;

namespace IGenericControlPanel.HR.Dto
{
    public class TeamMemberDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string About { get; set; }
        public string Biography { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public string Position { get; set; }
        //public int Order { get; set; }
        public string CultureCode { get; set; }
        public string ImageUrl { get; set; }

    }
}
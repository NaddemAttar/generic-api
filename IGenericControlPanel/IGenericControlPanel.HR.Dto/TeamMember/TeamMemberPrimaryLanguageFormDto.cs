﻿using CraftLab.Core.BoundedContext;

using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Core.Dto;

using System.Collections.Generic;

namespace IGenericControlPanel.HR.Dto
{
    public class TeamMemberPrimaryLanguageFormDto : IFormDto
    {
        public TeamMemberPrimaryLanguageFormDto()
        {
            Images = new List<FileDetailsDto>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Position { get; set; }
        public string CultureCode { get; set; }
        public string Description { get; set; }

        public int Order { get; set; }

        public ICollection<FileDetailsDto> Images { get; set; }

        public ActionOperationType OperationType { get; }
    }
}

﻿using CraftLab.Core.BoundedContext;

namespace IGenericControlPanel.HR.Dto
{
    public class TeamMemberTranslationFormDto : IFormDto
    {
        public int Id { get; set; }
        public int TeamMemberId { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string CultureCode { get; set; }
        public string Description { get; set; }
        public ActionOperationType OperationType { get; }
    }
}

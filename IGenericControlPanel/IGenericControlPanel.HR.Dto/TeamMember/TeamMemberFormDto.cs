﻿
using IGenericControlPanel.Content.Dto;

using Microsoft.AspNetCore.Http;

namespace IGenericControlPanel.HR.Dto
{
    public class TeamMemberFormDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string About { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public string Position { get; set; }
        public string CultureCode { get; set; }
        public IFormFile ImageFormFile { get; set; }
        public FileDetailsDto ImageDetails { get; set; }
        public string ImageUrl { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.HR.Dto
{
    public class TeamMemberTranslationDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string CultureCode { get; set; }
        public string Description { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.Globalization;

namespace IGenericControlPanel.Configuration.Dto
{
    public class UserLanguageDto
    {
        public string CultureCode { get; set; }
        public string LanguageCultureCode { get; set; }
        public string LanguageName { get; set; }
        public bool IsValid { get; set; }
        public bool IsPrimaryLanguage { get; set; }
        public CultureInfo CultureInfo { get; set; }
        public int Order { get; set; }
        public IEnumerable<UserLanguageTranslationDto> Translations { get; set; }

    }
}

﻿using CraftLab.Core.BoundedContext;

namespace IGenericControlPanel.Configuration.Dto
{
    public class UserLanguageTranslationDto : IFormDto
    {
        public int Id { get; set; }
        public string LanguageName { get; set; }
        public string LanguageCultureCode { get; set; }
        public string CultureCode { get; set; }
        public ActionOperationType OperationType { get; }
    }
}

﻿
namespace IGenericControlPanel.Configuration.Dto.Pagination;
public class PaginationDto
{
    public bool EnablePagination { get; set; }
    public int PageSize { get; set; }
    public int PageNumber { get; set; }
    public string Query { get; set; }
}

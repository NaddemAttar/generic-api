﻿namespace IGenericControlPanel.Configuration.Dto
{
    public class SettingDto
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public bool IsValid { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace IGenericControlPanel.Configuration.Dto
{
    public class SettingFormDto
    {

        public IList<SettingDto> Settings { get; set; }
        public bool IsValid { get; set; }
        public string CultureCode { get; set; }
    }
}

﻿
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.UserExperiences;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.UserExperiences.Dto;
using IGenericControlPanel.UserExperiences.Dto.Certificate;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.UserExperiences.Responsibilities.Responsibilities;
public static class CertificateRes
{
    #region RESPONSIBILITIES
    public static Certificate GenerateCertificateEntityAsync(
        this CertificateBasicDto actionDto, int hcpId,
        IEnumerable<FileDetailsDto> fileDtos)
    {
        var certificateEntity = new Certificate
        {
            Id = actionDto.Id,
            Title = actionDto.Title,
            Description = actionDto.Description,
            HcpId = hcpId,
            Institution = actionDto.Institution,
        };

        if (fileDtos != null && fileDtos.Any())
        {
            foreach (var fileDto in fileDtos)
            {
                var certificateFile = new CertificateFile
                {
                    Checksum = fileDto.Checksum,
                    FileContentType = fileDto.FileContentType,
                    Description = fileDto.Description,
                    Extension = fileDto.Extension,
                    FileName = fileDto.FileName,
                    Name = fileDto.Name,
                    Url = fileDto.Url,
                };

                certificateEntity.CertificateFiles.Add(certificateFile);
            }
        }

        return certificateEntity;
    }

    public static async Task<CertificateDetailsDto> GetCertificateDetailsAsync(
        this CPDbContext context, int id, int hcpId)
    {
        return await context.Certificates
                 .Where(entity => entity.IsValid && entity.Id == id && entity.HcpId == hcpId)
                 .Select(entity => new CertificateDetailsDto
                 {
                     Id = entity.Id,
                     Title = entity.Title,
                     Description = entity.Description,
                     Institution = entity.Institution,
                     CertificateFileDtos = entity.CertificateFiles.Where(file => file.IsValid)
                     .Select(file => new FileDataDto
                     {
                         Id = file.Id,
                         Extension = file.Extension,
                         Url = file.Url
                     }).ToList()
                 }).FirstOrDefaultAsync();
    }
    public static async Task<IEnumerable<CertificateDetailsDto>> GetCertificatesForResumeAsync(
        this CPDbContext context, int hcpId)
    {
        return await context.Certificates
                .Where(entity => entity.IsValid && entity.HcpId == hcpId)
                .Select(entity => new CertificateDetailsDto
                {
                    Id = entity.Id,
                    Title = entity.Title,
                    Description = entity.Title,
                    Institution = entity.Institution,
                    CertificateFileDtos = entity.CertificateFiles
                    .Where(fileEntity => fileEntity.IsValid)
                    .Select(fileEntity => new FileDataDto
                    {
                        Id = fileEntity.Id,
                        Extension = fileEntity.Extension,
                        Url = fileEntity.Url
                    }).ToList()
                }).ToListAsync();
    }
    public static async Task<IEnumerable<CertificateDto>> GetCertificatesAsync(
        this CPDbContext context, int hcpId)
    {
        return await context.Certificates
                .Where(entity => entity.IsValid && entity.HcpId == hcpId)
                .Select(entity => new CertificateDto
                {
                    Id = entity.Id,
                    Title = entity.Title,
                    Description = entity.Description,
                    Institution = entity.Institution,
                    CertificateImageUrl = entity.CertificateFiles
                    .Where(file => file.IsValid)
                    .Select(file => file.Url)
                    .FirstOrDefault()
                }).ToListAsync();
    }
    #endregion
}

﻿
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.UserExperiences;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.UserExperiences.Dto;
using IGenericControlPanel.UserExperiences.Dto.Experience;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.UserExperiences.Responsibilities.Responsibilities;
public static class ExperienceRes
{
    #region RESPONSIBILITIES
    public static async Task<IEnumerable<ExperienceDetailsDto>> GetExperiencesForResumeAsync(
        this CPDbContext context, int hcpId)
    {
        return await context.Experiences
                .Where(entity => entity.IsValid && entity.HcpId == hcpId)
                .Select(entity => new ExperienceDetailsDto
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Description = entity.Description,
                    StartDate = entity.StartDate,
                    EndDate = entity.EndDate,
                    Institution = entity.Institution,
                    Link = entity.Link,
                    ExperienceFiles = entity.ExperienceFiles
                    .Where(entity => entity.IsValid)
                    .Select(entity => new FileDataDto
                    {
                        Id = entity.Id,
                        Extension = entity.Extension,
                        Url = entity.Url
                    }).ToList()
                }).ToListAsync();
    }
    public static Experience GenerateExperienceEntityAsync(
        this ExperienceBasicDto actionDto, int hcpId,
        IEnumerable<FileDetailsDto> fileDtos)
    {
        var experienceEntity = new Experience
        {
            Id = actionDto.Id,
            Name = actionDto.Name,
            Description = actionDto.Description,
            StartDate = actionDto.StartDate,
            EndDate = actionDto.EndDate,
            HcpId = hcpId,
            Institution = actionDto.Institution,
            Link = actionDto.Link,
        };

        if (fileDtos != null && fileDtos.Any())
        {
            foreach (var fileDto in fileDtos)
            {
                var experienceFile = new ExperienceFile
                {
                    Checksum = fileDto.Checksum,
                    FileContentType = fileDto.FileContentType,
                    Description = fileDto.Description,
                    Extension = fileDto.Extension,
                    FileName = fileDto.FileName,
                    Name = fileDto.Name,
                    Url = fileDto.Url,
                };

                experienceEntity.ExperienceFiles.Add(experienceFile);
            }
        }

        return experienceEntity;
    }
    public static async Task<IEnumerable<ExperienceDto>> GetExperiencesAsync(
        this CPDbContext context, int hcpId)
    {
        return await context.Experiences
                .Where(exp => exp.IsValid && exp.HcpId == hcpId)
                .Select(exp => new ExperienceDto
                {
                    Id = exp.Id,
                    Name = exp.Name,
                    Description = exp.Description,
                    StartDate = exp.StartDate,
                    EndDate = exp.EndDate,
                    Institution = exp.Institution,
                    Link = exp.Link,
                    ExperienceImageUrl = exp.ExperienceFiles.Select(expF => expF.Url).FirstOrDefault()
                }).ToListAsync();
    }
    public static async Task<ExperienceDetailsDto> GetExperienceDetailsAsync(
        this CPDbContext context, int id, int hcpId)
    {
        return await context.Experiences
               .Where(exp => exp.IsValid && exp.Id == id)
               .Select(exp => new ExperienceDetailsDto
               {
                   Id = exp.Id,
                   Name = exp.Name,
                   Description = exp.Description,
                   StartDate = exp.StartDate,
                   EndDate = exp.EndDate,
                   Institution = exp.Institution,
                   Link = exp.Link,
                   ExperienceFiles = exp.ExperienceFiles.Select(expF => new FileDataDto
                   {
                       Id = expF.Id,
                       Url = expF.Url,
                       Extension = expF.Extension
                   }).ToList()
               }).FirstOrDefaultAsync();
    }
    #endregion
}

﻿
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.UserExperiences;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.UserExperiences.Dto;
using IGenericControlPanel.UserExperiences.Dto.License;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.UserExperiences.Responsibilities.Responsibilities;
public static class LicenseRes
{
    #region RESPONSIBILITIES
    public static async Task<IEnumerable<LicenseDetailsDto>> GetResumesForAsync(
        this CPDbContext context, int hcpId)
    {
        return await context.Licenses
                .Where(entity => entity.IsValid && entity.HcpId == hcpId)
                .Select(entity => new LicenseDetailsDto
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Description = entity.Description,
                    LiecenseFileDtos = entity.LicenseFiles
                    .Where(entity => entity.IsValid)
                    .Select(entity => new FileDataDto
                    {
                        Id = entity.Id,
                        Extension = entity.Extension,
                        Url = entity.Url
                    }).ToList()
                }).ToListAsync();
    }
    public static License GenerateLicenseEntityAsync(
        this LicenseBasicDto actionDto, int hcpId,
        IEnumerable<FileDetailsDto> fileDtos)
    {
        var licenseEntity = new License
        {
            Id = actionDto.Id,
            Name = actionDto.Name,
            Description = actionDto.Description,
            HcpId = hcpId,
        };

        if (fileDtos != null && fileDtos.Any())
        {
            foreach (var fileDto in fileDtos)
            {
                var licenseFile = new LicenseFile
                {
                    Checksum = fileDto.Checksum,
                    FileContentType = fileDto.FileContentType,
                    Description = fileDto.Description,
                    Extension = fileDto.Extension,
                    FileName = fileDto.FileName,
                    Name = fileDto.Name,
                    Url = fileDto.Url,
                };

                licenseEntity.LicenseFiles.Add(licenseFile);
            }
        }

        return licenseEntity;
    }
    public static async Task<IEnumerable<LicenseDto>> GetLicensesAsync(
        this CPDbContext context, int hcpId)
    {
        return await context.Licenses.Where(entity => entity.IsValid && entity.HcpId == hcpId)
                .Select(entity => new LicenseDto
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Description = entity.Description,
                    LicenseUrl = entity.LicenseFiles.Select(file => file.Url).FirstOrDefault()
                }).ToListAsync();
    }

    public static async Task<LicenseDetailsDto> GetLicenseDetailsAsync(
        this CPDbContext context, int id, int hcpId)
    {
        return await context.Licenses
                .Where(entity => entity.IsValid &&
                entity.Id == id &&
                entity.HcpId == hcpId)
                .Select(entity => new LicenseDetailsDto
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Description = entity.Description,
                    LiecenseFileDtos = entity.LicenseFiles.Where(file => file.IsValid)
                    .Select(file => new FileDataDto
                    {
                        Id = file.Id,
                        Extension = file.Extension,
                        Url = file.Url,
                    }).ToList()
                })
                .FirstOrDefaultAsync();
    }
    #endregion
}

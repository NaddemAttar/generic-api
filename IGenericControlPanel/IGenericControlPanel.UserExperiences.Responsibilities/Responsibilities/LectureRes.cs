﻿
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.UserExperiences;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.UserExperiences.Dto;
using IGenericControlPanel.UserExperiences.Dto.Lecture;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.UserExperiences.Responsibilities.Responsibilities;
public static class LectureRes
{
    #region RESPONSIBILITIES

    public static async Task<IEnumerable<LectureDetailsDto>> GetLecturesForResumeAsync(
       this CPDbContext context, int hcpId)
    {
        return await context.Lectures
                .Where(entity => entity.IsValid && entity.HcpId == hcpId)
                .Select(entity => new LectureDetailsDto
                {
                    Id = entity.Id,
                    Title = entity.Title,
                    Description = entity.Description,
                    Link = entity.Link,
                    LectureFileDtos = entity.LectureFiles
                    .Where(fileEntity => fileEntity.IsValid)
                    .Select(fileEntity => new FileDataDto
                    {
                        Id = fileEntity.Id,
                        Extension = fileEntity.Extension,
                        Url = fileEntity.Url
                    }).ToList()
                }).ToListAsync();
    }
    public static Lecture GenerateLectureEntityAsync(
        this LectureBasicDto actionDto, int hcpId,
        IEnumerable<FileDetailsDto> fileDtos)
    {
        var lectureEntity = new Lecture
        {
            Id = actionDto.Id,
            Title = actionDto.Title,
            Description = actionDto.Description,
            Link = actionDto.Link,
            HcpId = hcpId,
        };

        if (fileDtos != null && fileDtos.Any())
        {
            foreach (var fileDto in fileDtos)
            {
                var lectureFile = new LectureFile
                {
                    Checksum = fileDto.Checksum,
                    FileContentType = fileDto.FileContentType,
                    Description = fileDto.Description,
                    Extension = fileDto.Extension,
                    FileName = fileDto.FileName,
                    Name = fileDto.Name,
                    Url = fileDto.Url,
                };

                lectureEntity.LectureFiles.Add(lectureFile);
            }
        }

        return lectureEntity;
    }
    public static async Task<IEnumerable<LectureDto>> GetLecturesAsync(
       this CPDbContext context, int hcpId)
    {
        return await context.Lectures
                .Where(entity => entity.IsValid && entity.HcpId == hcpId)
                .Select(entity => new LectureDto
                {
                    Id = entity.Id,
                    Title = entity.Title,
                    Description = entity.Description,
                    Link = entity.Link,
                    LectureImageUrl = entity.LectureFiles
                    .Where(file => file.IsValid)
                    .Select(file => file.Url).FirstOrDefault()
                }).ToListAsync();
    }
    public static async Task<LectureDetailsDto> GetLectureDetailsAsync(
        this CPDbContext context, int id, int hcpId)
    {
        return await context.Lectures
                .Where(entity => entity.IsValid && entity.Id == id && entity.HcpId == hcpId)
                .Select(entity => new LectureDetailsDto
                {
                    Id = entity.Id,
                    Title = entity.Title,
                    Description = entity.Description,
                    Link = entity.Link,
                    LectureFileDtos = entity.LectureFiles.Where(file => file.IsValid)
                    .Select(file => new FileDataDto
                    {
                        Id = file.Id,
                        Extension = file.Extension,
                        Url = file.Url
                    }).ToList()
                }).FirstOrDefaultAsync();
    }
    #endregion
}

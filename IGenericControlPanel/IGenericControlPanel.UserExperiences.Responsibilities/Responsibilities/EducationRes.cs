﻿
using IGenericControlPanel.Content.Dto;
using IGenericControlPanel.Model.Content.File;
using IGenericControlPanel.Model.UserExperiences;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.UserExperiences.Dto;
using IGenericControlPanel.UserExperiences.Dto.Education;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.UserExperiences.Responsibilities.Responsibilities;
public static class EducationRes
{
    #region RESPONSIBILITIES

    public static async Task<IEnumerable<EducationDetailsDto>> GetEducationsForResume(
        this CPDbContext context, int hcpId)
    {
        return await context.Educations
                .Where(entity => entity.IsValid && entity.HcpId == hcpId)
                .Select(entity => new EducationDetailsDto
                {
                    Id = entity.Id,
                    SchoolName = entity.SchoolName,
                    Description = entity.Description,
                    StartDate = entity.StartDate,
                    EndDate = entity.EndDate,
                    Link = entity.Link,
                    Grade = entity.Grade,
                    ScientificDegree = entity.ScientificDegree,
                    StudyingField = entity.StudyingField,
                    EducationFiles = entity.EducationFiles
                    .Where(fileEntity => fileEntity.IsValid)
                    .Select(fileEntity => new FileDataDto
                    {
                        Id = fileEntity.Id,
                        Extension = fileEntity.Extension,
                        Url = fileEntity.Url
                    }).ToList()
                }).ToListAsync();
    }

    public static Education GenerateEducationEntityAsync(
     this EducationBasicDto actionDto, int hcpId,
     IEnumerable<FileDetailsDto> fileDtos)
    {
        var educationEntity = new Education
        {
            Id = actionDto.Id,
            Description = actionDto.Description,
            HcpId = hcpId,
            StartDate = actionDto.StartDate,
            EndDate = actionDto.EndDate,
            Grade = actionDto.Grade,
            Link = actionDto.Link,
            SchoolName = actionDto.SchoolName,
            ScientificDegree = actionDto.ScientificDegree,
            StudyingField = actionDto.StudyingField,
        };

        if (fileDtos != null && fileDtos.Any())
        {
            foreach (var fileDto in fileDtos)
            {
                var educationFile = new EducationFile
                {
                    Checksum = fileDto.Checksum,
                    FileContentType = fileDto.FileContentType,
                    Description = fileDto.Description,
                    Extension = fileDto.Extension,
                    FileName = fileDto.FileName,
                    Name = fileDto.Name,
                    Url = fileDto.Url,
                };

                educationEntity.EducationFiles.Add(educationFile);
            }
        }

        return educationEntity;
    }
    public static async Task<IEnumerable<EducationDto>> GetEducationsAsync(
        this CPDbContext context, int hcpId)
    {
        return await context.Educations
                .Where(ed => ed.IsValid && ed.HcpId == hcpId)
                .Select(ed => new EducationDto
                {
                    Id = ed.Id,
                    StudyingField = ed.StudyingField,
                    SchoolName = ed.SchoolName,
                    Description = ed.Description,
                    StartDate = ed.StartDate,
                    EndDate = ed.EndDate,
                    Link = ed.Link,
                    Grade = ed.Grade,
                    ScientificDegree = ed.ScientificDegree,
                    EducationImageUrl = ed.EducationFiles.Select(edF => edF.Url).FirstOrDefault()
                }).ToListAsync();

    }
    public static async Task<EducationDetailsDto> GetEducationDetailsAsync(
        this CPDbContext context, int id, int hcpId)
    {
        return await context.Educations
               .Where(entity => entity.IsValid && entity.Id == id && entity.HcpId == hcpId)
               .Select(entity => new EducationDetailsDto
               {
                   Id = entity.Id,
                   StudyingField = entity.StudyingField,
                   SchoolName = entity.SchoolName,
                   Description = entity.Description,
                   StartDate = entity.StartDate,
                   EndDate = entity.EndDate,
                   Link = entity.Link,
                   Grade = entity.Grade,
                   ScientificDegree = entity.ScientificDegree,
                   EducationFiles = entity.EducationFiles.Select(entityFile => new FileDataDto
                   {
                       Id = entityFile.Id,
                       Url = entityFile.Url,
                       Extension = entityFile.Extension
                   }).ToList()
               }).FirstOrDefaultAsync();
    }
    #endregion
}

﻿using System;
using System.Threading.Tasks;

using IGenericControlPanel.Seed.Seeders;

namespace IGenericControlPanel.Seed
{
    public static class Seeder
    {
        public async static Task SeedAdminsAsync(IServiceProvider serviceProvider)
        {
            var seederService = new SeederService(serviceProvider);

            await seederService.CreateRoles();

            await seederService.CreateUsers();
        }

        public async static Task SeedSettings(IServiceProvider serviceProvider)
        {
            var seederService = new SeederService(serviceProvider);
            await seederService.CreateSettings();
        }

        public async static Task SeedWebContentAsync(IServiceProvider serviceProvider)
        {
            var seederService = new SeederService(serviceProvider);
            await seederService.SeedWebContentAsync();
        }

        public async static Task SeedUserLanguages(IServiceProvider serviceProvider)
        {
            var seederService = new SeederService(serviceProvider);
            await seederService.CreateUserLanguages();
        }
    }
}

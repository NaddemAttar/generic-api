﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IGenericControlPanel.Model;
using IGenericControlPanel.Model.Configuration;
using IGenericControlPanel.Model.Content;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.Model.Security.Permissions;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Enums.Controllers;

using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace IGenericControlPanel.Seed.Seeders
{
    public class SeederService : ISeederService
    {
        private UserManager<CPUser> _userManager;
        private readonly RoleManager<CPRole> _roleManager;
        private CPDbContext _context;
        public SeederService(IServiceProvider serviceProvider)
        {
            _context = serviceProvider.GetService<CPDbContext>();
            _userManager = serviceProvider.GetService<UserManager<CPUser>>();
            _roleManager = serviceProvider.GetService<RoleManager<CPRole>>();
        }

        public async Task CreateRoles()
        {
            if (await _roleManager.FindByNameAsync("Admin") is null)
            {
                var roleItem = "Admin";
                var identityResult = await _roleManager.CreateAsync(new CPRole()
                {
                    Name = roleItem,
                    NormalizedName = roleItem.ToUpper()
                });


                if (!identityResult.Succeeded)
                {
                    string errors = null;
                    foreach (var item in identityResult.Errors)
                    {
                        errors += $"{item} | ";
                    }

                    throw new Exception($"Could not create the roles! Check your seeder! \n {errors}");
                }
            }
        }

        public async Task CreateUsers()
        {
            if (await _roleManager.RoleExistsAsync("Admin"))
            {
                var normalUser = new CPUser
                {
                    UserName = "admin",
                    Email = "admin@admin.com",
                    PhoneNumber = "+963955555555",
                    EmailConfirmed = true,
                };
                await _userManager.CreateAsync(normalUser, "Aaa@123");
                await _userManager.AddToRoleAsync(normalUser, "Admin");
            }
            else
            {
                Console.WriteLine("WARNING: Could not create an admin user because there's no such a role! Seed roles before users.");
            }
        }
        public async Task SeedWebContentAsync()
        {
            var controllers = Enum.GetNames(typeof(ControllersNames));

            var entities = new List<WebContentSet>();
            var webContents = new List<WebContentRoleSet>();

            foreach (var controller in controllers)
            {
                entities.Add(new WebContentSet()
                {
                    IsValid = true,
                    Name = controller
                });
            }

            await _context.AddRangeAsync(entities);
            await _context.SaveChangesAsync();

            var role = _context.Roles.Where(role => role.Name == nameof(Role.Admin)).SingleOrDefault();

            foreach (var webContent in entities)
            {
                webContents.Add(new WebContentRoleSet()
                {
                    IsValid = true,
                    RoleId = role.Id,
                    ContentId = webContent.Id,
                    CanAdd = true,
                    CanDelete = true,
                    CanEdit = true,
                    CanView = true,
                });
            }
            await _context.AddRangeAsync(webContents);
            await _context.SaveChangesAsync();
        }
        public async Task CreateSettings()
        {
            var SettingsData = new List<Setting>()
            {
                new Setting()
                {
                    Name = "PhoneNumber",
                    Value = "+963-999######"
                },

                 new Setting()
                {
                    Name = "DefaultCurrency_ar-SY",
                    Value = "ل.س"
                },

                   new Setting()
                {
                    Name = "DefaultCurrency_en-US",
                    Value = "L.S"
                },

                  new Setting()
                {
                    Name = "Whatsapp",
                    Value = "+963 999 999 999"
                },

                   new Setting()
                {
                    Name = "Telegram",
                    Value = "+963 999 999 999"
                },

                   new Setting()
                {
                    Name = "DefaultCultureCode",
                    Value = "ar-SY"
                },
                    new Setting()
                {
                    Name = "WorkTime",
                    Value = "From 9:00 AM - 6:00 PM"
                },
                     new Setting()
                {
                    Name = "Location",
                    Value = "Syria, Aleppo, Al-Jamilih"
                },
                      new Setting()
                {
                    Name = "Email",
                    Value = "craftlab.sy@gmail.com"
                },
                     new Setting()
                {
                    Name = "longitude",
                    Value = "37.143298173648446"
                },
                       new Setting()
                {
                    Name = "latitude",
                    Value = "36.20594682904884"
                },
                        new Setting()
                {
                    Name = "CurrentLanguage",
                    Value = "ar-SY"
                },
                         new Setting()
                {
                    Name = "FacebookLink",
                    Value = "ar-SY"
                },
                          new Setting()
                {
                    Name = "LinkedInLink",
                    Value = "ar-SY"
                }

            };

            await _context.AddRangeAsync(SettingsData);
            await _context.SaveChangesAsync();
        }

        public async Task CreateUserLanguages()
        {
            var userLanguagesData = new List<UserLanguage>()
            {
                new UserLanguage()
                {
                    CultureCode = "ar-SY",
                    LanguageName = "العربية",
                    LanguageCultureCode = "ar-SY",
                    Order = 1,
                    IsPrimaryLanguage = true,
                },
                 new UserLanguage()
                {
                    CultureCode = "en-US",
                    LanguageName = "الإنكليزية",
                    LanguageCultureCode = "en-US",
                    Order = 2,
                    IsPrimaryLanguage = false,
                }
            };

            await _context.AddRangeAsync(userLanguagesData);
            await _context.SaveChangesAsync();
        }

       
    }
}

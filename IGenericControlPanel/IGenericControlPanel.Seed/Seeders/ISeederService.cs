﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGenericControlPanel.Seed.Seeders
{
    public interface ISeederService
    {
        Task CreateRoles();
        Task CreateUsers();
        Task CreateSettings();
        Task SeedWebContentAsync();
    }
}

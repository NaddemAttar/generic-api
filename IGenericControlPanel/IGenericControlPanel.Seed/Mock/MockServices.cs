﻿using CraftLab.Core.Database.Identity.Interfaces;

using System;
using System.Collections.Generic;
using System.Text;

namespace IGenericControlPanel.Seed.Mock
{
    public class MockUserServices : IUserService
    {
        public IId GetCurrentUserId<IId>()
        {
            return default;
        }

        public bool IsUserLogin()
        {
            return true;
        }
    }
}

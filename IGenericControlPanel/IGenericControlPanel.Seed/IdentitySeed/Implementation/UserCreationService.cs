﻿using IGenericControlPanel.Model.Security;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Seed.IdentitySeed.Interfaces;
using IGenericControlPanel.SharedKernal.Enums;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;

namespace MProducts.TestAndSeed.Seed.IdentitySeed.Services
{
    public class UserCreationService : IUserCreationService
    {
        public static int AdminId = 1;
        private UserManager<CPUser> UserManager { get; }
        private CPDbContext Context { get; }


        public UserCreationService(UserManager<CPUser> userManager, CPDbContext context)
        {
            Context = context;
            UserManager = userManager;
        }

        public async Task CreateUsers()
        {
            #region Normal user

            var normalUser = new CPUser
            {
                Id = 1,
                UserName = "admin",
                Email = "admin@admin.com",
                PhoneNumber = "+963955555555",
                EmailConfirmed = true,
            };

            var result = await UserManager.CreateAsync(normalUser, "Aaa@123");

            if (result.Succeeded == false)
            {
                foreach (var error in result.Errors)
                {
                    Console.WriteLine(error.Description);
                }
            }
            else
            {
                Console.WriteLine("admin Created successfully.");
                result = await UserManager.AddToRoleAsync(normalUser, Role.Admin.ToString());
                if (result.Succeeded == false)
                {
                    foreach (var error in result.Errors)
                    {
                        Console.WriteLine(error.Description);
                    }
                }
                else
                {
                    Console.WriteLine("normal user added to user role successfully.");
                }
            }
            #endregion

        }
    }
}

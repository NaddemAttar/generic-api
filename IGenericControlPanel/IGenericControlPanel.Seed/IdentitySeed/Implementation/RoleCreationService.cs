﻿using IGenericControlPanel.Model.Security;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Enums;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace IGenericControlPanel.Seed.IdentitySeed.Interfaces
{
    public class RoleCreationService : IRoleCreationService
    {
        private readonly RoleManager<CPRole> RoleManager;

        public RoleCreationService(RoleManager<CPRole> roleManager)
        {
            RoleManager = roleManager;
        }

        public async Task CreateRoles()
        {
            var result = await RoleManager.CreateAsync(new CPRole()
            {
                Name = nameof(Role.Admin)
            });

            if (result.Succeeded == false)
            {
                foreach (var error in result.Errors)
                {
                    System.Console.WriteLine(error.Description);
                }
            }
            else
            {
                System.Console.WriteLine("Done.");
            }
        }
    }

}

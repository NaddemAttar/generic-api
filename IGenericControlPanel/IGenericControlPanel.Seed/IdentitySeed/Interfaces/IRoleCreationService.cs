﻿using System.Threading.Tasks;

namespace IGenericControlPanel.Seed.IdentitySeed.Interfaces
{
    public interface IRoleCreationService
    {
        Task CreateRoles();
    }
}

﻿using System.Threading.Tasks;

namespace IGenericControlPanel.Seed.IdentitySeed.Interfaces
{
    public interface IUserCreationService
    {
        Task CreateUsers();
    }
}

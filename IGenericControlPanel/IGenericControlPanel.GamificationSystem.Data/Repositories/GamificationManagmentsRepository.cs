﻿
using CraftLab.Core.OperationResults;
using IGenericControlPanel.GamificationSystem.Dto.Gamification;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.GamificationSystem.Responsibilities.Responsibilities;
using IGenericControlPanel.Model.Gamification;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Extension;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.GamificationSystem.Data.Repositories;
public class GamificationManagmentsRepository : BasicRepository<CPDbContext, Gamification>,
    IGamificationManagmentsRepository
{
    public GamificationManagmentsRepository(CPDbContext context) : base(context)
    { }
    public async Task<OperationResult<GenericOperationResult, GamificationDto>> AddNewGamificationSectionAsync(
        GamificationDto dto)
    {
        OperationResult<GenericOperationResult,
            GamificationDto> result = new(GenericOperationResult.Success);

        try
        {
            if (CheckIfTheKeyIsExistBefore(dto.Key))
            {
                return result.AddError(ErrorMessages.KeyIsExist)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            var gamificationEntity = new Gamification
            {
                TextKey = dto.Key,
                PointsValue = dto.Value,
                Enable = dto.Enable,
                GamificationType = dto.GamificationType
            };

            Context.Gamifications.Add(gamificationEntity);
            await Context.SaveChangesAsync();

            return await Task.FromResult(result.UpdateResultData(dto));
        }
        catch (Exception)
        {
            return await Task.FromResult(result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed));
        }
    }

    public async Task<OperationResult<GenericOperationResult, IEnumerable<GamificationDto>>> GetAllGamificationSectionsAsync()
    {
        OperationResult<GenericOperationResult,
            IEnumerable<GamificationDto>> result = new(GenericOperationResult.Success);

        try
        {
            var data = Context.Gamifications
                .Where(entity => entity.IsValid)
                .Select(entity => new GamificationDto
                {
                    Key = entity.TextKey,
                    Value = entity.PointsValue,
                    Enable = entity.Enable,
                    GamificationType = entity.GamificationType,
                }).ToList();

            return await Task.FromResult(result.UpdateResultData(data));
        }
        catch (Exception)
        {
            return await Task.FromResult(result.AddError(ErrorMessages.InternalServerError));
        }
    }

    public async Task<OperationResult<GenericOperationResult, IEnumerable<GamificationDto>>> UpdatePointsForGamificactionSectionsAsync(
        IEnumerable<GamificationDto> gamifications)
    {
        OperationResult<GenericOperationResult,
            IEnumerable<GamificationDto>> result = new(GenericOperationResult.Success);

        try
        {
            if (CheckIfTheValueIsZeroInEnableStatus(gamifications))
            {
                return result.AddError(ErrorMessages.ValueCanNotBeZero)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }
            var resultEntities = gamifications.Select(dto => new Gamification
            {
                TextKey = dto.Key,
                PointsValue = dto.Value,
                Enable = dto.Enable,
                GamificationType = dto.GamificationType
            }).ToList();
            foreach(var game in resultEntities){
                game.Id=Context.Gamifications.Where(gamefication=>gamefication.TextKey==game.TextKey).AsNoTracking().SingleOrDefault().Id;
            }

            Context.Gamifications.UpdateRange(resultEntities);
            await Context.SaveChangesAsync();

            return await Task.FromResult(result.UpdateResultData(gamifications));
        }
        catch (Exception)
        {
            return await Task.FromResult(result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed));
        }

    }

    public async Task<OperationResult<GenericOperationResult, IEnumerable<UserWithPointsDto>>> GetUsersWithTheirPointsAsync(
        FiltersUsersDto filtersDto)
    {
        OperationResult<GenericOperationResult,
            IEnumerable<UserWithPointsDto>> result = new(GenericOperationResult.Success);

        try
        {
            var data = Context.GetUsersRes(filtersDto);
            data = data.GetUsersWithAscendingOrderRes(filtersDto.IsAscendingOrder);
            data = data.GetUsersWithPaginationRes(filtersDto);

            return result.UpdateResultData(await data.GetUsersWithPointsDtoResAsync());
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }

    #region Helper Methods
    private bool CheckIfTheValueIsZeroInEnableStatus(IEnumerable<GamificationDto> gamifications) =>
         gamifications.Any(gamification => gamification.Enable && gamification.Value == 0);
    private bool CheckIfTheKeyIsExistBefore(string key) =>
        Context.Gamifications.Any(gam => gam.TextKey == key);
    #endregion


}

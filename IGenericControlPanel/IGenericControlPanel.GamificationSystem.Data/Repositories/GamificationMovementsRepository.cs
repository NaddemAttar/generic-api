﻿using CraftLab.Core.OperationResults;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.Model.Configuration;
using IGenericControlPanel.Model.Gamification;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.AspNetCore.Identity;

namespace IGenericControlPanel.GamificationSystem.Data.Repositories;
public class GamificationMovementsRepository : BasicRepository<CPDbContext, Gamification>,
    IGamificationMovementsRepository
{
    #region Consttuctor & Properties 
    private UserManager<CPUser> _userManager { get; }
    private IUserRepository _userRepository { get; }
    public GamificationMovementsRepository(
        CPDbContext context,
        IUserRepository userRepository,
        UserManager<CPUser> userManager) : base(context)
    {
        _userRepository = userRepository;
        _userManager = userManager;
    }
    #endregion

    #region Save points with tracking gamification operations
    public async Task<OperationResult<GenericOperationResult>> SavePoints(IEnumerable<string> keys)
    {
        OperationResult<GenericOperationResult> result = new(GenericOperationResult.Success);

        try
        {
            var userInfo = await _userRepository.GetCurrentUserInfo();

            if (userInfo.CurrentUserId == 0)
            {
                return result.AddError(ErrorMessages.LogInFirstBeforeDoingAnyAction)
                    .UpdateResultStatus(GenericOperationResult.Failed);
            }

            if (userInfo.IsUserAdmin)
            {
                return result;
            }

            var gamificationEntities = Context.Gamifications
                .Where(gm => keys.Contains(gm.TextKey)).ToList();

            var savePointsResult = await SavePointsInDB(gamificationEntities,
                userInfo.CurrentUserId);

            var savePointsInUsersPointsTable = await TrackingGamificationOperations(gamificationEntities);

            if (savePointsResult && savePointsInUsersPointsTable)
            {
                return result;
            }

            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }

    private async Task<bool> SavePointsInDB(
        IEnumerable<Gamification> gamificationEntities, int userId)
    {
        try
        {
            int pointsSum = 0;
            foreach (var gamificationEntity in gamificationEntities)
            {
                if (gamificationEntity.Enable)
                {
                    pointsSum = gamificationEntity.PointsValue;
                }
            }

            CPUser user = await _userManager.FindByIdAsync(userId.ToString());
            user.TotalOfPoints += pointsSum;

            await _userManager.UpdateAsync(user);

            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }
    private async Task<bool> TrackingGamificationOperations(List<Gamification> entities)
    {
        try
        {
            var usersPointsEntities = entities
                .Where(entity => entity.Enable)
                .Select(gamificationEntity => new UsersPoints
                {
                    GamificationId = gamificationEntity.Id,
                    Value = gamificationEntity.PointsValue
                }).ToList();

            if (usersPointsEntities.Count() != 0)
            {
                await Context.UsersPoints.AddRangeAsync(usersPointsEntities);
                await Context.SaveChangesAsync();
            }

            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    #endregion
}

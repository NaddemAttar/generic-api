﻿
using CraftLab.Core.OperationResults;
using IGenericControlPanel.Configuration.Dto.Pagination;
using IGenericControlPanel.GamificationSystem.Dto.Coupon;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.Model.Gamification;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.SharedKernal.Extension;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.GamificationSystem.Data.Repositories;
public class CouponManagementRepository : BasicRepository<CPDbContext, Coupon>, ICouponManagementRepository
{
    #region Constructor
    public CouponManagementRepository(CPDbContext context) : base(context) { }
    #endregion

    #region Get Section
    public async Task<OperationResult<GenericOperationResult, CouponDto>> GetCouponDetailsAsync(int id)
    {
        OperationResult<GenericOperationResult,
          CouponDto> result = new(GenericOperationResult.Success);

        try
        {
            if (!await IsExistAsync(id))
            {
                return result.AddError(ErrorMessages.ThisCouponNotFound)
                    .UpdateResultStatus(GenericOperationResult.NotFound);
            }

            var dataDto = await Context.Coupons
                .Where(coupon => coupon.IsValid && coupon.Id == id)
                .Select(coupon => new CouponDto
                {
                    Id = coupon.Id,
                    CouponPeriod = coupon.CouponPeriod,
                    Percentage = coupon.Percentage,
                    Points = coupon.Points
                }).SingleOrDefaultAsync();

            return result.UpdateResultData(dataDto);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
              .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }

    public async Task<OperationResult<GenericOperationResult, IEnumerable<CouponDto>>> GetCouponsAsync(
        PaginationDto dto)
    {
        OperationResult<GenericOperationResult,
            IEnumerable<CouponDto>> result = new(GenericOperationResult.Success);

        try
        {
            var data = Context.Coupons.Where(e => e.IsValid);

            if (dto.EnablePagination)
            {
                data = data.GetDataWithPagination(dto.PageSize, dto.PageNumber);
            }
            var resultDto = await data.Select(e => new CouponDto
            {
                Id = e.Id,
                Points = e.Points,
                Percentage = e.Percentage,
                CouponPeriod = e.CouponPeriod
            }).ToListAsync();

            return result.UpdateResultData(resultDto);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    #endregion

    #region Remove Coupon
    public async Task<OperationResult<GenericOperationResult>> RemoveCouponAsync(int id)
    {
        OperationResult<GenericOperationResult> result = new(GenericOperationResult.Success);

        try
        {
            if (!await IsExistAsync(id))
            {
                return result.AddError(ErrorMessages.ThisCouponNotFound)
                    .UpdateResultStatus(GenericOperationResult.NotFound);
            }

            Context.Coupons.Remove(Context.Coupons
                .Where(cp => cp.IsValid && cp.Id == id)
                .SingleOrDefault());

            await Context.SaveChangesAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    #endregion

    #region Add Coupon
    public async Task<OperationResult<GenericOperationResult, CouponDto>> AddCouponAsync(
        CouponDto createNewCouponDto)
    {
        OperationResult<GenericOperationResult,
            CouponDto> result = new(GenericOperationResult.Success);

        try
        {
            if (await IsSameValueAsync(createNewCouponDto.Points))
            {
                return result.AddError(ErrorMessages.ThisPointsValueIsExistBefore)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            if (!await CheckIfTheValueIsAvailable(createNewCouponDto))
            {
                return result.AddError(ErrorMessages.CanNotAddThisPercentage)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            await Context.AddAsync(CreateNewCoupon(createNewCouponDto));
            await Context.SaveChangesAsync();

            return result.UpdateResultData(createNewCouponDto);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    private Coupon CreateNewCoupon(CouponDto createNewCouponDto)
    {
        return new Coupon
        {
            Points = createNewCouponDto.Points,
            Percentage = createNewCouponDto.Percentage,
            CouponPeriod = createNewCouponDto.CouponPeriod
        };
    }

    #endregion

    #region Update Coupon
    public async Task<OperationResult<GenericOperationResult, CouponDto>> UpdateCouponAsync(
        CouponDto updateCouponDto)
    {
        OperationResult<GenericOperationResult,
             CouponDto> result = new(GenericOperationResult.Success);

        try
        {
            if (await IsSameValueAsync(updateCouponDto.Points, updateCouponDto.Id))
            {
                return result.AddError(ErrorMessages.ThisPointsValueIsExistBefore)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            if (!await CheckIfTheValueIsAvailable(updateCouponDto))
            {
                return result.AddError(ErrorMessages.CanNotAddThisPercentage)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            return result.UpdateResultData(await UpadteCouponAsync(updateCouponDto));
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
               .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    private async Task<CouponDto> UpadteCouponAsync(CouponDto updateCouponDto)
    {
        var couponEntity = await GetCouponForUpdateAsync(updateCouponDto.Id);

        couponEntity.Points = updateCouponDto.Points;
        couponEntity.Percentage = updateCouponDto.Percentage;
        couponEntity.CouponPeriod = updateCouponDto.CouponPeriod;

        Context.Coupons.Update(couponEntity);
        await Context.SaveChangesAsync();

        return updateCouponDto;
    }
    private async Task<Coupon> GetCouponForUpdateAsync(int id)
    {
        return await Context.Coupons
            .Where(e => e.IsValid && e.Id == id)
            .SingleOrDefaultAsync();
    }
    #endregion

    #region Helper Methods
    public async Task<bool> IsExistAsync(int id) =>
        await Context.Coupons.AnyAsync(e => e.IsValid && e.Id == id);

    public async Task<bool> IsSameValueAsync(int value, int id = 0) =>
        await Context.Coupons.AnyAsync(e => (id == 0 ? true : e.Id != id) && e.IsValid && e.Points == value);
    private async Task<bool> CheckIfTheValueIsAvailable(CouponDto dto)
    {
        var checkTheBiggerStatus = await Context.Coupons
            .Where(e => (e.Id == 0 ? true : e.Id != dto.Id) && e.Points > dto.Points)
            .AnyAsync(e => e.Percentage <= dto.Percentage);

        var checkTheSmallerStatus = await Context.Coupons
            .Where(e => (e.Id == 0 ? true : e.Id != dto.Id) && e.Points < dto.Points)
            .AnyAsync(e => e.Percentage >= dto.Percentage);

        return !checkTheBiggerStatus && !checkTheSmallerStatus;
    }

    #endregion
}

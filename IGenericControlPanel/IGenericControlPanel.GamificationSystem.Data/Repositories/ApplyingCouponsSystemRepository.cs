﻿
using CraftLab.Core.OperationResults;
using IGenericControlPanel.GamificationSystem.Dto.Coupon;
using IGenericControlPanel.GamificationSystem.IData.Interfaces;
using IGenericControlPanel.Model.Gamification;
using IGenericControlPanel.Model.Security;
using IGenericControlPanel.MySql.Database;
using IGenericControlPanel.Security.IData.Interfaces;
using IGenericControlPanel.SharedKernal.Messages;
using IGenericControlPanel.SharedKernal.Repository;
using Microsoft.EntityFrameworkCore;

namespace IGenericControlPanel.GamificationSystem.Data.Repositories;
public class ApplyingCouponsSystemRepository : BasicRepository<CPDbContext, Coupon>,
    IApplyingCouponsSystemRepository
{
    #region Constructor & Properties 
    private readonly IUserRepository _userRepository;
    public ApplyingCouponsSystemRepository(
        IUserRepository userRepository,
        CPDbContext context) : base(context)
    {
        _userRepository = userRepository;
    }
    #endregion

    #region Get Availabe Coupons 
    public async Task<OperationResult<GenericOperationResult, IEnumerable<CouponDto>>> GetAvailableCouponsAsync()
    {
        OperationResult<GenericOperationResult,
            IEnumerable<CouponDto>> result = new(GenericOperationResult.Success);

        try
        {
            int curruentUserId = _userRepository.CurrentUserIdentityId();

            if (curruentUserId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            var availableCoupons = await GetAvailableCouponsFromDb(
                await GetTotalPointsByUser(curruentUserId)
                );

            return result.UpdateResultData(availableCoupons);
        }

        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }


    private async Task<int> GetTotalPointsByUser(int userId)
    {
        try
        {
            var userEntity = await Context.Users
                .Where(user => user.IsValid && user.Id == userId)
                .FirstOrDefaultAsync();

            return userEntity.TotalOfPoints;
        }
        catch (Exception)
        {
            return 0;
        }
    }

    private async Task<IEnumerable<CouponDto>> GetAvailableCouponsFromDb(int pointsValue)
    {
        return await Context.Coupons
            .Where(c => c.IsValid && c.Points <= pointsValue)
            .Select(c => new CouponDto
            {
                Id = c.Id,
                CouponPeriod = c.CouponPeriod,
                Percentage = c.Percentage,
                Points = c.Points
            })
            .ToListAsync();
    }
    #endregion

    #region Coupon Booking
    public async Task<OperationResult<GenericOperationResult, string>> CouponBookingAsync(int couponId)
    {
        OperationResult<GenericOperationResult,
            string> result = new(GenericOperationResult.Success);

        try
        {
            int currentUserId = _userRepository.CurrentUserIdentityId();

            if (currentUserId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.NotFound);
            }

            var couponEntity = await GetCouponById(couponId);

            if (couponEntity is null)
            {
                return result.AddError(ErrorMessages.ThisCouponNotFound)
                    .UpdateResultStatus(GenericOperationResult.NotFound);
            }

            if (await CheckIfTheUserHasCoupon(currentUserId))
            {
                return result.AddError(ErrorMessages.ThisUserHasCouponNotUsingItYet)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            var userCouponEntityToCurrentUser = CreateNewCouponToCurrentUser(couponEntity);
            await Context.UserCoupons.AddAsync(userCouponEntityToCurrentUser);

            await Context.SaveChangesAsync();

            return result.UpdateResultData(userCouponEntityToCurrentUser.Code);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    private async Task<bool> CheckIfTheUserHasCoupon(int currentUserId)
    {
        return await Context.UserCoupons.AnyAsync(cp =>
        cp.IsValid &&
        cp.CreatedBy == currentUserId &&
        (cp.Activate || cp.ExpiryDate >= DateTime.Now));
    }

    public async Task<Coupon> GetCouponById(int id) =>
        await Context.Coupons.Where(cp => cp.IsValid && cp.Id == id).FirstOrDefaultAsync();
    private UserCoupon CreateNewCouponToCurrentUser(Coupon couponEntity)
    {
        return new UserCoupon
        {
            Activate = false,
            CouponId = couponEntity.Id,
            Percentage = couponEntity.Percentage,
            Points = couponEntity.Points,
            ExpiryDate = DateTime.Now.AddDays(couponEntity.CouponPeriod),
        };
    }
    #endregion

    #region Activate & DeActivate Coupon Booking
    public async Task<OperationResult<GenericOperationResult>> ActivateCouponBookingAsync(string code)
    {

        OperationResult<GenericOperationResult> result = new(GenericOperationResult.Success);

        try
        {
            var userCouponEntity = await GetUserCoupon(code);
            if (userCouponEntity is null)
            {
                return result.AddError(ErrorMessages.ThisCouponIsNotAvailable)
                    .UpdateResultStatus(GenericOperationResult.NotFound);
            }

            if (userCouponEntity.Activate)
            {
                return result.AddError(ErrorMessages.ThisCouponIsActivatedAlready)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            userCouponEntity.Activate = true;
            Context.UserCoupons.Update(userCouponEntity);

            await Context.SaveChangesAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    public async Task<OperationResult<GenericOperationResult>> DeActivateCouponBookingAsync()
    {
        OperationResult<GenericOperationResult> result = new(GenericOperationResult.Success);

        try
        {
            var userCouponEntity = await GetUserCoupon();

            if (userCouponEntity is null)
            {
                return result.AddError(ErrorMessages.ThisCouponIsNotAvailable)
                    .UpdateResultStatus(GenericOperationResult.NotFound);
            }

            if (!userCouponEntity.Activate)
            {
                return result.AddError(ErrorMessages.ThisCouponIsDeActivatedAlready)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            userCouponEntity.Activate = false;
            Context.UserCoupons.Update(userCouponEntity);
            await Context.SaveChangesAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    #endregion

    #region Get User Coupons 
    public async Task<OperationResult<GenericOperationResult, IEnumerable<UserCouponDto>>> GetUserCouponsAsync(
        bool isBeneficiary, bool isActivate)
    {
        OperationResult<GenericOperationResult,
            IEnumerable<UserCouponDto>> result = new(GenericOperationResult.Success);

        try
        {
            if (isActivate && isBeneficiary)
            {
                return result.AddError(ErrorMessages.ThisStatusIsNotAvailableInTheSystem)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            int currentUserId = _userRepository.CurrentUserIdentityId();

            if (currentUserId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            var userCpuponDtos = await Context.UserCoupons
            .Where(userCoupon => userCoupon.IsValid &&
            userCoupon.CreatedBy == currentUserId &&
            userCoupon.IsBeneficiary == isBeneficiary &&
            userCoupon.Activate == isActivate)
            .Select(userCoupon => new UserCouponDto
            {
                Id = userCoupon.Id,
                Percentage = userCoupon.Percentage,
                Points = userCoupon.Points
            }).ToListAsync();

            return result.UpdateResultData(userCpuponDtos);
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    #endregion

    private async Task<UserCoupon> GetUserCoupon(string code = null)
    {
        int currentUserId = _userRepository.CurrentUserIdentityId();

        return await Context.UserCoupons.Where(ucp =>
        ucp.IsValid &&
        ucp.CreatedBy == currentUserId &&
        (code == null ? true : code == ucp.Code) &&
       !ucp.IsBeneficiary &&
        ucp.ExpiryDate >= DateTime.Now)
            .FirstOrDefaultAsync();
    }

    #region Functionality To Deduct the Points from user after using Coupon
    public async Task<OperationResult<GenericOperationResult>> PointsDeductionFromUserAsync(int userCouponId)
    {
        OperationResult<GenericOperationResult> result = new(GenericOperationResult.Success);

        using var transactionAsync = await Context.Database.BeginTransactionAsync();

        try
        {
            if (userCouponId == 0)
            {
                return result.AddError(ErrorMessages.UserCouponIsNotValid)
                .UpdateResultStatus(GenericOperationResult.ValidationError);
            }
            int currentUserId = _userRepository.CurrentUserIdentityId();

            if (currentUserId == 0)
            {
                return result.AddError(ErrorMessages.UserMustLoginFirst)
                    .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            var userCouponEntity = await GetUserCouponByUserCouponId(userCouponId, currentUserId);

            var checkAllCasesToUserCouponEntity = CheckAllCasesToUserCouponEntity(userCouponEntity);

            if (checkAllCasesToUserCouponEntity.EnumResult != GenericOperationResult.Success)
            {
                return result.AddError(checkAllCasesToUserCouponEntity.ErrorMessages)
                    .UpdateResultStatus(checkAllCasesToUserCouponEntity.EnumResult);
            }

            userCouponEntity = checkAllCasesToUserCouponEntity.Result;

            var currentUserEntity = await GetUserById(currentUserId);

            if (currentUserEntity.TotalOfPoints < userCouponEntity.Points)
            {
                return result.AddError(ErrorMessages.ThisUserHasNotEnoughPointsToUseThisCoupon)
                  .UpdateResultStatus(GenericOperationResult.ValidationError);
            }

            currentUserEntity.TotalOfPoints -= userCouponEntity.Points;
            userCouponEntity.IsBeneficiary = true;
            userCouponEntity.Activate = false;

            Context.UserCoupons.Update(userCouponEntity);
            Context.Users.Update(currentUserEntity);
            await Context.SaveChangesAsync();

            await transactionAsync.CommitAsync();

            return result;
        }
        catch (Exception)
        {
            return result.AddError(ErrorMessages.InternalServerError)
                 .UpdateResultStatus(GenericOperationResult.Failed);
        }
    }
    private OperationResult<GenericOperationResult, UserCoupon> CheckAllCasesToUserCouponEntity(
        UserCoupon userCouponEntity)
    {
        OperationResult<GenericOperationResult,
            UserCoupon> result = new(GenericOperationResult.Success);

        if (userCouponEntity is null)
        {
            return result.AddError(ErrorMessages.ThisUserHasNotCoupon)
                .UpdateResultStatus(GenericOperationResult.NotFound);
        }

        if (!userCouponEntity.Activate)
        {
            return result.AddError(ErrorMessages.ThisUserHasNotActivateCoupon)
                .UpdateResultStatus(GenericOperationResult.ValidationError);
        }

        if (userCouponEntity.ExpiryDate < DateTime.Now)
        {
            return result.AddError(ErrorMessages.ThisCouponExpired)
                .UpdateResultStatus(GenericOperationResult.ValidationError);
        }

        return result.UpdateResultData(userCouponEntity);
    }
    private async Task<UserCoupon> GetUserCouponByUserCouponId(int userCouponId, int currentUserId)
    {
        return await Context.UserCoupons
                .Where(uc => uc.IsValid &&
                uc.Id == userCouponId &&
                uc.CreatedBy == currentUserId)
                .FirstOrDefaultAsync();
    }

    private async Task<CPUser> GetUserById(int userId) =>
        await Context.Users.Where(e => e.IsValid && e.Id == userId).FirstOrDefaultAsync();
    #endregion
}

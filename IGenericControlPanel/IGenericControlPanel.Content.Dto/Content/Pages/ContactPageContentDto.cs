﻿using System.Collections.Generic;

namespace IGenericControlPanel.Content.Dto
{
    public class ContactPageContentDto
    {
        public IDictionary<string, string> Labels { get; set; }
        public IDictionary<string, string> Settings { get; set; }
    }
}

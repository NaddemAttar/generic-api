﻿using System.Collections.Generic;

namespace IGenericControlPanel.Content.Dto
{
    public class MainPageContentDto
    {
        public IEnumerable<CarouselItemDto> MainSlider { get; set; }
        public ContactPageContentDto ContactPageContent { get; set; }
        public string AboutUs { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IGenericControlPanel.SharedKernal.Enums.Content;

namespace IGenericControlPanel.Content.Dto.Content
{
    public class PrivacyMaterialDto
    {
        public int Id { get; set; }
        public PrivacyMaterialsTypes Type { get; set; }
        public string Content { get; set; }
        public string Culture { get; set; }
    }
}

﻿using IGenericControlPanel.SharedKernal.Enums.File;

namespace IGenericControlPanel.Content.Dto
{
    public class FileDetailsDto : FileDto
    {
        public string Description { get; set; }
        public long Length { get; set; }
        public string Checksum { get; set; }
        public int SourceId { get; set; }
        public FileSourceType FileSourceType { get; set; }
    }
}

﻿using IGenericControlPanel.SharedKernal.Enums;
using IGenericControlPanel.SharedKernal.Enums.File;

namespace IGenericControlPanel.Content.Dto
{
    public class FileFilterCriteriaDto
    {
        public int SourceId { get; set; }
        public FileSourceType SourceType { get; set; } = FileSourceType.All;
        public FileContentTypee FileContentType { get; set; }
    }
}

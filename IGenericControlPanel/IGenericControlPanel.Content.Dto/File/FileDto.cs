﻿using CraftLab.Core.BoundedContext;
using CraftLab.Core.Services.HttpFile;

namespace IGenericControlPanel.Content.Dto
{
    public class FileDto : IFormDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public string Extension { get; set; }
        public string Url { get; set; }
        public int Order { get; set; }
        public bool IsValid { get; set; }
        public FileContentType FileContentType { get; set; }

        public string SourceName { get; set; }

        public ActionOperationType OperationType { get; }
        public string CultureCode { get; set; }
    }
}

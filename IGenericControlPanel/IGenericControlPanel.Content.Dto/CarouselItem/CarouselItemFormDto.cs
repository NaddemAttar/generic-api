﻿using System.Collections.Generic;

using CraftLab.Core.BoundedContext;

namespace IGenericControlPanel.Content.Dto
{
    public class CarouselItemFormDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string TopSubTitle { get; set; }
        public string BottomSubTitle { get; set; }
        public int Order { get; set; }
        public string ButtonActionUrl { get; set; }
        public string ButtonText { get; set; }
        public string CultureCode { get; set; }
        public int CarouselId { get; set; }
        public string ImageUrl { get; set; }
        public string TextColor { get; set; }
        public FileDetailsDto Image { get; set; }

        public ActionOperationType OperationType { get; }
        public IEnumerable<CarouselItemTranslationFormDto> TranslationsFormDtos { get; set; }
    }
}

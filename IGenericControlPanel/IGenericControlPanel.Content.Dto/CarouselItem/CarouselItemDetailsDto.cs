﻿using System.Collections.Generic;

namespace IGenericControlPanel.Content.Dto
{
    public class CarouselItemDetailsDto : CarouselItemDto
    {
        public IEnumerable<CarouselItemTranslationDto> Translations { get; set; }
        public FileDetailsDto Image { get; set; }
    }

}

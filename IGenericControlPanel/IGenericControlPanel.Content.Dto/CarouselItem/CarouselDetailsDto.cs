﻿using System.Collections.Generic;

using CraftLab.Core.BoundedContext;

namespace IGenericControlPanel.Content.Dto
{
    public class CarouselDetialsDto : IFormDto
    {
        public CarouselDetialsDto()
        {
            CarouselItems = new List<CarouselItemDetailsDto>();
        }
        public int Id { get; set; }
        public bool IsValid { get; set; }
        public string Name { get; set; }
        public ActionOperationType OperationType { get; }
        public string CultureCode { get; set; }
        public IEnumerable<CarouselItemDetailsDto> CarouselItems { get; set; }
    }


}

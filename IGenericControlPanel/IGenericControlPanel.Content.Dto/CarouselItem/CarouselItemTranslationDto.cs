﻿
using CraftLab.Core.BoundedContext;

namespace IGenericControlPanel.Content.Dto
{
    public class CarouselItemTranslationDto : IFormDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string TopSubTitle { get; set; }
        public string BottomSubTitle { get; set; }
        public string ButtonText { get; set; }

        public int CarouselItemId { get; set; }

        public string CultureCode { get; set; }

        public ActionOperationType OperationType { get; }
    }
}
